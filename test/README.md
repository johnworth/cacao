# Integration Testing

### Table of Contents
[[_TOC_]]


## Running the tests

You will need to run the ansible install with the setup expected for local development and Skaffold, with the addition of this variable in the `config.yml`:
```yaml
KEYCLOAK_USERS:
  - username: cacao-user
    password: cacao-user-password
    admin: false
  - username: cacao-admin
    password: cacao-admin-password
    admin: true
```

Then, go to the `../install` directory and run:
```
$ ansible-playbook -i <desired hosts file> playbook-integration-tests.yml
```

> :warning: This playbook will **delete all** user generated Kubernetes namespaces in the user cluster and drop and recreate the MongoDB deployment.  The reason for this is to clean up after failed tests that cause panics/segfaults.

## Gitlab CI setup

In order for the tests to run successfully in Gitlab, you need the following three CI variables present:

- `hosts_yml`: This should be a base64 encoded version of the contents of the hosts.yml file you want the CI environment to use.
- `svc_ci_id_ed25519`: This should be a base64 encoded ed25519 private key.
- `svc_ci_id_ed25519_pub`: This should be the corresponding public key to `svc_ci_id_ed25519`, also base64 encoded.  This key must be present on whichever machine is designated as `mgmt_host`  in your `hosts.yml` file.

Remember to use the `-w 0` option when using `base64` to encode these values, e.g.

```
$ cat ~/.ssh/gitlab_ci_id_ed25519 | base64 -w 0
```

If you don't use `-w 0`, you will end up with newline characters that will corrupt the value.


## Important Notes
- If you run the tests successfully directly using `go test` (i.e. not the Ansible playbook), everything except the `Runners` in MongoDB will be cleaned up at the end.

- The Build Service is currently not tested because it requires credentials for a container registry

- Since `WorkflowDefinitions` rely on git repositories, these tests could all be broken by a change to the `gitlab.com/cyverse/cacao-helloworld` repository

- `ScaleFromZero` functionality is currently untested because the retry requests will use the `Host` header of the incoming request rather than the original IP/domain being used


## About the tests
The tests are organized in files to group by what functionality they test. These files are named according to the order that they are run. Since order is important for these tests, each file has a slice near the top laying out the order of the tests in that file. Finally, all the slices are combined in `integration_test.go` and run in order. Then, it completes by deleting the namespace created for the test user, which will clean up all remaining resources in that namespace (this may take awhile).

Some variables are shared between test functions so that we can save the `workflowID` of a created `WorkflowDefinition` in order to use it for Runs and Builds.


## Adding to the tests
When adding tests, make sure to put your new test in the slice in the correct order. Make sure to be careful with variables defined at the top of the file before overwriting them because this could break other tests.

Some tests are defined in the corresponding files, but not included in the slice because they are instead added to the `cleanupTests` slice in `integration_test.go` to be run after all other tests. This is useful for deleting resources such as `WorkflowDefinitions` that may be needed by other tests.
