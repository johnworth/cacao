package test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

var workspaceID cacao_common.ID

var tests05Workspace = []testMap{
	{"CreateWorkspace": testCreateWorkspace},
	{"ListWorkspaces": testListWorkspaces},
	{"GetWorkspace": testGetWorkspace},
	{"UpdateWorkspace": testUpdateWorkspace},
	{"PatchWorkspace": testPatchWorkspace},
	{"DeleteWorkspace": testDeleteWorkspace},
}

func testCreateWorkspace(t *testing.T) {
	workspaceID = cacao_common_service.NewWorkspaceID()

	data := fmt.Sprintf(`{"id":"%s","name":"test_workspace_name","description":"test_workspace","default_provider_id":"test_default_provider_id"}`, workspaceID)

	req, err := newRequest("POST", "/workspaces", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
}

func testGetWorkspace(t *testing.T) {
	req, err := newRequest("GET", "/workspaces/"+workspaceID.String(), "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result cacao_common_http.Workspace
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, workspaceID, result.ID)
}

func testListWorkspaces(t *testing.T) {
	req, err := newRequest("GET", "/workspaces", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result []cacao_common_http.Workspace
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.GreaterOrEqual(t, len(result), 1)
}

func testUpdateWorkspace(t *testing.T) {
	// update
	data := `{"name":"test_workspace_name","description":"test_workspace_updated","default_provider_id":"test_default_provider_id_updated"}`

	reqUpdate, err := newRequest("PUT", "/workspaces/"+workspaceID.String(), data, userToken)
	assert.NoError(t, err)
	respUpdate, err := (&http.Client{}).Do(reqUpdate)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, respUpdate.StatusCode)

	// get
	reqGet, err := newRequest("GET", "/workspaces/"+workspaceID.String(), "", userToken)
	assert.NoError(t, err)
	respGet, err := (&http.Client{}).Do(reqGet)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, respGet.StatusCode)
	respBody, err := ioutil.ReadAll(respGet.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result cacao_common_http.Workspace
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, workspaceID, result.ID)
	assert.Equal(t, "test_workspace_updated", result.Description)
	assert.Equal(t, "test_default_provider_id_updated", string(result.DefaultProviderID))
}

func testPatchWorkspace(t *testing.T) {
	// patch
	data := `{"description":"test_workspace_patched"}`

	reqUpdate, err := newRequest("PATCH", "/workspaces/"+workspaceID.String(), data, userToken)
	assert.NoError(t, err)
	respUpdate, err := (&http.Client{}).Do(reqUpdate)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, respUpdate.StatusCode)

	// get
	reqGet, err := newRequest("GET", "/workspaces/"+workspaceID.String(), "", userToken)
	assert.NoError(t, err)
	respGet, err := (&http.Client{}).Do(reqGet)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, respGet.StatusCode)
	respBody, err := ioutil.ReadAll(respGet.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result cacao_common_http.Workspace
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, workspaceID, result.ID)
	assert.Equal(t, "test_workspace_patched", result.Description)
}

func testDeleteWorkspace(t *testing.T) {
	req, err := newRequest("DELETE", "/workspaces/"+workspaceID.String(), "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
}
