package test

import (
	// "context"
	// "encoding/base64"
	"io/ioutil"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	// "github.com/stretchr/testify/assert"
	// meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	// "k8s.io/client-go/kubernetes"
	// "k8s.io/client-go/tools/clientcmd"
)

func init() {
	log.SetOutput(ioutil.Discard)
}

func TestIntegration(t *testing.T) {
	cleanupTests := []testMap{
		// {"DeleteWorkflowDefinition": testDeleteWorkflowDefinition},
		// {"DeleteErrorWorkflowDefinition": testDeleteErrorWorkflowDefinition},
		// {"DeleteAdminWorkflowDefinition": testDeleteAdminWorkflowDefinition},
	}

	allTests := []testMap{}
	allTests = append(allTests, tests00User...)
	// allTests = append(allTests, tests01Cluster...)
	// allTests = append(allTests, tests02WorkflowDefinition...)
	// allTests = append(allTests, tests03Build...)
	// allTests = append(allTests, tests04Run...)
	allTests = append(allTests, tests05Workspace...)
	allTests = append(allTests, tests06Template...)
	allTests = append(allTests, cleanupTests...)

	for _, test := range allTests {
		for name := range test {
			t.Run(name, test[name])
			time.Sleep(50 * time.Millisecond)
		}
		time.Sleep(100 * time.Millisecond)
	}

	// Delete namespace
	// kubeconfig, err := base64.StdEncoding.DecodeString(generatedCluster.Config)
	// assert.NoError(t, err)
	// config, err := clientcmd.RESTConfigFromKubeConfig(kubeconfig)
	// assert.NoError(t, err)
	// clientset, err := kubernetes.NewForConfig(config)
	// assert.NoError(t, err)
	// err = clientset.CoreV1().Namespaces().Delete(context.TODO(), "cacao-user", meta.DeleteOptions{})
	// assert.NoError(t, err)
}
