package test

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"testing"

	"gitlab.com/cyverse/cacao/common"

	"github.com/stretchr/testify/assert"
)

var secretID string

var tests00User = []testMap{
	{"LoginAsAdmin": testLoginAsAdmin},
	{"CreateTestUser": testCreateTestUser},
	{"GetUser": testGetUser},
	{"LoginAsUser": testLoginAsUser},
	{"GetUserPermissionDenied": testGetUserPermissionDenied},
	{"GetUserNotFound": testGetUserNotFound},
	{"GetNonExistentUserPermissionDenied": testGetNonExistentUserPermissionDenied},
	{"GetAllUsersAdmin": testGetAllUsersAdmin},
	{"GetAllUsersRegular": testGetAllUsersRegular},
	{"DeleteUserPermissionDenied": testDeleteUserPermissionDenied},
	{"DeleteTestUser": testDeleteTestUser},
}

func testLoginAsAdmin(t *testing.T) {
	data := `username=cacao-admin&password=cacao-admin-password`
	req, err := newRequest("POST", "/user/login", data, "")
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	if err != nil {
		// Hack to prevent other tests from running if API is unavailable.
		log.Fatalf("Unable to reach Cacao API: %s\n", err)
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result struct {
		IDToken string `json:"id_token"`
	}
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Greater(t, len(result.IDToken), 0)
	adminToken = result.IDToken
}

func testCreateTestUser(t *testing.T) {
	data := `{"username":"cacao-test-user","first_name":"","last_name":"","primary_email":"","created_at":"","updated_at":"","updated_by":"","is_admin":false}

}`
	req, err := newRequest("POST", "/users", data, adminToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, resp.StatusCode)
}

func testLoginAsUser(t *testing.T) {
	data := `username=cacao-user&password=cacao-user-password`
	req, err := newRequest("POST", "/user/login", data, "")
	assert.NoError(t, err)
	resp, _ := (&http.Client{}).Do(req)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	var result struct {
		IDToken string `json:"id_token"`
	}
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Greater(t, len(result.IDToken), 0)
	userToken = result.IDToken
}

func testGetUser(t *testing.T) {
	var user common.User
	var err error
	//Generating the cluster config takes time.  Give it a few tries if it's
	//not there immediately.
	req, err := newRequest("GET", "/users/cacao-user", "", adminToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	_ = json.Unmarshal(respBody, &user)
	assert.NoError(t, err)
	assert.Equal(t, "cacao-user", user.Username)
}

func testGetUserPermissionDenied(t *testing.T) {
	req, err := newRequest("GET", "/users/cacao-admin", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusForbidden, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Contains(t, string(respBody), "unable to get user: non-admin user 'cacao-user' cannot get other user 'cacao-admin'")
}

func testGetUserNotFound(t *testing.T) {
	req, err := newRequest("GET", "/users/nonexistent-user", "", adminToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusNotFound, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Contains(t, string(respBody), "user nonexistent-user not found")
}

func testGetNonExistentUserPermissionDenied(t *testing.T) {
	req, err := newRequest("GET", "/users/nonexistent-user", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusForbidden, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Contains(t, string(respBody), "non-admin user 'cacao-user' cannot get other user 'nonexistent-user'")
}

func testGetAllUsersAdmin(t *testing.T) {
	req, err := newRequest("GET", "/users", "", adminToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusNotImplemented, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.Contains(t, string(respBody), "GET /users not yet implemented")
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
}

func testGetAllUsersRegular(t *testing.T) {
	req, err := newRequest("GET", "/users", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusNotImplemented, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
	assert.Contains(t, string(respBody), "GET /users not yet implemented")
}

func testDeleteUserPermissionDenied(t *testing.T) {
	req, err := newRequest("DELETE", "/users/cacao-user", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusUnauthorized, resp.StatusCode)
}

func testDeleteTestUser(t *testing.T) {
	req, err := newRequest("DELETE", "/users/cacao-test-user", "", adminToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
}
