package test

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
)

var adminToken, userToken string

type testMap map[string]func(t *testing.T)

func newRequest(verb, uri, body, token string) (*http.Request, error) {
	address := os.Getenv("CACAO_TEST_API_ADDRESS")
	req, err := http.NewRequest(verb, address+uri, nil)
	if err != nil {
		return req, err
	}
	req.Header.Add("Authorization", token)
	if len(body) > 0 {
		req.Body = ioutil.NopCloser(bytes.NewReader([]byte(body)))
	}
	return req, nil
}
