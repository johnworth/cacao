package test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

var templateID cacao_common.ID
var templateTypeName string

var tests06Template = []testMap{
	{"ListTemplateSourceTypes": testListTemplateSourceTypes},
	{"CreateTemplateType": testCreateTemplateType},
	{"GetTemplateType": testGetTemplateType},
	{"ListTemplateTypes": testListTemplateTypes},
	{"UpdateTemplateType": testUpdateTemplateType},
	{"PatchTemplateType": testPatchTemplateType},
	{"DeleteTemplateType": testDeleteTemplateType},

	{"ImportTemplate": testImportTemplate},
	{"ListTemplates": testListTemplates},
	{"GetTemplate": testGetTemplate},
	{"UpdateTemplate": testUpdateTemplate},
	{"PatchTemplate": testPatchTemplate},
	{"SyncTemplate": testSyncTemplate},
	{"DeleteTemplate": testDeleteTemplate},
}

func testListTemplateSourceTypes(t *testing.T) {
	req, err := newRequest("GET", "/templates/sourcetypes", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result []cacao_common_service.TemplateSourceType
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.GreaterOrEqual(t, len(result), 1)
}

func testCreateTemplateType(t *testing.T) {
	templateTypeName = cacao_common.NewID("templatetype").String()

	data := fmt.Sprintf(`{"name":"%s","formats":["yaml","json"],"engine":"terraform","provider_types":["openstack","kubernetes"]}`, templateTypeName)

	req, err := newRequest("POST", "/templates/types", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
}

func testGetTemplateType(t *testing.T) {
	req, err := newRequest("GET", "/templates/types/"+templateTypeName, "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result cacao_common_http.TemplateType
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, templateTypeName, result.Name)
}

func testListTemplateTypes(t *testing.T) {
	req, err := newRequest("GET", "/templates/types", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result []cacao_common_http.TemplateType
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.GreaterOrEqual(t, len(result), 1)
}

func testUpdateTemplateType(t *testing.T) {
	// update
	data := `{"formats":["yaml"],"engine":"terraform","provider_types":["openstack"]}`

	reqUpdate, err := newRequest("PUT", "/templates/types/"+templateTypeName, data, userToken)
	assert.NoError(t, err)
	respUpdate, err := (&http.Client{}).Do(reqUpdate)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, respUpdate.StatusCode)

	// get
	reqGet, err := newRequest("GET", "/templates/types/"+templateTypeName, "", userToken)
	assert.NoError(t, err)
	respGet, err := (&http.Client{}).Do(reqGet)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, respGet.StatusCode)
	respBody, err := ioutil.ReadAll(respGet.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result cacao_common_http.TemplateType
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, templateTypeName, result.Name)
	assert.ElementsMatch(t, []cacao_common_service.TemplateFormat{"yaml"}, result.Formats)
	assert.ElementsMatch(t, []cacao_common_service.TemplateProviderType{"openstack"}, result.ProviderTypes)
}

func testPatchTemplateType(t *testing.T) {
	// patch
	data := `{"provider_types":["openstack", "aws"]}`

	reqUpdate, err := newRequest("PATCH", "/templates/types/"+templateTypeName, data, userToken)
	assert.NoError(t, err)
	respUpdate, err := (&http.Client{}).Do(reqUpdate)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, respUpdate.StatusCode)

	// get
	reqGet, err := newRequest("GET", "/templates/types/"+templateTypeName, "", userToken)
	assert.NoError(t, err)
	respGet, err := (&http.Client{}).Do(reqGet)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, respGet.StatusCode)
	respBody, err := ioutil.ReadAll(respGet.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result cacao_common_http.TemplateType
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, templateTypeName, result.Name)
	assert.ElementsMatch(t, []cacao_common_service.TemplateProviderType{"openstack", "aws"}, result.ProviderTypes)
}

func testDeleteTemplateType(t *testing.T) {
	req, err := newRequest("DELETE", "/templates/types/"+templateTypeName, "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
}

func testImportTemplate(t *testing.T) {
	templateID = cacao_common_service.NewTemplateID()

	sourceData := `{"type":"git","uri":"https://github.com/cyverse/tf-openstack-single-image","access_parameters":{"branch":"master"},"source_visibility":"public"}`
	data := fmt.Sprintf(`{"id":"%s","name":"test_template_name","description":"test_template_description","public":false,"source":%s}`, templateID.String(), sourceData)

	req, err := newRequest("POST", "/templates", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
}

func testGetTemplate(t *testing.T) {
	req, err := newRequest("GET", "/templates/"+templateID.String(), "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result cacao_common_http.Template
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, templateID, result.ID)
}

func testListTemplates(t *testing.T) {
	req, err := newRequest("GET", "/templates", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result []cacao_common_http.Template
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.GreaterOrEqual(t, len(result), 1)
}

func testUpdateTemplate(t *testing.T) {
	// update
	sourceData := `{"type":"git","uri":"https://github.com/cyverse/tf-openstack-single-image","access_parameters":{"branch":"master"},"source_visibility":"public"}`
	data := fmt.Sprintf(`{"id":"%s","name":"test_template_name_updated","description":"test_template_description_updated","public":true,"source":%s}`, templateID.String(), sourceData)

	reqUpdate, err := newRequest("PUT", "/templates/"+templateID.String(), data, userToken)
	assert.NoError(t, err)
	respUpdate, err := (&http.Client{}).Do(reqUpdate)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, respUpdate.StatusCode)

	// get
	reqGet, err := newRequest("GET", "/templates/"+templateID.String(), "", userToken)
	assert.NoError(t, err)
	respGet, err := (&http.Client{}).Do(reqGet)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, respGet.StatusCode)
	respBody, err := ioutil.ReadAll(respGet.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result cacao_common_http.Template
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, templateID, result.ID)
	assert.Equal(t, "test_template_name_updated", result.Name)
	assert.Equal(t, "test_template_description_updated", result.Description)
	assert.Equal(t, true, result.Public)
}

func testPatchTemplate(t *testing.T) {
	// patch
	data := `{"description":"test_template_description_patched"}`

	reqUpdate, err := newRequest("PATCH", "/templates/"+templateID.String(), data, userToken)
	assert.NoError(t, err)
	respUpdate, err := (&http.Client{}).Do(reqUpdate)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, respUpdate.StatusCode)

	// get
	reqGet, err := newRequest("GET", "/templates/"+templateID.String(), "", userToken)
	assert.NoError(t, err)
	respGet, err := (&http.Client{}).Do(reqGet)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, respGet.StatusCode)
	respBody, err := ioutil.ReadAll(respGet.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result cacao_common_http.Template
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, templateID, result.ID)
	assert.Equal(t, "test_template_description_patched", result.Description)
}

func testSyncTemplate(t *testing.T) {
	// patch for sync
	data := `{"sync":true}`

	reqUpdate, err := newRequest("PATCH", "/templates/"+templateID.String(), data, userToken)
	assert.NoError(t, err)
	respUpdate, err := (&http.Client{}).Do(reqUpdate)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, respUpdate.StatusCode)

	// get
	reqGet, err := newRequest("GET", "/templates/"+templateID.String(), "", userToken)
	assert.NoError(t, err)
	respGet, err := (&http.Client{}).Do(reqGet)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, respGet.StatusCode)
	respBody, err := ioutil.ReadAll(respGet.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result cacao_common_http.Template
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, templateID, result.ID)
}

func testDeleteTemplate(t *testing.T) {
	req, err := newRequest("DELETE", "/templates/"+templateID.String(), "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
}
