#!/bin/bash
services=( "pleo" "api_service" "build_service" "workflow_definition_service" "phylax" )
for i in "${services[@]}"
do
    echo "Pruning images for skaffold/$i..."
    docker images | grep skaffold/$i | tr -s ' ' | cut -d ' ' -f 2 | xargs -I {} docker rmi skaffold/$i:{}
done
