#!/bin/bash

# usage: render_manifests.sh
# if IMAGE_RELEASE environment variable is defined, it will use that as the tag
# if IMAGE_REGISTRY environment variable is defined, it will use that as the registry

registry=${IMAGE_REGISTRY:-"registry.gitlab.com/cyverse/cacao"}
cur_branch=$(git branch --show-current)
#base_manifest_path=$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )
manifest_path=$(dirname $0)"/../kubernetes-manifests"

# these are the services that we need to create k8s manifests under this repo
# there is an assumption that the service yaml and subdirectory is named the same
local_modules=("api" "deployment" "workflow-event-aggregator" "workspace" )
remote_modules=("users" "wiretap" "ion" "creds")
echo "This script will render kubernetes manifests, useful for production or release environments"

if [ -z "$IMAGE_RELEASE" ]; then
    if [ -z "$cur_branch" ]; then
        IMAGE_RELEASE="latest"
        echo "fyi - IMAGE_RELEASE was not found, using 'latest' tag to render"
    else
        IMAGE_RELEASE=$cur_branch
        echo "Tagging manifests with branch: $cur_branch"
    fi
fi

for module in ${local_modules[@]}; do
    echo "rendering local $module"
    #skaffold render --default-repo $registry --module $module --output "${manifest_path}/${module}.yaml" --digest-source=tag
    skaffold render --default-repo $registry -p prod --module $module --output "${manifest_path}/${module}.yaml" --digest-source=tag
done

for module in ${remote_modules[@]}; do
    echo "copying $module.yaml to $manifest_path"
    cp ${manifest_path}/skaffold/${module}.yaml ${manifest_path}
done