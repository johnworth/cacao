#!/bin/bash

echo 
echo "This script should only clear out services launched from /opt/cacao and reclaim docker space, but not delete persistent data, if any."
echo "Do not run this on a production system, unless you want to reinstall."
echo 

while true; do
  read -p "Do you wish to continue (yes, no)? " yn
  case "$yn" in
    "yes" ) break;;
    "no" ) exit;;
    * ) echo "Please answer 'yes' or 'no'.";;
  esac
done

echo
echo $(/usr/bin/date)" Started"

cd /opt/cacao
skaffold delete
kubectl delete -f install/configs
helm uninstall ingress-nginx
k3d cluster delete service-cluster
k3d cluster delete user-cluster

docker stop registry.localhost
if [ $? -eq 0 ]; then
  docker rm registry.localhost
fi
docker system prune -f -a

echo $(/usr/bin/date)" Done"
echo
echo "If you are using install/deploy.sh, you can choose to to keep your existing install/config.yml and install/config.local.yml."
echo "OR, if you want do to a fresh redeploy, say for a local environemnt, you can do the following:"
echo
echo -e "\tcd /opt/cacao/install; rm -f config.yml config.local.yml; ./deploy.sh local\n\n"
