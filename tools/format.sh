#!/bin/bash

#Exit upon failure
set -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"


for SERVICE in $(ls "$SCRIPT_DIR/.." -1d -- */ | grep -Ev "(docs|tools|base|install|internal)")
do
    SERVICE_DIR="$SCRIPT_DIR/../$SERVICE"
    gofmt -s -l -d $SERVICE_DIR | tee /tmp/format_output.txt
    test $(cat /tmp/format_output.txt | wc -l) -eq 0
done
