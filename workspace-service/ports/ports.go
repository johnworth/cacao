package ports

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

// Port is a special type of port that will has an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly waitgroups)
type Port interface {
	Init(config *types.Config)
	Finalize()
}

// AsyncPort ...
type AsyncPort interface {
	Port
	InitChannel(channel chan types.WorkspaceChannelRequest)
	Start()
}

// IncomingQueryPort is an interface for a query port.
type IncomingQueryPort interface {
	AsyncPort
	List(actor string, emulator string) ([]types.Workspace, error)
	Get(actor string, emulator string, worksapceID cacao_common.ID) (types.Workspace, error)
}

// IncomingEventPort is an interface for an event port.
type IncomingEventPort interface {
	AsyncPort
	Create(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error
	Update(actor string, emulator string, workspace types.Workspace, updateFieldNames []string, transactionID cacao_common.TransactionID) error
	Delete(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error
}

// OutgoingEventPort is an outgoing event port
type OutgoingEventPort interface {
	Port
	Created(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error
	CreateFailed(actor string, emulator string, workspace types.Workspace, creationError error, transactionID cacao_common.TransactionID) error
	Updated(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error
	UpdateFailed(actor string, emulator string, workspace types.Workspace, updateError error, transactionID cacao_common.TransactionID) error
	Deleted(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error
	DeleteFailed(actor string, emulator string, workspace types.Workspace, deletionError error, transactionID cacao_common.TransactionID) error
}

// PersistentStoragePort is an interface for an driven port for Perstistent Storage.
type PersistentStoragePort interface {
	Port
	List(user string) ([]types.Workspace, error)
	Get(user string, workspaceID cacao_common.ID) (types.Workspace, error)
	Create(workspace types.Workspace) error
	Update(workspace types.Workspace, updateFieldNames []string) error
	Delete(user string, workspaceID cacao_common.ID) error
}
