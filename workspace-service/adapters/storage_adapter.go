package adapters

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

// MongoAdapter implements PersistentStoragePort
type MongoAdapter struct {
	Config *types.Config
	Store  cacao_common_db.ObjectStore
}

// Init initialize mongodb adapter
func (adapter *MongoAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.Init",
	})

	logger.Info("initializing MongoAdapter")

	adapter.Config = config

	store, err := cacao_common_db.CreateMongoDBObjectStore(&config.MongoDBConfig)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to MongoDB")
	}

	adapter.Store = store
}

// InitMock initialize mongodb adapter with mock_objectstore
func (adapter *MongoAdapter) InitMock(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.InitMock",
	})

	logger.Info("initializing MongoAdapter")

	adapter.Config = config

	store, err := cacao_common_db.CreateMockObjectStore()
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to MongoDB")
	}

	adapter.Store = store
}

// GetMock returns Mock
func (adapter *MongoAdapter) GetMock() *mock.Mock {
	if mockObjectStore, ok := adapter.Store.(*cacao_common_db.MockObjectStore); ok {
		return mockObjectStore.Mock
	}
	return nil
}

// Finalize finalizes mongodb adapter
func (adapter *MongoAdapter) Finalize() {
	err := adapter.Store.Release()
	if err != nil {
		log.Fatal(err)
	}

	adapter.Store = nil
}

// List returns workspaces owned by a user
func (adapter *MongoAdapter) List(user string) ([]types.Workspace, error) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.List",
	})

	results := []types.Workspace{}

	err := adapter.Store.ListForUser(adapter.Config.WorkspaceMongoDBCollectionName, user, &results)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to list workspaces for the user %s", user)
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return results, nil
}

// MockList sets expected results for List
func (adapter *MongoAdapter) MockList(user string, expectedWorkspaces []types.Workspace, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("ListForUser", adapter.Config.WorkspaceMongoDBCollectionName, user).Return(expectedWorkspaces, expectedError)
	return nil
}

// Get returns the workspace with the ID
func (adapter *MongoAdapter) Get(user string, workspaceID cacao_common.ID) (types.Workspace, error) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.Get",
	})

	result := types.Workspace{}

	err := adapter.Store.Get(adapter.Config.WorkspaceMongoDBCollectionName, workspaceID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the workspace for id %s", workspaceID)
		logger.WithError(err).Error(errorMessage)
		return result, cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != user {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the workspace for id %s", workspaceID)
		logger.Error(errorMessage)
		return result, cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	return result, nil
}

// MockGet sets expected results for Get
func (adapter *MongoAdapter) MockGet(user string, workspaceID cacao_common.ID, expectedWorkspace types.Workspace, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Get", adapter.Config.WorkspaceMongoDBCollectionName, workspaceID.String()).Return(expectedWorkspace, expectedError)
	return nil
}

// Create inserts a workspace
func (adapter *MongoAdapter) Create(workspace types.Workspace) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.Create",
	})

	err := adapter.Store.Insert(adapter.Config.WorkspaceMongoDBCollectionName, workspace)
	if err != nil {
		if cacao_common_db.IsDuplicateError(err) {
			errorMessage := fmt.Sprintf("unable to insert a workspace because id %s conflicts", workspace.ID)
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.NewCacaoAlreadyExistError(errorMessage)
		}

		errorMessage := fmt.Sprintf("unable to insert a workspace with id %s", workspace.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockCreate sets expected results for Create
func (adapter *MongoAdapter) MockCreate(workspace types.Workspace, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Insert", adapter.Config.WorkspaceMongoDBCollectionName).Return(expectedError)
	return nil
}

// Update updates/edits a workspace
func (adapter *MongoAdapter) Update(workspace types.Workspace, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.Update",
	})

	// get and check ownership
	result := types.Workspace{}

	err := adapter.Store.Get(adapter.Config.WorkspaceMongoDBCollectionName, workspace.ID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the workspace for id %s", workspace.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != workspace.Owner {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the workspace for id %s", workspace.ID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	// update
	updated, err := adapter.Store.Update(adapter.Config.WorkspaceMongoDBCollectionName, workspace.ID.String(), workspace, updateFieldNames)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to update the workspace for id %s", workspace.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !updated {
		errorMessage := fmt.Sprintf("unable to update the workspace for id %s", workspace.ID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockUpdate sets expected results for Update
func (adapter *MongoAdapter) MockUpdate(existingWorkspace types.Workspace, newWorkspace types.Workspace, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.Config.WorkspaceMongoDBCollectionName, newWorkspace.ID.String()).Return(existingWorkspace, expectedError)
	mock.On("Update", adapter.Config.WorkspaceMongoDBCollectionName, newWorkspace.ID.String()).Return(expectedResult, expectedError)
	return nil
}

// Delete deletes a workspace
func (adapter *MongoAdapter) Delete(user string, workspaceID cacao_common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.Delete",
	})

	// get and check ownership
	result := types.Workspace{}

	err := adapter.Store.Get(adapter.Config.WorkspaceMongoDBCollectionName, workspaceID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the workspace for id %s", workspaceID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != user {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the workspace for id %s", workspaceID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	// delete
	deleted, err := adapter.Store.Delete(adapter.Config.WorkspaceMongoDBCollectionName, workspaceID.String())
	if err != nil {
		errorMessage := fmt.Sprintf("unable to delete the workspace for id %s", workspaceID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !deleted {
		errorMessage := fmt.Sprintf("unable to delete the workspace for id %s", workspaceID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockDelete sets expected results for Delete
func (adapter *MongoAdapter) MockDelete(user string, workspaceID cacao_common.ID, existingWorkspace types.Workspace, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.Config.WorkspaceMongoDBCollectionName, workspaceID.String()).Return(existingWorkspace, expectedError)
	mock.On("Delete", adapter.Config.WorkspaceMongoDBCollectionName, workspaceID.String()).Return(expectedResult, expectedError)
	return nil
}
