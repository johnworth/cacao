package adapters

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/workspace-service/ports"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

func createTestQueryPort() *ports.MockIncomingQueryPort {
	var config types.Config
	config.ProcessDefaults()

	queryInImpl := &ports.MockIncomingQueryPort{}
	queryInImpl.Init(&config)

	return queryInImpl
}

func createTestQueryAdapter(queryInImpl ports.IncomingQueryPort) *QueryAdapter {
	var config types.Config
	config.ProcessDefaults()

	queryAdapter := &QueryAdapter{}
	queryAdapter.Init(&config)

	queryAdapter.IncomingPort = queryInImpl

	go queryAdapter.StartMock()

	time.Sleep(100 * time.Millisecond)
	return queryAdapter
}

func TestInitQueryAdapter(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)
	assert.NotNil(t, queryAdapter)
	assert.NotEmpty(t, queryAdapter.Connection)

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestWorkspaceListQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"

	queryData := cacao_common_service.Session{
		SessionActor:    testUser,
		SessionEmulator: testUser,
	}

	testTime := time.Now().UTC()

	expectedResults := []types.Workspace{
		{
			ID:                "0001",
			Owner:             "test_user1",
			Name:              "test_workspace1",
			Description:       "test_description1",
			DefaultProviderID: "test_default_provider1",
			CreatedAt:         testTime,
			UpdatedAt:         testTime,
		},
		{
			ID:                "0002",
			Owner:             "test_user1",
			Name:              "test_workspace2",
			Description:       "test_description2",
			DefaultProviderID: "test_default_provider2",
			CreatedAt:         testTime,
			UpdatedAt:         testTime,
		},
	}

	queryInImpl.SetListHandler(func(actor string, emulator string) ([]types.Workspace, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		return expectedResults, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.WorkspaceListQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.WorkspaceListModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result)
	assert.NoError(t, result.GetServiceError())
	assert.EqualValues(t, len(expectedResults), len(result.GetWorkspaces()))

	firstWorkspace := result.GetWorkspaces()[0]
	assert.EqualValues(t, expectedResults[0].ID, firstWorkspace.GetID())
	assert.EqualValues(t, expectedResults[0].Name, firstWorkspace.GetName())
	assert.EqualValues(t, expectedResults[0].Description, firstWorkspace.GetDescription())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestWorkspaceListQueryFailed(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"

	queryData := cacao_common_service.Session{
		SessionActor:    testUser,
		SessionEmulator: testUser,
	}

	queryInImpl.SetListHandler(func(actor string, emulator string) ([]types.Workspace, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		return nil, fmt.Errorf("unable to list workspaces")
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.WorkspaceListQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.WorkspaceListModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.Workspaces)
	assert.Error(t, result.GetServiceError())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestWorkspaceGetQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	queryData := cacao_common_service.WorkspaceModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID: testWorkspaceID,
	}

	testTime := time.Now().UTC()

	expectedResult := types.Workspace{
		ID:                testWorkspaceID,
		Owner:             testUser,
		Name:              "test_workspace1",
		Description:       "test_description1",
		DefaultProviderID: "test_default_provider1",
		CreatedAt:         testTime,
		UpdatedAt:         testTime,
	}

	queryInImpl.SetGetHandler(func(actor string, emulator string, workspaceID cacao_common.ID) (types.Workspace, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testWorkspaceID, workspaceID)
		return expectedResult, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.WorkspaceGetQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.WorkspaceModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result.ID)
	assert.NoError(t, result.GetServiceError())

	assert.Equal(t, expectedResult.ID, result.ID)
	assert.Equal(t, expectedResult.Name, result.Name)
	assert.Equal(t, expectedResult.Description, result.Description)

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestWorkspaceGetQueryFailed(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	queryData := cacao_common_service.WorkspaceModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID: testWorkspaceID,
	}

	queryInImpl.SetGetHandler(func(actor string, emulator string, workspaceID cacao_common.ID) (types.Workspace, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testWorkspaceID, workspaceID)
		return types.Workspace{}, fmt.Errorf("unable to get a workspace")
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.WorkspaceGetQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.WorkspaceModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.ID)
	assert.Error(t, result.GetServiceError())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}
