package adapters

import (
	"encoding/json"
	"fmt"
	"sync"

	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/workspace-service/ports"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

// EventAdapter communicates to IncomingEventPort and implements OutgoingEventPort
type EventAdapter struct {
	Config       *types.Config
	IncomingPort ports.IncomingEventPort
	// internal
	Connection     cacao_common_messaging.StreamingEventService
	EventWaitGroup sync.WaitGroup
}

// Init initializes the adapter
func (adapter *EventAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.Init",
	})

	logger.Info("initializing EventAdapter")

	adapter.Config = config

	// Use a WaitGroup to wait for a message to arrive
	adapter.EventWaitGroup = sync.WaitGroup{}
	adapter.EventWaitGroup.Add(1)
}

// Finalize finalizes the adapter
func (adapter *EventAdapter) Finalize() {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.Finalize",
	})

	logger.Info("finalizing EventAdapter")

	adapter.EventWaitGroup.Done()
}

func (adapter *EventAdapter) getEventHandlerMapping() []cacao_common_messaging.StreamingEventHandlerMapping {
	return []cacao_common_messaging.StreamingEventHandlerMapping{
		{
			Subject:      cacao_common_service.WorkspaceCreateRequestedEvent,
			EventHandler: adapter.handleWorkspaceCreateRequest,
		},
		{
			Subject:      cacao_common_service.WorkspaceDeleteRequestedEvent,
			EventHandler: adapter.handleWorkspaceDeleteRequest,
		},
		{
			Subject:      cacao_common_service.WorkspaceUpdateRequestedEvent,
			EventHandler: adapter.handleWorkspaceUpdateRequest,
		},
		{
			Subject:      cacao_common.EventType(""),
			EventHandler: adapter.handleDefaultEvent,
		},
	}
}

// Start starts the adapter
func (adapter *EventAdapter) Start() {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.Start",
	})

	logger.Info("starting EventAdapter")

	natsConfig := adapter.Config.NatsConfig
	natsConfig.ClientID = natsConfig.ClientID + "-event" + xid.New().String()

	eventHandlerMappings := adapter.getEventHandlerMapping()

	stanConn, err := cacao_common_messaging.ConnectStanForService(&natsConfig, &adapter.Config.StanConfig, eventHandlerMappings)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS Streaming")
	}

	adapter.Connection = stanConn

	defer stanConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

// StartMock starts the adapter
func (adapter *EventAdapter) StartMock() {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.StartMock",
	})

	logger.Info("starting EventAdapter")

	eventHandlerMappings := adapter.getEventHandlerMapping()

	stanConn, err := cacao_common_messaging.CreateMockStanConnection(&adapter.Config.NatsConfig, &adapter.Config.StanConfig, eventHandlerMappings)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS Streaming")
	}

	adapter.Connection = stanConn

	defer stanConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

func (adapter *EventAdapter) handleWorkspaceCreateRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.handleWorkspaceCreateRequest",
	})

	var createRequest cacao_common_service.WorkspaceModel
	err := json.Unmarshal(jsonData, &createRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into WorkspaceModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	workspace := types.ConvertFromModel(createRequest)

	err = adapter.IncomingPort.Create(createRequest.GetSessionActor(), createRequest.GetSessionEmulator(), workspace, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleWorkspaceDeleteRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.handleWorkspaceDeleteRequest",
	})

	var deleteRequest cacao_common_service.WorkspaceModel
	err := json.Unmarshal(jsonData, &deleteRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into WorkspaceModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	workspace := types.ConvertFromModel(deleteRequest)

	err = adapter.IncomingPort.Delete(deleteRequest.GetSessionActor(), deleteRequest.GetSessionEmulator(), workspace, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleWorkspaceUpdateRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.handleWorkspaceUpdateRequest",
	})

	var updateRequest cacao_common_service.WorkspaceModel
	err := json.Unmarshal(jsonData, &updateRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into WorkspaceModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	workspace := types.ConvertFromModel(updateRequest)

	if len(updateRequest.UpdateFieldNames) == 0 {
		// update all fields
		updateRequest.UpdateFieldNames = append(updateRequest.UpdateFieldNames, "name", "description", "default_provider_id")
	}

	err = adapter.IncomingPort.Update(updateRequest.GetSessionActor(), updateRequest.GetSessionEmulator(), workspace, updateRequest.UpdateFieldNames, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

// Implement OutgoingEventPort

// Created ...
func (adapter *EventAdapter) Created(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.Created",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, workspace)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.WorkspaceCreatedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.WorkspaceCreatedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.WorkspaceCreatedEvent)
	}

	return nil
}

// CreateFailed ...
func (adapter *EventAdapter) CreateFailed(actor string, emulator string, workspace types.Workspace, creationError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.CreateFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       creationError.Error(),
		ErrorMessage:    creationError.Error(),
	}

	if cerr, ok := creationError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(creationError.Error()).GetBase()
	}

	model := types.ConvertToModel(session, workspace)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.WorkspaceCreateFailedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.WorkspaceCreateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.WorkspaceCreateFailedEvent)
	}

	return nil
}

// Updated ...
func (adapter *EventAdapter) Updated(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.Updated",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, workspace)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.WorkspaceUpdatedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.WorkspaceUpdatedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.WorkspaceUpdatedEvent)
	}

	return nil
}

// UpdateFailed ...
func (adapter *EventAdapter) UpdateFailed(actor string, emulator string, workspace types.Workspace, updateError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.UpdateFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       updateError.Error(),
		ErrorMessage:    updateError.Error(),
	}

	if cerr, ok := updateError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(updateError.Error()).GetBase()
	}

	model := types.ConvertToModel(session, workspace)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.WorkspaceUpdateFailedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.WorkspaceUpdateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.WorkspaceUpdateFailedEvent)
	}

	return nil
}

// Deleted ...
func (adapter *EventAdapter) Deleted(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.Deleted",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, workspace)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.WorkspaceDeletedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.WorkspaceDeletedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.WorkspaceDeletedEvent)
	}

	return nil
}

// DeleteFailed ...
func (adapter *EventAdapter) DeleteFailed(actor string, emulator string, workspace types.Workspace, deletionError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.DeleteFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       deletionError.Error(),
		ErrorMessage:    deletionError.Error(),
	}

	if cerr, ok := deletionError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(deletionError.Error()).GetBase()
	}

	model := types.ConvertToModel(session, workspace)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.WorkspaceDeleteFailedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.WorkspaceDeleteFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.WorkspaceDeleteFailedEvent)
	}

	return nil
}

func (adapter *EventAdapter) handleDefaultEvent(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "EventAdapter.handleDefaultEvent",
	})

	logger.Tracef("received an unhandled event %s, TransactionID %s", subject, transactionID)
	return nil
}
