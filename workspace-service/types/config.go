package types

import (
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
)

// Config is the configuration settings, which can be used by the Domain object or Adapters
type Config struct {
	// NATS
	NatsConfig cacao_common_messaging.NatsConfig
	StanConfig cacao_common_messaging.StanConfig

	// MongoDB
	MongoDBConfig                  cacao_common_db.MongoDBConfig
	WorkspaceMongoDBCollectionName string
}

// ProcessDefaults will take a Config object and process the config object further, including
// populating any null values
func (c *Config) ProcessDefaults() {
	// NATS
	if c.NatsConfig.URL == "" {
		c.NatsConfig.URL = DefaultNatsURL
	}

	if c.NatsConfig.QueueGroup == "" {
		c.NatsConfig.QueueGroup = DefaultNatsQueueGroup
	}

	if c.NatsConfig.WildcardSubject == "" {
		c.NatsConfig.WildcardSubject = DefaultNatsWildcardSubject
	}

	if c.NatsConfig.ClientID == "" {
		c.NatsConfig.ClientID = DefaultNatsClientID
	}

	if c.NatsConfig.MaxReconnects <= 0 {
		c.NatsConfig.MaxReconnects = DefaultNatsMaxReconnect
	}

	if c.NatsConfig.ReconnectWait <= 0 {
		c.NatsConfig.ReconnectWait = DefaultNatsReconnectWait
	}

	if c.NatsConfig.RequestTimeout <= 0 {
		c.NatsConfig.RequestTimeout = DefaultNatsRequestTimeout
	}

	if c.StanConfig.ClusterID == "" {
		c.StanConfig.ClusterID = DefaultNatsClusterID
	}

	if c.StanConfig.DurableName == "" {
		c.StanConfig.DurableName = DefaultNatsDurableName
	}

	// MongoDB
	if c.MongoDBConfig.URL == "" {
		c.MongoDBConfig.URL = DefaultMongoDBURL
	}

	if c.MongoDBConfig.DBName == "" {
		c.MongoDBConfig.DBName = DefaultMongoDBName
	}

	if c.WorkspaceMongoDBCollectionName == "" {
		c.WorkspaceMongoDBCollectionName = DefaultWorkspaceMongoDBCollectionName
	}
}
