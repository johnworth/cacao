package types

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// ConvertFromModel converts WorkspaceModel to Workspace
func ConvertFromModel(model cacao_common_service.WorkspaceModel) Workspace {
	workspace := Workspace{
		ID:                model.ID,
		Owner:             model.Owner,
		Name:              model.Name,
		Description:       model.Description,
		DefaultProviderID: model.DefaultProviderID,
		CreatedAt:         model.CreatedAt,
		UpdatedAt:         model.UpdatedAt,
	}

	return workspace
}

// ConvertToModel converts Workspace to WorkspaceModel
func ConvertToModel(session cacao_common_service.Session, workspace Workspace) cacao_common_service.WorkspaceModel {
	return cacao_common_service.WorkspaceModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
			ErrorType:       session.ErrorType,
			ErrorMessage:    session.ErrorMessage,
			ServiceError:    session.ServiceError,
		},
		ID:                workspace.ID,
		Owner:             workspace.Owner,
		Name:              workspace.Name,
		Description:       workspace.Description,
		DefaultProviderID: workspace.DefaultProviderID,
		CreatedAt:         workspace.CreatedAt,
		UpdatedAt:         workspace.UpdatedAt,
	}
}

// ConvertToListItemModel converts Workspace to WorkspaceListItemModel
func ConvertToListItemModel(workspace Workspace) cacao_common_service.WorkspaceListItemModel {
	return cacao_common_service.WorkspaceListItemModel{
		ID:                workspace.ID,
		Owner:             workspace.Owner,
		Name:              workspace.Name,
		Description:       workspace.Description,
		DefaultProviderID: workspace.DefaultProviderID,
		CreatedAt:         workspace.CreatedAt,
		UpdatedAt:         workspace.UpdatedAt,
	}
}
