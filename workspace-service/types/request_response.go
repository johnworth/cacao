package types

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
)

// WorkspaceChannelRequest is a request struct used between adapters and domain
type WorkspaceChannelRequest struct {
	Actor            string
	Emulator         string
	Data             interface{}
	Operation        string
	UpdateFieldNames []string // list field names to update, this is used only when operation is Update
	TransactionID    cacao_common.TransactionID
	Response         chan WorkspaceChannelResponse // channel created by adapter(or other caller) and will receive the response
}

// WorkspaceChannelResponse is a response struct used between adapters and domain
type WorkspaceChannelResponse struct {
	Data  interface{}
	Error error
}
