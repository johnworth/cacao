package types

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
)

// Workspace is a struct for storing workspace information
type Workspace struct {
	ID                common.ID `bson:"_id" json:"id,omitempty"`
	Owner             string    `bson:"owner" json:"owner,omitempty"`
	Name              string    `bson:"name" json:"name,omitempty"`
	Description       string    `bson:"description" json:"description,omitempty"`
	DefaultProviderID common.ID `bson:"default_provider_id" json:"default_provider_id,omitempty"`
	CreatedAt         time.Time `bson:"created_at" json:"created_at,omitempty"`
	UpdatedAt         time.Time `bson:"updated_at" json:"updated_at,omitempty"`
}
