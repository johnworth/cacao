package domain

import (
	"fmt"
	"sync"

	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/workspace-service/ports"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

// Domain is the base struct for the domain service
type Domain struct {
	Config   *types.Config
	Storage  ports.PersistentStoragePort
	QueryIn  ports.IncomingQueryPort
	EventIn  ports.IncomingEventPort
	EventOut ports.OutgoingEventPort
}

// Init initializes all the specified adapters
func (d *Domain) Init(config *types.Config) {
	d.Config = config
}

// Finalize ...
func (d *Domain) Finalize() {
}

// Start will start the domain object, and in turn start all the async adapters
func (d *Domain) Start() {
	// using waitgroups to block termination gracefully
	var wg sync.WaitGroup

	// It is here that we need to handle asynchronous adapters
	// create channel for incoming queries, qchan
	wg.Add(1)
	qchannel := make(chan types.WorkspaceChannelRequest, types.DefaultChannelBufferSize)
	d.QueryIn.InitChannel(qchannel)
	go func() {
		defer wg.Done()
		d.QueryIn.Start()
	}()

	// start the domain's query worker
	wg.Add(1)
	go d.processQueryWorker(qchannel, &wg)

	wg.Add(1)
	echannel := make(chan types.WorkspaceChannelRequest, types.DefaultChannelBufferSize)
	d.EventIn.InitChannel(echannel)
	go func() {
		defer wg.Done()
		d.EventIn.Start()
	}()

	// start the domain's event worker
	wg.Add(1)
	go d.processEventWorker(echannel, &wg)

	wg.Wait()
}

func (d *Domain) processQueryWorker(qchannel chan types.WorkspaceChannelRequest, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.domain",
		"function": "domain.processQueryWorker",
	})

	defer wg.Done()

	for request := range qchannel {
		logger.Debugf("received a query - %s", request.Operation)

		workspaceRequest, ok := request.Data.(types.Workspace)
		if !ok {
			request.Response <- types.WorkspaceChannelResponse{
				Data:  nil,
				Error: cacao_common_service.NewCacaoMarshalError("unable to convert request data into workspace"),
			}
			return
		}

		switch request.Operation {
		case string(cacao_common_service.WorkspaceListQueryOp):
			workspaces, err := d.ListWorkspaces(request.Actor, request.Emulator)
			if request.Response != nil {
				request.Response <- types.WorkspaceChannelResponse{
					Data:  workspaces,
					Error: err,
				}
			}
		case string(cacao_common_service.WorkspaceGetQueryOp):
			workspace, err := d.GetWorkspace(request.Actor, request.Emulator, workspaceRequest.ID)
			if request.Response != nil {
				request.Response <- types.WorkspaceChannelResponse{
					Data:  workspace,
					Error: err,
				}
			}
		default:
			if request.Response != nil {
				errorMessage := fmt.Sprintf("unhandled query : %s", request.Operation)
				request.Response <- types.WorkspaceChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoNotImplementedError(errorMessage),
				}
			}
		}
	}
}

func (d *Domain) processEventWorker(echannel chan types.WorkspaceChannelRequest, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.domain",
		"function": "domain.processEventWorker",
	})

	defer wg.Done()

	for request := range echannel {
		logger.Debugf("received an event - %s", request.Operation)

		workspaceRequest, ok := request.Data.(types.Workspace)
		if !ok {
			request.Response <- types.WorkspaceChannelResponse{
				Data:  nil,
				Error: cacao_common_service.NewCacaoMarshalError("unable to convert request data into workspace"),
			}
			return
		}

		switch request.Operation {
		case string(cacao_common_service.WorkspaceCreateRequestedEvent):
			err := d.CreateWorkspace(request.Actor, request.Emulator, workspaceRequest, request.TransactionID)
			if request.Response != nil {
				request.Response <- types.WorkspaceChannelResponse{
					Data:  nil,
					Error: err,
				}
			}
		case string(cacao_common_service.WorkspaceUpdateRequestedEvent):
			err := d.UpdateWorkspace(request.Actor, request.Emulator, workspaceRequest, request.UpdateFieldNames, request.TransactionID)
			if request.Response != nil {
				request.Response <- types.WorkspaceChannelResponse{
					Data:  nil,
					Error: err,
				}
			}
		case string(cacao_common_service.WorkspaceDeleteRequestedEvent):
			err := d.DeleteWorkspace(request.Actor, request.Emulator, workspaceRequest.ID, request.TransactionID)
			if request.Response != nil {
				request.Response <- types.WorkspaceChannelResponse{
					Data:  nil,
					Error: err,
				}
			}
		default:
			if request.Response != nil {
				errorMessage := fmt.Sprintf("unhandled query : %s", request.Operation)
				request.Response <- types.WorkspaceChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoNotImplementedError(errorMessage),
				}
			}
		}
	}
}
