package types

import (
	cacaosvc "gitlab.com/cyverse/cacao-common/service"
)

// Event is a common interface for Events
type Event interface {
	EventType() EventType
}

// QueryOp is a type to represent query operations
type QueryOp string

// EventType is a type to represent event operations
type EventType string

// EventID is the ID of events, this is used to track trasaction
type EventID string

// Provider is the ID of cloud provider
type Provider cacaosvc.AWMProvider

func (p Provider) String() string {
	return string(p)
}

// EventErrorType is the error type that appear in the failure/error event emitted
type EventErrorType string

// EnvConfig is config from environment variables.
// Include config for NATS Streaming.
type EnvConfig struct {
	PodName    string `envconfig:"POD_NAME"`
	ConfigFile string `envconfig:"CONFIG_FILE" default:"config.yml"`
	LogLevel   string `envconfig:"LOG_LEVEL" default:"trace"`
}

// Config is config read from file.
type Config struct {
	Sink StanConfig            `yaml:"event_sink"`
	Srcs map[string]StanConfig `yaml:"event_srcs"`
}

// StanConfig is config for NATS Streaming
type StanConfig struct {
	URL        string `yaml:"url"`
	QueueGroup string `yaml:"queue"`
	Subject    string `yaml:"subject"`
	ClusterID  string `yaml:"cluster_id"`
}
