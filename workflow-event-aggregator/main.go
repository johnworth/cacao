package main

import (
	"io/ioutil"

	"github.com/kelseyhightower/envconfig"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/workflow-event-aggregator/adapters"
	"gitlab.com/cyverse/cacao/workflow-event-aggregator/domain"
	"gitlab.com/cyverse/cacao/workflow-event-aggregator/ports"
	"gitlab.com/cyverse/cacao/workflow-event-aggregator/types"
	"gopkg.in/yaml.v2"
)

var envConf types.EnvConfig
var aggregatorConf types.Config

func init() {
	err := envconfig.Process("", &envConf)
	if err != nil {
		log.Fatal(err)
	}
	if envConf.PodName == "" {
		envConf.PodName = "wea" + xid.New().String()
	}
	logLevel, err := log.ParseLevel(envConf.LogLevel)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(logLevel)

	aggregatorConf = loadConfigFromFile(envConf.ConfigFile)
}

func main() {
	svc := domain.NewDomain(createEventSink, createEventSrc)

	err := svc.Init(aggregatorConf)
	if err != nil {
		log.Fatal(err)
	}

	svc.Start()
}

func loadConfigFromFile(filename string) types.Config {
	var conf types.Config
	dat, err := ioutil.ReadFile(envConf.ConfigFile)
	if err != nil {
		log.Fatal(err)
	}

	err = yaml.Unmarshal(dat, &conf)
	if err != nil {
		log.Fatal(err)
	}

	log.Infof("load config from %s", filename)
	return conf
}

func createEventSink() ports.EventSink {
	return adapters.NewStanSink(envConf.PodName)
}

func createEventSrc(name string) ports.EventSrc {
	return adapters.NewStanSrc(name, envConf.PodName)
}
