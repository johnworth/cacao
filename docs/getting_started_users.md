# Getting Started

This document outlines getting started with CACAO as a user. This assumed you have an account and access to a running CACAO server and an OpenStack credential.


### Table of Contents
[[_TOC_]]


### Prepare CACAO CLI
The CACAO CLI is the simplest way to interact with the CACAO API.

In order to use the CACAO CLI, you will need to build it
1. Clone or download the repository from GitLab
	```bash
	git clone https://gitlab.com/cyverse/cacao.git
	# or download it without git:
	wget https://gitlab.com/cyverse/cacao/-/archive/master/cacao-master.zip
	unzip cacao.zip
	```
2. Build the `cacao` binary:
	```bash
	cd cacao/cmd
	go build -o cacao
	```
3. Move this to somewhere in your `$PATH`, or add this directory to your path:
	```bash
	sudo mv cacao /usr/local/bin/cacao
	# or
	export PATH=$PATH:$(pwd)
	```


### First Steps

In order to use the CACAO CLI, you need to log in:
```bash
$ cacao login
```

If this is your first time logging in, the CACAO CLI will prompt you for the address off the CACAO API.
```bash
Please provide address of CACAO API.
Format Should be: http://<ip address>:<port>      or    <ip address>:<port>
CACAO API address:
```

To access the production deployment of CACAO, enter `http://ca.cyverse.org`.  For local development environments, enter `ca.cyverse.local` (or `localhost` if you run services manually from cmd line).


### User Actions
Now you have everything you need to interact with your user's resources in CACAO, specifically the OpenStack resources.

```bash
cacao get user ${username}

# list all providers
cacao get provider
# get a specific provider
cacao get provider ${providerID}
# create a new provider
cacao create provider -name ${providerName} -type ${providerType} -url ${providerURL} -metadata ${metadataJSONString}
# create a new provider with metadata from file
cacao create provider -name ${providerName} -type ${providerType} -url ${providerURL} -metadata-file ${metadataJSONFilename}
# create a new provider from JSON file
cacao create provider -f provider.json
# update a provider
cacao update provider -name ${providerName} -type ${providerType} -url ${providerURL} -metadata ${metadataJSONString} ${providerID}
# delete a provider
cacao delete provider ${providerID}

# list all templates
cacao get template
# get a specific template
cacao get template ${templateID}
# create/import a new template
cacao create template -name ${templateName} -public true -source_type git -source_uri ${gitRepoURL} -source_branch ${gitBranch} -source_path ${subPath}
# create/import a new template from JSON file
cacao create template -f template.json
# sync a template with its source
cacao sync template ${templateID}
# delete a template
cacao delete template ${templateID}

# list all workspaces
cacao get workspace
# get a specific workspace
cacao get workspace ${workspaceID}
# create a new workspace
cacao create workspace -name ${workspaceName} -default_provider_id ${defaultProviderID}
# create a new workspace from JSON file
cacao create workspace -f workspace.json
# update a workspace
cacao update workspace -name ${workspaceName} -default_provider_id ${defaultProviderID} ${workspaceID}
# delete a workspace
cacao delete workspace ${workspaceID}

# list all credentials
cacao get credential
# get a specific credential
cacao get credential ${credentialID}
# create a new credential (example types: openstack, git, dockerhub, ssh, etc.)
cacao create credential -type ${type} -name ${credentialName} -value ${secret_string}
# create a new credential from JSON file
cacao create credential -f credential.json
# delete a credential
cacao delete credential ${credentialID}

# list all deployments
cacao get deployment
# get a specific deployment
cacao get deployment ${deploymentID}
# create a new deployment
cacao create deployment ${workspaceID} ${templateID} ${providerID} -cloud-cred ${credentialID}
# delete a deployment
cacao delete deployment ${deploymentID}

# list all runs in a deployment
cacao get run ${deploymentID}
# get a specific run
cacao get run ${deploymentID} ${runID}
# create a new run from JSON file
cacao create run ${deploymentID} -f run.json
```

### Create a prerequisite template
> Performed by Admin

First import a prerequiste template, use this template as an example https://gitlab.com/cyverse/cacao-tf-os-ops/-/blob/main/prerequisite/metadata.json .
Prerequiste template is a template that will be exectued before normal template (hence prerequisite), and it is specific to provider.
```bash
cacao create template -name openstack-prerequisite \
	-public true \
	-source_type git \
	-source_uri https://gitlab.com/cyverse/cacao-tf-os-ops.git \
	-source_branch main \
	-source_path prerequisite
```
Save the template ID generated from the output (e.g. `template-xxxxxxxxxxxxxxxxxxxx`)

### Create an OpenStack provider
> Performed by Admin

Create a metadata JSON file, use the one below as an example. Note that you need to populate the fields in the JSON object.

`provider_metadata.json`
```json
{
    "prerequisite_template": {
        "template_id": "<prerequisite-template-ID-from-previous-step>"
    },
    "public_ssh_key": "<your-public-ssh-key>",
    "external_network": "<openstack-external-network-uuid>"
}
```

Then, create the provider
```bash
cacao create provider -name my-provider -type openstack -url https://example.com -metadata-file provider_metadata.json
```

Save the provider ID generated from the output (e.g. `provider-xxxxxxxxxxxxxxxxxxxx`)

### Create a template
> Performed by Admin
Import a template. Use this single-image template as an example https://gitlab.com/cyverse/cacao-tf-os-ops/-/blob/main/single-image/metadata.json .
```bash
cacao create template -name openstack-single-image \
	-public true \
	-source_type git \
	-source_uri https://gitlab.com/cyverse/cacao-tf-os-ops.git \
	-source_branch main \
	-source_path single-image
```

Save the template ID generated from the output (e.g. `template-xxxxxxxxxxxxxxxxxxxx`)

### Create a workspace
Create a workspace with the default provider ID set to the one from previous step.
```bash
cacao create workspace -name my-workspace -default_provider_id ${providerID}
```

Save the workspace ID generated from the output (e.g. `workspace-xxxxxxxxxxxxxxxxxxxx`)

### Create a credential
Construct a JSON file that contains your openstack credential, use the example below as a guide. Note that the structure is very similar to the `.openrc` file the openstack generated.

`openstack_credential.json`
```json
{
  "OS_REGION_NAME": "os_region_name",
  "OS_PROJECT_DOMAIN_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "OS_INTERFACE": "os_interface",
  "OS_PROJECT_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "OS_USER_DOMAIN_NAME": "os_user_domain_name",
  "OS_PROJECT_NAME": "os_project_name",
  "OS_IDENTITY_API_VERSION": "3",
  "OS_AUTH_URL": "https://os-auth-url",
  "OS_AUTH_TYPE": "",
  "OS_USERNAME": "os_username",
  "OS_PASSWORD": "os_password"
}
```

`openstack_credential.json` with OpenStack application credential
```json
{
  "OS_REGION_NAME": "os_region_name",
  "OS_PROJECT_DOMAIN_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "OS_INTERFACE": "os_interface",
  "OS_PROJECT_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "OS_USER_DOMAIN_NAME": "os_user_domain_name",
  "OS_PROJECT_NAME": "os_project_name",
  "OS_IDENTITY_API_VERSION": "3",
  "OS_AUTH_URL": "https://os-auth-url",
  "OS_AUTH_TYPE": "v3applicationcredential",
  "OS_APPLICATION_CREDENTIAL_ID": "os_application_credential_id",
  "OS_APPLICATION_CREDENTIAL_SECRET": "os_application_credential_secret"
}
```

Create credential with the JSON file.
```bash
cacao create credential -type openstack -name ${providerID} -value-file openstack_credential.json
```

> In order to the images and flavor endpoints to work, you need to create a credential with the provider ID as the name of the credential. Also the credential value cannot be an application credential, and must be username&password.

> Note: you can create a credential for the images and flavor endpoints, and another credential for the deployment with a different name.

### Create a deployment
Create a deployment with the workspace, template, and provider from previous steps
```bash
cacao create deployment ${workspaceID} ${templateID} ${providerID} -cloud-cred ${credentialID}
```

> If you run the command from previous step, then the credentialID you should use is the providerID from eariler step. `${credentialID}` == `${providerID}`

Save the deployment ID generated from the output (e.g. `deployment-xxxxxxxxxxxxxxxxxxxx`)

### Run the deployment
First, create a JSON file for the request. 

`run.json`
```json
{
    "cloud_credentials": ["<credential-ID-from-above>"],
    "git_credential_id": "",
    "parameters": [
        {
            "key": "flavor",
            "value": "<openstack-instance-size>"
        },
        {
            "key": "image",
            "value": "<openstack-image-uuid>"
        },
        {
            "key": "instance_count",
            "value": "1"
        },
        {
            "key": "instance_name",
            "value": "my-cacao-instane"
        },
        {
            "key": "power_state",
            "value": "active"
        }
    ]
}

```

Run the deployment
```bash
cacao create run ${deploymentID} -f run.json
```
Save the run ID generated from the output (e.g. `run-xxxxxxxxxxxxxxxxxxxx`)

### Check status of the deployment & run

```bash
cacao get run ${deploymentID} ${runID} | jq -r .status
```
