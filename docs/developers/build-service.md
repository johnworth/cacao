# Build Service

The Build Service is responsible for building container images from a git repository and uploading to a container registry such as Dockerhub.

### Table of Contents
[[_TOC_]]


## Environment
| Variable name     | Required | Default                     | Description                                 |
| :---------------- | :------- | :-------------------------- | :------------------------------------------ |
| NATS_CLUSTER_ID   | yes      | `cacao-cluster`             | ID of the cluster to connect to             |
| NATS_CLIENT_ID    | yes      | `workflow-definition`       | ID of _this_ client                         |
| NATS_ADDRESS      | yes      | `nats://localhost:4222`     | address of the NATS Streaming server        |
| K8S_HOST          | yes      |                             | address for K8s cluster to run builds on    |
| K8S_CERTDATA      | yes      |                             | base64 encoded cert contents for K8s auth   |
| K8S_KEYDATA       | yes      |                             | base64 encoded key contents for K8s auth    |
| K8S_INSECURE      | yes      |                             | "true" if you want to skip SSL verification |


## Communication
This service will react to events/messages published to NATS and NATS Streaming.


### NATS Streaming
On NATS Streaming, this service subscribes to the `Build.Build` channel/subject in order to asynchronously start building container images.

If a request is received that does not have enough information about the WorkflowDefinition, it is re-published on the `WorkflowDefinition.Build` subject to be picked up by the WorkflowDefinition Service.

The WorkflowDefinition Service will then add any necessary info, and publish the request again. If a User is still required, the WorkflowDefinition Service will publish the request on the `User.Get` subject with an additional parameter specifying that the User Service should send it on to the `Build.Build` subject after adding User information.


### NATS
On NATS, it subscribes to the `Build.Status` channel/subject to synchronously respond to queries about a Build's status.


## Building Images

### [Kaniko]((https://github.com/GoogleContainerTools/kaniko))
The Build Service builds images using [Google's Kaniko project](https://github.com/GoogleContainerTools/kaniko). This tool was specifically created to build Docker container images inside of a container or Kubernetes cluster, so it is perfect for this use case.

### [Kubernetes Jobs](https://kubernetes.io/docs/concepts/workloads/controllers/job/)
When a user requests to build an image, CACAO will create a [Kubernetes Job](https://kubernetes.io/docs/concepts/workloads/controllers/job/) with the Kaniko image to handle this. The benefit of using a Job is that it can continue creating Pods until a specified number of them run to completion, which gives it an advantage over creating just a bare pod. Additionally, it will cleanup completed Pods. Jobs are great for running a computational task until it completes, such as performing some sort of methematical task or building container images. Kubernetes has future plans to implement some sort of [automatic Job cleanup](https://kubernetes.io/docs/concepts/workloads/controllers/job/#clean-up-finished-jobs-automatically) (or garbage collection) that will also be great for this use case.
