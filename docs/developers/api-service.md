# API Service

This service is used as an intermediary between the user (or web UI) and the rest of the microservices.

This service will interact with the other services with a mix of synchronous and asynchronous requests. Queries (often `GET` requests) will synchronously return information about an object. Commands will asynchronously trigger events in other services such as creating a WorkflowDefinition or starting a build.

### Table of Contents
[[_TOC_]]


## Environment

| Variable name               | Required | Default                                    | Description                                |
| :--------------------       | :------- | :----------------------------------------- | :----------------------------------------- |
| NATS_CLUSTER_ID             | no       | `cacao-cluster`                            | ID of the cluster to connect to            |
| NATS_CLIENT_ID              | yes      |                                            | ID of _this_ client                        |
| NATS_URL                    | no       | `nats://nats:4222`                         | address of the NATS Streaming server       |
| API_AUTH_METHOD             | no       | `simpletoken`                               | Authentication method. Use "keycloak" when using Keycloak. Defaults to simple token auth |
| API_LOG_LEVEL               | no       | `debug`                                    | debug level, either one of "debug", "trace", "info" |
| API_KEYCLOAK_URL            | no       | `http://keycloak:8080/auth/realms/cacao`   | URL to access Keycloak Realm               |
| API_KEYCLOAK_REDIRECT_URL   | no       | `http://api:8080/user/login/callback`      | CACAO API url to redirect to after login   |
| API_KEYCLOAK_CLIENT_ID      | no       | `cacao-client`                             | client id provided by Keycloak             |
| API_KEYCLOAK_CLIENT_SECRET  | yes      |                                            | Secret value provided by Keycloak          |
| API_KEYCLOAK_HMAC_SECRET    | no       | secret                                     | secret used to generate hmacs (default is not secure) |
| API_ENABLE_AUTO_CREATE_USER | no       | false                                      | after auth, if user does not exist, then create in user ms |
| API_SIMPLE_TOKEN_USERNAME   | no       |                                            | if simple token auth, then initialize with this user; note, this will not create user in users microservice unless API_ENABLE_AUTH_CREATE_USER = true as well |
| API_SIMPLE_TOKEN_TOKEN      | no       | cacaosimpletoken                           | if simple token auth, then initialize with this token; API_USERPASS_USERNAME must be set as well |
| API_SIMPLE_TOKEN_STANDALONE | no       | false                                      | if set to true (and the auth driver is set to "simpletoken"), then the simple token authentication driver will not require nats, stan, or users microservice to be running (though nats and stan may be needed by other parts of the rest api) |

## OpenAPI Specification
This API's specification can be found [here](../openapi/cacao-openapi.yaml).


## Usage
The best way to interact with the CACAO API is with the [`cacao` command](../../cmd/README.md)

Alternatively, you can use curl commands like this:
```bash
curl -X GET -H "Authorization: ${CACAO_TOKEN}" $CACAO_API/workflows/${workflowID}
```


## Developers
The API Service is developed using the [Gorilla `mux`](https://github.com/gorilla/mux) package. This package implements a request router for matching HTTP requests and dispatching them to the correct handler function. It has great compatibility with the builtin `net/http` package and uses a lot of structs and functions defined there, making it really intuitive and compatible with a lot of other things.


### Routing
The API Service code is organized with files for each top-level path in the route which often corresponds to the different services that it interacts with. These files will each export a function named something like `WorkflowDefinitionAPIRouter(router *mux.Router)` to handle routing the specific paths and handlers in that file. The `main` function of the API Service will call each of these functions to initialize the routes.

It is important to know that the order in which the routes are initialized is important. **The paths are matched in the order that they are defined.**


### Authentication Middleware
When calling the functions to initialize routes, the `main` method will pass a router to these functions that was created with the `AuthenticationMiddleware` setup.

A "middleware" is a function or handler that acts as an intermediate during routing. In our case, the `AuthenticationMiddleware` function is run for all requests before reaching the actual handlers to verify the user's token against Keycloak and then use it to authenticate with Vault and get a Vault Token. This middleware will also add more Headers to the request before forwarding it on to the actual handler. These headers provide the handler functions with information that was gleaned during the verification process such as username, admin status, and Vault token.


### Common Functions
There are a few common functions used by most handlers to process the incoming request, read the headers added by the middleware function, and read user information (clusters/secrets) from Vault.

Here are some of the important functions defined in `common.go`:
- `getWorkflowDefinitionData(request common.Request) (int, error)`
	- This function makes a request to the WorkflowDefinition Service and will parse the received information into the original `request` parameter
	- It returns an HTTP status code and error
- `prepareRequest(request common.Request, r *http.Request, workflowID string) (status int, err error)`
	- This function starts out by reading the `http.Request` body into our `common.Request` struct
	- Then, it will read and remove headers added by the middleware function to get the user's username, Vault token, and admin status
	- Finally, it will use the Vault token to read clusters and secrets from Vault if any secret or cluster IDs were included in the request
	- It returns an HTTP status code and error
- `checkAuthorization(request common.Request, r *http.Request, workflowID string) (int, error)`
	- This function just runs the previous two functions to first prepare the request and then get the WorkflowDefinition
	- This function was originally named this because it will fail if the user does not have access to the WorkflowDefinition, but it eventually evolved to do more than just that
	- It returns an HTTP status code and error
