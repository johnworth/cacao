# Debugging

### Debugging with Skaffold

Skaffold has a `debug` command that will automatically run a headless instance of [Delve](https://github.com/go-delve/delve), the
Go debugger, in each pod.

### Debugging a pod
0. [Install Delve](https://github.com/go-delve/delve/blob/master/Documentation/installation/linux/install.md) on your development machine.
1. Run `skaffold debug` to start Skaffold in debugging mode.
2. Set up port-fowarding to the pod on port using the `tools/debug_port_forward.sh` script:
```
$ tools/debug_port_forward.sh <CACAO SERVICE NAME>
```
`<CACAO SERVICE NAME>` should be the `name` property under the `metadata` section of the Deployment YAML file.
 For example, in the template for the API service YAML file (`install/roles/cacao/templates/dev-api-service.yaml.j2`), you'll see that the `name` property of the `metadata` is `api`:
 ```
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    service: api
  name: api
 ```
 So to set up port forwarding to the API service, you'd run:
```
$ tools/debug_port_forward.sh api
```
3. In another terminal window or tab, connect Delve to the remote debugging session:
```
$ dlv connect localhost:56268
Type 'help' for list of commands.
(dlv)
```

### Hints for using Delve

* You can specify a breakpoint using the package and function name and a relative line number.  For example, if you were connected to the `api` pod, you could use the command `b api.getUsers:8` to set a breakpoint on the eighth line of the `getUsers` function.

* Once you've hit a breakpoint, you can run the `locals` command to see local variables and the `args` command to see the arguments passed to the function.

* The way Skaffold configures `dlv`, it pauses the execution of the pods upon start up (see "Known issues" below).  You can work around this by commenting out the lines that set the `GOTRACEBACK` environment variables in the `Dockerfiles` of the services that you don't need `dlv` to be running on.


### Known issues

* Skaffold currently [has a bug](https://github.com/GoogleContainerTools/skaffold/issues/4863) which is causing the `SKAFFOLD_GO_GCFLAGS` build argument to be incorrectly parsed.  `SKAFFOLD_GO_GCFLAGS` should automatically populate the correct flags for `go build` to turn off optimizations when debugging.  Disabling these optimizations makes the program execution much easier for humans to follow.  The current work around is to use `eval` in the Dockerfile, so you'll see lines like:

```
RUN eval go build -gcflags="${SKAFFOLD_GO_GCFLAGS}"
```

* Skaffold automatically starts `dlv` in `continue` mode, which has the effect of suspending execution in the container.  This means that `main()` and `init()` in the `main` package won't execute until you connect to `dlv` and kill the headless debugging instance:

```
$ dlv connect localhost:56268
Type 'help' for list of commands.
(dlv) quit
Would you like to kill the headless instance? [Y/n] y
```

You may see error messages when trying to call the API like:

```
upstream connect error or disconnect/reset before headers. reset reason: connection failure
```

This is the result of the API service's `main()` and `init()` functions not executing, which results in there not being a process listening to respond to API requests.  Kill the headless `dlv` instance as described above and it should resolve the problem.

There is currently [an open issue](https://github.com/GoogleContainerTools/skaffold/issues/4870) to make this configurable.

* Since we're not currently copying the source code into the final image, the `list` command won't show you code while using `dlv`.  We will address this in the future.
