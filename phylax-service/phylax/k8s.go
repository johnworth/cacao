package phylax

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"gitlab.com/cyverse/cacao/common"

	batch "k8s.io/api/batch/v1"
	"k8s.io/api/core/v1"
	"k8s.io/client-go/tools/cache"
)

func createBuildInformer(request Request, natsInfo map[string]string) (err error) {
	log.Printf("Creating build informer for build %s\n", request.GetBuildID())
	cluster, ok := request.GetUser().Clusters[request.GetClusterID()]
	if !ok {
		err = fmt.Errorf("Error getting cluster config '%s'", request.GetClusterID())
		return
	}
	clientsets, err := common.NewUserClusterK8sClientsets(cluster.Config)
	if err != nil {
		return
	}
	stopper := make(chan struct{})
	handlers := cache.ResourceEventHandlerFuncs{
		UpdateFunc: func(oldObj, newObj interface{}) {
			newJob := newObj.(*batch.Job)
			for _, c := range newJob.Status.Conditions {
				if c.Status == v1.ConditionTrue {
					if c.Type == batch.JobComplete || c.Type == batch.JobFailed {
						build, err := db.GetBuild(request.GetBuildID(), request.GetUser().Username)
						if err != nil {
							log.Printf("Unable to retrieve build: %s\n", err)
						} else {
							updateBuild(request, build, natsInfo, c.Type)
						}
						close(stopper)
					}
					return
				}
			}
		},
	}
	// Set namespaceName if specified, otherwise use default for the Cluster
	namespaceName := request.Namespace
	if len(namespaceName) == 0 {
		namespaceName = cluster.DefaultNamespace
	}
	informer := clientsets.CreateInformer(namespaceName, handlers)
	go informer.Run(stopper)

	return
}

func updateBuild(request Request, build *Build, natsInfo map[string]string, condition batch.JobConditionType) error {
	build.EndDate = time.Now()
	if condition == batch.JobComplete {
		log.Printf("Build complete: %s\n", build.GetID())
		build.Condition = BuildComplete
		if request.RunAfterBuild == true {
			log.Printf("Build marked for immediate execution: %s\n", build.GetID())
			msg, err := json.Marshal(request)
			if err != nil {
				log.Println(err)
			}
			common.StreamingPublish(msg, natsInfo, "Pleo.Run", "Phylax")
		}
	} else if condition == batch.JobFailed {
		build.Condition = BuildFailed
		log.Printf("Build %s failed!\n", build.GetID())
	}
	return db.Save(build)
}
