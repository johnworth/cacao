package main

import (
	"sync"

	"gitlab.com/cyverse/cacao/provider-metadata-service/adapters"
	"gitlab.com/cyverse/cacao/provider-metadata-service/domain"
	"gitlab.com/cyverse/cacao/provider-metadata-service/types"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
)

func main() {
	var config types.Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.Fatal(err.Error())
	}

	// This is to overwrite default cluster id
	config.StanConfig.ClusterID = types.DefaultNatsClusterID
	// This is to overwrite default nats subject
	config.NatsConfig.WildcardSubject = types.DefaultNatsWildcardSubject

	// other empty parameters will be filled with defaults
	config.ProcessDefaults()

	// set the log level
	lvl, err := log.ParseLevel(config.LogLevel)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(lvl)

	// create an initialize Domain object
	var d domain.Domain
	d.Init(&config)

	// initialize query port
	queriesIn := &domain.QueryPortImpl{DomainImpl: &d}
	queriesIn.Init(&config)
	d.QueriesIn = queriesIn

	// initialize event port
	eventsIn := &domain.EventPortImpl{DomainImpl: &d}
	eventsIn.Init(&config)
	d.EventsIn = eventsIn

	// add and initialize the storage adapter
	storage := &adapters.MongoAdapter{}
	storage.Init(&config)
	d.Storage = storage

	// add and initialize the query adapter
	queryAdapter := &adapters.QueryAdapter{IncomingPort: queriesIn}
	queryAdapter.Init(&config)

	// add and initialize the event adapter
	eventAdapter := &adapters.EventAdapter{IncomingPort: eventsIn}
	eventAdapter.Init(&config)
	d.EventsOut = eventAdapter

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		d.Start()
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		queryAdapter.Start()
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		eventAdapter.Start()
	}()

	wg.Wait()

	eventAdapter.Finalize()
	queryAdapter.Finalize()

	eventsIn.Finalize()
	queriesIn.Finalize()

	d.Finalize()
}
