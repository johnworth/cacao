package domain

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-metadata-service/types"
)

// EventPortImpl implements IncomingEventPort
type EventPortImpl struct {
	Config     *types.Config
	Channel    chan types.ProviderRequest
	DomainImpl *Domain
}

// Init ...
func (impl *EventPortImpl) Init(config *types.Config) {
	impl.Config = config
}

// Finalize ...
func (impl *EventPortImpl) Finalize() {
}

// InitChannel ...
func (impl *EventPortImpl) InitChannel(channel chan types.ProviderRequest) {
	impl.Channel = channel
}

// Start ...
func (impl *EventPortImpl) Start() {

}

// Create creates a Provider
func (impl *EventPortImpl) Create(provider service.Provider, transactionID common.TransactionID) error {
	if len(provider.GetName()) == 0 {
		return fmt.Errorf("Create input validation error: Name is empty")
	}
	if len(provider.GetType()) == 0 {
		return fmt.Errorf("Create input validation error: Type is empty")
	}
	if len(provider.GetURL()) == 0 {
		return fmt.Errorf("Create input validation error: URL is empty")
	}

	now := time.Now().UTC()

	p := service.ProviderModel{
		ID:        common.NewID("provider"),
		Name:      provider.GetName(),
		URL:       provider.GetURL(),
		Type:      provider.GetType(),
		Metadata:  provider.GetMetadata(),
		CreatedAt: now,
		UpdatedAt: now,
	}

	err := impl.DomainImpl.Storage.Create(p)
	if err != nil {
		status := common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}
		impl.DomainImpl.EventsOut.CreateFailed(&p, status, transactionID)
		return err
	}

	status := common.HTTPStatus{
		Message: "created the provider successfully",
		Code:    http.StatusAccepted,
	}
	impl.DomainImpl.EventsOut.Created(&p, status, transactionID)
	return nil
}

// Update updates the Provider
func (impl *EventPortImpl) Update(provider service.Provider, updateFieldNames []string, transactionID common.TransactionID) error {
	if len(provider.GetID()) == 0 {
		return fmt.Errorf("Update input validation error: ProviderID is empty")
	}

	now := time.Now().UTC()

	p := service.ProviderModel{
		ID:        provider.GetID(),
		Name:      provider.GetName(),
		URL:       provider.GetURL(),
		Type:      provider.GetType(),
		Metadata:  provider.GetMetadata(),
		CreatedAt: provider.GetCreatedAt(),
		UpdatedAt: now,
	}
	updateFieldNames = append(updateFieldNames, "updated_at")

	err := impl.DomainImpl.Storage.Update(p, updateFieldNames)
	if err != nil {
		status := common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}
		impl.DomainImpl.EventsOut.UpdateFailed(&p, status, transactionID)
		return err
	}

	status := common.HTTPStatus{
		Message: "updated the provider successfully",
		Code:    http.StatusAccepted,
	}
	impl.DomainImpl.EventsOut.Updated(&p, status, transactionID)
	return nil
}

// Delete deletes the Provider
func (impl *EventPortImpl) Delete(id common.ID, transactionID common.TransactionID) error {
	if len(id) == 0 {
		return fmt.Errorf("Delete input validation error: ProviderID is empty")
	}

	p, err := impl.DomainImpl.Storage.Get(id)
	if err != nil {
		status := common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}
		impl.DomainImpl.EventsOut.DeleteFailed(id, status, transactionID)
		return err
	}

	err = impl.DomainImpl.Storage.Delete(id)
	if err != nil {
		status := common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}
		impl.DomainImpl.EventsOut.DeleteFailed(id, status, transactionID)
		return err
	}

	status := common.HTTPStatus{
		Message: "updated the provider successfully",
		Code:    http.StatusAccepted,
	}
	impl.DomainImpl.EventsOut.Deleted(&p, status, transactionID)
	return nil
}
