package types

import "fmt"

// ProviderNotFoundError is a struct for provider not found error
type ProviderNotFoundError struct {
	message string
}

// NewProviderNotFoundError creates ProviderNotFoundError struct
func NewProviderNotFoundError(message string) *ProviderNotFoundError {
	return &ProviderNotFoundError{
		message: message,
	}
}

// NewProviderNotFoundErrorf creates ProviderNotFoundError struct
func NewProviderNotFoundErrorf(format string, v ...interface{}) *ProviderNotFoundError {
	return &ProviderNotFoundError{
		message: fmt.Sprintf(format, v...),
	}
}

// Error returns error message
func (e *ProviderNotFoundError) Error() string {
	return e.message
}

// IsProviderNotFoundError evaluates if the given error is ProviderNotFoundError
func IsProviderNotFoundError(err error) bool {
	if _, ok := err.(*ProviderNotFoundError); ok {
		return true
	}

	return false
}

// ProviderNotAuthorizedError is a struct for unauthorized access error
type ProviderNotAuthorizedError struct {
	message string
}

// NewProviderNotAuthorizedError creates ProviderNotAuthorizedError struct
func NewProviderNotAuthorizedError(message string) *ProviderNotAuthorizedError {
	return &ProviderNotAuthorizedError{
		message: message,
	}
}

// NewProviderNotAuthorizedErrorf creates ProviderNotAuthorizedError struct
func NewProviderNotAuthorizedErrorf(format string, v ...interface{}) *ProviderNotAuthorizedError {
	return &ProviderNotAuthorizedError{
		message: fmt.Sprintf(format, v...),
	}
}

// Error returns error message
func (e *ProviderNotAuthorizedError) Error() string {
	return e.message
}

// IsProviderNotAuthorizedError evaluates if the given error is ProviderNotAuthorizedError
func IsProviderNotAuthorizedError(err error) bool {
	if _, ok := err.(*ProviderNotAuthorizedError); ok {
		return true
	}

	return false
}

// ProviderConflictError is a struct for conflict error
type ProviderConflictError struct {
	message string
}

// NewProviderConflictError creates ProviderConflictError struct
func NewProviderConflictError(message string) *ProviderConflictError {
	return &ProviderConflictError{
		message: message,
	}
}

// NewProviderConflictErrorf creates ProviderConflictError struct
func NewProviderConflictErrorf(format string, v ...interface{}) *ProviderConflictError {
	return &ProviderConflictError{
		message: fmt.Sprintf(format, v...),
	}
}

// Error returns error message
func (e *ProviderConflictError) Error() string {
	return e.message
}

// IsProviderConflictError evaluates if the given error is ProviderConflictError
func IsProviderConflictError(err error) bool {
	if _, ok := err.(*ProviderConflictError); ok {
		return true
	}

	return false
}
