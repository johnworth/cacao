package types

import (
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/messaging"
)

// Config ...
type Config struct {
	LogLevel   string
	OnlyAdmins bool // set to true to restrict crud events to admins
	// NATS
	NatsConfig messaging.NatsConfig
	StanConfig messaging.StanConfig

	// MongoDB
	MongoDBConfig                 db.MongoDBConfig
	ProviderMongoDBCollectionName string
}

// ProcessDefaults will take a Config object and process the config object further, including
// populating any null values
func (c *Config) ProcessDefaults() {
	if c.LogLevel == "" {
		c.LogLevel = "debug"
	}

	// NATS
	if c.NatsConfig.URL == "" {
		c.NatsConfig.URL = DefaultNatsURL
	}

	if c.NatsConfig.QueueGroup == "" {
		c.NatsConfig.QueueGroup = DefaultNatsQueueGroup
	}

	if c.NatsConfig.WildcardSubject == "" {
		c.NatsConfig.WildcardSubject = DefaultNatsWildcardSubject
	}

	if c.NatsConfig.ClientID == "" {
		c.NatsConfig.ClientID = DefaultNatsClientID
	}

	if c.NatsConfig.MaxReconnects <= 0 {
		c.NatsConfig.MaxReconnects = DefaultNatsMaxReconnect
	}

	if c.NatsConfig.ReconnectWait <= 0 {
		c.NatsConfig.ReconnectWait = DefaultNatsReconnectWait
	}

	if c.NatsConfig.RequestTimeout <= 0 {
		c.NatsConfig.RequestTimeout = DefaultNatsRequestTimeout
	}

	if c.StanConfig.ClusterID == "" {
		c.StanConfig.ClusterID = DefaultNatsClusterID
	}

	if c.StanConfig.DurableName == "" {
		c.StanConfig.DurableName = DefaultNatsDurableName
	}

	// MongoDB
	if c.MongoDBConfig.URL == "" {
		c.MongoDBConfig.URL = DefaultMongoDBURL
	}

	if c.MongoDBConfig.DBName == "" {
		c.MongoDBConfig.DBName = DefaultMongoDBName
	}

	if c.ProviderMongoDBCollectionName == "" {
		c.ProviderMongoDBCollectionName = DefaultProviderMongoDBCollectionName
	}
}
