# provider-metadata-service

A microservice for managing providers and their associated metadata. A NATS client interface facilitates basic CRUD operations on providers. The metadata associated with a provider includes:
- ID - a cacao-common ID
- Name - a string
- Type - a string, e.g. openstack
- URL - a string
- Metadata - a map of string keys to things like strings, arrays of strings, any other object that can be marshalled to JSON
- CreatedAt - a UTC timestamp of when the provider was first added
- UpdatedAt - a UTC timestamp of the most recent update to the provider

## Building

To build this microservice, you'll need to have a reasonably up-to-date version
of Go installed. Then run the following in the top-level directory containing
the service code:

```
go build
```

The service should build with that single step. If that is not the case, please
file an issue.

This service uses Go modules.

## Configuration

The service is configured through a set of environment variables. Here are the
currently supported variables and their defaults:
- `ADMINS_ONLY` - set to true to restrict CRUD operations on providers to admins
- `LOG_LEVEL` - the logrus level. Default is `debug`.
