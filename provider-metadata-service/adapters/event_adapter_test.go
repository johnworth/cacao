package adapters

import (
	"gitlab.com/cyverse/cacao-common/service"
	"testing"
)

func Test_validate(t *testing.T) {
	type args struct {
		p *service.ProviderModel
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "normal",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "external-network",
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids": []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
					},
				},
			},
			wantErr: false,
		},
		{
			name: "nil",
			args: args{
				p: &service.ProviderModel{
					ID:       "provider-aaaaaaaaaaaaaaaaaaaa",
					Type:     "openstack",
					Metadata: nil,
				},
			},
			wantErr: true,
		},
		{
			name: "empty",
			args: args{
				p: &service.ProviderModel{
					ID:       "provider-aaaaaaaaaaaaaaaaaaaa",
					Type:     "openstack",
					Metadata: map[string]interface{}{},
				},
			},
			wantErr: true,
		},
		{
			name: "normal",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "external-network",
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids": []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
					},
				},
			},
			wantErr: false,
		},
		{
			name: "empty template id",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "external-network",
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids": []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "empty prerequisite_template",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "external-network",
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids": []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "missing public_ssh_key key",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"external_network_name": "external-network",
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids": []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "public_ssh_key nil",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        nil,
						"external_network_name": "external-network",
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids": []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "empty public_ssh_key",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "",
						"external_network_name": "external-network",
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids": []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "missing external_network_name key",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids": []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "external_network_name nil",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": nil,
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids": []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "empty external_network_name",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "",
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids": []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "missing external_network_uuid key",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "external-network",
						"external_subnet_uuids": []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "external_network_uuid nil",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "external-network",
						"external_network_uuid": nil,
						"external_subnet_uuids": []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "empty external_network_uuid",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "external-network",
						"external_network_uuid": "",
						"external_subnet_uuids": []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "external_network_uuid not uuid",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "external-network",
						"external_network_uuid": "not-uuid",
						"external_subnet_uuids": []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "external_network_uuid int",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "external-network",
						"external_network_uuid": 123,
						"external_subnet_uuids": []string{"cccccccc-cccc-cccc-cccc-cccccccccccc"},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "missing external_subnet_uuids key",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "external-network",
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
					},
				},
			},
			wantErr: true,
		},
		{
			name: "external_subnet_uuids nil",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "external-network",
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids": nil,
					},
				},
			},
			wantErr: true,
		},
		{
			name: "empty external_subnet_uuids",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "external-network",
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids": []string{},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "empty uuid in external_subnet_uuids",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "external-network",
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids": []string{""},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "external_subnet_uuids int",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "external-network",
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids": 123,
					},
				},
			},
			wantErr: true,
		},
		{
			name: "external_subnet_uuids []int",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "external-network",
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids": []int{123},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "external_subnet_uuids []string not uuid",
			args: args{
				p: &service.ProviderModel{
					ID:   "provider-aaaaaaaaaaaaaaaaaaaa",
					Type: "openstack",
					Metadata: map[string]interface{}{
						"prerequisite_template": map[string]string{
							"template_id": "template-aaaaaaaaaaaaaaaaaaaa",
						},
						"public_ssh_key":        "ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA localhost",
						"external_network_name": "external-network",
						"external_network_uuid": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
						"external_subnet_uuids": []string{"not-uuid"},
					},
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validate(tt.args.p); (err != nil) != tt.wantErr {
				t.Errorf("validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
