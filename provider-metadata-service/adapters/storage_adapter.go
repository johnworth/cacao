package adapters

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-metadata-service/types"
)

// MongoAdapter implements PersistentStoragePort
type MongoAdapter struct {
	Config *types.Config
	Store  db.ObjectStore
}

// Init initialize mongodb adapter
func (adapter *MongoAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "MongoAdapter.Init",
	})

	logger.Info("Initializing Mongo Adapter")

	adapter.Config = config

	store, err := db.CreateMongoDBObjectStore(&config.MongoDBConfig)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Fatal("Unable to connect to MongoDB")
	}

	adapter.Store = store
}

// InitMock initialize mongodb adapter with mock_objectstore
func (adapter *MongoAdapter) InitMock(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "MongoAdapter.InitMock",
	})

	logger.Info("Initializing Mongo Adapter")

	adapter.Config = config

	store, err := db.CreateMockObjectStore()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Fatal("Unable to connect to MongoDB")
	}

	adapter.Store = store
}

// GetMock returns Mock
func (adapter *MongoAdapter) GetMock() *mock.Mock {
	if mockObjectStore, ok := adapter.Store.(*db.MockObjectStore); ok {
		return mockObjectStore.Mock
	}
	return nil
}

// Finalize finalizes mongodb adapter
func (adapter *MongoAdapter) Finalize() {
	err := adapter.Store.Release()
	if err != nil {
		log.Fatal(err)
	}

	adapter.Store = nil
}

// Create inserts a Provider
func (adapter *MongoAdapter) Create(provider service.ProviderModel) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "MongoAdapter.Create",
	})

	err := adapter.Store.Insert(adapter.Config.ProviderMongoDBCollectionName, provider)
	if err != nil {
		if db.IsDuplicateError(err) {
			errorMessage := "unable to insert a provider due to conflict"
			logger.WithError(err).Error(errorMessage)
			return types.NewProviderConflictError(errorMessage)
		}

		errorMessage := "unable to insert a provider"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	return nil
}

// MockCreate sets expected results for Create
func (adapter *MongoAdapter) MockCreate(provider service.Provider, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Insert", adapter.Config.ProviderMongoDBCollectionName).Return(expectedError)
	return nil
}

// Delete deletes a Provider
func (adapter *MongoAdapter) Delete(providerID common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "MongoAdapter.Delete",
	})

	// get and check ownership
	p := service.ProviderModel{}

	err := adapter.Store.Get(adapter.Config.ProviderMongoDBCollectionName, string(providerID), &p)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to get a provider for id %s", providerID)
		return types.NewProviderNotFoundErrorf("unable to get a provider for id %s", providerID)
	}

	// delete
	deleted, err := adapter.Store.Delete(adapter.Config.ProviderMongoDBCollectionName, string(providerID))
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to delete a provider for id %s", providerID)
		return fmt.Errorf("unable to delete a provider for id %s", providerID)
	}

	if !deleted {
		logger.Errorf("unable to delete a provider for id %s", providerID)
		return types.NewProviderNotFoundErrorf("unable to delete a provider for id %s", providerID)
	}

	return nil
}

// MockDelete sets expected results for Delete
func (adapter *MongoAdapter) MockDelete(providerID common.ID, existingProvider service.Provider, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.Config.ProviderMongoDBCollectionName, string(providerID)).Return(existingProvider, expectedError)
	mock.On("Delete", adapter.Config.ProviderMongoDBCollectionName, string(providerID)).Return(expectedResult, expectedError)
	return nil
}

// Get returns the Provider with the ID
func (adapter *MongoAdapter) Get(providerID common.ID) (service.ProviderModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "MongoAdapter.Get",
	})

	p := service.ProviderModel{}

	err := adapter.Store.Get(adapter.Config.ProviderMongoDBCollectionName, string(providerID), &p)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to get a provider for id %s", providerID)
		return p, fmt.Errorf("unable to get a provider for id %s", providerID)
	}

	return p, nil
}

// MockGet sets expected results for Get
func (adapter *MongoAdapter) MockGet(providerID common.ID, expectedProvider service.Provider, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Get", adapter.Config.ProviderMongoDBCollectionName, string(providerID)).Return(expectedProvider, expectedError)
	return nil
}

// List returns Providers
func (adapter *MongoAdapter) List() ([]service.ProviderModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "MongoAdapter.List",
	})

	providers := []service.ProviderModel{}
	err := adapter.Store.List(adapter.Config.ProviderMongoDBCollectionName, &providers)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to list providers")
		return nil, fmt.Errorf("unable to list providers")
	}
	return providers, nil
}

// MockList sets expected results for List
func (adapter *MongoAdapter) MockList(expectedProviders []service.Provider, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("List", adapter.Config.ProviderMongoDBCollectionName).Return(expectedProviders, expectedError)
	return nil
}

// Update updates/edits a Provider
func (adapter *MongoAdapter) Update(provider service.ProviderModel, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "MongoAdapter.Update",
	})

	// get and check ownership
	p := service.ProviderModel{}

	err := adapter.Store.Get(adapter.Config.ProviderMongoDBCollectionName, string(provider.GetID()), &p)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to get a provider for id %s", provider.GetID())
		return types.NewProviderNotFoundErrorf("unable to get a provider for id %s", provider.GetID())
	}

	// update
	updated, err := adapter.Store.Update(adapter.Config.ProviderMongoDBCollectionName, string(provider.GetID()), provider, updateFieldNames)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("unable to update a provider for id %s", provider.GetID())
		return fmt.Errorf("unable to update a provider for id %s", provider.GetID())
	}

	if !updated {
		logger.Errorf("unable to update a provider for id %s", provider.GetID())
		return types.NewProviderNotFoundErrorf("unable to update a provider for id %s", provider.GetID())
	}

	return nil
}

// MockUpdate sets expected results for Update
func (adapter *MongoAdapter) MockUpdate(existingProvider service.Provider, provider service.Provider, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.Config.ProviderMongoDBCollectionName, string(provider.GetID())).Return(existingProvider, expectedError)
	mock.On("Update", adapter.Config.ProviderMongoDBCollectionName, string(provider.GetID())).Return(expectedResult, expectedError)
	return nil
}
