package adapters

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"sync"

	log "github.com/sirupsen/logrus"
	"github.com/xeipuuv/gojsonschema"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-metadata-service/ports"
	"gitlab.com/cyverse/cacao/provider-metadata-service/types"
)

// EventAdapter communicates to IncomingEventPort and implements OutgoingEventPort
type EventAdapter struct {
	Config       *types.Config
	IncomingPort ports.IncomingEventPort
	// internal
	Connection     messaging.StreamingEventService
	EventWaitGroup sync.WaitGroup
}

// Init initializes the adapter
func (adapter *EventAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.Init",
	})

	logger.Info("Initializing Event Adapter")

	adapter.Config = config

	// Use a WaitGroup to wait for a message to arrive
	adapter.EventWaitGroup = sync.WaitGroup{}
	adapter.EventWaitGroup.Add(1)
}

// Finalize finalizes the adapter
func (adapter *EventAdapter) Finalize() {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.Finalize",
	})

	logger.Info("Finalizing Event Adapter")

	adapter.EventWaitGroup.Done()
}

// Start starts the adapter
func (adapter *EventAdapter) Start() {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.Start",
	})

	logger.Info("Starting Event Adapter")

	natsConfig := adapter.Config.NatsConfig
	natsConfig.ClientID = natsConfig.ClientID + "-event"

	eventHandlerMappings := []messaging.StreamingEventHandlerMapping{
		{
			Subject:      service.ProviderCreateRequestedEvent,
			EventHandler: adapter.handleProviderCreateRequest,
		},
		{
			Subject:      service.ProviderDeleteRequestedEvent,
			EventHandler: adapter.handleProviderDeleteRequest,
		},
		{
			Subject:      service.ProviderUpdateRequestedEvent,
			EventHandler: adapter.handleProviderUpdateRequest,
		},
		{
			Subject:      common.EventType(""),
			EventHandler: adapter.handleDefaultEvent,
		},
	}

	stanConn, err := messaging.ConnectStanForService(&natsConfig, &adapter.Config.StanConfig, eventHandlerMappings)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Fatal("Unable to connect to NATS Streaming")
	}

	adapter.Connection = stanConn

	defer stanConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

// StartMock starts the adapter
func (adapter *EventAdapter) StartMock() {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.StartMock",
	})

	logger.Info("Starting Event Adapter")

	eventHandlerMappings := []messaging.StreamingEventHandlerMapping{
		{
			Subject:      service.ProviderCreateRequestedEvent,
			EventHandler: adapter.handleProviderCreateRequest,
		},
		{
			Subject:      service.ProviderDeleteRequestedEvent,
			EventHandler: adapter.handleProviderDeleteRequest,
		},
		{
			Subject:      service.ProviderUpdateRequestedEvent,
			EventHandler: adapter.handleProviderUpdateRequest,
		},
		{
			Subject:      common.EventType(""),
			EventHandler: adapter.handleDefaultEvent,
		},
	}

	stanConn, err := messaging.CreateMockStanConnection(&adapter.Config.NatsConfig, &adapter.Config.StanConfig, eventHandlerMappings)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Fatal("Unable to connect to NATS Streaming")
	}

	adapter.Connection = stanConn

	defer stanConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

func checkForAdmin(session *service.Session, config *types.Config) error {
	if !config.OnlyAdmins {
		return nil
	}
	userclient, err := service.NewNatsUserClient(context.Background(), session.GetSessionActor(), session.GetSessionEmulator(), config.NatsConfig, config.StanConfig)
	if err != nil {
		return err
	}
	username := session.GetSessionEmulator()
	if username == "" {
		username = session.GetSessionActor()
	}
	if username == "" {
		return fmt.Errorf("session actor and emulator not specified")
	}
	var user service.User
	fmt.Println("about to userclient.Get")
	user, err = userclient.Get(username)
	fmt.Println("back from userclient.Get")
	if err != nil {
		return err
	}
	if !user.GetIsAdmin() {
		return fmt.Errorf("user is not admin, unable to perform operation on provider metadata")
	}
	return nil
}

func (adapter *EventAdapter) handleProviderCreateRequest(subject common.EventType, transactionID common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.handleProviderCreateRequest",
	})

	var request service.ProviderRequest
	err := json.Unmarshal(jsonData, &request)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into ProviderRequest"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	if err = checkForAdmin(&request.Session, adapter.Config); err != nil {
		return err
	}

	if err = validate(&request.ProviderModel); err != nil {
		return err
	}

	err = adapter.IncomingPort.Create(&request.ProviderModel, transactionID)
	if err != nil {
		errorMessage := "unable to handle the request"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	return nil
}

func (adapter *EventAdapter) handleProviderDeleteRequest(subject common.EventType, transactionID common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.handleProviderDeleteRequest",
	})

	var request service.ProviderRequest
	err := json.Unmarshal(jsonData, &request)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into ProviderRequest"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	if err = checkForAdmin(&request.Session, adapter.Config); err != nil {
		return err
	}

	err = adapter.IncomingPort.Delete(request.ProviderModel.ID, transactionID)
	if err != nil {
		errorMessage := "unable to handle the request"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	return nil
}

func (adapter *EventAdapter) handleProviderUpdateRequest(subject common.EventType, transactionID common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.handleProviderUpdateRequest",
	})

	var request service.ProviderRequest
	err := json.Unmarshal(jsonData, &request)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into ProviderRequest"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	if err = checkForAdmin(&request.Session, adapter.Config); err != nil {
		return err
	}

	// check which fields to update
	updateFieldNames := []string{}
	if request.ProviderModel.Name != "" {
		updateFieldNames = append(updateFieldNames, "name")
	}
	if request.ProviderModel.URL != "" {
		updateFieldNames = append(updateFieldNames, "url")
	}
	if request.ProviderModel.Type != "" {
		updateFieldNames = append(updateFieldNames, "type")
	}
	if request.ProviderModel.Metadata != nil {
		if err = validate(&request.ProviderModel); err != nil {
			return err
		}
		updateFieldNames = append(updateFieldNames, "metadata")
	}

	err = adapter.IncomingPort.Update(&request.ProviderModel, updateFieldNames, transactionID)
	if err != nil {
		errorMessage := "unable to handle the request"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	return nil
}

// Created ...
func (adapter *EventAdapter) Created(responseData service.Provider, status common.HTTPStatus, transactionID common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.Created",
	})

	response := common.ServiceRequestResult{
		Data:   responseData,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(service.ProviderCreatedEvent, response, transactionID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("failed to publish an event %s", service.ProviderCreatedEvent)
		return fmt.Errorf("failed to publish an event %s", service.ProviderCreatedEvent)
	}

	return nil
}

// CreateFailed ...
func (adapter *EventAdapter) CreateFailed(provider service.Provider, status common.HTTPStatus, transactionID common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.CreateFailed",
	})

	response := common.ServiceRequestResult{
		Data:   provider,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(service.ProviderCreateFailedEvent, response, transactionID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("failed to publish an event %s", service.ProviderCreateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", service.ProviderCreateFailedEvent)
	}

	return nil
}

// Updated ...
func (adapter *EventAdapter) Updated(provider service.Provider, status common.HTTPStatus, transactionID common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.Updated",
	})

	response := common.ServiceRequestResult{
		Data:   provider,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(service.ProviderUpdatedEvent, response, transactionID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("failed to publish an event %s", service.ProviderUpdatedEvent)
		return fmt.Errorf("failed to publish an event %s", service.ProviderUpdatedEvent)
	}

	return nil
}

// UpdateFailed ...
func (adapter *EventAdapter) UpdateFailed(responseData service.Provider, status common.HTTPStatus, transactionID common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.UpdateFailed",
	})

	response := common.ServiceRequestResult{
		Data:   responseData,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(service.ProviderUpdateFailedEvent, response, transactionID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("failed to publish an event %s", service.ProviderUpdateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", service.ProviderUpdateFailedEvent)
	}

	return nil
}

// Deleted ...
func (adapter *EventAdapter) Deleted(responseData service.Provider, status common.HTTPStatus, transactionID common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.Deleted",
	})

	response := common.ServiceRequestResult{
		Data:   responseData,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(service.ProviderDeletedEvent, response, transactionID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("failed to publish an event %s", service.ProviderDeletedEvent)
		return fmt.Errorf("failed to publish an event %s", service.ProviderDeletedEvent)
	}

	return nil
}

// DeleteFailed ...
func (adapter *EventAdapter) DeleteFailed(id common.ID, status common.HTTPStatus, transactionID common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.DeleteFailed",
	})

	response := common.ServiceRequestResult{
		Data:   id,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(service.ProviderDeleteFailedEvent, response, transactionID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Errorf("failed to publish an event %s", service.ProviderDeleteFailedEvent)
		return fmt.Errorf("failed to publish an event %s", service.ProviderDeleteFailedEvent)
	}

	return nil
}

func (adapter *EventAdapter) handleDefaultEvent(subject common.EventType, transactionID common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.handleDefaultEvent",
	})

	logger.Tracef("Received an unhandled event %s, TransactionID %s", subject, transactionID)
	return nil
}

var schemas = map[string]string{
	"openstack": `{"type": "object",
		"properties": {
			"prerequisite_template": {
				"type": "object",
				"properties": {
					"template_id": {
						"type": "string",
						"minLength": 1
					}
				},
				"required": ["template_id"]
			},
			"public_ssh_key": {
				"type": "string",
				"minLength": 1
			},
			"external_network_name": {
				"type": "string",
				"minLength": 1
			},
			"external_network_uuid": {
				"type": "string",
				"format": "uuid",
				"minLength": 1
			},
			"external_subnet_uuids": {
				"type": "array",
				"items": {
					"type": "string",
					"format": "uuid",
					"minLength": 1
				},
				"minItems": 1,
				"uniqueItems": true
			}
		},
		"required": ["prerequisite_template", "public_ssh_key", "external_network_name", "external_network_uuid", "external_subnet_uuids"]}`,
}

func validate(p *service.ProviderModel) error {
	schema, ok := schemas[p.Type]
	if !ok {
		return nil
	}
	schemaLoader := gojsonschema.NewStringLoader(schema)
	documentLoader := gojsonschema.NewGoLoader(p.Metadata)
	result, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		return err
	}
	if !result.Valid() {
		s := ""
		for _, desc := range result.Errors() {
			s += fmt.Sprintf("- %s\n", desc)
		}
		return errors.New(s)
	}
	return nil
}
