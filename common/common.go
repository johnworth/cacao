// Package common provides functions and structs used by multiple Cacao services
// for interacting with Kubernetes clusters, NATS queues, and other services
package common

import (
	"bufio"
	"io"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/rs/xid"
)

// GetNATSInfo returns a map containing necessary NATS connection variables. It is
// used to get this information from environment variables or use the defaults
func GetNATSInfo(defaultClusterID string, defaultClientID string, defaultConnAddress string) map[string]string {
	natsInfo := make(map[string]string)
	natsInfo["cluster_id"] = os.Getenv("NATS_CLUSTER_ID")
	if len(natsInfo["cluster_id"]) == 0 {
		natsInfo["cluster_id"] = defaultClusterID
	}
	natsInfo["client_id"] = os.Getenv("NATS_CLIENT_ID")
	if len(natsInfo["client_id"]) == 0 {
		natsInfo["client_id"] = defaultClientID
	}

	// Append xid to client ID to allow scaling
	natsInfo["client_id"] += "-" + xid.New().String()
	natsInfo["address"] = os.Getenv("NATS_ADDRESS")
	if len(natsInfo["address"]) == 0 {
		natsInfo["address"] = defaultConnAddress
	}
	return natsInfo
}

// FixName will make a string K8s-valid by making it lowercase and hyphenated
func FixName(name string) string {
	re := regexp.MustCompile(`[/:\.]`)
	return strings.ToLower(re.ReplaceAllString(name, "-"))
}

// CreateResourceName is used to create a K8s resource name that is both unique
// and informative to users:
//  <username:limit to 8>-<name:limit to 32 chars>-<xid>
func CreateResourceName(username, name, id string) string {
	if len(username) > 8 {
		username = username[0:8]
	}
	if len(name) > 32 {
		name = name[0:32]
	}
	return username + "-" + name + "-" + id
}

// MsToTime takes a MS String value and returns a GO TIME value.
func MsToTime(ms string) (time.Time, error) {
	msInt, err := strconv.ParseInt(ms, 10, 64)
	if err != nil {
		return time.Time{}, err
	}

	return time.Unix(0, msInt*int64(time.Millisecond)), nil
}

// ReadLastLine Function that returns last line of any file on host FS, filename is full path.
func ReadLastLine(filename string) string {
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	// we need to calculate the size of the last line for file.ReadAt(offset) to work
	// NOTE : not a very effective solution as we need to read
	// the entire file at least for 1 pass :(
	lastLineSize := 0

	for {
		line, _, err := reader.ReadLine()
		if err == io.EOF {
			break
		}
		lastLineSize = len(line)
	}
	fileInfo, _ := os.Stat(filename)
	// make a buffer size according to the lastLineSize
	buffer := make([]byte, lastLineSize)
	// +1 to compensate for the initial 0 byte of the line
	// otherwise, the initial character of the line will be missing
	// instead of reading the whole file into memory, we just read from certain offset
	offset := fileInfo.Size() - int64(lastLineSize+1)
	numRead, _ := file.ReadAt(buffer, offset)

	buffer = buffer[:numRead]

	return string(buffer)
}
