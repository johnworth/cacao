#!/usr/bin/python3
'''
Generate 256bits key and print out the base64 encoded key
'''
import base64
from cryptography.fernet import Fernet

# generate 32bytes/256bits key
KEY_LEN_BYTES = 32

key = Fernet.generate_key()
if len(key) < KEY_LEN_BYTES:
    exit(1)
elif len(key) > KEY_LEN_BYTES:
    key = key[:KEY_LEN_BYTES]
print(base64.standard_b64encode(key).decode("utf-8"))
