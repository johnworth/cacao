#!/bin/bash

SUDO="sudo"
APT_UPDATE_RAN=0

function run_apt_update {
    if [ "$APT_UPDATE_RAN" == 0 ]; then
        $SUDO apt update -y
        APT_UPDATE_RAN=1
    fi

}
if [ $# -lt 1 ] || [[ "$1" != "local"  && "$1" != "prod" && "$1" != "ci" ]]; then
    echo "Usage: $0 [local|prod|ci]"
	  exit 1
fi

CACAO_DEPLOY_ENVIRONMENT="$1"
if [ "$CACAO_DEPLOY_ENVIRONMENT" == "prod" ] && [ ! -f "hosts.yml" ]; then
    printf "No hosts.yml found for production deployment.  Exiting.\n"
    exit 1
fi

if [ "$CACAO_DEPLOY_ENVIRONMENT" == "ci" ]; then
    # Dont' use sudo in CI since we're already root in the container
    SUDO=''
    run_apt_update
    apt install -y apt-utils
    echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
    apt install -y openssh-client
fi

CACAO_CONFIG_FILE="config.$CACAO_DEPLOY_ENVIRONMENT.yml"
printf "Deploying to $CACAO_DEPLOY_ENVIRONMENT...\n"

if ! command -v envsubst &> /dev/null ; then
    run_apt_update
    apt install -y gettext-base
fi

if ! command -v pip3 &> /dev/null ; then
    run_apt_update
    $SUDO apt install -y python3-pip
fi

if ! command -v ansible &> /dev/null ; then
    run_apt_update
    yes | pip3 install -Iv ansible==2.9.16
fi

if ! command -v jq &> /dev/null ; then
    run_apt_update
    $SUDO apt-get install -y jq
fi

if ! command -v curl &> /dev/null ; then
    run_apt_update
    $SUDO apt-get install -y curl
fi

if [ ! -f "$CACAO_CONFIG_FILE" ] ; then
    cp "example_$CACAO_CONFIG_FILE" $CACAO_CONFIG_FILE

    if [ "$CACAO_DEPLOY_ENVIRONMENT" == "local" ]; then
	DEFAULT_CLUSTER_TYPE="k3d"
        read -p "Enter your cluster type, either k3s or k3d [$DEFAULT_CLUSTER_TYPE]: " CLUSTER_TYPE
        CLUSTER_TYPE=${CLUSTER_TYPE:-$DEFAULT_CLUSTER_TYPE}
        export CLUSTER_TYPE

        DEFAULT_EDGE_SRC_DIR="$(dirname $(dirname $(pwd)))/cacao-edge-deployment"
        read -p "Enter the path to the cacao-edge-deployment source code directory [$DEFAULT_EDGE_SRC_DIR]: " EDGE_SRC_DIR
        EDGE_SRC_DIR=${EDGE_SRC_DIR:-$DEFAULT_EDGE_SRC_DIR}
        export EDGE_SRC_DIR

        DEFAULT_SRC_DIR=`dirname $PWD`
        read -p "Enter the path to the Cacao source code directory [$DEFAULT_SRC_DIR]: " SRC_DIR
        SRC_DIR=${SRC_DIR:-$DEFAULT_SRC_DIR}
        export SRC_DIR
        DEFAULT_GIT_URL=`git remote get-url origin`
        DEFAULT_GIT_BRANCH=`git rev-parse --abbrev-ref HEAD`
    else
        DEFAULT_GIT_URL="https://gitlab.com/cyverse/cacao.git"
        DEFAULT_GIT_BRANCH="master"
    fi

    if [ "$CACAO_DEPLOY_ENVIRONMENT" == "ci" ]; then
        GIT_URL="${CI_MERGE_REQUEST_PROJECT_URL:-https://gitlab.com/cyverse/cacao}"
        GIT_BRANCH="${CI_MERGE_REQUEST_TARGET_BRANCH_NAME:-master}"
        CACAO_ROOT="${CACAO_ROOT:-/opt/cacao}" # set to /opt/cacao as default, if the env variable doesn't already exist
        export CACAO_ROOT
        echo "DEBUG: GIT_URL = $GIT_URL"
        echo "DEBUG: GIT_BRANCH = $GIT_BRANCH"
        echo "DEBUG: CACAO_ROOT = $CACAO_ROOT"
    else
        read -p "Enter the URL to the Git remote repo [$DEFAULT_GIT_URL]: " GIT_URL
        GIT_URL=${GIT_URL:-$DEFAULT_GIT_URL}

        read -p "Enter the Git branch you want to use [$DEFAULT_GIT_BRANCH]: " GIT_BRANCH
        GIT_BRANCH=${GIT_BRANCH:-$DEFAULT_GIT_BRANCH}
    fi
    export GIT_URL
    export GIT_BRANCH

    envsubst < $CACAO_CONFIG_FILE > config_subst.yml
    mv config_subst.yml $CACAO_CONFIG_FILE
fi

if [ -L config.yml ]; then
    rm config.yml
fi

ln -s $CACAO_CONFIG_FILE config.yml

# if AWM_KEY is not populated in config.yml, generate a new one and replace it.
if ! grep -e "^AWM_KEY:" config.yml > /dev/null; then
    echo "AWM_KEY: $(python3 roles/cacao/files/key_gen.py)" >> config.yml
    echo "AWM_KEY added to config.yml"
elif [ $(grep -e "^AWM_KEY:" config.yml | xargs | wc -c) != 54 ]; then
    sed "s/^AWM_KEY:.*/AWM_KEY: $(python3 roles/cacao/files/key_gen.py)/g"
    echo "AWM_KEY is populated with generated key in config.yml"
fi

ansible-galaxy role install -r requirements.yml

if [ "$CACAO_DEPLOY_ENVIRONMENT" == "local" ]; then

    if [ `id -u` -eq 0 ]; then
       ansible-playbook -i localhost.yml playbook-cacao.yml
       if [ $? -ne 0 ]; then
            echo "?Non-zero status was returned, propagating the non-zero status"
            exit 1
       fi
    else
       USER_IN_DOCKER_GROUP=`id -Gn $USER | grep '\bdocker\b'`
       if [[ ! $USER_IN_DOCKER_GROUP ]]; then
           $SUDO addgroup docker
           $SUDO usermod -aG docker $USER
           printf "User \"$USER\" has been added to the \"docker\" group\n"
           printf "To complete local deployment, please log out, log back in, and re-run this script.\n"
           exit 1
       fi

       ansible-playbook -K -i localhost.yml playbook-cacao.yml
       if [ $? -ne 0 ]; then
            echo "?Non-zero status was returned, propagating the non-zero status"
            exit 1
       fi
    fi

    printf "Setup complete! \n"
    printf "If you don't see any failures, go to CACAO_SRC_DIR and run 'skaffold dev --status-check=false' to start Cacao\n"
    cat config.yml | grep CACAO_SRC_DIR:
elif [ "$CACAO_DEPLOY_ENVIRONMENT" == "ci" ]; then
    ansible-playbook --private-key ~/.ssh/id_ed25519 -i hosts.yml playbook-cacao.yml
    if [ $? -ne 0 ]; then
        echo "?Non-zero status was returned, propagating the non-zero status"
        exit 1
    else
        printf "Set up complete!\n"
    fi
elif [ "$CACAO_DEPLOY_ENVIRONMENT" == "prod" ]; then
    ansible-playbook -i hosts.yml playbook-cacao.yml
    if [ $? -ne 0 ]; then
        echo "?Non-zero status was returned, propagating the non-zero status"
        exit 1
    else
        printf "Set up complete!\n"
    fi
fi

#
# deploy edge components including AWM
#
if [ "$CACAO_DEPLOY_ENVIRONMENT" == "local" ]; then
    if [ -d "cacao-edge-deployment" ]; then

        # copy config.yml for cacao-edge-deployment
        cp config.yml cacao-edge-deployment/argo-workflow-mediator/install/${CACAO_CONFIG_FILE}

        if [ ! -f "cacao-edge-deployment/argo-workflow-mediator/install/deploy.sh" ]; then
            echo "cacao-edge-deployment/argo-workflow-mediator/install/ is missing deploy.sh"
            exit 1
        fi

        # run deploy.sh from cacao-edge-deployment
        cd cacao-edge-deployment/argo-workflow-mediator/install && ./deploy.sh ${CACAO_DEPLOY_ENVIRONMENT}
        if [ $? -ne 0 ]; then
            echo "?Non-zero status was returned, propagating the non-zero status"
            exit 1
        else
            printf "cacao-edge-deployment set up complete!\n"
        fi
    else
        echo "cacao-edge-deployment repo is absent"
        exit 1
    fi
fi
