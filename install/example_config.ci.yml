#####
# THIS SECTION SHOULD BE REVIEWED
#####

# Force Ansible to use Python 3.  The is necessary for Ubuntu 18.04 support.
ansible_python_interpreter: "/usr/bin/python3"

# is this a development environment, meaning deploy development k8s services
IS_DEV_ENVIRONMENT: false

# if dev environment and you want also want to deploy developer k8s cluster
# SKAFFOLD_DELETE will also execute a skaffold delete just before a skaffold run
CLUSTER_TYPE: k3s
SKAFFOLD: true
SKAFFOLD_FORCE_DELETE: true
SKAFFOLD_VERSION: "v1.26.1"

##
# NGINX INGRESS SETTINGS
##
# The domain at which the Cacao API is available.
# For dev environments, an entry will be added to /etc/hosts with this domain and
# the NGINX ingress IP.
API_DOMAIN: "cacaosvc.cyverse.rocks"
# force SSL REDIRECT
SSL_REDIRECT: false

# Set the Docker registry that k3d should use
CACAO_REGISTRY_HOSTNAME: "registry.localhost"
CACAO_REGISTRY_PORT: 5000

#####
# THIS SECTION IS OPTIONAL TO REVIEW
#####

# set the go version
go_version: "1.16.5"

# If deploying kubernetes and tools, set the version
KUBE_VERSION: "1.19.5"

# location where to checkout cacao code (for config yaml etc)
CACAO_GIT_REPO:  $GIT_URL
CACAO_GIT_BRANCH: $GIT_BRANCH
CACAO_SRC_DIR: $CACAO_ROOT
CACAO_EDGE_DEPLOYMENT_SRC_DIR: $CACAO_ROOT/../cacao-edge-deployment
CACAO_CONFIG_DIR: "{{ CACAO_SRC_DIR }}/install/configs"
CACAO_DEPLOY_DIR: "{{ CACAO_SRC_DIR }}/install/deploy"

# Uncomment and define if necessary; defines the base path for images used for microservices
# CACAO_REGISTRY_PATH: "registry.gitlab.com/simpsonw/cacao"

# uncomment this variable, if you want a specific version of KUBECTL; otherwise, latest will be used
# NOTE: prefix the version with "v" for consistency with obtaining latest version
# KUBECTL_VERSION: "v1.18.2"

# TLS Settings
TLS_SECRET_KEY: /etc/ssl/private/cyverse.rocks.key
TLS_SECRET_CERT: /etc/ssl/certs/cyverse.rocks.pem
TLS_SECRET_NAME: cyverse-tls


# anchore
# Set this to the version of ANCHORE
ANCHORE_VERSION: "1.6.9"
ANCHORE_ENABLED: true

# keda
# Set this to the version of KEDA
KEDA_VERSION: "1.5.0"

# kubernetes-specific information
CACAO_SERVICE_CONTEXT: "cacao-svc"
CACAO_USER_CONTEXT: "cacao-usr"

# Define this variable to use a specific kubeconfig file for admin access
# if this is not defined, the kind-user-cluster config will be automatically used
#ADMIN_KUBECONFIG: "base64_encoded_kubeconfig_file"

# vault configuration
VAULT_IMAGE: "vault:1.4.7"
VAULT_CONTAINER_ENABLED: true
VAULT_TOKEN: "sometokenyougenerate"
VAULT_ADDRESS: http://vault:8200

# mongodb configuration
MONGODB_CONTAINER_ENABLED: true
MONGODB_ADDRESS: "mongodb://cacao-mongo:27017"
MONGODB_NAME: "cacao"

# other cacao configuration
NATS_ADDRESS: "nats://nats:4222"
# increase replicas if you want replicas, BUT don't set to > 1 unless using a shared volume, like the local path provisioner (see below)
NATS_NUM_REPLICAS: 1

# This select the authentication; current supported values are "globus", "keycloak" or "simpletoken"
CACAO_AUTH_DRIVER: "keycloak"

# Simple Token is only used if CACAO_AUTH_DRIVER == "simpletoken"
# SIMPLETOKEN_USERNAME: testuser
# SIMPLETOKEN_TOKEN: testtoken

# only used if auth driver = "globus"
# GLOBUS_URL: "https://auth.globus.org/v2/oauth2" # this is default, only modify if necessary
GLOBUS_API_REDIRECT_URL: "http://{{ API_DOMAIN }}/api/user/login/callback"
GLOBUS_API_CLIENT_ID: "some-api-globus-client-id"
GLOBUS_API_CLIENT_SECRET: "some-api-globus-client-secret"

# this configuration section is for Keycloak
KEYCLOAK_IMAGE: 'jboss/keycloak:10.0.2'
KEYCLOAK_ENABLED: true
KEYCLOAK_LOCAL: true
KEYCLOAK_SECRET: "top-secret-info"
KEYCLOAK_REALM: "cacao"
KEYCLOAK_BASE_URL: "https://{{ API_DOMAIN }}"
KEYCLOAK_URL: "https://{{ API_DOMAIN }}/auth/realms/cacao"
KEYCLOAK_REDIRECT_URL: "https://{{ API_DOMAIN }}/api/user/login/callback"
KEYCLOAK_USERS:
  - username: cacao-user
    password: cacao-user-password
    admin: false
  - username: cacao-admin
    password: cacao-admin-password
    admin: true

# enable local-path-provisioner csi driver
# note: LOCAL_PATH_PROVISIONER_ENABLE must be enabled if it's enabled for any cacao dependency services
# note: default path is all pods pvs will be located in /opt/local-path-provisioner
LOCAL_PATH_PROVISIONER_ENABLE: false
LOCAL_PATH_PROVISIONER_HOST_PATH: "/opt/local-path-provisioner" # this currently cannot be changed in this ansible... yet
# a separate directory for pv resource files so that PVs don't get accidentally blown away
LOCAL_PATH_PROVISIONER_CONFIG_DIR: "{{ CACAO_CONFIG_DIR }}_pv"
# this enables local path for nats-stan
LOCAL_PATH_PROVISIONER_NATS_ENABLE: true
# size of local path volume for nats, in k8s-speak
LOCAL_PATH_PROVISIONER_NATS_SIZE: "10Gi"

###
# CACAO-SPECIFIC CONFIGURATION
###
# address to connect to the cacao api
#CACAO_API_INTERNAL_ADDRESS: http://api:8080
# loglevel to be passed to containers
#CACAO_LOG_LEVEL: "trace"

###
# ION-SPECIFIC CONFIGURATION
###
#ION_THEME: "cyverse"
ION_SESSION_SECRET: "some-random-text"
ION_KEYCLOAK_CLIENT: "ion-prod"
ION_KEYCLOAK_SECRET: "xxx"
#ION_GLOBUS_CLIENT: "some-globus-client"
#ION_GLOBUS_SECRET: "some-globus-secret"
ION_UI_BASE_URL: "http://{{ API_DOMAIN }}"
ION_WS_BASE_URL: "http://{{ API_DOMAIN }}/ws"
ION_API_BASE_URL: "http://{{ API_DOMAIN }}/api"

###
# DEPLOYMENT-SPECIFIC CONFIGURATION
###
# this is used to populate the prerequisite template for deployment execution service (temporary for now, should be moved to provider)
OPENSTACK_EXTERNAL_NETWORK_UUID: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa

# this key is used to encrypt credentials to pass to argo workflow mediator(AWM)
# this key will be generated by deployment-service ansible role, if not provided here
#AWM_KEY: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa=
