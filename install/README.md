# Install

### Table of Contents
[[_TOC_]]


## Getting Started

This directory is reserved for ansible and other resources needed to deploy the CACAO platform and user clusters.

### Prerequisites

0. Ubuntu 18.04 and newer are currently the only supported operating systems.
1. [Install Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-ubuntu)
2. [Install Docker](https://docs.docker.com/engine/install/ubuntu/)
3. Ensure that you can [manage Docker as a non-root user](https://docs.docker.com/engine/install/linux-postinstall/)

## CACAO Deployment

CACAO deployment is done using the `deploy.sh` script in this directory.  Currently, there are two supported environments that are passed as arguments to this script: `local` and `prod`.

### Local development

If you pass `local` as the argument to `deploy.sh`, the script will generate a configuration file called `config.local.yml`, symbolically link it to `config.yml`, and run the `playbook-cacao.yml` Ansible playbook.  This playbook will install [k3d](https://k3d.io/) and use it to provision two Kubernetes clusters running in Docker, one for running the application itself (the "service cluster") and one for running the analyses (the "user cluster").  It will then deploy the CACAO application itself and any required dependencies to these clusters, including Vault, Keycloak, and MongoDB.

```
$ ./deploy.sh local
Deploying to local...
Enter the URL to the Git remote repo [git@gitlab.com:simpsonw/cacao.git]:
Enter the Git branch you want to use [master]:
Enter the path to the CACAO source code directory [/home/will/workspace/golang/src/gitlab.com/cyverse/cacao]:
```

Once the script has finished, go to the project root. Ensure that you have the service cluster selected as your current context:

```
kubectl config use-context k3d-service-cluster
```

Now run `skaffold dev --status-check=false` to deploy CACAO to your local Kubernetes cluster.

An entry will be added to your `/etc/hosts` file pointing the value of `API_DOMAIN` at the IP address of the NGINX ingress gateway.

### Production deployment

Before you deploy to production, you must:

0. Verify that you have access to [Stache](https://stache.arizona.edu/).
1. Copy the production `hosts.yml` file secret to a file named `hosts.yml` in this directory (ask in Slack if you have trouble finding it).

Now run `./deploy.sh prod`.  You will be prompted for the Git repo and branch you wish to deploy (these will default to `https://gitlab.com/cyverse/cacao.git` and `master` respectively, unlike `local` which defaults to the remote and branch you have checked out).  Next, you will be prompted for the Stache API endpoint for the production secrets.  Ask for the name of the secret in Slack and look it up in Stache.  Once you've found it, click the "Options" tab on the right side of the page.  Copy the path in the "API Endpoint" text area under "READ DATA" and paste it into your terminal.  Then you'll be prompted for the `X-STACHE-KEY`.  Click the greyed out text area entitled "X-STACHE-KEY" to view the value and copy it into your terminal.  The script will now generate a file called `config.prod.yml`, symbolically link it to `config.yml`, and then run Ansible to do a production deployment.
