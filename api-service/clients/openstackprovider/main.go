package openstackprovider

import (
	"context"
	"errors"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
)

// Client is an interface for interacting with the provider openstack microservice. Implementations of this interface
// should encapsulate information necessary to interact with the microservice, such as Nats and Stan connection
// information.
type Client interface {
	Session(ctx context.Context, actor, emulator string) (Session, error)
}

// Session is an interface for interacting with the provider openstack microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	ApplicationCredentialList(providerID, credentialID string) ([]providers.ApplicationCredential, error)
	GetApplicationCredential(providerID, credentialID, applicationCredentialID string) (*providers.ApplicationCredential, error)
	ImageList(providerID, credentialID string) ([]providers.Image, error)
	GetImage(providerID, credentialID, imageID string) (*providers.Image, error)
	FlavorList(providerID, credentialID string) ([]providers.Flavor, error)
	GetFlavor(providerID, credentialID, flavorID string) (*providers.Flavor, error)
	ProjectList(providerID, credentialID string) ([]providers.Project, error)
	GetProject(providerID, credentialID, projectID string) (*providers.Project, error)
}

// primary implementation of Client interface.
type providerClient struct {
	natsConfig messaging.NatsConfig
}

// New creates a new provider openstack microservice client.
func New(natsConfig messaging.NatsConfig) Client {
	return &providerClient{
		natsConfig: natsConfig,
	}
}

// Session ...
func (c providerClient) Session(ctx context.Context, actor, emulator string) (Session, error) {
	if actor == "" {
		return nil, errors.New("no actor specified")
	}
	if ctx == nil {
		log.WithFields(log.Fields{
			"package":  "openstackprovider",
			"function": "providerClient.Session",
		}).Error("context is nil")
		return nil, errors.New("context is nil")
	}
	return providerSession{
		natsConfig: &c.natsConfig,
		ctx:        ctx,
		actor:      actor,
		emulator:   emulator,
	}, nil
}

// primary implementation of Session interface.
type providerSession struct {
	natsConfig *messaging.NatsConfig
	ctx        context.Context
	actor      string
	emulator   string
}

// ApplicationCredentialList ...
func (s providerSession) ApplicationCredentialList(providerID, credentialID string) ([]providers.ApplicationCredential, error) {
	svcClient := providers.NewOpenStackProvider(common.ID(providerID), s.natsConfig)
	applicationCredentialList, err := svcClient.ApplicationCredentialList(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, credentialID)
	if err != nil {
		return nil, err
	}
	return applicationCredentialList, nil
}

// GetApplicationCredential ...
func (s providerSession) GetApplicationCredential(providerID, credentialID, applicationCredentialID string) (*providers.ApplicationCredential, error) {
	svcClient := providers.NewOpenStackProvider(common.ID(providerID), s.natsConfig)
	applicationCredential, err := svcClient.GetApplicationCredential(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, applicationCredentialID, credentialID)
	if err != nil {
		return nil, err
	}
	return applicationCredential, nil
}

// ImageList ...
func (s providerSession) ImageList(providerID, credentialID string) ([]providers.Image, error) {
	svcClient := providers.NewOpenStackProvider(common.ID(providerID), s.natsConfig)
	imageList, err := svcClient.ImageList(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, credentialID)
	if err != nil {
		return nil, err
	}
	return imageList, nil
}

// GetImage ...
func (s providerSession) GetImage(providerID, credentialID, imageID string) (*providers.Image, error) {
	svcClient := providers.NewOpenStackProvider(common.ID(providerID), s.natsConfig)
	image, err := svcClient.GetImage(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, imageID, credentialID)
	if err != nil {
		return nil, err
	}
	return image, nil
}

// FlavorList ...
func (s providerSession) FlavorList(providerID, credentialID string) ([]providers.Flavor, error) {
	svcClient := providers.NewOpenStackProvider(common.ID(providerID), s.natsConfig)
	flavorList, err := svcClient.FlavorList(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, credentialID)
	if err != nil {
		return nil, err
	}
	return flavorList, nil
}

// GetFlavor ...
func (s providerSession) GetFlavor(providerID, credentialID, flavorID string) (*providers.Flavor, error) {
	svcClient := providers.NewOpenStackProvider(common.ID(providerID), s.natsConfig)
	flavor, err := svcClient.GetFlavor(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, flavorID, credentialID)
	if err != nil {
		return nil, err
	}
	return flavor, nil
}

// ProjectList ...
func (s providerSession) ProjectList(providerID, credentialID string) ([]providers.Project, error) {
	svcClient := providers.NewOpenStackProvider(common.ID(providerID), s.natsConfig)
	projectList, err := svcClient.ProjectList(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, credentialID)
	if err != nil {
		return nil, err
	}
	return projectList, nil
}

// GetProject ...
func (s providerSession) GetProject(providerID, credentialID, projectID string) (*providers.Project, error) {
	svcClient := providers.NewOpenStackProvider(common.ID(providerID), s.natsConfig)
	project, err := svcClient.GetProject(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, projectID, credentialID)
	if err != nil {
		return nil, err
	}
	return project, nil
}
