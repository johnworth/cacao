package deployments

import (
	"encoding/json"
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao-common/service"
)

// ServiceDeploymentToHTTP  converts the service representation of deployment to HTTP
func ServiceDeploymentToHTTP(svcDeployment service.Deployment) hm.Deployment {
	var httpCloudCreds []string
	if svcDeployment.CloudCredentials != nil {
		httpCloudCreds = make([]string, 0, len(svcDeployment.CloudCredentials))
		for cred := range svcDeployment.CloudCredentials {
			httpCloudCreds = append(httpCloudCreds, cred)
		}
	}

	var httpParameters []hm.KeyValue
	var currentRun hm.DeploymentRun
	if svcDeployment.LastRun == nil {
		httpParameters = []hm.KeyValue{}
	} else {
		httpParameters = ServiceParametersToHTTP(svcDeployment.LastRun.Parameters)
		currentRun = ServiceRunToHTTP(*svcDeployment.LastRun)
	}
	return hm.Deployment{
		ID:                svcDeployment.ID.String(),
		Owner:             svcDeployment.Owner,
		Name:              svcDeployment.Name,
		Description:       svcDeployment.Description,
		WorkspaceID:       svcDeployment.Workspace.String(),
		TemplateID:        svcDeployment.Template.String(),
		PrimaryProviderID: svcDeployment.PrimaryCloud.String(),
		CreatedAt:         svcDeployment.CreatedAt,
		UpdatedAt:         svcDeployment.UpdatedAt,
		CurrentStatus:     svcDeployment.CurrentStatus.String(),
		PendingStatus:     svcDeployment.PendingStatus.String(),
		StatusMsg:         svcDeployment.StatusMsg,
		Parameters:        httpParameters,
		CloudCredentials:  httpCloudCreds,
		GitCredentialID:   svcDeployment.GitCredential,
		CurrentBuild:      hm.DeploymentBuild{}, // TODO Deployment build
		CurrentRun:        currentRun,
	}
}

// HTTPBuildToService converts the HTTP representation of deployment build to service
// TODO deployment build
func HTTPBuildToService(httpBuild hm.DeploymentBuild) interface{} {
	panic("implement this")
}

// ServiceBuildToHTTP converts the service representation of deployment build to HTTP
// TODO deployment build
func ServiceBuildToHTTP(svcBuild interface{}) hm.DeploymentBuild {
	panic("implement this")
}

// HTTPRunToService converts the HTTP representation of deployment run to service
func HTTPRunToService(httpRun hm.DeploymentRun, meta service.TemplateMetadata) service.DeploymentRun {
	var svcCloudCreds = make(map[string]common.ID)
	if httpRun.CloudCredentials != nil {
		for _, cred := range httpRun.CloudCredentials {
			svcCloudCreds[cred] = ""
		}
	}

	return service.DeploymentRun{
		ID:               common.ID(httpRun.ID),
		Deployment:       common.ID(httpRun.DeploymentID),
		Owner:            httpRun.Owner,
		Start:            httpRun.Start,
		End:              httpRun.End,
		GitURL:           "", // no matching field to convert from
		CommitHash:       "", // no matching field to convert from
		Parameters:       HTTPParametersToService(meta, httpRun.Parameters),
		CloudCredentials: svcCloudCreds,
		GitCredential:    httpRun.GitCredentialID,
		Status:           httpRun.Status,
		LastState:        HTTPStateViewToService(httpRun.LastState),
		StateUpdatedAt:   time.Time{}, // no matching field to convert from
	}
}

// ServiceRunToHTTP converts the service representation of deployment run to HTTP
func ServiceRunToHTTP(svcRun service.DeploymentRun) hm.DeploymentRun {
	var httpCloudCreds []string
	if svcRun.CloudCredentials != nil {
		for cred := range svcRun.CloudCredentials {
			httpCloudCreds = append(httpCloudCreds, cred)
		}
	}
	return hm.DeploymentRun{
		ID:               svcRun.ID.String(),
		Owner:            svcRun.Owner,
		DeploymentID:     svcRun.Deployment.String(),
		BuildID:          "", // TODO Deployment build
		Parameters:       ServiceParametersToHTTP(svcRun.Parameters),
		CloudCredentials: httpCloudCreds,
		GitCredentialID:  svcRun.GitCredential,
		LastState:        ServiceStateViewToHTTP(svcRun.LastState),
		Status:           svcRun.Status,
		StatusMsg:        svcRun.StatusMsg,
		Start:            svcRun.Start,
		End:              svcRun.End,
	}
}

// HTTPParametersToService converts the HTTP representation of parameters to service
func HTTPParametersToService(meta service.TemplateMetadata, httpParameters []hm.KeyValue) []service.DeploymentParameter {
	var parameters = make([]service.DeploymentParameter, 0)
	if httpParameters == nil {
		return parameters
	}
	// TODO reconsider parameter value parsing.
	for _, keyVal := range httpParameters {
		parsedType, parsedValue, err := parseParamValueFromString(meta, keyVal.Key, keyVal.Value)
		if err != nil {
			// TODO: handle error
			continue
		}

		parameters = append(parameters, service.DeploymentParameter{
			Name:  keyVal.Key,
			Type:  parsedType,
			Value: parsedValue,
		})
	}
	return parameters
}

// HTTPParametersToServiceParameterValues converts the HTTP representation of parameters to service representation of parameter values
func HTTPParametersToServiceParameterValues(meta service.TemplateMetadata, httpParameters []hm.KeyValue) (service.DeploymentParameterValues, error) {
	var parameters = make(service.DeploymentParameterValues)
	if httpParameters == nil {
		return parameters, nil
	}

	for _, keyVal := range httpParameters {
		_, parsedValue, err := parseParamValueFromString(meta, keyVal.Key, keyVal.Value)
		if err != nil {
			return nil, err
		}

		parameters[keyVal.Key] = parsedValue
	}
	return parameters, nil
}

// getTemplateParamDef returns parameter definition in template metadata, returns type, default, enum
func getTemplateParamDef(meta service.TemplateMetadata, key string) (string, interface{}, []interface{}, error) {
	if param, hasParameterDef := meta.Parameters[key]; hasParameterDef {
		paramMap, isParameterDefMap := param.(map[string]interface{})
		if !isParameterDefMap {
			return "", nil, nil, service.NewCacaoInvalidParameterError("cannot type case parameters in template metadata to map")
		}

		// find data type
		dataType := ""
		dataTypeObj, hasDataType := paramMap["type"]
		if !hasDataType {
			return "", nil, nil, service.NewCacaoInvalidParameterError(fmt.Sprintf("cannot find data type for the parameter %s in template metadata", key))
		}

		dataType, isDataTypeString := dataTypeObj.(string)
		if isDataTypeString {
			// make it lowercase to handle easily
			dataType = strings.ToLower(dataType)
		} else {
			return "", nil, nil, service.NewCacaoMarshalError("type field value is not string")
		}

		// find default
		var defaultValue interface{}
		defaultParamValueObj, hasDefaultVal := paramMap["default"]
		defaultValue = nil

		// find enum
		var enum []interface{}
		enumObj, hasEnum := paramMap["enum"]
		enum = nil

		switch dataType {
		case "integer":
			if hasDefaultVal {
				defaultValueFloat, isDefaultValueFloat := defaultParamValueObj.(float64)
				if !isDefaultValueFloat {
					return "", nil, nil, service.NewCacaoMarshalError(fmt.Sprintf("field '%s' default value is not integer", key))
				}

				defaultValue = int64(math.Trunc(defaultValueFloat))
			}

			if hasEnum {
				enumArr, isEnumArr := enumObj.([]interface{})
				if !isEnumArr {
					return "", nil, nil, service.NewCacaoMarshalError(fmt.Sprintf("field '%s' enum values is not an array", key))
				}

				enum = []interface{}{}
				for _, enumVal := range enumArr {
					enumValFloat, isEnumValFloat := enumVal.(float64)
					if isEnumValFloat {
						return "", nil, nil, service.NewCacaoMarshalError(fmt.Sprintf("field '%s' enum value %v is not a number", key, enumVal))
					}
					enumValInt := int64(math.Trunc(enumValFloat))
					enum = append(enum, enumValInt)
				}
			}
		case "bool":
			if hasDefaultVal {
				defaultValueBool, isDefaultvalueBool := defaultParamValueObj.(bool)
				if !isDefaultvalueBool {
					return "", nil, nil, service.NewCacaoMarshalError(fmt.Sprintf("field '%s' default value is not bool", key))
				}

				defaultValue = defaultValueBool
			}
		case "float":
			if hasDefaultVal {
				defaultValueFloat, isDefaultValueFloat := defaultParamValueObj.(float64)
				if !isDefaultValueFloat {
					return "", nil, nil, service.NewCacaoMarshalError(fmt.Sprintf("field '%s' default value is not float", key))
				}

				defaultValue = defaultValueFloat
			}

			if hasEnum {
				enumArr, isEnumArr := enumObj.([]interface{})
				if !isEnumArr {
					return "", nil, nil, service.NewCacaoMarshalError(fmt.Sprintf("field '%s' enum values is not an array", key))
				}

				enum = []interface{}{}
				for _, enumVal := range enumArr {
					enumValFloat, isEnumValFloat := enumVal.(float64)
					if isEnumValFloat {
						return "", nil, nil, service.NewCacaoMarshalError(fmt.Sprintf("field '%s' enum value %v is not float", key, enumVal))
					}
					enum = append(enum, enumValFloat)
				}
			}
		default: // for string, string defined one
			if hasDefaultVal {
				defaultValueString, isDefaultValueString := defaultParamValueObj.(string)
				if !isDefaultValueString {
					return "", nil, nil, service.NewCacaoMarshalError(fmt.Sprintf("field '%s' default value is not string", key))
				}

				defaultValue = defaultValueString
			}

			if hasEnum {
				enumArr, isEnumArr := enumObj.([]interface{})
				if !isEnumArr {
					return "", nil, nil, service.NewCacaoMarshalError(fmt.Sprintf("field '%s' enum values is not an array", key))
				}

				enum = []interface{}{}
				for _, enumVal := range enumArr {
					enumValString, isEnumValString := enumVal.(string)
					if !isEnumValString {
						return "", nil, nil, service.NewCacaoMarshalError(fmt.Sprintf("field '%s' enum value %v is not string", key, enumVal))
					}
					enum = append(enum, enumValString)
				}
			}
		}

		return dataType, defaultValue, enum, nil
	}

	return "", nil, nil, service.NewCacaoInvalidParameterError(fmt.Sprintf("unknown parameter %s", key))
}

// getTemplateParamType returns data type of parameter defined in template metadata
func getTemplateParamType(meta service.TemplateMetadata, key string) (string, error) {
	dataType, _, _, err := getTemplateParamDef(meta, key)
	return dataType, err
}

// parseParamValueFromString ...
func parseParamValueFromString(meta service.TemplateMetadata, key string, val string) (string, interface{}, error) {
	dataType, defaultVal, enumVals, err := getTemplateParamDef(meta, key)
	if err != nil {
		return "", nil, err
	}

	var paramValue interface{}
	if defaultVal != nil {
		paramValue = defaultVal
	}

	if len(val) > 0 {
		switch dataType {
		case "integer":
			parseFloat, err := strconv.ParseFloat(val, 64)
			if err != nil {
				return "", nil, service.NewCacaoInvalidParameterError(fmt.Sprintf("unable to parse parameter %s to int", key))
			}

			paramValue = int64(math.Trunc(parseFloat))
		case "bool":
			parseBool, err := strconv.ParseBool(val)
			if err != nil {
				return "", nil, service.NewCacaoInvalidParameterError(fmt.Sprintf("unable to parse parameter %s to bool", key))
			}
			paramValue = parseBool
		case "float":
			parseFloat, err := strconv.ParseFloat(val, 64)
			if err != nil {
				return "", nil, service.NewCacaoInvalidParameterError(fmt.Sprintf("unable to parse parameter %s to float", key))
			}
			paramValue = parseFloat
		default:
			paramValue = val
		}
	}

	if len(enumVals) > 0 {
		// has enum val
		correctEnumValue := false
		for _, enumVal := range enumVals {
			if enumVal == val {
				// correct value
				correctEnumValue = true
				break
			}
		}

		if !correctEnumValue {
			return "", nil, service.NewCacaoInvalidParameterError(fmt.Sprintf("parameter value must be one of %v", enumVals))
		}
	}

	return dataType, paramValue, nil
}

// ServiceParametersToHTTP converts the service representation of parameters to  HTTP
func ServiceParametersToHTTP(svcParameters []service.DeploymentParameter) []hm.KeyValue {
	var parameters = make([]hm.KeyValue, 0)
	if svcParameters == nil {
		return parameters
	}
	for _, param := range svcParameters {
		var paramValueAsString string
		switch paramVal := param.Value.(type) {
		case string:
			paramValueAsString = paramVal
		//case int:
		//case float32:
		//case float64:
		//case bool:
		default:
			marshal, err := json.Marshal(param.Value)
			if err != nil {
				return nil
			}
			paramValueAsString = string(marshal)
		}
		parameters = append(parameters, hm.KeyValue{
			Key:   param.Name,
			Value: paramValueAsString,
		})
	}
	return parameters
}

// ServiceStateViewToHTTP converts the service representation of state view to HTTP
func ServiceStateViewToHTTP(svcView service.DeploymentStateView) hm.DeploymentStateView {
	var httpResources []hm.DeploymentResource
	if svcView.Resources != nil {
		httpResources = make([]hm.DeploymentResource, 0, len(svcView.Resources))
		for _, resource := range svcView.Resources {
			httpResources = append(httpResources, ServiceResourceToHTTP(resource))
		}
	}
	return hm.DeploymentStateView{
		Resources: httpResources,
	}
}

// HTTPStateViewToService  converts the HTTP representation of state view to service
func HTTPStateViewToService(httpView hm.DeploymentStateView) service.DeploymentStateView {
	var svcResources []service.DeploymentResource
	if httpView.Resources != nil {
		svcResources = make([]service.DeploymentResource, 0, len(httpView.Resources))
		for _, resource := range httpView.Resources {
			svcResources = append(svcResources, HTTPResourceToService(resource))
		}
	}
	return service.DeploymentStateView{
		Resources: svcResources,
	}
}

// HTTPResourceToService converts the HTTP representation of resource to service
func HTTPResourceToService(resource hm.DeploymentResource) service.DeploymentResource {
	var sensitiveAttributes []interface{}
	// TODO the type of SensitiveAttributes is different between http and service, need to figure out why or fix it.
	if resource.SensitiveAttributes != nil {
		attributes, ok := resource.SensitiveAttributes.([]interface{})
		if !ok {
			sensitiveAttributes = nil
		} else {
			sensitiveAttributes = attributes
		}
	}
	return service.DeploymentResource{
		ID:                  resource.ID,
		Type:                service.DeploymentResourceType(resource.Type),
		ProviderType:        resource.ProviderType,
		Provider:            common.ID(resource.Provider),
		Attributes:          resource.Attributes,
		SensitiveAttributes: sensitiveAttributes,
		AvailableActions:    []service.DeploymentResourceAction{},
	}
}

// ServiceResourceToHTTP converts the service representation of resource to HTTP
func ServiceResourceToHTTP(resource service.DeploymentResource) hm.DeploymentResource {
	var httpActions []string
	if resource.AvailableActions != nil {
		httpActions = make([]string, 0, len(resource.AvailableActions))
		for _, action := range resource.AvailableActions {
			httpActions = append(httpActions, string(action))
		}
	}
	return hm.DeploymentResource{
		ID:                  resource.ID,
		Type:                string(resource.Type),
		ProviderType:        resource.ProviderType,
		Provider:            resource.Provider.String(),
		Attributes:          resource.Attributes,
		SensitiveAttributes: resource.SensitiveAttributes,
		AvailableActions:    httpActions,
	}
}
