package deployments

import (
	"context"
	"fmt"
	cacaocommon "gitlab.com/cyverse/cacao-common/common"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
)

// Client is an interface for interacting with the Deployments microservice. Implementations of this interface
// should encapsulate information necessary to interact with the microservice, such as Nats and Stan connection
// information.
type Client interface {
	Session(actor, emulator string, isAdmin bool) (Session, error)
}

// ListOption is options for listing deployments
type ListOption struct {
	WorkspaceID      cacaocommon.ID
	CurrentStatus    string
	AllCurrentStatus bool
	Offset           int
	PageSize         int
	// Return full run object for the current run
	FullRun bool
}

// Session is an interface for interacting with the Deployments microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	ListDeployments(option ListOption) ([]hm.Deployment, error)
	ValidateDeploymentCreationRequest(creationRequest *hm.Deployment) error
	AddDeployment(creationRequest *hm.Deployment) (cacaocommon.ID, error)
	GetDeployment(deploymentID cacaocommon.ID) (hm.Deployment, error)
	ValidateDeploymentUpdateRequest(deploymentID cacaocommon.ID, deployment *hm.DeploymentUpdate) error
	UpdateDeployment(deploymentID cacaocommon.ID, deployment *hm.DeploymentUpdate) (cacaocommon.ID, error)
	ValidateDeploymentDeletionRequest(deploymentID cacaocommon.ID) error
	DeleteDeployment(deploymentID cacaocommon.ID) (cacaocommon.ID, bool, error)
	ListDeploymentBuilds(deploymentID cacaocommon.ID) ([]hm.DeploymentBuild, error)
	ValidateDeploymentRebuildRequest(deploymentID cacaocommon.ID, params *hm.Parameters) error
	RebuildDeployment(deploymentID cacaocommon.ID, params *hm.Parameters) (cacaocommon.ID, error)
	GetDeploymentBuild(deploymentID, buildID cacaocommon.ID) (hm.DeploymentBuild, error)
	ListDeploymentRuns(deploymentID cacaocommon.ID) ([]hm.DeploymentRun, error)
	ValidateDeploymentRunRequest(deploymentID cacaocommon.ID, runRequest *hm.DeploymentRun) error
	RunDeployment(deploymentID cacaocommon.ID, runRequest *hm.DeploymentRun) (cacaocommon.ID, error)
	GetDeploymentRun(deploymentID, runID cacaocommon.ID) (hm.DeploymentRun, error)
}

// deploymentsClient is the primary Client implementation.
type deploymentsClient struct {
	natsConfig *messaging.NatsConfig
	stanConfig *messaging.StanConfig
}

// New creates a new Deployments microservice client.
func New(natsConfig *messaging.NatsConfig, stanConfig *messaging.StanConfig) Client {
	return &deploymentsClient{
		natsConfig: natsConfig,
		stanConfig: stanConfig,
	}
}

// Session returns a new Deployments microservice client session.
func (c *deploymentsClient) Session(actor, emulator string, isAdmin bool) (Session, error) {

	// At a minimum, the actor must be specified.
	if actor == "" {
		return nil, fmt.Errorf("no actor specified")
	}

	// Define and return the session.
	session := deploymentsSession{
		natsConfig: c.natsConfig,
		stanConfig: c.stanConfig,
		actor:      actor,
		emulator:   emulator,
		isAdmin:    isAdmin,
	}
	return &session, nil
}

// deploymentsSession is the primary DeploymentsSession implementation.
type deploymentsSession struct {
	natsConfig *messaging.NatsConfig
	stanConfig *messaging.StanConfig
	actor      string
	emulator   string
	isAdmin    bool
}

func (s *deploymentsSession) getSvcClient() (service.DeploymentClient, error) {
	if s.natsConfig == nil {
		return nil, service.NewCacaoCommunicationError("nats config is nil, cannot create svc client")
	}
	if s.stanConfig == nil {
		return nil, service.NewCacaoCommunicationError("stan config is nil, cannot create svc client")
	}

	return service.NewDeploymentClient(context.TODO(), s.actor, s.emulator, *s.natsConfig, *s.stanConfig), nil
}

func (s *deploymentsSession) getTemplateSvcClient() (service.TemplateClient, error) {
	if s.natsConfig == nil {
		return nil, service.NewCacaoCommunicationError("nats config is nil, cannot create svc client")
	}
	if s.stanConfig == nil {
		return nil, service.NewCacaoCommunicationError("stan config is nil, cannot create svc client")
	}

	return service.NewNatsTemplateClient(context.TODO(), s.actor, s.emulator, *s.natsConfig, *s.stanConfig)
}

// ListDeployments obtains a list of deployments, optionally filtered by workspace ID.
func (s *deploymentsSession) ListDeployments(option ListOption) ([]hm.Deployment, error) {
	if option.WorkspaceID != "" {
		// check workspace ID only if it is provided
		if err := checkWorkspaceID(option.WorkspaceID); err != nil {
			return nil, err
		}
	}
	client, err := s.getSvcClient()
	if err != nil {
		return nil, err
	}
	result, err := client.Search(service.DeploymentListOption{
		SortBy:        service.SortByID,
		SortDirection: service.DescendingSort,
		Filter: service.DeploymentFilter{
			User:             s.actor,
			Workspace:        option.WorkspaceID,
			CurrentStatus:    service.DeploymentStatus(option.CurrentStatus),
			AllCurrentStatus: option.AllCurrentStatus,
		},
		Offset:        option.Offset,
		PageSize:      option.PageSize,
		FullRunObject: option.FullRun,
	})
	if err != nil {
		return nil, err
	}
	deploymentList := make([]hm.Deployment, 0, result.GetSize())
	for _, deployment := range result.GetDeployments() {
		deploymentList = append(deploymentList, ServiceDeploymentToHTTP(deployment))
	}
	return deploymentList, nil
}

// ValidateDeploymentCreationRequest checks a deployment creation request to ensure that it's valid.
func (s *deploymentsSession) ValidateDeploymentCreationRequest(creationRequest *hm.Deployment) error {
	if err := checkWorkspaceID(cacaocommon.ID(creationRequest.WorkspaceID)); err != nil {
		return err
	}
	if err := checkTemplateID(cacaocommon.ID(creationRequest.TemplateID)); err != nil {
		return err
	}
	if err := checkProviderID(cacaocommon.ID(creationRequest.PrimaryProviderID)); err != nil {
		return err
	}
	for _, cred := range creationRequest.CloudCredentials {
		if cred == "" {
			return service.NewCacaoInvalidParameterError("cloud credential ID cannot be empty")
		}
	}
	if len(creationRequest.CloudCredentials) == 0 {
		return service.NewCacaoInvalidParameterError("no cloud credential ID is provided")
	}
	return nil
}

// AddDeployment creates a new deployment.
func (s *deploymentsSession) AddDeployment(creationRequest *hm.Deployment) (cacaocommon.ID, error) {
	client, err := s.getSvcClient()
	if err != nil {
		return "", err
	}
	var svcCloudCreds = make(map[string]cacaocommon.ID)
	for _, credID := range creationRequest.CloudCredentials {
		// associate all credential in creation request to the primary provider
		svcCloudCreds[credID] = cacaocommon.ID(creationRequest.PrimaryProviderID)
	}
	deploymentID, err := client.Create(messaging.NewTransactionID(), service.DeploymentCreateParam{
		Name:             creationRequest.Name,
		Description:      creationRequest.Description,
		Workspace:        cacaocommon.ID(creationRequest.WorkspaceID),
		Template:         cacaocommon.ID(creationRequest.TemplateID),
		PrimaryCloud:     cacaocommon.ID(creationRequest.PrimaryProviderID),
		CloudProviders:   []cacaocommon.ID{}, // no other provider other than primary at present
		CloudCredentials: svcCloudCreds,
		GitCredential:    creationRequest.GitCredentialID,
	})
	if err != nil {
		return "", err
	}
	return deploymentID, nil
}

// GetDeployment returns the deployment with the given ID if it exists and the user has permission to view it.
func (s *deploymentsSession) GetDeployment(deploymentID cacaocommon.ID) (hm.Deployment, error) {
	if err := checkDeploymentID(deploymentID); err != nil {
		return hm.Deployment{}, err
	}
	client, err := s.getSvcClient()
	if err != nil {
		return hm.Deployment{}, err
	}
	deployment, err := client.Get(deploymentID)
	if err != nil {
		return hm.Deployment{}, err
	}
	if deployment == nil {
		return hm.Deployment{}, service.NewCacaoNotFoundError("returned deployment is nil")
	}
	httpDeployment := ServiceDeploymentToHTTP(*deployment)
	return httpDeployment, nil
}

// ValidateDeploymentUpdateRequest checks a deployment parameter update request to ensure that it's valid.
func (s *deploymentsSession) ValidateDeploymentUpdateRequest(
	deploymentID cacaocommon.ID, updateRequest *hm.DeploymentUpdate,
) error {
	if err := checkDeploymentID(deploymentID); err != nil {
		return err
	}
	if updateRequest.Name != nil && len(*updateRequest.Name) > 100 {
		return service.NewCacaoInvalidParameterError("deployment name too long")
	}
	if updateRequest.Description != nil && len(*updateRequest.Description) > 255 {
		return service.NewCacaoInvalidParameterError("deployment description too long")
	}
	if updateRequest.WorkspaceID != nil && !cacaocommon.ID(*updateRequest.WorkspaceID).Validate() {
		return service.NewCacaoInvalidParameterError("deployment has bad workspace id")
	}
	if updateRequest.TemplateID != nil && !cacaocommon.ID(*updateRequest.TemplateID).Validate() {
		return service.NewCacaoInvalidParameterError("deployment has bad template id")
	}
	if updateRequest.PrimaryProviderID != nil && !cacaocommon.ID(*updateRequest.PrimaryProviderID).Validate() {
		return service.NewCacaoInvalidParameterError("deployment has bad primary provider id")
	}
	if len(updateRequest.Parameters) > 0 {
		for _, keyValPair := range updateRequest.Parameters {
			if keyValPair.Key == "" {
				return service.NewCacaoInvalidParameterError("parameter name cannot be empty")
			}
		}
	}
	return nil
}

// UpdateDeployment updates parameters for an existing deployment.
func (s *deploymentsSession) UpdateDeployment(
	deploymentID cacaocommon.ID, updateRequest *hm.DeploymentUpdate,
) (cacaocommon.ID, error) {
	// TODO: deployment microservices have not implemented this
	//return "", service.NewCacaoNotImplementedError("not implemented")
	client, err := s.getSvcClient()
	if err != nil {
		return "", err
	}
	deployment, updateFields := s.deploymentUpdateFields(deploymentID, updateRequest)
	runID, err := client.Update(messaging.NewTransactionID(), deployment, updateFields)
	if err != nil {
		return "", err
	}
	return runID, nil
}

func (s *deploymentsSession) deploymentUpdateFields(
	deploymentID cacaocommon.ID, updateRequest *hm.DeploymentUpdate,
) (deployment service.Deployment, fields []string) {
	deployment.ID = deploymentID
	fields = make([]string, 0)
	if updateRequest.Owner != nil {
		deployment.Owner = *updateRequest.Owner
		fields = append(fields, "owner")
	}
	if updateRequest.Name != nil {
		deployment.Name = *updateRequest.Name
		fields = append(fields, "name")
	}
	if updateRequest.Description != nil {
		deployment.Description = *updateRequest.Description
		fields = append(fields, "description")
	}
	if updateRequest.WorkspaceID != nil {
		deployment.Workspace = cacaocommon.ID(*updateRequest.WorkspaceID)
		fields = append(fields, "workspace_id")
	}
	if updateRequest.TemplateID != nil {
		deployment.Template = cacaocommon.ID(*updateRequest.TemplateID)
		fields = append(fields, "template_id")
	}
	if updateRequest.PrimaryProviderID != nil {
		deployment.PrimaryCloud = cacaocommon.ID(*updateRequest.PrimaryProviderID)
		fields = append(fields, "primary_provider_id")
	}
	if updateRequest.Parameters != nil {
		// TODO update parameter should be done via this call
	}
	if updateRequest.CloudCredentials != nil {
		for _, credID := range updateRequest.CloudCredentials {
			// note, provider ID is left as blank
			deployment.CloudCredentials[credID] = ""
		}
		fields = append(fields, "cloud_credentials")
	}
	if updateRequest.GitCredentialID != nil {
		deployment.GitCredential = *updateRequest.GitCredentialID
		fields = append(fields, "git_credential_id")
	}
	return
}

// ValidateDeploymentDeletionRequest checks a deployment deletion request to ensure that it's valid.
func (s *deploymentsSession) ValidateDeploymentDeletionRequest(deploymentID cacaocommon.ID) error {
	if err := checkDeploymentID(deploymentID); err != nil {
		return err
	}
	return nil
}

// DeleteDeployment deletes an existing deployment.
func (s *deploymentsSession) DeleteDeployment(deploymentID cacaocommon.ID) (cacaocommon.ID, bool, error) {
	client, err := s.getSvcClient()
	if err != nil {
		return "", false, err
	}
	deleted, err := client.Delete(messaging.NewTransactionID(), deploymentID)
	if err != nil {
		return "", false, err
	}
	return deploymentID, deleted, nil
}

// ListDeploymentBuilds lists the builds for the given deployment.
// TODO: implement me
func (s *deploymentsSession) ListDeploymentBuilds(deploymentID cacaocommon.ID) ([]hm.DeploymentBuild, error) {
	if err := checkDeploymentID(deploymentID); err != nil {
		return nil, err
	}
	return nil, service.NewCacaoNotImplementedError("not implemented")
}

// ValidateDeploymentRebuildRequest checks a deployment rebuild request to ensure that it's valid.
// TODO: implement me
func (s *deploymentsSession) ValidateDeploymentRebuildRequest(
	deploymentID cacaocommon.ID, params *hm.Parameters,
) error {
	if err := checkDeploymentID(deploymentID); err != nil {
		return err
	}
	return service.NewCacaoNotImplementedError("not implemented")
}

// RebuildDeployment rebuilds an existing deployment.
// TODO: implement me
func (s *deploymentsSession) RebuildDeployment(
	deploymentID cacaocommon.ID, params *hm.Parameters,
) (cacaocommon.ID, error) {
	return "", service.NewCacaoNotImplementedError("not implemented")
}

// GetDeploymentBuild returns information about a single deployment build.
// TODO: implement me
func (s *deploymentsSession) GetDeploymentBuild(deploymentID, buildID cacaocommon.ID) (hm.DeploymentBuild, error) {
	err := checkDeploymentID(deploymentID)
	if err != nil {
		return hm.DeploymentBuild{}, err
	}
	// TODO check build ID
	return hm.DeploymentBuild{}, service.NewCacaoNotImplementedError("not implemented")
}

// ListDeploymentRuns lists the runs for the given deployment.
func (s *deploymentsSession) ListDeploymentRuns(deploymentID cacaocommon.ID) ([]hm.DeploymentRun, error) {
	err := checkDeploymentID(deploymentID)
	if err != nil {
		return nil, err
	}
	client, err := s.getSvcClient()
	if err != nil {
		return nil, err
	}
	runList, err := client.SearchRun(service.DeploymentRunListOption{
		Deployment: deploymentID,
		Offset:     0,
		PageSize:   -1,
	})
	if err != nil {
		return nil, err
	}
	if runList.GetSize() == 0 {
		return []hm.DeploymentRun{}, nil
	}
	var httpRunList = make([]hm.DeploymentRun, 0, runList.GetSize())
	for _, run := range runList.GetRuns() {
		httpRunList = append(httpRunList, ServiceRunToHTTP(run))
	}
	return httpRunList, nil
}

// ValidateDeploymentRunRequest checks a deployment run request to ensure that it's valid.
func (s *deploymentsSession) ValidateDeploymentRunRequest(
	deploymentID cacaocommon.ID, runRequest *hm.DeploymentRun,
) error {
	if err := checkDeploymentID(deploymentID); err != nil {
		return err
	}
	if err := checkDeploymentID(cacaocommon.ID(runRequest.DeploymentID)); err != nil {
		return err
	}
	return nil
}

// RunDeployment launches a new deployment run.
func (s *deploymentsSession) RunDeployment(
	deploymentID cacaocommon.ID, runRequest *hm.DeploymentRun,
) (cacaocommon.ID, error) {
	client, err := s.getSvcClient()
	if err != nil {
		return "", err
	}

	templateClient, err := s.getTemplateSvcClient()
	if err != nil {
		return "", err
	}

	deployment, err := client.Get(deploymentID)
	if err != nil {
		return "", err
	}

	if !deployment.Template.Validate() {
		return "", service.NewCacaoInvalidParameterError("templateID is not valid")
	}

	template, err := templateClient.Get(deployment.Template)
	if err != nil {
		return "", err
	}

	meta := template.GetMetadata()

	parameterValues, err := HTTPParametersToServiceParameterValues(meta, runRequest.Parameters)
	if err != nil {
		return "", err
	}

	run, err := client.CreateRun(messaging.NewTransactionID(), service.DeploymentRunCreateParam{
		Deployment:  deploymentID,
		ParamValues: parameterValues,
	})
	if err != nil {
		return "", err
	}
	return run.ID, nil
}

// GetDeploymentRun gets information about an existing deployment run.
func (s *deploymentsSession) GetDeploymentRun(deploymentID, runID cacaocommon.ID) (hm.DeploymentRun, error) {
	if err := checkDeploymentID(deploymentID); err != nil {
		return hm.DeploymentRun{}, err
	}
	if err := checkRunID(runID); err != nil {
		return hm.DeploymentRun{}, err
	}
	client, err := s.getSvcClient()
	if err != nil {
		return hm.DeploymentRun{}, err
	}
	run, err := client.GetRun(deploymentID, runID)
	if err != nil {
		return hm.DeploymentRun{}, err
	}
	httpRun := ServiceRunToHTTP(*run)
	return httpRun, nil
}

func checkDeploymentID(id cacaocommon.ID) error {
	if !id.Validate() {
		return service.NewCacaoInvalidParameterError("deployment ID is not valid")
	}
	if id.Prefix() != service.DeploymentIDPrefix {
		return service.NewCacaoInvalidParameterError("deployment ID is not valid")
	}
	return nil
}

func checkRunID(id cacaocommon.ID) error {
	if !id.Validate() {
		return service.NewCacaoInvalidParameterError("run ID is not valid")
	}
	if id.Prefix() != service.RunIDPrefix {
		return service.NewCacaoInvalidParameterError("run ID is not valid")
	}
	return nil
}

func checkWorkspaceID(id cacaocommon.ID) error {
	if !id.Validate() {
		return service.NewCacaoInvalidParameterError("workspace ID is not valid")
	}
	if id.Prefix() != "workspace" {
		return service.NewCacaoInvalidParameterError("workspace ID is not valid")
	}
	return nil
}

func checkTemplateID(id cacaocommon.ID) error {
	if !id.Validate() {
		return service.NewCacaoInvalidParameterError("template ID is not valid")
	}
	if id.Prefix() != "template" {
		return service.NewCacaoInvalidParameterError("template ID is not valid")
	}
	return nil
}

func checkProviderID(id cacaocommon.ID) error {
	if !id.Validate() {
		return service.NewCacaoInvalidParameterError("provider ID is not valid")
	}
	if id.Prefix() != "provider" {
		return service.NewCacaoInvalidParameterError("provider ID is not valid")
	}
	return nil
}
