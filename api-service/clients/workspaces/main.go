package workspaces

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// Client is an interface for interacting with the Workspace microservice. Implementations of this interface
// should encapsulate information necessary to interact with the microservice, such as Nats and Stan connection
// information.
type Client interface {
	Session(actor, emulator string, isAdmin bool) (Session, error)
}

// Session is an interface for interacting with the Workspace microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	ListWorkspaces() ([]cacao_common_http.Workspace, error)
	GetWorkspace(workspaceID cacao_common.ID) (cacao_common_http.Workspace, error)

	CreateWorkspace(creationRequest cacao_common_http.Workspace) (cacao_common.ID, error)
	ValidateWorkspaceCreationRequest(creationRequest cacao_common_http.Workspace) error
	UpdateWorkspace(workspaceID cacao_common.ID, updateRequest cacao_common_http.Workspace) (cacao_common.ID, error)
	ValidateWorkspaceUpdateRequest(workspaceID cacao_common.ID, updateRequest cacao_common_http.Workspace) error
	UpdateWorkspaceFields(workspaceID cacao_common.ID, updateRequest cacao_common_http.Workspace, updateFields []string) (cacao_common.ID, error)
	ValidateWorkspaceUpdateFieldsRequest(workspaceID cacao_common.ID, updateRequest cacao_common_http.Workspace, updateFields []string) error
	DeleteWorkspace(workspaceID cacao_common.ID) (cacao_common.ID, error)
	ValidateWorkspaceDeletionRequest(workspaceID cacao_common.ID) error
}

// workspacesClient is the primary Client implementation.
type workspacesClient struct {
	natsConfig cacao_common_messaging.NatsConfig
	stanConfig cacao_common_messaging.StanConfig
}

// New creates a new Workspaces microservice client.
func New(natsConfig cacao_common_messaging.NatsConfig, stanConfig cacao_common_messaging.StanConfig) Client {
	return &workspacesClient{
		natsConfig: natsConfig,
		stanConfig: stanConfig,
	}
}

// Session returns a new Workspaces microservice client session.
func (c *workspacesClient) Session(actor string, emulator string, isAdmin bool) (Session, error) {
	// At a minimum, the actor must be specified.
	if actor == "" {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("no actor specified")
	}

	// Define and return the session.
	session := workspacesSession{
		natsConfig: c.natsConfig,
		stanConfig: c.stanConfig,
		actor:      actor,
		emulator:   emulator,
		isAdmin:    isAdmin,
		context:    context.Background(),
	}
	return &session, nil
}

// workspacesSession is the primary WorkspacesSession implementation.
type workspacesSession struct {
	natsConfig cacao_common_messaging.NatsConfig
	stanConfig cacao_common_messaging.StanConfig
	actor      string
	emulator   string
	isAdmin    bool
	context    context.Context
}

func (s *workspacesSession) convertToHTTPObject(obj cacao_common_service.Workspace) cacao_common_http.Workspace {
	return cacao_common_http.Workspace{
		ID:                obj.GetID(),
		Owner:             obj.GetOwner(),
		Name:              obj.GetName(),
		Description:       obj.GetDescription(),
		DefaultProviderID: obj.GetDefaultProviderID(),
		CreatedAt:         obj.GetCreatedAt(),
		UpdatedAt:         obj.GetUpdatedAt(),
	}
}

func (s *workspacesSession) convertToServiceObject(obj cacao_common_http.Workspace) cacao_common_service.Workspace {
	return &cacao_common_service.WorkspaceModel{
		ID:                obj.ID,
		Owner:             obj.Owner,
		Name:              obj.Name,
		Description:       obj.Description,
		DefaultProviderID: obj.DefaultProviderID,
	}
}

// ListWorkspaces obtains a list of workspaces.
func (s *workspacesSession) ListWorkspaces() ([]cacao_common_http.Workspace, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListWorkspaces",
	})

	serviceClient, err := cacao_common_service.NewNatsWorkspaceClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a workspace client"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	workspaces, err := serviceClient.List()
	if err != nil {
		msg := "failed to list workspaces"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	// convert to http object
	httpObjects := make([]cacao_common_http.Workspace, 0, len(workspaces))
	for _, workspace := range workspaces {
		httpObject := s.convertToHTTPObject(workspace)
		httpObjects = append(httpObjects, httpObject)
	}

	return httpObjects, nil
}

// GetWorkspace returns the workspace with the given ID if it exists and the user has permission to view it.
func (s *workspacesSession) GetWorkspace(workspaceID cacao_common.ID) (cacao_common_http.Workspace, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "GetWorkspace",
	})

	serviceClient, err := cacao_common_service.NewNatsWorkspaceClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a workspace client"
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.Workspace{}, err
	}

	workspace, err := serviceClient.Get(workspaceID)
	if err != nil {
		msg := fmt.Sprintf("failed to get the workspace for ID %s", workspaceID)
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.Workspace{}, err
	}

	// convert to http object
	httpObject := s.convertToHTTPObject(workspace)
	return httpObject, nil
}

// CreateWorkspace creates a new workspace.
func (s *workspacesSession) CreateWorkspace(creationRequest cacao_common_http.Workspace) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "CreateWorkspace",
	})

	serviceClient, err := cacao_common_service.NewNatsWorkspaceClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a workspace client"
		logger.WithField("error", err).Error(msg)
		return cacao_common.ID(""), err
	}

	// convert to service object
	serviceObject := s.convertToServiceObject(creationRequest)

	tid, err := serviceClient.Create(serviceObject)
	if err != nil {
		msg := "failed to create a workspace"
		logger.WithField("error", err).Error(msg)
		return cacao_common.ID(""), err
	}

	return tid, nil
}

// ValidateWorkspaceCreationRequest checks a workspace creation request to ensure that it's valid.
func (s *workspacesSession) ValidateWorkspaceCreationRequest(creationRequest cacao_common_http.Workspace) error {
	if len(creationRequest.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("workspace name is not valid")
	}

	return nil
}

// UpdateWorkspace updates the workspace with the given ID.
func (s *workspacesSession) UpdateWorkspace(workspaceID cacao_common.ID, updateRequest cacao_common_http.Workspace) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateWorkspace",
	})

	serviceClient, err := cacao_common_service.NewNatsWorkspaceClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a workspace client"
		logger.WithField("error", err).Error(msg)
		return cacao_common.ID(""), err
	}

	// convert to service object
	serviceObject := s.convertToServiceObject(updateRequest)
	serviceObject.SetID(workspaceID)

	err = serviceClient.Update(serviceObject)
	if err != nil {
		msg := fmt.Sprintf("failed to update the workspace for ID %s", workspaceID)
		logger.WithField("error", err).Error(msg)
		return cacao_common.ID(""), err
	}

	return workspaceID, nil
}

// ValidateWorkspaceUpdateRequest checks a workspace update request to ensure that it's valid.
func (s *workspacesSession) ValidateWorkspaceUpdateRequest(workspaceID cacao_common.ID, updateRequest cacao_common_http.Workspace) error {
	if !workspaceID.Validate() {
		return cacao_common_service.NewCacaoInvalidParameterError("workspace ID is not valid")
	}

	if len(updateRequest.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("workspace name is not valid")
	}

	return nil
}

// UpdateWorkspaceFields updates fields of the workspace with the given ID.
func (s *workspacesSession) UpdateWorkspaceFields(workspaceID cacao_common.ID, updateRequest cacao_common_http.Workspace, updateFields []string) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateWorkspaceFields",
	})

	serviceClient, err := cacao_common_service.NewNatsWorkspaceClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a workspace client"
		logger.WithField("error", err).Error(msg)
		return cacao_common.ID(""), err
	}

	// convert to service object
	serviceObject := s.convertToServiceObject(updateRequest)
	serviceObject.SetID(workspaceID)

	err = serviceClient.UpdateFields(serviceObject, updateFields)
	if err != nil {
		msg := fmt.Sprintf("failed to update fields of the workspace for ID %s", workspaceID)
		logger.WithField("error", err).Error(msg)
		return cacao_common.ID(""), err
	}

	return workspaceID, nil
}

// ValidateWorkspaceUpdateFieldsRequest checks a workspace update request to ensure that it's valid.
func (s *workspacesSession) ValidateWorkspaceUpdateFieldsRequest(workspaceID cacao_common.ID, updateRequest cacao_common_http.Workspace, updateFields []string) error {
	if !workspaceID.Validate() {
		return cacao_common_service.NewCacaoInvalidParameterError("workspace ID is not valid")
	}

	for _, field := range updateFields {
		switch field {
		case "name":
			if len(updateRequest.Name) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("workspace name is not valid")
			}
		default:
			// pass
		}
	}

	return nil
}

// DeleteWorkspace deletes an existing workspace.
func (s *workspacesSession) DeleteWorkspace(workspaceID cacao_common.ID) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "DeleteWorkspace",
	})

	serviceClient, err := cacao_common_service.NewNatsWorkspaceClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a workspace client"
		logger.WithField("error", err).Error(msg)
		return cacao_common.ID(""), err
	}

	err = serviceClient.Delete(workspaceID)
	if err != nil {
		msg := fmt.Sprintf("failed to delete the workspace for ID %s", workspaceID)
		logger.WithField("error", err).Error(msg)
		return cacao_common.ID(""), err
	}

	return workspaceID, nil
}

// ValidateWorkspaceDeletionRequest checks a workspace delete request to ensure that it's valid.
func (s *workspacesSession) ValidateWorkspaceDeletionRequest(workspaceID cacao_common.ID) error {
	if !workspaceID.Validate() {
		return cacao_common_service.NewCacaoInvalidParameterError("workspace ID is not valid")
	}
	return nil
}
