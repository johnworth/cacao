package templates

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	cc "gitlab.com/cyverse/cacao/api-service/clients/common"
)

// Client is an interface for interacting with the Template microservice. Implementations of this interface
// should encapsulate information necessary to interact with the microservice, such as Nats and Stan connection
// information.
type Client interface {
	Session(actor, emulator string, isAdmin bool) (Session, error)
}

// Session is an interface for interacting with the Template microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	ListTemplateTypes() ([]cacao_common_http.TemplateType, error)
	ListTemplateTypesForProvideType(providerType cacao_common_service.TemplateProviderType) ([]cacao_common_http.TemplateType, error)
	GetTemplateType(templateTypeName string) (cacao_common_http.TemplateType, error)

	CreateTemplateType(creationRequest cacao_common_http.TemplateType) (string, error)
	ValidateTemplateTypeCreationRequest(creationRequest cacao_common_http.TemplateType) error
	UpdateTemplateType(templateTypeName string, updateRequest cacao_common_http.TemplateType) (string, error)
	ValidateTemplateTypeUpdateRequest(templateTypeName string, updateRequest cacao_common_http.TemplateType) error
	UpdateTemplateTypeFields(templateTypeName string, updateRequest cacao_common_http.TemplateType, updateFields []string) (string, error)
	ValidateTemplateTypeUpdateFieldsRequest(templateTypeName string, updateRequest cacao_common_http.TemplateType, updateFields []string) error
	DeleteTemplateType(templateTypeName string) (string, error)
	ValidateTemplateTypeDeletionRequest(templateTypeName string) error

	ListTemplateSourceTypes() ([]cacao_common_service.TemplateSourceType, error)

	ListTemplates() ([]cacao_common_http.Template, error)
	GetTemplate(templateID cacao_common.ID) (cacao_common_http.Template, error)

	ImportTemplate(importRequest cacao_common_http.Template, credentialID string) (cacao_common.ID, error)
	ValidateTemplateImportRequest(importRequest cacao_common_http.Template, credentialID string) error
	UpdateTemplate(templateID cacao_common.ID, updateRequest cacao_common_http.Template) (cacao_common.ID, error)
	ValidateTemplateUpdateRequest(templateID cacao_common.ID, updateRequest cacao_common_http.Template) error
	UpdateTemplateFields(templateID cacao_common.ID, updateRequest cacao_common_http.Template, updateFields []string) (cacao_common.ID, error)
	ValidateTemplateUpdateFieldsRequest(templateID cacao_common.ID, updateRequest cacao_common_http.Template, updateFields []string) error
	DeleteTemplate(templateID cacao_common.ID) (cacao_common.ID, error)
	ValidateTemplateDeletionRequest(templateID cacao_common.ID) error
	SyncTemplate(templateID cacao_common.ID, credentialID string) (cacao_common.ID, error)
	ValidateTemplateSyncRequest(templateID cacao_common.ID, credentialID string) error
}

// templatesClient is the primary Client implementation.
type templatesClient struct {
	natsConfig cacao_common_messaging.NatsConfig
	stanConfig cacao_common_messaging.StanConfig
}

// New creates a new Templates microservice client.
func New(natsConfig cacao_common_messaging.NatsConfig, stanConfig cacao_common_messaging.StanConfig) Client {
	return &templatesClient{
		natsConfig: natsConfig,
		stanConfig: stanConfig,
	}
}

// Session returns a new Templates microservice client session.
func (c *templatesClient) Session(actor string, emulator string, isAdmin bool) (Session, error) {
	// At a minimum, the actor must be specified.
	if actor == "" {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("no actor specified")
	}

	// Define and return the session.
	session := templatesSession{
		natsConfig: c.natsConfig,
		stanConfig: c.stanConfig,
		actor:      actor,
		emulator:   emulator,
		isAdmin:    isAdmin,
		context:    context.Background(),
	}
	return &session, nil
}

// templatesSession is the primary TemplatesSession implementation.
type templatesSession struct {
	natsConfig cacao_common_messaging.NatsConfig
	stanConfig cacao_common_messaging.StanConfig
	actor      string
	emulator   string
	isAdmin    bool
	context    context.Context
}

func (s *templatesSession) convertTemplateTypeToHTTPObject(obj cacao_common_service.TemplateType) cacao_common_http.TemplateType {
	return cacao_common_http.TemplateType{
		Name:          obj.GetName(),
		Formats:       obj.GetFormats(),
		Engine:        obj.GetEngine(),
		ProviderTypes: obj.GetProviderTypes(),
	}
}

func (s *templatesSession) convertTemplateTypeToServiceObject(obj cacao_common_http.TemplateType) cacao_common_service.TemplateType {
	return &cacao_common_service.TemplateTypeModel{
		Name:          obj.Name,
		Formats:       obj.Formats,
		Engine:        obj.Engine,
		ProviderTypes: obj.ProviderTypes,
	}
}

func (s *templatesSession) convertTemplateToHTTPObject(obj cacao_common_service.Template) cacao_common_http.Template {
	return cacao_common_http.Template{
		ID:          obj.GetID(),
		Owner:       obj.GetOwner(),
		Name:        obj.GetName(),
		Description: obj.GetDescription(),
		Public:      obj.IsPublic(),
		Source:      obj.GetSource(),
		Metadata:    obj.GetMetadata(),
		CreatedAt:   obj.GetCreatedAt(),
		UpdatedAt:   obj.GetUpdatedAt(),
	}
}

func (s *templatesSession) convertTemplateToServiceObject(obj cacao_common_http.Template) cacao_common_service.Template {
	return &cacao_common_service.TemplateModel{
		ID:          obj.ID,
		Owner:       obj.Owner,
		Name:        obj.Name,
		Description: obj.Description,
		Public:      obj.Public,
		Source:      obj.Source,
	}
}

// ListTemplateTypes obtains a list of template types.
func (s *templatesSession) ListTemplateTypes() ([]cacao_common_http.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListTemplateTypes",
	})

	serviceClient, err := cacao_common_service.NewNatsTemplateClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a template client"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	templateTypes, err := serviceClient.ListTypes()
	if err != nil {
		msg := "failed to list template types"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	// convert to http object
	httpObjects := make([]cacao_common_http.TemplateType, 0, len(templateTypes))
	for _, templateType := range templateTypes {
		httpObject := s.convertTemplateTypeToHTTPObject(templateType)
		httpObjects = append(httpObjects, httpObject)
	}

	return httpObjects, nil
}

// ListTemplateTypesForProvideType obtains a list of template types that support the given provider type.
func (s *templatesSession) ListTemplateTypesForProvideType(providerType cacao_common_service.TemplateProviderType) ([]cacao_common_http.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListTemplateTypesForProvideType",
	})

	serviceClient, err := cacao_common_service.NewNatsTemplateClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a template client"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	templateTypes, err := serviceClient.ListTypesForProviderType(providerType)
	if err != nil {
		msg := fmt.Sprintf("failed to list template types for provider type %s", providerType)
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	// convert to http object
	httpObjects := make([]cacao_common_http.TemplateType, 0, len(templateTypes))
	for _, templateType := range templateTypes {
		httpObject := s.convertTemplateTypeToHTTPObject(templateType)
		httpObjects = append(httpObjects, httpObject)
	}

	return httpObjects, nil
}

// GetTemplateType returns the template type with the given name if it exists.
func (s *templatesSession) GetTemplateType(templateTypeName string) (cacao_common_http.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "GetTemplateType",
	})

	serviceClient, err := cacao_common_service.NewNatsTemplateClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a template client"
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.TemplateType{}, err
	}

	templateType, err := serviceClient.GetType(templateTypeName)
	if err != nil {
		msg := fmt.Sprintf("failed to get the template type for %s", templateTypeName)
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.TemplateType{}, err
	}

	// convert to http object
	httpObject := s.convertTemplateTypeToHTTPObject(templateType)
	return httpObject, nil
}

// CreateTemplateType creates a new template type.
func (s *templatesSession) CreateTemplateType(creationRequest cacao_common_http.TemplateType) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "CreateTemplateType",
	})

	serviceClient, err := cacao_common_service.NewNatsTemplateClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a template client"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	// convert to service object
	serviceObject := s.convertTemplateTypeToServiceObject(creationRequest)

	err = serviceClient.CreateType(serviceObject)
	if err != nil {
		msg := "failed to create a template type"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return creationRequest.Name, nil
}

// ValidateTemplateTypeCreationRequest checks a template type creation request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateTypeCreationRequest(creationRequest cacao_common_http.TemplateType) error {
	if len(creationRequest.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("type name is not valid")
	}

	if len(creationRequest.Formats) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("formats are not valid")
	}

	if len(creationRequest.Engine) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("engine is not valid")
	}

	if len(creationRequest.Engine) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("provider types are not valid")
	}

	return nil
}

// UpdateTemplateType updates the template type with the given name.
func (s *templatesSession) UpdateTemplateType(templateTypeName string, updateRequest cacao_common_http.TemplateType) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateTemplateType",
	})

	serviceClient, err := cacao_common_service.NewNatsTemplateClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a template client"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	// convert to service object
	serviceObject := s.convertTemplateTypeToServiceObject(updateRequest)
	serviceObject.SetName(templateTypeName)

	err = serviceClient.UpdateType(serviceObject)
	if err != nil {
		msg := fmt.Sprintf("failed to update the template type for %s", templateTypeName)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return templateTypeName, nil
}

// ValidateTemplateTypeUpdateRequest checks a template type update request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateTypeUpdateRequest(templateTypeName string, updateRequest cacao_common_http.TemplateType) error {
	if len(templateTypeName) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("type name is not valid")
	}

	if len(updateRequest.Formats) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("formats are not valid")
	}

	if len(updateRequest.Engine) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("engine is not valid")
	}

	if len(updateRequest.Engine) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("provider types are not valid")
	}

	return nil
}

// UpdateTemplateTypeFields updates fields of the template type with the given name.
func (s *templatesSession) UpdateTemplateTypeFields(templateTypeName string, updateRequest cacao_common_http.TemplateType, updateFields []string) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateTemplateTypeFields",
	})

	serviceClient, err := cacao_common_service.NewNatsTemplateClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a template client"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	// convert to service object
	serviceObject := s.convertTemplateTypeToServiceObject(updateRequest)
	serviceObject.SetName(templateTypeName)

	err = serviceClient.UpdateTypeFields(serviceObject, updateFields)
	if err != nil {
		msg := fmt.Sprintf("failed to update fields of the template type for %s", templateTypeName)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return templateTypeName, nil
}

// ValidateTemplateTypeUpdateFieldsRequest checks a template type update request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateTypeUpdateFieldsRequest(templateTypeName string, updateRequest cacao_common_http.TemplateType, updateFields []string) error {
	if len(templateTypeName) == 0 {
		return cc.ErrBadRequest
	}

	for _, field := range updateFields {
		switch field {
		case "formats":
			if len(updateRequest.Formats) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("formats are not valid")
			}
		case "engine":
			if len(updateRequest.Engine) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("engine is not valid")
			}
		case "provider_types":
			if len(updateRequest.ProviderTypes) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("provider types are not valid")
			}
		default:
			// pass
		}
	}

	return nil
}

// DeleteTemplateType deletes an existing template type.
func (s *templatesSession) DeleteTemplateType(templateTypeName string) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "DeleteTemplateType",
	})

	serviceClient, err := cacao_common_service.NewNatsTemplateClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a template type client"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	err = serviceClient.DeleteType(templateTypeName)
	if err != nil {
		msg := fmt.Sprintf("failed to delete the template type for %s", templateTypeName)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return templateTypeName, nil
}

// ValidateTemplateTypeDeletionRequest checks a template type delete request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateTypeDeletionRequest(templateTypeName string) error {
	if len(templateTypeName) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("type name is not valid")
	}

	return nil
}

// ListTemplateSourceTypes obtains a list of template source types.
func (s *templatesSession) ListTemplateSourceTypes() ([]cacao_common_service.TemplateSourceType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListTemplateSourceTypes",
	})

	serviceClient, err := cacao_common_service.NewNatsTemplateClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a template client"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	templateSourceTypes, err := serviceClient.ListSourceTypes()
	if err != nil {
		msg := "failed to list template source types"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	return templateSourceTypes, nil
}

// ListTemplates obtains a list of templates.
func (s *templatesSession) ListTemplates() ([]cacao_common_http.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListTemplates",
	})

	serviceClient, err := cacao_common_service.NewNatsTemplateClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a template client"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	templates, err := serviceClient.List()
	if err != nil {
		msg := "failed to list templates"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	// convert to http object
	httpObjects := make([]cacao_common_http.Template, 0, len(templates))
	for _, template := range templates {
		httpObject := s.convertTemplateToHTTPObject(template)
		httpObjects = append(httpObjects, httpObject)
	}

	return httpObjects, nil
}

// GetTemplate returns the template with the given name if it exists.
func (s *templatesSession) GetTemplate(templateID cacao_common.ID) (cacao_common_http.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "GetTemplate",
	})

	serviceClient, err := cacao_common_service.NewNatsTemplateClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a template client"
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.Template{}, err
	}

	template, err := serviceClient.Get(templateID)
	if err != nil {
		msg := fmt.Sprintf("failed to get the template for ID %s", templateID)
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.Template{}, err
	}

	// convert to http object
	httpObject := s.convertTemplateToHTTPObject(template)
	return httpObject, nil
}

// ImportTemplate imports a template.
func (s *templatesSession) ImportTemplate(importRequest cacao_common_http.Template, credentialID string) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ImportTemplate",
	})

	serviceClient, err := cacao_common_service.NewNatsTemplateClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a template client"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	// convert to service object
	serviceObject := s.convertTemplateToServiceObject(importRequest)

	tid, err := serviceClient.Import(serviceObject, credentialID)
	if err != nil {
		msg := "failed to import a template"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return tid, nil
}

// ValidateTemplateImportRequest checks a template import request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateImportRequest(importRequest cacao_common_http.Template, credentialID string) error {
	if len(importRequest.Source.URI) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template source URI is not valid")
	}

	if len(importRequest.Source.Type) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template source type is not valid")
	}

	// requires credential ID if source visibility is private
	if importRequest.Source.Visibility == cacao_common_service.TemplateSourceVisibilityPrivate {
		if len(credentialID) == 0 {
			return cacao_common_service.NewCacaoInvalidParameterError("credential ID is not valid")
		}
	}

	return nil
}

// UpdateTemplate updates the template with the given name.
func (s *templatesSession) UpdateTemplate(templateID cacao_common.ID, updateRequest cacao_common_http.Template) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateTemplate",
	})

	serviceClient, err := cacao_common_service.NewNatsTemplateClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a template client"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	// convert to service object
	serviceObject := s.convertTemplateToServiceObject(updateRequest)
	serviceObject.SetID(templateID)

	err = serviceClient.Update(serviceObject)
	if err != nil {
		msg := fmt.Sprintf("failed to update the template for ID %s", templateID)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return templateID, nil
}

// ValidateTemplateUpdateRequest checks a template update request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateUpdateRequest(templateID cacao_common.ID, updateRequest cacao_common_http.Template) error {
	if !templateID.Validate() {
		return cacao_common_service.NewCacaoInvalidParameterError("template ID is not valid")
	}

	if len(updateRequest.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template name is not valid")
	}

	if len(updateRequest.Source.URI) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template source URI is not valid")
	}

	if len(updateRequest.Source.Type) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template source type is not valid")
	}

	return nil
}

// UpdateTemplateFields updates fields of the template with the given name.
func (s *templatesSession) UpdateTemplateFields(templateID cacao_common.ID, updateRequest cacao_common_http.Template, updateFields []string) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateTemplateFields",
	})

	serviceClient, err := cacao_common_service.NewNatsTemplateClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a template client"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	// convert to service object
	serviceObject := s.convertTemplateToServiceObject(updateRequest)
	serviceObject.SetID(templateID)

	err = serviceClient.UpdateFields(serviceObject, updateFields)
	if err != nil {
		msg := fmt.Sprintf("failed to update fields of the template for ID %s", templateID)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return templateID, nil
}

// ValidateTemplateUpdateFieldsRequest checks a template update request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateUpdateFieldsRequest(templateID cacao_common.ID, updateRequest cacao_common_http.Template, updateFields []string) error {
	if !templateID.Validate() {
		return cacao_common_service.NewCacaoInvalidParameterError("template ID is not valid")
	}

	for _, field := range updateFields {
		switch field {
		case "name":
			if len(updateRequest.Name) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("template name is not valid")
			}
		case "source":
			if len(updateRequest.Source.URI) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("template source URI is not valid")
			}

			if len(updateRequest.Source.Type) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("template source type is not valid")
			}
		default:
			// pass
		}
	}

	return nil
}

// DeleteTemplate deletes an existing template.
func (s *templatesSession) DeleteTemplate(templateID cacao_common.ID) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "DeleteTemplate",
	})

	serviceClient, err := cacao_common_service.NewNatsTemplateClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a template type client"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	err = serviceClient.Delete(templateID)
	if err != nil {
		msg := fmt.Sprintf("failed to delete the template for ID %s", templateID)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return templateID, nil
}

// ValidateTemplateDeletionRequest checks a template delete request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateDeletionRequest(templateID cacao_common.ID) error {
	if !templateID.Validate() {
		return cacao_common_service.NewCacaoInvalidParameterError("template ID is not valid")
	}

	return nil
}

// SyncTemplate syncs the template with the given name.
func (s *templatesSession) SyncTemplate(templateID cacao_common.ID, credentialID string) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "SyncTemplate",
	})

	serviceClient, err := cacao_common_service.NewNatsTemplateClient(s.context, s.actor, s.emulator, s.natsConfig, s.stanConfig)
	if err != nil {
		msg := "failed to create a template client"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	err = serviceClient.Sync(templateID, credentialID)
	if err != nil {
		msg := fmt.Sprintf("failed to sync the template for ID %s", templateID)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return templateID, nil
}

// ValidateTemplateSyncRequest checks a template sync request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateSyncRequest(templateID cacao_common.ID, credentialID string) error {
	if !templateID.Validate() {
		return cacao_common_service.NewCacaoInvalidParameterError("template ID is not valid")
	}

	// credentialID may be empty if the source repo is public

	return nil
}
