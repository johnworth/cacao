package config

import (
	"context"

	"gitlab.com/cyverse/cacao-common/messaging"
)

// Config - holds the configurable constants for the microservice
type Config struct {
	Nats       messaging.NatsConfig
	Stan       messaging.StanConfig
	AppContext context.Context `ignored:"true"`
	LogLevel   string          `default:"debug" split_words:"true"`

	KeycloakURL          string `default:"http://keycloak:8080/auth/realms/cacao" split_words:"true"`
	KeycloakRedirectURL  string `default:"http://api:8080/user/login/callback" split_words:"true"`
	KeycloakClientID     string `default:"cacao-client" split_words:"true"`
	KeycloakClientSecret string `split_words:"true"`
	KeycloakHmacSecret   string `default:"secret" split_words:"true"`

	GlobusOAuthURL     string   `default:"https://auth.globus.org/v2/oauth2" split_words:"true"`
	GlobusRedirectURL  string   `default:"http://api:8080/user/login/callback" split_words:"true"`
	GlobusClientID     string   `split_words:"true"`
	GlobusClientSecret string   `split_words:"true"`
	GlobusScopes       []string `default:"profile,email" split_words:"true"`
	GlobusHmacSecret   string   `default:"secret" split_words:"true"`

	// If enabled, will activate the autocreation of users upon login; default = false
	AutoCreateUser bool `default:"false" split_words:"true"`

	// AuthDriver how to auth, choices are "keycloak", "globus" or "simpletoken"
	AuthDriver string `default:"simpletoken" split_words:"true"`

	// SimpleTokenUsername is the username to use, if using the SimpleToken authentication driver
	SimpleTokenUsername string `default:"" split_words:"true"`

	// SimpleTokenToken is the token to associate with a given username
	SimpleTokenToken string `default:"cacaosimpletoken" split_words:"true"`

	// SimpleTokenStandalone if true will not require nats, mongo, or users microservice to authentication/validate
	SimpleTokenStandalone bool `default:"false" split_words:"true"`

	//	CacaoDirectory path to where cacao was cloned by GIT, currently only used by Version Check API
	CacaoDirectory string `default:"/opt/cacao"`
}

// GlobalConfig conains the configuration that can be shared across calls
// It should be cautioned that this should be initialized once, upon start of api service and not changed
// this is not thread-safe approach for mutable fields
var GlobalConfig Config
