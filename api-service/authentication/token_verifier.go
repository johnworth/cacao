package authentication

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	fp "github.com/amonsat/fullname_parser"
	log "github.com/sirupsen/logrus"
)

// UserProfile contains information about an authorized user.
type UserProfile struct {
	Username   string
	FirstName  string
	LastName   string
	Email      string
	Attributes map[string]interface{}
}

// AccessTokenVerifier is the primary interface used for verifying that an access token is legitimate.
type AccessTokenVerifier interface {

	// Verify accepts a single access token and validates it. If the token is valid, user profile information will be
	// returned. Otherwise, an error will be returned.
	Verify(accessToken string) (*UserProfile, error)
}

// RFC7662AccessTokenVerifier validates OAuth2 access tokens according to RFC 7662.
type RFC7662AccessTokenVerifier struct {
	tokenIntrospectionURL string
	usernameClaimNames    []string
	fullNameClaimNames    []string
	firstNameClaimNames   []string
	lastNameClaimNames    []string
	emailClaimsNames      []string
}

// NewRFC7662AccessTokenVerifier returns a new access token verifier. Note that the claim names used to obtain
// attribute for the user profile are currently hard-coded. They're included in the struct itself in order to
// make it easy to customize them later if we need to.
func NewRFC7662AccessTokenVerifier(introspectionURL string) *RFC7662AccessTokenVerifier {
	return &RFC7662AccessTokenVerifier{
		tokenIntrospectionURL: introspectionURL,
		usernameClaimNames:    []string{"username", "preferred_username"},
		fullNameClaimNames:    []string{"name"},
		firstNameClaimNames:   []string{"given_name"},
		lastNameClaimNames:    []string{"last_name"},
		emailClaimsNames:      []string{"email"},
	}
}

// extractAttribute takes a map form a parsed token introspection response along with a list of claim names. It
// returns the value of the first claim that exists in the map and whose value is a string or something that can be
// converted to a string easily.
func (v *RFC7662AccessTokenVerifier) extractAttribute(claims map[string]interface{}, claimNames []string) string {
	for _, claimName := range claimNames {
		rawClaimValue, ok := claims[claimName]
		if ok {
			switch str := rawClaimValue.(type) {
			case string:
				return str
			case fmt.Stringer:
				return str.String()
			}
		}
	}
	return ""
}

// Verify verifies that an access token is valid using the method described in RFC 7662.
func (v *RFC7662AccessTokenVerifier) Verify(accessToken string) (*UserProfile, error) {
	logger := log.WithFields(log.Fields{
		"package":  "authentication",
		"function": "RFC7662AccessTokenVerifier.Verify",
		"url":      v.tokenIntrospectionURL,
	})
	logger.Trace("start")

	// Build the request body.
	values := &url.Values{}
	values.Set("token", accessToken)
	body := values.Encode()

	// Send the request.
	resp, err := http.Post(v.tokenIntrospectionURL, "application/x-www-form-urlencoded", strings.NewReader(body))
	if err != nil {
		logger.WithField("error", err).Error("token introspection failed")
		return nil, err
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		msg := fmt.Sprintf("unexpected status code in token introspection response: %d", resp.StatusCode)
		logger.Error(msg)
		return nil, fmt.Errorf(msg)
	}

	// Read and parse the response body.
	responseBodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		msg := "unable to read the token introspection response body"
		log.WithField("error", err).Error(msg)
		return nil, err
	}
	var claims map[string]interface{}
	err = json.Unmarshal(responseBodyBytes, &claims)
	if err != nil {
		msg := "unable to parse the token introspection response body"
		log.WithFields(log.Fields{"error": err, "response": responseBodyBytes}).Error(msg)
		return nil, err
	}

	// If the token is inactive then the user needs to reauthorize.
	active, ok := claims["active"]
	if !ok {
		msg := "the token introspection response did not contain the active flag"
		log.Error(msg)
		return nil, fmt.Errorf(msg)
	}
	switch isActive := active.(type) {
	case bool:
		if !isActive {
			msg := "invalid or expired access token"
			log.WithField("claims", claims).Debug(msg)
			return nil, fmt.Errorf(msg)
		}
	default:
		msg := "invalid value for active flag"
		log.WithField("claims", claims).Debug(msg)
		return nil, fmt.Errorf(msg)
	}

	// The username has to be present in the response for authorization to succeed.
	username := v.extractAttribute(claims, v.usernameClaimNames)
	if username == "" {
		msg := "no username claim found in the token introspection response"
		log.WithField("claims", claims).Error(msg)
		return nil, fmt.Errorf(msg)
	}

	// Get the name fields.
	firstName := v.extractAttribute(claims, v.firstNameClaimNames)
	lastName := v.extractAttribute(claims, v.lastNameClaimNames)

	// If the first and last names aren't provided separately, try to guess them from the full name. I'm very
	// skeptical that we can always accurately determine the first and last names, so we may want to remove this
	// code if it doesn't work as intended.
	if firstName == "" && lastName == "" {
		name := fp.ParseFullname(v.extractAttribute(claims, v.fullNameClaimNames))
		firstName = name.First
		lastName = name.Last
	}

	// Build the user profile from the response.
	profile := &UserProfile{
		Username:   username,
		FirstName:  firstName,
		LastName:   lastName,
		Email:      v.extractAttribute(claims, v.emailClaimsNames),
		Attributes: claims,
	}

	return profile, nil
}
