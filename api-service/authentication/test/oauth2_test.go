package authentication

import (
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"testing/iotest"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/authentication"
	"gitlab.com/cyverse/cacao/api-service/authentication/mocks"
	"gitlab.com/cyverse/cacao/api-service/config"
	"golang.org/x/oauth2"
)

// A constant for the CSRF cookie name.
const csrfCookieName = "cacao_csrf"

// Define profiles for an admin user and a non-admin user for testing.
var adminUserProfile = &authentication.UserProfile{
	Username:   "admin-user",
	FirstName:  "Admin",
	LastName:   "User",
	Email:      "admin@example.org",
	Attributes: map[string]interface{}{"hasSuperheroCape": true},
}
var regularUserProfile = &authentication.UserProfile{
	Username:   "regular-user",
	FirstName:  "Regular",
	LastName:   "User",
	Email:      "regular@example.org",
	Attributes: map[string]interface{}{"hasSuperheroCape": false},
}
var unknownUserProfile = &authentication.UserProfile{
	Username:   "unknown-user",
	FirstName:  "Unknown",
	LastName:   "User",
	Email:      "unknown@example.org",
	Attributes: map[string]interface{}{"hasSuperheroCape": false, "incognito": true},
}

// Define an admin user and a non-admin user for testing.
var adminUser = &service.UserModel{
	Username:     "admin-user",
	FirstName:    "Admin",
	LastName:     "User",
	PrimaryEmail: "admin@example.org",
	IsAdmin:      true,
}
var regularUser = &service.UserModel{
	Username:     "regular-user",
	FirstName:    "Regular",
	LastName:     "User",
	PrimaryEmail: "regular@example.org",
	IsAdmin:      false,
}

// oauth2AuthenticationTest defines a single test of OAuth2.Auhenticate.
type oauth2AuthenticationTest struct {
	description  string
	authHeader   string
	expectedUser service.User
	expectedErr  error
}

// testConfig contains the configuration to use for testing this driver.
var testConfig = config.Config{
	GlobusOAuthURL:     "https://auth.globus.org/v2/oauth2",
	GlobusRedirectURL:  "http://localhost:8080/user/login/callback",
	GlobusClientID:     "some-client-id",
	GlobusClientSecret: "some-client-secret",
	GlobusHmacSecret:   "some-hmac-secret",
	AuthDriver:         "globus",
}

func TestOAuth2Authenticate(t *testing.T) {

	// Create a mock access token verifier for testing.
	tokenVerifier := new(mocks.AccessTokenVerifier)
	tokenVerifier.On("Verify", "admin-token").
		Return(adminUserProfile, nil)
	tokenVerifier.On("Verify", "regular-token").
		Return(regularUserProfile, nil)
	tokenVerifier.On("Verify", "bogus-token").
		Return(nil, fmt.Errorf("invalid access token: bogus-token"))
	tokenVerifier.On("Verify", "unknown-token").
		Return(unknownUserProfile, nil)

	// Create a mock user resolver for testing.
	userResolver := new(mocks.UserResolver)
	userResolver.On(
		"GetOrCreateUser",
		adminUserProfile.Username,
		adminUserProfile.Email,
		adminUserProfile.FirstName,
		adminUserProfile.LastName,
		false,
	).Return(adminUser, nil)
	userResolver.On(
		"GetOrCreateUser",
		regularUserProfile.Username,
		regularUserProfile.Email,
		regularUserProfile.FirstName,
		regularUserProfile.LastName,
		false,
	).Return(regularUser, nil)
	userResolver.On(
		"GetOrCreateUser",
		unknownUserProfile.Username,
		unknownUserProfile.Email,
		unknownUserProfile.FirstName,
		unknownUserProfile.LastName,
		false,
	).Return(nil, fmt.Errorf("unknown user: unknown-user"))

	// Initialize the authentication driver.
	authDriver := authentication.NewOAuth2Driver(
		&oauth2.Config{}, tokenVerifier, userResolver, testConfig.GlobusHmacSecret, false,
	)
	if authDriver == nil {
		t.Fatal("NewOAuth2Driver returned nil")
	}

	// Define the test cases.
	authenticationTests := []oauth2AuthenticationTest{
		{
			description:  "no authorization header",
			authHeader:   "",
			expectedUser: nil,
			expectedErr:  fmt.Errorf("there was no authorization information in the request"),
		},
		{
			description:  "incorrect token type",
			authHeader:   "Foo Bar",
			expectedUser: nil,
			expectedErr:  fmt.Errorf("unsupported token type: Foo"),
		},
		{
			description:  "invalid token",
			authHeader:   "Bearer bogus-token",
			expectedUser: nil,
			expectedErr:  fmt.Errorf("invalid access token: bogus-token"),
		},
		{
			description:  "known user",
			authHeader:   "Bearer admin-token",
			expectedUser: adminUser,
			expectedErr:  nil,
		},
		{
			description:  "known user with lower case token type",
			authHeader:   "bearer regular-token",
			expectedUser: regularUser,
			expectedErr:  nil,
		},
		{
			description:  "user not known to CACAO",
			authHeader:   "Bearer unknown-token",
			expectedUser: nil,
			expectedErr:  fmt.Errorf("unknown user: unknown-user"),
		},
	}

	// Run each test.
	for _, test := range authenticationTests {
		t.Run(test.description, func(t *testing.T) {
			assert := assert.New(t)

			// Build the headers.
			header := http.Header{}
			if test.authHeader != "" {
				header.Set("Authorization", test.authHeader)
			}

			// Check the authorization.
			user, err := authDriver.Authenticate(header)
			assert.Equal(test.expectedUser, user)
			assert.Equal(test.expectedErr, err)
		})
	}
}

// getCSRFCookie returns the CSRF cookie from an HTTP response if it was set.
func getCSRFCookie(res *http.Response) *http.Cookie {
	for _, cookie := range res.Cookies() {
		if cookie.Name == csrfCookieName {
			return cookie
		}
	}
	return nil
}

// encodeState encodes the state for the given cookie value. If the cookie state is empty, an empty string is returned.
// The HMAC secret in the test configuration is used to hash the cookie value.
func encodeState(cookieValue string) string {
	var state string
	if cookieValue != "" {
		mac := hmac.New(sha256.New, []byte(testConfig.GlobusHmacSecret))
		mac.Write([]byte(cookieValue))
		state = hex.EncodeToString(mac.Sum(nil))
	}
	return state
}

func checkRedirectURL(assert *assert.Assertions, res *http.Response, expectedCookieValue string) {

	// Verify that we received the expected URL path in the redirect.
	redirectURL, err := res.Location()
	assert.NoError(err)
	assert.Equal("auth.globus.org", redirectURL.Host)
	assert.Equal("/v2/oauth2/authorize", redirectURL.Path)

	// Verify that the State query parameter is present in the location URL.
	state := redirectURL.Query().Get("state")
	assert.NotEmpty(state)

	// Verify that the CSRF cookie was set.
	csrfCookie := getCSRFCookie(res)
	assert.NotNil(csrfCookie)

	// We can't do the remaining checks if the CSRF cookie wasn't set.
	if csrfCookie == nil {
		return
	}

	// Check the cookie value, if it's expected to have a specific value.
	if expectedCookieValue != "" {
		assert.Equal(expectedCookieValue, csrfCookie.Value)
	}

	// Verify that the CSRF cookie matches the state.
	assert.Equal(encodeState(csrfCookie.Value), state)
}

// oauth2LoginTest defines a single test of OAuth2.login.
type oauth2LoginTest struct {
	description         string
	expectedCookieValue string
}

func TestLogin(t *testing.T) {

	// Define the test cases.
	loginTests := []oauth2LoginTest{
		{
			description:         "without preexisting cookie",
			expectedCookieValue: "",
		},
		{
			description:         "with preexisting cookie",
			expectedCookieValue: "ekky.ekky.ekky.zbang.zoom.boing.znourringmn",
		},
	}

	// Run each test.
	for _, test := range loginTests {
		t.Run(test.description, func(t *testing.T) {
			assert := assert.New(t)

			// Create the HTTP request.
			req, err := http.NewRequest("GET", "/user/login", nil)
			assert.NoError(err)

			// Add the CSRF cookie if there's an expected cookie value.
			if test.expectedCookieValue != "" {
				req.AddCookie(&http.Cookie{Name: csrfCookieName, Value: test.expectedCookieValue})
			}

			// Process the request.
			driver, err := authentication.NewGlobusOAuth2Driver(testConfig)
			assert.NoError(err)
			recorder := httptest.NewRecorder()
			router := mux.NewRouter()
			driver.AddRoutes(router)
			router.ServeHTTP(recorder, req)

			// Verify that we got the expected response status code.
			assert.Equal(http.StatusFound, recorder.Code)

			// Verify that we got the expected redirect URL.
			checkRedirectURL(assert, recorder.Result(), test.expectedCookieValue)
		})
	}
}

// maybeSetQueryParam sets a query parameter in a URL to the first non-empty value in the list of values. If all
// values are empty, the query parameter is not set.
func maybeSetQueryParam(urlToUpdate *url.URL, name string, values ...string) {
	for _, value := range values {
		if value != "" {
			if urlToUpdate.RawQuery != "" {
				urlToUpdate.RawQuery = fmt.Sprintf("%s&%s=%s", urlToUpdate.RawQuery, name, url.QueryEscape(value))
			} else {
				urlToUpdate.RawQuery = fmt.Sprintf("%s=%s", name, url.QueryEscape(value))
			}
			return
		}
	}
}

// oauth2CallbackTest defines a single test of OAuth2.callback.
type oauth2CallbackTest struct {
	description    string
	cookieValue    string
	stateValue     string
	code           string
	exchangeError  error
	expectedStatus int
	token          *oauth2.Token
}

func TestCallback(t *testing.T) {

	// Define the test cases.
	callbackTests := []oauth2CallbackTest{
		{
			description:    "missing CSRF cookie",
			expectedStatus: http.StatusBadRequest,
		},
		{
			description:    "missing state parameter",
			cookieValue:    "the foo is in the bar, probably getting drunk",
			expectedStatus: http.StatusBadRequest,
		},
		{
			description:    "invalid state parameter",
			cookieValue:    "the-value",
			stateValue:     "not-the-value",
			expectedStatus: http.StatusBadRequest,
		},
		{
			description:    "missing authorization code",
			cookieValue:    "the-value",
			stateValue:     "the-value",
			expectedStatus: http.StatusBadRequest,
		},
		{
			description:    "token exchange failure",
			cookieValue:    "the-value",
			stateValue:     "the-value",
			code:           "the-code",
			exchangeError:  fmt.Errorf("something bad happened"),
			expectedStatus: http.StatusInternalServerError,
		},
		{
			description:    "token exchange success",
			cookieValue:    "the-value",
			stateValue:     "the-value",
			code:           "the-code",
			token:          &oauth2.Token{AccessToken: "the-token", TokenType: "Bearer"},
			expectedStatus: http.StatusOK,
		},
	}

	// Run each of the tests.
	for _, test := range callbackTests {
		t.Run(test.description, func(t *testing.T) {
			assert := assert.New(t)

			// Create the mock OAuth2 config.
			oauth2Config := new(mocks.OAuth2Config)
			if test.exchangeError != nil {
				oauth2Config.On("Exchange", context.Background(), test.code).
					Return(nil, test.exchangeError)
			} else if test.token != nil {
				oauth2Config.On("Exchange", context.Background(), test.code).
					Return(test.token, nil)
			}

			// Begin building the requestURL.
			requestURL, err := url.Parse(testConfig.GlobusRedirectURL)
			assert.NoError(err)

			// Add the query parameters.
			maybeSetQueryParam(requestURL, "state", encodeState(test.stateValue))
			maybeSetQueryParam(requestURL, "code", test.code)

			// Create the HTTP request.
			req, err := http.NewRequest("GET", requestURL.String(), nil)
			assert.NoError(err)

			// Add the CSRF cookie if there's an expected cookie value.
			if test.cookieValue != "" {
				req.AddCookie(&http.Cookie{Name: csrfCookieName, Value: test.cookieValue})
			}

			// Process the request.
			driver := authentication.NewOAuth2Driver(oauth2Config, nil, nil, testConfig.GlobusHmacSecret, false)
			recorder := httptest.NewRecorder()
			router := mux.NewRouter()
			driver.AddRoutes(router)
			router.ServeHTTP(recorder, req)

			// Verify that we got the expected status code.
			assert.Equal(test.expectedStatus, recorder.Code)

			// Verify that we got the expected response body if the call was successful.
			if test.token != nil {
				expectedResponse, err := json.Marshal(test.token)
				assert.NoError(err)
				assert.Equal(expectedResponse, recorder.Body.Bytes())
			}
		})
	}
}

type postLoginTest struct {
	description    string
	enabled        bool
	readError      error
	requestBody    string
	queryString    string
	exchangeError  error
	token          *oauth2.Token
	expectedStatus int
}

func TestPostLogin(t *testing.T) {

	// Define the test cases.
	postLoginTests := []postLoginTest{
		{
			description:    "post logins disabled",
			enabled:        false,
			expectedStatus: http.StatusNotImplemented,
		},
		{
			description:    "body read error",
			enabled:        true,
			readError:      fmt.Errorf("something bad happened"),
			expectedStatus: http.StatusBadRequest,
		},
		{
			description:    "body parse error",
			enabled:        true,
			requestBody:    "%% I dare you to parse this",
			expectedStatus: http.StatusBadRequest,
		},
		{
			description:    "missing username param",
			enabled:        true,
			requestBody:    "password=the-password",
			expectedStatus: http.StatusBadRequest,
		},
		{
			description:    "missing password param",
			enabled:        true,
			requestBody:    "username=the-username",
			expectedStatus: http.StatusBadRequest,
		},
		{
			description:    "password credentials token exchange error",
			enabled:        true,
			requestBody:    "username=the-username&password=the-password",
			exchangeError:  fmt.Errorf("something bad happened"),
			expectedStatus: http.StatusBadRequest,
		},
		{
			description:    "password credentials token exchange success",
			enabled:        true,
			requestBody:    "username=the-username&password=the-password",
			token:          &oauth2.Token{AccessToken: "the-token", TokenType: "Bearer"},
			expectedStatus: http.StatusOK,
		},
		{
			description:    "credentials in query string",
			enabled:        true,
			queryString:    "username=the-username&password=the-password",
			token:          &oauth2.Token{AccessToken: "the-token", TokenType: "Bearer"},
			expectedStatus: http.StatusOK,
		},
	}

	// Run each of the tests.
	for _, test := range postLoginTests {
		t.Run(test.description, func(t *testing.T) {
			assert := assert.New(t)

			// Create the reader for the HTTP request.
			var reader io.Reader
			if test.readError != nil {
				reader = iotest.ErrReader(test.readError)
			} else {
				reader = strings.NewReader(test.requestBody)
			}

			// Build the request URL.
			requestURL, err := url.Parse("http://localhost/user/login")
			assert.NoError(err)
			if test.queryString != "" {
				requestURL.RawQuery = test.queryString
			}

			// Create the HTTP request.
			req, err := http.NewRequest("POST", requestURL.String(), reader)
			req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
			assert.NoError(err)

			// Extract the username and password.
			var username, password string
			formString := test.requestBody
			if formString == "" {
				formString = test.queryString
			}
			params, err := url.ParseQuery(formString)
			if err == nil {
				username = params.Get("username")
				password = params.Get("password")
			}

			// Create the mock OAuth2 config.
			oauth2Config := new(mocks.OAuth2Config)
			if test.exchangeError != nil {
				oauth2Config.On("PasswordCredentialsToken", context.Background(), username, password).
					Return(nil, test.exchangeError)
			} else if test.token != nil {
				oauth2Config.On("PasswordCredentialsToken", context.Background(), username, password).
					Return(test.token, nil)
			}

			// Process the request.
			driver := authentication.NewOAuth2Driver(oauth2Config, nil, nil, testConfig.GlobusHmacSecret, test.enabled)
			recorder := httptest.NewRecorder()
			router := mux.NewRouter()
			driver.AddRoutes(router)
			router.ServeHTTP(recorder, req)

			// Verify that we got the expected status code.
			assert.Equal(test.expectedStatus, recorder.Code)

			// Verify that we got the expected response body if applicable.
			if test.token != nil {
				expectedResponse, err := json.Marshal(test.token)
				assert.NoError(err)
				assert.Equal(expectedResponse, recorder.Body.Bytes())
			}
		})
	}
}
