package authentication

import (
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/config"
)

// UserResolver can be used to get information about an authenticated user.
type UserResolver interface {

	// GetOrCreateUser returns information about an authenticated user. If the user does not exist and automatic user
	// creation is enabled, a new user will be created and returned. Only the username is used for lookup. The rest of
	// the parameters are only used when a user is being created automatically.
	GetOrCreateUser(username, email, firstname, lastname string, isAdmin bool) (service.User, error)
}

// DefaultUserResolver provides the default user resolver implementation, which uses BaseNatsDriver to
// get or record user information.
type DefaultUserResolver struct {
	BaseNatsDriver
}

// NewDefaultUserResolver returns the default implementation of the UserResolver interface.
func NewDefaultUserResolver(c config.Config) *DefaultUserResolver {
	return &DefaultUserResolver{
		BaseNatsDriver: BaseNatsDriver{
			context:        c.AppContext,
			natsConfig:     c.Nats,
			stanConfig:     c.Stan,
			autoCreateUser: c.AutoCreateUser,
		},
	}
}

// GetOrCreateUser returns information about an authenticated user. If the user does not exist and automatic user
// creation is enabled, a new user will be created and returned. Only the username is used for lookup. The rest of
// the parameters are only used when a user is being created automatically.
func (resolver *DefaultUserResolver) GetOrCreateUser(
	username, email, firstname, lastname string, isAdmin bool,
) (service.User, error) {
	return resolver.getOrCreateUser(username, email, firstname, lastname, isAdmin)
}
