package authentication

import (
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"github.com/gorilla/mux"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/config"
	"gitlab.com/cyverse/cacao/api-service/utils"
	"golang.org/x/oauth2"
)

// A constant for the CSRF cookie name.
const csrfCookieName = "cacao_csrf"

// OAuth2 is the OAuth2 driver.
type OAuth2 struct {
	oauthConfig     OAuth2Config
	tokenVerifier   AccessTokenVerifier
	userResolver    UserResolver
	csrfCookieName  string
	hmacSecret      []byte
	enablePostLogin bool
}

// NewOAuth2Driver returns a new OAuth2 authentication driver without any OAuth configuration settings. This is a
// lower-level function, primarily intended to be used either internally or for unit testing.
func NewOAuth2Driver(
	oauth2Config OAuth2Config,
	tokenVerifier AccessTokenVerifier,
	userResolver UserResolver,
	hmacSecret string,
	enablePostLogin bool,
) *OAuth2 {
	return &OAuth2{
		oauthConfig:     oauth2Config,
		tokenVerifier:   tokenVerifier,
		userResolver:    userResolver,
		csrfCookieName:  csrfCookieName,
		hmacSecret:      []byte(hmacSecret),
		enablePostLogin: enablePostLogin,
	}
}

// NewGlobusOAuth2Driver returns an OAuth2 authentication driver for use with Globus.
func NewGlobusOAuth2Driver(c config.Config) (*OAuth2, error) {
	oauthURL := strings.TrimRight(c.GlobusOAuthURL, "/")

	// Create the OAuth2 endpoint struct.
	endpoint := oauth2.Endpoint{
		AuthURL:  oauthURL + "/authorize",
		TokenURL: oauthURL + "/token",
	}

	// Build the token introspection URL.
	introspectionURL, err := url.Parse(oauthURL)
	if err != nil {
		return nil, err
	}
	introspectionURL.Path = introspectionURL.Path + "/token/introspect"
	introspectionURL.User = url.UserPassword(c.GlobusClientID, c.GlobusClientSecret)

	// Define the driver instance.
	driver := &OAuth2{
		oauthConfig: &oauth2.Config{
			ClientID:     c.GlobusClientID,
			ClientSecret: c.GlobusClientSecret,
			RedirectURL:  c.GlobusRedirectURL,
			Endpoint:     endpoint,
		},

		tokenVerifier:   NewRFC7662AccessTokenVerifier(introspectionURL.String()),
		userResolver:    NewDefaultUserResolver(c),
		csrfCookieName:  csrfCookieName,
		hmacSecret:      []byte(c.GlobusHmacSecret),
		enablePostLogin: false,
	}

	return driver, nil
}

// AddRoutes adds additional routes to the gorilla mux router.
func (o *OAuth2) AddRoutes(router *mux.Router) {
	log.Trace("OAuth2.AddRoutes() start")

	router.HandleFunc("/user/login", o.login).Methods("GET")
	router.HandleFunc("/user/login", o.postLogin).Methods("POST")
	router.HandleFunc("/user/login/callback", o.callback).Methods("GET")
}

// Authenticate performs the authentication incoming requests.
func (o *OAuth2) Authenticate(header http.Header) (service.User, error) {
	logger := log.WithFields(log.Fields{"package": "authentication", "function": "Oauth2.Authenticate"})
	logger.Trace("start")

	// Extract the access token.
	token, err := o.getAccessToken(header)
	if err != nil {
		logger.WithField("error", err).Trace("unable to get the access token")
		return nil, err
	}

	// Validate the access token.
	profile, err := o.tokenVerifier.Verify(token)
	if err != nil {
		logger.WithField("error", err).Trace("access token validation failed")
		return nil, err
	}

	// Look up the user.
	user, err := o.userResolver.GetOrCreateUser(
		profile.Username, profile.Email, profile.FirstName, profile.LastName, false,
	)
	if err != nil {
		logger.WithField("error", err).Trace("user resolution failed")
		return nil, err
	}

	return user, nil
}

// getAccessToken extracts the access token from the HTTP headers.
func (o *OAuth2) getAccessToken(header http.Header) (string, error) {

	// Get the authorization header value.
	authHeaderValue := header.Get("Authorization")
	if authHeaderValue == "" {
		return "", fmt.Errorf("there was no authorization information in the request")
	}

	// Extract the token type and the token.
	var tokenType, token string
	re := regexp.MustCompile(`\s+`)
	fields := re.Split(authHeaderValue, 2)
	tokenType = fields[0]
	if len(fields) > 1 {
		token = fields[1]
	}

	// Validate the token type.
	if strings.ToLower(tokenType) != "bearer" {
		return "", fmt.Errorf("unsupported token type: %s", tokenType)
	}

	return token, nil
}

// login is the handler for the GET /user/login enpoint, which begins the Authorization Grant flow.
func (o OAuth2) login(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{"package": "authentication", "function": "OAuth2.login"})
	logger.Trace("start")

	// Add the CSRF cookie to the response.
	cookie, err := r.Cookie(o.csrfCookieName)
	if err != nil {
		logger.WithField("error", err).Debug("adding a new CSRF cookie")
		cookie = &http.Cookie{Name: o.csrfCookieName, Value: xid.New().String()}
	}
	http.SetCookie(w, cookie)

	// Calculate the state query parameter using the cookie value.
	mac := hmac.New(sha256.New, o.hmacSecret)
	mac.Write([]byte(cookie.Value))
	state := hex.EncodeToString(mac.Sum(nil))
	http.Redirect(w, r, o.oauthConfig.AuthCodeURL(state), http.StatusFound)
}

// callback is the handler for the GET /user/login/callback endpoint, which handles the callback for the Authorzation
// Grant flow.
func (o OAuth2) callback(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{"package": "authentication", "function": "OAuth2.callback"})
	logger.Trace("start")

	// Get the CSRF cookie.
	csrfCookie, err := r.Cookie(o.csrfCookieName)
	if err != nil {
		msg := "no CSRF cookie found in request"
		logger.WithField("error", err).Debug(msg)
		utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
		return
	}

	// Get the state parameter.
	state := r.URL.Query().Get("state")
	if state == "" {
		msg := "no state parameter found in request"
		logger.Debug(msg)
		utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
		return
	}

	// Check the state parameter against the cookie value.
	mac := hmac.New(sha256.New, o.hmacSecret)
	mac.Write([]byte(csrfCookie.Value))
	expectedState := hex.EncodeToString(mac.Sum(nil))
	if expectedState != state {
		msg := "invalid state parameter found in request"
		logger.Debug(msg)
		utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
		return
	}

	// Get the code parameter.
	code := r.URL.Query().Get("code")
	if code == "" {
		msg := "no authorization code found in request"
		logger.Debug(msg)
		utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
		return
	}

	// Exchange the authorization code for the access token.
	token, err := o.oauthConfig.Exchange(context.Background(), code)
	if err != nil {
		msg := "unable to exchange token"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "failed to obtain access token", err.Error(), http.StatusInternalServerError)
		return
	}

	utils.ReturnStatus(w, token, http.StatusOK)
}

// postLogin is the handler for the POST /user/login endpoint, which implements the Resource Owner Credentials Grant
// flow. This endpoint isn't supported (or is supported but actively discouraged) by some identity providers, so it
// can be disabled in the configuration. If the endpoint is disabled, it will be available, but will always return
// status code 501.
func (o OAuth2) postLogin(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{"package": "authentication", "function": "OAuth2.callback"})
	logger.Trace("start")

	// Simply return a 501 if the endpoint is disabled.
	if !o.enablePostLogin {
		logger.Debug("the post login endpoint was called, but is disabled")
		utils.JSONError(w, r, "not implemented", "POST logins are currently disabled", http.StatusNotImplemented)
		return
	}

	// Read and parse the request body. I couldn't find a way to distinguish an I/O error from a bad request when using
	// ParseForm, but in the case of an I/O error, the response probably won't actually be returned anyway.
	err := r.ParseForm()
	if err != nil {
		msg := "unable to parse the request body"
		logger.WithField("error", err).Debug(msg)
		utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
		return
	}

	// Extract the username and password from the request.
	username := r.Form.Get("username")
	if username == "" {
		msg := "missing parameter: username"
		logger.Debug(msg)
		utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
		return
	}
	password := r.Form.Get("password")
	if password == "" {
		msg := "missing parameter: password"
		logger.Debug(msg)
		utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
		return
	}

	// Exchange the username and password for a token. I couldn't find a good way to distinguish an I/O error from a
	// bad request in this case either, but a bad request is more likely.
	token, err := o.oauthConfig.PasswordCredentialsToken(context.Background(), username, password)
	if err != nil {
		msg := "unable to exchange the credentials for a token"
		logger.WithField("error", err).Debug(msg)
		utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
		return
	}

	utils.ReturnStatus(w, token, http.StatusOK)
}
