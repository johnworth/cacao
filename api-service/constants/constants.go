package constants

const (
	// ConfigVarPrefix is used by kelseyhightower/envconfig as the prefix the env variables
	ConfigVarPrefix = "API"

	// DefaultNatsClientID defines the default nats client id for api service
	DefaultNatsClientID = "api"

	// DefaultKeycloakURL is the default url to keycloak
	DefaultKeycloakURL = "http://keycloak:8080/auth/realms/cacao"

	// DefaultKeycloakRedirectURL is the default redirect url from keycloak
	DefaultKeycloakRedirectURL = "http://api:8080/user/login/callback"

	// DefaultKeycloakClientID is the default keycloak id
	DefaultKeycloakClientID = "cacao-client"
)

// These are authentication driver related constants
const (
	// AuthDriverGlobus designates the constant to use for the Globus AuthDriver.
	AuthDriverGlobus = "globus"

	// AuthDriverKeycloak designates the constant to use the Keycloak AuthDriver
	AuthDriverKeycloak = "keycloak"

	// AuthDriverSimpleToken designates the constant to use the SimpleToken AuthDriver (local dev only)
	AuthDriverSimpleToken = "simpletoken"
)

// These are http request related headers
const (
	// RequestHeaderCacaoUser is the request header for the cacao user/actor
	RequestHeaderCacaoUser = "X-Cacao-User"

	// RequestHeaderCacaoAdmin is the request header to check if the user/actor is an admin
	RequestHeaderCacaoAdmin = "X-Cacao-Admin"

	// RequestHeaderCacaoEmulator is the request header to use if there is an emulator (to the actor)
	RequestHeaderCacaoEmulator = "X-Cacao-Emulator"
)
