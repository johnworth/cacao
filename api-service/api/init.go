package api

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao/api-service/authentication"
	"gitlab.com/cyverse/cacao/api-service/config"
	"gitlab.com/cyverse/cacao/api-service/constants"
)

var (
// unused	natsInfo map[string]string // TODO: remove this and replace relevant nats connetion methods
//vaultClient vault.Adapter
)

// InitAuthenticationDriver initializes the authentication driver
func InitAuthenticationDriver(c *config.Config) error {
	log.Trace("InitAuthentication(): start with driver = " + c.AuthDriver)
	var err error = nil

	switch c.AuthDriver {
	case constants.AuthDriverGlobus:
		if c.GlobusClientID == "" || c.GlobusClientSecret == "" {
			log.Panic("Globus client ID and secret should not be empty")
		}

		Authenticator, err = authentication.NewGlobusOAuth2Driver(*c)
	case constants.AuthDriverKeycloak:
		if c.KeycloakClientSecret == "" {
			log.Panic("Keycloak client secret should not be empty")
		}

		Authenticator, err = authentication.NewKeycloakAuth(*c)
	default:
		Authenticator = authentication.NewSimpleTokenAuth(*c)
	}

	return err
}

// InitNATSConnectionVars is used to define the NATS connection variables
func InitNATSConnectionVars(c *config.Config) {
	if c.Nats.URL == "" {
		c.Nats.URL = messaging.DefaultNatsURL
	}
	if c.Nats.ClientID == "" {
		c.Nats.ClientID = constants.DefaultNatsClientID
	}

	// unused
	// TODO: remove this and refactor where this is used
	// natsInfo = common.GetNATSInfo(c.Stan.ClusterID, c.Nats.ClientID, c.Nats.URL)
}
