// Package api is used to create routing and handler functions providing an API
// gateway for interacting with Cacao services over HTTP
package api

import (
	"fmt"
	"net/http"
)

// JSONError will create and write a nice JSON error response
func JSONError(w http.ResponseWriter, msg string, code int) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write([]byte(fmt.Sprintf(`{"error": {"code": %d, "message": "%s"}}`, code, msg)))
}

// unused
// func idResponse(w http.ResponseWriter, id string, code int) {
// 	w.Header().Add("Content-Type", "application/json")
// 	w.WriteHeader(code)
// 	w.Write([]byte(fmt.Sprintf(`{"id": "%s"}`, id)))
// }

// unused
// func getResponseCode(data []byte) int {
// 	var response struct {
// 		Error struct {
// 			Code int `json:"code"`
// 		} `json:"error"`
// 	}
// 	json.Unmarshal(data, &response)
// 	if response.Error.Code == 0 {
// 		return http.StatusOK
// 	}
// 	return response.Error.Code
// }

// unused
// func getWorkflowDefinitionData(request common.Request) (int, error) {
// 	// Synchronously request WorkflowDefinition
// 	msg, err := common.PublishRequest(
// 		request,
// 		"WorkflowDefinition.Get",
// 		"API",
// 		natsInfo["address"],
// 	)
// 	if err != nil {
// 		return http.StatusServiceUnavailable, err
// 	}

// 	err = json.Unmarshal(msg, request)
// 	if err != nil {
// 		return http.StatusInternalServerError, err
// 	}
// 	if len(request.GetError().Message) > 0 {
// 		return request.GetError().Code, fmt.Errorf(request.GetError().Message)
// 	}

// 	return http.StatusOK, nil
// }

// unused
// waitTimeout waits but with timeout, returns true if timed out
// func waitTimeout(wg *sync.WaitGroup, timeout time.Duration) bool {
// 	c := make(chan struct{})
// 	go func() {
// 		defer close(c)
// 		wg.Wait()
// 	}()

// 	select {
// 	case <-c:
// 		return false // completed normally
// 	case <-time.After(timeout):
// 		return true // timed out
// 	}
// }
