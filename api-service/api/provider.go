package api

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/config"
	"gitlab.com/cyverse/cacao/api-service/utils"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

// ProviderAPIRouter creates routes for provider-related operations such as creating and
// updating providers and logging in
func ProviderAPIRouter(router *mux.Router) {
	router.HandleFunc("/providers", getProviders).Methods("GET")
	router.HandleFunc("/providers", createProvider).Methods("POST")
	router.HandleFunc("/providers/{providerid}", getProvider).Methods("GET")
	router.HandleFunc("/providers/{providerid}", updateProvider).Methods("PATCH")
	router.HandleFunc("/providers/{providerid}", deleteProvider).Methods("DELETE")
}

// createProvider creates a provider within cacao
func createProvider(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "createProvider",
	})
	logger.Info("api.createProvider(): start")

	// get the request body
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request body")
		utils.JSONError(w, r, "unable to read request body", err.Error(), http.StatusBadRequest)
		return
	}

	// get the http provider
	var provider service.ProviderModel
	err = utils.StrictJSONDecode(reqBody, &provider)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("json request body is invalid")
		utils.JSONError(w, r, "json request body is invalid", err.Error(), http.StatusBadRequest)
		return
	}

	actor, emulator, _ := utils.GetCacaoHeaders(r)

	// Get the client, using the providername as the actor
	// TODO: provide a more informative error type/message
	logger.Trace("api.createProvider: creating NatsProviderClient")
	client, err := service.NewNatsProviderClient(config.GlobalConfig.AppContext, actor, emulator, config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats provider client")
		utils.JSONError(w, r, fmt.Sprintf("unable to get provider: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	// now write back the provider
	providerid, tid, err := client.Create(&provider)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create provider")
		utils.JSONError(w, r, "provider could not be created", err.Error(), http.StatusBadRequest)
		return
	}

	utils.ReturnStatus(w, utils.NewAcceptedResponse(string(providerid), string(tid)), http.StatusCreated)
}

func deleteProvider(w http.ResponseWriter, r *http.Request) {
	providerid := common.ID(mux.Vars(r)["providerid"])
	logger := log.WithFields(log.Fields{
		"package":    "api",
		"function":   "deleteProvider",
		"providerid": providerid,
	})
	logger.Info("api.deleteProvider(): start")

	actor, emulator, _ := utils.GetCacaoHeaders(r)

	// Get the client, using the providerid as the actor
	// TODO: provide a more informative error type/message
	logger.Trace("api.deleteProvider: creating NatsProviderClient")
	client, err := service.NewNatsProviderClient(config.GlobalConfig.AppContext, actor, emulator, config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats provider client")
		utils.JSONError(w, r, fmt.Sprintf("unable to get provider: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	tid, err := client.Delete(providerid)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not delete provider")
		utils.JSONError(w, r, fmt.Sprintf("provider %v could not be deleted", providerid), err.Error(), http.StatusBadRequest)
		return
	}

	utils.ReturnStatus(w, utils.NewAcceptedResponse(string(providerid), string(tid)), http.StatusAccepted)
}

func getProvider(w http.ResponseWriter, r *http.Request) {
	providerid := common.ID(mux.Vars(r)["providerid"])
	logger := log.WithFields(log.Fields{
		"package":    "api",
		"function":   "getProvider",
		"providerid": providerid,
	})

	// Get the client
	// TODO: provide a more informative error type/message
	logger.Trace("api.getProvider: creating NatsProviderClient")
	actor, emulator, _ := utils.GetCacaoHeaders(r)
	client, err := service.NewNatsProviderClient(config.GlobalConfig.AppContext, actor, emulator, config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats provider client")
		utils.JSONError(w, r, fmt.Sprintf("unable to get provider: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	// Get the provider object
	// TODO: provide a more informative error type/message
	log.Trace("api.getProvider: getting Provider")
	var provider service.Provider
	provider, err = client.Get(providerid)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("error when trying to get providerid")
		utils.JSONError(w, r, fmt.Sprintf("unable to get provider: %v", providerid), err.Error(), http.StatusInternalServerError)
		return
	}

	utils.ReturnStatus(w, service.NewProviderModel(provider), http.StatusOK)
}

func getProviders(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getProviders",
	})

	// Get the client
	// TODO: provide a more informative error type/message
	logger.Trace("api.getProvider: creating NatsProviderClient")
	actor, emulator, _ := utils.GetCacaoHeaders(r)
	client, err := service.NewNatsProviderClient(config.GlobalConfig.AppContext, actor, emulator, config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats provider client")
		utils.JSONError(w, r, fmt.Sprintf("unable to get providers: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	// Get the provider object
	// TODO: provide a more informative error type/message
	log.Trace("api.getProviders: getting Providers")
	providers, err := client.List()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("error when trying to get providers")
		utils.JSONError(w, r, fmt.Sprintf("unable to get providers: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	utils.ReturnStatus(w, providers, http.StatusOK)
}

// updateProvider allows someone to update a provider's information/secrets in Vault
func updateProvider(w http.ResponseWriter, r *http.Request) {
	providerid := common.ID(mux.Vars(r)["providerid"])
	logger := log.WithFields(log.Fields{
		"package":    "api",
		"function":   "updateProvider",
		"providerid": providerid,
	})
	logger.Info("api.updateProvider(): start")

	// get the request body
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request body")
		utils.JSONError(w, r, "unable to read request body", err.Error(), http.StatusBadRequest)
		return
	}

	// get the http provider
	var provider service.ProviderModel
	err = utils.StrictJSONDecode(reqBody, &provider)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("invalid request body")
		utils.JSONError(w, r, "invalid request body", err.Error(), http.StatusBadRequest)
		return
	}
	provider.ID = providerid

	actor, emulator, _ := utils.GetCacaoHeaders(r)

	// Get the client, using the providerid as the actor
	// TODO: provide a more informative error type/message
	logger.Trace("api.updateProvider: creating NatsProviderClient")
	client, err := service.NewNatsProviderClient(config.GlobalConfig.AppContext, actor, emulator, config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats provider client")
		utils.JSONError(w, r, fmt.Sprintf("unable to get provider: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	// now write back the provider
	tid, err := client.Update(&provider)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not update provider")
		utils.JSONError(w, r, fmt.Sprintf("provider %v could not be updated", providerid), err.Error(), http.StatusBadRequest)
		return
	}

	utils.ReturnStatus(w, utils.NewAcceptedResponse(string(providerid), string(tid)), http.StatusCreated)
}
