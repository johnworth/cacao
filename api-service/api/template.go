package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	templates "gitlab.com/cyverse/cacao/api-service/clients/templates"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

// templatesAPI is a single instance of the templates API implementation.
type templatesAPI struct {
	templatesClient templates.Client
}

// TemplateAPIRouter creates routes for operations related to templates.
func TemplateAPIRouter(templatesClient templates.Client, router *mux.Router) {
	tapi := &templatesAPI{templatesClient: templatesClient}

	// template types
	router.HandleFunc("/templates/types", tapi.listTemplateTypes).Methods("GET")
	router.HandleFunc("/templates/types", tapi.createTemplateType).Methods("POST")
	router.HandleFunc("/templates/types/{template_type_name}", tapi.getTemplateTypeByName).Methods("GET")
	router.HandleFunc("/templates/types/{template_type_name}", tapi.updateTemplateType).Methods("PUT")
	router.HandleFunc("/templates/types/{template_type_name}", tapi.updateTemplateTypeFields).Methods("PATCH")
	router.HandleFunc("/templates/types/{template_type_name}", tapi.deleteTemplateType).Methods("DELETE")

	// template source type
	router.HandleFunc("/templates/sourcetypes", tapi.listTemplateSourceTypes).Methods("GET")

	// template
	router.HandleFunc("/templates", tapi.listTemplates).Methods("GET")
	router.HandleFunc("/templates", tapi.importTemplate).Methods("POST")
	router.HandleFunc("/templates/{template_id}", tapi.getTemplateByID).Methods("GET")
	router.HandleFunc("/templates/{template_id}", tapi.updateTemplate).Methods("PUT")
	router.HandleFunc("/templates/{template_id}", tapi.updateTemplateFields).Methods("PATCH") // can be used for both update and sync
	router.HandleFunc("/templates/{template_id}", tapi.deleteTemplate).Methods("DELETE")
}

func (tapi *templatesAPI) JSONError(logger *log.Entry, w http.ResponseWriter, r *http.Request, err error) {
	logger.Error(err)

	status := http.StatusOK
	switch err.(type) {
	case *cacao_common_service.CacaoInvalidParameterError, *cacao_common_service.CacaoMarshalError:
		status = http.StatusBadRequest
	case *cacao_common_service.CacaoNotImplementedError:
		status = http.StatusNotImplemented
	case *cacao_common_service.CacaoNotFoundError:
		status = http.StatusNotFound
	case *cacao_common_service.CacaoTimeoutError:
		status = http.StatusRequestTimeout
	case *cacao_common_service.CacaoAlreadyExistError:
		status = http.StatusConflict
	case *cacao_common_service.CacaoUnauthorizedError:
		status = http.StatusUnauthorized
	default:
		status = http.StatusInternalServerError
	}

	if cerr, ok := err.(cacao_common_service.CacaoError); ok {
		utils.JSONError(w, r, string(cerr.StandardError()), cerr.ContextualError(), status)
	} else {
		utils.JSONError(w, r, "internal service error", err.Error(), status)
	}
}

// unmarshalRequest reads the request body and unmarshals it.
func (tapi *templatesAPI) unmarshalRequest(logger *log.Entry, w http.ResponseWriter, r *http.Request, dest interface{}) error {
	// Read the request body.
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		errorMessage := "unable to read request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoCommunicationError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return err
	}

	// Unmarshal the request body.
	err = json.Unmarshal(requestBody, dest)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return err
	}

	return nil
}

// unmarshalRequestAndGetFieldNames reads the request body and unmarshals it. Also returns field names.
func (tapi *templatesAPI) unmarshalRequestAndGetFieldNames(logger *log.Entry, w http.ResponseWriter, r *http.Request, dest interface{}) ([]string, error) {
	// Read the request body.
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		errorMessage := "unable to read request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoCommunicationError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return nil, err
	}

	// Unmarshal the request body.
	err = json.Unmarshal(requestBody, dest)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return nil, err
	}

	requestMap := map[string]interface{}{}
	// Unmarshal the request body to Map.
	err = json.Unmarshal(requestBody, &requestMap)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return nil, err
	}

	fields := []string{}
	for k := range requestMap {
		fields = append(fields, k)
	}

	return fields, nil
}

// getTemplatesClientSession obtains the templates client session for a request. Writes an error response and
// returns nil if the session can't be created.
func (tapi *templatesAPI) getTemplatesClientSession(logger *log.Entry, w http.ResponseWriter, r *http.Request) templates.Session {
	actor, emulator, isAdmin := utils.GetCacaoHeaders(r)
	session, err := tapi.templatesClient.Session(actor, emulator, isAdmin)
	if err != nil {
		logger.WithField("error", err).Error("unable to create the templates microservice client")
		tapi.JSONError(logger, w, r, err)
		return nil
	}
	return session
}

// extractID extracts an ID from the URL path and returns it, if the ID cannot be extracted, an error response is sent
// to the client, and an error is returned.
func (tapi *templatesAPI) extractID(logger *log.Entry, w http.ResponseWriter, r *http.Request, placeholder string, idType string) (cacao_common.ID, error) {
	templateID := cacao_common.ID(mux.Vars(r)[placeholder])
	if !templateID.Validate() {
		errorMessage := fmt.Sprintf("invalid %s ID: %s", idType, templateID)
		cerr := cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return cacao_common.ID(""), cerr
	}
	return templateID, nil
}

// extractName extracts a Name from the URL path and returns it, if the Name cannot be extracted, an error response is sent
// to the client, and an error is returned.
func (tapi *templatesAPI) extractName(logger *log.Entry, w http.ResponseWriter, r *http.Request, placeholder string) (string, error) {
	name := mux.Vars(r)[placeholder]
	if len(name) == 0 {
		errorMessage := fmt.Sprintf("invalid Name: %s", name)
		cerr := cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return "", cerr
	}
	return name, nil
}

// extractParam extracts a parameter from the URL params and returns it, if the param does not exist, an empty string is returned.
func (tapi *templatesAPI) extractParam(r *http.Request, paramkey string) string {
	return r.URL.Query().Get(paramkey)
}

// listTemplateTypes implements the GET /tempaltes/types endpoint.
func (tapi *templatesAPI) listTemplateTypes(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listTemplateTypes",
	})
	logger.Info("api.listTemplateTypes(): start")
	var err error

	// Extract the provider type from the request.
	providerTypeString := tapi.extractParam(r, "provider")
	providerType := cacao_common_service.TemplateProviderType(providerTypeString)

	// Create the templates client session.
	session := tapi.getTemplatesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the list of template types
	var templateTypes []cacao_common_http.TemplateType
	if len(providerType) == 0 {
		templateTypes, err = session.ListTemplateTypes()
		if err != nil {
			errorMessage := "error listing template types"
			logger.WithField("error", err).Error(errorMessage)
			tapi.JSONError(logger, w, r, err)
			return
		}
	} else {
		templateTypes, err = session.ListTemplateTypesForProvideType(providerType)
		if err != nil {
			errorMessage := "error listing template types"
			logger.WithField("error", err).Error(errorMessage)
			tapi.JSONError(logger, w, r, err)
			return
		}
	}

	// Guard against ListTemplateTypes returning nil.
	if templateTypes == nil {
		templateTypes = make([]cacao_common_http.TemplateType, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, templateTypes, http.StatusOK)
}

// getTemplateTypeByName implements the GET /templates/types/{template_type_name} endpoint.
// getTemplateTypeByName returns the templateType with the name if it exists and the authenticated user may view it.
func (tapi *templatesAPI) getTemplateTypeByName(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getTemplateTypeByName",
	})
	logger.Info("api.getTemplateTypeByName(): start")
	var err error

	// Extract the template type name from the request and validate it.
	templateTypeName, err := tapi.extractName(logger, w, r, "template_type_name")
	if err != nil {
		return
	}

	// Create the templates client session.
	session := tapi.getTemplatesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the template type.
	templateType, err := session.GetTemplateType(templateTypeName)
	if err != nil {
		errorMessage := "error obtaining template type information"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	if len(templateType.Name) == 0 {
		errorMessage := fmt.Sprintf("template type not found: %s", templateTypeName)
		cerr := cacao_common_service.NewCacaoNotFoundError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, templateType, http.StatusOK)
}

// createTemplateType implements the POST /templates/types endpoint.
func (tapi *templatesAPI) createTemplateType(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "createTemplateType",
	})
	logger.Info("api.createTemplateType(): start")
	var err error

	// Unmarshal the request body.
	var incomingRequest cacao_common_http.TemplateType
	err = tapi.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the templates session.
	session := tapi.getTemplatesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the template type creation request.
	err = session.ValidateTemplateTypeCreationRequest(incomingRequest)
	if err != nil {
		errorMessage := "template type creation request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Submit the creation request.
	tname, err := session.CreateTemplateType(incomingRequest)
	if err != nil {
		errorMessage := "template type creation request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", tname)
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// updateTemplateType implements the PUT /templates/types/{template_type_name} endpoint.
// updateTemplateType updates a template type information.
func (tapi *templatesAPI) updateTemplateType(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateTemplateType",
	})
	logger.Info("api.updateTemplateType(): start")

	// Extract the template type name from the request and validate it.
	templateTypeName, err := tapi.extractName(logger, w, r, "template_type_name")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest cacao_common_http.TemplateType
	err = tapi.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the templates client session.
	session := tapi.getTemplatesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the template type update request.
	err = session.ValidateTemplateTypeUpdateRequest(templateTypeName, incomingRequest)
	if err != nil {
		errorMessage := "template type update request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Submit the update request.
	tname, err := session.UpdateTemplateType(templateTypeName, incomingRequest)
	if err != nil {
		errorMessage := "template type update request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", tname)
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// updateTemplateTypeFields implements the PATCH /templates/types/{template_type_name} endpoint.
// updateTemplateTypeFields updates a template type information.
func (tapi *templatesAPI) updateTemplateTypeFields(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateTemplateTypeFields",
	})
	logger.Info("api.updateTemplateTypeFields(): start")

	// Extract the template type name from the request and validate it.
	templateTypeName, err := tapi.extractName(logger, w, r, "template_type_name")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest cacao_common_http.TemplateType
	updateFields, err := tapi.unmarshalRequestAndGetFieldNames(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the templates client session.
	session := tapi.getTemplatesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the template type update request.
	err = session.ValidateTemplateTypeUpdateFieldsRequest(templateTypeName, incomingRequest, updateFields)
	if err != nil {
		errorMessage := "template type update request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Submit the update request.
	tname, err := session.UpdateTemplateTypeFields(templateTypeName, incomingRequest, updateFields)
	if err != nil {
		errorMessage := "template type update request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", tname)
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// deleteTemplateType implements the DELETE /templates/types/{template_type_name} endpoint.
func (tapi *templatesAPI) deleteTemplateType(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deleteTemplateType",
	})
	logger.Info("api.deleteTemplateType(): start")

	// Extract the template type name from the request and validate it.
	templateTypeName, err := tapi.extractName(logger, w, r, "template_type_name")
	if err != nil {
		return
	}

	// Create the templates client session.
	session := tapi.getTemplatesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and vaidate the template type deletion request.
	err = session.ValidateTemplateTypeDeletionRequest(templateTypeName)
	if err != nil {
		errorMessage := "template type deletion request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Submit the deletion request.
	tname, err := session.DeleteTemplateType(templateTypeName)
	if err != nil {
		errorMessage := "template type deletion request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse(templateTypeName, tname)
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// listTemplateSourceTypes implements the GET /tempaltes/sourcetypes endpoint.
func (tapi *templatesAPI) listTemplateSourceTypes(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listTemplateSourceTypes",
	})
	logger.Info("api.listTemplateSourceTypes(): start")
	var err error

	// Create the templates client session.
	session := tapi.getTemplatesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the list of template source types
	templateSourceTypes, err := session.ListTemplateSourceTypes()
	if err != nil {
		errorMessage := "error listing template source types"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Guard against ListTemplateTypes returning nil.
	if templateSourceTypes == nil {
		templateSourceTypes = make([]cacao_common_service.TemplateSourceType, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, templateSourceTypes, http.StatusOK)
}

// listTemplates implements the GET /templates endpoint.
func (tapi *templatesAPI) listTemplates(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listTemplates",
	})
	logger.Info("api.listTemplates(): start")
	var err error

	// Create the templates client session.
	session := tapi.getTemplatesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the list of templates
	templates, err := session.ListTemplates()
	if err != nil {
		errorMessage := "error listing templates"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Guard against ListTemplates returning nil.
	if templates == nil {
		templates = make([]cacao_common_http.Template, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, templates, http.StatusOK)
}

// getTemplateByID implements the GET /templates/{template_id} endpoint.
// getTemplateByID returns the template with the specified ID if it exists and the authenticated user may view it.
func (tapi *templatesAPI) getTemplateByID(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getTemplateByID",
	})
	logger.Info("api.getTemplateByID(): start")
	var err error

	// Extract the template ID from the request and validate it.
	templateID, err := tapi.extractID(logger, w, r, "template_id", "template")
	if err != nil {
		return
	}

	// Create the templates client session.
	session := tapi.getTemplatesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the template.
	template, err := session.GetTemplate(templateID)
	if err != nil {
		errorMessage := "error obtaining template information"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	if !template.ID.Validate() {
		errorMessage := fmt.Sprintf("template not found: %s", templateID)
		cerr := cacao_common_service.NewCacaoNotFoundError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, template, http.StatusOK)
}

// importTemplate implements the POST /templates endpoint.
func (tapi *templatesAPI) importTemplate(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "importTemplate",
	})
	logger.Info("api.importTemplate(): start")
	var err error

	// Unmarshal the request body.
	var incomingRequest cacao_common_http.Template
	err = tapi.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the templates session.
	session := tapi.getTemplatesClientSession(logger, w, r)
	if session == nil {
		return
	}

	credentialID := incomingRequest.CredentialID

	// Build and validate the template import request.
	err = session.ValidateTemplateImportRequest(incomingRequest, credentialID)
	if err != nil {
		errorMessage := "template import request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Submit the import request.
	tid, err := session.ImportTemplate(incomingRequest, credentialID)
	if err != nil {
		errorMessage := "template import request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// updateTemplate implements the PUT /templates/{template_id} endpoint.
// updateTemplate updates a template information.
func (tapi *templatesAPI) updateTemplate(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateTemplate",
	})
	logger.Info("api.updateTemplate(): start")

	// Extract the template ID from the request and validate it.
	templateID, err := tapi.extractID(logger, w, r, "template_id", "template")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest cacao_common_http.Template
	err = tapi.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the templates client session.
	session := tapi.getTemplatesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// update
	// Build and validate the template update request.
	err = session.ValidateTemplateUpdateRequest(templateID, incomingRequest)
	if err != nil {
		errorMessage := "template update request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Submit the update request.
	tid, err := session.UpdateTemplate(templateID, incomingRequest)
	if err != nil {
		errorMessage := "template update request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// updateTemplateFields implements the PATCH /templates/{template_id} endpoint.
// updateTemplateFields updates fields of a template information.
func (tapi *templatesAPI) updateTemplateFields(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateTemplateFields",
	})
	logger.Info("api.updateTemplateFields(): start")

	// Extract the template ID from the request and validate it.
	templateID, err := tapi.extractID(logger, w, r, "template_id", "template")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest cacao_common_http.Template
	updateFields, err := tapi.unmarshalRequestAndGetFieldNames(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	sync := incomingRequest.Sync
	credentialID := incomingRequest.CredentialID

	// Create the templates client session.
	session := tapi.getTemplatesClientSession(logger, w, r)
	if session == nil {
		return
	}

	if sync {
		// sync
		// Build and validate the template sync request.
		err = session.ValidateTemplateSyncRequest(templateID, credentialID)
		if err != nil {
			errorMessage := "template sync request validation failed"
			logger.WithField("error", err).Error(errorMessage)
			tapi.JSONError(logger, w, r, err)
			return
		}

		// Submit the sync request.
		tid, err := session.SyncTemplate(templateID, credentialID)
		if err != nil {
			errorMessage := "template sync request submission failed"
			logger.WithField("error", err).Error(errorMessage)
			tapi.JSONError(logger, w, r, err)
			return
		}

		// Format the response body.
		body := utils.NewAcceptedResponse("", tid.String())
		utils.ReturnStatus(w, body, http.StatusAccepted)
	} else {
		// update
		// Build and validate the template update request.
		err = session.ValidateTemplateUpdateFieldsRequest(templateID, incomingRequest, updateFields)
		if err != nil {
			errorMessage := "template update request validation failed"
			logger.WithField("error", err).Error(errorMessage)
			tapi.JSONError(logger, w, r, err)
			return
		}

		// Submit the update request.
		tid, err := session.UpdateTemplateFields(templateID, incomingRequest, updateFields)
		if err != nil {
			errorMessage := "template update request submission failed"
			logger.WithField("error", err).Error(errorMessage)
			tapi.JSONError(logger, w, r, err)
			return
		}

		// Format the response body.
		body := utils.NewAcceptedResponse("", tid.String())
		utils.ReturnStatus(w, body, http.StatusAccepted)
	}
}

// deleteTemplate implements the DELETE /templates/{template_id} endpoint.
func (tapi *templatesAPI) deleteTemplate(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deleteTemplate",
	})
	logger.Info("api.deleteTemplate(): start")

	// Extract the template ID from the request and validate it.
	templateID, err := tapi.extractID(logger, w, r, "template_id", "template")
	if err != nil {
		return
	}

	// Create the templates client session.
	session := tapi.getTemplatesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and vaidate the template deletion request.
	err = session.ValidateTemplateDeletionRequest(templateID)
	if err != nil {
		errorMessage := "template deletion request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Submit the deletion request.
	tid, err := session.DeleteTemplate(templateID)
	if err != nil {
		errorMessage := "template deletion request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse(templateID.String(), tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}
