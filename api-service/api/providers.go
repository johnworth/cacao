package api

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"time"

	"gitlab.com/cyverse/cacao/api-service/clients/openstackprovider"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

// OpenstackProviderAPIRouter adds routes for getting images, flavors, projects, and credentials
// from a provider.
func OpenstackProviderAPIRouter(client openstackprovider.Client, router *mux.Router) {
	api := NewOpenStackProviderAPI(client)
	router.HandleFunc("/providers/{providerid}/applicationCredentials", api.ApplicationCredentialList).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/applicationCredentials/{applicationcredentialid}", api.GetApplicationCredential).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/images", api.ImageList).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/images/{imageid}", api.GetImage).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/flavors", api.FlavorList).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/flavors/{flavorid}", api.GetFlavor).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/projects", api.ProjectList).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/projects/{projectid}", api.GetProject).Methods(http.MethodGet)
}

// writeError is a utility function for logging and sending back an error
// to the client.
func writeError(w http.ResponseWriter, code int, err error) {
	log.Error(err)
	http.Error(w, err.Error(), code)
}

// writeJSON is a utility function for responding with JSON to the caller.
func writeJSON(w http.ResponseWriter, data interface{}) {
	var err error
	w.Header().Set("Content-Type", "application/json")
	if err = json.NewEncoder(w).Encode(data); err != nil {
		writeError(w, http.StatusInternalServerError, err)
	}
}

// unused
// timeoutContext() returns a new context with a timeout set for 30
// seconds, based on the background context.
// func timeoutContext() (context.Context, context.CancelFunc) {
// 	return context.WithTimeout(context.Background(), 30*time.Second)
// }

// OSProviderConstructor describes the signature of a function that can
// create new OpenStackProvider. Useful for unit tests.
type OSProviderConstructor func(string, *messaging.NatsConfig, *messaging.StanConfig) providers.OpenStackProvider

//OpenStackProviderAPI implements the HTTP API for the OpenStack Provider stuff.
type OpenStackProviderAPI struct {
	client openstackprovider.Client
}

// NewOpenStackProviderAPI ...
func NewOpenStackProviderAPI(client openstackprovider.Client) *OpenStackProviderAPI {
	return &OpenStackProviderAPI{client}
}

// ApplicationCredentialList is an HTTP handler listing all of the available application credentials available
// to the user.
func (p *OpenStackProviderAPI) ApplicationCredentialList(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "OpenStackProviderAPI.ApplicationCredentialList",
	})
	logger.Info("request received")

	providerID, credentialID, err := p.extractSharedFromRequest(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusBadRequest)
		writeError(w, http.StatusBadRequest, err)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credentialID,
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusInternalServerError)
		writeError(w, http.StatusInternalServerError, err)
		return
	}
	defer cancel()
	applicationCredentialList, err := session.ApplicationCredentialList(providerID, credentialID)
	if err != nil {
		logger.WithError(err).Error(http.StatusInternalServerError)
		writeError(w, http.StatusInternalServerError, err)
		return
	}
	if applicationCredentialList != nil {
		logger.WithField("length", len(applicationCredentialList)).Info("responded")
	} else {
		logger.WithField("length", nil).Info("responded")
	}
	writeJSON(w, applicationCredentialList)
}

// GetApplicationCredential is an HTTP handler for getting a single application credential description.
func (p *OpenStackProviderAPI) GetApplicationCredential(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "OpenStackProviderAPI.GetApplicationCredential",
	})
	logger.Info("request received")

	providerID, credentialID, err := p.extractSharedFromRequest(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusBadRequest)
		writeError(w, http.StatusBadRequest, err)
		return
	}
	applicationCredentialID, err := p.extractApplicationCredentialIDFromRequest(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusBadRequest)
		writeError(w, http.StatusBadRequest, err)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":              providerID,
		"credential":            credentialID,
		"applicationCredential": applicationCredentialID,
	})
	session1, cancel, err := p.newSession(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusInternalServerError)
		writeError(w, http.StatusInternalServerError, err)
		return
	}
	defer cancel()
	applicationCredential, err := session1.GetApplicationCredential(providerID, credentialID, applicationCredentialID)
	if err != nil {
		logger.WithError(err).Error(http.StatusInternalServerError)
		writeError(w, http.StatusInternalServerError, err)
		return
	}
	logger.Info("responded")
	writeJSON(w, applicationCredential)
}

// ImageList is an HTTP handler listing all of the available images available
// to the user.
func (p *OpenStackProviderAPI) ImageList(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "OpenStackProviderAPI.ImageList",
	})
	logger.Info("request received")

	providerID, credentialID, err := p.extractSharedFromRequest(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusBadRequest)
		writeError(w, http.StatusBadRequest, err)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credentialID,
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusInternalServerError)
		writeError(w, http.StatusInternalServerError, err)
		return
	}
	defer cancel()
	imageList, err := session.ImageList(providerID, credentialID)
	if err != nil {
		logger.WithError(err).Error(http.StatusInternalServerError)
		writeError(w, http.StatusInternalServerError, err)
		return
	}
	if imageList != nil {
		logger.WithField("length", len(imageList)).Info("responded")
	} else {
		logger.WithField("length", nil).Info("responded")
	}
	writeJSON(w, imageList)
}

// GetImage is an HTTP handler for getting a single image description.
func (p *OpenStackProviderAPI) GetImage(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "OpenStackProviderAPI.GetImage",
	})
	logger.Info("request received")

	providerID, credentialID, err := p.extractSharedFromRequest(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusBadRequest)
		writeError(w, http.StatusBadRequest, err)
		return
	}
	imageID, err := p.extractImageIDFromRequest(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusBadRequest)
		writeError(w, http.StatusBadRequest, err)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credentialID,
		"image":      imageID,
	})
	session1, cancel, err := p.newSession(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusInternalServerError)
		writeError(w, http.StatusInternalServerError, err)
		return
	}
	defer cancel()
	image, err := session1.GetImage(providerID, credentialID, imageID)
	if err != nil {
		logger.WithError(err).Error(http.StatusInternalServerError)
		writeError(w, http.StatusInternalServerError, err)
		return
	}
	logger.Info("responded")
	writeJSON(w, image)
}

// FlavorList is an HTTP handler for listing all of the available flavors.
func (p *OpenStackProviderAPI) FlavorList(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "OpenStackProviderAPI.FlavorList",
	})
	logger.Info("request received")

	providerID, credentialID, err := p.extractSharedFromRequest(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusBadRequest)
		writeError(w, http.StatusBadRequest, err)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credentialID,
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusBadRequest)
		writeError(w, http.StatusBadRequest, err)
		return
	}
	defer cancel()
	flavorList, err := session.FlavorList(providerID, credentialID)
	if err != nil {
		logger.WithError(err).Error(http.StatusInternalServerError)
		writeError(w, http.StatusInternalServerError, err)
		return
	}
	if flavorList != nil {
		logger.WithField("length", len(flavorList)).Info("responded")
	} else {
		logger.WithField("length", nil).Info("responded")
	}
	writeJSON(w, flavorList)
}

// GetFlavor is an HTTP handler for getting a single flavor.
func (p *OpenStackProviderAPI) GetFlavor(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "OpenStackProviderAPI.GetFlavor",
	})
	logger.Info("request received")

	providerID, credentialID, err := p.extractSharedFromRequest(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusBadRequest)
		writeError(w, http.StatusBadRequest, err)
		return
	}
	flavorID, err := p.extractFlavorIDFromRequest(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusBadRequest)
		writeError(w, http.StatusBadRequest, err)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credentialID,
		"flavor":     flavorID,
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusInternalServerError)
		writeError(w, http.StatusInternalServerError, err)
		return
	}
	defer cancel()
	flavor, err := session.GetFlavor(providerID, credentialID, flavorID)
	if err != nil {
		logger.WithError(err).Error(http.StatusInternalServerError)
		writeError(w, http.StatusInternalServerError, err)
		return
	}
	logger.Info("responded")
	writeJSON(w, flavor)
}

// ProjectList is an HTTP handler for listing all of the available projects.
func (p *OpenStackProviderAPI) ProjectList(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "OpenStackProviderAPI.ProjectList",
	})
	logger.Info("request received")

	providerID, credentialID, err := p.extractSharedFromRequest(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusBadRequest)
		writeError(w, http.StatusBadRequest, err)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credentialID,
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusBadRequest)
		writeError(w, http.StatusBadRequest, err)
		return
	}
	defer cancel()
	projectList, err := session.ProjectList(providerID, credentialID)
	if err != nil {
		logger.WithError(err).Error(http.StatusInternalServerError)
		writeError(w, http.StatusInternalServerError, err)
		return
	}
	if projectList != nil {
		logger.WithField("length", len(projectList)).Info("responded")
	} else {
		logger.WithField("length", nil).Info("responded")
	}
	writeJSON(w, projectList)
}

// GetProject is an HTTP handler for getting a single project.
func (p *OpenStackProviderAPI) GetProject(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "OpenStackProviderAPI.GetProject",
	})
	logger.Info("request received")

	providerID, credentialID, err := p.extractSharedFromRequest(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusBadRequest)
		writeError(w, http.StatusBadRequest, err)
		return
	}
	projectID, err := p.extractProjectIDFromRequest(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusBadRequest)
		writeError(w, http.StatusBadRequest, err)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credentialID,
		"project":    projectID,
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		logger.WithError(err).Error(http.StatusInternalServerError)
		writeError(w, http.StatusInternalServerError, err)
		return
	}
	defer cancel()
	project, err := session.GetProject(providerID, credentialID, projectID)
	if err != nil {
		logger.WithError(err).Error(http.StatusInternalServerError)
		writeError(w, http.StatusInternalServerError, err)
		return
	}
	logger.Info("responded")
	writeJSON(w, project)
}

// create a new session from request for user
func (p OpenStackProviderAPI) newSession(r *http.Request) (openstackprovider.Session, context.CancelFunc, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*8)

	actor, emulator, _ := utils.GetCacaoHeaders(r)
	session, err := p.client.Session(ctx, actor, emulator)
	return session, cancel, err
}

// extract values from an HTTP request that is shared by all requests to provider openstack service
func (p OpenStackProviderAPI) extractSharedFromRequest(r *http.Request) (providerID, credentialID string, err error) {
	providerID, ok := mux.Vars(r)["providerid"]
	if !ok {
		err = errors.New("missing providerid variable")
		return "", "", err
	}
	// credential ID is optional, so it can be absent (empty)
	credentialID = r.URL.Query().Get("credential")
	return providerID, credentialID, nil
}

func (p OpenStackProviderAPI) extractApplicationCredentialIDFromRequest(r *http.Request) (string, error) {
	id, ok := mux.Vars(r)["applicationcredentialid"]
	if !ok {
		err := errors.New("missing applicationcredentialid variable")
		return "", err
	}
	return id, nil
}

func (p OpenStackProviderAPI) extractImageIDFromRequest(r *http.Request) (string, error) {
	id, ok := mux.Vars(r)["imageid"]
	if !ok {
		err := errors.New("missing imageid variable")
		return "", err
	}
	return id, nil
}

func (p OpenStackProviderAPI) extractFlavorIDFromRequest(r *http.Request) (string, error) {
	id, ok := mux.Vars(r)["flavorid"]
	if !ok {
		err := errors.New("missing flavorid variable")
		return "", err
	}
	return id, nil
}

func (p OpenStackProviderAPI) extractProjectIDFromRequest(r *http.Request) (string, error) {
	id, ok := mux.Vars(r)["projectid"]
	if !ok {
		err := errors.New("missing projectid variable")
		return "", err
	}
	return id, nil
}
