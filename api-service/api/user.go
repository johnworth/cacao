package api

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/config"
	"gitlab.com/cyverse/cacao/api-service/utils"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

// UserAPIRouter creates routes for user-related operations such as creating and
// updating users and logging in
func UserAPIRouter(router *mux.Router) {
	router.HandleFunc("/users", getUsers).Methods("GET")
	router.HandleFunc("/users", createUser).Methods("POST")
	router.HandleFunc("/users/{username}", getUser).Methods("GET")
	router.HandleFunc("/users/{username}", updateUser).Methods("PUT")
	router.HandleFunc("/users/{username}", patchUser).Methods("PATCH")
	router.HandleFunc("/users/{username}", deleteUser).Methods("DELETE")
	router.HandleFunc("/users/mine", getUser).Methods("GET")
}

// HTTPUser contains the json data structure for between the api service and the client
// type HTTPUser struct {
// 	Username     string            `json:"username"`
// 	FirstName    string            `json:"first_name"`
// 	LastName     string            `json:"last_name"`
// 	PrimaryEmail string            `json:"primary_email"`
// 	CreatedAt    string            `json:"created_at"`
// 	UpdatedAt    string            `json:"updated_at"`
// 	UpdatedBy    string            `json:"updated_by"`
// 	IsAdmin      bool              `json:"is_admin"`
// 	DisabledAt   string            `json:"disabled_at,omitempty"`
// 	Preferences  map[string]string `json:"preferences,omitempty"`
// }

// NewHTTPUser creates a new HTTPUser from a service.User object
func NewHTTPUser(u service.User) common.HTTPUser {
	uresp := common.HTTPUser{
		Username:     u.GetUsername(),
		FirstName:    u.GetFirstName(),
		LastName:     u.GetLastName(),
		PrimaryEmail: u.GetPrimaryEmail(),
		CreatedAt:    u.GetCreatedAt().UTC().Format(time.RFC3339),
		UpdatedAt:    u.GetUpdatedAt().UTC().Format(time.RFC3339),
		UpdatedBy:    u.GetUpdatedBy(),
		IsAdmin:      u.GetIsAdmin(),
	}

	if !u.GetDisabledAt().IsZero() {
		uresp.DisabledAt = u.GetDisabledAt().UTC().Format(time.RFC3339)
	}
	if u.GetPreferencesMap() != nil {
		uresp.Preferences = u.GetPreferencesMap()
	}

	return uresp
}

// UpdateToHTTPUser assumes h is the most recent user definition (from a client) and will attempt to update the service.User
// Note, this method will never update protected fields, like isAdmin, Created*, Updated*
func UpdateToHTTPUser(h *common.HTTPUser, user service.User, actorIsAdmin bool) {
	user.SetFirstName(h.FirstName)
	user.SetLastName(h.LastName)
	user.SetPrimaryEmail(h.PrimaryEmail)

	if actorIsAdmin {
		user.SetIsAdmin(h.IsAdmin)
		if h.DisabledAt != "" {
			ddate, err := time.Parse(time.RFC3339, h.DisabledAt)
			if err == nil {
				user.SetDisabledAt(ddate)
			} else {
				log.Debug("HTTPUser.Update: Warning, could not convert DisabledDate into an valid time, skipping for now...")
			}
		}
	}
}

// ConvertHTTPUserToUser will convert and return a service.User object, given an HTTPUser
// Note, the service.User is a copy
func ConvertHTTPUserToUser(h *common.HTTPUser, actor, emulator string) service.User {
	var u service.User = &service.UserModel{
		Session: service.Session{
			SessionActor:    actor,
			SessionEmulator: emulator,
		},
		Username:     h.Username,
		FirstName:    h.FirstName,
		LastName:     h.LastName,
		PrimaryEmail: h.PrimaryEmail,
		IsAdmin:      h.IsAdmin,
	}

	if h.DisabledAt != "" {
		ddate, err := time.Parse(time.RFC3339, h.DisabledAt)
		if err == nil {
			u.SetDisabledAt(ddate)
		} else {
			log.Debug("ConvertHTTPUserToUser: Warning, could not convert DisabledDate into an valid time, skipping for now...")
		}
	}

	return u
}

// createUser creates a user within cacao
func createUser(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "createUser",
	})
	logger.Info("api.createUser(): start")

	// get the request body
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request body")
		utils.JSONError(w, r, "unable to read request body", err.Error(), http.StatusBadRequest)
		return
	}

	// get the http user
	var huser common.HTTPUser = common.HTTPUser{}
	err = utils.StrictJSONDecode(reqBody, &huser)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("json request body is invalid")
		utils.JSONError(w, r, "json request body is invalid", err.Error(), http.StatusBadRequest)
		return
	}

	actor, emulator, isAdmin := utils.GetCacaoHeaders(r)

	// Make sure authenticated user is an admin
	// TODO: this is duplcating validation logic. Need to centralize authorization somewhere else
	if !isAdmin {
		err = fmt.Errorf("non-admin user '%s' cannot update other user '%s'", actor, huser.Username)
		logger.WithFields(log.Fields{"error": err}).Error("unable to get user")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusUnauthorized)
		return
	}

	// Get the userclient, using the username as the actor
	// TODO: provide a more informative error type/message
	logger.Trace("api.createUser: creating NatsUserClient")
	userclient, err := service.NewNatsUserClient(config.GlobalConfig.AppContext, actor, emulator, config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats user client")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	// get the user object from the HTTPUser
	user := ConvertHTTPUserToUser(&huser, actor, emulator)

	// now write back the user
	err = userclient.Add(user)
	if err != nil {
		if err.Error() == service.UserUsernameExistsCannotAddError {
			logger.WithFields(log.Fields{"error": err}).Error("username already exists")
			utils.JSONError(w, r, fmt.Sprintf("user %v already exists", huser.Username), err.Error(), http.StatusConflict)
			return
		} else if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("could not create user")
			utils.JSONError(w, r, fmt.Sprintf("user %v could not be created", huser.Username), err.Error(), http.StatusBadRequest)
			return
		}
	}

	// for now, return that we accepted the request
	utils.ReturnStatus(w, "OK", http.StatusCreated)
}

// updateUser allows someone to update a user's information/secrets in Vault
func updateUser(w http.ResponseWriter, r *http.Request) {
	username := mux.Vars(r)["username"]
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateUser",
		"username": username,
	})
	logger.Info("api.updateUser(): start")

	// get the request body
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request body")
		utils.JSONError(w, r, "unable to read request body", err.Error(), http.StatusBadRequest)
		return
	}

	// get the http user
	var huser common.HTTPUser = common.HTTPUser{}
	err = utils.StrictJSONDecode(reqBody, &huser)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("invalid request body")
		utils.JSONError(w, r, "invalid request body", err.Error(), http.StatusBadRequest)
		return
	}

	actor, emulator, isAdmin := utils.GetCacaoHeaders(r)

	// Make sure authenticated user has permission to update info on this user
	// TODO: this is duplcating validation logic. Need to centralize authorization somewhere else
	if actor != username && !isAdmin {
		err = fmt.Errorf("non-admin user '%s' cannot update other user '%s'", actor, username)
		logger.WithFields(log.Fields{"error": err}).Error("unable to get user")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusUnauthorized)
		return
	} else if username != huser.Username {
		err = fmt.Errorf("user in url path != username found in request body, found %s and %s", username, huser.Username)
		logger.WithFields(log.Fields{"error": err}).Error("user in url path != username found in request body, found" + username + " and " + huser.Username)
		utils.JSONError(w, r, fmt.Sprintf("user in url path != username found in request body: %v", err), err.Error(), http.StatusBadRequest)
		return
	}

	// Get the userclient, using the username as the actor
	// TODO: provide a more informative error type/message
	logger.Trace("api.updateUser: creating NatsUserClient")
	userclient, err := service.NewNatsUserClient(config.GlobalConfig.AppContext, actor, emulator, config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats user client")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	// Get the user object
	// TODO: provide a more informative error type/message
	log.Trace("api.updateUser: getting User")
	var user service.User
	user, err = userclient.Get(username)
	if err != nil {
		if err.Error() == service.UserUsernameNotFoundError {
			logger.WithFields(log.Fields{"error": err}).Error("could not find username")
			utils.JSONError(w, r, fmt.Sprintf("user %v not found", username), err.Error(), http.StatusNotFound)
		} else {
			logger.WithFields(log.Fields{"error": err}).Error("error when trying to get username")
			utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", username), err.Error(), http.StatusInternalServerError)
		}
		return
	}

	// update user
	UpdateToHTTPUser(&huser, user, isAdmin)

	// now write back the user
	err = userclient.Update(user)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not update user")
		utils.JSONError(w, r, fmt.Sprintf("user %v could not be updated", username), err.Error(), http.StatusBadRequest)
		return
	}

	// for now, return that we accepted the request
	utils.ReturnStatus(w, "OK", http.StatusCreated)
}

// patchUser allows someone to partially update a user
func patchUser(w http.ResponseWriter, r *http.Request) {
	username := mux.Vars(r)["username"]
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "patchUser",
		"username": username,
	})
	logger.Info("api.patchUser(): start")

	// get the request body
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request body")
		utils.JSONError(w, r, "unable to read request body", err.Error(), http.StatusBadRequest)
		return
	}

	// get the attributes as a map
	var usermap map[string]interface{}
	err = utils.StrictJSONDecode(reqBody, &usermap)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("invalid request body")
		utils.JSONError(w, r, "invalid request body", err.Error(), http.StatusBadRequest)
		return
	}

	actor, emulator, isAdmin := utils.GetCacaoHeaders(r)

	// Make sure authenticated user has permission to update info on this user
	// TODO: this is duplcating validation logic. Need to centralize authorization somewhere else
	if actor != username && !isAdmin {
		err = fmt.Errorf("non-admin user '%s' cannot update other user '%s'", actor, username)
		logger.WithFields(log.Fields{"error": err}).Error("unable to get user")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusUnauthorized)
		return
	}

	// Get the userclient, using the username as the actor
	// TODO: provide a more informative error type/message
	logger.Trace("api.patchUser: creating NatsUserClient")
	userclient, err := service.NewNatsUserClient(config.GlobalConfig.AppContext, actor, emulator, config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats user client")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	// Get the user object
	// TODO: provide a more informative error type/message
	log.Trace("api.patchUser: getting User")
	var user service.User
	user, err = userclient.Get(username)
	if err != nil {
		if err.Error() == service.UserUsernameNotFoundError {
			logger.WithFields(log.Fields{"error": err}).Error("could not find username")
			utils.JSONError(w, r, fmt.Sprintf("user %v not found", username), err.Error(), http.StatusNotFound)
		} else {
			logger.WithFields(log.Fields{"error": err}).Error("error when trying to get username")
			utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", username), err.Error(), http.StatusInternalServerError)
		}
		return
	}

	// Now, the hard part -- we need to take each attribute and map it to a user
	err = nil
	for k, v := range usermap {
		switch {
		case k == "username":
			// skipping username since it cannot be changed
			log.Trace("api.patchUser: found 'username' attribute, skipping")
		case k == "first_name" && v != "":
			user.SetFirstName(fmt.Sprintf("%v", v))
		case k == "last_name" && v != "":
			user.SetLastName(fmt.Sprintf("%v", v))
		case k == "primary_email" && v != "":
			user.SetPrimaryEmail(fmt.Sprintf("%v", v))
		case k == "is_admin" && isAdmin: // only allow admins change is_admin
			if v == true {
				user.SetIsAdmin(true)
			} else if v == false {
				user.SetIsAdmin(false)
			} else {
				err = errors.New("is_admin attribute was found but was not a boolean value")
			}
		case k == "disabled_at" && v != "" && isAdmin: // only allow admins change disabled_at
			ddate, err := time.Parse(time.RFC3339, fmt.Sprintf("%v", v))
			if err == nil {
				user.SetDisabledAt(ddate)
			} else {
				log.Debug("patchUser: Warning, could not convert DisabledDate into an valid time, skipping for now...")
			}
		default:
			err = errors.New("editing of an invalid field attempted: k = " + k + ", v = " + fmt.Sprintf("%v", v))
		}
	}

	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not update user")
		utils.JSONError(w, r, fmt.Sprintf("user %v could not be updated", username), err.Error(), http.StatusBadRequest)
		return
	}

	// now write back the user
	err = userclient.Update(user)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not update user")
		utils.JSONError(w, r, fmt.Sprintf("user %v could not be updated", username), err.Error(), http.StatusBadRequest)
		return
	}

	// for now, return that we accepted the request
	utils.ReturnStatus(w, "OK", http.StatusCreated)
}

func getUsers(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getUsers",
	})
	logger.Info("received request to get users")

	utils.JSONError(w, r, "GET /users not yet implemented", "GET /users not yet implemented", http.StatusNotImplemented)
}

func getUser(w http.ResponseWriter, r *http.Request) {
	log.Trace("api.getUser(): start")
	actor, emulator, isAdmin := utils.GetCacaoHeaders(r)
	var username string
	if r.URL.Path == "/users/mine" {
		username = actor
	} else {
		username = mux.Vars(r)["username"]
	}

	logger := log.WithFields(log.Fields{
		"package":         "api",
		"function":        "getUser",
		"username":        username,
		"requesting_user": actor,
	})

	// Make sure authenticated user has permission to get info on this user
	// TODO: this is duplcating validation logic. Need to centralize authorization somewhere else
	if actor != username && !isAdmin {
		err := fmt.Errorf("non-admin user '%s' cannot get other user '%s'", actor, username)
		logger.WithFields(log.Fields{"error": err}).Error("unable to get user")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusForbidden)
		return
	}

	// Get the userclient, using the username as the actor
	// TODO: provide a more informative error type/message
	logger.Trace("api.getUser: creating NatsUserClient")
	userclient, err := service.NewNatsUserClient(config.GlobalConfig.AppContext, actor, emulator, config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats user client")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	// Get the user object
	// TODO: provide a more informative error type/message
	log.Trace("api.getUser: getting User")
	var user service.User
	user, err = userclient.Get(username)
	if err != nil {
		if err.Error() == service.UserUsernameNotFoundError {
			logger.WithFields(log.Fields{"error": err}).Error("could not find username")
			utils.JSONError(w, r, fmt.Sprintf("user %v not found", username), err.Error(), http.StatusNotFound)
		} else {
			logger.WithFields(log.Fields{"error": err}).Error("error when trying to get username")
			utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", username), err.Error(), http.StatusInternalServerError)
		}
		return
	}

	newuser := NewHTTPUser(user)
	utils.ReturnStatus(w, newuser, http.StatusOK)
}

func deleteUser(w http.ResponseWriter, r *http.Request) {
	username := mux.Vars(r)["username"]
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deleteUser",
		"username": username,
	})
	logger.Info("api.deleteUser(): start")

	actor, emulator, isAdmin := utils.GetCacaoHeaders(r)

	// Make sure authenticated user has permission to delete this user
	// TODO: this is duplcating validation logic. Need to centralize authorization somewhere else
	var err error
	if !isAdmin {
		err = fmt.Errorf("non-admin user '%s' cannot delete another user '%s'", actor, username)
		logger.WithFields(log.Fields{"error": err}).Error("unable to get user")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusUnauthorized)
		return
	} else if username == actor {
		err = fmt.Errorf("a user cannot delete his own account, found %s and %s", actor, username)
		logger.WithFields(log.Fields{"error": err}).Error("a user cannot delete his own account, found" + actor + " and " + username)
		utils.JSONError(w, r, fmt.Sprintf("a user cannot delete his own account: %v", err), err.Error(), http.StatusBadRequest)
		return
	}

	// Get the userclient, using the username as the actor
	// TODO: provide a more informative error type/message
	logger.Trace("api.deleteUser: creating NatsUserClient")
	userclient, err := service.NewNatsUserClient(config.GlobalConfig.AppContext, actor, emulator, config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats user client")
		utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	// Get the user object
	// TODO: provide a more informative error type/message
	log.Trace("api.deleteUser: getting User")
	var user service.User
	user, err = userclient.Get(username)
	if err != nil {
		if err.Error() == service.UserUsernameNotFoundError {
			logger.WithFields(log.Fields{"error": err}).Error("could not find username")
			utils.JSONError(w, r, fmt.Sprintf("user %v not found", username), err.Error(), http.StatusNotFound)
		} else {
			logger.WithFields(log.Fields{"error": err}).Error("error when trying to get username")
			utils.JSONError(w, r, fmt.Sprintf("unable to get user: %v", username), err.Error(), http.StatusInternalServerError)
		}
		return
	}

	// now write back the user
	err = userclient.Delete(user)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not delete user")
		utils.JSONError(w, r, fmt.Sprintf("user %v could not be deleted", username), err.Error(), http.StatusBadRequest)
		return
	}

	// for now, return that we accepted the request
	utils.ReturnStatus(w, "OK", http.StatusAccepted)

}
