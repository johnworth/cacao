package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/clients/deployments"
	"gitlab.com/cyverse/cacao/api-service/clients/workspaces"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

// workspacesAPI is a single instance of the workspaces API implementation.
type workspacesAPI struct {
	workspacesClient  workspaces.Client
	deploymentsClient deployments.Client
}

// WorkspaceAPIRouter creates routes for operations related to workspaces.
func WorkspaceAPIRouter(workspacesClient workspaces.Client, deploymentsClient deployments.Client, router *mux.Router) {
	wapi := &workspacesAPI{
		workspacesClient:  workspacesClient,
		deploymentsClient: deploymentsClient,
	}

	router.HandleFunc("/workspaces", wapi.listWorkspaces).Methods("GET")
	router.HandleFunc("/workspaces", wapi.createWorkspace).Methods("POST")
	router.HandleFunc("/workspaces/{workspace_id}", wapi.getWorkspaceByID).Methods("GET")
	router.HandleFunc("/workspaces/{workspace_id}", wapi.updateWorkspace).Methods("PUT")
	router.HandleFunc("/workspaces/{workspace_id}", wapi.updateWorkspaceFields).Methods("PATCH")
	router.HandleFunc("/workspaces/{workspace_id}", wapi.deleteWorkspace).Methods("DELETE")
	router.HandleFunc("/workspaces/{workspace_id}/deployments", wapi.getDeployments).Methods("GET")
}

func (wapi *workspacesAPI) JSONError(logger *log.Entry, w http.ResponseWriter, r *http.Request, err error) {
	logger.Error(err)

	status := http.StatusOK
	switch err.(type) {
	case *cacao_common_service.CacaoInvalidParameterError, *cacao_common_service.CacaoMarshalError:
		status = http.StatusBadRequest
	case *cacao_common_service.CacaoNotImplementedError:
		status = http.StatusNotImplemented
	case *cacao_common_service.CacaoNotFoundError:
		status = http.StatusNotFound
	case *cacao_common_service.CacaoTimeoutError:
		status = http.StatusRequestTimeout
	case *cacao_common_service.CacaoAlreadyExistError:
		status = http.StatusConflict
	case *cacao_common_service.CacaoUnauthorizedError:
		status = http.StatusUnauthorized
	default:
		status = http.StatusInternalServerError
	}

	if cerr, ok := err.(cacao_common_service.CacaoError); ok {
		utils.JSONError(w, r, string(cerr.StandardError()), cerr.ContextualError(), status)
	} else {
		utils.JSONError(w, r, "internal service error", err.Error(), status)
	}
}

// unmarshalRequest reads the request body and unmarshals it.
func (wapi *workspacesAPI) unmarshalRequest(logger *log.Entry, w http.ResponseWriter, r *http.Request, dest interface{}) error {
	// Read the request body.
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		errorMessage := "unable to read request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoCommunicationError(errorMessage)
		wapi.JSONError(logger, w, r, cerr)
		return err
	}

	// Unmarshal the request body.
	err = json.Unmarshal(requestBody, dest)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		wapi.JSONError(logger, w, r, cerr)
		return err
	}

	return nil
}

// unmarshalRequestAndGetFieldNames reads the request body and unmarshals it. Also returns field names.
func (wapi *workspacesAPI) unmarshalRequestAndGetFieldNames(logger *log.Entry, w http.ResponseWriter, r *http.Request, dest interface{}) ([]string, error) {
	// Read the request body.
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		errorMessage := "unable to read request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoCommunicationError(errorMessage)
		wapi.JSONError(logger, w, r, cerr)
		return nil, err
	}

	// Unmarshal the request body.
	err = json.Unmarshal(requestBody, dest)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		wapi.JSONError(logger, w, r, cerr)
		return nil, err
	}

	requestMap := map[string]interface{}{}
	// Unmarshal the request body to Map.
	err = json.Unmarshal(requestBody, &requestMap)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		wapi.JSONError(logger, w, r, cerr)
		return nil, err
	}

	fields := []string{}
	for k := range requestMap {
		fields = append(fields, k)
	}

	return fields, nil
}

// getWorkspacesClientSession obtains the workspaces client session for a request. Writes an error response and
// returns nil if the session can't be created.
func (wapi *workspacesAPI) getWorkspacesClientSession(logger *log.Entry, w http.ResponseWriter, r *http.Request) workspaces.Session {
	actor, emulator, isAdmin := utils.GetCacaoHeaders(r)
	session, err := wapi.workspacesClient.Session(actor, emulator, isAdmin)
	if err != nil {
		logger.WithField("error", err).Error("unable to create the workspaces microservice client")
		wapi.JSONError(logger, w, r, err)
		return nil
	}
	return session
}

// getDeploymentsClientSession obtains the deployments client session for a request. Writes an error response and
// returns nil if the session can't be created.
func (wapi *workspacesAPI) getDeploymentsClientSession(logger *log.Entry, w http.ResponseWriter, r *http.Request) deployments.Session {
	actor, emulator, isAdmin := utils.GetCacaoHeaders(r)
	session, err := wapi.deploymentsClient.Session(actor, emulator, isAdmin)
	if err != nil {
		logger.WithField("error", err).Error("unable to create the deployments microservice client")
		wapi.JSONError(logger, w, r, err)
		return nil
	}
	return session
}

// extractID extracts an ID from the URL path and returns it, if the ID cannot be extracted, an error response is sent
// to the client, and an error is returned.
func (wapi *workspacesAPI) extractID(logger *log.Entry, w http.ResponseWriter, r *http.Request, placeholder string, idType string) (cacao_common.ID, error) {
	workspaceID := cacao_common.ID(mux.Vars(r)[placeholder])
	if !workspaceID.Validate() {
		errorMessage := fmt.Sprintf("invalid %s ID: %s", idType, workspaceID)
		cerr := cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
		wapi.JSONError(logger, w, r, cerr)
		return cacao_common.ID(""), cerr
	}
	return workspaceID, nil
}

// listWorkspaces implements the GET /workspaces endpoint.
func (wapi *workspacesAPI) listWorkspaces(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listWorkspaces",
	})
	logger.Info("api.listWorkspaces(): start")
	var err error

	// Create the workspaces client session.
	session := wapi.getWorkspacesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the list of workspaces
	workspaces, err := session.ListWorkspaces()
	if err != nil {
		errorMessage := "error listing workspaces"
		logger.WithField("error", err).Error(errorMessage)
		wapi.JSONError(logger, w, r, err)
		return
	}

	// Guard against ListWorkspaces returning nil.
	if workspaces == nil {
		workspaces = make([]cacao_common_http.Workspace, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, workspaces, http.StatusOK)
}

// getWorkspaceByID implements the GET /workspaces/{workspace_id} endpoint.
// getWorkspaceByID returns the workspace with the specified ID if it exists and the authenticated user may view it.
func (wapi *workspacesAPI) getWorkspaceByID(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getWorkspaceByID",
	})
	logger.Info("api.getWorkspaceByID(): start")
	var err error

	// Extract the workspace ID from the request and validate it.
	workspaceID, err := wapi.extractID(logger, w, r, "workspace_id", "workspace")
	if err != nil {
		return
	}

	// Create the workspaces client session.
	session := wapi.getWorkspacesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the workspace.
	workspace, err := session.GetWorkspace(workspaceID)
	if err != nil {
		errorMessage := "error obtaining workspace information"
		logger.WithField("error", err).Error(errorMessage)
		wapi.JSONError(logger, w, r, err)
		return
	}

	if !workspace.ID.Validate() {
		errorMessage := fmt.Sprintf("workspace not found: %s", workspaceID)
		cerr := cacao_common_service.NewCacaoNotFoundError(errorMessage)
		wapi.JSONError(logger, w, r, cerr)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, workspace, http.StatusOK)
}

// createWorkspace implements the POST /workspaces endpoint.
func (wapi *workspacesAPI) createWorkspace(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "createWorkspace",
	})
	logger.Info("api.createWorkspace(): start")
	var err error

	// Unmarshal the request body.
	var incomingRequest cacao_common_http.Workspace
	err = wapi.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the workspaces session.
	session := wapi.getWorkspacesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the workspace creation request.
	err = session.ValidateWorkspaceCreationRequest(incomingRequest)
	if err != nil {
		errorMessage := "workspace creation request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		wapi.JSONError(logger, w, r, err)
		return
	}

	// Submit the creation request.
	tid, err := session.CreateWorkspace(incomingRequest)
	if err != nil {
		errorMessage := "workspace creation request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		wapi.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// updateWorkspace implements the PUT /workspaces/{workspace_id} endpoint.
// updateWorkspace updates (replaces) a workspace information.
func (wapi *workspacesAPI) updateWorkspace(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateWorkspace",
	})
	logger.Info("api.updateWorkspace(): start")

	// Extract the workspace ID from the request and validate it.
	workspaceID, err := wapi.extractID(logger, w, r, "workspace_id", "workspace")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest cacao_common_http.Workspace
	err = wapi.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the workspaces client session.
	session := wapi.getWorkspacesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the workspace update request.
	err = session.ValidateWorkspaceUpdateRequest(workspaceID, incomingRequest)
	if err != nil {
		errorMessage := "workspace update request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		wapi.JSONError(logger, w, r, err)
		return
	}

	// Submit the update request.
	tid, err := session.UpdateWorkspace(workspaceID, incomingRequest)
	if err != nil {
		errorMessage := "workspace update request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		wapi.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// updateWorkspaceFields implements the PATCH /workspaces/{workspace_id} endpoint.
// updateWorkspaceFields updates fields of a workspace information.
func (wapi *workspacesAPI) updateWorkspaceFields(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateWorkspaceFields",
	})
	logger.Info("api.updateWorkspaceFields(): start")

	// Extract the workspace ID from the request and validate it.
	workspaceID, err := wapi.extractID(logger, w, r, "workspace_id", "workspace")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest cacao_common_http.Workspace
	updateFields, err := wapi.unmarshalRequestAndGetFieldNames(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the workspaces client session.
	session := wapi.getWorkspacesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the workspace update request.
	err = session.ValidateWorkspaceUpdateFieldsRequest(workspaceID, incomingRequest, updateFields)
	if err != nil {
		errorMessage := "workspace update request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		wapi.JSONError(logger, w, r, err)
		return
	}

	// Submit the update request.
	tid, err := session.UpdateWorkspaceFields(workspaceID, incomingRequest, updateFields)
	if err != nil {
		errorMessage := "workspace update request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		wapi.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// deleteWorkspace implements the DELETE /workspaces/{workspace_id} endpoint.
func (wapi *workspacesAPI) deleteWorkspace(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deleteWorkspace",
	})
	logger.Info("api.deleteWorkspace(): start")

	// Extract the workspace ID from the request and validate it.
	workspaceID, err := wapi.extractID(logger, w, r, "workspace_id", "workspace")
	if err != nil {
		return
	}

	// Create the workspaces client session.
	session := wapi.getWorkspacesClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and vaidate the workspace deletion request.
	err = session.ValidateWorkspaceDeletionRequest(workspaceID)
	if err != nil {
		errorMessage := "workspace deletion request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		wapi.JSONError(logger, w, r, err)
		return
	}

	// Submit the deletion request.
	tid, err := session.DeleteWorkspace(workspaceID)
	if err != nil {
		errorMessage := "workspace deletion request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		wapi.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse(workspaceID.String(), tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// getDeployments implements the GET /workspaces/{workspace_id}/deployments endpoint.
// the same functionality is implemented in GET /deployments endpoint.
func (wapi *workspacesAPI) getDeployments(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getDeployments",
	})
	logger.Info("api.getDeployments(): start")
	var err error

	// Extract the workspace ID from the request and validate it.
	workspaceID, err := wapi.extractID(logger, w, r, "workspace_id", "workspace")
	if err != nil {
		return
	}
	fullRunStr := r.URL.Query().Get("full_run")
	fullRun, err := strconv.ParseBool(fullRunStr)
	if err != nil {
		fullRun = false
	}

	// Create the deployments client session.
	session := wapi.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the list of deployments
	deploymentList, err := session.ListDeployments(deployments.ListOption{
		WorkspaceID: workspaceID,
		Offset:      0,
		PageSize:    -1,
		FullRun:     fullRun,
	})
	if err != nil {
		errorMessage := fmt.Sprintf("error listing deployments: %s", err.Error())
		logger.WithField("error", err).Error(errorMessage)
		wapi.JSONError(logger, w, r, err)
		return
	}

	// Guard against ListDeployments returning nil.
	if deploymentList == nil {
		deploymentList = make([]cacao_common_http.Deployment, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, deploymentList, http.StatusOK)
}
