package api

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/config"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

// CredentialAPIRouter creates routes for credential-related operations such as creating and
// updating credentials and logging in
func CredentialAPIRouter(router *mux.Router) {
	router.HandleFunc("/credentials", getCredentials).Methods("GET")
	router.HandleFunc("/credentials", createCredential).Methods("POST")
	router.HandleFunc("/credentials/{credentialname}", getCredential).Methods("GET")
	router.HandleFunc("/credentials/{credentialname}", patchCredential).Methods("PATCH")
	router.HandleFunc("/credentials/{credentialname}", deleteCredential).Methods("DELETE")
	router.HandleFunc("/credentials_types", getCredentialTypes).Methods("GET")
}

func getCredentialTypes(writer http.ResponseWriter, request *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "credentials_types",
	})
	logger.Info("api.getCredentialTypes(): start")
	var mytypes [9]string
	mytypes[0] = "openstack"
	mytypes[1] = "github"
	mytypes[2] = "gitlab"
	mytypes[3] = "dockerhub"
	mytypes[4] = "kubernetes"
	mytypes[5] = "ssh_public_key"
	mytypes[6] = "aws"
	mytypes[7] = "gcp"
	mytypes[8] = "azure"
	utils.ReturnStatus(writer, mytypes, http.StatusOK)
}

func deleteCredential(writer http.ResponseWriter, request *http.Request) {

	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deleteCredential",
	})
	logger.Info("api.deleteCredential(): start")

	id := mux.Vars(request)["credentialname"]

	//TODO JSON valid check

	//TODO Admin credential should be able to get all secrets of all credentials?
	actor, emulator, _ := utils.GetCacaoHeaders(request)

	logger.Trace("api.deleteCredential: creating NatsCredentialClient")
	credentialClient, err := service.NewNatsCredentialClient(config.GlobalConfig.AppContext, actor, emulator, config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats credential client")
		utils.JSONError(writer, request, fmt.Sprintf("unable to delete credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	message, err := credentialClient.Delete(id)

	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not delete credential")
		utils.JSONError(writer, request, fmt.Sprintf("unable to delete credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	utils.ReturnStatus(writer, message, http.StatusCreated)

}

func patchCredential(writer http.ResponseWriter, request *http.Request) {
	//TODO finalize update function
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "PatchCredential",
	})
	logger.Info("api.patchCredential(): start")

	//TODO special logic here if we are changing the ID of a credential
	logger.Info("api.patchCredential(): calling createCredential")
	createCredential(writer, request)
}

func getCredential(writer http.ResponseWriter, request *http.Request) {

	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getCredential",
	})
	logger.Info("api.getCredential(): start")

	id := mux.Vars(request)["credentialname"]

	//TODO JSON valid check

	//TODO Admin credential should be able to get all secrets of all credentials?
	actor, emulator, _ := utils.GetCacaoHeaders(request)

	logger.Trace("api.createCredential: creating NatsCredentialClient")
	credentialClient, err := service.NewNatsCredentialClient(config.GlobalConfig.AppContext, actor, emulator, config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats credential client")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	cred, err := credentialClient.Get(id)

	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not get credential")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	utils.ReturnStatus(writer, cred, http.StatusOK)

}

func createCredential(writer http.ResponseWriter, request *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "createCredential",
	})
	logger.Info("api.createCredential(): start")

	reqBody, err := ioutil.ReadAll(request.Body)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request body")
		utils.JSONError(writer, request, "unable to read request body", err.Error(), http.StatusBadRequest)
		return
	}

	credential := service.CredentialModel{}
	err = utils.StrictJSONDecode(reqBody, &credential)

	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("json request body is invalid")
		utils.JSONError(writer, request, "json request body is invalid", err.Error(), http.StatusBadRequest)
		return
	}

	//TODO Admin credential should be able to get all secrets of all credentials?
	actor, emulator, _ := utils.GetCacaoHeaders(request)

	logger.Trace("api.createCredential: creating NatsCredentialClient")
	credentialClient, err := service.NewNatsCredentialClient(config.GlobalConfig.AppContext, actor, emulator, config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats credential client")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	//important that we set the username as actor.
	credential.SetUsername(actor)

	message, err := credentialClient.Add(&credential)

	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create credential")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}
	utils.ReturnStatus(writer, message, http.StatusCreated)

}

func getCredentials(writer http.ResponseWriter, request *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getCredentials",
	})
	logger.Info("api.getCredentials(): start")

	//TODO JSON valid check
	//TODO Admin credential should be able to get all secrets of all credentials?
	actor, emulator, _ := utils.GetCacaoHeaders(request)

	logger.Trace("api.createCredential: creating NatsCredentialClient")
	credentialClient, err := service.NewNatsCredentialClient(config.GlobalConfig.AppContext, actor, emulator, config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats credential client")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	credList, err := credentialClient.List()

	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not get credential")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	if len(credList) == 0 {
		writer.Header().Add("Content-Type", "application/json")
		writer.WriteHeader(200)
		writer.Write([]byte("[]"))
		return
	}

	utils.ReturnStatus(writer, credList, http.StatusOK)
}
