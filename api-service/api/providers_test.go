package api

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	mock "github.com/stretchr/testify/mock"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/api-service/clients/openstackprovider"
	mockclient "gitlab.com/cyverse/cacao/api-service/clients/openstackprovider/mocks"
	"gitlab.com/cyverse/cacao/api-service/constants"
)

type FakeProvider struct {
	ID common.ID
}

func (f *FakeProvider) Init() error {
	return nil
}

var expectedApplicationCredentials = []providers.ApplicationCredential{
	{
		ID:           "00000000-1111-2222-aaaa-abcdefabcdef",
		Name:         "cred1",
		Description:  "first cred",
		ExpiresAt:    "2022-01-01",
		ProjectID:    "12345678-1111-2222-aaaa-abcdefabcdef",
		Roles:        nil,
		Unrestricted: true,
	},
	{
		ID:           "00000000-1111-2222-bbbb-abcdefabcdef",
		Name:         "cred2",
		Description:  "second cred",
		ExpiresAt:    "2022-01-01",
		ProjectID:    "12345678-1111-2222-bbbb-abcdefabcdef",
		Roles:        nil,
		Unrestricted: true,
	},
	{
		ID:           "00000000-1111-2222-cccc-abcdefabcdef",
		Name:         "cred3",
		Description:  "third cred",
		ExpiresAt:    "2022-01-01",
		ProjectID:    "12345678-1111-2222-cccc-abcdefabcdef",
		Roles:        nil,
		Unrestricted: true,
	},
}

var expectedImages = []providers.Image{
	{
		ID:              "85effb53-9842-49cc-9c5f-f26b4a2bf08c",
		Name:            "CentOS-6.5-x86_64-bin-DVD1.iso",
		DiskFormat:      "iso",
		ContainerFormat: "bare",
		Size:            4467982336,
		Checksum:        "83221db52687c7b857e65bfe60787838",
		Status:          "active",
		Visibility:      "public",
		Protected:       false,
		Project:         "74d53196e919420b8a23b6fd286cab63",
	},
	{
		ID:              "bff03fee-1886-4e4c-8d18-02f7e2e8ac96",
		Name:            "centos-7-x86_64-dvd-1708.iso",
		DiskFormat:      "iso",
		ContainerFormat: "bare",
		Size:            4521459712,
		Checksum:        "82b4160df8d2a360f0f38432ad7e049b",
		Status:          "active",
		Visibility:      "public",
		Protected:       false,
		Project:         "74d53196e919420b8a23b6fd286cab63",
	},
	{
		ID:              "faca47d6-0dc0-4871-b444-f2114fc88bb0",
		Name:            "CentOS-7-x86_64-DVD-1708.iso",
		DiskFormat:      "qcow2",
		ContainerFormat: "bare",
		Size:            4521459712,
		Checksum:        "82b4160df8d2a360f0f38432ad7e049b",
		Status:          "queued",
		Visibility:      "public",
		Protected:       false,
		Project:         "74d53196e919420b8a23b6fd286cab63",
	},
}

var expectedFlavors = []providers.Flavor{
	{
		Name:      "large3",
		RAM:       65536,
		Ephemeral: 0,
		VCPUs:     8,
		IsPublic:  true,
		Disk:      0,
		ID:        "000d1872-c4a7-11eb-8529-0242ac130003",
	},
	{
		Name:      "xlarge1",
		RAM:       32768,
		Ephemeral: 0,
		VCPUs:     16,
		IsPublic:  true,
		Disk:      0,
		ID:        "08daa0be-c4a7-11eb-8529-0242ac130003",
	},
	{
		Name:      "xxlarge1",
		RAM:       65536,
		Ephemeral: 0,
		VCPUs:     32,
		IsPublic:  true,
		Disk:      0,
		ID:        "12492d6e-c4a7-11eb-8529-0242ac130003",
	},
}

var expectedProjects = []providers.Project{
	{
		ID:          "abcdef12-1111-2222-aaaa-abcdefabcdef",
		Name:        "project1",
		Description: "first project",
		DomainID:    "dddddd12-1111-2222-aaaa-abcdefabcdef",
		Enabled:     true,
		IsDomain:    false,
		ParentID:    "ffffeedd-1111-2222-aaaa-abcdefabcdef",
		Properties:  nil,
	},
	{
		ID:          "abcdef12-1111-3333-aaaa-abcdefabcdef",
		Name:        "project2",
		Description: "second project",
		DomainID:    "dddddd12-1111-3333-aaaa-abcdefabcdef",
		Enabled:     true,
		IsDomain:    false,
		ParentID:    "ffffeedd-1111-3333-aaaa-abcdefabcdef",
		Properties:  nil,
	},
	{
		ID:          "abcdef12-1111-4444-aaaa-abcdefabcdef",
		Name:        "project3",
		Description: "third project",
		DomainID:    "dddddd12-1111-4444-aaaa-abcdefabcdef",
		Enabled:     true,
		IsDomain:    false,
		ParentID:    "ffffeedd-1111-4444-aaaa-abcdefabcdef",
		Properties:  nil,
	},
}

func createRecorderForOpenStackProvider(client openstackprovider.Client, r *http.Request) *httptest.ResponseRecorder {
	router := mux.NewRouter()
	OpenstackProviderAPIRouter(client, router)
	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, r)
	return recorder
}

func (f *FakeProvider) ApplicationCredentialList(ctx context.Context, session *service.Session) ([]providers.ApplicationCredential, error) {
	return expectedApplicationCredentials, nil
}

func (f *FakeProvider) GetApplicationCredential(ctx context.Context, session *service.Session, id string) (*providers.ApplicationCredential, error) {
	credentials, _ := f.ApplicationCredentialList(ctx, session)
	for _, credential := range credentials {
		if credential.ID == id {
			return &credential, nil
		}
	}
	return nil, errors.New("credential not found")
}

func (f *FakeProvider) ImageList(ctx context.Context, session *service.Session) ([]providers.Image, error) {
	return expectedImages, nil

}

func (f *FakeProvider) GetImage(ctx context.Context, session *service.Session, id string) (*providers.Image, error) {
	images, _ := f.ImageList(ctx, session)
	for _, image := range images {
		if image.ID == id {
			return &image, nil
		}
	}
	return nil, errors.New("image not found")
}

func (f *FakeProvider) FlavorList(ctx context.Context, session *service.Session) ([]providers.Flavor, error) {
	return expectedFlavors, nil
}

func (f *FakeProvider) GetFlavor(ctx context.Context, session *service.Session, id string) (*providers.Flavor, error) {
	flavors, _ := f.FlavorList(ctx, session)
	for _, flavor := range flavors {
		if flavor.ID == id {
			return &flavor, nil
		}
	}
	return nil, errors.New("flavor not found")
}

func (f *FakeProvider) ProjectList(ctx context.Context, session *service.Session) ([]providers.Project, error) {
	return expectedProjects, nil
}

func (f *FakeProvider) GetProject(ctx context.Context, session *service.Session, id string) (*providers.Project, error) {
	projects, _ := f.ProjectList(ctx, session)
	for _, project := range projects {
		if project.ID == id {
			return &project, nil
		}
	}
	return nil, errors.New("project not found")
}

func (f *FakeProvider) Finish() error {
	return nil
}

func createMockClientForOpenStackProvider(session *mockclient.Session) *mockclient.Client {
	client := &mockclient.Client{}
	client.On("Session", mock.Anything, "test_username", "").Return(session, nil)
	return client
}

func TestListApplicationCredentials(t *testing.T) {
	var (
		providerID = common.NewID("provider").String()
		credID     = ""
	)
	session := &mockclient.Session{}
	session.On("ApplicationCredentialList", providerID, credID).Return(expectedApplicationCredentials, nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/applicationCredentials", providerID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	client.AssertExpectations(t)
	session.AssertExpectations(t)
	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actual []providers.ApplicationCredential
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedApplicationCredentials, actual)
		}
	}
}

func TestListImages(t *testing.T) {
	var (
		providerID = common.NewID("provider").String()
		credID     = ""
	)
	session := &mockclient.Session{}
	session.On("ImageList", providerID, credID).Return(expectedImages, nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/images", providerID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	client.AssertExpectations(t)
	session.AssertExpectations(t)
	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actual []providers.Image
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedImages, actual)
		}
	}
}

func TestListFlavors(t *testing.T) {
	var (
		providerID = common.NewID("provider").String()
		credID     = ""
	)
	session := &mockclient.Session{}
	session.On("FlavorList", providerID, credID).Return(expectedFlavors, nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/flavors", providerID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actual []providers.Flavor
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedFlavors, actual)
		}
	}
}

func TestListProjects(t *testing.T) {
	var (
		providerID = common.NewID("provider").String()
		credID     = ""
	)
	session := &mockclient.Session{}
	session.On("ProjectList", providerID, credID).Return(expectedProjects, nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/projects", providerID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actual []providers.Project
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedProjects, actual)
		}
	}
}

func TestGetApplicationCredential(t *testing.T) {
	var (
		providerID              = common.NewID("provider").String()
		credID                  = ""
		applicationCredentialID = "bff03fee-1886-4e4c-8d18-02f7e2e8ac96"
	)
	session := &mockclient.Session{}
	session.On("GetApplicationCredential", providerID, credID, applicationCredentialID).Return(&expectedApplicationCredentials[1], nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/applicationCredentials/%s", providerID, applicationCredentialID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actual providers.ApplicationCredential
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedApplicationCredentials[1], actual)
		}
	}
}

func TestGetImage(t *testing.T) {
	var (
		providerID = common.NewID("provider").String()
		credID     = ""
		imageID    = "bff03fee-1886-4e4c-8d18-02f7e2e8ac96"
	)
	session := &mockclient.Session{}
	session.On("GetImage", providerID, credID, imageID).Return(&expectedImages[1], nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/images/%s", providerID, imageID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		var actual providers.Image
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedImages[1], actual)
		}
	}
}

func TestGetFlavor(t *testing.T) {
	var (
		providerID = common.NewID("provider").String()
		credID     = ""
		flavorID   = "08daa0be-c4a7-11eb-8529-0242ac130003"
	)
	session := &mockclient.Session{}
	session.On("GetFlavor", providerID, credID, flavorID).Return(&expectedFlavors[1], nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/flavors/%s", providerID, flavorID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		actual := providers.Flavor{}
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedFlavors[1], actual)
		}
	}
}

func TestGetProject(t *testing.T) {
	var (
		providerID = common.NewID("provider").String()
		credID     = ""
		projectID  = "08daa0be-c4a7-11eb-8529-0242ac130003"
	)
	session := &mockclient.Session{}
	session.On("GetProject", providerID, credID, projectID).Return(&expectedProjects[1], nil)
	client := createMockClientForOpenStackProvider(session)

	path := fmt.Sprintf("/providers/%s/projects/%s", providerID, projectID)
	request := httptest.NewRequest(http.MethodGet, path, nil)
	request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	recorder := createRecorderForOpenStackProvider(client, request)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))

	body, err := io.ReadAll(recorder.Body)
	if assert.NoError(t, err) {
		actual := providers.Project{}
		err = json.Unmarshal(body, &actual)
		if assert.NoError(t, err) {
			assert.Equal(t, expectedProjects[1], actual)
		}
	}
}
