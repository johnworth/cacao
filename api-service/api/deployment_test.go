package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	cacaocommon "gitlab.com/cyverse/cacao-common/common"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/clients/deployments"
	"gitlab.com/cyverse/cacao/api-service/clients/deployments/mocks"
	"gitlab.com/cyverse/cacao/api-service/constants"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

var fakeOutgoingHTTPDeployments = []hm.Deployment{
	{
		ID:                cacaocommon.NewID("deployment").String(),
		CreatedAt:         time.Now().Add(-time.Hour * 2).Truncate(time.Second),
		UpdatedAt:         time.Now().Add(-time.Hour).Truncate(time.Second),
		Owner:             "test_username",
		WorkspaceID:       cacaocommon.NewID("workspace").String(),
		TemplateID:        cacaocommon.NewID("template").String(),
		PrimaryProviderID: cacaocommon.NewID("primarycloud").String(),
	},
	{
		ID:                cacaocommon.NewID("deployment").String(),
		CreatedAt:         time.Now().Add(-time.Minute * 2).Truncate(time.Second),
		UpdatedAt:         time.Now().Add(-time.Minute).Truncate(time.Second),
		Owner:             "test_username",
		WorkspaceID:       cacaocommon.NewID("workspace").String(),
		TemplateID:        cacaocommon.NewID("template").String(),
		PrimaryProviderID: cacaocommon.NewID("primarycloud").String(),
		Parameters:        []hm.KeyValue{{Key: "foo", Value: "bar"}},
	},
}

var fakeIncomingHTTPDeployments = []hm.Deployment{
	{
		ID:                fakeOutgoingHTTPDeployments[0].ID,
		Owner:             fakeOutgoingHTTPDeployments[0].Owner,
		WorkspaceID:       fakeOutgoingHTTPDeployments[0].WorkspaceID,
		TemplateID:        fakeOutgoingHTTPDeployments[0].TemplateID,
		PrimaryProviderID: fakeOutgoingHTTPDeployments[0].PrimaryProviderID,
		CloudCredentials:  []string{cacaocommon.NewID("credential").String()},
	},
	{
		ID:                fakeOutgoingHTTPDeployments[1].ID,
		Owner:             fakeOutgoingHTTPDeployments[1].Owner,
		WorkspaceID:       fakeOutgoingHTTPDeployments[1].WorkspaceID,
		TemplateID:        fakeOutgoingHTTPDeployments[1].TemplateID,
		PrimaryProviderID: fakeOutgoingHTTPDeployments[1].PrimaryProviderID,
		CloudCredentials:  []string{cacaocommon.NewID("credential").String()},
		Parameters:        []hm.KeyValue{{Key: "foo", Value: "bar"}},
	},
}

var fakeHTTPParameters = []hm.Parameters{
	{
		Parameters: fakeIncomingHTTPDeployments[0].Parameters,
	},
	{
		Parameters: fakeIncomingHTTPDeployments[1].Parameters,
	},
}

var fakeHTTPDeploymentBuilds = [][]hm.DeploymentBuild{
	{
		{
			ID:           cacaocommon.NewID("build").String(),
			Owner:        "test_username",
			DeploymentID: fakeOutgoingHTTPDeployments[0].ID,
			Parameters:   []hm.KeyValue{},
			Status:       "extant",
			Start:        time.Now().Add(-time.Hour * 2),
			End:          time.Now().Add(-time.Hour),
		},
		{
			ID:           cacaocommon.NewID("build").String(),
			Owner:        "test_username",
			DeploymentID: fakeOutgoingHTTPDeployments[0].ID,
			Parameters:   []hm.KeyValue{},
			Status:       "superimposed",
			Start:        time.Now().Add(-time.Minute * 2),
		},
	},
	{
		{
			ID:           cacaocommon.NewID("build").String(),
			Owner:        "test_username",
			DeploymentID: fakeOutgoingHTTPDeployments[1].ID,
			Parameters:   fakeOutgoingHTTPDeployments[1].Parameters,
			Status:       "extant",
			Start:        time.Now().Add(-time.Hour * 2),
			End:          time.Now().Add(-time.Hour),
		},
		{
			ID:           cacaocommon.NewID("build").String(),
			Owner:        "test_username",
			DeploymentID: fakeOutgoingHTTPDeployments[1].ID,
			Parameters:   fakeOutgoingHTTPDeployments[1].Parameters,
			Status:       "superimposed",
			Start:        time.Now().Add(-time.Minute * 2),
		},
	},
}

var fakeHTTPDeploymentStates = [][]hm.DeploymentStateView{
	{
		{
			Resources: []hm.DeploymentResource{
				{
					ID:                  cacaocommon.NewID("resource").String(),
					Type:                "resourceType1",
					Provider:            cacaocommon.NewID("provider").String(),
					ProviderType:        "providerType1",
					Attributes:          map[string]interface{}{"foo1": "bar1"},
					SensitiveAttributes: []interface{}{"baz1"},
					AvailableActions:    []string{"web_shell"},
				},
				{
					ID:                  cacaocommon.NewID("resource").String(),
					Type:                "resourceType2",
					Provider:            cacaocommon.NewID("provider").String(),
					ProviderType:        "providerType2",
					Attributes:          map[string]interface{}{"foo2": "bar2"},
					SensitiveAttributes: []interface{}{"baz2"},
					AvailableActions:    []string{"web_desktop"},
				},
			},
		},
		{
			Resources: []hm.DeploymentResource{
				{
					ID:                  cacaocommon.NewID("resource").String(),
					Type:                "resourceType3",
					Provider:            cacaocommon.NewID("provider").String(),
					ProviderType:        "providerType3",
					Attributes:          map[string]interface{}{"foo3": "bar3"},
					SensitiveAttributes: []interface{}{"baz3"},
					AvailableActions:    []string{"shut_down"},
				},
				{
					ID:                  cacaocommon.NewID("resource").String(),
					Type:                "resourceType4",
					Provider:            cacaocommon.NewID("provider").String(),
					ProviderType:        "providerType4",
					Attributes:          map[string]interface{}{"foo4": "bar4"},
					SensitiveAttributes: []interface{}{"baz4"},
					AvailableActions:    []string{"suspend"},
				},
			},
		},
	},
	{
		{
			Resources: []hm.DeploymentResource{
				{
					ID:                  cacaocommon.NewID("resource").String(),
					Type:                "resourceType5",
					Provider:            cacaocommon.NewID("provider").String(),
					ProviderType:        "providerType5",
					Attributes:          map[string]interface{}{"foo5": "bar5"},
					SensitiveAttributes: []interface{}{"baz5"},
					AvailableActions:    []string{"web_shell"},
				},
				{
					ID:                  cacaocommon.NewID("resource").String(),
					Type:                "resourceType6",
					Provider:            cacaocommon.NewID("provider").String(),
					ProviderType:        "providerType6",
					Attributes:          map[string]interface{}{"foo6": "bar6"},
					SensitiveAttributes: []interface{}{"baz6"},
					AvailableActions:    []string{"web_desktop"},
				},
			},
		},
		{
			Resources: []hm.DeploymentResource{
				{
					ID:                  cacaocommon.NewID("resource").String(),
					Type:                "resourceType7",
					Provider:            cacaocommon.NewID("provider").String(),
					ProviderType:        "providerType7",
					Attributes:          map[string]interface{}{"foo7": "bar7"},
					SensitiveAttributes: []interface{}{"baz7"},
					AvailableActions:    []string{"shut_down"},
				},
				{
					ID:                  cacaocommon.NewID("resource").String(),
					Type:                "resourceType8",
					Provider:            cacaocommon.NewID("provider").String(),
					ProviderType:        "providerType8",
					Attributes:          map[string]interface{}{"foo8": "bar8"},
					SensitiveAttributes: []interface{}{"baz8"},
					AvailableActions:    []string{"suspend"},
				},
			},
		},
	},
}

var fakeHTTPDeploymentRuns = [][]hm.DeploymentRun{
	{
		{
			ID:               cacaocommon.NewID("run").String(),
			Owner:            "test_username",
			DeploymentID:     fakeOutgoingHTTPDeployments[0].ID,
			Parameters:       fakeOutgoingHTTPDeployments[0].Parameters,
			CloudCredentials: []string{cacaocommon.NewID("cred").String()},
			GitCredentialID:  cacaocommon.NewID("cred").String(),
			LastState:        fakeHTTPDeploymentStates[0][0],
			Status:           "errored",
			Start:            time.Now().Add(-time.Hour * 2),
			End:              time.Now().Add(-time.Hour),
		},
		{
			ID:               cacaocommon.NewID("run").String(),
			Owner:            "test_username",
			DeploymentID:     fakeOutgoingHTTPDeployments[0].ID,
			Parameters:       fakeOutgoingHTTPDeployments[0].Parameters,
			CloudCredentials: []string{cacaocommon.NewID("cred").String()},
			GitCredentialID:  cacaocommon.NewID("cred").String(),
			LastState:        fakeHTTPDeploymentStates[0][1],
			Status:           "running",
			Start:            time.Now().Add(-time.Minute * 2),
		},
	},
	{
		{
			ID:               cacaocommon.NewID("run").String(),
			Owner:            "test_username",
			DeploymentID:     fakeOutgoingHTTPDeployments[1].ID,
			Parameters:       fakeOutgoingHTTPDeployments[1].Parameters,
			CloudCredentials: []string{cacaocommon.NewID("cred").String()},
			GitCredentialID:  cacaocommon.NewID("cred").String(),
			LastState:        fakeHTTPDeploymentStates[1][0],
			Status:           "errored",
			Start:            time.Now().Add(-time.Hour * 2),
			End:              time.Now().Add(-time.Hour),
		},
		{
			ID:               cacaocommon.NewID("run").String(),
			Owner:            "test_username",
			DeploymentID:     fakeOutgoingHTTPDeployments[1].ID,
			Parameters:       fakeOutgoingHTTPDeployments[1].Parameters,
			CloudCredentials: []string{cacaocommon.NewID("cred").String()},
			GitCredentialID:  cacaocommon.NewID("cred").String(),
			LastState:        fakeHTTPDeploymentStates[1][1],
			Status:           "running",
			Start:            time.Now().Add(-time.Minute * 2),
		},
	},
}

var fakeHTTPDeploymentRunRequest = hm.DeploymentRun{
	BuildID:          cacaocommon.NewID("build").String(),
	Parameters:       fakeIncomingHTTPDeployments[1].Parameters,
	CloudCredentials: fakeHTTPDeploymentRuns[1][0].CloudCredentials,
	GitCredentialID:  fakeHTTPDeploymentRuns[1][0].GitCredentialID,
}

// createRequestForDefaultUser creates a request with the default CACAO auth headers for testing.
// TODO: this isn't specific to testing deployments endpoints. Find a better home for this function.
func createRequestForDefaultUser(assert *assert.Assertions, method, urlPath string, body []byte) *http.Request {

	// Create the reader for the request body.
	var reader io.Reader
	if body != nil {
		reader = ioutil.NopCloser(bytes.NewReader(body))
	}

	// Build the HTTP request.
	req, err := http.NewRequest(method, urlPath, reader)
	assert.NoError(err)

	// Add the CACAO auth headers
	req.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
	req.Header.Set(constants.RequestHeaderCacaoEmulator, "")
	req.Header.Set(constants.RequestHeaderCacaoAdmin, "false")

	return req
}

// processDeploymentsRequest processes a request and returns a recorder containing information about the HTTP response.
func processDeploymentsRequest(client deployments.Client, request *http.Request) *httptest.ResponseRecorder {
	recorder := httptest.NewRecorder()
	router := mux.NewRouter()
	DeploymentAPIRouter(client, router)
	router.ServeHTTP(recorder, request)
	return recorder
}

// processDeploymentsRequestForDefaultUser creates and processes a request with the default CACAO auth headers for
// testing. A recorder containing information about the HTTP response is returned.
func processDeploymentsRequestForDefaultUser(
	assert *assert.Assertions, client deployments.Client, method, urlPath string, body []byte,
) *httptest.ResponseRecorder {
	request := createRequestForDefaultUser(assert, method, urlPath, body)
	return processDeploymentsRequest(client, request)
}

// testFailureBeforeDeploymentsSessionCreation tests a case where the endpoint fails before the call to create the
// deployments client session.
func testFailureBeforeDeploymentsSessionCreation(
	t *testing.T, method, urlPath string, body []byte, expectedStatusCode int,
) {
	assert := assert.New(t)

	// Create the mock deployments client.
	client := new(mocks.Client)

	// Create and process the incoming request.
	recorder := processDeploymentsRequestForDefaultUser(assert, client, method, urlPath, body)

	// Verify that all expectations were met.
	client.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(expectedStatusCode, recorder.Code)
}

// testDeploymentsSessionCreationFailure tests a case where the deployment session can't be created.
func testDeploymentsSessionCreationFailure(t *testing.T, method, urlPath string, body []byte) {
	assert := assert.New(t)

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(nil, service.NewCacaoGeneralError("something bad happened"))

	// Create and process the incoming request.
	recorder := processDeploymentsRequestForDefaultUser(assert, client, method, urlPath, body)

	// Verify that all expectations were met.
	client.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusInternalServerError, recorder.Code)
}

func TestEmptyDeploymentListing(t *testing.T) {
	assert := assert.New(t)

	// Create a mock deployments session
	session := new(mocks.Session)
	session.On("ListDeployments", deployments.ListOption{
		WorkspaceID: "",
		Offset:      0,
		PageSize:    -1,
		FullRun:     false,
	}).Return(make([]hm.Deployment, 0), nil)

	// Create a mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", "/deployments", nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusOK, recorder.Code)
	assert.Equal("[]", recorder.Body.String())
}

func TestNilDeploymentListing(t *testing.T) {
	assert := assert.New(t)

	// Create a mock deployments session
	session := new(mocks.Session)
	session.On("ListDeployments", deployments.ListOption{
		WorkspaceID: "",
		Offset:      0,
		PageSize:    -1,
		FullRun:     false,
	}).Return(nil, nil)

	// Create a mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", "/deployments", nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusOK, recorder.Code)
	assert.Equal("[]", recorder.Body.String())
}

func TestNonEmptyDeploymentListing(t *testing.T) {
	assert := assert.New(t)

	// Create a mock deployments session.
	session := new(mocks.Session)
	session.On("ListDeployments", deployments.ListOption{
		WorkspaceID: "",
		Offset:      0,
		PageSize:    -1,
		FullRun:     false,
	}).Return(fakeOutgoingHTTPDeployments, nil)

	// Create a mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", "/deployments", nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusOK, recorder.Code)
	expectedResponse, err := json.Marshal(fakeOutgoingHTTPDeployments)
	assert.NoError(err)
	assert.Equal(expectedResponse, recorder.Body.Bytes())
}

func TestDeploymentListingWithOffset(t *testing.T) {
	assert := assert.New(t)
	const listOffset = 10
	// Create a mock deployments session.
	session := new(mocks.Session)
	session.On("ListDeployments", deployments.ListOption{
		WorkspaceID: "",
		Offset:      listOffset,
		PageSize:    -1,
		FullRun:     false,
	}).Return(fakeOutgoingHTTPDeployments, nil)

	// Create a mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments?offset=%d", listOffset)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusOK, recorder.Code)
	expectedResponse, err := json.Marshal(fakeOutgoingHTTPDeployments)
	assert.NoError(err)
	assert.Equal(expectedResponse, recorder.Body.Bytes())
}

func TestDeploymentListingWithPageSize(t *testing.T) {
	assert := assert.New(t)
	const listPageSize = 10
	// Create a mock deployments session.
	session := new(mocks.Session)
	session.On("ListDeployments", deployments.ListOption{
		WorkspaceID: "",
		Offset:      0,
		PageSize:    listPageSize,
		FullRun:     false,
	}).Return(fakeOutgoingHTTPDeployments, nil)

	// Create a mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments?page-size=%d", listPageSize)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusOK, recorder.Code)
	expectedResponse, err := json.Marshal(fakeOutgoingHTTPDeployments)
	assert.NoError(err)
	assert.Equal(expectedResponse, recorder.Body.Bytes())
}

func TestDeploymentListingForWorkspace(t *testing.T) {
	assert := assert.New(t)
	workspaceID := cacaocommon.ID(fakeOutgoingHTTPDeployments[0].WorkspaceID)

	// Create a mock deployments session.
	session := new(mocks.Session)
	session.On("ListDeployments", deployments.ListOption{
		WorkspaceID: workspaceID,
		Offset:      0,
		PageSize:    -1,
		FullRun:     false,
	}).Return(fakeOutgoingHTTPDeployments, nil)

	// Create a mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments?workspace-id=%s", workspaceID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusOK, recorder.Code)
	expectedResponse, err := json.Marshal(fakeOutgoingHTTPDeployments)
	assert.NoError(err)
	assert.Equal(expectedResponse, recorder.Body.Bytes())
}

func TestDeploymentListingSessionError(t *testing.T) {
	testDeploymentsSessionCreationFailure(t, "GET", "/deployments", nil)
}

func TestDeploymentListingError(t *testing.T) {
	assert := assert.New(t)

	// Create a mock deployments session.
	session := new(mocks.Session)
	session.On("ListDeployments", deployments.ListOption{
		WorkspaceID: "",
		Offset:      0,
		PageSize:    -1,
		FullRun:     false,
	}).Return(nil, service.NewCacaoGeneralError("something bad happened"))

	// Create a mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", "/deployments", nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusInternalServerError, recorder.Code)
}

func TestDeploymentListingNotImplemented(t *testing.T) {
	assert := assert.New(t)

	// Create a mock deployments session.
	session := new(mocks.Session)
	session.On("ListDeployments", deployments.ListOption{
		WorkspaceID: "",
		Offset:      0,
		PageSize:    -1,
		FullRun:     false,
	}).Return(nil, service.NewCacaoNotImplementedError("not implemented"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", "/deployments", nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusNotImplemented, recorder.Code)
}

func TestDeploymentListingWorkspaceIDValidationFailure(t *testing.T) {
	testFailureBeforeDeploymentsSessionCreation(t, "GET", "/deployments?workspace-id=quux", nil, http.StatusBadRequest)
}

func TestAddDeployment(t *testing.T) {
	assert := assert.New(t)
	expectedTID := cacaocommon.NewID("tid")

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentCreationRequest", &fakeIncomingHTTPDeployments[1]).
		Return(nil)
	session.On("AddDeployment", &fakeIncomingHTTPDeployments[1]).
		Return(expectedTID, nil)

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Marshal the request body.
	requestBody, err := json.Marshal(fakeIncomingHTTPDeployments[1])
	assert.NoError(err)

	// Create and process the incoming request.
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "POST", "/deployments", requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected status code.
	assert.Equal(http.StatusAccepted, recorder.Code)

	// Unmarshal the response body.
	var actual utils.AcceptedResponse
	err = json.Unmarshal(recorder.Body.Bytes(), &actual)
	assert.NoError(err)

	// Verify that we got back the expected fields.
	assert.Equal(expectedTID.String(), actual.TransactionID)
	assert.Equal("", actual.ID)
}

func TestAddDeploymentSessionError(t *testing.T) {
	testDeploymentsSessionCreationFailure(t, "POST", "/deployments", []byte("{}"))
}

func TestAddDeploymentUnmarshalError(t *testing.T) {
	testFailureBeforeDeploymentsSessionCreation(t, "POST", "/deployments", []byte("{"), http.StatusBadRequest)
}

func TestAddDeploymentValidationError(t *testing.T) {
	assert := assert.New(t)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentCreationRequest", &fakeIncomingHTTPDeployments[1]).
		Return(service.NewCacaoInvalidParameterError("something bad happened"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Marshal the request body.
	requestBody, err := json.Marshal(fakeIncomingHTTPDeployments[1])
	assert.NoError(err)

	// Process the request.
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "POST", "/deployments", requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected status code.
	assert.Equal(http.StatusBadRequest, recorder.Code)
}

func TestAddDeploymentValidationNotImplemented(t *testing.T) {
	assert := assert.New(t)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentCreationRequest", &fakeIncomingHTTPDeployments[1]).
		Return(service.NewCacaoNotImplementedError("not implemented"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Marshal the request body.
	requestBody, err := json.Marshal(fakeIncomingHTTPDeployments[1])
	assert.NoError(err)

	// Process the request.
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "POST", "/deployments", requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected status code.
	assert.Equal(http.StatusNotImplemented, recorder.Code)
}

func TestAddDeploymentSubmissionError(t *testing.T) {
	assert := assert.New(t)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentCreationRequest", &fakeIncomingHTTPDeployments[1]).
		Return(nil)
	session.On("AddDeployment", &fakeIncomingHTTPDeployments[1]).
		Return(cacaocommon.ID(""), service.NewCacaoGeneralError("something bad happened"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Marshal the request body.
	requestBody, err := json.Marshal(fakeIncomingHTTPDeployments[1])
	assert.NoError(err)

	// Process the request.
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "POST", "/deployments", requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected status code.
	assert.Equal(http.StatusInternalServerError, recorder.Code)
}

func TestAddDeploymentSubmissionNotImplemented(t *testing.T) {
	assert := assert.New(t)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentCreationRequest", &fakeIncomingHTTPDeployments[1]).
		Return(nil)
	session.On("AddDeployment", &fakeIncomingHTTPDeployments[1]).
		Return(cacaocommon.ID(""), service.NewCacaoNotImplementedError("not implemented"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Marshal the request body.
	requestBody, err := json.Marshal(fakeIncomingHTTPDeployments[1])
	assert.NoError(err)

	// Process the request.
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "POST", "/deployments", requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected status code.
	assert.Equal(http.StatusNotImplemented, recorder.Code)
}

func TestGetDeployment(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[0].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("GetDeployment", deploymentID).
		Return(fakeOutgoingHTTPDeployments[0], nil)

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusOK, recorder.Code)
	expectedResponse, err := json.Marshal(fakeOutgoingHTTPDeployments[0])
	assert.NoError(err)
	assert.Equal(expectedResponse, recorder.Body.Bytes())
}

func TestGetDeploymentIDValidationError(t *testing.T) {
	testFailureBeforeDeploymentsSessionCreation(t, "GET", "/deployments/quux", nil, http.StatusBadRequest)
}

func TestGetDeploymentSessionError(t *testing.T) {
	urlPath := fmt.Sprintf("/deployments/%s", fakeOutgoingHTTPDeployments[0].ID)
	testDeploymentsSessionCreationFailure(t, "GET", urlPath, nil)
}

func TestGetDeploymentFetchError(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[0].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("GetDeployment", deploymentID).
		Return(hm.Deployment{}, service.NewCacaoGeneralError("something bad happened"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusInternalServerError, recorder.Code)
}

func TestGetDeploymentNotImplemented(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[0].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("GetDeployment", deploymentID).
		Return(hm.Deployment{}, service.NewCacaoNotImplementedError("not implemented"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusNotImplemented, recorder.Code)
}

func TestGetDeploymentNotFound(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[0].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("GetDeployment", deploymentID).
		Return(hm.Deployment{}, nil)

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusNotFound, recorder.Code)
}

func TestUpdateDeploymentDeploymentIDValidationError(t *testing.T) {
	testFailureBeforeDeploymentsSessionCreation(t, "PATCH", "/deployments/quux", nil, http.StatusBadRequest)
}

func TestUpdateDeploymentUnmarshalError(t *testing.T) {
	urlPath := fmt.Sprintf("/deployments/%s", fakeOutgoingHTTPDeployments[0].ID)
	testFailureBeforeDeploymentsSessionCreation(t, "PATCH", urlPath, []byte("{"), http.StatusBadRequest)
}

func TestUpdateDeploymentSessionError(t *testing.T) {
	urlPath := fmt.Sprintf("/deployments/%s", fakeOutgoingHTTPDeployments[0].ID)
	testDeploymentsSessionCreationFailure(t, "PATCH", urlPath, []byte("{}"))
}

func TestUpdateDeploymentValidationFailure(t *testing.T) {
	assert := assert.New(t)
	longName := strings.Repeat("a", 101)
	testUpdate := hm.DeploymentUpdate{Name: &longName}
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentUpdateRequest", deploymentID, &testUpdate).
		Return(service.NewCacaoInvalidParameterError("something bad happened"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Marshal the request body.
	requestBody, err := json.Marshal(testUpdate)
	assert.NoError(err)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "PATCH", urlPath, requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusBadRequest, recorder.Code)

}

func TestUpdateDeploymentSubmissionFailure(t *testing.T) {
	assert := assert.New(t)
	newName := "new-name"
	testUpdate := hm.DeploymentUpdate{Name: &newName}
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentUpdateRequest", deploymentID, &testUpdate).
		Return(nil)
	session.On("UpdateDeployment", deploymentID, &testUpdate).
		Return(cacaocommon.ID(""), service.NewCacaoGeneralError("something bad happened"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Marshal the request body.
	requestBody, err := json.Marshal(testUpdate)
	assert.NoError(err)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "PATCH", urlPath, requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusInternalServerError, recorder.Code)
}

func TestUpdateDeploymentSuccess(t *testing.T) {
	assert := assert.New(t)
	newName := "new-name"
	testUpdate := hm.DeploymentUpdate{Name: &newName}
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	expectedTID := cacaocommon.NewID("tid")

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentUpdateRequest", deploymentID, &testUpdate).
		Return(nil)
	session.On("UpdateDeployment", deploymentID, &testUpdate).
		Return(expectedTID, nil)

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Marshal the request body.
	requestBody, err := json.Marshal(testUpdate)
	assert.NoError(err)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "PATCH", urlPath, requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusAccepted, recorder.Code)

	// Unmarshal the response body.
	var actual utils.AcceptedResponse
	err = json.Unmarshal(recorder.Body.Bytes(), &actual)
	assert.NoError(err)

	// Verify that we got back the expected fields.
	assert.Equal(expectedTID.String(), actual.TransactionID)
	assert.Equal("", actual.ID)
}

func TestDeleteDeploymentIDValidationError(t *testing.T) {
	testFailureBeforeDeploymentsSessionCreation(t, "DELETE", "/deployments/quux", nil, http.StatusBadRequest)
}

func TestDeleteDeploymentSessionError(t *testing.T) {
	urlPath := fmt.Sprintf("/deployments/%s", fakeOutgoingHTTPDeployments[0].ID)
	testDeploymentsSessionCreationFailure(t, "DELETE", urlPath, nil)
}

func TestDeleteDeploymentValidationError(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[0].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentDeletionRequest", deploymentID).
		Return(service.NewCacaoInvalidParameterError("something bad happened"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "DELETE", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusBadRequest, recorder.Code)
}

func TestDeleteDeploymentValidationNotImplemented(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[0].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentDeletionRequest", deploymentID).
		Return(service.NewCacaoNotImplementedError("not implemented"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "DELETE", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusNotImplemented, recorder.Code)
}

func TestDeleteDeploymentSubmissionError(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentDeletionRequest", deploymentID).
		Return(nil)
	session.On("DeleteDeployment", deploymentID).
		Return(cacaocommon.ID(""), false, service.NewCacaoGeneralError("something bad happened"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "DELETE", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusInternalServerError, recorder.Code)
}

func TestDeleteDeploymentSubmissionNotImplemented(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentDeletionRequest", deploymentID).
		Return(nil)
	session.On("DeleteDeployment", deploymentID).
		Return(cacaocommon.ID(""), false, service.NewCacaoNotImplementedError("not implemented"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "DELETE", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusNotImplemented, recorder.Code)
}

func TestDeleteDeploymentStarted(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	expectedTID := cacaocommon.NewID("tid")

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentDeletionRequest", deploymentID).
		Return(nil)
	session.On("DeleteDeployment", deploymentID).
		Return(expectedTID, false, nil) // false indicate deletion has started, but not finished

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "DELETE", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusAccepted, recorder.Code)

	// Unmarshal the response body.
	var actual utils.AcceptedResponse
	err := json.Unmarshal(recorder.Body.Bytes(), &actual)
	assert.NoError(err)

	// Verify that we got back the expected fields.
	assert.Equal(expectedTID.String(), actual.TransactionID)
	assert.Equal(fakeOutgoingHTTPDeployments[1].ID, actual.ID)
}

func TestDeleteDeploymentSuccess(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	expectedTID := cacaocommon.NewID("tid")

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentDeletionRequest", deploymentID).
		Return(nil)
	session.On("DeleteDeployment", deploymentID).
		Return(expectedTID, true, nil)

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "DELETE", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusOK, recorder.Code)

	// Unmarshal the response body.
	var actual utils.AcceptedResponse
	err := json.Unmarshal(recorder.Body.Bytes(), &actual)
	assert.NoError(err)
	fmt.Println(actual)

	// Verify that we got back the expected fields.
	assert.Equal(expectedTID.String(), actual.TransactionID)
	assert.Equal(fakeOutgoingHTTPDeployments[1].ID, actual.ID)
}

func TestListDeploymentBuildsIDValidationError(t *testing.T) {
	testFailureBeforeDeploymentsSessionCreation(t, "GET", "/deployments/quux/builds", nil, http.StatusBadRequest)
}

func TestListDeploymentBuildsSessionError(t *testing.T) {
	urlPath := fmt.Sprintf("/deployments/%s/builds", fakeOutgoingHTTPDeployments[0].ID)
	testDeploymentsSessionCreationFailure(t, "GET", urlPath, nil)
}

func TestListDeploymentBuildsListingError(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ListDeploymentBuilds", deploymentID).
		Return(nil, service.NewCacaoGeneralError("something bad happened"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/builds", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusInternalServerError, recorder.Code)
}

func TestListDeploymentBuildsListingNotImplemented(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ListDeploymentBuilds", deploymentID).
		Return(nil, service.NewCacaoNotImplementedError("not implemented"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/builds", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusNotImplemented, recorder.Code)
}

func TestListDeploymentBuildsSuccess(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	deploymentBuilds := fakeHTTPDeploymentBuilds[1]

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ListDeploymentBuilds", deploymentID).
		Return(deploymentBuilds, nil)

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/builds", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusOK, recorder.Code)
	expectedResponse, err := json.Marshal(deploymentBuilds)
	assert.NoError(err)
	assert.Equal(expectedResponse, recorder.Body.Bytes())
}

func TestListDeploymentBuildsNil(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ListDeploymentBuilds", deploymentID).
		Return(nil, nil)

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/builds", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusOK, recorder.Code)
	assert.Equal([]byte("[]"), recorder.Body.Bytes())
}

func TestRebuildDeploymentIDValidationError(t *testing.T) {
	testFailureBeforeDeploymentsSessionCreation(t, "POST", "/deployments/quux/builds", nil, http.StatusBadRequest)
}

func TestRebuildDeploymentUnmarshalError(t *testing.T) {
	urlPath := fmt.Sprintf("/deployments/%s/builds", fakeOutgoingHTTPDeployments[0].ID)
	testFailureBeforeDeploymentsSessionCreation(t, "POST", urlPath, []byte("{"), http.StatusBadRequest)
}

func TestRebuildDeploymentSessionError(t *testing.T) {
	urlPath := fmt.Sprintf("/deployments/%s/builds", fakeOutgoingHTTPDeployments[0].ID)
	testDeploymentsSessionCreationFailure(t, "POST", urlPath, []byte("{}"))
}

func TestRebuildDeploymentValidationNotImplemented(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	params := fakeHTTPParameters[1]

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentRebuildRequest", deploymentID, &params).
		Return(service.NewCacaoNotImplementedError("not implemented"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Marshal the request body.
	requestBody, err := json.Marshal(params)
	assert.NoError(err)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/builds", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "POST", urlPath, requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusNotImplemented, recorder.Code)
}

func TestRebuildDeploymentValidationError(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	params := fakeHTTPParameters[1]

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentRebuildRequest", deploymentID, &params).
		Return(service.NewCacaoInvalidParameterError("something bad happened"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Marshal the request body.
	requestBody, err := json.Marshal(params)
	assert.NoError(err)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/builds", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "POST", urlPath, requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusBadRequest, recorder.Code)
}

func TestRebuildDeploymentSubmissionNotImplemented(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	params := fakeHTTPParameters[1]

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentRebuildRequest", deploymentID, &params).
		Return(nil)
	session.On("RebuildDeployment", deploymentID, &params).
		Return(cacaocommon.ID(""), service.NewCacaoNotImplementedError("not implemented"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Marshal the request body.
	requestBody, err := json.Marshal(params)
	assert.NoError(err)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/builds", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "POST", urlPath, requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusNotImplemented, recorder.Code)
}

func TestRebuildDeploymentSubmissionFailure(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	params := fakeHTTPParameters[1]

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentRebuildRequest", deploymentID, &params).
		Return(nil)
	session.On("RebuildDeployment", deploymentID, &params).
		Return(cacaocommon.ID(""), service.NewCacaoInvalidParameterError("something bad happened"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Marshal the request body.
	requestBody, err := json.Marshal(params)
	assert.NoError(err)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/builds", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "POST", urlPath, requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusBadRequest, recorder.Code)
}

func TestRebuildDeploymentSuccess(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	params := fakeHTTPParameters[1]
	expectedTID := cacaocommon.NewID("tid")

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentRebuildRequest", deploymentID, &params).
		Return(nil)
	session.On("RebuildDeployment", deploymentID, &params).
		Return(expectedTID, nil)

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Marshal the request body.
	requestBody, err := json.Marshal(params)
	assert.NoError(err)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/builds", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "POST", urlPath, requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusAccepted, recorder.Code)

	// Unmarshal the response body.
	var actual utils.AcceptedResponse
	err = json.Unmarshal(recorder.Body.Bytes(), &actual)
	assert.NoError(err)

	// Verify that we got back the expected response fields.
	assert.Equal(expectedTID.String(), actual.TransactionID)
	assert.Equal(deploymentID.String(), actual.ID)
}

func TestGetDeploymentBuildInvalidDeploymentID(t *testing.T) {
	testFailureBeforeDeploymentsSessionCreation(t, "GET", "/deployments/quux/builds/baz", nil, http.StatusBadRequest)
}

func TestGetDeploymentBuildInvalidBuildID(t *testing.T) {
	urlPath := fmt.Sprintf("/deployments/%s/builds/baz", fakeOutgoingHTTPDeployments[0].ID)
	testFailureBeforeDeploymentsSessionCreation(t, "GET", urlPath, nil, http.StatusBadRequest)
}

func TestGetDeploymentBuildSessionError(t *testing.T) {
	deploymentID := fakeOutgoingHTTPDeployments[0].ID
	buildID := fakeHTTPDeploymentBuilds[0][1].ID
	urlPath := fmt.Sprintf("/deployments/%s/builds/%s", deploymentID, buildID)
	testDeploymentsSessionCreationFailure(t, "GET", urlPath, nil)
}

func TestGetDeploymentBuildNotImplemented(t *testing.T) {
	assert := assert.New(t)
	testDeployment := fakeOutgoingHTTPDeployments[0]
	deploymentID := cacaocommon.ID(testDeployment.ID)
	testBuild := fakeHTTPDeploymentBuilds[0][0]
	buildID := cacaocommon.ID(testBuild.ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("GetDeploymentBuild", deploymentID, buildID).
		Return(hm.DeploymentBuild{}, service.NewCacaoNotImplementedError("not implemented"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/builds/%s", deploymentID, buildID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusNotImplemented, recorder.Code)
}

func TestGetDeploymentBuildError(t *testing.T) {
	assert := assert.New(t)
	testDeployment := fakeOutgoingHTTPDeployments[0]
	deploymentID := cacaocommon.ID(testDeployment.ID)
	testBuild := fakeHTTPDeploymentBuilds[0][0]
	buildID := cacaocommon.ID(testBuild.ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("GetDeploymentBuild", deploymentID, buildID).
		Return(hm.DeploymentBuild{}, service.NewCacaoGeneralError("something bad happened"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/builds/%s", deploymentID, buildID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusInternalServerError, recorder.Code)
}

func TestGetDeploymentBuildSuccess(t *testing.T) {
	assert := assert.New(t)
	testDeployment := fakeOutgoingHTTPDeployments[1]
	deploymentID := cacaocommon.ID(testDeployment.ID)
	testBuild := fakeHTTPDeploymentBuilds[1][0]
	buildID := cacaocommon.ID(testBuild.ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("GetDeploymentBuild", deploymentID, buildID).
		Return(testBuild, nil)

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/builds/%s", deploymentID, buildID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusOK, recorder.Code)
	expectedResponse, err := json.Marshal(testBuild)
	assert.NoError(err)
	assert.Equal(expectedResponse, recorder.Body.Bytes())
}

func TestListDeploymentRunsInvalidDeploymentID(t *testing.T) {
	testFailureBeforeDeploymentsSessionCreation(t, "GET", "/deployments/quux/runs", nil, http.StatusBadRequest)
}

func TestListDeploymentRunsSessionError(t *testing.T) {
	urlPath := fmt.Sprintf("/deployments/%s/runs", fakeOutgoingHTTPDeployments[0].ID)
	testDeploymentsSessionCreationFailure(t, "GET", urlPath, nil)
}

func TestListDeploymentRunsNotImplemented(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[0].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ListDeploymentRuns", deploymentID).
		Return(nil, service.NewCacaoNotImplementedError("not implemented"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/runs", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusNotImplemented, recorder.Code)
}

func TestListDeploymentRunsError(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[0].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ListDeploymentRuns", deploymentID).
		Return(nil, service.NewCacaoGeneralError("something bad happened"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/runs", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusInternalServerError, recorder.Code)
}

func TestListDeploymentRunsSuccess(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	testRuns := fakeHTTPDeploymentRuns[1]

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ListDeploymentRuns", deploymentID).
		Return(testRuns, nil)

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/runs", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusOK, recorder.Code)
	expectedResponse, err := json.Marshal(testRuns)
	assert.NoError(err)
	assert.Equal(expectedResponse, recorder.Body.Bytes())
}

func TestListDeploymentRunsNil(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ListDeploymentRuns", deploymentID).
		Return(nil, nil)

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/runs", deploymentID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusOK, recorder.Code)
	assert.Equal([]byte("[]"), recorder.Body.Bytes())
}

func TestRunDeploymentIDValidationError(t *testing.T) {
	testFailureBeforeDeploymentsSessionCreation(t, "POST", "/deployments/quux/runs", nil, http.StatusBadRequest)
}

func TestRunDeploymentUnmarshalError(t *testing.T) {
	urlPath := fmt.Sprintf("/deployments/%s/runs", fakeOutgoingHTTPDeployments[0].ID)
	testFailureBeforeDeploymentsSessionCreation(t, "POST", urlPath, []byte("{"), http.StatusBadRequest)
}

func TestRunDeploymentSessionError(t *testing.T) {
	urlPath := fmt.Sprintf("/deployments/%s/runs", fakeOutgoingHTTPDeployments[0].ID)
	testDeploymentsSessionCreationFailure(t, "POST", urlPath, []byte("{}"))
}

func TestRunDeploymentValidationNotImplemented(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	runRequest := fakeHTTPDeploymentRunRequest
	runRequest.DeploymentID = deploymentID.String()

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentRunRequest", deploymentID, &runRequest).
		Return(service.NewCacaoNotImplementedError("not implemented"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/runs", deploymentID)
	requestBody, err := json.Marshal(runRequest)
	assert.NoError(err)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "POST", urlPath, requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusNotImplemented, recorder.Code)
}

func TestRunDeploymentValidationError(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	runRequest := fakeHTTPDeploymentRunRequest
	runRequest.DeploymentID = deploymentID.String()

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentRunRequest", deploymentID, &runRequest).
		Return(service.NewCacaoInvalidParameterError("something bad happened"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/runs", deploymentID)
	requestBody, err := json.Marshal(runRequest)
	assert.NoError(err)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "POST", urlPath, requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusBadRequest, recorder.Code)
}

func TestRunDeploymentSubmissionNotImplemented(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	runRequest := fakeHTTPDeploymentRunRequest
	runRequest.DeploymentID = deploymentID.String()

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentRunRequest", deploymentID, &runRequest).
		Return(nil)
	session.On("RunDeployment", deploymentID, &runRequest).
		Return(cacaocommon.ID(""), service.NewCacaoNotImplementedError("not implemented"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/runs", deploymentID)
	requestBody, err := json.Marshal(runRequest)
	assert.NoError(err)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "POST", urlPath, requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusNotImplemented, recorder.Code)
}

func TestRunDeploymentSubmissionError(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	runRequest := fakeHTTPDeploymentRunRequest
	runRequest.DeploymentID = deploymentID.String()

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentRunRequest", deploymentID, &runRequest).
		Return(nil)
	session.On("RunDeployment", deploymentID, &runRequest).
		Return(cacaocommon.ID(""), service.NewCacaoGeneralError("something bad happened"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/runs", deploymentID)
	requestBody, err := json.Marshal(runRequest)
	assert.NoError(err)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "POST", urlPath, requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusInternalServerError, recorder.Code)
}

func TestRunDeploymentSuccess(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	runRequest := fakeHTTPDeploymentRunRequest
	runRequest.DeploymentID = deploymentID.String()
	expectedTID := cacaocommon.NewID("tid")

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("ValidateDeploymentRunRequest", deploymentID, &runRequest).
		Return(nil)
	session.On("RunDeployment", deploymentID, &runRequest).
		Return(expectedTID, nil)

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/runs", deploymentID)
	requestBody, err := json.Marshal(runRequest)
	assert.NoError(err)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "POST", urlPath, requestBody)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusAccepted, recorder.Code)

	// Unmarshal the response body.
	var actual utils.AcceptedResponse
	err = json.Unmarshal(recorder.Body.Bytes(), &actual)
	assert.NoError(err)

	// Verify that we got back the expected fields.
	assert.Equal(expectedTID.String(), actual.TransactionID)
	assert.Equal("", actual.ID)
}

func TestGetDeploymentRunDeploymentIDValidationError(t *testing.T) {
	testFailureBeforeDeploymentsSessionCreation(t, "GET", "/deployments/quux/runs/baz", nil, http.StatusBadRequest)
}

func TestGetDeploymentRunIDValidationError(t *testing.T) {
	urlPath := fmt.Sprintf("/deployments/%s/runs/baz", fakeOutgoingHTTPDeployments[1].ID)
	testFailureBeforeDeploymentsSessionCreation(t, "GET", urlPath, nil, http.StatusBadRequest)
}

func TestGetDeploymentRunSessionError(t *testing.T) {
	urlPath := fmt.Sprintf("/deployments/%s/runs/%s", fakeOutgoingHTTPDeployments[1].ID, fakeHTTPDeploymentRuns[1][0].ID)
	testDeploymentsSessionCreationFailure(t, "GET", urlPath, nil)
}

func TestGetDeploymentRunNotImplemented(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	runID := cacaocommon.ID(fakeHTTPDeploymentRuns[1][0].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("GetDeploymentRun", deploymentID, runID).
		Return(hm.DeploymentRun{}, service.NewCacaoNotImplementedError("not implemented"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/runs/%s", deploymentID, runID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusNotImplemented, recorder.Code)
}

func TestGetDeploymentRunError(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	runID := cacaocommon.ID(fakeHTTPDeploymentRuns[1][0].ID)

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("GetDeploymentRun", deploymentID, runID).
		Return(hm.DeploymentRun{}, service.NewCacaoGeneralError("something bad happened"))

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/runs/%s", deploymentID, runID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response code.
	assert.Equal(http.StatusInternalServerError, recorder.Code)
}

func TestGetDeploymentRunSuccess(t *testing.T) {
	assert := assert.New(t)
	deploymentID := cacaocommon.ID(fakeOutgoingHTTPDeployments[1].ID)
	runID := cacaocommon.ID(fakeHTTPDeploymentRuns[1][0].ID)
	run := fakeHTTPDeploymentRuns[1][0]

	// Create the mock deployments session.
	session := new(mocks.Session)
	session.On("GetDeploymentRun", deploymentID, runID).
		Return(run, nil)

	// Create the mock deployments client.
	client := new(mocks.Client)
	client.On("Session", "test_username", "", false).
		Return(session, nil)

	// Create and process the incoming request.
	urlPath := fmt.Sprintf("/deployments/%s/runs/%s", deploymentID, runID)
	recorder := processDeploymentsRequestForDefaultUser(assert, client, "GET", urlPath, nil)

	// Verify that all expectations were met.
	client.AssertExpectations(t)
	session.AssertExpectations(t)

	// Verify that we got back the expected response.
	assert.Equal(http.StatusOK, recorder.Code)
	expected, err := json.Marshal(run)
	assert.NoError(err)
	assert.Equal(expected, recorder.Body.Bytes())
}
