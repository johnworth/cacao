package api

import (
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/api-service/utils"
	"gitlab.com/cyverse/cacao/common"
	"net/http"
	"strings"
	"time"
)

// VersionAPIRouter VersionAPIRouter...
func VersionAPIRouter(router *mux.Router) {
	router.HandleFunc("/version", getVersion).Methods("GET")
}

var start = time.Now()

func getVersion(writer http.ResponseWriter, request *http.Request) {

	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getVersion",
	})

	logger.Info("api.GetVersion(): start")
	exPath := common.ReadLastLine(".git/logs/HEAD")
	fields := strings.Fields(exPath)

	hash := "Unable To Parse Hash"
	t := "Unable To Parse Time"
	if len(fields) > 4 {
		hash = fields[1]
		t = findTimestampFromGitLogFields(fields)
	}
	newt, err := common.MsToTime(t + "000") //hacky but works seconds to ms
	if err != nil {
		log.Error(err)
	} else {
		t = newt.String()
	}

	versionObj := map[string]interface{}{
		"hash":           hash,
		"hash_time":      t,
		"execution_time": start.String(),
	}

	utils.ReturnStatus(writer, versionObj, http.StatusOK)

}

func findTimestampFromGitLogFields(fields []string) string {
	t := "Unable To Parse Time"
	t = fields[4]
	foundEmailField := false

	// 0     1     2
	// hash1 hash2 author_name <email> timestamp ...
	// if the author name has spaces in it, then the timestamp will not be at index 4
	for _, field := range fields[2:] {
		// assume author name does not start with '<' and ends with '>'
		if field[0] == '<' && field[len(field)-1] == '>' {
			foundEmailField = true
		}
		// timestamp field is after the email field
		if field[0] != '<' && field[len(field)-1] != '>' && foundEmailField {
			t = field
			break
		}
	}
	return t
}
