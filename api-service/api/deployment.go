package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	cacaocommon "gitlab.com/cyverse/cacao-common/common"
	hm "gitlab.com/cyverse/cacao-common/http"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/clients/deployments"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

// deploymentsAPI is a single instance of the deployments API implementation.
type deploymentsAPI struct {
	deploymentsClient deployments.Client
}

// DeploymentAPIRouter creates routes for operations related to deployments.
func DeploymentAPIRouter(deploymentsClient deployments.Client, router *mux.Router) {
	d := &deploymentsAPI{deploymentsClient: deploymentsClient}

	router.HandleFunc("/deployments", d.getDeployments).Methods("GET")
	router.HandleFunc("/deployments", d.addDeployment).Methods("POST")
	router.HandleFunc("/deployments/{deployment_id}", d.getDeploymentByID).Methods("GET")
	router.HandleFunc("/deployments/{deployment_id}", d.updateDeployment).Methods("PATCH")
	router.HandleFunc("/deployments/{deployment_id}", d.deleteDeployment).Methods("DELETE")
	router.HandleFunc("/deployments/{deployment_id}/builds", d.listDeploymentBuilds).Methods("GET")
	router.HandleFunc("/deployments/{deployment_id}/builds", d.rebuildDeployment).Methods("POST")
	router.HandleFunc("/deployments/{deployment_id}/builds/{build_id}", d.getBuildInformation).Methods("GET")
	router.HandleFunc("/deployments/{deployment_id}/runs", d.listDeploymentRuns).Methods("GET")
	router.HandleFunc("/deployments/{deployment_id}/runs", d.runDeployment).Methods("POST")
	router.HandleFunc("/deployments/{deployment_id}/runs/{run_id}", d.getDeploymentRun).Methods("GET")
}

func (d *deploymentsAPI) JSONError(logger *log.Entry, w http.ResponseWriter, r *http.Request, err error) {
	logger.Error(err)

	status := http.StatusOK
	switch err.(type) {
	case *cacao_common_service.CacaoInvalidParameterError, *cacao_common_service.CacaoMarshalError:
		status = http.StatusBadRequest
	case *cacao_common_service.CacaoNotImplementedError:
		status = http.StatusNotImplemented
	case *cacao_common_service.CacaoNotFoundError:
		status = http.StatusNotFound
	case *cacao_common_service.CacaoTimeoutError:
		status = http.StatusRequestTimeout
	case *cacao_common_service.CacaoAlreadyExistError:
		status = http.StatusConflict
	case *cacao_common_service.CacaoUnauthorizedError:
		status = http.StatusUnauthorized
	default:
		status = http.StatusInternalServerError
	}

	if cerr, ok := err.(cacao_common_service.CacaoError); ok {
		utils.JSONError(w, r, string(cerr.StandardError()), cerr.ContextualError(), status)
	} else {
		utils.JSONError(w, r, "internal service error", err.Error(), status)
	}
}

// unmarshalRequest reads the request body and unmarshals it.
func (d *deploymentsAPI) unmarshalRequest(logger *log.Entry, w http.ResponseWriter, r *http.Request, dest interface{}) error {
	// Read the request body.
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		errorMessage := "unable to read request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoCommunicationError(errorMessage)
		d.JSONError(logger, w, r, cerr)
		return err
	}

	// Unmarshal the request body.
	err = json.Unmarshal(requestBody, dest)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		d.JSONError(logger, w, r, cerr)
		return err
	}

	return nil
}

// getDeploymentsClientSession obtains the deployments client session for a request. Writes an error response and
// returns nil if the session can't be created.
func (d *deploymentsAPI) getDeploymentsClientSession(logger *log.Entry, w http.ResponseWriter, r *http.Request) deployments.Session {
	actor, emulator, isAdmin := utils.GetCacaoHeaders(r)
	session, err := d.deploymentsClient.Session(actor, emulator, isAdmin)
	if err != nil {
		logger.WithField("error", err).Error("unable to create the deployments microservice client")
		utils.JSONError(w, r, "unexpected error", err.Error(), http.StatusInternalServerError)
		return nil
	}
	return session
}

// extractID extracts an ID from the URL path and returns it, if the ID cannot be extracted, an error response is sent
// to the client, and an error is returned.
func (d *deploymentsAPI) extractID(logger *log.Entry, w http.ResponseWriter, r *http.Request, placeholder, idType string) (cacaocommon.ID, error) {
	deploymentID := cacaocommon.ID(mux.Vars(r)[placeholder])
	if !deploymentID.Validate() {
		errorMessage := fmt.Sprintf("invalid %s ID: %s", idType, deploymentID)
		cerr := cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
		d.JSONError(logger, w, r, cerr)
		return cacaocommon.ID(""), cerr
	}
	return deploymentID, nil
}

// getDeployments implements the GET /deployments endpoint.
func (d *deploymentsAPI) getDeployments(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getDeployments",
	})
	logger.Info("api.getDeployments(): start")
	var err error

	listOption, err := d.extractGetDeploymentsOption(r)
	if err != nil {
		d.JSONError(logger, w, r, err)
		return
	}

	// Create the deployments client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the list of deployments
	deploymentList, err := session.ListDeployments(listOption)
	if err != nil {
		errorMessage := fmt.Sprintf("error listing deployments: %s", err.Error())
		logger.WithField("error", err).Error(errorMessage)
		d.JSONError(logger, w, r, err)
		return
	}

	// Guard against ListDeployments returning nil.
	if deploymentList == nil {
		deploymentList = make([]hm.Deployment, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, deploymentList, http.StatusOK)
}

func (d *deploymentsAPI) extractGetDeploymentsOption(r *http.Request) (deployments.ListOption, error) {
	// The listing may optionally be filtered by workspace ID.
	workspaceID := cacaocommon.ID(r.URL.Query().Get("workspace-id"))
	if workspaceID != "" {
		if !workspaceID.Validate() {
			err := cacao_common_service.NewCacaoInvalidParameterError(fmt.Sprintf("invalid workspace ID: %s", workspaceID))
			return deployments.ListOption{}, err
		}
	}
	var err error
	var fullRun bool
	fullRunStr := r.URL.Query().Get("full-run")
	if fullRunStr != "" {
		fullRun, err = strconv.ParseBool(fullRunStr)
		if err != nil {
			fullRun = false
		}
	}
	var offset int64
	offsetStr := r.URL.Query().Get("offset")
	if offsetStr != "" {
		offset, err = strconv.ParseInt(offsetStr, 10, 32)
		if err != nil || offset < 0 {
			offset = 0
		}
	}
	var pageSize int64
	pageSizeStr := r.URL.Query().Get("page-size")
	if pageSizeStr == "" {
		pageSize = -1
	} else {
		pageSize, err = strconv.ParseInt(pageSizeStr, 10, 32)
		if err != nil || pageSize == 0 {
			pageSize = 100
		}
	}
	var allCurrentStatus bool
	allCurrentStatusStr := r.URL.Query().Get("all-status")
	if allCurrentStatusStr != "" {
		allCurrentStatus, err = strconv.ParseBool(allCurrentStatusStr)
		if err != nil {
			allCurrentStatus = false
		}
	}
	var currentStatus string
	if !allCurrentStatus {
		// only process status flag if all-status is not set
		currentStatus = r.URL.Query().Get("status")
	}
	return deployments.ListOption{
		WorkspaceID:      workspaceID,
		CurrentStatus:    currentStatus,
		AllCurrentStatus: allCurrentStatus,
		Offset:           int(offset),
		PageSize:         int(pageSize),
		FullRun:          fullRun,
	}, nil
}

// addDeployment implements the POST /deployments endpoint.
func (d *deploymentsAPI) addDeployment(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "addDeployment",
	})
	logger.Info("api.addDeployment(): start")
	var err error

	// Unmarshal the request body.
	var incomingRequest hm.Deployment
	err = d.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the deployments session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the deployment creation request.
	err = session.ValidateDeploymentCreationRequest(&incomingRequest)
	if err != nil {
		errorMessage := "deployment creation request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		d.JSONError(logger, w, r, err)
		return
	}

	// Submit the creation request.
	tid, err := session.AddDeployment(&incomingRequest)
	if err != nil {
		errorMessage := "deployment creation request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		d.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// getDeploymentByID returns the deployment with the specified ID if it exists and the authenticated user may view it.
func (d *deploymentsAPI) getDeploymentByID(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getDeployments",
	})
	logger.Info("api.getDeploymentByID(): start")
	var err error

	// Extract the deployment ID from the request and validate it.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}

	// Create the deployments client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the deployment.
	deployment, err := session.GetDeployment(deploymentID)
	if err != nil {
		errorMessage := "error obtaining deployment information"
		logger.WithField("error", err).Error(errorMessage)
		d.JSONError(logger, w, r, err)
		return
	}

	if len(deployment.ID) == 0 {
		errorMessage := fmt.Sprintf("deployment not found: %s", deploymentID)
		cerr := cacao_common_service.NewCacaoNotFoundError(errorMessage)
		d.JSONError(logger, w, r, cerr)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, deployment, http.StatusOK)
}

// updateDeployment updates a deployment.
func (d *deploymentsAPI) updateDeployment(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateDeployment",
	})
	logger.Info("api.updateDeployment(): start")

	// Extract the deployment ID from the request and validate it.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest hm.DeploymentUpdate
	err = d.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the deployments client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the deployment update request.
	err = session.ValidateDeploymentUpdateRequest(deploymentID, &incomingRequest)
	if err != nil {
		errorMessage := "deployment update request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		d.JSONError(logger, w, r, err)
		return
	}

	// Submit the update request.
	tid, err := session.UpdateDeployment(deploymentID, &incomingRequest)
	if err != nil {
		errorMessage := "deployment update request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		d.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

func (d *deploymentsAPI) deleteDeployment(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deleteDeployment",
	})
	logger.Info("api.deleteDeployment(): start")

	// Extract the deployment ID from the request and validate it.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}

	// Create the deployments client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and vaidate the deployment deletion request.
	err = session.ValidateDeploymentDeletionRequest(deploymentID)
	if err != nil {
		errorMessage := "deployment deletion request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		d.JSONError(logger, w, r, err)
		return
	}

	// Submit the deletion request.
	tid, deleted, err := session.DeleteDeployment(deploymentID)
	if err != nil {
		errorMessage := "deployment deletion request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		d.JSONError(logger, w, r, err)
		return
	}
	if deleted {
		// If deployment is deleted then return 200 OK
		body := utils.NewAcceptedResponse(deploymentID.String(), tid.String())
		utils.ReturnStatus(w, body, http.StatusOK)
		return
	}

	// If deployment is still in deletion process, then return 202 ACCEPTED
	body := utils.NewAcceptedResponse(deploymentID.String(), tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

func (d *deploymentsAPI) listDeploymentBuilds(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listDeploymentBuilds",
	})
	logger.Info("api.listDeploymentBuilds(): start")

	// Extract the deployment ID from the request and validate it.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}

	// Create the deployment client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// List the deployment builds.
	builds, err := session.ListDeploymentBuilds(deploymentID)
	if err != nil {
		errorMessage := "deployment build listing failed"
		logger.WithField("error", err).Error(errorMessage)
		d.JSONError(logger, w, r, err)
		return
	}

	// Guard against ListDeploymentBuild returning nil.
	if builds == nil {
		builds = make([]hm.DeploymentBuild, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, builds, http.StatusOK)
}

func (d *deploymentsAPI) rebuildDeployment(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "rebuildDeployment",
	})
	logger.Info("api.rebuildDeployment(): start")

	// Extract the deployment ID from the request and validate it.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest hm.Parameters
	err = d.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the deployment client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the rebuild request.
	err = session.ValidateDeploymentRebuildRequest(deploymentID, &incomingRequest)
	if err != nil {
		errorMessage := "deployment rebuild request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		d.JSONError(logger, w, r, err)
		return
	}

	// Submit the rebuild request.
	tid, err := session.RebuildDeployment(deploymentID, &incomingRequest)
	if err != nil {
		errorMessage := "deployment rebuild request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		d.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse(deploymentID.String(), tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

func (d *deploymentsAPI) getBuildInformation(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getBuildInformation",
	})
	logger.Info("api.getBuildInformation(): start")

	// Extract and validate the deployment ID.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}

	// Extract and validate the build ID.
	buildID, err := d.extractID(logger, w, r, "build_id", "build")
	if err != nil {
		return
	}

	// Create the deployment client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the deployment build information.
	build, err := session.GetDeploymentBuild(deploymentID, buildID)
	if err != nil {
		errorMessage := "deployment build information retrieval failed"
		logger.WithField("error", err).Error(errorMessage)
		d.JSONError(logger, w, r, err)
		return
	}

	if len(build.ID) == 0 {
		errorMessage := fmt.Sprintf("deployment build not found: %s", buildID)
		cerr := cacao_common_service.NewCacaoNotFoundError(errorMessage)
		d.JSONError(logger, w, r, cerr)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, build, http.StatusOK)
}

func (d *deploymentsAPI) listDeploymentRuns(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listDeploymentRuns",
	})
	logger.Info("api.listDeploymentRuns(): start")

	// Extract and validate the deployment ID.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}

	// Create the deployment client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Obtain the list of deployment runs.
	runs, err := session.ListDeploymentRuns(deploymentID)
	if err != nil {
		errorMessage := "deployment run listing failed"
		logger.WithField("error", err).Error(errorMessage)
		d.JSONError(logger, w, r, err)
		return
	}

	// Guard against ListDeploymentRuns returning nil.
	if runs == nil {
		runs = make([]hm.DeploymentRun, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, runs, http.StatusOK)
}

func (d *deploymentsAPI) runDeployment(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "runDeployment",
	})
	logger.Info("api.runDeployment(): start")

	// Extract and validate the deployment ID.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest hm.DeploymentRun
	err = d.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}
	incomingRequest.DeploymentID = deploymentID.String() // use deployment ID in the URL

	// Get the deployments client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the deployment run request.
	err = session.ValidateDeploymentRunRequest(deploymentID, &incomingRequest)
	if err != nil {
		errorMessage := "deployment run request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		d.JSONError(logger, w, r, err)
		return
	}

	// Submit the request.
	tid, err := session.RunDeployment(deploymentID, &incomingRequest)
	if err != nil {
		errorMessage := "deployment run request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		d.JSONError(logger, w, r, err)
		return
	}

	// Format and return the response.
	body := utils.NewAcceptedResponse("", tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

func (d *deploymentsAPI) getDeploymentRun(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getDeploymentRun",
	})
	logger.Info("api.getDeploymentRun(): start")

	// Extract and validate the deployment ID and run ID.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}
	runID, err := d.extractID(logger, w, r, "run_id", "run")
	if err != nil {
		return
	}

	// Get the deployments client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the deployment run.
	run, err := session.GetDeploymentRun(deploymentID, runID)
	if err != nil {
		errorMessage := "deployment run information retrieval failed"
		logger.WithField("error", err).Error(errorMessage)
		d.JSONError(logger, w, r, err)
		return
	}

	if len(run.ID) == 0 {
		errorMessage := fmt.Sprintf("deployment run not found: %s", runID)
		cerr := cacao_common_service.NewCacaoNotFoundError(errorMessage)
		d.JSONError(logger, w, r, cerr)
		return
	}

	// Return the response.
	utils.ReturnStatus(w, run, http.StatusOK)
}
