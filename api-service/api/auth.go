package api

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/api-service/authentication"
	"gitlab.com/cyverse/cacao/api-service/constants"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

var (
	// unused	tokenStore = map[string]string{}

	// Authenticator contains the current driver used by api service
	Authenticator authentication.AuthDriver
)

// getErrorResponseCode returns the response code to use for the given error.
func getErrorResponseCode(err error) int {
	if httpError, ok := err.(*utils.HTTPError); ok {
		return httpError.Code()
	}
	return http.StatusUnauthorized
}

// AuthAPIRouter will map the necessary auth functions to the endpoints
func AuthAPIRouter(router *mux.Router) {
	Authenticator.AddRoutes(router)
}

// AuthenticationMiddleware is a middleware function required on some routes to
// make sure a user is authenticated and authorizaed to access that endpoint
func AuthenticationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger := log.WithFields(log.Fields{
			"package":  "api",
			"function": "AuthenticationMiddleware",
		})

		user, err := Authenticator.Authenticate(r.Header)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to authenticate request")
			utils.JSONError(w, r, err.Error(), err.Error(), getErrorResponseCode(err))
			return
		}

		// As a precaution, let's delete these headers, just in case (though Header.Set should be enough)
		log.Trace("api.AuthenticationMiddleware: user authenticated, setting headers")
		r.Header.Del(constants.RequestHeaderCacaoUser)
		r.Header.Del(constants.RequestHeaderCacaoAdmin)
		r.Header.Del(constants.RequestHeaderCacaoEmulator)

		r.Header.Set(constants.RequestHeaderCacaoUser, user.GetUsername())
		r.Header.Set(constants.RequestHeaderCacaoAdmin, fmt.Sprintf("%t", user.GetIsAdmin()))

		if user.GetSessionEmulator() != "" {
			r.Header.Set(constants.RequestHeaderCacaoEmulator, user.GetSessionEmulator())
		}
		next.ServeHTTP(w, r)
	})
}
