package ports

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/storage"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// ReplySink is a sink to send the query reply into
type ReplySink interface {
	Init(messaging.NatsConfig) error
	Reply(subject string, reply cloudevents.Event) error
}

// QuerySrc is a source of incoming query to be processed
type QuerySrc interface {
	Init(messaging.NatsConfig) error
	InitChannel(chan<- types.Query)
	Start() error
}

// DeploymentStorage is storage for Deployment objects
type DeploymentStorage interface {
	Init() error
	Create(deploymentcommon.Deployment) error
	Get(common.ID) (deploymentcommon.Deployment, error)
	List(filters storage.DeploymentFilter, offset int64, limit int64, sort storage.DeploymentSort) ([]deploymentcommon.Deployment, error)
}

// DeploymentBuildStorage is storage for DeploymentBuild
type DeploymentBuildStorage interface {
	// TODO implement later
}

// DeploymentRunStorage is storage for DeploymentRun objects
type DeploymentRunStorage interface {
	Init() error
	Get(runID common.ID) (deploymentcommon.DeploymentRun, error)
	// GetLatestRun returns the latest run of a deployment if any.
	GetLatestRun(deployment common.ID) (*deploymentcommon.DeploymentRun, error)
	List(filter storage.DeploymentRunFilter, pagination service.RequestPagination) ([]deploymentcommon.DeploymentRun, error)
	BatchGet(runIDs []common.ID) ([]deploymentcommon.DeploymentRun, error)
}
