package domain

import (
	"context"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

const queryChannDepth = 5
const workerCount = 5

// Domain is the entrypoint object of this microservice
type Domain struct {
	NatsSrc    ports.QuerySrc
	NatsSink   ports.ReplySink
	queryChan  chan types.Query
	Storage    ports.DeploymentStorage
	RunStorage ports.DeploymentRunStorage
	conf       types.Config
}

// Init initialize all adapters, and domain internals
func (svc *Domain) Init(envConf types.EnvConfig, conf types.Config) error {

	svc.conf = conf

	if err := svc.Storage.Init(); err != nil {
		return err
	}

	if err := svc.RunStorage.Init(); err != nil {
		return err
	}

	if err := svc.NatsSrc.Init(conf.NatsConfig); err != nil {
		return err
	}

	svc.queryChan = make(chan types.Query, queryChannDepth)
	svc.NatsSrc.InitChannel(svc.queryChan)
	return nil
}

// Start starts accepting incoming queries and processing them
func (svc Domain) Start() error {
	err := svc.NatsSrc.Start()
	if err != nil {
		return err
	}

	svc.spawnQueryWorker(workerCount)
	return nil
}

func (svc Domain) spawnQueryWorker(workerCount int) {
	var wg sync.WaitGroup
	for i := 0; i < workerCount; i++ {
		wg.Add(1)
		worker := NewQueryWorker(svc.conf.NatsConfig.ClientID, svc.NatsSink, svc.Storage, svc.RunStorage)
		worker.Run(&wg, svc.queryChan)
	}
	wg.Wait()
}

// Reply is reply to query
type Reply interface {
	ToCloudEvent(source string) (cloudevents.Event, error)
}

// QueryHandler handles a particular type of query, and generate a replies for query
type QueryHandler interface {
	Handle(query types.Query) Reply
	HandleStream(query types.Query) []Reply
	QueryOp() common.QueryOp
}

// QueryWorker process queries from channel
type QueryWorker struct {
	getHandler     GetQueryHandler
	listHandler    ListQueryHandler
	getRunHandler  GetRunHandler
	listRunHandler ListRunHandler
	getLogs        GetLogsQueryHandler
	getRawState    GetRawStateQueryHandler

	clientID string
	sink     ports.ReplySink
}

// NewQueryWorker ...
func NewQueryWorker(
	clientID string,
	sink ports.ReplySink,
	storage ports.DeploymentStorage,
	runStorage ports.DeploymentRunStorage) QueryWorker {

	var w QueryWorker

	commonDependencies := handlerCommon{perm: NewPermissionCheck(storage)}
	w.getHandler = GetQueryHandler{handlerCommon: commonDependencies, storage: storage, runStorage: runStorage}
	w.listHandler = ListQueryHandler{handlerCommon: commonDependencies, storage: storage, runStorage: runStorage}
	w.getRunHandler = GetRunHandler{handlerCommon: commonDependencies, storage: runStorage}
	w.listRunHandler = ListRunHandler{handlerCommon: commonDependencies, storage: runStorage}
	w.getLogs = GetLogsQueryHandler{handlerCommon: commonDependencies, storage: runStorage}
	w.getRawState = GetRawStateQueryHandler{handlerCommon: commonDependencies, storage: runStorage}
	w.sink = sink
	w.clientID = clientID
	return w
}

// Run process queries from channel
func (w QueryWorker) Run(wg *sync.WaitGroup, queryChan <-chan types.Query) {
	defer wg.Done()

	for query := range queryChan {
		handler := w.SelectHandler(query.QueryType())
		if handler == nil {
			log.WithField("queryType", query.QueryType()).Trace("unsupported query")
			continue
		}
		replies := handler.HandleStream(query)
		for _, reply := range replies {
			w.ReplyToQuery(query, reply)
		}
	}
}

// SelectHandler returns appropriate handler for query based on its operation.
func (w QueryWorker) SelectHandler(op common.QueryOp) QueryHandler {
	switch op {
	case service.DeploymentGetQueryType:
		return w.getHandler
	case service.DeploymentListQueryType:
		return w.listHandler
	case service.DeploymentGetRawStateQueryType:
		return w.getRawState
	case service.DeploymentGetLogsQueryType:
		return w.getLogs
	case service.DeploymentGetRunQueryType:
		return w.getRunHandler
	case service.DeploymentListRunQueryType:
		return w.listRunHandler
	default:
		return nil
	}
}

// ReplyToQuery sends out the reply
func (w QueryWorker) ReplyToQuery(query types.Query, reply Reply) {
	logger := log.WithFields(log.Fields{
		"query":    query.QueryType(),
		"querySrc": query.CloudEvent().Source(),
	})

	replyCe, err := reply.ToCloudEvent(w.clientID)
	if err != nil {
		logger.WithError(err).Error("fail to marshal reply to cloudevent")
		return
	}

	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*3)
	err = retry(ctx, 5, func() error {
		return w.sink.Reply(query.ReplySubject(), replyCe)
	})
	cancel()

	if err != nil {
		logger.WithError(err).Error("fail to send reply")
	} else {
		logger.Info("query replied")
	}
}

// retry a specified function for upto specified times within the specified context
func retry(ctx context.Context, retryTimes int, fn func() error) error {
	var err error
	for i := 0; i < retryTimes; i++ {
		err = fn()
		if err == nil {
			return nil
		}
		select {
		case <-ctx.Done():
			return nil
		default:
		}
	}
	return err
}
