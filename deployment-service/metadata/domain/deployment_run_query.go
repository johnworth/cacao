package domain

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/storage"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// GetRunHandler is handler for GetRun query
type GetRunHandler struct {
	storage ports.DeploymentRunStorage
	handlerCommon
}

// Handle handles the query
func (h GetRunHandler) Handle(query types.Query) Reply {
	logger := log.WithFields(log.Fields{
		"query": "getRun",
	})
	var getRunQuery service.DeploymentGetRunQuery
	err := json.Unmarshal(query.CloudEvent().Data(), &getRunQuery)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to DeploymentGetRunQuery"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error())))
	}

	run, err := h.storage.Get(getRunQuery.Run)
	if err != nil {
		logger.Error(err)
		return h.createErrorReply(err)
	}

	ok, err := h.perm.DeploymentAccessByID(getRunQuery.GetSessionActor(), run.Deployment)
	if err != nil {
		errorMessage := "permission check failed"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoGeneralError(errorMessage))
	} else if !ok {
		errorMessage := "not authorized"
		logger.Error(errorMessage)
		return h.createErrorReply(service.NewCacaoUnauthorizedError(errorMessage))
	}

	return h.createReply(run)
}

// HandleStream handles the query
func (h GetRunHandler) HandleStream(query types.Query) []Reply {
	return []Reply{h.Handle(query)}
}

// QueryOp ...
func (h GetRunHandler) QueryOp() common.QueryOp {
	return service.DeploymentGetRunQueryType
}

func (h GetRunHandler) createErrorReply(err error) service.DeploymentGetRunReply {
	var reply service.DeploymentGetRunReply

	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	reply.ServiceError = serviceError.GetBase()
	return reply
}

func (h GetRunHandler) createReply(run deploymentcommon.DeploymentRun) service.DeploymentGetRunReply {
	return service.DeploymentGetRunReply{
		Session:    service.Session{},
		Deployment: run.Deployment,
		Run:        run.ConvertToExternal(),
	}
}

// ListRunHandler is handler for ListRun query
type ListRunHandler struct {
	storage ports.DeploymentRunStorage
	handlerCommon
}

// Handle handles the query
func (h ListRunHandler) Handle(query types.Query) Reply {
	panic("not implemented")
}

// HandleStream handles the query
func (h ListRunHandler) HandleStream(query types.Query) []Reply {
	logger := log.WithFields(log.Fields{
		"query": "listRun",
	})

	var listRunQuery service.DeploymentListRunQuery
	err := json.Unmarshal(query.CloudEvent().Data(), &listRunQuery)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to DeploymentListRunQuery"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error())))
	}

	ok, err := h.perm.DeploymentAccessByID(listRunQuery.GetSessionActor(), listRunQuery.Deployment)
	if err != nil {
		errorMessage := "permission check failed"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoGeneralError(errorMessage))
	} else if !ok {
		errorMessage := "not authorized"
		logger.Error(errorMessage)
		return h.createErrorReply(service.NewCacaoUnauthorizedError(errorMessage))
	}

	filter := storage.DeploymentRunFilter{
		Deployment: listRunQuery.Deployment,
	}
	runs, err := h.storage.List(filter, listRunQuery.RequestPagination)
	if err != nil {
		return h.createErrorReply(err)
	}

	return h.createReplies(listRunQuery.Offset, runs)
}

// QueryOp ...
func (h ListRunHandler) QueryOp() common.QueryOp {
	return service.DeploymentGetRunQueryType
}

func (h ListRunHandler) createErrorReply(err error) []Reply {
	var reply service.DeploymentListRunReply
	reply.ReplyListStream = service.ReplyListStream{
		TotalReplies: 1,
		ReplyIndex:   0,
	}

	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	reply.ServiceError = serviceError.GetBase()
	return []Reply{reply}
}

func (h ListRunHandler) createReplies(initialOffset int, runs []deploymentcommon.DeploymentRun) []Reply {
	replies := make([]Reply, 0)
	offsetCounter := initialOffset

	if len(runs) == 0 {
		return []Reply{h.createEmptyReply(initialOffset)}
	} else if len(runs) < types.DeploymentRunStreamThreshold {
		return []Reply{h.singleReply(initialOffset, runs)}
	}
	allChunks := h.divideList(h.convertRunsToExternal(runs))
	for chunkIndex, chunk := range allChunks {
		replies = append(replies,
			service.DeploymentListRunReply{
				Session: service.Session{},
				Offset:  offsetCounter,
				Count:   len(chunk),
				Runs:    chunk,
				ReplyListStream: service.ReplyListStream{
					TotalReplies: len(allChunks),
					ReplyIndex:   chunkIndex,
				},
			})
		offsetCounter += len(chunk)
	}
	return replies
}

func (h ListRunHandler) createEmptyReply(initialOffset int) service.DeploymentListRunReply {
	return service.DeploymentListRunReply{
		Session: service.Session{},
		Offset:  initialOffset,
		Count:   0,
		Runs:    []service.DeploymentRun{},
		ReplyListStream: service.ReplyListStream{
			TotalReplies: 1,
			ReplyIndex:   0,
		},
	}
}

func (h ListRunHandler) singleReply(initialOffset int, runs []deploymentcommon.DeploymentRun) service.DeploymentListRunReply {
	return service.DeploymentListRunReply{
		Session: service.Session{},
		Offset:  initialOffset,
		Count:   len(runs),
		Runs:    h.convertRunsToExternal(runs),
		ReplyListStream: service.ReplyListStream{
			TotalReplies: 1,
			ReplyIndex:   0,
		},
	}
}

func (h ListRunHandler) convertRunsToExternal(runs []deploymentcommon.DeploymentRun) []service.DeploymentRun {
	extRuns := make([]service.DeploymentRun, 0, len(runs))
	for _, run := range runs {
		extRuns = append(extRuns, run.ConvertToExternal())
	}
	return extRuns
}

func (h ListRunHandler) divideList(runs []service.DeploymentRun) [][]service.DeploymentRun {

	allChunks := make([][]service.DeploymentRun, 0)
	var chunk []service.DeploymentRun
	var start = 0
	var end = types.DeploymentRunStreamThreshold
	for start < len(runs) {
		chunk = runs[start:end]
		allChunks = append(allChunks, chunk)
		start = end
		if start+types.DeploymentRunStreamThreshold >= len(runs) {
			end = len(runs)
		} else {
			end = start + types.DeploymentRunStreamThreshold
		}
	}
	return allChunks
}

// GetRawStateQueryHandler handles GetRawState query
type GetRawStateQueryHandler struct {
	storage ports.DeploymentRunStorage
	handlerCommon
}

// QueryOp ...
func (h GetRawStateQueryHandler) QueryOp() common.QueryOp {
	return service.DeploymentGetRawStateQueryType
}

// Handle handles the query
func (h GetRawStateQueryHandler) Handle(query types.Query) Reply {
	logger := log.WithFields(log.Fields{
		"query": "getRawState",
	})
	var getRawState service.DeploymentGetRawStateQuery
	err := json.Unmarshal(query.CloudEvent().Data(), &getRawState)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to DeploymentGetRawStateQuery"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error())))
	}
	logger = logger.WithFields(log.Fields{
		"deployment": getRawState.Deployment,
		"actor":      getRawState.SessionActor,
		"emulator":   getRawState.SessionEmulator,
	})

	run, err := h.storage.Get(getRawState.Run)
	if err != nil {
		logger.Error(err)
		return h.createErrorReply(err)
	}

	ok, err := h.perm.DeploymentAccessByID(getRawState.GetSessionActor(), run.Deployment)
	if err != nil {
		errorMessage := "permission check failed"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoGeneralError(errorMessage))
	} else if !ok {
		errorMessage := "not authorized"
		logger.Error(errorMessage)
		return h.createErrorReply(service.NewCacaoUnauthorizedError(errorMessage))
	}

	if !run.HasRawState() {
		return h.createErrorReply(service.NewCacaoNotFoundError("deployment raw state not available"))
	}

	return h.createReply(*run.RawState)
}

// HandleStream handles the query
func (h GetRawStateQueryHandler) HandleStream(query types.Query) []Reply {
	return []Reply{h.Handle(query)}
}

func (h GetRawStateQueryHandler) createErrorReply(err error) service.DeploymentGetRawStateReply {
	var reply service.DeploymentGetRawStateReply

	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	reply.ServiceError = serviceError.GetBase()
	reply.RawState = nil
	return reply
}

func (h GetRawStateQueryHandler) createReply(rawState deploymentcommon.RawDeploymentState) service.DeploymentGetRawStateReply {
	var rawStateBytes []byte
	var err error
	switch rawState.Type {
	case deploymentcommon.TerraformRawState:
		rawStateBytes, err = json.Marshal(rawState.TFState)
		if err != nil {
			errorMessage := fmt.Sprintf("unable to unmarshal JSON bytes to TerraformRawState - %s", err.Error())
			return h.createErrorReply(service.NewCacaoMarshalError(errorMessage))
		}
	default:
		rawStateBytes = nil
	}

	return service.DeploymentGetRawStateReply{
		Session:  service.Session{},
		RawState: rawStateBytes,
	}
}

// GetLogsQueryHandler handles GetLogs query
type GetLogsQueryHandler struct {
	storage ports.DeploymentRunStorage
	handlerCommon
}

// QueryOp ...
func (h GetLogsQueryHandler) QueryOp() common.QueryOp {
	return service.DeploymentGetLogsQueryType
}

// Handle handles the query
func (h GetLogsQueryHandler) Handle(query types.Query) Reply {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "GetLogsQueryHandler.Handle",
	})
	var getLogsQuery service.DeploymentGetLogsQuery
	err := json.Unmarshal(query.CloudEvent().Data(), &getLogsQuery)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to DeploymentGetLogsQuery"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error())))
	}

	run, err := h.storage.Get(getLogsQuery.Run)
	if err != nil {
		logger.Error(err)
		return h.createErrorReply(err)
	}

	ok, err := h.perm.DeploymentAccessByID(getLogsQuery.GetSessionActor(), run.Deployment)
	if err != nil {
		errorMessage := "permission check failed"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoGeneralError(errorMessage))
	} else if !ok {
		errorMessage := "not authorized"
		logger.Error(errorMessage)
		return h.createErrorReply(service.NewCacaoUnauthorizedError(errorMessage))
	}

	if !run.HasLogs() {
		return h.createErrorReply(service.NewCacaoNotFoundError("deployment logs not available"))
	}

	return h.createReply(*run.Logs)
}

// HandleStream handles the query
func (h GetLogsQueryHandler) HandleStream(query types.Query) []Reply {
	return []Reply{h.Handle(query)}
}

func (h GetLogsQueryHandler) createErrorReply(err error) service.DeploymentGetLogsReply {
	var reply service.DeploymentGetLogsReply

	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	reply.ServiceError = serviceError.GetBase()
	return reply
}

func (h GetLogsQueryHandler) createReply(logs string) service.DeploymentGetLogsReply {
	return service.DeploymentGetLogsReply{
		Session: service.Session{},
		Logs:    logs,
	}
}
