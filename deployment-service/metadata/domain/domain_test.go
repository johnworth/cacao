package domain

import (
	"fmt"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/adapters"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	"reflect"
	"testing"
)

func TestQueryWorker_SelectHandler(t *testing.T) {
	type fields struct {
		getHandler  GetQueryHandler
		listHandler ListQueryHandler
		getRun      GetRunHandler
		listRun     ListRunHandler
		getLogs     GetLogsQueryHandler
		getRawState GetRawStateQueryHandler
	}
	type args struct {
		op common.QueryOp
	}
	allFields := fields{
		getHandler:  GetQueryHandler{},
		listHandler: ListQueryHandler{},
		getLogs:     GetLogsQueryHandler{},
		getRawState: GetRawStateQueryHandler{},
		getRun:      GetRunHandler{},
		listRun:     ListRunHandler{},
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   QueryHandler
	}{
		{"get", allFields, args{service.DeploymentGetQueryType}, allFields.getHandler},
		{"list", allFields, args{service.DeploymentListQueryType}, allFields.listHandler},
		{"getLogs", allFields, args{service.DeploymentGetLogsQueryType}, allFields.getLogs},
		{"getRawState", allFields, args{service.DeploymentGetRawStateQueryType}, allFields.getRawState},
		{"getRun", allFields, args{service.DeploymentGetRunQueryType}, allFields.getRun},
		{"listRun", allFields, args{service.DeploymentListRunQueryType}, allFields.listRun},
		{"unknown", allFields, args{common.QueryOp("unknown")}, nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := QueryWorker{
				getHandler:     tt.fields.getHandler,
				listHandler:    tt.fields.listHandler,
				getRunHandler:  tt.fields.getRun,
				listRunHandler: tt.fields.listRun,
				getLogs:        tt.fields.getLogs,
				getRawState:    tt.fields.getRawState,
				clientID:       "",
				sink:           nil,
			}
			if got := w.SelectHandler(tt.args.op); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SelectHandler() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQueryWorker_ReplyToQuery(t *testing.T) {
	type fields struct {
		getHandler  GetQueryHandler
		listHandler ListQueryHandler
		getRun      GetRunHandler
		listRun     ListRunHandler
		getLogs     GetLogsQueryHandler
		getRawState GetRawStateQueryHandler
		clientID    string
		sink        *adapters.NatsMock
	}
	type args struct {
		query types.Query
		reply Reply
	}
	generateReplySuject := func() string {
		return common.NewID("inbox").String()
	}
	createCloudEvent := func(queryOp common.QueryOp, data ...interface{}) cloudevents.Event {
		ce := cloudevents.NewEvent()
		ce.SetType(string(queryOp))
		if len(data) > 0 {
			err := ce.SetData(cloudevents.ApplicationJSON, data[0])
			assert.NoError(t, err)
		}
		return ce
	}

	tests := []struct {
		name     string
		fields   fields
		args     args
		hasReply bool
	}{
		{"getDeployment", fields{clientID: "client-12345", sink: adapters.NewNatsMock()},
			args{query: adapters.NewQuery(generateReplySuject(), createCloudEvent(service.DeploymentGetQueryType)), reply: service.DeploymentGetReply{}},
			true,
		},
		{"listDeployment", fields{clientID: "client-12345", sink: adapters.NewNatsMock()},
			args{query: adapters.NewQuery(generateReplySuject(), createCloudEvent(service.DeploymentListQueryType)), reply: service.DeploymentListReply{}},
			true,
		},
		{"getRawState", fields{clientID: "client-12345", sink: adapters.NewNatsMock()},
			args{query: adapters.NewQuery(generateReplySuject(), createCloudEvent(service.DeploymentGetRawStateQueryType)), reply: service.DeploymentGetRawStateReply{}},
			true,
		},
		{"getLogs", fields{clientID: "client-12345", sink: adapters.NewNatsMock()},
			args{query: adapters.NewQuery(generateReplySuject(), createCloudEvent(service.DeploymentGetLogsQueryType)), reply: service.DeploymentGetLogsReply{}},
			true,
		},
		{"getRun", fields{clientID: "client-12345", sink: adapters.NewNatsMock()},
			args{query: adapters.NewQuery(generateReplySuject(), createCloudEvent(service.DeploymentGetRunQueryType)), reply: service.DeploymentGetRunReply{}},
			true,
		},
		{"listRun", fields{clientID: "client-12345", sink: adapters.NewNatsMock()},
			args{query: adapters.NewQuery(generateReplySuject(), createCloudEvent(service.DeploymentListRunQueryType)), reply: service.DeploymentListRunReply{}},
			true},
		{"marshal failed", fields{clientID: "client-12345", sink: adapters.NewNatsMock()},
			args{query: adapters.NewQuery(generateReplySuject(), createCloudEvent(service.DeploymentGetQueryType)), reply: replyWithChannel{queryOp: service.DeploymentGetQueryType}},
			false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := QueryWorker{
				getHandler:     tt.fields.getHandler,
				listHandler:    tt.fields.listHandler,
				getRunHandler:  tt.fields.getRun,
				listRunHandler: tt.fields.listRun,
				getLogs:        tt.fields.getLogs,
				getRawState:    tt.fields.getRawState,
				clientID:       tt.fields.clientID,
				sink:           tt.fields.sink,
			}
			w.ReplyToQuery(tt.args.query, tt.args.reply)
			reply := tt.fields.sink.GetReply(tt.args.query.ReplySubject())
			if tt.hasReply {
				assert.NotNil(t, reply, "no reply")
				if reply != nil {
					assert.Equal(t, string(tt.args.query.QueryType()), reply.Type())
				}
			} else {
				assert.Nil(t, reply)
			}
		})
	}
}

// this struct cannot marshal to JSON
type replyWithChannel struct {
	queryOp common.QueryOp
	Channel chan int
}

func (r replyWithChannel) ToCloudEvent(source string) (cloudevents.Event, error) {
	ce, err := messaging.CreateCloudEvent(r, string(r.queryOp), source)
	fmt.Println("marshalErr:", err)
	return ce, err
}
