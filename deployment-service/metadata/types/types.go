package types

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/messaging"
)

// Query is the the query type that is passed in between domain and storage via ports
type Query interface {
	QueryType() common.QueryOp
	CloudEvent() cloudevents.Event
	ReplySubject() string
}

// ID prefix
const (
	// DeploymentIDPrefix is prefix for ID for deployment objects
	DeploymentIDPrefix = "deployment"
	// RunIDPrefix is prefix for ID for deployment run objects
	RunIDPrefix = "run"
)

// EnvConfig is configuration via environment variable
type EnvConfig struct {
	PodName  string `envconfig:"POD_NAME"`
	LogLevel string `envconfig:"LOG_LEVEL" default:"trace"`
}

// Config is configuration via config file
type Config struct {
	NatsConfig  messaging.NatsConfig
	MongoConfig db.MongoDBConfig
}

// DeploymentWildcardSubject is the wildcard subject this microservice subcribe to for all incoming queries
const DeploymentWildcardSubject common.QueryOp = "cyverse.deployment.*"

const (
	// DeploymentStreamThreshold is a threshold for streaming deployments in multiple replies.
	// If num of deployments in the list exceeds this threshold, start streaming result in multiple replies.
	// 220 is after accounting for the LastRun field in the deployment object.
	DeploymentStreamThreshold = 220
	// DeploymentRunStreamThreshold is a threshold for streaming runs in multiple replies.
	// If num of runs in the list exceeds this threshold, start streaming result in multiple replies.
	DeploymentRunStreamThreshold = 1000
)
