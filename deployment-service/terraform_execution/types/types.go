package types

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
)

// IncomingEvent is a incoming event to be handled
type IncomingEvent interface {
	EventType() common.EventType
	Transaction() common.TransactionID
	CloudEvent() cloudevents.Event
}

// OutgoingEvent is a outgoing event to be published
type OutgoingEvent interface {
	EventType() common.EventType
	Transaction() common.TransactionID
	ToCloudEvent(source string) (cloudevents.Event, error)
}

// EnvConfig ...
type EnvConfig struct {
	PodName  string `envconfig:"POD_NAME"`
	LogLevel string `envconfig:"LOG_LEVEL" default:"trace"`
}

// Config ...
type Config struct {
	StanConfig StanConfig
	// STAN config for AWM
	AWMStanConfig StanConfig
	MongoConfig   db.MongoDBConfig
}

// StanConfig is config for STAN
type StanConfig struct {
	messaging.NatsConfig
	messaging.StanConfig
}

// ID prefix
const (
	// DeploymentIDPrefix is prefix for ID for deployment objects
	DeploymentIDPrefix = "deployment"
	// RunIDPrefix is prefix for ID for deployment run objects
	RunIDPrefix = "run"
)

// TerraformWorkflowFilename is the filename for Terraform workflow
const TerraformWorkflowFilename string = "terraform.yml"

// Actor is the user that performs certain action.
type Actor struct {
	Actor    string
	Emulator string
}

// ActorFromSession construct Actor from service.Session
func ActorFromSession(session service.Session) Actor {
	return Actor{
		Actor:    session.GetSessionActor(),
		Emulator: session.GetSessionEmulator(),
	}
}
