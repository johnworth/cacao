package ports

import (
	"context"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/storage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// EventSink is a sink to publish events into
type EventSink interface {
	Init(types.StanConfig) error
	Publish(types.OutgoingEvent) error
}

// EventSrc is a sink to publish events into
type EventSrc interface {
	Init(types.StanConfig) error
	InitChannel(chan<- types.IncomingEvent)
	Start() error
}

// NatsSink is a sink to publish NATS msg into
type NatsSink interface {
	Init(messaging.NatsConfig) error
	Request(subject string, req cloudevents.Event) (*cloudevents.Event, error)
}

// DeploymentStorage is storage for deployment objects
type DeploymentStorage interface {
	Init() error
	Get(id common.ID) (deploymentcommon.Deployment, error)
	SearchByDeletionWorkflow(wfName string) (deploymentcommon.Deployment, error)
	Create(deployment deploymentcommon.Deployment) error
	Update(id common.ID, update storage.DeploymentUpdate) error
	StatusActive(id common.ID) error
	StatusCreationFailed(id common.ID) error
	StatusDeletionErrored(id common.ID) error
	StatusDeleted(id common.ID) error
	PendingStatusCreating(id common.ID) (bool, error)
	PendingStatusDeleting(id common.ID) (bool, error)
	UpdateLastRun(id common.ID, run common.ID) error
	StartDeletionWorkflow(id common.ID, tid common.TransactionID, wfName string, awmProvider common.ID) error
}

// DeploymentRunStorage is storage for DeploymentRun objects
type DeploymentRunStorage interface {
	Init() error
	Get(runID common.ID) (deploymentcommon.DeploymentRun, error)
	GetLatestRun(deployment common.ID) (*deploymentcommon.DeploymentRun, error)
	DeploymentHasRun(deployment common.ID) (bool, error)
	SearchByWorkflow(wfName string) (deploymentcommon.DeploymentRun, error)
	Create(run deploymentcommon.DeploymentRun) (runID common.ID, err error)
	FailedToStartPreflight(runID common.ID, err error) error
	StartPreflightWorkflow(runID common.ID, wfName string, awmProvider common.ID) error
	PreflightWorkflowSucceed(runID common.ID, wfName, wfStatus string) error
	PreflightWorkflowFailed(runID common.ID, wfName, wfStatus string) error
	StartExecWorkflow(runID common.ID, wfName string, awmProvider common.ID) error
	FailToStartExecWorkflow(runID common.ID, err error) error
	ExecWorkflowSucceed(
		runID common.ID,
		wfName string,
		wfStatus string,
		runStatus deploymentcommon.DeploymentRunStatus,
		rawState *deploymentcommon.RawDeploymentState,
		stateView deploymentcommon.DeploymentStateView,
	) error
	ExecWorkflowFailed(runID common.ID, wfName string, wfStatus string) error
}

// WorkspaceMicroservice is an interface to interact with Workspace service
type WorkspaceMicroservice interface {
	Get(actor types.Actor, id common.ID) (service.Workspace, error)
}

// TemplateMicroservice is an interface to interact with Template service
type TemplateMicroservice interface {
	Get(actor types.Actor, id common.ID) (service.Template, error)
}

// ProviderMicroservice is an interface to interact with Provider service
type ProviderMicroservice interface {
	Get(actor types.Actor, id common.ID) (service.Provider, error)
	List(actor types.Actor) ([]service.Provider, error)
}

// CredentialMicroservice is an interface to interact with Credential service
type CredentialMicroservice interface {
	Get(actor types.Actor, owner, id string) (service.Credential, error)
	// List return a list of credential ID
	List(actor types.Actor, owner string) ([]service.Credential, error)
}

// ArgoWorkflowMediator is an interface to interact with ArgoWorkflowMediator
type ArgoWorkflowMediator interface {
	Create(ctx context.Context, provider common.ID, username string, workflowFilename string, opts ...awmclient.AWMWorkflowCreateOpt) (wfName string, err error)
	CreateAsync(provider common.ID, username string, workflowFilename string, opts ...awmclient.AWMWorkflowCreateOpt) error
}

// KeySrc is an way to get encryption key
type KeySrc interface {
	GetKey() []byte
}

// Ports contains a ref to most ports, this struct exist to ease passing dependency as parameter.
type Ports struct {
	EventSink  EventSink
	Storage    DeploymentStorage
	RunStorage DeploymentRunStorage
	KeySrc     KeySrc

	TemplateMS   TemplateMicroservice
	WorkspaceMS  WorkspaceMicroservice
	CredentialMS CredentialMicroservice
	ProviderMS   ProviderMicroservice
	AWM          ArgoWorkflowMediator
}
