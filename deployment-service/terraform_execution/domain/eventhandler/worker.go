package eventhandler

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/executionstage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/preflightstage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"sync"
)

// DoNotSent this constant is for event that should not be sent, but has to return an event
//
// FIXME consider making return response event optional, not all cases have a valid response event.
const DoNotSent common.EventType = "DoNotSentThis"

// EventWorker is a worker that will process incoming events
type EventWorker struct {
	eventSink          ports.EventSink
	creationHandler    CreationHandler
	runCreationHandler RunCreationHandler
	deletionHandler    DeletionHandler
	updateHandler      UpdateHandler
	wfSucceededHandler WorkflowSucceededHandler
	wfFailedHandler    WorkflowFailedHandler
}

// NewEventWorker create a event worker
func NewEventWorker(portsDependency ports.Ports, preflightChan chan<- preflightstage.PreflightRequest, executionChan chan<- executionstage.ExecutionRequest) EventWorker {
	return EventWorker{
		eventSink:          portsDependency.EventSink,
		creationHandler:    NewCreationHandler(portsDependency),
		runCreationHandler: NewRunCreationHandler(portsDependency, preflightChan),
		deletionHandler:    NewDeletionHandler(portsDependency),
		updateHandler:      NewUpdateHandler(portsDependency.Storage),
		wfSucceededHandler: NewWorkflowSucceededHandler(portsDependency, executionChan),
		wfFailedHandler:    NewWorkflowFailedHandler(portsDependency),
	}
}

// Run starts processing incoming events from the channel
func (w EventWorker) Run(wg *sync.WaitGroup, eventChan <-chan types.IncomingEvent) {
	defer wg.Done()

	for event := range eventChan {
		handler := w.SelectEventHandler(event)
		if handler != nil {
			response := handler.Handle(event)
			if response.EventType() == DoNotSent {
				// ignore
				continue
			}
			err := w.eventSink.Publish(response)
			if err != nil {
				log.WithFields(log.Fields{
					"incoming": event.EventType(),
					"outgoing": response.EventType(),
					"err":      err.Error(),
				}).Error("fail to publish response event")
			}
		}
	}
}

// SelectEventHandler selects an EventHandler for the event
func (w EventWorker) SelectEventHandler(event types.IncomingEvent) EventHandler {
	switch event.EventType() {
	case service.DeploymentCreationRequested:
		return w.creationHandler
	case service.DeploymentCreateRunRequested:
		return w.runCreationHandler
	case service.DeploymentDeletionRequested:
		return w.deletionHandler
	case service.DeploymentUpdateRequested:
		return w.updateHandler
	case awmclient.WorkflowSucceededEvent:
		return w.wfSucceededHandler
	case awmclient.WorkflowFailedEvent:
		return w.wfFailedHandler
	default:
		return nil
	}
}
