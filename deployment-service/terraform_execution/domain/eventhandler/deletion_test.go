package eventhandler

import (
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/rs/xid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"testing"
	"time"
)

// IncomingEvent ...
type IncomingEvent struct {
	ce cloudevents.Event
}

// NewIncomingEvent ...
func NewIncomingEvent(eventType common.EventType, event interface{}) IncomingEvent {
	ce, err := messaging.CreateCloudEventWithTransactionID(event, string(eventType), "test", messaging.NewTransactionID())
	if err != nil {
		panic(err)
	}
	return IncomingEvent{ce: ce}
}

func (i IncomingEvent) EventType() common.EventType {
	return common.EventType(i.ce.Type())
}

func (i IncomingEvent) Transaction() common.TransactionID {
	return messaging.GetTransactionID(&i.ce)
}

func (i IncomingEvent) CloudEvent() cloudevents.Event {
	return i.ce
}

func TestDeletionHandler_Handle(t *testing.T) {
	t.Run("no run", testDeleteStandAloneDeployment)
	t.Run("not authorized", testDeleteDeploymentNotAuthorized)
	t.Run("with run", testDeleteDeploymentWithRun)
	t.Run("deletion workflow failed", testDeleteDeploymentWorkflowFailed)
}

func testDeleteStandAloneDeployment(t *testing.T) {
	storageMock := &portsmocks.DeploymentStorage{}
	runStorageMock := &portsmocks.DeploymentRunStorage{}
	keySrcMock := &portsmocks.KeySrc{}
	templateMSMock := &portsmocks.TemplateMicroservice{}
	credentialMSMock := &portsmocks.CredentialMicroservice{}
	awmMock := &portsmocks.ArgoWorkflowMediator{}
	var portsDependency = ports.Ports{
		EventSink:    nil,
		Storage:      storageMock,
		RunStorage:   runStorageMock,
		KeySrc:       keySrcMock,
		TemplateMS:   templateMSMock,
		WorkspaceMS:  nil,
		CredentialMS: credentialMSMock,
		ProviderMS:   nil,
		AWM:          awmMock,
	}
	var username = "test-user123"
	var deploymentID = common.NewID(service.DeploymentIDPrefix)
	var deployment = deploymentcommon.Deployment{
		ID:        deploymentID,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		Workspace: common.NewID("workspace"),
		CreatedBy: deploymentcommon.Creator{
			User:     username,
			Emulator: "",
		},
		Template:             common.NewID("template"),
		TemplateType:         "",
		PrimaryCloudProvider: common.NewID("provider"),
		CloudCredentials:     nil,
		GitCredential:        "",
		LastRun:              nil,
		Deletion:             deploymentcommon.DeploymentDeletion{},
	}
	var incomingEvent = NewIncomingEvent(service.DeploymentDeletionRequested, service.DeploymentDeletionRequest{
		Session: service.Session{
			SessionActor:    username,
			SessionEmulator: "",
		},
		ID: deploymentID,
	})
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	storageMock.On("PendingStatusDeleting", deploymentID).Return(true, nil)
	storageMock.On("StatusDeleted", deploymentID).Return(nil)

	runStorageMock.On("DeploymentHasRun", deploymentID).Return(false, nil)

	handler := NewDeletionHandler(portsDependency)
	outgoingEvent := handler.Handle(incomingEvent)

	assert.NotNil(t, outgoingEvent)
	assert.Equal(t, string(service.DeploymentDeleted), string(outgoingEvent.EventType()))
	assert.Equal(t, incomingEvent.Transaction(), outgoingEvent.Transaction())
	responseCe, err := outgoingEvent.ToCloudEvent("test")
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, string(service.DeploymentDeleted), responseCe.Type())
	assert.Equal(t, incomingEvent.Transaction(), messaging.GetTransactionID(&responseCe))
	var response service.DeploymentDeletionResult
	err = json.Unmarshal(responseCe.Data(), &response)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, deploymentID, response.ID)
	assert.Equal(t, username, response.Session.SessionActor)
	assert.Equal(t, "", response.Session.SessionEmulator)
	assert.Empty(t, response.ErrorType)
	assert.Empty(t, response.ErrorMessage)
	assert.Empty(t, response.ServiceError.StandardMessage)
	assert.Empty(t, response.ServiceError.ContextualMessage)

	storageMock.AssertExpectations(t)
	runStorageMock.AssertExpectations(t)
	keySrcMock.AssertExpectations(t)
	templateMSMock.AssertExpectations(t)
	credentialMSMock.AssertExpectations(t)
	awmMock.AssertExpectations(t)
}

func testDeleteDeploymentNotAuthorized(t *testing.T) {
	storageMock := &portsmocks.DeploymentStorage{}
	runStorageMock := &portsmocks.DeploymentRunStorage{}
	keySrcMock := &portsmocks.KeySrc{}
	templateMSMock := &portsmocks.TemplateMicroservice{}
	credentialMSMock := &portsmocks.CredentialMicroservice{}
	awmMock := &portsmocks.ArgoWorkflowMediator{}
	var portsDependency = ports.Ports{
		EventSink:    nil,
		Storage:      storageMock,
		RunStorage:   runStorageMock,
		KeySrc:       keySrcMock,
		TemplateMS:   templateMSMock,
		WorkspaceMS:  nil,
		CredentialMS: credentialMSMock,
		ProviderMS:   nil,
		AWM:          awmMock,
	}
	var actor = xid.New().String()
	var owner = "test-user123"
	var deploymentID = common.NewID(service.DeploymentIDPrefix)
	var deployment = deploymentcommon.Deployment{
		ID:        deploymentID,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		Workspace: common.NewID("workspace"),
		CreatedBy: deploymentcommon.Creator{
			User:     owner,
			Emulator: "",
		},
		Template:             common.NewID("template"),
		TemplateType:         "",
		PrimaryCloudProvider: common.NewID("provider"),
		CloudCredentials:     nil,
		GitCredential:        "",
		LastRun:              nil,
		Deletion:             deploymentcommon.DeploymentDeletion{},
	}
	var incomingEvent = NewIncomingEvent(service.DeploymentDeletionRequested, service.DeploymentDeletionRequest{
		Session: service.Session{
			SessionActor:    actor,
			SessionEmulator: "",
		},
		ID: deploymentID,
	})
	storageMock.On("Get", deploymentID).Return(deployment, nil)

	handler := NewDeletionHandler(portsDependency)
	outgoingEvent := handler.Handle(incomingEvent)

	assert.NotNil(t, outgoingEvent)
	assert.Equal(t, string(service.DeploymentDeleteFailed), string(outgoingEvent.EventType()))
	assert.Equal(t, incomingEvent.Transaction(), outgoingEvent.Transaction())
	responseCe, err := outgoingEvent.ToCloudEvent("test")
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, string(service.DeploymentDeleteFailed), responseCe.Type())
	assert.Equal(t, incomingEvent.Transaction(), messaging.GetTransactionID(&responseCe))
	var response service.DeploymentDeletionResult
	err = json.Unmarshal(responseCe.Data(), &response)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, deploymentID, response.ID)
	assert.Equal(t, actor, response.Session.SessionActor)
	assert.Equal(t, "", response.Session.SessionEmulator)
	assert.Empty(t, response.ErrorType)
	assert.Empty(t, response.ErrorMessage)
	assert.NotEmpty(t, response.ServiceError.StandardMessage)
	assert.Equal(t, service.CacaoUnauthorizedErrorMessage, response.ServiceError.StandardMessage)
	assert.NotEmpty(t, response.ServiceError.ContextualMessage)

	storageMock.AssertExpectations(t)
	runStorageMock.AssertExpectations(t)
	keySrcMock.AssertExpectations(t)
	templateMSMock.AssertExpectations(t)
	credentialMSMock.AssertExpectations(t)
	awmMock.AssertExpectations(t)
}

func testDeleteDeploymentWithRun(t *testing.T) {
	var workflowName = "workflow-name-123"
	var username = "test-user123"
	var deploymentID = common.NewID(service.DeploymentIDPrefix)
	var runID = common.NewID(service.RunIDPrefix)
	var primaryProviderID = common.NewID("provider")
	var credID = "cred-123"
	var deployment = deploymentcommon.Deployment{
		ID:        deploymentID,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		Workspace: common.NewID("workspace"),
		CreatedBy: deploymentcommon.Creator{
			User:     username,
			Emulator: "",
		},
		Template:             common.NewID("template"),
		TemplateType:         "",
		PrimaryCloudProvider: primaryProviderID,
		CloudCredentials: deploymentcommon.ProviderCredentialMappings{
			{
				Credential: deploymentcommon.CredentialID(credID),
				Provider:   primaryProviderID,
			},
		},
		GitCredential: "",
		LastRun:       &runID,
		Deletion:      deploymentcommon.DeploymentDeletion{},
	}
	var run = deploymentcommon.DeploymentRun{
		ID:         runID,
		Deployment: deploymentID,
		CreatedBy: deploymentcommon.Creator{
			User:     username,
			Emulator: "",
		},
		CreatedAt:        time.Now(),
		EndsAt:           time.Now(),
		TemplateSnapshot: deploymentcommon.TemplateSnapshot{},
		Parameters: deploymentcommon.DeploymentParameters{
			{
				Name:  "param1",
				Type:  "string",
				Value: "value1",
			},
		},
		Status: "",
		LastState: deploymentcommon.DeploymentStateView{
			Resources: []service.DeploymentResource{
				{
					ID: "resource-123",
				},
			},
		},
		StateUpdatedAt: time.Time{},
		RawState:       nil,
		Logs:           nil,
		Workflows:      nil,
		Histories:      nil,
	}
	var template = &service.TemplateModel{
		Session:     service.Session{},
		ID:          deployment.Template,
		Owner:       username,
		Name:        "",
		Description: "",
		Public:      false,
		Source: service.TemplateSource{
			Type: "git",
			URI:  "https://gitlab.com/cyverse/cacao.git",
			AccessParameters: map[string]interface{}{
				"branch": "some-branch",
				"path":   "foo/bar",
			},
			Visibility: "",
		},
		Metadata: service.TemplateMetadata{
			Name:             "",
			Author:           "",
			AuthorEmail:      "",
			Description:      "",
			TemplateTypeName: "",
			CacaoPostTasks:   nil,
			Parameters: map[string]interface{}{
				"param1": map[string]interface{}{
					"type": "string",
				},
			},
		},
		CreatedAt:        time.Time{},
		UpdatedAt:        time.Time{},
		UpdateFieldNames: nil,
		CredentialID:     "",
	}
	var incomingEvent = NewIncomingEvent(service.DeploymentDeletionRequested, service.DeploymentDeletionRequest{
		Session: service.Session{
			SessionActor:    username,
			SessionEmulator: "",
		},
		ID: deploymentID,
	})

	storageMock := &portsmocks.DeploymentStorage{}
	runStorageMock := &portsmocks.DeploymentRunStorage{}
	keySrcMock := &portsmocks.KeySrc{}
	templateMSMock := &portsmocks.TemplateMicroservice{}
	credentialMSMock := &portsmocks.CredentialMicroservice{}
	awmMock := awmclient.NewArgoWorkflowMediatorMock(workflowName, nil)
	var portsDependency = ports.Ports{
		EventSink:    nil,
		Storage:      storageMock,
		RunStorage:   runStorageMock,
		KeySrc:       keySrcMock,
		TemplateMS:   templateMSMock,
		WorkspaceMS:  nil,
		CredentialMS: credentialMSMock,
		ProviderMS:   nil,
		AWM:          awmMock,
	}

	deployment1 := deployment
	deployment1.Deletion.Deleting = true
	deployment1.Deletion.Transaction = incomingEvent.Transaction()
	// Note: the handler actually re-fetch deployment in-between 2 storageMock.Update() call, but multiple calls of Get()
	// with different result is hard to enforce with mockery object (Notice that there is only one Get()
	// mock call specified).
	deployment2 := deployment
	deployment2.Deletion.DeploymentWorkflow = deploymentcommon.NewDeploymentWorkflow(
		deploymentcommon.DeletionPurpose, workflowName, primaryProviderID)

	storageMock.On("Get", deploymentID).Return(deployment, nil)
	storageMock.On("PendingStatusDeleting", deploymentID).Return(true, nil)
	storageMock.On("StartDeletionWorkflow", deploymentID, incomingEvent.Transaction(), workflowName, deployment.PrimaryCloudProvider).Return(nil)

	runStorageMock.On("Get", run.ID).Return(run, nil)
	runStorageMock.On("GetLatestRun", deploymentID).Return(&run, nil)

	templateMSMock.On("Get", types.Actor{
		Actor:    username,
		Emulator: "",
	}, deployment.Template).Return(template, nil)
	credentialMSMock.On("Get", types.Actor{
		Actor:    username,
		Emulator: "",
	}, username, credID).Return(&service.CredentialModel{
		Session:           service.Session{},
		Username:          "",
		Value:             "{}",
		Type:              "",
		ID:                "",
		CreatedAt:         time.Time{},
		UpdatedAt:         time.Time{},
		UpdatedBy:         "",
		UpdatedEmulatorBy: "",
	}, nil)
	var key = make([]byte, 0)
	for i := 0; i < 32; i++ {
		key = append(key, byte(i))
	}
	keySrcMock.On("GetKey").Return(key)

	handler := NewDeletionHandler(portsDependency)
	outgoingEvent := handler.Handle(incomingEvent)

	assert.NotNil(t, outgoingEvent)
	assert.Equal(t, string(service.DeploymentDeletionStarted), string(outgoingEvent.EventType()))
	assert.Equal(t, incomingEvent.Transaction(), outgoingEvent.Transaction())
	responseCe, err := outgoingEvent.ToCloudEvent("test")
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, string(service.DeploymentDeletionStarted), responseCe.Type())
	assert.Equal(t, incomingEvent.Transaction(), messaging.GetTransactionID(&responseCe))
	var response service.DeploymentDeletionResult
	err = json.Unmarshal(responseCe.Data(), &response)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, deploymentID, response.ID)
	assert.Empty(t, response.ErrorType)
	assert.Empty(t, response.ErrorMessage)
	assert.Empty(t, response.ServiceError.StandardMessage)
	assert.Empty(t, response.ServiceError.ContextualMessage)

	storageMock.AssertExpectations(t)
	runStorageMock.AssertExpectations(t)
	keySrcMock.AssertExpectations(t)
	templateMSMock.AssertExpectations(t)
	credentialMSMock.AssertExpectations(t)

	assert.True(t, awmMock.Called)
	assert.Equal(t, primaryProviderID, awmMock.Provider)
	assert.Equal(t, username, awmMock.Username)
	assert.Equal(t, awmclient.TerraformWorkflowFilename, awmMock.WorkflowFilename)
	resultTFWfData := awmMock.TFWfData
	resultTFWfData.CloudCredentialBase64 = "" // omit cloud credential from equality assertion due to randomness
	assert.Equal(t, awmclient.TerraformWorkflowData{
		Username:          username,
		Deployment:        deploymentID,
		TemplateID:        deployment.Template,
		TerraformStateKey: deploymentID.String(),
		GitURL:            template.GetSource().URI,
		GitTrackedUpStream: struct {
			Branch string `mapstructure:"branch"`
			Tag    string `mapstructure:"tag"`
			Commit string `mapstructure:"commit"`
		}{
			Branch: template.GetSource().AccessParameters["branch"].(string),
			Tag:    "",
			Commit: "",
		},
		Path:        template.GetSource().AccessParameters["path"].(string),
		CloudCredID: credID,
		AnsibleVars: map[string]interface{}{
			"TF_INPUT_VALUES": map[string]interface{}{
				"param1": "value1",
			},
			"TF_INPUT_VARS": map[string]interface{}{
				"param1": map[string]string{"type": "string"},
			},
			"TF_MODULE":    "cacao_module",
			"TF_OPERATION": "destroy",
		},
		CloudCredentialBase64: "", // omitted
		GitCredID:             "",
		GitCredentialBase64:   "",
	}, resultTFWfData)
}

func testDeleteDeploymentWorkflowFailed(t *testing.T) {
	var username = "test-user123"
	var deploymentID = common.NewID(service.DeploymentIDPrefix)
	var runID = common.NewID(service.RunIDPrefix)
	var primaryProviderID = common.NewID("provider")
	var credID = "cred-123"
	var deployment = deploymentcommon.Deployment{
		ID:        deploymentID,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		Workspace: common.NewID("workspace"),
		CreatedBy: deploymentcommon.Creator{
			User:     username,
			Emulator: "",
		},
		Template:             common.NewID("template"),
		TemplateType:         "",
		PrimaryCloudProvider: primaryProviderID,
		CloudCredentials: deploymentcommon.ProviderCredentialMappings{
			{
				Credential: deploymentcommon.CredentialID(credID),
				Provider:   primaryProviderID,
			},
		},
		GitCredential: "",
		LastRun:       &runID,
		Deletion:      deploymentcommon.DeploymentDeletion{},
	}
	var run = deploymentcommon.DeploymentRun{
		ID:         runID,
		Deployment: deploymentID,
		CreatedBy: deploymentcommon.Creator{
			User:     username,
			Emulator: "",
		},
		CreatedAt:        time.Now(),
		EndsAt:           time.Now(),
		TemplateSnapshot: deploymentcommon.TemplateSnapshot{},
		Parameters: deploymentcommon.DeploymentParameters{
			{
				Name:  "param1",
				Type:  "string",
				Value: "value1",
			},
		},
		Status: "",
		LastState: deploymentcommon.DeploymentStateView{
			Resources: []service.DeploymentResource{
				{
					ID: "resource-123",
				},
			},
		},
		StateUpdatedAt: time.Time{},
		RawState:       nil,
		Logs:           nil,
		Workflows:      nil,
		Histories:      nil,
	}
	var template = &service.TemplateModel{
		Session:     service.Session{},
		ID:          deployment.Template,
		Owner:       username,
		Name:        "",
		Description: "",
		Public:      false,
		Source: service.TemplateSource{
			Type: "git",
			URI:  "https://gitlab.com/cyverse/cacao.git",
			AccessParameters: map[string]interface{}{
				"branch": "some-branch",
				"path":   "foo/bar",
			},
			Visibility: "",
		},
		Metadata: service.TemplateMetadata{
			Name:             "",
			Author:           "",
			AuthorEmail:      "",
			Description:      "",
			TemplateTypeName: "",
			CacaoPostTasks:   nil,
			Parameters: map[string]interface{}{
				"param1": map[string]interface{}{
					"type": "string",
				},
			},
		},
		CreatedAt:        time.Time{},
		UpdatedAt:        time.Time{},
		UpdateFieldNames: nil,
		CredentialID:     "",
	}
	var incomingEvent = NewIncomingEvent(service.DeploymentDeletionRequested, service.DeploymentDeletionRequest{
		Session: service.Session{
			SessionActor:    username,
			SessionEmulator: "",
		},
		ID: deploymentID,
	})

	storageMock := &portsmocks.DeploymentStorage{}
	runStorageMock := &portsmocks.DeploymentRunStorage{}
	keySrcMock := &portsmocks.KeySrc{}
	templateMSMock := &portsmocks.TemplateMicroservice{}
	credentialMSMock := &portsmocks.CredentialMicroservice{}
	awmMock := awmclient.NewArgoWorkflowMediatorMock("", service.NewCacaoGeneralError("mock workflow failed"))
	var portsDependency = ports.Ports{
		EventSink:    nil,
		Storage:      storageMock,
		RunStorage:   runStorageMock,
		KeySrc:       keySrcMock,
		TemplateMS:   templateMSMock,
		WorkspaceMS:  nil,
		CredentialMS: credentialMSMock,
		ProviderMS:   nil,
		AWM:          awmMock,
	}

	deployment1 := deployment
	deployment1.Deletion.Deleting = true
	deployment1.Deletion.Transaction = incomingEvent.Transaction()

	storageMock.On("Get", deploymentID).Return(deployment, nil)
	storageMock.On("PendingStatusDeleting", deploymentID).Return(true, nil)
	storageMock.On("StatusDeletionErrored", deploymentID).Return(nil)

	runStorageMock.On("Get", run.ID).Return(run, nil)
	runStorageMock.On("GetLatestRun", deploymentID).Return(&run, nil)

	templateMSMock.On("Get", types.Actor{
		Actor:    username,
		Emulator: "",
	}, deployment.Template).Return(template, nil)
	credentialMSMock.On("Get", types.Actor{
		Actor:    username,
		Emulator: "",
	}, username, credID).Return(&service.CredentialModel{
		Session:           service.Session{},
		Username:          "",
		Value:             "{}",
		Type:              "",
		ID:                "",
		CreatedAt:         time.Time{},
		UpdatedAt:         time.Time{},
		UpdatedBy:         "",
		UpdatedEmulatorBy: "",
	}, nil)
	var key = make([]byte, 0)
	for i := 0; i < 32; i++ {
		key = append(key, byte(i))
	}
	keySrcMock.On("GetKey").Return(key)

	handler := NewDeletionHandler(portsDependency)
	outgoingEvent := handler.Handle(incomingEvent)

	assert.NotNil(t, outgoingEvent)
	assert.Equal(t, string(service.DeploymentDeleteFailed), string(outgoingEvent.EventType()))
	assert.Equal(t, incomingEvent.Transaction(), outgoingEvent.Transaction())
	responseCe, err := outgoingEvent.ToCloudEvent("test")
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, string(service.DeploymentDeleteFailed), responseCe.Type())
	assert.Equal(t, incomingEvent.Transaction(), messaging.GetTransactionID(&responseCe))
	var response service.DeploymentDeletionResult
	err = json.Unmarshal(responseCe.Data(), &response)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, deploymentID, response.ID)
	assert.Empty(t, response.ErrorType)
	assert.Empty(t, response.ErrorMessage)
	assert.Equal(t, service.CacaoGeneralErrorMessage, response.ServiceError.StandardMessage)
	assert.Equal(t, "mock workflow failed", response.ServiceError.ContextualMessage)

	storageMock.AssertExpectations(t)
	runStorageMock.AssertExpectations(t)
	keySrcMock.AssertExpectations(t)
	templateMSMock.AssertExpectations(t)
	credentialMSMock.AssertExpectations(t)

	assert.True(t, awmMock.Called)
	assert.Equal(t, primaryProviderID, awmMock.Provider)
	assert.Equal(t, username, awmMock.Username)
	assert.Equal(t, awmclient.TerraformWorkflowFilename, awmMock.WorkflowFilename)
	resultTFWfData := awmMock.TFWfData
	resultTFWfData.CloudCredentialBase64 = "" // omit cloud credential from equality assertion due to randomness
	assert.Equal(t, awmclient.TerraformWorkflowData{
		Username:          username,
		Deployment:        deploymentID,
		TemplateID:        deployment.Template,
		TerraformStateKey: deploymentID.String(),
		GitURL:            template.GetSource().URI,
		GitTrackedUpStream: struct {
			Branch string `mapstructure:"branch"`
			Tag    string `mapstructure:"tag"`
			Commit string `mapstructure:"commit"`
		}{
			Branch: template.GetSource().AccessParameters["branch"].(string),
			Tag:    "",
			Commit: "",
		},
		Path:        template.GetSource().AccessParameters["path"].(string),
		CloudCredID: credID,
		AnsibleVars: map[string]interface{}{
			"TF_INPUT_VALUES": map[string]interface{}{
				"param1": "value1",
			},
			"TF_INPUT_VARS": map[string]interface{}{
				"param1": map[string]string{"type": "string"},
			},
			"TF_MODULE":    "cacao_module",
			"TF_OPERATION": "destroy",
		},
		CloudCredentialBase64: "", // omitted
		GitCredID:             "",
		GitCredentialBase64:   "",
	}, resultTFWfData)
}
