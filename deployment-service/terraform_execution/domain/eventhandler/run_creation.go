package eventhandler

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"time"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/domainutils"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/preflightstage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// RunCreationHandler handles creation of deployment run
// This handler only performs barebone validation on the request, just enough
// to warrant the creation of an entry in the storage.
type RunCreationHandler struct {
	runCreationPorts
	preflightChan chan<- preflightstage.PreflightRequest
}

// runCreationPorts are ports that are needed for creating a run
type runCreationPorts struct {
	storage    ports.DeploymentStorage
	runStorage ports.DeploymentRunStorage
	providerMS ports.ProviderMicroservice
	templateMS ports.TemplateMicroservice
	credMS     ports.CredentialMicroservice
	keySrc     ports.KeySrc
}

// NewRunCreationHandler ...
func NewRunCreationHandler(portsDependency ports.Ports, preflightChan chan<- preflightstage.PreflightRequest) RunCreationHandler {
	return RunCreationHandler{
		runCreationPorts: runCreationPorts{
			storage:    portsDependency.Storage,
			runStorage: portsDependency.RunStorage,
			providerMS: portsDependency.ProviderMS,
			templateMS: portsDependency.TemplateMS,
			credMS:     portsDependency.CredentialMS,
			keySrc:     portsDependency.KeySrc,
		},
		preflightChan: preflightChan,
	}
}

// Handle ...
func (h RunCreationHandler) Handle(event types.IncomingEvent) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "RunCreationHandler.Handle",
	})
	var request service.DeploymentCreateRunRequest
	request, err := h.eventToRequest(event)
	if err != nil {
		logger.WithError(err).Trace("fail to convert event to request")
		result := h.createRunFailedResult(request, err)

		return h.toRespEvent(event.Transaction(), result)
	}
	return h.toRespEvent(event.Transaction(), h.handle(request))
}

func (h RunCreationHandler) eventToRequest(event types.IncomingEvent) (service.DeploymentCreateRunRequest, error) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "RunCreationHandler.eventToRequest",
	})

	var request service.DeploymentCreateRunRequest
	err := json.Unmarshal(event.CloudEvent().Data(), &request)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to DeploymentCreateRunRequest"
		logger.WithError(err).Error(errorMessage)
		return service.DeploymentCreateRunRequest{}, service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error()))
	}
	return request, nil
}

func (h RunCreationHandler) handle(request service.DeploymentCreateRunRequest) service.DeploymentCreateRunResult {
	session := newRunCreationSession(h.runCreationPorts)
	run, err := session.CreateRun(request, h.preflightChan)
	if err != nil {
		return h.createRunFailedResult(request, err)
	}
	return service.DeploymentCreateRunResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Run: run.ConvertToExternal(),
	}
}

// runCreationSession is a session for create a run. It contains dependencies and intermediate data for run creation.
type runCreationSession struct {
	runCreationPorts
	deployment           deploymentcommon.Deployment
	templateParamDef     map[string]interface{}
	snapshot             deploymentcommon.TemplateSnapshot
	providerMetadata     types.OpenStackProviderMetadata
	deploymentParameters []deploymentcommon.DeploymentParameter
	cloudCred            deploymentcommon.CredentialID
	cloudCredBase64      string
	gitCredBase64        string
	run                  deploymentcommon.DeploymentRun
}

func newRunCreationSession(
	runCreationPorts runCreationPorts,
) *runCreationSession {
	return &runCreationSession{runCreationPorts: runCreationPorts}
}

// CreateRun create a deployment run from the request, if request succeeds then post to preflightChan.
func (s *runCreationSession) CreateRun(
	request service.DeploymentCreateRunRequest,
	preflightChan chan<- preflightstage.PreflightRequest,
) (deploymentcommon.DeploymentRun, error) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "runCreationSession.CreateRun",
	})
	var err error
	if err = s.getDeployment(request); err != nil {
		logger.WithError(err).Error("fail to get deployment")
		return deploymentcommon.DeploymentRun{}, err
	}
	if err = s.checkDeployment(request); err != nil {
		logger.WithError(err).Error("error while checking deployment")
		return deploymentcommon.DeploymentRun{}, err
	}
	if err = s.getTemplate(request); err != nil {
		logger.WithError(err).Error("fail to get template")
		return deploymentcommon.DeploymentRun{}, err
	}
	if err = s.getProviderMetadata(request); err != nil {
		logger.WithError(err).Error("fail to get provider metadata")
		return deploymentcommon.DeploymentRun{}, err
	}
	if err = s.prepareParameters(request); err != nil {
		logger.WithError(err).Error("error while prep parameter")
		return deploymentcommon.DeploymentRun{}, err
	}
	if err = s.getCredentials(request); err != nil {
		logger.WithError(err).Error("fail to get credential(s)")
		return deploymentcommon.DeploymentRun{}, err
	}
	if err = s.updateDeploymentStatus(); err != nil {
		// update the status before run is created in storage. this ensures there is no competing run creation.
		logger.WithError(err).Error("fail to update deployment pending status in storage")
		return deploymentcommon.DeploymentRun{}, err
	}
	if err = s.createAndSaveRun(request); err != nil {
		logger.WithError(err).Error("fail to create deployment run in storage")
		return deploymentcommon.DeploymentRun{}, err
	}
	logger.WithField("run", s.run.ID).Info("deployment run created in storage")

	if err = s.updateDeploymentLastRun(); err != nil {
		logger.WithFields(log.Fields{
			"deployment": s.deployment.ID,
			"run":        s.run.ID,
		}).WithError(err).Error("fail to update last run of deployment in storage")
		return deploymentcommon.DeploymentRun{}, err
	}
	s.proceedToPreflight(request, preflightChan)
	return s.run, nil
}

func (s *runCreationSession) getDeployment(request service.DeploymentCreateRunRequest) error {
	deployment, err := s.storage.Get(request.CreateParam.Deployment)
	if err != nil {
		return err
	}
	s.deployment = deployment
	return nil
}

func (s *runCreationSession) checkDeployment(request service.DeploymentCreateRunRequest) error {
	// check for permission
	if s.deployment.CreatedBy.User != request.GetSessionActor() {
		return service.NewCacaoUnauthorizedError("unauthorized access to deployment")
	}
	if !s.deployment.CurrentStatus.AllowRunCreation() {
		return service.NewCacaoGeneralError("current status does not allow run creation")
	}
	if s.deployment.PendingStatus == service.DeploymentStatusDeleting {
		// cannot create a run when the deployment is deleting, they are mutual exclusive
		return service.NewCacaoGeneralError("deployment is deleting")
	}
	if s.deployment.PendingStatus == service.DeploymentStatusCreating {
		// for terraform template, only 1 run is allowed at a time, no concurrent runs.
		return service.NewCacaoGeneralError("there is another on-going run creation")
	}
	return nil
}

func (s *runCreationSession) getTemplate(request service.DeploymentCreateRunRequest) error {
	template, err := s.templateMS.Get(types.Actor{
		Actor:    request.GetSessionActor(),
		Emulator: request.GetSessionEmulator(),
	}, s.deployment.Template)
	if err != nil {
		return err
	}
	s.templateParamDef = template.GetMetadata().Parameters
	snapshot, err := domainutils.GetTemplateSnapshot(template)
	if err != nil {
		return err
	}
	s.snapshot = snapshot
	return nil
}

func (s *runCreationSession) getCredentials(request service.DeploymentCreateRunRequest) error {
	actor := types.Actor{
		Actor:    request.GetSessionActor(),
		Emulator: request.GetSessionEmulator(),
	}
	// only support 1 cred currently
	cloudCredID, ok := s.deployment.FindCloudCredential(s.deployment.PrimaryCloudProvider)
	if !ok {
		return service.NewCacaoInvalidParameterError("missing cred for primary provider")
	}

	credEncoder := domainutils.NewCredentialEncoder(s.credMS, s.keySrc)
	cloudCredBase64, err := credEncoder.EncodeOpenstackCred(actor, cloudCredID.String())
	if err != nil {
		return err
	}
	s.cloudCred = cloudCredID
	s.cloudCredBase64 = cloudCredBase64

	if s.deployment.GitCredential != "" {
		var gitCredBase64 string
		gitCredBase64, err = credEncoder.EncodeGitCred(actor, s.deployment.GitCredential.String())
		if err != nil {
			return err
		}
		s.gitCredBase64 = gitCredBase64
	}
	return nil
}

func (s *runCreationSession) getProviderMetadata(request service.DeploymentCreateRunRequest) error {
	actor := types.Actor{
		Actor:    request.GetSessionActor(),
		Emulator: request.GetSessionEmulator(),
	}
	provider, err := s.providerMS.Get(actor, s.deployment.PrimaryCloudProvider)
	if err != nil {
		return err
	}
	if provider.GetType() != "openstack" {
		return service.NewCacaoInvalidParameterError(fmt.Sprintf("bad provider type, %s", provider.GetType()))
	}
	metadataMap := provider.GetMetadata()
	if metadataMap == nil {
		return service.NewCacaoInvalidParameterError("provider metadata is nil")
	}
	var metadata types.OpenStackProviderMetadata
	err = mapstructure.Decode(metadataMap, &metadata)
	if err != nil {
		return fmt.Errorf("fail to decode provider metadata, %w", err)
	}
	if !metadata.Validate() {
		return service.NewCacaoInvalidParameterErrorWithOptions("invalid openstack provider metadata")
	}
	s.providerMetadata = metadata
	return nil
}

func (s *runCreationSession) prepareParameters(request service.DeploymentCreateRunRequest) error {
	var paramConverter domainutils.ParameterConverter
	extNetworkName := s.providerMetadata.ExternalNetworkName
	extSubnetUUID := s.selectExternalSubnet(s.providerMetadata.ExternalSubnetUUIDs)

	// override param value for username and keypair-name
	overrideValues := map[string]interface{}{
		domainutils.TemplateUsernameType:                request.GetSessionActor(),
		domainutils.TemplateProviderKeyPairType:         domainutils.KeyPairName(request.GetSessionActor()),
		domainutils.TemplateProviderExternalNetworkType: extNetworkName,
		domainutils.TemplateProviderExternalSubnetType:  extSubnetUUID,
	}

	// only generate cloud-init script if there is a template param that requires it.
	if domainutils.TemplateParameterDefHasType(s.templateParamDef, domainutils.TemplateCloudInitType) {
		cloudInitScript, err := s.generateCloudInitScript(request)
		if err != nil {
			return err
		}
		overrideValues[domainutils.TemplateCloudInitType] = cloudInitScript
	}

	parameters, err := paramConverter.Convert(
		s.templateParamDef,
		request.CreateParam.ParamValues,
		domainutils.OverrideValueForTypes(overrideValues),
	)
	if err != nil {
		return err
	}
	s.deploymentParameters = parameters
	return nil
}

// selectExternalSubnet selects a subnet from a list of subnets
func (s *runCreationSession) selectExternalSubnet(extSubnetList []string) string {
	return extSubnetList[rand.Intn(len(extSubnetList))]
}

func (s *runCreationSession) generateCloudInitScript(request service.DeploymentCreateRunRequest) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "runCreationSession.generateCloudInitScript",
	})
	generator := domainutils.NewCloudInitGenerator(0)

	sshKeys, err := s.listAllSSHCredentials(request)
	if err != nil {
		logger.WithError(err).Error("fail to list all SSH credentials for cloud-init script generation")
		return "", err
	}
	logger.WithField("sshKeyCount", len(sshKeys)).Debug("ssh keys fetched for cloud-init script generation")
	generator.AddSSHKeys(sshKeys)

	generator.CreateUser(request.GetSessionActor())

	cloudInitScript, err := generator.Generate()
	if err != nil {
		logger.WithError(err).Error("fail to generate cloud-init script")
		return "", err
	}
	return cloudInitScript, nil
}

// list all ssh key credential that owned by the actor.
func (s *runCreationSession) listAllSSHCredentials(request service.DeploymentCreateRunRequest) ([]string, error) {
	actor := types.ActorFromSession(request.Session)
	list, err := s.credMS.List(actor, actor.Actor)
	if err != nil {
		return nil, err
	}
	sshCredIDs := make([]string, 0)
	for _, cred := range list {
		if cred.GetType() == "ssh" {
			sshCredIDs = append(sshCredIDs, cred.GetID())
		}
	}
	// because the list operation returns value as REDACTED, thus each credential is fetched individually
	allPublicSSHKeys := make([]string, 0, len(sshCredIDs))
	for _, credID := range sshCredIDs {
		var cred service.Credential
		cred, err = s.credMS.Get(actor, actor.Actor, credID)
		if err != nil {
			return nil, err
		}
		allPublicSSHKeys = append(allPublicSSHKeys, cred.GetValue())
	}
	return allPublicSSHKeys, nil
}

func (s *runCreationSession) updateDeploymentStatus() error {
	ok, err := s.storage.PendingStatusCreating(s.deployment.ID)
	if err != nil {
		return err
	}
	if !ok {
		return service.NewCacaoGeneralError("current status does not allow run creation")
	}
	return nil
}

func (s *runCreationSession) createAndSaveRun(request service.DeploymentCreateRunRequest) error {
	run := deploymentcommon.DeploymentRun{
		ID:         common.NewID(types.RunIDPrefix),
		Deployment: request.CreateParam.Deployment,
		CreatedBy: deploymentcommon.Creator{
			User:     request.GetSessionActor(),
			Emulator: request.GetSessionEmulator(),
		},
		CreatedAt:        time.Now().UTC(),
		EndsAt:           time.Time{},
		TemplateSnapshot: s.snapshot,
		Parameters:       s.deploymentParameters,
		Status:           deploymentcommon.DeploymentRunPreflight,
		LastState:        deploymentcommon.DeploymentStateView{},
		StateUpdatedAt:   time.Time{},
		RawState:         nil,
		Logs:             nil,
		Histories:        nil,
	}
	_, err := s.runStorage.Create(run)
	if err != nil {
		return err
	}
	s.run = run
	return nil
}

func (s *runCreationSession) updateDeploymentLastRun() error {
	s.deployment.LastRun = &s.run.ID
	return s.storage.UpdateLastRun(s.deployment.ID, s.run.ID)
}

func (s *runCreationSession) reFetchDeployment() error {
	deployment, err := s.storage.Get(s.deployment.ID)
	if err != nil {
		return err
	}
	s.deployment = deployment
	return nil
}

func (s *runCreationSession) proceedToPreflight(
	request service.DeploymentCreateRunRequest,
	preflightChan chan<- preflightstage.PreflightRequest,
) {
	preflightRequest := preflightstage.PreflightRequest{
		Actor:           request.GetSessionActor(),
		Emulator:        request.GetSessionEmulator(),
		Deployment:      s.deployment,
		Run:             s.run,
		CloudCredID:     s.cloudCred,
		CloudCredBase64: s.cloudCredBase64,
		GitCredID:       s.deployment.GitCredential,
		GitCredBase64:   s.gitCredBase64,
	}
	// call as go routine to avoid block on channel
	go func(channel chan<- preflightstage.PreflightRequest) {
		channel <- preflightRequest
	}(preflightChan)
}

func (h RunCreationHandler) createRunFailedResult(request service.DeploymentCreateRunRequest, err error) service.DeploymentCreateRunResult {
	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	return service.DeploymentCreateRunResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
			ServiceError:    serviceError.GetBase(),
		},
	}
}

func (h RunCreationHandler) toRespEvent(transaction common.TransactionID, result service.DeploymentCreateRunResult) ResponseEvent {
	if result.GetServiceError() == nil {
		return NewEvent(service.DeploymentRunCreated, transaction, result)
	}
	return NewEvent(service.DeploymentRunCreateFailed, transaction, result)
}
