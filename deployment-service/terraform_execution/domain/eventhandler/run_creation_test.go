package eventhandler

import (
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymenttestutils"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/domainutils"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/preflightstage"
	portsmock "gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"testing"
	"time"
)

func Test_runCreationSession_CreateRun(t *testing.T) {
	t.Run("success", testRunCreationSession)
	t.Run("not authorized", testRunCreationSessionNotAuthorized)
	t.Run("deployment not found", testRunCreationSessionDeploymentNotFound)
	t.Run("fail to find template", testRunCreationSessionTemplateNotFound)
}

const sshPublicKey = "ssh-rsa fffff"

func testRunCreationSession(t *testing.T) {
	var actor = types.Actor{
		Actor:    "test_user123",
		Emulator: "",
	}
	var deploymentID = common.NewID(service.DeploymentIDPrefix)
	var runID common.ID
	var templateID = common.NewID("template")
	var providerID = common.NewID("provider")
	var deployment = deploymentcommon.Deployment{
		ID:        deploymentID,
		Workspace: "",
		CreatedBy: deploymentcommon.Creator{
			User:     actor.Actor,
			Emulator: actor.Emulator,
		},
		Template:             templateID,
		TemplateType:         "",
		PrimaryCloudProvider: providerID,
		CurrentStatus:        service.DeploymentStatusNone,
		PendingStatus:        service.DeploymentStatusNoPending,
		CloudCredentials: deploymentcommon.ProviderCredentialMappings{
			{
				Credential: "cred1",
				Provider:   providerID,
			},
		},
		GitCredential: "",
		LastRun:       nil,
		Deletion: deploymentcommon.DeploymentDeletion{
			Deleting:           false,
			Transaction:        "",
			DeploymentWorkflow: deploymentcommon.DeploymentWorkflow{},
		},
	}
	var provider = service.ProviderModel{
		ID:   providerID,
		Name: "",
		Type: "openstack",
		URL:  "",
		Metadata: map[string]interface{}{
			"prerequisite_template": map[string]interface{}{
				"template_id": templateID.String(),
				"param_value": map[string]interface{}{
					"foo": "bar",
				},
			},
			"public_ssh_key":        sshPublicKey,
			"external_network_name": "external_network_123",
			"external_network_uuid": "aaaaaaaa-aaaa-aaaa-aaaa-aaaaabbbbbbb",
			"external_subnet_uuids": []string{"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaccccccc"},
		},
	}
	var template = &service.TemplateModel{
		Session: service.Session{},
		ID:      deployment.Template,
		Owner:   actor.Actor,
		Source: service.TemplateSource{
			Type: "git",
			URI:  "https://gitlab.com/cyverse/foobar.git",
			AccessParameters: map[string]interface{}{
				"branch": "branch123",
				"path":   "path123",
			},
			Visibility: "",
		},
		Metadata: service.TemplateMetadata{
			Parameters: map[string]interface{}{
				"username": map[string]interface{}{
					"type": domainutils.TemplateUsernameType,
				},
				"param1": map[string]interface{}{
					"type": "string",
				},
				"param2": map[string]interface{}{
					"type": "integer",
				},
				"keypair_name": map[string]interface{}{
					"type": domainutils.TemplateProviderKeyPairType,
				},
				"external_network_name": map[string]interface{}{
					"type": domainutils.TemplateProviderExternalNetworkType,
				},
				"external_subnet_uuid": map[string]interface{}{
					"type": domainutils.TemplateProviderExternalSubnetType,
				},
				"cloud_init": map[string]interface{}{
					"type": domainutils.TemplateCloudInitType,
				},
			},
		},
	}
	var cloudCred = &service.CredentialModel{
		Username: actor.Actor,
		Value:    "{}",
		Type:     "openstack",
		ID:       "cred1",
	}
	var sshCred = &service.CredentialModel{
		Username: actor.Actor,
		Value:    "ssh-rsa AAAAA root",
		Type:     "ssh",
		ID:       "ssh1",
	}
	var aesKey = make([]byte, 32)
	var expectedRunParameters = deploymentcommon.DeploymentParameters{
		{
			Name:  "username",
			Type:  domainutils.TemplateUsernameType,
			Value: actor.Actor,
		},
		{
			Name:  "keypair_name",
			Type:  domainutils.TemplateProviderKeyPairType,
			Value: domainutils.KeyPairName(actor.Actor),
		},
		{
			Name:  "external_network_name",
			Type:  domainutils.TemplateProviderExternalNetworkType,
			Value: "external_network_123",
		},
		{
			Name:  "external_subnet_uuid",
			Type:  domainutils.TemplateProviderExternalSubnetType,
			Value: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaccccccc",
		},
		{
			Name: "cloud_init",
			Type: domainutils.TemplateCloudInitType,
			Value: fmt.Sprintf(`#cloud-config
users:
- default
- name: %s
  sudo: ALL=(ALL) NOPASSWD:ALL
  ssh_authorized_keys:
  - %s

`, actor.Actor, sshCred.Value),
		},
		{
			Name:  "param1",
			Type:  "string",
			Value: "value1",
		},
		{
			Name:  "param2",
			Type:  "integer",
			Value: 222,
		},
	}
	storage := &portsmock.DeploymentStorage{}
	runStorage := &portsmock.DeploymentRunStorage{}
	providerMS := &portsmock.ProviderMicroservice{}
	templateMS := &portsmock.TemplateMicroservice{}
	credMS := &portsmock.CredentialMicroservice{}
	keySrc := &portsmock.KeySrc{}

	request := service.DeploymentCreateRunRequest{
		Session: service.Session{
			SessionActor:    actor.Actor,
			SessionEmulator: actor.Emulator,
		},
		CreateParam: service.DeploymentRunCreateParam{
			Deployment: deploymentID,
			ParamValues: map[string]interface{}{
				"username": "foobar",
				"param1":   "value1",
				"param2":   222,
			},
		},
	}

	storage.On("Get", request.CreateParam.Deployment).Return(deployment, nil)
	storage.On("PendingStatusCreating", deploymentID).Return(true, nil)
	storage.On("UpdateLastRun", deploymentID, mock.AnythingOfType("common.ID")).Return(nil)
	runStorage.On("Create", mock.AnythingOfType("DeploymentRun")).Run(
		func(args mock.Arguments) {
			run := args.Get(0).(deploymentcommon.DeploymentRun)
			runID = run.ID
			assert.NotEmpty(t, run.ID)
			assert.Equal(t, deploymentID, run.Deployment)
			assert.Equal(t, actor.Actor, run.CreatedBy.User)
			assert.Equal(t, actor.Emulator, run.CreatedBy.Emulator)
			assert.False(t, run.CreatedAt.IsZero())
			assert.True(t, run.EndsAt.IsZero())
			assert.Equal(t, templateID, run.TemplateSnapshot.TemplateID)
			assert.Equal(t, "https://gitlab.com/cyverse/foobar.git", run.TemplateSnapshot.GitURL)
			assert.Equal(t, "path123", run.TemplateSnapshot.SubPath)
			assert.Equal(t, "branch123", run.TemplateSnapshot.UpstreamTracked.Branch)
			assert.Empty(t, run.TemplateSnapshot.UpstreamTracked.Tag)
			assert.Len(t, run.Parameters, 7)
			assert.ElementsMatch(t, expectedRunParameters, run.Parameters)
			assert.Equal(t, deploymentcommon.DeploymentRunPreflight, run.Status)
			assert.Nil(t, run.RawState)
			assert.Nil(t, run.Logs)
			assert.True(t, run.Workflows == nil || len(run.Workflows) == 0)
		}).Return(runID, nil)
	providerMS.On("Get", actor, providerID).Return(&provider, nil)
	templateMS.On("Get", actor, deployment.Template).Return(template, nil)
	credMS.On(
		"Get",
		actor,
		actor.Actor,
		deployment.CloudCredentials[0].Credential.String(),
	).Return(cloudCred, nil)
	credMS.On(
		"List",
		actor,
		actor.Actor,
	).Return([]service.Credential{cloudCred, sshCred}, nil)
	credMS.On(
		"Get",
		actor,
		actor.Actor,
		sshCred.ID,
	).Return(sshCred, nil)
	keySrc.On("GetKey").Return(aesKey)
	keySrc.GetKey()

	preflightChan := make(chan preflightstage.PreflightRequest, 1)
	run, err := newRunCreationSession(runCreationPorts{
		storage:    storage,
		runStorage: runStorage,
		providerMS: providerMS,
		templateMS: templateMS,
		credMS:     credMS,
		keySrc:     keySrc,
	}).CreateRun(request, preflightChan)
	if !assert.NoError(t, err) {
		return
	}
	assert.NotEmpty(t, run.ID)
	select {
	case preflightRequest := <-preflightChan:
		assert.Equal(t, actor.Actor, preflightRequest.Actor)
		assert.Equal(t, actor.Emulator, preflightRequest.Emulator)
		assert.Equal(t, deploymentID, preflightRequest.Deployment.ID)
		if assert.NotNil(t, preflightRequest.Deployment.LastRun) {
			assert.Equal(t, runID, *preflightRequest.Deployment.LastRun)
		}
		deploymenttestutils.AssertRunEqual(t, preflightRequest.Run, deploymentcommon.DeploymentRun{
			ID:         runID,
			Deployment: deploymentID,
			CreatedBy: deploymentcommon.Creator{
				User:     actor.Actor,
				Emulator: actor.Emulator,
			},
			CreatedAt: time.Time{},
			EndsAt:    time.Time{},
			TemplateSnapshot: deploymentcommon.TemplateSnapshot{
				TemplateID: templateID,
				UpstreamTracked: struct {
					Branch string `bson:"branch"`
					Tag    string `bson:"tag"`
				}{
					Branch: template.Source.AccessParameters["branch"].(string),
					Tag:    "",
				},
				GitURL:     template.Source.URI,
				CommitHash: "",
				SubPath:    template.Source.AccessParameters["path"].(string),
			},
			Parameters:     expectedRunParameters,
			Status:         deploymentcommon.DeploymentRunPreflight,
			LastState:      deploymentcommon.DeploymentStateView{},
			StateUpdatedAt: time.Time{},
			RawState:       nil,
			Logs:           nil,
			Workflows:      nil,
			Histories:      nil,
		})
		assert.Equal(t, cloudCred.ID, preflightRequest.CloudCredID.String())
		assert.NotEmpty(t, preflightRequest.CloudCredBase64)
		assert.Empty(t, preflightRequest.GitCredID)
		assert.Empty(t, preflightRequest.GitCredBase64)
	case <-time.After(time.Second):
		assert.Fail(t, "no request in preflight channel after 1 sec")
	}

	storage.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	providerMS.AssertExpectations(t)
	templateMS.AssertExpectations(t)
	credMS.AssertExpectations(t)
	keySrc.AssertExpectations(t)
}

func testRunCreationSessionNotAuthorized(t *testing.T) {
	var actor = types.Actor{
		Actor:    "test_user123",
		Emulator: "",
	}
	var deploymentID = common.NewID(service.DeploymentIDPrefix)
	var providerID = common.NewID("provider")
	var deployment = deploymentcommon.Deployment{
		ID:        deploymentID,
		Workspace: "",
		CreatedBy: deploymentcommon.Creator{
			User:     "not-" + actor.Actor,
			Emulator: actor.Emulator,
		},
		Template:             common.NewID("template"),
		TemplateType:         "",
		PrimaryCloudProvider: providerID,
		CloudCredentials: deploymentcommon.ProviderCredentialMappings{
			{
				Credential: "cred1",
				Provider:   providerID,
			},
		},
		GitCredential: "",
		LastRun:       nil,
		Deletion: deploymentcommon.DeploymentDeletion{
			Deleting:           false,
			Transaction:        "",
			DeploymentWorkflow: deploymentcommon.DeploymentWorkflow{},
		},
	}
	storage := &portsmock.DeploymentStorage{}
	runStorage := &portsmock.DeploymentRunStorage{}
	providerMS := &portsmock.ProviderMicroservice{}
	templateMS := &portsmock.TemplateMicroservice{}
	credMS := &portsmock.CredentialMicroservice{}
	keySrc := &portsmock.KeySrc{}

	request := service.DeploymentCreateRunRequest{
		Session: service.Session{
			SessionActor:    actor.Actor,
			SessionEmulator: actor.Emulator,
		},
		CreateParam: service.DeploymentRunCreateParam{
			Deployment: deploymentID,
			ParamValues: map[string]interface{}{
				"username": "foobar",
				"param1":   "value1",
				"param2":   222,
			},
		},
	}

	storage.On("Get", request.CreateParam.Deployment).Return(deployment, nil)

	preflightChan := make(chan preflightstage.PreflightRequest, 1)
	run, err := newRunCreationSession(runCreationPorts{
		storage:    storage,
		runStorage: runStorage,
		providerMS: providerMS,
		templateMS: templateMS,
		credMS:     credMS,
		keySrc:     keySrc,
	}).CreateRun(request, preflightChan)
	if !assert.NotNil(t, err) {
		return
	}
	assert.IsType(t, &service.CacaoUnauthorizedError{}, err)
	assert.Empty(t, run.ID)
	assert.Len(t, preflightChan, 0)

	storage.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	providerMS.AssertExpectations(t)
	templateMS.AssertExpectations(t)
	credMS.AssertExpectations(t)
	keySrc.AssertExpectations(t)
}

func testRunCreationSessionDeploymentNotFound(t *testing.T) {
	var actor = types.Actor{
		Actor:    "test_user123",
		Emulator: "",
	}
	var deploymentID = common.NewID(service.DeploymentIDPrefix)

	storage := &portsmock.DeploymentStorage{}
	runStorage := &portsmock.DeploymentRunStorage{}
	providerMS := &portsmock.ProviderMicroservice{}
	templateMS := &portsmock.TemplateMicroservice{}
	credMS := &portsmock.CredentialMicroservice{}
	keySrc := &portsmock.KeySrc{}

	request := service.DeploymentCreateRunRequest{
		Session: service.Session{
			SessionActor:    actor.Actor,
			SessionEmulator: actor.Emulator,
		},
		CreateParam: service.DeploymentRunCreateParam{
			Deployment: deploymentID,
			ParamValues: map[string]interface{}{
				"username": "foobar",
				"param1":   "value1",
				"param2":   222,
			},
		},
	}

	storage.On("Get", request.CreateParam.Deployment).Return(
		deploymentcommon.Deployment{},
		service.NewCacaoNotFoundError("mock error"),
	)

	preflightChan := make(chan preflightstage.PreflightRequest, 1)
	run, err := newRunCreationSession(runCreationPorts{
		storage:    storage,
		runStorage: runStorage,
		providerMS: providerMS,
		templateMS: templateMS,
		credMS:     credMS,
		keySrc:     keySrc,
	}).CreateRun(request, preflightChan)
	if !assert.NotNil(t, err) {
		return
	}
	assert.IsType(t, &service.CacaoNotFoundError{}, err)
	assert.Empty(t, run.ID)
	assert.Len(t, preflightChan, 0)

	storage.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	providerMS.AssertExpectations(t)
	templateMS.AssertExpectations(t)
	credMS.AssertExpectations(t)
	keySrc.AssertExpectations(t)
}

func testRunCreationSessionTemplateNotFound(t *testing.T) {
	var actor = types.Actor{
		Actor:    "test_user123",
		Emulator: "",
	}
	var deploymentID = common.NewID(service.DeploymentIDPrefix)
	var templateID = common.NewID("template")
	var providerID = common.NewID("provider")
	var deployment = deploymentcommon.Deployment{
		ID:        deploymentID,
		Workspace: "",
		CreatedBy: deploymentcommon.Creator{
			User:     actor.Actor,
			Emulator: actor.Emulator,
		},
		Template:             templateID,
		TemplateType:         "",
		PrimaryCloudProvider: providerID,
		CurrentStatus:        service.DeploymentStatusNone,
		PendingStatus:        service.DeploymentStatusNoPending,
		StatusMsg:            "",
		CloudCredentials: deploymentcommon.ProviderCredentialMappings{
			{
				Credential: "cred1",
				Provider:   providerID,
			},
		},
		GitCredential: "",
		LastRun:       nil,
		Deletion: deploymentcommon.DeploymentDeletion{
			Deleting:           false,
			Transaction:        "",
			DeploymentWorkflow: deploymentcommon.DeploymentWorkflow{},
		},
	}
	storage := &portsmock.DeploymentStorage{}
	runStorage := &portsmock.DeploymentRunStorage{}
	providerMS := &portsmock.ProviderMicroservice{}
	templateMS := &portsmock.TemplateMicroservice{}
	credMS := &portsmock.CredentialMicroservice{}
	keySrc := &portsmock.KeySrc{}

	request := service.DeploymentCreateRunRequest{
		Session: service.Session{
			SessionActor:    actor.Actor,
			SessionEmulator: actor.Emulator,
		},
		CreateParam: service.DeploymentRunCreateParam{
			Deployment: deploymentID,
			ParamValues: map[string]interface{}{
				"username": "foobar",
				"param1":   "value1",
				"param2":   222,
			},
		},
	}

	storage.On("Get", request.CreateParam.Deployment).Return(deployment, nil)
	templateMS.On("Get", actor, deployment.Template).Return(&service.TemplateModel{}, errors.New("mock error, not found"))

	preflightChan := make(chan preflightstage.PreflightRequest, 1)
	run, err := newRunCreationSession(runCreationPorts{
		storage:    storage,
		runStorage: runStorage,
		providerMS: providerMS,
		templateMS: templateMS,
		credMS:     credMS,
		keySrc:     keySrc,
	}).CreateRun(request, preflightChan)
	if !assert.NotNil(t, err) {
		return
	}
	assert.Equal(t, "mock error, not found", err.Error())
	assert.Empty(t, run.ID)
	assert.Len(t, preflightChan, 0)

	storage.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	providerMS.AssertExpectations(t)
	templateMS.AssertExpectations(t)
	credMS.AssertExpectations(t)
	keySrc.AssertExpectations(t)
}
