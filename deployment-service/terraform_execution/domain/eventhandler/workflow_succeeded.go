package eventhandler

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/deploymentworkflow"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/executionstage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// WorkflowSucceededHandler handles events that is emitted when workflow succeeded.
type WorkflowSucceededHandler struct {
	storage         ports.DeploymentStorage
	runStorage      ports.DeploymentRunStorage
	executionChan   chan<- executionstage.ExecutionRequest
	portsDependency ports.Ports
}

// NewWorkflowSucceededHandler ...
func NewWorkflowSucceededHandler(portsDependency ports.Ports, executionChan chan<- executionstage.ExecutionRequest) WorkflowSucceededHandler {
	return WorkflowSucceededHandler{
		storage:         portsDependency.Storage,
		runStorage:      portsDependency.RunStorage,
		executionChan:   executionChan,
		portsDependency: portsDependency,
	}
}

// Handle ...
func (h WorkflowSucceededHandler) Handle(event types.IncomingEvent) ResponseEvent {
	unmarshalled, err := h.unmarshalEvent(event)
	if err != nil {
		log.WithFields(log.Fields{"eventType": event.EventType(), "tid": event.Transaction()}).WithError(err).Error("fail to unmarshal event")
		return h.newDoNotSentEvent()
	}
	return h.handle(unmarshalled)
}

func (h WorkflowSucceededHandler) unmarshalEvent(event types.IncomingEvent) (awmclient.WorkflowSucceeded, error) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "WorkflowSucceededHandler.unmarshalEvent",
	})

	var result awmclient.WorkflowSucceeded
	err := json.Unmarshal(event.CloudEvent().Data(), &result)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to WorkflowSucceeded"
		logger.WithError(err).Error(errorMessage)
		return awmclient.WorkflowSucceeded{}, service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error()))
	}
	return result, nil
}

func (h WorkflowSucceededHandler) handle(event awmclient.WorkflowSucceeded) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "WorkflowSucceededHandler.handle",
		"wfName":   event.WorkflowName,
	})
	logger.WithField("len(WfOutputs)", len(event.WfOutputs)).Info("WorkflowSucceeded event received")

	_, err := h.storage.SearchByDeletionWorkflow(event.WorkflowName)
	if err == nil {
		h.deploymentDeletionWorkflow(event)
		return h.newDoNotSentEvent()
	}
	logger.WithError(err).Warn("workflow is not deletion workflow")

	// if not deployment deletion workflow, then it is a deployment run workflow
	return h.deploymentRunWorkflow(event)
}

func (h WorkflowSucceededHandler) deploymentDeletionWorkflow(event awmclient.WorkflowSucceeded) {
	handler := deploymentworkflow.NewDeletionWorkflowHandler(h.portsDependency)
	handler.WorkflowSucceeded(event)
}

// workflow related to deployment run, e.g. preflight, execution
func (h WorkflowSucceededHandler) deploymentRunWorkflow(event awmclient.WorkflowSucceeded) ResponseEvent {

	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "WorkflowSucceededHandler.deploymentRunWorkflow",
		"wfName":   event.WorkflowName,
		"wfStatus": event.Status,
	})
	// find the deployment run with the wfName
	run, err := h.runStorage.SearchByWorkflow(event.WorkflowName)
	if err != nil {
		logger.WithError(err).Error("fail to find deployment run matching workflow")

		// TODO this should just return, but should NOT return an event
		return h.newDoNotSentEvent()
	}
	logger = logger.WithFields(log.Fields{
		"run":       run.ID,
		"runStatus": run.Status,
	})

	workflow, err := run.FindWorkflow(event.WorkflowName)
	if err != nil {
		logger.WithError(err).Error("fail to find workflow in run")
		return h.newDoNotSentEvent()
	}

	switch workflow.Purpose {
	case deploymentcommon.PreflightPurpose:
		if run.Status != deploymentcommon.DeploymentRunPreflight {
			logger.Warn("run has inconsistent status, should be " + deploymentcommon.DeploymentRunPreflight)
		}
		handler := deploymentworkflow.NewPreflightWorkflowHandler(h.portsDependency, h.executionChan)
		handler.WorkflowSucceeded(event, run)
	case deploymentcommon.ExecutionPurpose:
		if run.Status != deploymentcommon.DeploymentRunRunning {
			logger.Warn("run has inconsistent status, should be " + deploymentcommon.DeploymentRunRunning)
		}
		handler := deploymentworkflow.NewExecutionWorkflowHandler(h.portsDependency)
		handler.WorkflowSucceeded(event, run)
	default:
		logger.WithFields(log.Fields{"wfPurpose": workflow.Purpose}).Error("workflow has incorrect purpose")
	}
	return h.newDoNotSentEvent()
}

func (h WorkflowSucceededHandler) newDoNotSentEvent() ResponseEvent {
	return NewEvent(DoNotSent, "", service.Session{})
}
