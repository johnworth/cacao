package eventhandler

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/domainutils"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

type deletionResult struct {
	service.Session
	Deployment common.ID
	Deleted    bool // if error is empty, and Deleted is false, then the deletion process has just started
}

// ToRespEvent ...
func (result deletionResult) ToRespEvent(transaction common.TransactionID) ResponseEvent {
	var event = service.DeploymentDeletionResult{
		Session: result.Session,
		ID:      result.Deployment,
	}
	if result.GetServiceError() != nil {
		return NewEvent(service.DeploymentDeleteFailed, transaction, event)
	}
	if result.Deleted {
		return NewEvent(service.DeploymentDeleted, transaction, event)
	}
	// deletion started but not finished
	return NewEvent(service.DeploymentDeletionStarted, transaction, event)
}

// DeletionHandler handles deletion of deployment
type DeletionHandler struct {
	storage    ports.DeploymentStorage
	runStorage ports.DeploymentRunStorage
	awm        ports.ArgoWorkflowMediator
	templateMS ports.TemplateMicroservice
	credMS     ports.CredentialMicroservice
	keySrc     ports.KeySrc
}

// NewDeletionHandler ...
func NewDeletionHandler(portsDependency ports.Ports) DeletionHandler {
	return DeletionHandler{
		storage:    portsDependency.Storage,
		runStorage: portsDependency.RunStorage,
		awm:        portsDependency.AWM,
		templateMS: portsDependency.TemplateMS,
		credMS:     portsDependency.CredentialMS,
		keySrc:     portsDependency.KeySrc,
	}
}

// Handle ..
func (h DeletionHandler) Handle(event types.IncomingEvent) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "DeletionHandler.Handle",
	})
	request, err := h.eventToRequest(event)
	if err != nil {
		logger.WithError(err).Trace("fail to convert event to request")
		result := h.deleteFailedResult(request, err)
		return result.ToRespEvent(event.Transaction())
	}
	result := h.handle(event.Transaction(), request)
	h.logResponse(logger, result)
	return result.ToRespEvent(event.Transaction())
}

func (h DeletionHandler) eventToRequest(event types.IncomingEvent) (service.DeploymentDeletionRequest, error) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "DeletionHandler.eventToRequest",
	})

	var request service.DeploymentDeletionRequest
	err := json.Unmarshal(event.CloudEvent().Data(), &request)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to DeploymentDeletionRequest"
		logger.WithError(err).Error(errorMessage)
		return service.DeploymentDeletionRequest{}, service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error()))
	}
	return request, nil
}

// return nil if no event needs to be sent
func (h DeletionHandler) handle(transaction common.TransactionID, request service.DeploymentDeletionRequest) deletionResult {
	logger := log.WithFields(log.Fields{
		"package":    "eventhandler",
		"function":   "DeletionHandler.handle",
		"deployment": request.ID,
	})

	// check permission
	deployment, err := h.storage.Get(request.ID)
	if err != nil {
		return h.deleteFailedResult(request, err)
	}
	if !h.hasPermission(request, deployment) {
		return h.deleteFailedResult(request, service.NewCacaoUnauthorizedError("unauthorized access to deployment"))
	}

	err = h.deploymentStatusToDeleting(deployment)
	if err != nil {
		return h.deleteFailedResult(request, err)
	}

	// re-fetch after marking
	deployment, err = h.storage.Get(request.ID)
	if err != nil {
		return h.deleteFailedResult(request, err)
	}

	// check if deployment has resource that needs to be cleaned up
	needsCleanup, err := h.requireResourceCleanup(deployment)
	if err != nil {
		return h.deleteFailedResult(request, err)
	}
	if !needsCleanup {
		logger.Info("no resource to clean up, delete right away")
		// no need to clean up, delete deployment right away
		return h.performSimpleDelete(request)
	}

	// delete resources provisioned by the deployment
	err = h.startDeletingResources(transaction, request, deployment)
	if err != nil {
		logger.WithError(err).Error("workflow deletion failed immediately")
		return h.deletionFailedImmediately(request, deployment.ID, err)
	}
	logger.Trace("workflow deletion started")
	return h.deletionStartedResult(request)
}

func (h DeletionHandler) hasPermission(request service.DeploymentDeletionRequest, deployment deploymentcommon.Deployment) bool {
	// only owner/creator has permission to delete
	return request.GetSessionActor() == deployment.Owner()
}

func (h DeletionHandler) deploymentStatusToDeleting(deployment deploymentcommon.Deployment) error {
	ok, err := h.storage.PendingStatusDeleting(deployment.ID)
	if err != nil {
		return err
	}
	if !ok {
		// While it is possible for the deployment to be altered or deleted in storage (between pre-fetch call and update status call), it is unlikely.
		// Thus, (ok == false) likely mean that deployment status does not allow deletion or is already deleting.
		if deployment.PendingStatus == service.DeploymentStatusDeleting {
			// use pre-fetched value to determine if already deleting.
			return service.NewCacaoGeneralError("deployment already deleting")
		}
		if !deployment.CurrentStatus.AllowDeletion() {
			// use pre-fetched value to determine if current status allows deleting.
			msg := fmt.Sprintf("deployment status (%s, %s) does not allow deletion", deployment.CurrentStatus, deployment.PendingStatus)
			return service.NewCacaoGeneralError(msg)
		}
		log.WithFields(log.Fields{
			"package":  "eventhandler",
			"function": "DeletionHandler.deploymentStatusToDeleting",
		}).Info("corrupted data, deployment has conflict status or no longer exists")
		return service.NewCacaoGeneralError("deployment has conflict status or no longer exists")
	}
	return nil
}

func (h DeletionHandler) requireResourceCleanup(deployment deploymentcommon.Deployment) (bool, error) {
	if !h.hasDeploymentRun(deployment) {
		// if no deployment run, then there is no resource to clean up.
		return false, nil
	}
	if deployment.LastRun == nil {
		// guard against nil
		return false, service.NewCacaoGeneralError("corrupted data, deployment missing last run")
	}
	lastRun, err := h.runStorage.Get(*deployment.LastRun)
	if err != nil {
		return false, err
	}
	if len(lastRun.LastState.Resources) == 0 {
		// last run has no resource, no need to clean up
		return false, nil
	}
	return true, nil
}

// change status current_status to deleted, and clear pending_status, does NOT perform cleanup on resource.
func (h DeletionHandler) performSimpleDelete(request service.DeploymentDeletionRequest) deletionResult {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "DeletionHandler.performSimpleDelete",
	})
	if err := h.storage.StatusDeleted(request.ID); err != nil {
		logger.WithError(err).Error()
		return h.deleteFailedResult(request, err)
	}
	return h.deletedResult(request)
}

// hasDeploymentRun check if deployment has any deployment run, basically check if there are resources needs clean up.
// if there is no run, then no resource are provisioned.
func (h DeletionHandler) hasDeploymentRun(deployment deploymentcommon.Deployment) bool {
	if deployment.LastRun != nil {
		return true
	}
	hasRun, err := h.runStorage.DeploymentHasRun(deployment.ID)
	if err != nil {
		// assume there is still deployment run, even if cannot fetch it at the moment
		log.WithFields(log.Fields{
			"package":  "eventhandler",
			"function": "DeletionHandler.hasDeploymentRun",
		}).WithError(err).Error("fail to list runs under deployment")
		return true
	}
	return hasRun
}

func (h DeletionHandler) startDeletingResources(tid common.TransactionID, request service.DeploymentDeletionRequest, deployment deploymentcommon.Deployment) error {
	template, err := h.templateMS.Get(types.ActorFromSession(request.Session), deployment.Template)
	if err != nil {
		return err
	}
	param, err := h.getDeploymentParameters(template, deployment)
	if err != nil {
		return err
	}
	awmProvider, wfName, err := h.createWorkflow(request, deployment, param)
	if err != nil {
		return err
	}
	return h.storage.StartDeletionWorkflow(deployment.ID, tid, wfName, awmProvider)
}

func (h DeletionHandler) getDeploymentParameters(template service.Template, deployment deploymentcommon.Deployment) ([]deploymentcommon.DeploymentParameter, error) {
	// use the parameters from last run
	lastRun, err := h.runStorage.GetLatestRun(deployment.ID)
	if err != nil {
		return nil, err
	}
	converter := domainutils.ParameterConverter{}
	parameters, err := converter.Convert(template.GetMetadata().Parameters, lastRun.Parameters.ToParameterValues())
	if err != nil {
		return nil, err
	}
	return parameters, nil
}

func (h DeletionHandler) createWorkflow(
	request service.DeploymentDeletionRequest,
	deployment deploymentcommon.Deployment,
	param []deploymentcommon.DeploymentParameter,
) (common.ID, string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "DeletionHandler.createWorkflow",
	})
	actor := types.ActorFromSession(request.Session)
	// TODO use snapshot from deployment run if available
	template, err := h.templateMS.Get(actor, deployment.Template)
	if err != nil {
		logger.WithError(err).Error("fail to fetch template")
		return "", "", err
	}
	snapshot, err := domainutils.GetTemplateSnapshot(template)
	if err != nil {
		logger.WithError(err).Error("fail to convert template to snapshot")
		return "", "", err
	}

	wfBuilder := domainutils.NewTerraformWorkflowBuilder(h.awm, h.credMS, h.keySrc)
	wfBuilder.SetActor(actor)
	wfBuilder.SetDeployment(deployment)
	wfBuilder.SetTemplateSnapshot(snapshot)
	wfBuilder.SetParameter(param)
	wfBuilder.SetTerraformOperation("destroy")

	awmProvider, wfName, err := wfBuilder.CreateWorkflow()
	if err != nil {
		logger.WithError(err).Error("fail to create workflow via AWM")
		return "", "", err
	}
	logger.WithField("wfName", wfName).Info("workflow created")
	return awmProvider, wfName, nil
}

// workflow deletion failed immediately
func (h DeletionHandler) deletionFailedImmediately(request service.DeploymentDeletionRequest, deployment common.ID, err error) deletionResult {
	logger := log.WithFields(log.Fields{
		"function":   "DeletionHandler.deletionFailedImmediately",
		"deployment": deployment,
	})
	err2 := h.storage.StatusDeletionErrored(deployment)
	if err2 != nil {
		logger.WithError(err2).Error("fail to update deployment status after deletion failed")
		return h.deleteFailedResult(request, err2)
	}
	return h.deleteFailedResult(request, err)
}

func (h DeletionHandler) deleteFailedResult(request service.DeploymentDeletionRequest, err error) deletionResult {
	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	return deletionResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
			ServiceError:    serviceError.GetBase(),
		},
		Deployment: request.ID,
		Deleted:    false,
	}
}

func (h DeletionHandler) deletedResult(request service.DeploymentDeletionRequest) deletionResult {
	return deletionResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Deployment: request.ID,
		Deleted:    true,
	}
}

func (h DeletionHandler) deletionStartedResult(request service.DeploymentDeletionRequest) deletionResult {
	return deletionResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Deployment: request.ID,
		Deleted:    false,
	}
}

func (h DeletionHandler) logResponse(logger *log.Entry, result deletionResult) {
	logger.WithFields(log.Fields{
		"actor":      result.GetSessionActor(),
		"emulator":   result.GetSessionEmulator(),
		"deployment": result.Deployment,
		"err":        result.GetServiceError(),
		"deleted":    result.Deleted,
	}).Info("response event")
}
