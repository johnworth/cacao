package eventhandler

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/storage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// UpdateHandler handles updating deployment
type UpdateHandler struct {
	storage ports.DeploymentStorage
}

// NewUpdateHandler ...
func NewUpdateHandler(storage ports.DeploymentStorage) UpdateHandler {
	return UpdateHandler{storage: storage}
}

// Handle ...
func (h UpdateHandler) Handle(event types.IncomingEvent) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "UpdateHandler.Handle",
	})
	var request service.DeploymentUpdateRequest
	request, err := h.eventToRequest(event)
	if err != nil {
		logger.WithError(err).Trace("fail to convert event to request")
		result := h.createFailedResult(request, err)

		return h.toRespEvent(event.Transaction(), result)
	}
	return h.toRespEvent(event.Transaction(), h.handle(request))
}

func (h UpdateHandler) eventToRequest(event types.IncomingEvent) (service.DeploymentUpdateRequest, error) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "UpdateHandler.eventToRequest",
	})

	var request service.DeploymentUpdateRequest
	err := json.Unmarshal(event.CloudEvent().Data(), &request)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to DeploymentUpdateRequest"
		logger.WithError(err).Error(errorMessage)
		return service.DeploymentUpdateRequest{}, service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error()))
	}
	return request, nil
}

func (h UpdateHandler) handle(request service.DeploymentUpdateRequest) service.DeploymentUpdateResult {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "UpdateHandler.handle",
		"fields":   request.UpdateFields,
	})
	if err := h.validateRequest(request); err != nil {
		logger.WithError(err).Error("fail to valid request")
		return h.createFailedResult(request, err)
	}
	deployment, err := h.storage.Get(request.Deployment.ID)
	if err != nil {
		logger.WithError(err).Error("fail to get deployment")
		return h.createFailedResult(request, err)
	}
	if err = h.checkAuthorization(request, deployment); err != nil {
		logger.WithError(err).Error("authorization check failed")
		return h.createFailedResult(request, err)
	}
	updatedFieldCount, err := h.updateDeployment(request, deployment)
	if err != nil {
		logger.WithError(err).Error("fail to update deployment in storage")
		return service.DeploymentUpdateResult{}
	}
	if updatedFieldCount == 0 {
		logger.Info("nothing to update")
	} else {
		logger.Info("fields updated")
	}
	return service.DeploymentUpdateResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		ID: request.Deployment.ID,
	}
}

func (h UpdateHandler) validateRequest(request service.DeploymentUpdateRequest) error {
	if request.GetSessionActor() == "" {
		return service.NewCacaoInvalidParameterError("actor is empty")
	}
	if request.Deployment.ID == "" || !request.Deployment.ID.Validate() {
		return service.NewCacaoInvalidParameterError("bad deployment id")
	}
	return nil
}

func (h UpdateHandler) checkAuthorization(request service.DeploymentUpdateRequest, deployment deploymentcommon.Deployment) error {
	if deployment.CreatedBy.User != request.GetSessionActor() {
		return service.NewCacaoUnauthorizedError("not authorized")
	}
	return nil
}

func (h UpdateHandler) updateDeployment(request service.DeploymentUpdateRequest, deployment deploymentcommon.Deployment) (int, error) {
	if request.UpdateFields == nil || len(request.UpdateFields) <= 0 {
		return 0, nil
	}
	updatedDeployment, fieldCount := h.updateStructFields(request, deployment)
	if fieldCount == 0 {
		// still no field to update, all fields specified are not supported for update
		return 0, nil
	}
	// only update in storage if there are fields to update
	err := h.storage.Update(request.Deployment.ID, updatedDeployment)
	if err != nil {
		return fieldCount, err
	}
	return fieldCount, nil
}

func (h UpdateHandler) updateStructFields(request service.DeploymentUpdateRequest, deployment deploymentcommon.Deployment) (updates storage.DeploymentUpdate, fieldCount int) {
	for _, field := range request.UpdateFields {
		switch field {
		case "name":
			updates.Name = &request.Deployment.Name
		case "description":
			updates.Description = &request.Deployment.Description
		case "template":
			updates.Template = &request.Deployment.Template
		case "primary_provider":
			updates.PrimaryCloudProvider = &request.Deployment.PrimaryCloud
		case "cloud_credentials":
			updates.CloudCredentials = make([]deploymentcommon.ProviderCredentialPair, 0)
			for credID, providerID := range request.Deployment.CloudCredentials {
				if credID == "" {
					// skip empty credential ID
					log.WithFields(log.Fields{
						"package":  "eventhandler",
						"function": "UpdateHandler.updateStructFields",
					}).Debug("skip empty credential ID")
					continue
				}
				if providerID == "" {
					providerID = deployment.PrimaryCloudProvider
				}
				updates.CloudCredentials = append(
					updates.CloudCredentials,
					deploymentcommon.ProviderCredentialPair{
						Credential: deploymentcommon.CredentialID(credID),
						Provider:   providerID,
					})
			}
		case "git_credential":
			var gitCred = deploymentcommon.CredentialID(request.Deployment.GitCredential)
			updates.GitCredential = &gitCred
		default:
			continue
		}
		fieldCount++
	}
	return updates, fieldCount
}

func (h UpdateHandler) createFailedResult(request service.DeploymentUpdateRequest, err error) service.DeploymentUpdateResult {
	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	return service.DeploymentUpdateResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
			ServiceError:    serviceError.GetBase(),
		},
		ID: request.Deployment.ID,
	}
}

func (h UpdateHandler) toRespEvent(transaction common.TransactionID, result service.DeploymentUpdateResult) ResponseEvent {
	if result.GetServiceError() == nil {
		return NewEvent(service.DeploymentUpdated, transaction, result)
	}

	return NewEvent(service.DeploymentUpdateFailed, transaction, result)
}
