package eventhandler

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/storage"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports/mocks"
	"testing"
)

func TestUpdateHandler_Handle(t *testing.T) {
	t.Run("update name", testUpdateDeploymentName)
	t.Run("update description", testUpdateDeploymentDescription)
	t.Run("update template", testUpdateDeploymentTemplate)
	t.Run("update primary provider", testUpdateDeploymentPrimaryProvider)
	t.Run("update cloud cred", testUpdateDeploymentCloudCred)
	t.Run("update git cred", testUpdateDeploymentGitCred)
	t.Run("update id (not supported)", testUpdateDeploymentIDNotSupported)
	t.Run("no field to update", testUpdateDeploymentNoField)
	t.Run("not authorized", testUpdateDeploymentNotAuthorized)
	t.Run("not exist", testUpdateNonExistDeployment)
}

func testUpdateDeploymentName(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := deploymentcommon.Deployment{
		ID:   deploymentID,
		Name: "oldName",
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
	}
	newName := "newName"
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	storageMock.On("Update", deploymentID, storage.DeploymentUpdate{Name: &newName}).Return(nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:   deploymentID,
			Name: "newName",
		},
		UpdateFields: []string{"name"},
	}
	event := NewIncomingEvent(service.DeploymentUpdateRequested, request)
	responseEvent := handler.Handle(event)
	assert.NotNil(t, responseEvent)
	assert.Equal(t, event.Transaction(), responseEvent.Transaction())
	assert.Equal(t, service.DeploymentUpdated, responseEvent.EventType())
	result, ok := responseEvent.event.(service.DeploymentUpdateResult)
	if !assert.True(t, ok) {
		return
	}
	assert.Equal(t, deploymentID, result.ID)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentDescription(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := deploymentcommon.Deployment{
		ID:          deploymentID,
		Description: "oldDescription",
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
	}
	newDescription := "newDescription"
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	storageMock.On("Update", deploymentID, storage.DeploymentUpdate{Description: &newDescription}).Return(nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:          deploymentID,
			Description: "newDescription",
		},
		UpdateFields: []string{"description"},
	}
	event := NewIncomingEvent(service.DeploymentUpdateRequested, request)
	responseEvent := handler.Handle(event)
	assert.NotNil(t, responseEvent)
	assert.Equal(t, event.Transaction(), responseEvent.Transaction())
	assert.Equal(t, service.DeploymentUpdated, responseEvent.EventType())
	result, ok := responseEvent.event.(service.DeploymentUpdateResult)
	if !assert.True(t, ok) {
		return
	}
	assert.Equal(t, deploymentID, result.ID)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentTemplate(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := deploymentcommon.Deployment{
		ID: deploymentID,
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
		Template: common.NewID("template"),
	}
	newTemplateID := common.NewID("template")
	newTemplate := newTemplateID
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	storageMock.On("Update", deploymentID, storage.DeploymentUpdate{Template: &newTemplate}).Return(nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:       deploymentID,
			Template: newTemplateID,
		},
		UpdateFields: []string{"template"},
	}
	event := NewIncomingEvent(service.DeploymentUpdateRequested, request)
	responseEvent := handler.Handle(event)
	assert.NotNil(t, responseEvent)
	assert.Equal(t, event.Transaction(), responseEvent.Transaction())
	assert.Equal(t, service.DeploymentUpdated, responseEvent.EventType())
	result, ok := responseEvent.event.(service.DeploymentUpdateResult)
	if !assert.True(t, ok) {
		return
	}
	assert.Equal(t, deploymentID, result.ID)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentPrimaryProvider(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := deploymentcommon.Deployment{
		ID: deploymentID,
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
		PrimaryCloudProvider: common.NewID("provider"),
	}
	newProviderID := common.NewID("provider")
	newPrimaryCloudProvider := newProviderID
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	storageMock.On("Update", deploymentID, storage.DeploymentUpdate{PrimaryCloudProvider: &newPrimaryCloudProvider}).Return(nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:           deploymentID,
			PrimaryCloud: newProviderID,
		},
		UpdateFields: []string{"primary_provider"},
	}
	event := NewIncomingEvent(service.DeploymentUpdateRequested, request)
	responseEvent := handler.Handle(event)
	assert.NotNil(t, responseEvent)
	assert.Equal(t, event.Transaction(), responseEvent.Transaction())
	assert.Equal(t, service.DeploymentUpdated, responseEvent.EventType())
	result, ok := responseEvent.event.(service.DeploymentUpdateResult)
	if !assert.True(t, ok) {
		return
	}
	assert.Equal(t, deploymentID, result.ID)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentCloudCred(t *testing.T) {
	deploymentID := common.NewID("deployment")
	primaryProviderID := common.NewID("provider")
	deployment := deploymentcommon.Deployment{
		ID: deploymentID,
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
		PrimaryCloudProvider: primaryProviderID,
		CloudCredentials: deploymentcommon.ProviderCredentialMappings{
			{
				Credential: "old-cred",
				Provider:   primaryProviderID,
			},
		},
	}
	newCloudCred := "new-cloud-cred"
	updatedCloudCredentials := deploymentcommon.ProviderCredentialMappings{
		{
			Credential: deploymentcommon.CredentialID(newCloudCred),
			Provider:   primaryProviderID,
		},
	}
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	storageMock.On("Update", deploymentID, storage.DeploymentUpdate{CloudCredentials: updatedCloudCredentials}).Return(nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID: deploymentID,
			CloudCredentials: map[string]common.ID{
				newCloudCred: "", // provider ID may be missing from request
			},
		},
		UpdateFields: []string{"cloud_credentials"},
	}
	event := NewIncomingEvent(service.DeploymentUpdateRequested, request)
	responseEvent := handler.Handle(event)
	assert.NotNil(t, responseEvent)
	assert.Equal(t, event.Transaction(), responseEvent.Transaction())
	assert.Equal(t, service.DeploymentUpdated, responseEvent.EventType())
	result, ok := responseEvent.event.(service.DeploymentUpdateResult)
	if !assert.True(t, ok) {
		return
	}
	assert.Nil(t, result.GetServiceError())
	assert.Equal(t, deploymentID, result.ID)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentGitCred(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := deploymentcommon.Deployment{
		ID: deploymentID,
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
	}
	newGitCred := "newGitCred"
	updatedGitCredential := deploymentcommon.CredentialID(newGitCred)
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	storageMock.On("Update", deploymentID, storage.DeploymentUpdate{GitCredential: &updatedGitCredential}).Return(nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:            deploymentID,
			GitCredential: newGitCred,
		},
		UpdateFields: []string{"git_credential"},
	}
	event := NewIncomingEvent(service.DeploymentUpdateRequested, request)
	responseEvent := handler.Handle(event)
	assert.NotNil(t, responseEvent)
	assert.Equal(t, event.Transaction(), responseEvent.Transaction())
	assert.Equal(t, service.DeploymentUpdated, responseEvent.EventType())
	result, ok := responseEvent.event.(service.DeploymentUpdateResult)
	if !assert.True(t, ok) {
		return
	}
	assert.Equal(t, deploymentID, result.ID)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentIDNotSupported(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := deploymentcommon.Deployment{
		ID:          deploymentID,
		Description: "oldDescription",
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
	}
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID: deploymentID,
		},
		UpdateFields: []string{"id"},
	}
	event := NewIncomingEvent(service.DeploymentUpdateRequested, request)
	responseEvent := handler.Handle(event)
	assert.NotNil(t, responseEvent)
	assert.Equal(t, event.Transaction(), responseEvent.Transaction())
	assert.Equal(t, service.DeploymentUpdated, responseEvent.EventType())
	result, ok := responseEvent.event.(service.DeploymentUpdateResult)
	if !assert.True(t, ok) {
		return
	}
	assert.Equal(t, deploymentID, result.ID)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentNoField(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := deploymentcommon.Deployment{
		ID:   deploymentID,
		Name: "oldName",
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
	}
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:   deploymentID,
			Name: "newName",
		},
		UpdateFields: []string{}, // no field to update
	}
	event := NewIncomingEvent(service.DeploymentUpdateRequested, request)
	responseEvent := handler.Handle(event)
	assert.NotNil(t, responseEvent)
	assert.Equal(t, event.Transaction(), responseEvent.Transaction())
	assert.Equal(t, service.DeploymentUpdated, responseEvent.EventType())
	result, ok := responseEvent.event.(service.DeploymentUpdateResult)
	if !assert.True(t, ok) {
		return
	}
	assert.Equal(t, deploymentID, result.ID)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentNotAuthorized(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := deploymentcommon.Deployment{
		ID:   deploymentID,
		Name: "oldName",
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
	}
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "not-authorized-user123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:   deploymentID,
			Name: "newName",
		},
		UpdateFields: []string{"name"},
	}
	event := NewIncomingEvent(service.DeploymentUpdateRequested, request)
	responseEvent := handler.Handle(event)
	assert.NotNil(t, responseEvent)
	assert.Equal(t, event.Transaction(), responseEvent.Transaction())
	assert.Equal(t, service.DeploymentUpdateFailed, responseEvent.EventType())
	result, ok := responseEvent.event.(service.DeploymentUpdateResult)
	if !assert.True(t, ok) {
		return
	}
	assert.Equal(t, deploymentID, result.ID)
	assert.IsType(t, &service.CacaoUnauthorizedError{}, result.GetServiceError())
	storageMock.AssertExpectations(t)
}

func testUpdateNonExistDeployment(t *testing.T) {
	deploymentID := common.NewID("deployment")
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deploymentcommon.Deployment{}, service.NewCacaoNotFoundError("not found"))

	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:   deploymentID,
			Name: "newName",
		},
		UpdateFields: []string{"name"},
	}
	event := NewIncomingEvent(service.DeploymentUpdateRequested, request)
	responseEvent := handler.Handle(event)
	assert.NotNil(t, responseEvent)
	assert.Equal(t, event.Transaction(), responseEvent.Transaction())
	assert.Equal(t, service.DeploymentUpdateFailed, responseEvent.EventType())
	result, ok := responseEvent.event.(service.DeploymentUpdateResult)
	if !assert.True(t, ok) {
		return
	}
	assert.Equal(t, deploymentID, result.ID)
	assert.IsType(t, &service.CacaoNotFoundError{}, result.GetServiceError())
	storageMock.AssertExpectations(t)
}
