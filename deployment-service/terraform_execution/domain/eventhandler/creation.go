package eventhandler

import (
	"encoding/json"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// CreationHandler handles creation of deployment.
// This handles only perform permission checks and basic validation, just enough to create
// a valid deployment object in storage.
type CreationHandler struct {
	storage     ports.DeploymentStorage
	templateMS  ports.TemplateMicroservice
	workspaceMS ports.WorkspaceMicroservice
	providerMS  ports.ProviderMicroservice
	credMS      ports.CredentialMicroservice
}

// NewCreationHandler ...
func NewCreationHandler(portsDependency ports.Ports) CreationHandler {
	return CreationHandler{
		storage:     portsDependency.Storage,
		templateMS:  portsDependency.TemplateMS,
		workspaceMS: portsDependency.WorkspaceMS,
		providerMS:  portsDependency.ProviderMS,
		credMS:      portsDependency.CredentialMS,
	}
}

// Handle ...
func (h CreationHandler) Handle(event types.IncomingEvent) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "CreationHandler.Handle",
	})
	var request service.DeploymentCreationRequest
	request, err := h.eventToRequest(event)
	if err != nil {
		logger.WithError(err).Trace("fail to convert event to request")
		result := h.createFailedResult(request, err)

		return h.toRespEvent(event.Transaction(), result)
	}
	return h.toRespEvent(event.Transaction(), h.handle(request))
}

func (h CreationHandler) eventToRequest(event types.IncomingEvent) (service.DeploymentCreationRequest, error) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "CreationHandler.eventToRequest",
	})

	var request service.DeploymentCreationRequest
	err := json.Unmarshal(event.CloudEvent().Data(), &request)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to DeploymentCreationRequest"
		logger.WithError(err).Error(errorMessage)
		return service.DeploymentCreationRequest{}, service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error()))
	}
	return request, nil
}

func (h CreationHandler) handle(request service.DeploymentCreationRequest) service.DeploymentCreationResult {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "CreationHandler.handle",
	})

	deployment, err := h.convertRequestToDeployment(request)
	if err != nil {
		logger.WithError(err).Error("fail to convert request to deployment object")
		return h.createFailedResult(request, err)
	}

	err = h.storage.Create(*deployment)
	if err != nil {
		logger.WithError(err).Error("fail to insert deployment into storage")
		return h.createFailedResult(request, err)
	}
	logger.WithField("deployment", deployment.ID).Info("deployment created")

	return service.DeploymentCreationResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.SessionEmulator,
		},
		ID: deployment.ID,
	}
}

func (h CreationHandler) convertRequestToDeployment(request service.DeploymentCreationRequest) (*deploymentcommon.Deployment, error) {

	actor := types.ActorFromSession(request.Session)
	if actor.Actor == "" {
		return nil, service.NewCacaoInvalidParameterError("actor cannot be empty")
	}
	err := h.checkWorkspace(actor, request.CreateParam.Workspace)
	if err != nil {
		return nil, err
	}
	err = h.checkProviders(actor, request.CreateParam.PrimaryCloud, request.CreateParam.CloudProviders)
	if err != nil {
		return nil, err
	}
	err = h.checkCredentials(actor, request.CreateParam.CloudCredentials, request.CreateParam.GitCredential)
	if err != nil {
		return nil, err
	}

	template, err := h.getTemplate(request)
	if err != nil {
		return nil, err
	}

	now := time.Now().UTC()

	return &deploymentcommon.Deployment{
		ID:          common.NewID(types.DeploymentIDPrefix),
		Name:        request.CreateParam.Name,
		Description: request.CreateParam.Description,
		CreatedAt:   now,
		UpdatedAt:   now,
		Workspace:   request.CreateParam.Workspace,
		CreatedBy: deploymentcommon.Creator{
			User:     request.GetSessionActor(),
			Emulator: request.GetSessionEmulator(),
		},
		Template:             request.CreateParam.Template,
		TemplateType:         template.GetMetadata().TemplateTypeName,
		PrimaryCloudProvider: request.CreateParam.PrimaryCloud,
		CurrentStatus:        service.DeploymentStatusNone,
		PendingStatus:        service.DeploymentStatusNoPending,
		StatusMsg:            "",
		CloudCredentials:     h.convertCloudCredentials(request),
		GitCredential:        deploymentcommon.CredentialID(request.CreateParam.GitCredential),
		LastRun:              nil,
	}, nil
}

func (h CreationHandler) checkWorkspace(actor types.Actor, workspaceID common.ID) error {
	if workspaceID == "" {
		return service.NewCacaoInvalidParameterError("workspace ID is required")
	}
	if !workspaceID.Validate() {
		return service.NewCacaoInvalidParameterError("workspace ID is not valid")
	}
	// check if workspace exist and accessible
	workspace, err := h.workspaceMS.Get(actor, workspaceID)
	if err != nil {
		return err
	}
	if workspace.GetOwner() != actor.Actor {
		return service.NewCacaoUnauthorizedError("unauthorized access to the workspace")
	}
	return nil
}

func (h CreationHandler) checkProviders(actor types.Actor, primaryProvider common.ID, otherProviders []common.ID) error {
	err := h.checkProviderIDs(primaryProvider, otherProviders)
	if err != nil {
		return err
	}
	providerList, err := h.providerMS.List(actor)
	if err != nil {
		return err
	}
	primaryProviderExist := false
	for _, provider := range providerList {
		if provider.GetID() == primaryProvider {
			primaryProviderExist = true
			break
		}
	}
	if !primaryProviderExist {
		return service.NewCacaoNotFoundError("primary provider not exist")
	}

	if otherProviders == nil || len(otherProviders) == 0 {
		return nil
	}
	for _, providerID := range otherProviders {
		providerExist := false
		for _, provider := range providerList {
			if provider.GetID() == providerID {
				providerExist = true
				break
			}
		}
		if !providerExist {
			errorMessage := fmt.Sprintf("provider %s not exist", providerID)
			return service.NewCacaoNotFoundError(errorMessage)
		}
	}
	return nil
}

func (h CreationHandler) checkProviderIDs(primaryProvider common.ID, otherProviders []common.ID) error {
	if primaryProvider == "" {
		return service.NewCacaoInvalidParameterError("primary provider is required")
	}
	if !primaryProvider.Validate() {
		return service.NewCacaoInvalidParameterError("primary provider ID is not valid")
	}
	for _, providerID := range otherProviders {
		if !providerID.Validate() {
			errorMessage := fmt.Sprintf("provider ID %s is not valid", providerID)
			return service.NewCacaoInvalidParameterError(errorMessage)
		}
	}
	return nil
}

// check if credential IDs (cloud & git) exists
func (h CreationHandler) checkCredentials(actor types.Actor, cloudCredentials map[string]common.ID, gitCredential string) error {
	err := h.checkCloudCredentialIDs(cloudCredentials)
	if err != nil {
		return err
	}
	for cloudCred := range cloudCredentials {
		_, err := h.credMS.Get(actor, actor.Actor, cloudCred)
		if err != nil {
			errorMessage := fmt.Sprintf("cloud credential %s does not exist, %s", cloudCred, err.Error())
			return service.NewCacaoNotFoundError(errorMessage)
		}
	}
	if gitCredential == "" {
		// skip checking git cred if empty (not provided)
		return nil
	}
	_, err = h.credMS.Get(actor, actor.Actor, gitCredential)
	if err != nil {
		errorMessage := fmt.Sprintf("git credential does not exist, %s", err.Error())
		return service.NewCacaoNotFoundError(errorMessage)
	}
	return nil
}

func (h CreationHandler) checkCloudCredentialIDs(cloudCredentials map[string]common.ID) error {
	if cloudCredentials == nil || len(cloudCredentials) == 0 {
		return service.NewCacaoInvalidParameterError("no cloud credential is provided")
	}
	for credID := range cloudCredentials {
		if credID == "" {
			return service.NewCacaoInvalidParameterError("cloud credential cannot be empty string")
		}
	}
	return nil
}

func (h CreationHandler) convertCloudCredentials(request service.DeploymentCreationRequest) deploymentcommon.ProviderCredentialMappings {
	var providerCredMapping = make(deploymentcommon.ProviderCredentialMappings, 0, len(request.CreateParam.CloudCredentials))
	for credID, providerID := range request.CreateParam.CloudCredentials {
		pair := deploymentcommon.ProviderCredentialPair{
			Credential: deploymentcommon.CredentialID(credID),
			Provider:   providerID,
		}
		providerCredMapping = append(providerCredMapping, pair)
	}
	return providerCredMapping
}

func (h CreationHandler) getTemplate(request service.DeploymentCreationRequest) (service.Template, error) {
	actor := types.ActorFromSession(request.Session)
	// get template
	template, err := h.templateMS.Get(actor, request.CreateParam.Template)
	if err != nil {
		return nil, err
	}
	if template.GetMetadata().TemplateTypeName == "" {
		return nil, service.NewCacaoInvalidParameterError("template does not have template type")
	}
	return template, nil
}

func (h CreationHandler) createFailedResult(request service.DeploymentCreationRequest, err error) service.DeploymentCreationResult {
	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	return service.DeploymentCreationResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
			ServiceError:    serviceError.GetBase(),
		},
	}
}

func (h CreationHandler) toRespEvent(transaction common.TransactionID, result service.DeploymentCreationResult) ResponseEvent {
	if result.GetServiceError() == nil {
		return NewEvent(service.DeploymentCreated, transaction, result)
	}

	return NewEvent(service.DeploymentCreateFailed, transaction, result)
}
