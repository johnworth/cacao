package eventhandler

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/deploymentworkflow"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// WorkflowFailedHandler handles events that is emitted when workflow failed.
type WorkflowFailedHandler struct {
	storage         ports.DeploymentStorage
	runStorage      ports.DeploymentRunStorage
	portsDependency ports.Ports
}

// NewWorkflowFailedHandler ...
func NewWorkflowFailedHandler(portsDependency ports.Ports) WorkflowFailedHandler {
	return WorkflowFailedHandler{
		storage:         portsDependency.Storage,
		runStorage:      portsDependency.RunStorage,
		portsDependency: portsDependency,
	}
}

// Handle ...
func (h WorkflowFailedHandler) Handle(event types.IncomingEvent) ResponseEvent {
	unmarshalled, err := h.unmarshalEvent(event)
	if err != nil {
		log.WithFields(log.Fields{"eventType": event.EventType(), "tid": event.Transaction()}).WithError(err).Error("fail to unmarshal event")
		return h.newDoNotSentEvent()
	}
	return h.handle(unmarshalled)
}

func (h WorkflowFailedHandler) unmarshalEvent(event types.IncomingEvent) (awmclient.WorkflowFailed, error) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "WorkflowFailedHandler.unmarshalEvent",
	})

	var result awmclient.WorkflowFailed
	err := json.Unmarshal(event.CloudEvent().Data(), &result)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to WorkflowFailed"
		logger.WithError(err).Error(errorMessage)
		return awmclient.WorkflowFailed{}, service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error()))
	}
	return result, nil
}

func (h WorkflowFailedHandler) handle(event awmclient.WorkflowFailed) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "WorkflowFailedHandler.handle",
		"wfName":   event.WorkflowName,
	})
	logger.WithField("wfStatus", event.Status).Info("WorkflowFailed event received")

	_, err := h.storage.SearchByDeletionWorkflow(event.WorkflowName)
	if err == nil {
		h.deploymentDeletionWorkflow(event)
		return h.newDoNotSentEvent()
	}

	return h.deploymentRunWorkflow(event)
}

func (h WorkflowFailedHandler) deploymentDeletionWorkflow(event awmclient.WorkflowFailed) {
	handler := deploymentworkflow.NewDeletionWorkflowHandler(h.portsDependency)
	handler.WorkflowFailed(event)
}

func (h WorkflowFailedHandler) deploymentRunWorkflow(event awmclient.WorkflowFailed) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "WorkflowFailedHandler.deploymentRunWorkflow",
		"wfName":   event.WorkflowName,
		"wfStatus": event.Status,
	})
	// find the deployment run with the wfName
	run, err := h.runStorage.SearchByWorkflow(event.WorkflowName)
	if err != nil {
		logger.WithError(err).Error("fail to find deployment run matching workflow")

		// TODO this should just return, but should NOT return an event
		return h.newDoNotSentEvent()
	}
	logger = logger.WithFields(log.Fields{
		"run":       run.ID,
		"runStatus": run.Status,
	})

	workflow, err := run.FindWorkflow(event.WorkflowName)
	if err != nil {
		return ResponseEvent{}
	}
	switch workflow.Purpose {
	case deploymentcommon.PreflightPurpose:
		if run.Status != deploymentcommon.DeploymentRunPreflight {
			logger.Warn("run has inconsistent status, should be " + deploymentcommon.DeploymentRunPreflight)
		}
		handler := deploymentworkflow.NewPreflightWorkflowHandler(h.portsDependency, nil)
		handler.WorkflowFailed(event, run)
	case deploymentcommon.ExecutionPurpose:
		if run.Status != deploymentcommon.DeploymentRunRunning {
			logger.Warn("run has inconsistent status, should be " + deploymentcommon.DeploymentRunRunning)
		}
		handler := deploymentworkflow.NewExecutionWorkflowHandler(h.portsDependency)
		handler.WorkflowFailed(event, run)
	default:
		logger.WithFields(log.Fields{"wfPurpose": workflow.Purpose}).Error("workflow has incorrect purpose")
	}
	return h.newDoNotSentEvent()
}

func (h WorkflowFailedHandler) newDoNotSentEvent() ResponseEvent {
	return NewEvent(DoNotSent, "", service.Session{})
}
