package domainutils

import (
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
)

// GetTemplateSnapshot get a snapshot of a template
func GetTemplateSnapshot(template service.Template) (deploymentcommon.TemplateSnapshot, error) {
	var gitBranch string
	var gitTag string
	var gitCommit string
	var subPath string
	_, ok := template.GetSource().AccessParameters["branch"]
	if ok {
		gitBranch = template.GetSource().AccessParameters["branch"].(string)
	}
	_, ok = template.GetSource().AccessParameters["tag"]
	if ok {
		gitTag = template.GetSource().AccessParameters["tag"].(string)
	}
	_, ok = template.GetSource().AccessParameters["commit"]
	if ok {
		gitCommit = template.GetSource().AccessParameters["commit"].(string)
	}
	_, ok = template.GetSource().AccessParameters["path"]
	if ok {
		subPath = template.GetSource().AccessParameters["path"].(string)
	}
	return deploymentcommon.TemplateSnapshot{
		TemplateID: template.GetID(),
		UpstreamTracked: struct {
			Branch string `bson:"branch"`
			Tag    string `bson:"tag"`
		}{
			Branch: gitBranch,
			Tag:    gitTag,
		},
		GitURL:     template.GetSource().URI,
		CommitHash: gitCommit,
		SubPath:    subPath,
	}, nil
}
