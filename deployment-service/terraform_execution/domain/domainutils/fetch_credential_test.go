package domainutils

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"io"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	portsmock "gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

func TestCredentialEncoder_EncodeGitCred(t *testing.T) {
	credMS := &portsmock.CredentialMicroservice{}
	keySrc := &portsmock.KeySrc{}
	encoder := NewCredentialEncoder(credMS, keySrc)

	actor := types.Actor{
		Actor:    "test_user123",
		Emulator: "",
	}
	credential := &service.CredentialModel{
		Session:           service.Session{},
		Username:          "test_user123",
		Value:             `{"username":"foo","password":"bar"}`,
		Type:              "git",
		ID:                "git-cred-123",
		CreatedAt:         time.Time{},
		UpdatedAt:         time.Time{},
		UpdatedBy:         "",
		UpdatedEmulatorBy: "",
	}
	credMS.On("Get", actor, actor.Actor, "git-cred-123").Return(credential, nil)
	key := make([]byte, 32)
	_, err := io.ReadFull(rand.Reader, key)
	if err != nil {
		panic(err)
	}
	keySrc.On("GetKey").Return(key)

	credEncoded, err := encoder.EncodeGitCred(actor, "git-cred-123")

	assert.NoError(t, err)
	assert.NotEmpty(t, credEncoded)
	cipher, err := base64.StdEncoding.DecodeString(credEncoded)
	assert.NoError(t, err)
	plaintext, err := AESDecrypt(key, cipher)
	assert.NoError(t, err)
	var gitCred awmclient.GitCredential
	err = json.Unmarshal(plaintext, &gitCred)
	assert.NoError(t, err)
	assert.Equal(t, "foo", gitCred.Username)
	assert.Equal(t, "bar", gitCred.Password)

	credMS.AssertExpectations(t)
	keySrc.AssertExpectations(t)
}

func TestCredentialEncoder_EncodeOpenstackCred(t *testing.T) {
	t.Run("normal", func(t *testing.T) {
		credMS := &portsmock.CredentialMicroservice{}
		keySrc := &portsmock.KeySrc{}
		encoder := NewCredentialEncoder(credMS, keySrc)

		actor := types.Actor{
			Actor:    "test_user123",
			Emulator: "",
		}
		openstackCredIn := awmclient.OpenStackCredential{
			IdentityAPIVersion: "apiVersion",
			RegionName:         "regionName",
			Interface:          "interface",
			AuthURL:            "https://foobar.com",
			ProjectDomainID:    "projectDomainID",
			ProjectDomainName:  "projectDomainName",
			ProjectID:          "projectID",
			ProjectName:        "projectName",
			UserDomainName:     "userDomainName",
			Username:           "username",
			Password:           "password",
			AuthType:           "authType",
			AppCredID:          "appCredID",
			AppCredName:        "appCredName",
			AppCredSecret:      "appCredSecret",
		}
		credMarshaled, err := json.Marshal(openstackCredIn)
		if err != nil {
			panic(err)
		}
		credential := &service.CredentialModel{
			Session:           service.Session{},
			Username:          "test_user123",
			Value:             string(credMarshaled),
			Type:              "openstack",
			ID:                "openstack-cred-123",
			CreatedAt:         time.Time{},
			UpdatedAt:         time.Time{},
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}
		credMS.On("Get", actor, actor.Actor, "openstack-cred-123").Return(credential, nil)
		key := make([]byte, 32)
		_, err = io.ReadFull(rand.Reader, key)
		if err != nil {
			panic(err)
		}
		keySrc.On("GetKey").Return(key)

		credEncoded, err := encoder.EncodeOpenstackCred(actor, "openstack-cred-123")

		assert.NoError(t, err)
		assert.NotEmpty(t, credEncoded)
		cipher, err := base64.StdEncoding.DecodeString(credEncoded)
		assert.NoError(t, err)
		plaintext, err := AESDecrypt(key, cipher)
		assert.NoError(t, err)
		var openstackCredOut awmclient.OpenStackCredential
		err = json.Unmarshal(plaintext, &openstackCredOut)
		assert.NoError(t, err)
		assert.Equal(t, openstackCredIn, openstackCredOut)

		credMS.AssertExpectations(t)
		keySrc.AssertExpectations(t)
	})
	t.Run("credMS return error", func(t *testing.T) {
		credMS := &portsmock.CredentialMicroservice{}
		keySrc := &portsmock.KeySrc{}
		encoder := NewCredentialEncoder(credMS, keySrc)

		actor := types.Actor{
			Actor:    "test_user123",
			Emulator: "",
		}
		credMS.On("Get", actor, actor.Actor, "openstack-cred-123").Return(nil, service.NewCacaoNotFoundError("not found"))

		_, err := encoder.EncodeOpenstackCred(actor, "openstack-cred-123")

		assert.Error(t, err)
		credMS.AssertExpectations(t)
		keySrc.AssertExpectations(t)
	})
	t.Run("keySrc return bad key", func(t *testing.T) {
		credMS := &portsmock.CredentialMicroservice{}
		keySrc := &portsmock.KeySrc{}
		encoder := NewCredentialEncoder(credMS, keySrc)

		actor := types.Actor{
			Actor:    "test_user123",
			Emulator: "",
		}
		openstackCredIn := awmclient.OpenStackCredential{
			IdentityAPIVersion: "apiVersion",
			RegionName:         "regionName",
			Interface:          "interface",
			AuthURL:            "https://foobar.com",
			ProjectDomainID:    "projectDomainID",
			ProjectDomainName:  "projectDomainName",
			ProjectID:          "projectID",
			ProjectName:        "projectName",
			UserDomainName:     "userDomainName",
			Username:           "username",
			Password:           "password",
			AuthType:           "authType",
			AppCredID:          "appCredID",
			AppCredName:        "appCredName",
			AppCredSecret:      "appCredSecret",
		}
		credMarshaled, err := json.Marshal(openstackCredIn)
		if err != nil {
			panic(err)
		}
		credential := &service.CredentialModel{
			Session:           service.Session{},
			Username:          "test_user123",
			Value:             string(credMarshaled),
			Type:              "openstack",
			ID:                "openstack-cred-123",
			CreatedAt:         time.Time{},
			UpdatedAt:         time.Time{},
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}
		credMS.On("Get", actor, actor.Actor, "openstack-cred-123").Return(credential, nil)
		key := make([]byte, 4) // bad key length
		_, err = io.ReadFull(rand.Reader, key)
		if err != nil {
			panic(err)
		}
		keySrc.On("GetKey").Return(key)

		_, err = encoder.EncodeOpenstackCred(actor, "openstack-cred-123")

		assert.Error(t, err)
		credMS.AssertExpectations(t)
		keySrc.AssertExpectations(t)
	})
}
