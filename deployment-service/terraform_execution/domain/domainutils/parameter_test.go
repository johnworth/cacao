package domainutils

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"reflect"
	"testing"
)

// https://www.terraform.io/docs/language/expressions/types.html#types

func TestParamToAnsibleVars(t *testing.T) {
	type args struct {
		parameters []deploymentcommon.DeploymentParameter
	}
	tests := []struct {
		name    string
		args    args
		want    map[string]interface{}
		wantErr bool
	}{
		{
			name: "simple",
			args: args{
				parameters: []deploymentcommon.DeploymentParameter{
					{Name: "username", Type: "string", Value: "test_user123"},
				},
			},
			want: map[string]interface{}{
				"TF_MODULE": tfModuleName,
				"TF_INPUT_VARS": map[string]interface{}{
					"username": map[string]string{"type": "string"},
				},
				"TF_INPUT_VALUES": map[string]interface{}{
					"username": "test_user123",
				},
			},
			wantErr: false,
		},
		{
			name: "basic TF types",
			args: args{
				parameters: []deploymentcommon.DeploymentParameter{
					{Name: "username", Type: "string", Value: "test_user123"},
					{Name: "count", Type: "number", Value: 123},
					{Name: "flag", Type: "bool", Value: true},
				},
			},
			want: map[string]interface{}{
				"TF_MODULE": tfModuleName,
				"TF_INPUT_VARS": map[string]interface{}{
					"username": map[string]string{"type": "string"},
					"count":    map[string]string{"type": "number"},
					"flag":     map[string]string{"type": "bool"},
				},
				"TF_INPUT_VALUES": map[string]interface{}{
					"username": "test_user123",
					"count":    123,
					"flag":     true,
				},
			},
			wantErr: false,
		},
		{
			name: "more complex TF types",
			args: args{
				parameters: []deploymentcommon.DeploymentParameter{
					{Name: "list1", Type: "list", Value: []string{"foo", "bar"}},
					{Name: "map1", Type: "map", Value: map[string]interface{}{"foo": "bar"}},
				},
			},
			want: map[string]interface{}{
				"TF_MODULE": tfModuleName,
				"TF_INPUT_VARS": map[string]interface{}{
					"list1": map[string]string{"type": "list"},
					"map1":  map[string]string{"type": "map"},
				},
				"TF_INPUT_VALUES": map[string]interface{}{
					"list1": []string{"foo", "bar"},
					"map1":  map[string]interface{}{"foo": "bar"},
				},
			},
			wantErr: false,
		},
		{
			name: "cacao-specific types 1",
			args: args{
				parameters: []deploymentcommon.DeploymentParameter{
					{Name: "flavor", Type: "cacao_provider_flavor", Value: "tiny1"},
					{Name: "image", Type: "cacao_provider_image", Value: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"},
					{Name: "project", Type: templateProviderProjectType, Value: "my-project"},
					{Name: "count", Type: "integer", Value: 123},
				},
			},
			want: map[string]interface{}{
				"TF_MODULE": tfModuleName,
				"TF_INPUT_VARS": map[string]interface{}{
					"flavor":  map[string]string{"type": "string"},
					"image":   map[string]string{"type": "string"},
					"project": map[string]string{"type": "string"},
					"count":   map[string]string{"type": "number"},
				},
				"TF_INPUT_VALUES": map[string]interface{}{
					"flavor":  "tiny1",
					"image":   "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
					"project": "my-project",
					"count":   123,
				},
			},
			wantErr: false,
		},
		{
			name: "cacao-specific types 2",
			args: args{
				parameters: []deploymentcommon.DeploymentParameter{
					{Name: "key_pair", Type: TemplateProviderKeyPairType, Value: "my-key-pair"},
					{Name: "ext_net", Type: TemplateProviderExternalNetworkType, Value: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"},
					{Name: "ext_subnet", Type: TemplateProviderExternalSubnetType, Value: "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"},
					{Name: "cloud_init", Type: TemplateCloudInitType, Value: "#cloud-init\nusers:\n  - default\n"},
				},
			},
			want: map[string]interface{}{
				"TF_MODULE": tfModuleName,
				"TF_INPUT_VARS": map[string]interface{}{
					"key_pair":   map[string]string{"type": "string"},
					"ext_net":    map[string]string{"type": "string"},
					"ext_subnet": map[string]string{"type": "string"},
					"cloud_init": map[string]string{"type": "string"},
				},
				"TF_INPUT_VALUES": map[string]interface{}{
					"key_pair":   "my-key-pair",
					"ext_net":    "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
					"ext_subnet": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
					"cloud_init": "#cloud-init\nusers:\n  - default\n",
				},
			},
			wantErr: false,
		},
		{
			name: "unknown types",
			args: args{
				parameters: []deploymentcommon.DeploymentParameter{
					{Name: "foo", Type: "bar", Value: "foobar"},
				},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParamToAnsibleVars(tt.args.parameters)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParamToAnsibleVars() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParamToAnsibleVars() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParameterConverter_Convert(t *testing.T) {
	type args struct {
		templateParam map[string]interface{}
		paramValues   service.DeploymentParameterValues
	}
	tests := []struct {
		name    string
		args    args
		want    []deploymentcommon.DeploymentParameter
		wantErr bool
	}{
		{
			name: "simple",
			args: args{
				templateParam: map[string]interface{}{
					"username": map[string]interface{}{
						"type": "string",
					},
				},
				paramValues: service.DeploymentParameterValues{
					"username": "test_user123",
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "username",
					Type:  "string",
					Value: "test_user123",
				},
			},
			wantErr: false,
		},
		{
			name: "more types",
			args: args{
				templateParam: map[string]interface{}{
					"instance_name":  map[string]interface{}{"type": "string"},
					"instance_count": map[string]interface{}{"type": "integer"},
					"flavor":         map[string]interface{}{"type": "cacao_provider_flavor"},
					"keypair":        map[string]interface{}{"type": "string"},
					"image":          map[string]interface{}{"type": "cacao_provider_image"},
				},
				paramValues: service.DeploymentParameterValues{
					"instance_name":  "foobar",
					"instance_count": 123,
					"flavor":         "tiny1",
					"keypair":        "myKeyPair",
					"image":          "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "instance_name",
					Type:  "string",
					Value: "foobar",
				},
				{
					Name:  "instance_count",
					Type:  "integer",
					Value: 123,
				},
				{
					Name:  "flavor",
					Type:  "cacao_provider_flavor",
					Value: "tiny1",
				},
				{
					Name:  "keypair",
					Type:  "string",
					Value: "myKeyPair",
				},
				{
					Name:  "image",
					Type:  "cacao_provider_image",
					Value: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
				},
			},
			wantErr: false,
		},
		{
			name: "bad string value",
			args: args{
				templateParam: map[string]interface{}{
					"instance_name": map[string]interface{}{"type": "string"},
				},
				paramValues: service.DeploymentParameterValues{
					"instance_name": 123,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "bad int value",
			args: args{
				templateParam: map[string]interface{}{
					"instance_count": map[string]interface{}{"type": "integer"},
				},
				paramValues: service.DeploymentParameterValues{
					"instance_name": "foo",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "bad float value",
			args: args{
				templateParam: map[string]interface{}{
					"decimal_val": map[string]interface{}{"type": "float"},
				},
				paramValues: service.DeploymentParameterValues{
					"decimal_val": "foo",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "default value",
			args: args{
				templateParam: map[string]interface{}{
					"count": map[string]interface{}{"default": 123, "type": "integer"},
				},
				paramValues: service.DeploymentParameterValues{},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "count",
					Type:  "integer",
					Value: 123,
				},
			},
			wantErr: false,
		},
		{
			name: "default value wrong type",
			args: args{
				templateParam: map[string]interface{}{
					"count": map[string]interface{}{"default": "bar", "type": "integer"},
				},
				paramValues: service.DeploymentParameterValues{},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "enum",
			args: args{
				templateParam: map[string]interface{}{
					"power_state": map[string]interface{}{
						"type":    "string",
						"default": "active",
						"enum":    []string{"active", "shutoff", "suspend"},
					},
				},
				paramValues: service.DeploymentParameterValues{
					"power_state": "suspend",
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "power_state",
					Type:  "string",
					Value: "suspend",
				},
			},
			wantErr: false,
		},
		{
			name: "enum bad value",
			args: args{
				templateParam: map[string]interface{}{
					"power_state": map[string]interface{}{
						"type":    "string",
						"default": "active",
						"enum":    []string{"active", "shutoff", "suspend"},
					},
				},
				paramValues: service.DeploymentParameterValues{
					"power_state": "foo",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "enum value bad type",
			args: args{
				templateParam: map[string]interface{}{
					"power_state": map[string]interface{}{
						"type":    "string",
						"default": "active",
						"enum":    []int{123, 456, 789},
					},
				},
				paramValues: service.DeploymentParameterValues{
					"power_state": "suspend",
				},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pc := ParameterConverter{}
			got, err := pc.Convert(tt.args.templateParam, tt.args.paramValues)
			if tt.wantErr {
				assert.Error(t, err)
				return
			}
			assert.NoError(t, err)
			if len(got) == 1 {
				// assert.Equal cares about list order, so only used on len==1
				assert.Equal(t, tt.want, got)
				return
			}
			assert.Len(t, got, len(tt.want))
			for _, elem := range got {
				assert.Contains(t, tt.want, elem)
			}
			for _, elem := range tt.want {
				assert.Contains(t, got, elem)
			}
		})
	}
}
