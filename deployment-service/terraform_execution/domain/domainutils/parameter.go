package domainutils

import (
	"errors"
	"fmt"
	"math"
	"reflect"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
)

const (
	templateStringType = "string"
	// TemplateUsernameType is CACAO username.
	// This constant is exported because parameter of this type can be overridden, thus can be used by other packages.
	TemplateUsernameType        = "cacao_username"
	templateProviderFlavorType  = "cacao_provider_flavor"
	templateProviderImageType   = "cacao_provider_image"
	templateProviderProjectType = "cacao_provider_project"
	// TemplateProviderKeyPairType is name of the ssh key pair used by provider.
	// This constant is exported because parameter of this type can be overridden, thus can be used by other packages.
	TemplateProviderKeyPairType = "cacao_provider_key_pair"
	// TemplateProviderExternalNetworkType is name of external network.
	// This constant is exported because parameter of this type can be overridden, thus can be used by other packages.
	TemplateProviderExternalNetworkType = "cacao_provider_external_network"
	// TemplateProviderExternalSubnetType is uuid of subnet of external network.
	// This constant is exported because parameter of this type can be overridden, thus can be used by other packages.
	TemplateProviderExternalSubnetType = "cacao_provider_external_subnet"
	// TemplateCloudInitType is cloud-init script to be passed to instance.
	// This constant is exported because parameter of this type can be overridden, thus can be used by other packages.
	TemplateCloudInitType = "cacao_cloud_init"
	templateNumberType    = "number"
	templateIntegerType   = "integer"
	templateFloatType     = "float"
	templateBoolType      = "bool"
	templateListType      = "list"
	templateMapType       = "map"
)

// https://www.terraform.io/docs/language/expressions/types.html#types
const (
	tfStringType = "string"
	tfNumberType = "number"
	tfBoolType   = "bool"
	tfListType   = "list"
	tfTupleType  = "tuple"
	tfMapType    = "map"
	tfObjectType = "object"
)
const tfModuleName = "cacao_module"

// ParamConstraint is constraints on a parameter
type ParamConstraint struct {
	Type    string        `mapstructure:"type"`
	Default interface{}   `mapstructure:"default"`
	Enum    []interface{} `mapstructure:"enum"`
}

// ParameterConverter converts external representation of deployment parameter and
// template parameter definition to internal parameter representation.
type ParameterConverter struct {
	option parameterConverterOption
}

type parameterConverterOption struct {
	// override parameter value for certain parameter types (as defined in the template param def), map[paramType]paramValue
	paramOverride map[string]interface{}
}

// ParameterConverterOption option function for converting parameter
type ParameterConverterOption func(option *parameterConverterOption)

// OverrideValueForTypes override parameter value for certain parameter types (as defined in the template param def), map[paramType]paramValue
func OverrideValueForTypes(typeValueMap map[string]interface{}) ParameterConverterOption {
	return func(option *parameterConverterOption) {
		option.paramOverride = typeValueMap
	}
}

// Convert converts external representation of deployment parameter and
// template parameter definition to internal parameter representation.
func (pc *ParameterConverter) Convert(
	templateParam map[string]interface{},
	paramValues service.DeploymentParameterValues,
	opts ...ParameterConverterOption) ([]deploymentcommon.DeploymentParameter, error) {

	var allResultParam = make([]deploymentcommon.DeploymentParameter, 0, len(templateParam))

	pc.option = pc.aggregateOptions(opts...)

	for paramName, paramConstraintRaw := range templateParam {
		var constraint ParamConstraint
		err := mapstructure.Decode(paramConstraintRaw, &constraint)
		if err != nil {
			return nil, err
		}
		paramVal, ok := paramValues[paramName]
		if !ok {
			paramVal = nil
		}
		resultParam, err := pc.parseParamValue(constraint, paramName, paramVal)
		if err != nil {
			return nil, err
		}
		allResultParam = append(allResultParam, resultParam)
	}
	return allResultParam, nil
}

// combine all option functions
func (pc ParameterConverter) aggregateOptions(opts ...ParameterConverterOption) parameterConverterOption {
	var option parameterConverterOption
	if len(opts) == 0 {
		return option
	}
	for _, opt := range opts {
		opt(&option)
	}
	return option
}

// parse a single parameter value
func (pc ParameterConverter) parseParamValue(constraint ParamConstraint, paramName string, paramValue interface{}) (deploymentcommon.DeploymentParameter, error) {
	paramValue, _ = pc.overrideParamValue(constraint, paramValue)

	result, err := pc.applyDefault(constraint, paramName, paramValue)
	if err != nil {
		return result, err
	}
	err = pc.paramTypeMatching(constraint, paramName, result.Value)
	if err != nil {
		return result, err
	}
	err = pc.enumMatching(constraint, paramName, result.Value)
	if err != nil {
		return result, err
	}

	result.Name = paramName
	result.Type = constraint.Type
	return result, nil
}

func (pc ParameterConverter) applyDefault(constraint ParamConstraint, paramName string, paramValue interface{}) (deploymentcommon.DeploymentParameter, error) {
	var result deploymentcommon.DeploymentParameter
	if paramValue == nil {
		if constraint.Default == nil {
			return result, fmt.Errorf("missing required field, %s", paramName)
		}
		result.Value = constraint.Default
	} else {
		result.Value = paramValue
	}
	return result, nil
}

// override the parameter value based on the option.
// return the resulting parameter value, and a flag indicating whether the value is overridden (true=overridden).
func (pc ParameterConverter) overrideParamValue(constraint ParamConstraint, paramValue interface{}) (interface{}, bool) {
	if pc.option.paramOverride == nil || len(pc.option.paramOverride) == 0 {
		return paramValue, false
	}
	switch constraint.Type {
	case TemplateUsernameType:
		username, ok := pc.option.paramOverride[TemplateUsernameType]
		if ok {
			return username, true
		}
	case TemplateProviderKeyPairType:
		keyPairName, ok := pc.option.paramOverride[TemplateProviderKeyPairType]
		if ok {
			return keyPairName, true
		}
	case TemplateProviderExternalNetworkType:
		extNetwork, ok := pc.option.paramOverride[TemplateProviderExternalNetworkType]
		if ok {
			return extNetwork, true
		}
	case TemplateProviderExternalSubnetType:
		subnetUUID, ok := pc.option.paramOverride[TemplateProviderExternalSubnetType]
		if ok {
			return subnetUUID, true
		}
	case TemplateCloudInitType:
		cloudInitScript, ok := pc.option.paramOverride[TemplateCloudInitType]
		if ok {
			return cloudInitScript, true
		}
	}
	return paramValue, false
}

// check if the parameter value satisfy the type constraint
func (pc ParameterConverter) paramTypeMatching(constraint ParamConstraint, paramName string, paramValue interface{}) error {
	// TODO handle enum
	switch value := paramValue.(type) {
	case string:
		if !pc.stringlyParamType(constraint.Type) {
			return fmt.Errorf("param %s does not take string value", paramName)
		}
	case bool:
		if constraint.Type != templateBoolType {
			return fmt.Errorf("param %s does not take bool value", paramName)
		}
	case int:
		if constraint.Type != templateIntegerType {
			return fmt.Errorf("param %s does not take int value", paramName)
		}
	case float32:
		if float32IsInteger(value) {
			if constraint.Type != templateFloatType && constraint.Type != templateIntegerType {
				return fmt.Errorf("param %s does not take float value", paramName)
			}
		} else {
			if constraint.Type != templateFloatType {
				return fmt.Errorf("param %s does not take float value", paramName)
			}
		}
	case float64:
		if float64IsInteger(value) {
			if constraint.Type != templateFloatType && constraint.Type != templateIntegerType {
				return fmt.Errorf("param %s does not take float value", paramName)
			}
		} else {
			if constraint.Type != templateFloatType {
				return fmt.Errorf("param %s does not take float value", paramName)
			}
		}
	// TODO handles more complex type, e.g. list, map
	default:
		return errors.New("unknown type, " + reflect.TypeOf(paramValue).String())
	}
	return nil
}

// enumMatching checks if the parameter value is one of the allowed values listed in enum
func (pc ParameterConverter) enumMatching(constraint ParamConstraint, paramName string, paramValue interface{}) error {
	if constraint.Enum == nil {
		return nil
	}
	for _, val := range constraint.Enum {
		if paramValue == val {
			return nil
		}
	}
	return errors.New("param value %s not in enum")
}

// check if a type name is a stringly type (types that based on string, takes in string value)
func (pc ParameterConverter) stringlyParamType(typeName string) bool {
	switch typeName {
	case templateStringType:
	case TemplateUsernameType:
	case templateProviderFlavorType:
	case templateProviderImageType:
	case templateProviderProjectType:
	case TemplateProviderKeyPairType:
	case TemplateProviderExternalNetworkType:
	case TemplateProviderExternalSubnetType:
	case TemplateCloudInitType:
	default:
		return false
	}
	return true
}

// EmptyParameters populate with empty (nil) as the value for parameters
func (pc ParameterConverter) EmptyParameters(
	templateParam map[string]interface{}) ([]deploymentcommon.DeploymentParameter, error) {

	var allResultParam = make([]deploymentcommon.DeploymentParameter, 0, len(templateParam))

	for paramName, paramConstraintRaw := range templateParam {
		var constraint ParamConstraint
		err := mapstructure.Decode(paramConstraintRaw, &constraint)
		if err != nil {
			return nil, err
		}
		var resultParam = deploymentcommon.DeploymentParameter{
			Name:  paramName,
			Type:  constraint.Type,
			Value: nil,
		}
		allResultParam = append(allResultParam, resultParam)
	}
	return allResultParam, nil
}

// ParamToAnsibleVars convert parameter to ansible variables
func ParamToAnsibleVars(parameters []deploymentcommon.DeploymentParameter) (map[string]interface{}, error) {
	var ansibleVars = make(map[string]interface{})
	ansibleVars["TF_MODULE"] = tfModuleName
	inputValues := map[string]interface{}{}
	inputVars := map[string]interface{}{}
	for _, param := range parameters {
		tfType, err := convertTemplateParamTypeToTerraformType(param.Type)
		if err != nil {
			return nil, err
		}
		inputVars[param.Name] = map[string]string{
			"type": tfType,
		}
		inputValues[param.Name] = param.Value
	}
	ansibleVars["TF_INPUT_VALUES"] = inputValues
	ansibleVars["TF_INPUT_VARS"] = inputVars
	return ansibleVars, nil
}

func convertTemplateParamTypeToTerraformType(paramType string) (string, error) {
	switch paramType {
	case templateStringType:
		return tfStringType, nil
	case templateNumberType:
		return tfNumberType, nil
	case templateBoolType:
		return tfBoolType, nil
	case templateListType:
		return tfListType, nil
	case templateMapType:
		return tfMapType, nil
	case TemplateUsernameType:
		return tfStringType, nil
	case templateProviderFlavorType:
		return tfStringType, nil
	case templateProviderImageType:
		return tfStringType, nil
	case templateProviderProjectType:
		return tfStringType, nil
	case TemplateProviderKeyPairType:
		return tfStringType, nil
	case TemplateProviderExternalNetworkType:
		return tfStringType, nil
	case TemplateProviderExternalSubnetType:
		return tfStringType, nil
	case TemplateCloudInitType:
		return tfStringType, nil
	case templateIntegerType:
		return tfNumberType, nil
	default:
		return "", fmt.Errorf("unknown param type, %s", paramType)
	}
}

func float64IsInteger(val float64) bool {
	return val == math.Trunc(val)
}

func float32IsInteger(val float32) bool {
	return float64(val) == math.Trunc(float64(val))
}

// TemplateParameterDefHasType checks if there is a parameter that has the specified type in the template parameter definition.
func TemplateParameterDefHasType(templateParamDef map[string]interface{}, paramType string) bool {
	for _, paramConstraintRaw := range templateParamDef {
		var constraint ParamConstraint
		err := mapstructure.Decode(paramConstraintRaw, &constraint)
		if err != nil {
			return false
		}
		if constraint.Type == paramType {
			return true
		}
	}
	return false
}

// KeyPairName generates the keypair name based on trimmed username.
func KeyPairName(username string) string {
	return "keypair-" + TrimUsername(username)
}
