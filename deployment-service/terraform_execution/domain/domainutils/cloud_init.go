package domainutils

import (
	"errors"
	"fmt"
	yaml "gopkg.in/yaml.v2"
	"strings"
	"text/template"
)

// new user is granted sudo privilege, all ssh keys are added to each new user
const cloudInitTemplate = `#cloud-config
users:
- default
{{$keys := .SSHKeys}}
{{range .Users}}- name: {{ . }}
  sudo: ALL=(ALL) NOPASSWD:ALL
{{if $keys}}
  ssh_authorized_keys:
{{end}}
{{range $keys}}  - {{ . }}
{{end}}
{{end}}

`

// OpenStackUserDataLengthLimit is a user data length limit for OpenStack.
// The limit is for data after encoding is 65535 bytes. For not-encoded data that is ~49151 bytes.
// https://docs.openstack.org/nova/latest/user/metadata.html#user-data
const OpenStackUserDataLengthLimit = 49151

// AWSEC2UserDataLengthLimit is user data length limit for AWS EC2.
// The limit of 16K is before encoding.
// https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instancedata-add-user-data.html
const AWSEC2UserDataLengthLimit = 16 << 10

// CloudInitGenerator is a generator for cloud-init scripts.
type CloudInitGenerator struct {
	users       []string
	sshKeys     []string
	lengthLimit int
}

// magic number to set the initial cap for strings.Builder, it is roughly the length of the script that has 1 new user and a few ssh keys.
// The goal is to reduce number of memory allocations by pre-allocate.
const cloudInitStringBuilderMagicNumber = 3000

// NewCloudInitGenerator creates a new CloudInitGenerator. Use default length limit if lengthLimit <= 0.
func NewCloudInitGenerator(lengthLimit int) CloudInitGenerator {
	if lengthLimit <= 0 {
		lengthLimit = OpenStackUserDataLengthLimit
	}
	return CloudInitGenerator{
		users:       make([]string, 0),
		sshKeys:     make([]string, 0),
		lengthLimit: lengthLimit,
	}
}

// CreateUser creates a sudo-capable user. Username will be trimmed.
func (gen *CloudInitGenerator) CreateUser(username string) {
	gen.users = append(gen.users, TrimUsername(username))
}

// AddSSHKey adds ssh key to all created users.
func (gen *CloudInitGenerator) AddSSHKey(publicKey string) {
	if len(publicKey) == 0 {
		return
	}
	gen.sshKeys = append(gen.sshKeys, publicKey)
}

// AddSSHKeys adds multiple ssh keys.
func (gen *CloudInitGenerator) AddSSHKeys(publicKeys []string) {
	gen.sshKeys = append(gen.sshKeys, publicKeys...)
}

// GenerateWithTemplate ...
func (gen CloudInitGenerator) GenerateWithTemplate() (string, error) {
	template, err := template.New("cloud-init").Parse(cloudInitTemplate)
	if err != nil {
		return "", err
	}
	var output strings.Builder
	output.Grow(cloudInitStringBuilderMagicNumber)
	var data = struct {
		Users   []string
		SSHKeys []string
	}{gen.users, gen.sshKeys}
	err = template.Execute(&output, data)
	if err != nil {
		return "", err
	}
	if output.Len() > gen.lengthLimit {
		return output.String(), errors.New("user data length exceeds limit")
	}
	return output.String(), nil
}

// Generate a faster implementation using strings.Builder
func (gen CloudInitGenerator) Generate() (string, error) {
	var output strings.Builder
	output.Grow(cloudInitStringBuilderMagicNumber)
	output.WriteString("#cloud-config\nusers:\n- default\n")
	for _, user := range gen.users {
		output.WriteString("- name: ")
		output.WriteString(user)
		output.WriteRune('\n')
		output.WriteString("  sudo: ALL=(ALL) NOPASSWD:ALL\n")
		if len(gen.sshKeys) > 0 {
			output.WriteString("  ssh_authorized_keys:\n")
		}
		for _, key := range gen.sshKeys {
			output.WriteString("  - ")
			output.WriteString(key)
			output.WriteRune('\n')
		}
	}
	output.WriteRune('\n')
	if output.Len() > gen.lengthLimit {
		return output.String(), errors.New("user data length exceeds limit")
	}
	return output.String(), nil
}

// GenerateWithYAML ...
func (gen CloudInitGenerator) GenerateWithYAML() (string, error) {
	var output strings.Builder
	output.Grow(cloudInitStringBuilderMagicNumber)

	useList := []interface{}{
		"default",
	}
	for _, user := range gen.users {
		useList = append(useList, map[string]interface{}{
			"name":                user,
			"sudo":                "ALL=(ALL) NOPASSWD:ALL",
			"ssh_authorized_keys": gen.sshKeys,
		})
	}
	cloudInit := map[string]interface{}{
		"users": useList,
	}

	output.WriteString("#cloud-config\n")

	encoder := yaml.NewEncoder(&output)
	err := encoder.Encode(cloudInit)
	if err != nil {
		return "", err
	}
	if output.Len() > gen.lengthLimit {
		return output.String(), errors.New("user data length exceeds limit")
	}
	return output.String(), nil
}

// TrimUsername trims cacao username to be used as linux username.
// If nothing is left after trimming, "cacao" is returned as username.
// Goes up to 32 characters long.
func TrimUsername(cacaoUsername string) string {
	var i int
	var c int32
	notAlphaNumerical := func(c int32) bool {
		return !((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9'))
	}
	for i, c = range cacaoUsername {
		if i == 0 && notAlphaNumerical(c) {
			// must start with alphanumerical character
			fmt.Printf("1st char is bad, %c\n", c)
			break
		}
		if notAlphaNumerical(c) && c != '-' && c != '_' && c != '.' {
			// stop at 1st prohibited character
			fmt.Printf("bad char: %d\n", i)
			break
		}
	}
	fmt.Println(i)
	if i == 0 {
		// start with non-alphanumerical
		return "cacao"
	}
	if i > 31 {
		// up to 32 character long
		i = 31
	}
	if i == len(cacaoUsername)-1 {
		return cacaoUsername
	}
	return cacaoUsername[:i]
}
