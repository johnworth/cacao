package domainutils

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// TerraformWorkflowBuilder is a builder for Terraform workflow
type TerraformWorkflowBuilder struct {
	awm    ports.ArgoWorkflowMediator
	credMS ports.CredentialMicroservice
	keySrc ports.KeySrc

	actor types.Actor
	// if empty use actor as username.
	// actor does not always equal to username.
	username         string
	deployment       deploymentcommon.Deployment
	templateSnapshot deploymentcommon.TemplateSnapshot
	param            []deploymentcommon.DeploymentParameter
	cloudCredID      deploymentcommon.CredentialID
	cloudCredBase64  string
	gitCredID        deploymentcommon.CredentialID
	gitCredBase64    string
	tfOperation      string
	tfStateKey       string
}

// NewTerraformWorkflowBuilder ...
func NewTerraformWorkflowBuilder(awm ports.ArgoWorkflowMediator, credMS ports.CredentialMicroservice, keySrc ports.KeySrc) TerraformWorkflowBuilder {
	return TerraformWorkflowBuilder{awm: awm, credMS: credMS, keySrc: keySrc}
}

// SetActor ...
func (builder *TerraformWorkflowBuilder) SetActor(actor types.Actor) {
	builder.actor = actor
}

// SetUsername ...
func (builder *TerraformWorkflowBuilder) SetUsername(user string) {
	builder.username = user
}

// SetDeployment ...
func (builder *TerraformWorkflowBuilder) SetDeployment(deployment deploymentcommon.Deployment) {
	builder.deployment = deployment
}

// SetCloudCredential ...
func (builder *TerraformWorkflowBuilder) SetCloudCredential(credID deploymentcommon.CredentialID, credBase64 string) {
	builder.cloudCredID = credID
	builder.cloudCredBase64 = credBase64
}

// SetGitCredential ...
func (builder *TerraformWorkflowBuilder) SetGitCredential(credID deploymentcommon.CredentialID, credBase64 string) {
	builder.gitCredID = credID
	builder.gitCredBase64 = credBase64
}

// SetTemplateSnapshot ...
// TODO provide other ways of setting this, e.g. fetching from template MS, getting from deployment last run (if present).
func (builder *TerraformWorkflowBuilder) SetTemplateSnapshot(template deploymentcommon.TemplateSnapshot) {
	builder.templateSnapshot = template
}

// SetParameter ...
func (builder *TerraformWorkflowBuilder) SetParameter(param []deploymentcommon.DeploymentParameter) {
	builder.param = param
}

// SetTerraformOperation ...
func (builder *TerraformWorkflowBuilder) SetTerraformOperation(tfOperation string) {
	builder.tfOperation = tfOperation
}

// SetTerraformStateKey ...
func (builder *TerraformWorkflowBuilder) SetTerraformStateKey(tfStateKey string) {
	builder.tfStateKey = tfStateKey
}

// CreateWorkflow returns provider ID and workflow name
func (builder *TerraformWorkflowBuilder) CreateWorkflow() (common.ID, string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "domainutils",
		"function": "TerraformWorkflowBuilder.CreateWorkflow",
	})
	err := builder.validatePrerequisite()
	if err != nil {
		return "", "", err
	}

	ansibleVars, err := ParamToAnsibleVars(builder.param)
	if err != nil {
		return "", "", err
	}

	if builder.tfOperation != "" {
		ansibleVars["TF_OPERATION"] = builder.tfOperation
	}
	if builder.tfStateKey == "" {
		// use deployment ID as key if not set
		builder.tfStateKey = builder.deployment.ID.String()
	}
	if builder.username == "" {
		builder.username = builder.actor.Actor
	}
	if builder.cloudCredID == "" {
		err = builder.fetchCloudCred()
		if err != nil {
		}
	}
	if builder.gitCredID == "" {
		err = builder.fetchGitCred()
		if err != nil {
			return "", "", err
		}
	}

	// compose a request to AWM
	tfWfData := awmclient.TerraformWorkflowData{
		Username:          builder.username,
		Deployment:        builder.deployment.ID,
		TemplateID:        builder.deployment.Template,
		TerraformStateKey: builder.tfStateKey,
		GitURL:            builder.templateSnapshot.GitURL,
		GitTrackedUpStream: struct {
			Branch string `mapstructure:"branch"`
			Tag    string `mapstructure:"tag"`
			Commit string `mapstructure:"commit"`
		}{
			Branch: builder.templateSnapshot.UpstreamTracked.Branch,
			Tag:    builder.templateSnapshot.UpstreamTracked.Tag,
			Commit: builder.templateSnapshot.CommitHash,
		},
		Path:                  builder.templateSnapshot.SubPath,
		CloudCredID:           builder.cloudCredID.String(),
		AnsibleVars:           ansibleVars,
		CloudCredentialBase64: builder.cloudCredBase64,
		GitCredID:             builder.gitCredID.String(),
		GitCredentialBase64:   builder.gitCredBase64,
	}

	awmProvider := builder.deployment.PrimaryCloudProvider
	wfName, err := builder.awm.Create(
		context.TODO(),
		awmProvider,
		builder.username,
		awmclient.TerraformWorkflowFilename,
		awmclient.AWMTerraformWorflowData(tfWfData),
	)
	if err != nil {
		logger.WithError(err).Error("fail to create workflow via AWM")
		return "", "", err
	}
	return awmProvider, wfName, nil
}

func (builder *TerraformWorkflowBuilder) validatePrerequisite() error {
	if builder.actor.Actor == "" {
		return service.NewCacaoInvalidParameterError("actor not set, cannot build tf workflow")
	}
	if builder.deployment.ID == "" {
		return service.NewCacaoInvalidParameterError("deployment not set, cannot build tf workflow")
	}
	if builder.templateSnapshot.TemplateID == "" {
		return service.NewCacaoInvalidParameterError("template snapshot not set, cannot build tf workflow")
	}
	if len(builder.param) == 0 {
		return service.NewCacaoInvalidParameterError("param not set, cannot build tf workflow")
	}
	switch builder.tfOperation {
	default:
		errorMessage := fmt.Sprintf("unknown terraform operation, %s", builder.tfOperation)
		return service.NewCacaoGeneralError(errorMessage)
	case "apply":
	case "destroy":
	case "": // no operation specified
	}
	return nil
}

func (builder *TerraformWorkflowBuilder) fetchCloudCred() error {
	credID, ok := builder.deployment.FindCloudCredential(builder.deployment.PrimaryCloudProvider)
	if !ok {
		return fmt.Errorf("no credential for provider %s", builder.deployment.PrimaryCloudProvider)
	}
	credEncoder := NewCredentialEncoder(builder.credMS, builder.keySrc)
	credBase64, err := credEncoder.EncodeOpenstackCred(builder.actor, credID.String())
	if err != nil {
		return err
	}
	builder.cloudCredID = credID
	builder.cloudCredBase64 = credBase64
	return nil
}

func (builder *TerraformWorkflowBuilder) fetchGitCred() error {
	credEncoder := NewCredentialEncoder(builder.credMS, builder.keySrc)
	if builder.deployment.GitCredential != "" {
		credBase64, err := credEncoder.EncodeGitCred(builder.actor, builder.deployment.GitCredential.String())
		if err != nil {
			return err
		}
		builder.gitCredID = builder.deployment.GitCredential
		builder.gitCredBase64 = credBase64
		return nil
	}
	return nil
}
