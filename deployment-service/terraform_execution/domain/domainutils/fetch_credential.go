package domainutils

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// CredentialEncoder encrypt credential and then encodes it in base64
type CredentialEncoder struct {
	credMS ports.CredentialMicroservice
	keySrc ports.KeySrc
}

// NewCredentialEncoder ...
func NewCredentialEncoder(credMS ports.CredentialMicroservice, keySrc ports.KeySrc) *CredentialEncoder {
	return &CredentialEncoder{credMS: credMS, keySrc: keySrc}
}

// EncodeGitCred encodes git credential, also convert from the template source cred format to the format accepted by AWM.
func (encoder CredentialEncoder) EncodeGitCred(actor types.Actor, credID string) (string, error) {
	cred, err := encoder.credMS.Get(actor, actor.Actor, credID)
	if err != nil {
		return "", err
	}
	if !encoder.validateOpenstackCred(cred) {
		return "", fmt.Errorf("credential %s is not valid git credential", credID)
	}
	templateSrcCred, err := encoder.unmarshalTemplateSrcCred(cred)
	if err != nil {
	}
	gitCredMarshaled, err := encoder.convertGitCredAndMarshal(templateSrcCred)
	if err != nil {
		return "", err
	}
	cipher, err := encoder.encrypt(gitCredMarshaled)
	if err != nil {
		return "", fmt.Errorf("fail to encrypt credential %s", credID)
	}
	return encoder.base64Encode(cipher), nil
}

func (encoder CredentialEncoder) unmarshalTemplateSrcCred(cred service.Credential) (service.TemplateSourceCredential, error) {
	// unmarshal to check schema
	var credOut service.TemplateSourceCredential
	err := json.Unmarshal([]byte(cred.GetValue()), &credOut)
	if err != nil {
		return service.TemplateSourceCredential{}, fmt.Errorf("credential %s is not valid template source credential", cred.GetID())
	}
	return credOut, nil
}

func (encoder CredentialEncoder) convertGitCredAndMarshal(cred service.TemplateSourceCredential) ([]byte, error) {
	var credOut = awmclient.GitCredential{
		Username: cred.Username,
		Password: cred.Password,
	}
	return json.Marshal(credOut)
}

// EncodeOpenstackCred encodes OpenStack credential.
func (encoder CredentialEncoder) EncodeOpenstackCred(actor types.Actor, credID string) (string, error) {
	cred, err := encoder.credMS.Get(actor, actor.Actor, credID)
	if err != nil {
		return "", err
	}
	if !encoder.validateOpenstackCred(cred) {
		return "", fmt.Errorf("credential %s is not valid openstack credential", credID)
	}
	cipher, err := encoder.encrypt([]byte(cred.GetValue()))
	if err != nil {
		return "", fmt.Errorf("fail to encrypt credential %s", credID)
	}
	return encoder.base64Encode(cipher), nil
}

func (encoder CredentialEncoder) validateOpenstackCred(cred service.Credential) bool {
	// unmarshal to struct to check schema
	var openstackCred awmclient.OpenStackCredential
	err := json.Unmarshal([]byte(cred.GetValue()), &openstackCred)
	if err != nil {
		return false
	}
	return true
}

func (encoder CredentialEncoder) encrypt(input []byte) ([]byte, error) {
	encryptionKey := encoder.keySrc.GetKey()
	cipher, err := AESEncrypt(encryptionKey, input)
	if err != nil {
		return nil, err
	}
	return cipher, nil
}

func (encoder CredentialEncoder) base64Encode(input []byte) string {
	return base64.StdEncoding.EncodeToString(input)
}
