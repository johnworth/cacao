package domainutils

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"sigs.k8s.io/yaml"
	"testing"
)

var users = []string{
	"foobarrr",
	"rtetqertyu123456iw",
	"fUDksghaSUk13A6wi372reu9kr4wuA",
}
var sshKeys = []string{
	"ssh-ed25519 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA123/aaaaaaaaaaaaaaaa user123@instance123",
	"ssh-ed25519 BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB123/bbbbbbbbbbbbbbbb user123@instance123",
	"ssh-ed25519 CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC123/cccccccccccccccc user123@instance123",
	"ssh-ed25519 DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD123/dddddddddddddddd user123@instance123",
	"ssh-ed25519 EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE123/eeeeeeeeeeeeeeee user123@instance123",
	"ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA123/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/aaaaaaaaaaaaaaaaaaa/aaaaaa+aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/aaaaaa user123@instance123",
	"ssh-rsa BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB123/bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb/bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb/bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb/bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb/bbbbbbbbbbbbbbbbbbb/bbbbbb+bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb/bbbbbb user123@instance123",
	"ssh-rsa CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC123/cccccccccccccccccccccccccccccccccccccccccccccccccccccc/cccccccccccccccccccccccccccccccccccccccccccccccc/cccccccccccccccccccccccccccccccccccc/ccccccccccccccccccccccccccccccccccccccccccccccc/ccccccccccccccccccc/cccccc+cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc/cccccc user123@instance123",
	"ssh-rsa DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD123/dddddddddddddddddddddddddddddddddddddddddddddddddddddd/dddddddddddddddddddddddddddddddddddddddddddddddd/dddddddddddddddddddddddddddddddddddd/ddddddddddddddddddddddddddddddddddddddddddddddd/ddddddddddddddddddd/dddddd+dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd/dddddd user123@instance123",
	"ssh-rsa EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE123/eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee/eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee/eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee/ddddddddddddddddddddddddddddddddddddddddddddddd/eeeeeeeeeeeeeeeeeee/eeeeee+eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee/eeeeee user123@instance123",
}

func BenchmarkCloudInitGenerator_GenerateWithTemplate(b *testing.B) {
	var allResults = make([]string, 0, b.N)
	for i := 0; i < b.N; i++ {
		var generator = CloudInitGenerator{
			users:   users[:1], // 1 new user
			sshKeys: sshKeys,
		}
		result, _ := generator.GenerateWithTemplate()
		allResults = append(allResults, result)
	}
}

func BenchmarkCloudInitGenerator_Generate(b *testing.B) {
	var allResults = make([]string, 0, b.N)
	for i := 0; i < b.N; i++ {
		var generator = CloudInitGenerator{
			users:   users[:1], // 1 new user
			sshKeys: sshKeys,
		}
		result, _ := generator.Generate()
		allResults = append(allResults, result)
	}
}

func BenchmarkCloudInitGenerator_GenerateWithYAML(b *testing.B) {
	var allResults = make([]string, 0, b.N)
	for i := 0; i < b.N; i++ {
		var generator = CloudInitGenerator{
			users:   users[:1], // 1 new user
			sshKeys: sshKeys,
		}
		result, _ := generator.GenerateWithYAML()
		allResults = append(allResults, result)
	}
}

func TestGrow(t *testing.T) {
	var output bytes.Buffer
	output.Grow(2500)
	assert.Equal(t, 2500, output.Cap())
}

func TestCloudInitGenerator_Compare3GenerateImplementations(t *testing.T) {
	var generator = NewCloudInitGenerator(0)
	generator.users = users
	generator.sshKeys = sshKeys
	templateResult, err := generator.GenerateWithTemplate()
	if !assert.NoError(t, err) {
		return
	}
	result, err := generator.Generate()
	if !assert.NoError(t, err) {
		return
	}
	yamlResult, err := generator.GenerateWithYAML()
	if !assert.NoError(t, err) {
		return
	}

	var templateCloudInitScript map[string]interface{}
	err = yaml.Unmarshal([]byte(templateResult), &templateCloudInitScript)
	if !assert.NoError(t, err) {
		return
	}
	var cloudInitScript map[string]interface{}
	err = yaml.Unmarshal([]byte(result), &cloudInitScript)
	if !assert.NoError(t, err) {
		return
	}
	var yamlCloudInitScript map[string]interface{}
	err = yaml.Unmarshal([]byte(yamlResult), &yamlCloudInitScript)
	if !assert.NoError(t, err) {
		return
	}

	if templateResult == result {
		return
	}
	assert.Equal(t, templateCloudInitScript, cloudInitScript)
	assert.Equal(t, templateCloudInitScript, yamlCloudInitScript)
}

func TestTrimUsername(t *testing.T) {
	type args struct {
		cacaoUsername string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "normal",
			args: args{
				cacaoUsername: "foobar",
			},
			want: "foobar",
		},
		{
			name: "email",
			args: args{
				cacaoUsername: "foo@bar.com",
			},
			want: "foo",
		},
		{
			name: "1st is non-alphanumerical",
			args: args{
				cacaoUsername: "_foo",
			},
			want: "cacao",
		},
		{
			name: "with .-_",
			args: args{
				cacaoUsername: "foo.bar-foo_bar@bar.com",
			},
			want: "foo.bar-foo_bar",
		},
		{
			name: "all bad characters",
			args: args{
				cacaoUsername: "!@#$%^&*()",
			},
			want: "cacao",
		},
		{
			name: "too long 1",
			args: args{
				cacaoUsername: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
			},
			want: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		},
		{
			name: "too long 2",
			args: args{
				cacaoUsername: "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb@foo.com",
			},
			want: "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := TrimUsername(tt.args.cacaoUsername); got != tt.want {
				t.Errorf("trimUsername() = %v, want %v", got, tt.want)
			}
		})
	}
}
