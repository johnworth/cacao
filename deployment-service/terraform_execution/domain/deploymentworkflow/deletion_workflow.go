package deploymentworkflow

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
)

// DeletionWorkflowHandler handles deletion workflow result.
type DeletionWorkflowHandler struct {
	storage    ports.DeploymentStorage
	runStorage ports.DeploymentRunStorage
	eventSink  ports.EventSink
}

// NewDeletionWorkflowHandler ...
func NewDeletionWorkflowHandler(portsDependency ports.Ports) *DeletionWorkflowHandler {
	return &DeletionWorkflowHandler{
		storage:    portsDependency.Storage,
		runStorage: portsDependency.RunStorage,
		eventSink:  portsDependency.EventSink,
	}
}

// WorkflowSucceeded handles WorkflowSucceeded event
func (h DeletionWorkflowHandler) WorkflowSucceeded(event awmclient.WorkflowSucceeded) {
	logger := log.WithFields(log.Fields{
		"package":  "deploymentworkflow",
		"function": "DeletionWorkflowHandler.WorkflowSucceeded",
	})

	deployment, err := h.storage.SearchByDeletionWorkflow(event.WorkflowName)
	if err != nil {
		logger.WithError(err).Error("fail to find any deployment matching the deletion workflow")
		return
	}

	logger = logger.WithFields(log.Fields{"deployment": deployment.ID})
	resultEvent := h.workflowSucceeded(deployment)

	err = h.publishResultEvent(deployment.Deletion.Transaction, resultEvent)
	if err != nil {
		logger.WithError(err).Error("fail to publish event")
		return
	}
}

// return nil if there is no event to be sent
func (h DeletionWorkflowHandler) workflowSucceeded(deployment deploymentcommon.Deployment) service.DeploymentDeletionResult {
	logger := log.WithFields(log.Fields{
		"package":    "workflow",
		"function":   "DeletionWorkflowHandler.workflowSucceeded",
		"deployment": deployment.ID,
	})
	err := h.storage.StatusDeleted(deployment.ID)
	if err != nil {
		logger.WithError(err).Error("fail to delete deployment in storage")
		return h.deleteFailedEvent(deployment.ID, err)
	}
	logger.Info("deployment deleted")

	return h.deletedEvent(deployment.ID)
}

func (h DeletionWorkflowHandler) deleteFailedEvent(deployment common.ID, err error) service.DeploymentDeletionResult {
	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	return service.DeploymentDeletionResult{
		Session: service.Session{
			SessionActor:    "", // FIXME get actor from event or storage
			SessionEmulator: "", // FIXME get emulator from event or storage
			ServiceError:    serviceError.GetBase(),
		},
		ID: deployment,
	}
}

func (h DeletionWorkflowHandler) deletedEvent(deployment common.ID) service.DeploymentDeletionResult {
	return service.DeploymentDeletionResult{
		Session: service.Session{
			SessionActor:    "", // FIXME get actor from event or storage
			SessionEmulator: "", // FIXME get emulator from event or storage
		},
		ID: deployment,
	}
}

// WorkflowFailed handles WorkflowFailed event
func (h DeletionWorkflowHandler) WorkflowFailed(event awmclient.WorkflowFailed) {
	logger := log.WithFields(log.Fields{
		"package":  "workflow",
		"function": "DeletionWorkflowHandler.WorkflowFailed",
	})
	deployment, err := h.storage.SearchByDeletionWorkflow(event.WorkflowName)
	if err != nil {
		logger.WithError(err).Error("fail to find any deployment matching the deletion workflow")
		// cannot find matching deployment, thus cannot publish failure event, so early return
		return
	}

	errorMessage := fmt.Sprintf("workflow '%s' failed", event.WorkflowName)
	result := service.DeploymentDeletionResult{
		Session: service.Session{
			SessionActor:    "", // FIXME get actor from event or storage
			SessionEmulator: "", // FIXME get emulator from event or storage
			ServiceError:    service.NewCacaoGeneralError(errorMessage).GetBase(),
		},
		ID: deployment.ID,
	}
	deployment.Deletion.Deleting = false
	err = h.storage.StatusDeletionErrored(deployment.ID)
	if err != nil {
		logger.WithError(err).Error("fail to remove deleting flag from deployment")
		// proceed to publish event, no return
	}

	err = h.publishResultEvent(deployment.Deletion.Transaction, result)
	if err != nil {
		logger.WithError(err).Error("fail to publish event")
		return
	}
}

func (h DeletionWorkflowHandler) publishResultEvent(transaction common.TransactionID, result service.DeploymentDeletionResult) error {
	respEvent := h.toRespEvent(transaction, result)
	err := h.eventSink.Publish(respEvent)
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"package":     "workflow",
		"function":    "DeletionWorkflowHandler.publishResultEvent",
		"eventType":   respEvent.EventType(),
		"transaction": respEvent.Transaction(),
	}).Info("event published")
	return nil
}

func (h DeletionWorkflowHandler) toRespEvent(transaction common.TransactionID, result service.DeploymentDeletionResult) ResponseEvent {
	if result.GetServiceError() == nil {
		return NewEvent(service.DeploymentDeleted, transaction, result)
	}

	return NewEvent(service.DeploymentDeleteFailed, transaction, result)
}
