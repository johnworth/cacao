package deploymentworkflow

import (
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
)

// ExecutionWorkflowHandler handles events that is emitted when a execution workflow succeeded.
type ExecutionWorkflowHandler struct {
	storage    ports.DeploymentStorage
	runStorage ports.DeploymentRunStorage
	eventSink  ports.EventSink
}

// NewExecutionWorkflowHandler ...
func NewExecutionWorkflowHandler(portsDependency ports.Ports) ExecutionWorkflowHandler {
	return ExecutionWorkflowHandler{
		storage:    portsDependency.Storage,
		runStorage: portsDependency.RunStorage,
		eventSink:  portsDependency.EventSink,
	}
}

// WorkflowSucceeded handles the case when workflow has succeeded
func (h ExecutionWorkflowHandler) WorkflowSucceeded(event awmclient.WorkflowSucceeded, run deploymentcommon.DeploymentRun) {
	logger := log.WithFields(log.Fields{
		"package":  "executionstage",
		"function": "ExecutionWorkflowHandler.WorkflowSucceeded",
		"run":      run.ID,
		"wfName":   event.WorkflowName,
	})
	runOldStatus := run.Status

	finalRunStatus, err := h.updateRun(run, event.WorkflowName, event.Status, event.WfOutputs)
	if err != nil {
		logger.WithError(err).Error("fail to update deployment run in storage")
		return
	}
	if finalRunStatus == deploymentcommon.DeploymentRunErrored {
		logger.Error("deployment run errored")
		if err = h.storage.StatusCreationFailed(run.Deployment); err != nil {
			logger.WithError(err).Error("fail to update deployment status in storage")
			return
		}
	} else if finalRunStatus == deploymentcommon.DeploymentRunActive {
		if err = h.storage.StatusActive(run.Deployment); err != nil {
			logger.WithError(err).Error("fail to update deployment status in storage")
			return
		}
	}

	err = h.publishStatusUpdatedEvent(run, runOldStatus, finalRunStatus)
	if err != nil {
		logger.WithError(err).Error("fail to publish status updated event")
		return
	}
}

// only return err!=nil when error is returned from storage.
func (h ExecutionWorkflowHandler) updateRun(
	run deploymentcommon.DeploymentRun,
	wfName string,
	wfStatus awmclient.WorkflowStatus,
	wfOutputs map[string]interface{}) (deploymentcommon.DeploymentRunStatus, error) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "ExecutionWorkflowHandler.updateRun",
		"run":      run.ID,
		"wfName":   wfName,
	})
	deployment, err := h.storage.Get(run.Deployment)
	if err != nil {
		log.Error(err)
		return "", err
	}
	badWfOutput := false
	rawState, err := decodeRawState(wfOutputs)
	if err != nil {
		// log the error but do not return immediately
		logger.WithError(err).Error("fail to parse TF raw state")
		badWfOutput = true
	}
	var stateParser deploymentcommon.TerraformStateParser
	stateView, err := stateParser.ToStateView(*rawState.TFState, deployment.PrimaryCloudProvider)
	if err != nil {
		// log the error but do not return immediately
		logger.WithError(err).Error("fail to convert TF raw state to state view")
		badWfOutput = true
	}

	if badWfOutput {
		run.Status = deploymentcommon.DeploymentRunErrored
	} else {
		run.Status = deploymentcommon.DeploymentRunActive
	}

	// update deployment run in storage
	err = h.runStorage.ExecWorkflowSucceed(
		run.ID,
		wfName,
		string(wfStatus),
		run.Status,
		rawState,
		deploymentcommon.DeploymentStateViewFromExternal(stateView),
	)
	if err != nil {
		return "", err
	}
	return run.Status, nil
}

// WorkflowFailed handles the case when the workflow has failed
func (h ExecutionWorkflowHandler) WorkflowFailed(event awmclient.WorkflowFailed, run deploymentcommon.DeploymentRun) {
	logger := log.WithFields(log.Fields{
		"package":  "executionstage",
		"function": "ExecutionWorkflowHandler.WorkflowFailed",
		"run":      run.ID,
		"wfName":   event.WorkflowName,
	})
	runOldStatus := run.Status
	err := h.runStorage.ExecWorkflowFailed(run.ID, event.WorkflowName, string(event.Status))
	if err != nil {
		logger.WithError(err).Error("fail to update run in storage")
		return
	}
	err = h.storage.StatusCreationFailed(run.Deployment)
	if err != nil {
		logger.WithError(err).Error("fail to update deployment status in storage")
		return
	}

	// publish status update event
	err = h.publishStatusUpdatedEvent(run, runOldStatus, deploymentcommon.DeploymentRunErrored)
	if err != nil {
		logger.WithError(err).Error("fail to publish status updated event")
		return
	}
}

func (h ExecutionWorkflowHandler) publishStatusUpdatedEvent(
	run deploymentcommon.DeploymentRun,
	runOldStatus deploymentcommon.DeploymentRunStatus,
	finalRunStatus deploymentcommon.DeploymentRunStatus,
) error {
	statusUpdatedEvent := deploymentcommon.DeploymentRunStatusUpdated{
		Session: service.Session{
			SessionActor:    run.CreatedBy.User,
			SessionEmulator: run.CreatedBy.Emulator,
		},
		Deployment:    run.Deployment,
		Run:           run.ID,
		Status:        finalRunStatus,
		OldStatus:     runOldStatus,
		TransactionID: "", // TODO transaction ID
	}
	return h.eventSink.Publish(statusUpdatedEvent)
}

func decodeRawState(wfOutputs map[string]interface{}) (*deploymentcommon.RawDeploymentState, error) {
	// update raw state
	var tfState deploymentcommon.TerraformState
	err := mapstructure.Decode(wfOutputs, &tfState)
	if err != nil {
		return nil, service.NewCacaoGeneralError(err.Error())
	}
	return &deploymentcommon.RawDeploymentState{
		Type:    deploymentcommon.TerraformRawState,
		TFState: &tfState,
	}, nil
}
