package deploymentworkflow

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/domainutils"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/executionstage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// PreflightWorkflowHandler handles events that is emitted when a preflight workflow succeeded.
type PreflightWorkflowHandler struct {
	storage       ports.DeploymentStorage
	runStorage    ports.DeploymentRunStorage
	credMS        ports.CredentialMicroservice
	keySrc        ports.KeySrc
	executionChan chan<- executionstage.ExecutionRequest
	eventSink     ports.EventSink
}

// NewPreflightWorkflowHandler ...
func NewPreflightWorkflowHandler(
	portsDependency ports.Ports,
	executionChan chan<- executionstage.ExecutionRequest,
) *PreflightWorkflowHandler {
	return &PreflightWorkflowHandler{
		storage:       portsDependency.Storage,
		runStorage:    portsDependency.RunStorage,
		credMS:        portsDependency.CredentialMS,
		keySrc:        portsDependency.KeySrc,
		executionChan: executionChan,
		eventSink:     portsDependency.EventSink,
	}
}

// WorkflowSucceeded handles the case when the workflow has succeeded.
// When workflow succeeds, the deployment run proceeds to execution stage.
func (h PreflightWorkflowHandler) WorkflowSucceeded(
	event awmclient.WorkflowSucceeded,
	run deploymentcommon.DeploymentRun,
) {
	logger := log.WithFields(log.Fields{
		"package":  "preflightstage",
		"function": "PreflightWorkflowHandler.WorkflowSucceed",
		"wfName":   event.WorkflowName,
	})
	err := h.runStorage.PreflightWorkflowSucceed(run.ID, event.WorkflowName, string(event.Status))
	if err != nil {
		logger.WithError(err).Error("fail to update deployment run in storage")
		return
	}
	logger.WithFields(log.Fields{
		"run":       run.ID,
		"runStatus": run.Status,
		"wfName":    event.WorkflowName,
		"wfStatus":  event.Status,
	}).Info("workflow end-dated")

	err = h.proceedToExecutionStage(run)
	if err != nil {
		logger.WithError(err).Error("fail to hand over to execution stage")
		return
	}
}

func (h PreflightWorkflowHandler) proceedToExecutionStage(run deploymentcommon.DeploymentRun) error {
	logger := log.WithFields(log.Fields{
		"package":  "preflightstage",
		"function": "PreflightWorkflowHandler.proceedToExecutionStage",
		"run":      run.ID,
	})
	deployment, err := h.storage.Get(run.Deployment)
	if err != nil {
		return err
	}
	actor := types.Actor{
		Actor:    run.CreatedBy.User,
		Emulator: run.CreatedBy.Emulator,
	}
	credEncoder := domainutils.NewCredentialEncoder(h.credMS, h.keySrc)
	if len(deployment.CloudCredentials) == 0 {
		return service.NewCacaoGeneralError("deployment has no cloud credentials, cannot proceed to execution")
	}
	cloudCredID, ok := deployment.FindCloudCredential(deployment.PrimaryCloudProvider)
	if !ok {
		return fmt.Errorf("no credential for provider %s", deployment.PrimaryCloudProvider)
	}
	cloudCredBase64, err := credEncoder.EncodeOpenstackCred(actor, cloudCredID.String())
	if err != nil {
		return err
	}
	var gitCredBase64 string
	if deployment.GitCredential != "" {
		gitCredBase64, err = credEncoder.EncodeGitCred(actor, deployment.GitCredential.String())
		if err != nil {
			return err
		}
	}
	h.sendExecutionRequest(executionstage.ExecutionRequest{
		Actor:           run.CreatedBy.User,
		Emulator:        run.CreatedBy.Emulator,
		Deployment:      deployment,
		Run:             run,
		CloudCredID:     cloudCredID,
		CloudCredBase64: cloudCredBase64,
		GitCredID:       deployment.GitCredential,
		GitCredBase64:   gitCredBase64,
	})
	logger.Info("hand over to execution")
	return nil
}

func (h PreflightWorkflowHandler) sendExecutionRequest(request executionstage.ExecutionRequest) {
	// call as go routine to avoid block on channel
	go func(channel chan<- executionstage.ExecutionRequest) {
		channel <- request
	}(h.executionChan)
}

// WorkflowFailed handles the case when the workflow has failed
func (h PreflightWorkflowHandler) WorkflowFailed(event awmclient.WorkflowFailed, run deploymentcommon.DeploymentRun) {
	logger := log.WithFields(log.Fields{
		"package":  "deploymentworkflow",
		"function": "PreflightWorkflowHandler.WorkflowFailed",
	})
	runOldStatus := run.Status

	// update run in storage
	err := h.runStorage.PreflightWorkflowFailed(run.ID, event.WorkflowName, string(event.Status))
	if err != nil {
		logger.WithError(err).Error("fail to update run in storage")
		return
	}
	err = h.storage.StatusCreationFailed(run.Deployment)
	if err != nil {
		logger.WithError(err).Error("fail to update deployment status in storage")
		return
	}

	// publish status update event
	var statusUpdated = deploymentcommon.DeploymentRunStatusUpdated{
		Session: service.Session{
			SessionActor:    run.CreatedBy.User,
			SessionEmulator: run.CreatedBy.Emulator,
		},
		Deployment: run.Deployment,
		Run:        run.ID,
		Status:     deploymentcommon.DeploymentRunErrored,
		OldStatus:  runOldStatus,
		// TODO carry the transaction ID from DeploymentRunCreate request rather than generate a new one.
		TransactionID: messaging.NewTransactionID(),
	}
	err = h.eventSink.Publish(statusUpdated)
	if err != nil {
		logger.WithError(err).Error("fail to publish event")
		return
	}
}
