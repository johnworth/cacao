package deploymentworkflow

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// Handler handles workflow result
type Handler interface {
	WorkflowSucceeded(event awmclient.WorkflowSucceeded)
	WorkflowFailed(event awmclient.WorkflowFailed)
}

// ResponseEvent is response event
type ResponseEvent struct {
	eventType   common.EventType
	transaction common.TransactionID
	event       interface{}
}

// NewEvent creates a new response event
func NewEvent(eventType common.EventType, transaction common.TransactionID, event interface{}) ResponseEvent {
	return ResponseEvent{eventType: eventType, transaction: transaction, event: event}
}

// EventType returns event type
func (e ResponseEvent) EventType() common.EventType {
	return e.eventType
}

// Transaction ...
func (e ResponseEvent) Transaction() common.TransactionID {
	return e.transaction
}

// ToCloudEvent converts event to cloudevent
func (e ResponseEvent) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEventWithTransactionID(e.event, string(e.eventType), source, e.transaction)
}

// EventHandler is handler for events
type EventHandler interface {
	Handle(types.IncomingEvent) ResponseEvent
}
