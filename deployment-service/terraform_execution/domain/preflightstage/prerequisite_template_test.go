package preflightstage

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/domainutils"
	portsmock "gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"testing"
	"time"
)

const sshPublicKey = "ssh-rsa fffff"

func TestPrerequisiteTemplateExecution_AsyncExecute(t *testing.T) {
	providerMS := &portsmock.ProviderMicroservice{}
	templateMS := &portsmock.TemplateMicroservice{}
	awm := awmclient.NewArgoWorkflowMediatorMock("wf-name-123", nil)

	var actor = types.Actor{
		Actor:    "test_user123",
		Emulator: "",
	}
	var deploymentID = common.NewID(service.DeploymentIDPrefix)
	var runID = common.NewID(service.RunIDPrefix)
	var providerID = common.NewID("provider")
	var templateID = common.NewID("template")

	var run = deploymentcommon.DeploymentRun{
		ID:         runID,
		Deployment: deploymentID,
	}
	var updatedRun = run
	updatedRun.AddWorkflow(deploymentcommon.NewDeploymentWorkflow(
		deploymentcommon.PreflightPurpose,
		"wf-name-123",
		providerID,
	))

	var provider = service.ProviderModel{
		ID:        providerID,
		Name:      "",
		Type:      "",
		URL:       "",
		CreatedAt: time.Time{},
		UpdatedAt: time.Time{},
		Metadata: map[string]interface{}{
			"prerequisite_template": map[string]interface{}{
				"template_id": templateID.String(),
				"param_value": map[string]interface{}{
					"foo": "bar",
				},
			},
			"public_ssh_key":        sshPublicKey,
			"external_network_name": "external_network_123",
			"external_network_uuid": "aaaaaaaa-aaaa-aaaa-aaaa-aaaaabbbbbbb",
			"external_subnet_uuids": []string{"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaccccccc"},
		},
	}
	providerMS.On("Get", actor, providerID).Return(&provider, nil)

	var template = service.TemplateModel{
		Session:     service.Session{},
		ID:          templateID,
		Owner:       "",
		Name:        "",
		Description: "",
		Public:      false,
		Source: service.TemplateSource{
			URI: "https://gitlab.com/cyverse/foobar.git",
			AccessParameters: map[string]interface{}{
				"branch": "master",
				"path":   "path123",
			},
		},
		Metadata: service.TemplateMetadata{
			Name:             "",
			Author:           "",
			AuthorEmail:      "",
			Description:      "",
			TemplateTypeName: "",
			CacaoPostTasks:   nil,
			Parameters: map[string]interface{}{
				"foo": map[string]interface{}{
					"type": "string",
				},
				"username": map[string]interface{}{
					"type": "string",
				},
				"external_network_uuid": map[string]interface{}{
					"type": "string",
				},
				"keypair_name": map[string]interface{}{
					"type":    "string",
					"default": "this_value_should_be_replaced_by_injection_at_runtime",
				},
				"public_ssh_key": map[string]interface{}{
					"type": "string",
				},
			},
		},
		CreatedAt:        time.Time{},
		UpdatedAt:        time.Time{},
		UpdateFieldNames: nil,
		CredentialID:     "",
	}
	templateMS.On("Get", actor, templateID).Return(&template, nil)

	var request = PreflightRequest{
		Actor:    actor.Actor,
		Emulator: "",
		Deployment: deploymentcommon.Deployment{
			ID:                   deploymentID,
			Template:             templateID,
			PrimaryCloudProvider: providerID,
			CloudCredentials:     nil,
			LastRun:              nil,
		},
		Run:             run,
		CloudCredID:     "cloud-cred123",
		CloudCredBase64: "AAAAA",
		GitCredID:       "",
		GitCredBase64:   "",
	}
	prerequisiteExec := NewPrerequisiteTemplateExecution(providerMS, templateMS, awm)
	_, _, err := prerequisiteExec.AsyncExecute(request)
	assert.NoError(t, err)

	assert.True(t, awm.Called)
	assert.Equal(t, providerID, awm.Provider)
	assert.Equal(t, actor.Actor, awm.Username)
	assert.Equal(t, awmclient.TerraformWorkflowData{
		Username:          actor.Actor,
		Deployment:        deploymentID,
		TemplateID:        templateID,
		TerraformStateKey: PrerequisiteTemplateStateKey(request),
		GitURL:            template.Source.URI,
		GitTrackedUpStream: struct {
			Branch string `mapstructure:"branch"`
			Tag    string `mapstructure:"tag"`
			Commit string `mapstructure:"commit"`
		}{
			Branch: template.Source.AccessParameters["branch"].(string),
		},
		Path:        template.Source.AccessParameters["path"].(string),
		CloudCredID: "cloud-cred123",
		AnsibleVars: map[string]interface{}{
			"TF_INPUT_VALUES": map[string]interface{}{
				"foo":                   "bar",
				"username":              actor.Actor,
				"external_network_uuid": "aaaaaaaa-aaaa-aaaa-aaaa-aaaaabbbbbbb",
				"keypair_name":          domainutils.KeyPairName(actor.Actor),
				"public_ssh_key":        sshPublicKey,
			},
			"TF_INPUT_VARS": map[string]interface{}{
				"foo": map[string]string{
					"type": "string",
				},
				"username": map[string]string{
					"type": "string",
				},
				"external_network_uuid": map[string]string{
					"type": "string",
				},
				"keypair_name": map[string]string{
					"type": "string",
				},
				"public_ssh_key": map[string]string{
					"type": "string",
				},
			},
			"TF_MODULE": "cacao_module",
		},
		CloudCredentialBase64: "AAAAA",
		GitCredID:             "",
		GitCredentialBase64:   "",
	}, awm.TFWfData)
	providerMS.AssertExpectations(t)
	templateMS.AssertExpectations(t)
}
