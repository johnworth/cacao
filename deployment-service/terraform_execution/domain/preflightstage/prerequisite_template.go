package preflightstage

import (
	"fmt"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/domainutils"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// prerequisite template is template that needs to be applied first before executing the actual deployment run.
// For openstack, it could be things like network, security group, etc.

// PrerequisiteTemplateExecution initiate the execution of the prerequisite template.
type PrerequisiteTemplateExecution struct {
	providerMS ports.ProviderMicroservice
	templateMS ports.TemplateMicroservice
	awm        ports.ArgoWorkflowMediator
}

// NewPrerequisiteTemplateExecution ...
func NewPrerequisiteTemplateExecution(
	providerMS ports.ProviderMicroservice,
	templateMS ports.TemplateMicroservice,
	awm ports.ArgoWorkflowMediator,
) PrerequisiteTemplateExecution {
	return PrerequisiteTemplateExecution{
		providerMS: providerMS,
		templateMS: templateMS,
		awm:        awm,
	}
}

// AsyncExecute execute the template asynchronously, this does not wait for the result of the execution.
func (exec PrerequisiteTemplateExecution) AsyncExecute(request PreflightRequest) (awmProvider common.ID, wfName string, err error) {
	logger := log.WithFields(log.Fields{
		"package":  "preflightstage",
		"function": "PrerequisiteTemplateExecution.asyncExecute",
	})
	providerMetadata, err := exec.fetchProviderMetadata(request)
	if err != nil {
		logger.WithError(err).Error("fail to fetch provider metadata from primary provider")
		return "", "", err
	}
	template, err := exec.fetchTemplate(request, providerMetadata)
	if err != nil {
		logger.WithError(err).Error("fail to fetch prerequisite template")
		return "", "", err
	}
	paramValues := exec.injectParameterValue(
		request,
		providerMetadata,
	)
	awmProvider, wfName, err = exec.execTemplate(request, template, paramValues)
	if err != nil {
		logger.WithError(err).Error("fail to start execution of prerequisite template")
	}
	return awmProvider, wfName, err
}

func (exec PrerequisiteTemplateExecution) fetchProviderMetadata(request PreflightRequest) (types.OpenStackProviderMetadata, error) {
	logger := log.WithFields(log.Fields{
		"package":    "preflightstage",
		"function":   "PrerequisiteTemplateExecution.fetchProviderMetadata",
		"deployment": request.Deployment.ID,
		"run":        request.Run.ID,
		"provider":   request.Deployment.PrimaryCloudProvider,
	})
	actor := types.Actor{
		Actor:    request.Actor,
		Emulator: request.Emulator,
	}
	primaryProvider, err := exec.providerMS.Get(actor, request.Deployment.PrimaryCloudProvider)
	if err != nil {
		return types.OpenStackProviderMetadata{}, err
	}
	var metadata types.OpenStackProviderMetadata
	err = mapstructure.Decode(primaryProvider.GetMetadata(), &metadata)
	if err != nil {
		logger.WithError(err).Errorf("provider %s has illformed metadata", primaryProvider.GetID())
		return types.OpenStackProviderMetadata{}, fmt.Errorf("fail to decode provider metadata, %w", err)
	}
	if !metadata.Validate() {
		err = service.NewCacaoInvalidParameterErrorWithOptions("invalid openstack provider metadata")
		return types.OpenStackProviderMetadata{}, err
	}
	return metadata, err
}

// Fetch prerequisite template from Template MS based on the template ID in provider metadata.
func (exec PrerequisiteTemplateExecution) fetchTemplate(request PreflightRequest, providerMetadata types.OpenStackProviderMetadata) (service.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":    "preflightstage",
		"function":   "PrerequisiteTemplateExecution.fetchTemplate",
		"deployment": request.Deployment.ID,
		"run":        request.Run.ID,
	})
	actor := types.Actor{
		Actor:    request.Actor,
		Emulator: request.Emulator,
	}
	template, err := exec.templateMS.Get(actor, providerMetadata.PreRequisiteTemplate.TemplateID)
	if err != nil {
		logger.WithError(err).Error("fail to get template")
		return nil, err
	}
	return template, nil
}

// execPrerequisiteTemplate only launch workflow to execute the prerequisite
// template, but does not wait for workflow completion.
// Return provider ID that workflow is on, workflow name if success.
func (exec PrerequisiteTemplateExecution) execTemplate(
	request PreflightRequest,
	template service.Template,
	paramValues service.DeploymentParameterValues,
) (common.ID, string, error) {
	logger := log.WithFields(log.Fields{
		"package":    "preflightstage",
		"function":   "PreflightHandler.execPrerequisiteTemplate",
		"deployment": request.Deployment.ID,
		"run":        request.Run.ID,
	})
	actor := types.Actor{
		Actor:    request.Actor,
		Emulator: request.Emulator,
	}
	paramConverter := domainutils.ParameterConverter{}
	param, err := paramConverter.Convert(template.GetMetadata().Parameters, paramValues)
	if err != nil {
		return "", "", err
	}
	snapshot, err := domainutils.GetTemplateSnapshot(template)
	if err != nil {
		return "", "", err
	}
	wfBuilder := domainutils.NewTerraformWorkflowBuilder(exec.awm, nil, nil)
	wfBuilder.SetActor(actor)
	wfBuilder.SetDeployment(request.Deployment)
	wfBuilder.SetTemplateSnapshot(snapshot)
	wfBuilder.SetParameter(param)
	wfBuilder.SetCloudCredential(request.CloudCredID, request.CloudCredBase64)
	// FIXME pre-requisite template should not require git cred,
	// even if it does, this will likely be a different git cred
	// than the one provided by request.
	wfBuilder.SetGitCredential(request.GitCredID, request.GitCredBase64)
	wfBuilder.SetTerraformStateKey(PrerequisiteTemplateStateKey(request))

	awmProvider, wfName, err := wfBuilder.CreateWorkflow()
	if err != nil {
		logger.WithError(err).Error("fail to create workflow to apply pre-requisite")
		return "", "", err
	}
	logger.WithField("wfName", wfName).Info("workflow launched for pre-requisite template")
	return awmProvider, wfName, nil
}

// injectParameterValue inject parameter values that are specific for the user, or the provider.
// e.g. username, external network id, public ssh key, etc.
func (exec PrerequisiteTemplateExecution) injectParameterValue(
	request PreflightRequest,
	providerMetadata types.OpenStackProviderMetadata,
) (paramVal service.DeploymentParameterValues) {
	// prepare extra param values to be injected
	paramInjection := types.OpenStackPreRequisiteTemplateParameterInjection{
		Username: request.Actor,
	}
	paramInjection.KeyPairName = domainutils.KeyPairName(request.Actor)
	if providerMetadata.PublicSSHKey != "" {
		paramInjection.PublicSSHKey = providerMetadata.PublicSSHKey
	}
	if len(providerMetadata.ExternalNetworkUUID) > 0 {
		paramInjection.ExternalNetworkUUID = providerMetadata.ExternalNetworkUUID
	}

	paramVal = make(map[string]interface{})

	// use param values from metadata as a basis
	if providerMetadata.PreRequisiteTemplate.ParamValue != nil {
		for key, val := range providerMetadata.PreRequisiteTemplate.ParamValue {
			paramVal[key] = val
		}
	}

	// inject extra param values
	err := mapstructure.Decode(paramInjection, &paramVal)
	if err != nil {
		log.WithFields(log.Fields{
			"package":  "preflightstage",
			"function": "PrerequisiteTemplateExecution.injectParameterValue",
		}).WithError(err).Error("failed to inject param value for prerequisite template")
		return paramVal
	}
	return
}

// PrerequisiteTemplateStateKey is the key to raw state in state storage (e.g. tf workspace name in tf backend) for
// applying prerequisite template. Uses a different one than the actual deployment run to keep the state separate.
// For now, assume the application of prerequisite is unique and specific to a provider. So the key should be unique
// for all deployment run for a user on a specific provider.
func PrerequisiteTemplateStateKey(request PreflightRequest) string {
	return request.Deployment.PrimaryCloudProvider.String() + "-" + request.Actor
}
