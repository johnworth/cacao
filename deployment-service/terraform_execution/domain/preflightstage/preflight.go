package preflightstage

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/executionstage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"sync"
)

// PreflightRequest is a request to perform preflight on a deployment run
type PreflightRequest struct {
	Actor           string
	Emulator        string
	Deployment      deploymentcommon.Deployment
	Run             deploymentcommon.DeploymentRun
	CloudCredID     deploymentcommon.CredentialID
	CloudCredBase64 string
	GitCredID       deploymentcommon.CredentialID
	GitCredBase64   string
}

// PreflightHandler ...
type PreflightHandler struct {
	reqChan <-chan PreflightRequest

	storage    ports.DeploymentStorage
	runStorage ports.DeploymentRunStorage
	awm        ports.ArgoWorkflowMediator
	providerMS ports.ProviderMicroservice
	templateMS ports.TemplateMicroservice

	executionChan chan<- executionstage.ExecutionRequest
}

// NewPreflightHandler ...
func NewPreflightHandler(portsDependency ports.Ports, requestChan <-chan PreflightRequest, executionChan chan<- executionstage.ExecutionRequest) *PreflightHandler {
	return &PreflightHandler{
		reqChan:       requestChan,
		storage:       portsDependency.Storage,
		runStorage:    portsDependency.RunStorage,
		awm:           portsDependency.AWM,
		providerMS:    portsDependency.ProviderMS,
		templateMS:    portsDependency.TemplateMS,
		executionChan: executionChan,
	}
}

// Start starts process request from channel.
func (h PreflightHandler) Start(wg *sync.WaitGroup) {
	defer wg.Done()
	for req := range h.reqChan {
		h.handle(req)
	}
}

func (h PreflightHandler) handle(request PreflightRequest) {
	logger := log.WithFields(log.Fields{
		"package":  "preflightstage",
		"function": "PreflightHandler.handle",
	})

	if request.Run.Status != deploymentcommon.DeploymentRunPreflight {
		err := errors.New("status is not preflight")
		logger.WithField("status", request.Run.Status).WithError(err).Error()
		h.preflightFailed(logger, request, err)
		return
	}

	if !h.prerequisiteTemplateApplied() {
		if err := h.applyPrerequisiteTemplate(logger, request); err != nil {
			return
		}
	} else {
		// hand over to execution
		h.proceedToExecution(executionstage.ExecutionRequest{
			Actor:           request.Actor,
			Emulator:        request.Emulator,
			Deployment:      request.Deployment,
			Run:             request.Run,
			CloudCredID:     request.CloudCredID,
			CloudCredBase64: request.CloudCredBase64,
			GitCredID:       request.GitCredID,
			GitCredBase64:   request.GitCredBase64,
		})
	}
}

func (h PreflightHandler) applyPrerequisiteTemplate(logger *log.Entry, request PreflightRequest) error {
	prerequisiteExec := NewPrerequisiteTemplateExecution(h.providerMS, h.templateMS, h.awm)
	awmProvider, wfName, err := prerequisiteExec.AsyncExecute(request)
	if err != nil {
		h.preflightFailed(logger, request, err)
		return err
	}
	err = h.updateRunWithWorkflow(logger, request, wfName, awmProvider)
	if err != nil {
		return err
	}
	return nil
}

// check if pre-requisite template has been applied.
func (h PreflightHandler) prerequisiteTemplateApplied() bool {
	// for now always assume the pre-requisite template is absent, and needs to be deployed first
	return false
}

func (h PreflightHandler) updateRunWithWorkflow(logger *log.Entry, request PreflightRequest, wfName string, awmProvider common.ID) error {
	err := h.runStorage.StartPreflightWorkflow(request.Run.ID, wfName, awmProvider)
	if err != nil {
		logger.WithError(err).Error("fail to save workflow name (pre-requisite) in storage")
		return err
	}
	return nil
}

func (h PreflightHandler) preflightFailed(logger *log.Entry, request PreflightRequest, preflightErr error) {
	err := h.runStorage.FailedToStartPreflight(request.Run.ID, preflightErr)
	if err != nil {
		logger.WithError(err).Error("fail to change run status to errored")
		return
	}
	err = h.storage.StatusCreationFailed(request.Deployment.ID)
	if err != nil {
		logger.WithError(err).Error("fail to change deployment status to creation_errored")
		return
	}
}

func (h PreflightHandler) proceedToExecution(request executionstage.ExecutionRequest) {
	// call as go routine to avoid block on channel
	go func(channel chan<- executionstage.ExecutionRequest) {
		channel <- request
	}(h.executionChan)
}
