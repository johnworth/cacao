package executionstage

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/domainutils"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"sync"
	"time"
)

// ExecutionRequest is a request to perform execution on a deployment run
type ExecutionRequest struct {
	Actor           string
	Emulator        string
	Deployment      deploymentcommon.Deployment
	Run             deploymentcommon.DeploymentRun
	CloudCredID     deploymentcommon.CredentialID
	CloudCredBase64 string
	GitCredID       deploymentcommon.CredentialID
	GitCredBase64   string
}

// DeploymentExecutor starts the execution on the template.
//
// Note: this only starts the execution, but does not wait for the execution completion.
type DeploymentExecutor struct {
	reqChan <-chan ExecutionRequest

	awm        ports.ArgoWorkflowMediator
	runStorage ports.DeploymentRunStorage
	eventSink  ports.EventSink
}

// NewDeploymentExecutor ...
func NewDeploymentExecutor(portsDependency ports.Ports, requestChan <-chan ExecutionRequest) *DeploymentExecutor {
	return &DeploymentExecutor{
		reqChan:    requestChan,
		awm:        portsDependency.AWM,
		runStorage: portsDependency.RunStorage,
		eventSink:  portsDependency.EventSink,
	}
}

// Start starts process request from channel.
func (e DeploymentExecutor) Start(wg *sync.WaitGroup) {
	defer wg.Done()
	for req := range e.reqChan {
		e.handle(req)
	}
}

// handle execute a deployment run
func (e DeploymentExecutor) handle(request ExecutionRequest) {
	logger := log.WithFields(log.Fields{
		"package":  "executionstage",
		"function": "DeploymentExecutor.handle",
	})

	ansibleVars, err := domainutils.ParamToAnsibleVars(request.Run.Parameters)

	if err != nil {
		logger.WithError(err).Error("fail to construct ansible vars for AWM request")
		e.executionFailed(request, err)
		return
	}

	// compose a request to AWM
	tfWfData := awmclient.TerraformWorkflowData{
		Username:          request.Actor,
		Deployment:        request.Deployment.ID,
		TemplateID:        request.Deployment.Template,
		TerraformStateKey: request.Deployment.ID.String(), // use deployment ID as the key
		GitURL:            request.Run.TemplateSnapshot.GitURL,
		GitTrackedUpStream: struct {
			Branch string `mapstructure:"branch"`
			Tag    string `mapstructure:"tag"`
			Commit string `mapstructure:"commit"`
		}{
			Branch: request.Run.TemplateSnapshot.UpstreamTracked.Branch,
			Tag:    request.Run.TemplateSnapshot.UpstreamTracked.Tag,
			Commit: request.Run.TemplateSnapshot.CommitHash,
		},
		Path:                  request.Run.TemplateSnapshot.SubPath,
		CloudCredID:           request.CloudCredID.String(),
		AnsibleVars:           ansibleVars,
		CloudCredentialBase64: request.CloudCredBase64,
		GitCredID:             request.GitCredID.String(),
		GitCredentialBase64:   request.GitCredBase64,
	}

	awmProvider := request.Deployment.PrimaryCloudProvider

	wfName, err := e.awm.Create(
		context.TODO(),
		awmProvider,
		request.Actor,
		awmclient.TerraformWorkflowFilename,
		awmclient.AWMTerraformWorflowData(tfWfData),
	)
	if err != nil {
		logger.WithError(err).Error("fail to create workflow via AWM")
		e.executionFailed(request, err)
		return
	}
	logger.WithField("wfName", wfName).Info("workflow created")

	runOldStatus := request.Run.Status

	// update deployment run in storage
	err = e.runStorage.StartExecWorkflow(request.Run.ID, wfName, awmProvider)
	if err != nil {
		logger.WithError(err).Errorf("fail to update deployment run status to %s", deploymentcommon.DeploymentRunRunning)
		return
	}

	// emit event to indicate execution started
	err = e.publishStatusUpdatedEvent(request, runOldStatus, deploymentcommon.DeploymentRunRunning)
	if err != nil {
		logger.WithError(err).Error("fail to publish run status updated event")
		return
	}
}

func (e DeploymentExecutor) executionFailed(request ExecutionRequest, execErr error) {
	logger := log.WithFields(log.Fields{
		"package":  "executionstage",
		"function": "DeploymentExecutor.executionFailed",
	})
	runOldStatus := request.Run.Status
	request.Run.Status = deploymentcommon.DeploymentRunErrored
	request.Run.EndsAt = time.Now().UTC()
	err := e.runStorage.FailToStartExecWorkflow(request.Run.ID, execErr)
	if err != nil {
		logger.WithError(err).Errorf("fail to update run status to %s", request.Run.Status)
		return
	}
	err = e.publishStatusUpdatedEvent(request, runOldStatus, deploymentcommon.DeploymentRunErrored)
	if err != nil {
		logger.WithError(err).Error("fail to publish run status update event")
		return
	}
}

func (e DeploymentExecutor) publishStatusUpdatedEvent(request ExecutionRequest, oldStatus, status deploymentcommon.DeploymentRunStatus) error {
	var statusUpdated = deploymentcommon.DeploymentRunStatusUpdated{
		Session: service.Session{
			SessionActor:    request.Actor,
			SessionEmulator: request.Emulator,
		},
		Deployment: request.Deployment.ID,
		Run:        request.Run.ID,
		Status:     status,
		OldStatus:  oldStatus,
		// TODO carry the transaction ID from DeploymentRunCreate request rather than generate a new one.
		TransactionID: messaging.NewTransactionID(),
	}
	err := e.eventSink.Publish(statusUpdated)
	if err != nil {
		return err
	}
	return nil
}
