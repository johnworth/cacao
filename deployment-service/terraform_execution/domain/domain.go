package domain

import (
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/eventhandler"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/executionstage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/preflightstage"
	"sync"

	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

const eventWorkerCount = 5
const channelDepth = 5

// Domain is the entrypoint object for this service
type Domain struct {
	EventSrc   ports.EventSrc
	EventSink  ports.EventSink
	Storage    ports.DeploymentStorage
	RunStorage ports.DeploymentRunStorage
	KeySrc     ports.KeySrc

	WorkspaceMS  ports.WorkspaceMicroservice
	TemplateMS   ports.TemplateMicroservice
	CredentialMS ports.CredentialMicroservice
	ProviderMS   ports.ProviderMicroservice
	AWM          ports.ArgoWorkflowMediator

	eventChan     chan types.IncomingEvent
	preflightChan chan preflightstage.PreflightRequest
	executionChan chan executionstage.ExecutionRequest
}

// Init ...
func (svc *Domain) Init(conf types.Config) error {
	var err error
	err = svc.Storage.Init()
	if err != nil {
		return err
	}
	err = svc.RunStorage.Init()
	if err != nil {
		return err
	}
	err = svc.EventSrc.Init(conf.StanConfig)
	if err != nil {
		return err
	}
	svc.eventChan = make(chan types.IncomingEvent, channelDepth)
	svc.EventSrc.InitChannel(svc.eventChan)
	err = svc.EventSink.Init(conf.StanConfig)
	if err != nil {
		return err
	}
	svc.preflightChan = make(chan preflightstage.PreflightRequest, channelDepth)
	svc.executionChan = make(chan executionstage.ExecutionRequest, channelDepth)
	return nil
}

// Start start receiving and processing events
func (svc *Domain) Start() error {
	err := svc.EventSrc.Start()
	if err != nil {
		return err
	}

	var wg sync.WaitGroup
	svc.spawnEventWorker(&wg, eventWorkerCount)
	svc.startPreflightHandler(&wg)
	svc.startExecutor(&wg)

	wg.Wait()
	return nil
}

func (svc Domain) spawnEventWorker(wg *sync.WaitGroup, workerCount int) {
	for i := 0; i < workerCount; i++ {
		wg.Add(1)
		worker := eventhandler.NewEventWorker(svc.commonDependencies(), svc.preflightChan, svc.executionChan)
		go worker.Run(wg, svc.eventChan)
	}
}

func (svc Domain) startPreflightHandler(wg *sync.WaitGroup) {
	wg.Add(1)
	handler := preflightstage.NewPreflightHandler(svc.commonDependencies(), svc.preflightChan, svc.executionChan)
	go handler.Start(wg)
}

func (svc Domain) startExecutor(wg *sync.WaitGroup) {
	wg.Add(1)
	executor := executionstage.NewDeploymentExecutor(svc.commonDependencies(), svc.executionChan)
	go executor.Start(wg)
}

func (svc Domain) commonDependencies() ports.Ports {
	return ports.Ports{
		EventSink:    svc.EventSink,
		Storage:      svc.Storage,
		RunStorage:   svc.RunStorage,
		KeySrc:       svc.KeySrc,
		TemplateMS:   svc.TemplateMS,
		WorkspaceMS:  svc.WorkspaceMS,
		CredentialMS: svc.CredentialMS,
		ProviderMS:   svc.ProviderMS,
		AWM:          svc.AWM,
	}
}
