package main

import (
	"github.com/kelseyhightower/envconfig"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/deployment-service/internal/storage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/adapters"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

var envConf types.EnvConfig
var globalConf types.Config

func init() {
	err := envconfig.Process("", &envConf)
	if err != nil {
		log.Fatal(err)
	}
	logLevel, err := log.ParseLevel(envConf.LogLevel)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(logLevel)
	err = envconfig.Process("", &globalConf.StanConfig)
	if err != nil {
		log.Fatal(err)
	}
	err = envconfig.Process("AWM", &globalConf.AWMStanConfig)
	if err != nil {
		log.Fatal(err)
	}
	if envConf.PodName == "" {
		globalConf.StanConfig.ClientID = "deployment-exec-" + xid.New().String()
	} else {
		globalConf.StanConfig.ClientID = envConf.PodName
	}
	err = envconfig.Process("", &globalConf.MongoConfig)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	svc := newService()
	if err := svc.Init(globalConf); err != nil {
		log.Fatal(err)
	}
	if err := svc.Start(); err != nil {
		log.Fatal(err)
	}
}

func newService() domain.Domain {
	stanAdapter := adapters.NewStanAdapter()
	return domain.Domain{
		EventSrc:     stanAdapter,
		EventSink:    stanAdapter,
		Storage:      adapters.NewDeploymentStorage(storage.NewMongoDeploymentStorage(globalConf.MongoConfig)),
		RunStorage:   adapters.NewRunStorage(storage.NewMongoDeploymentRunStorage(globalConf.MongoConfig)),
		KeySrc:       adapters.NewEncryptionKeyEnvVar("ENCRYPTION_KEY"),
		WorkspaceMS:  adapters.NewWorkspaceMicroservice(globalConf.StanConfig),
		TemplateMS:   adapters.NewTemplateMicroservice(globalConf.StanConfig),
		CredentialMS: adapters.NewCredentialMicroservice(globalConf.StanConfig),
		ProviderMS:   adapters.NewProviderMicroservice(globalConf.StanConfig),
		AWM:          adapters.NewArgoWorkflowMediator(globalConf.AWMStanConfig),
	}
}
