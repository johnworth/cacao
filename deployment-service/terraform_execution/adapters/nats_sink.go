package adapters

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging"
	"time"
)

// NatsSink is a sink to publish NATS msg into
type NatsSink struct {
	nc *nats.Conn
}

// NewNatsSink ...
func NewNatsSink() *NatsSink {
	return &NatsSink{}
}

// Init establish a NATS connection
func (sink NatsSink) Init(config messaging.NatsConfig) error {
	nc, err := nats.Connect(config.URL, nats.ReconnectWait(time.Duration(config.ReconnectWait)*time.Second))
	if err != nil {
		return err
	}
	sink.nc = nc
	return nil
}

// Request sends a NATS request and return the reply
func (sink NatsSink) Request(subject string, req cloudevents.Event) (*cloudevents.Event, error) {
	log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "NatsSink.Request",
		"subject":  subject,
	}).Trace()
	// FIXME
	panic("implement me")
}
