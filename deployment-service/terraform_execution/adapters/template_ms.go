package adapters

import (
	"context"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// TemplateMicroservice is a client for template microservice
type TemplateMicroservice struct {
	stanConf types.StanConfig
}

// NewTemplateMicroservice ...
func NewTemplateMicroservice(stanConf types.StanConfig) TemplateMicroservice {
	return TemplateMicroservice{stanConf: stanConf}
}

// Get fetches a template by ID
func (t TemplateMicroservice) Get(actor types.Actor, id common.ID) (service.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "TemplateMicroservice.Get",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
		"template": id,
	})
	if actor.Actor == "" {
		return nil, service.NewCacaoInvalidParameterError("actor is empty")
	}
	client, err := service.NewNatsTemplateClient(context.TODO(), actor.Actor, actor.Emulator, t.stanConf.NatsConfig, t.stanConf.StanConfig)
	if err != nil {
		errorMessage := "fail to create template svc client"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoCommunicationError(errorMessage)
	}
	template, err := client.Get(id)
	if err != nil {
		errorMessage := "fail to get template"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoGeneralError(errorMessage)
	}
	logger.Trace("got template from template ms")
	return template, nil
}
