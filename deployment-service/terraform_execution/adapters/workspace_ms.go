package adapters

import (
	"context"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// WorkspaceMicroservice is a client to interact with Workspace microservice
type WorkspaceMicroservice struct {
	stanConf types.StanConfig
}

// NewWorkspaceMicroservice ...
func NewWorkspaceMicroservice(stanConf types.StanConfig) ports.WorkspaceMicroservice {
	return WorkspaceMicroservice{
		stanConf: stanConf,
	}
}

// Get a workspace
func (w WorkspaceMicroservice) Get(actor types.Actor, id common.ID) (service.Workspace, error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "WorkspaceMicroservice.Get",
		"actor":     actor.Actor,
		"emulator":  actor.Emulator,
		"workspace": id,
	})
	client, err := service.NewNatsWorkspaceClient(context.TODO(), actor.Actor, actor.Emulator, w.stanConf.NatsConfig, w.stanConf.StanConfig)
	if err != nil {
		logger.WithError(err).Error("fail to create workspace svc client")
		return nil, err
	}

	workspace, err := client.Get(id)
	if err != nil {
		logger.WithError(err).Error("fail to get workspace")
		return nil, err
	}
	logger.Debug("got workspace from workspace ms")
	return workspace, nil
}
