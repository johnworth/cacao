package adapters

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/storage"
	"time"
)

// BasicDeploymentStorage is a basic storage interface for deployment that supports basic operation (CRUD).
type BasicDeploymentStorage interface {
	Init() error
	Create(deploymentcommon.Deployment) error
	Get(common.ID) (deploymentcommon.Deployment, error)
	List(filters storage.DeploymentFilter, offset int64, limit int64, sort storage.DeploymentSort) ([]deploymentcommon.Deployment, error)
	Update(id common.ID, deployment storage.DeploymentUpdate, filter storage.DeploymentFilter) (bool, error)
	Delete(common.ID) error
}

// DeploymentStorage is a middleware that implements an abstraction on top of basic storage operation (CRUD).
// It also enforces some semantics on the object.
type DeploymentStorage struct {
	basicStorage BasicDeploymentStorage
	getTime      func() time.Time
}

// NewDeploymentStorage ...
func NewDeploymentStorage(basicStorage BasicDeploymentStorage) *DeploymentStorage {
	return &DeploymentStorage{basicStorage: basicStorage, getTime: getTimeNow}
}

// Init ...
func (ds DeploymentStorage) Init() error {
	return ds.basicStorage.Init()
}

// Get ...
func (ds DeploymentStorage) Get(id common.ID) (deploymentcommon.Deployment, error) {
	return ds.basicStorage.Get(id)
}

// SearchByDeletionWorkflow searches for deployment by deletion workflow name.
// The deployment must have current status that allows deleting, and a pending status of deleting.
func (ds DeploymentStorage) SearchByDeletionWorkflow(wfName string) (deploymentcommon.Deployment, error) {
	list, err := ds.basicStorage.List(storage.DeploymentFilter{
		DeletionWorkflow: wfName,
		CurrentStatus:    service.DeploymentStatusAllowDeletion,
		PendingStatus:    []service.DeploymentPendingStatus{service.DeploymentStatusDeleting},
	}, 0, -1, storage.DeploymentSort{
		SortBy:  storage.SortByID,
		SortDir: storage.AscendingSort,
	})
	if err != nil {
		log.Error(err)
		return deploymentcommon.Deployment{}, err
	}

	if len(list) == 0 {
		errorMessage := "no deployment match the deletion workflow"
		return deploymentcommon.Deployment{}, service.NewCacaoNotFoundError(errorMessage)
	} else if len(list) > 1 {
		// shouldn't happen normally, likely due to collision on workflow name
		deploymentIDs := make([]common.ID, 0, len(list))
		for _, deployment := range list {
			deploymentIDs = append(deploymentIDs, deployment.ID)
		}
		log.WithFields(log.Fields{
			"package":         "deploymentworkflow",
			"function":        "DeletionWorkflowHandler.findDeploymentByWorkflow",
			"wfName":          wfName,
			"deploymentCount": len(list),
			"deployments":     deploymentIDs,
		}).Warn("there is more than 1 deployment matched with the same deletion workflow")
		// FIXME this is likely due a collision of the workflow name

		// return the 1st one for now
		return list[0], nil
	}
	return list[0], nil
}

// Create ...
func (ds DeploymentStorage) Create(deployment deploymentcommon.Deployment) error {
	if !deployment.ID.Validate() {
		deployment.ID = common.NewID("deployment")
	}
	now := getTimeNow()
	deployment.CreatedAt = now
	deployment.UpdatedAt = now
	if err := ds.validateBeforeCreate(deployment); err != nil {
		return err
	}
	err := ds.basicStorage.Create(deployment)
	if err != nil {
		return err
	}
	return nil
}

func (ds DeploymentStorage) validateBeforeCreate(deployment deploymentcommon.Deployment) error {
	if len(deployment.Name) > deploymentcommon.DeploymentNameMaxLength {
		return service.NewCacaoInvalidParameterError("name of deployment too long")
	}
	if len(deployment.Description) > deploymentcommon.DeploymentDescriptionMaxLength {
		return service.NewCacaoInvalidParameterError("deployment description too long")
	}
	if deployment.CreatedBy.User == "" {
		return service.NewCacaoInvalidParameterError("deployment missing creator")
	}
	if deployment.Template == "" {
		return service.NewCacaoInvalidParameterError("deployment missing template")
	}
	if deployment.TemplateType == "" {
		return service.NewCacaoInvalidParameterError("deployment missing template type")
	}
	if deployment.PrimaryCloudProvider == "" {
		return service.NewCacaoInvalidParameterError("deployment missing primary provider")
	}
	if deployment.CurrentStatus == "" {
		return service.NewCacaoGeneralError("deployment missing current status")
	}
	err := validateDeploymentStatus(deployment.CurrentStatus, deployment.PendingStatus)
	if err != nil {
		return err
	}
	if len(deployment.StatusMsg) > deploymentcommon.DeploymentStatusMsgMaxLength {
		return service.NewCacaoInvalidParameterError("deployment status msg too long")
	}
	return nil
}

// Update ...
func (ds DeploymentStorage) Update(id common.ID, update storage.DeploymentUpdate) error {
	ok, err := ds.basicStorage.Update(id, update, storage.DeploymentFilter{})
	if err != nil {
		return err
	}
	if !ok {
		return service.NewCacaoNotFoundError("deployment not found")
	}
	return nil
}

// StatusActive sets the current status to active and pending status to none.
func (ds DeploymentStorage) StatusActive(id common.ID) error {
	return ds.changeCurrentStatus(id, service.DeploymentStatusActive)
}

// StatusCreationFailed sets the current status to creation_errored and pending status to none.
func (ds DeploymentStorage) StatusCreationFailed(id common.ID) error {
	return ds.changeCurrentStatus(id, service.DeploymentStatusCreationErrored)
}

// StatusDeletionErrored sets the current status to deletion_errored and pending status to none.
func (ds DeploymentStorage) StatusDeletionErrored(id common.ID) error {
	return ds.changeCurrentStatus(id, service.DeploymentStatusDeletionErrored)
}

// StatusDeleted sets the current status to deleted and pending status to none.
func (ds DeploymentStorage) StatusDeleted(id common.ID) error {
	return ds.changeCurrentStatus(id, service.DeploymentStatusDeleted)
}

func (ds DeploymentStorage) changeCurrentStatus(id common.ID, status service.DeploymentStatus) error {
	now := ds.getTime()
	newPendingStatus := service.DeploymentStatusNoPending
	ok, err := ds.basicStorage.Update(
		id,
		storage.DeploymentUpdate{
			UpdatedAt:     &now,
			CurrentStatus: &status,
			PendingStatus: &newPendingStatus,
		},
		storage.DeploymentFilter{})
	if err != nil {
		return err
	}
	if !ok {
		return service.NewCacaoNotFoundError("deployment not found")
	}
	return nil
}

// PendingStatusCreating changes status to creating.
// Deployment will only be updated if:
// 1. deployment's current status must allow run creation.
// 2. deployment must NOT have deleting or creating as pending status.
func (ds DeploymentStorage) PendingStatusCreating(id common.ID) (bool, error) {
	now := ds.getTime()
	newPending := service.DeploymentStatusCreating
	ok, err := ds.basicStorage.Update(
		id,
		storage.DeploymentUpdate{
			UpdatedAt:     &now,
			PendingStatus: &newPending,
		},
		storage.DeploymentFilter{
			// current status must allow run creation
			CurrentStatus: service.DeploymentStatusAllowRunCreation,
			// no pending status
			PendingStatus: []service.DeploymentPendingStatus{service.DeploymentStatusNoPending},
		})
	if err != nil {
		return false, err
	}
	return ok, nil
}

// PendingStatusDeleting changes the pending status to deleting.
// Deployment will only be updated if:
// 1. deployment's current status must allow deletion.
// 2. deployment must NOT have deleting or creating as pending status.
func (ds DeploymentStorage) PendingStatusDeleting(id common.ID) (bool, error) {
	now := ds.getTime()
	newPending := service.DeploymentStatusDeleting
	ok, err := ds.basicStorage.Update(
		id,
		storage.DeploymentUpdate{
			UpdatedAt:     &now,
			PendingStatus: &newPending,
		},
		storage.DeploymentFilter{
			// current status must allow deletion
			CurrentStatus: service.DeploymentStatusAllowDeletion,
			PendingStatus: []service.DeploymentPendingStatus{service.DeploymentStatusNoPending},
		})
	if err != nil {
		return false, err
	}
	return ok, nil
}

func (ds DeploymentStorage) deploymentNotUpdatedDueToStatusError() error {
	// If the caller trys to update status on deployment that allows it (1. exists 2. status changes is valid).
	// Then the reason why the status is not updated (but with no error) would be the deployment is altered
	// before last fetch and the update. Either deleted from storage or altered the status.
	return service.NewCacaoGeneralError("deployment no longer exists or has conflict status")
}

// UpdateLastRun sets the last run ID for the deployment.
func (ds DeploymentStorage) UpdateLastRun(id common.ID, run common.ID) error {
	now := ds.getTime()
	newLastRun := &run
	ok, err := ds.basicStorage.Update(
		id,
		storage.DeploymentUpdate{
			UpdatedAt: &now,
			LastRun:   &newLastRun,
		},
		storage.DeploymentFilter{})
	if err != nil {
		return err
	}
	if !ok {
		return service.NewCacaoNotFoundError("deployment not found")
	}
	return nil
}

// StartDeletionWorkflow ...
func (ds DeploymentStorage) StartDeletionWorkflow(id common.ID, tid common.TransactionID, wfName string, awmProvider common.ID) error {
	updateFields := storage.DeploymentUpdate{
		Deletion: &deploymentcommon.DeploymentDeletion{
			Transaction:        tid,
			DeploymentWorkflow: deploymentcommon.NewDeploymentWorkflow(deploymentcommon.DeletionPurpose, wfName, awmProvider),
		},
	}
	// only update if current status allows deletion AND pending status is deleting.
	filter := storage.DeploymentFilter{
		CurrentStatus: service.DeploymentStatusAllowDeletion,
		PendingStatus: []service.DeploymentPendingStatus{service.DeploymentStatusDeleting},
	}
	ok, err := ds.basicStorage.Update(id, updateFields, filter)
	if err != nil {
		return err
	}
	if !ok {
		return ds.deploymentNotUpdatedDueToStatusError()
	}
	return nil
}

func getTimeNow() time.Time {
	return time.Now().UTC()
}

func validateDeploymentStatus(status service.DeploymentStatus, pendingStatus service.DeploymentPendingStatus) error {
	if !status.Valid() {
		return service.NewCacaoGeneralError("deployment has invalid current status")
	}
	if !pendingStatus.Valid() {
		return service.NewCacaoGeneralError("deployment has invalid pending status")
	}
	if !status.ValidPendingStatus(pendingStatus) {
		return service.NewCacaoGeneralError("deployment has invalid pending status")
	}
	return nil
}
