package adapters

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/storage"
	"time"
)

// BasicRunStorage is a basic storage interface for deployment run that supports basic operation (CRUD).
type BasicRunStorage interface {
	Init() error
	Get(runID common.ID) (deploymentcommon.DeploymentRun, error)
	GetLatestRun(deploymentID common.ID) (*deploymentcommon.DeploymentRun, error)
	List(filter storage.DeploymentRunFilter, pagination service.RequestPagination) ([]deploymentcommon.DeploymentRun, error)
	Create(run deploymentcommon.DeploymentRun) (runID common.ID, err error)
	Update(id common.ID, run storage.DeploymentRunUpdate, filter storage.DeploymentRunFilter) (bool, error)
	Delete(runID common.ID) error
	DeleteAllRunByDeployment(deployment common.ID) error
}

// RunStorage is a middleware that implements an abstraction that suits the need of this microservice on top of
// basic storage operation (CRUD).
// It also enforces some semantics on the object.
type RunStorage struct {
	basicStorage BasicRunStorage
	getTime      func() time.Time
}

// NewRunStorage ...
func NewRunStorage(basicStorage BasicRunStorage) *RunStorage {
	return &RunStorage{basicStorage: basicStorage, getTime: getTimeNow}
}

// Init ...
func (rs RunStorage) Init() error {
	return rs.basicStorage.Init()
}

// Get ...
func (rs RunStorage) Get(runID common.ID) (deploymentcommon.DeploymentRun, error) {
	return rs.basicStorage.Get(runID)
}

// GetLatestRun ...
func (rs RunStorage) GetLatestRun(deployment common.ID) (*deploymentcommon.DeploymentRun, error) {
	return rs.basicStorage.GetLatestRun(deployment)
}

// DeploymentHasRun checks if a deployment has any runs.
func (rs RunStorage) DeploymentHasRun(deployment common.ID) (bool, error) {
	list, err := rs.basicStorage.List(storage.DeploymentRunFilter{
		Deployment: deployment,
	}, service.RequestPagination{
		Offset:        0,
		PageSizeLimit: -1,
	})
	if err != nil {
		return false, err
	}
	if len(list) == 0 {
		return false, nil
	}
	return true, nil
}

// SearchByWorkflow searches for a run that matches with a workflow. The workflow is launched by the deployment run.
func (rs RunStorage) SearchByWorkflow(wfName string) (deploymentcommon.DeploymentRun, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "RunStorage.SearchByWorkflow",
		"wfName":   wfName,
	})
	list, err := rs.basicStorage.List(
		storage.DeploymentRunFilter{
			WorkflowName: wfName,
		},
		service.RequestPagination{
			Offset:        0,
			PageSizeLimit: -1,
		},
	)
	if err != nil {
		return deploymentcommon.DeploymentRun{}, err
	}
	if len(list) == 0 {
		return deploymentcommon.DeploymentRun{}, service.NewCacaoNotFoundError("no deployment run matched workflow name")
	} else if len(list) > 1 {
		// shouldn't happen normally, likely due to collision on workflow name
		logger.WithFields(log.Fields{
			"runs":   list,
			"wfName": wfName,
		}).Error("more than 1 run has the same workflow name")
	}

	// uses the first in the list for now.
	return list[0], nil
}

// Create ...
func (rs RunStorage) Create(run deploymentcommon.DeploymentRun) (runID common.ID, err error) {
	return rs.basicStorage.Create(run)
}

// StartPreflightWorkflow ...
func (rs RunStorage) StartPreflightWorkflow(runID common.ID, wfName string, awmProvider common.ID) error {
	// fetch the run
	run, err := rs.basicStorage.Get(runID)
	if err != nil {
		return err
	}

	// save workflow name
	run.AddWorkflow(deploymentcommon.NewDeploymentWorkflow(
		deploymentcommon.PreflightPurpose,
		wfName,
		awmProvider,
	))

	// update run status
	run.Status = deploymentcommon.DeploymentRunPreflight

	update := storage.DeploymentRunUpdate{
		Status:    &run.Status,
		Workflows: run.Workflows,
	}
	ok, err := rs.basicStorage.Update(runID, update, storage.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !ok {
		return service.NewCacaoNotFoundError("deployment run not found")
	}
	return nil
}

// FailedToStartPreflight ...
func (rs RunStorage) FailedToStartPreflight(runID common.ID, preflightErr error) error {
	newRunStatus := deploymentcommon.DeploymentRunErrored
	runEndsAt := rs.getTime()
	runStatusMsg := preflightErr.Error()
	var updateRun = storage.DeploymentRunUpdate{
		Status:    &newRunStatus,
		StatusMsg: &runStatusMsg,
		EndsAt:    &runEndsAt,
	}
	ok, err := rs.basicStorage.Update(runID, updateRun, storage.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !ok {
		return rs.runNotFoundError(runID)
	}
	return nil
}

// PreflightWorkflowSucceed ...
func (rs RunStorage) PreflightWorkflowSucceed(runID common.ID, wfName, wfStatus string) error {
	// fetch run to obtain list of workflows and end-date the matched workflow.
	run, err := rs.basicStorage.Get(runID)
	if err != nil {
		return err
	}
	err = run.EndWorkflow(wfName, wfStatus)
	if err != nil {
		return err
	}

	now := rs.getTime()
	newRunStatus := deploymentcommon.DeploymentRunErrored
	ok, err := rs.basicStorage.Update(runID, storage.DeploymentRunUpdate{
		EndsAt:    &now,
		Status:    &newRunStatus,
		Logs:      nil, // TODO add logs
		Workflows: run.Workflows,
	}, storage.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !ok {
		return rs.runNotFoundError(runID)
	}
	return nil
}

// PreflightWorkflowFailed ...
func (rs RunStorage) PreflightWorkflowFailed(runID common.ID, wfName, wfStatus string) error {
	// fetch run to obtain list of workflows and end-date the matched workflow.
	run, err := rs.basicStorage.Get(runID)
	if err != nil {
		return err
	}
	err = run.EndWorkflow(wfName, wfStatus)
	if err != nil {
		return err
	}

	now := rs.getTime()
	newRunStatus := deploymentcommon.DeploymentRunErrored
	newRunStatusMsg := fmt.Sprintf("workflow %s failed, status %s", wfName, wfStatus)
	ok, err := rs.basicStorage.Update(runID, storage.DeploymentRunUpdate{
		EndsAt:    &now,
		Status:    &newRunStatus,
		StatusMsg: &newRunStatusMsg,
		Logs:      nil, // TODO add logs
		Workflows: run.Workflows,
	}, storage.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !ok {
		return rs.runNotFoundError(runID)
	}
	return nil
}

// StartExecWorkflow ...
func (rs RunStorage) StartExecWorkflow(runID common.ID, wfName string, awmProvider common.ID) error {
	// fetch the run
	run, err := rs.basicStorage.Get(runID)
	if err != nil {
		return err
	}

	// save workflow name
	run.AddWorkflow(deploymentcommon.NewDeploymentWorkflow(
		deploymentcommon.ExecutionPurpose,
		wfName,
		awmProvider,
	))

	// update run status
	run.Status = deploymentcommon.DeploymentRunRunning

	update := storage.DeploymentRunUpdate{
		Status:    &run.Status,
		Workflows: run.Workflows,
	}
	ok, err := rs.basicStorage.Update(runID, update, storage.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !ok {
		return service.NewCacaoNotFoundError("deployment run not found")
	}
	return nil
}

// FailToStartExecWorkflow ...
func (rs RunStorage) FailToStartExecWorkflow(runID common.ID, execErr error) error {
	runStatus := deploymentcommon.DeploymentRunErrored
	runEndsAt := rs.getTime()
	runStatusMsg := execErr.Error()
	update := storage.DeploymentRunUpdate{
		EndsAt:    &runEndsAt,
		Status:    &runStatus,
		StatusMsg: &runStatusMsg,
	}
	ok, err := rs.basicStorage.Update(runID, update, storage.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !ok {
		return service.NewCacaoNotFoundError("deployment run not found")
	}
	return nil
}

// ExecWorkflowSucceed ...
func (rs RunStorage) ExecWorkflowSucceed(
	runID common.ID,
	wfName string,
	wfStatus string,
	newRunStatus deploymentcommon.DeploymentRunStatus,
	rawState *deploymentcommon.RawDeploymentState,
	stateView deploymentcommon.DeploymentStateView,
) error {
	run, err := rs.basicStorage.Get(runID)
	if err != nil {
		return err
	}

	err = run.EndWorkflow(wfName, wfStatus)
	if err != nil {
		return rs.workflowNotInRun(runID, err)
	}

	now := rs.getTime()
	update := storage.DeploymentRunUpdate{
		EndsAt:         &now,
		Status:         &newRunStatus,
		LastState:      &stateView,
		StateUpdatedAt: &now,
		RawState:       &rawState,
		Logs:           nil, // TODO add logs
		Workflows:      run.Workflows,
	}
	ok, err := rs.basicStorage.Update(runID, update, storage.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !ok {
		return rs.runNotFoundError(runID)
	}
	return nil
}

// ExecWorkflowFailed ...
func (rs RunStorage) ExecWorkflowFailed(
	runID common.ID,
	wfName string,
	wfStatus string,
) error {
	run, err := rs.basicStorage.Get(runID)
	if err != nil {
		return err
	}

	err = run.EndWorkflow(wfName, wfStatus)
	if err != nil {
		return rs.workflowNotInRun(runID, err)
	}

	now := rs.getTime()
	newRunStatus := deploymentcommon.DeploymentRunErrored
	newRunStatusMsg := fmt.Sprintf("workflow %s failed, status %s", wfName, wfStatus)
	update := storage.DeploymentRunUpdate{
		EndsAt:    &now,
		Status:    &newRunStatus,
		StatusMsg: &newRunStatusMsg,
		Logs:      nil, // TODO add logs
		Workflows: run.Workflows,
	}
	ok, err := rs.basicStorage.Update(runID, update, storage.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !ok {
		return rs.runNotFoundError(runID)
	}
	return nil
}

// if workflow does not belong to run, change run status to errored
func (rs RunStorage) workflowNotInRun(runID common.ID, parentErr error) error {
	now := rs.getTime()
	runStatus := deploymentcommon.DeploymentRunErrored
	ok, err := rs.basicStorage.Update(runID, storage.DeploymentRunUpdate{
		EndsAt: &now,
		Status: &runStatus,
	}, storage.DeploymentRunFilter{})
	if err != nil {
		return fmt.Errorf("%w, %s", parentErr, err.Error())
	}
	if !ok {
		return rs.runNotFoundError(runID)
	}
	return parentErr
}

func (rs RunStorage) runNotFoundError(runID common.ID) error {
	return service.NewCacaoNotFoundError(fmt.Sprintf("run %s not found", runID))
}
