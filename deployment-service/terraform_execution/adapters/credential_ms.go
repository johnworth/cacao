package adapters

import (
	"context"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// CredentialMicroservice ...
type CredentialMicroservice struct {
	stanConf types.StanConfig
}

// NewCredentialMicroservice ...
func NewCredentialMicroservice(stanConf types.StanConfig) *CredentialMicroservice {
	return &CredentialMicroservice{stanConf: stanConf}
}

// Get get a credential by ID from credential microservice.
// Note: the combination of username and cred ID is unique. The ID alone is Not.
func (c CredentialMicroservice) Get(actor types.Actor, owner, id string) (service.Credential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.Get",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
		"cred":     id,
	})
	if actor.Actor == "" {
		return nil, service.NewCacaoInvalidParameterError("actor is empty")
	}
	if actor.Actor != owner {
		// for now assume actor == owner, meaning user can only query their own credential without emulation.
		return nil, service.NewCacaoUnauthorizedError("actor != username")
	}
	client, err := service.NewNatsCredentialClient(context.TODO(), actor.Actor, actor.Emulator, c.stanConf.NatsConfig, c.stanConf.StanConfig)
	if err != nil {
		errorMessage := "fail to create credential svc client"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoCommunicationError(errorMessage)
	}
	cred, err := client.Get(id)
	if err != nil {
		errorMessage := "fail to get credential"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoGeneralError(errorMessage)
	}
	logger.Trace("got cred from cred ms")
	return cred, nil
}

// List returns a list of all credentials owned by a user.
func (c CredentialMicroservice) List(actor types.Actor, owner string) ([]service.Credential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.List",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
		"owner":    owner,
	})
	if actor.Actor == "" {
		return nil, service.NewCacaoInvalidParameterError("actor is empty")
	}
	if actor.Actor != owner {
		// for now assume actor == owner, meaning user can only query their own credential without emulation.
		return nil, service.NewCacaoUnauthorizedError("actor != username")
	}
	client, err := service.NewNatsCredentialClient(context.TODO(), actor.Actor, actor.Emulator, c.stanConf.NatsConfig, c.stanConf.StanConfig)
	if err != nil {
		errorMessage := "fail to create credential svc client"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoCommunicationError(errorMessage)
	}
	credentials, err := client.List()
	if err != nil {
		errorMessage := "fail to list credentials"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoGeneralError(errorMessage)
	}
	logger.Trace("got credentials from cred ms")
	return credentials, nil
}
