package adapters

import (
	"context"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// ArgoWorkflowMediator is a client to interact with ArgoWorkflowMediator
type ArgoWorkflowMediator struct {
	stanConf types.StanConfig
}

// NewArgoWorkflowMediator creates a new ArgoWorkflowMediator
func NewArgoWorkflowMediator(stanConf types.StanConfig) ArgoWorkflowMediator {
	stanConf.ClientID = stanConf.ClientID + "-" + xid.New().String()
	return ArgoWorkflowMediator{
		stanConf: stanConf,
	}
}

// Create perform sync workflow creation
func (awm ArgoWorkflowMediator) Create(
	ctx context.Context,
	provider common.ID,
	username string,
	workflowFilename string,
	opts ...awmclient.AWMWorkflowCreateOpt) (wfName string, err error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "ArgoWorkflowMediator.Create",
		"provider": provider,
		"username": username,
		"wf":       workflowFilename,
	})
	logger.Trace()
	logger.Warn("Implement ArgoWorkflowMediator adapter")

	client := awmclient.NewArgoWorkflowMediator(awm.stanConf.NatsConfig, awm.stanConf.StanConfig)
	return client.Create(ctx, awmclient.AWMProvider(provider.String()), username, workflowFilename, opts...)
}

// CreateAsync perform async creation
func (awm ArgoWorkflowMediator) CreateAsync(
	provider common.ID,
	username string,
	workflowFilename string,
	opts ...awmclient.AWMWorkflowCreateOpt) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "ArgoWorkflowMediator.CreateAsync",
		"provider": provider,
		"username": username,
		"wf":       workflowFilename,
	})
	logger.Trace()
	logger.Warn("Implement ArgoWorkflowMediator adapter")

	client := awmclient.NewArgoWorkflowMediator(awm.stanConf.NatsConfig, awm.stanConf.StanConfig)
	return client.CreateAsync(awmclient.AWMProvider(provider.String()), username, workflowFilename, opts...)
}
