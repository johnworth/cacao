package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// ProviderMicroservice is a client to provider microservice
type ProviderMicroservice struct {
	stanConf types.StanConfig
}

// NewProviderMicroservice ...
func NewProviderMicroservice(stanConf types.StanConfig) *ProviderMicroservice {
	return &ProviderMicroservice{stanConf: stanConf}
}

// Get fetch a provider by ID
func (p ProviderMicroservice) Get(actor types.Actor, id common.ID) (service.Provider, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "ProviderMicroservice.Get",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
		"provider": id,
	})
	client, err := service.NewNatsProviderClient(context.TODO(), actor.Actor, actor.Emulator, p.stanConf.NatsConfig, p.stanConf.StanConfig)
	if err != nil {
		logger.WithError(err).Error("fail to create provider svc client")
		return nil, err
	}
	provider, err := client.Get(id)
	if err != nil {
		logger.WithError(err).Error("fail to get provider")
		return nil, err
	}
	logger.WithFields(log.Fields{
		"name": provider.GetName(),
		"type": provider.GetType(),
		"url":  provider.GetURL(),
	}).Trace("got provider from provider ms")
	return provider, nil
}

// List fetches a list of provider
func (p ProviderMicroservice) List(actor types.Actor) ([]service.Provider, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "ProviderMicroservice.List",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
	})
	client, err := service.NewNatsProviderClient(context.TODO(), actor.Actor, actor.Emulator, p.stanConf.NatsConfig, p.stanConf.StanConfig)
	if err != nil {
		logger.WithError(err).Error("fail to create provider svc client")
		return nil, err
	}
	providerList, err := client.List()
	if err != nil {
		logger.WithError(err).Error("fail to list providers")
		return nil, err
	}
	logger.WithFields(log.Fields{
		"len": len(providerList),
	}).Trace("got provider list from provider ms")
	return providerList, nil
}
