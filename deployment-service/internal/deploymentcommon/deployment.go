package deploymentcommon

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// Creator is the creator of deployments
type Creator struct {
	User     string `bson:"creator"`
	Emulator string `bson:"creator_emulator"`
}

// DeploymentDeletion stores deletion related data for a deployment
type DeploymentDeletion struct {
	Deleting           bool                 `bson:"deleting"`
	Transaction        common.TransactionID `bson:"transaction"`
	DeploymentWorkflow `bson:",inline"`
}

// CredentialID is the ID of credential
type CredentialID string

// String ...
func (c CredentialID) String() string {
	return string(c)
}

// ProviderCredentialPair is a mapping between a cloud provider and a cloud credential.
type ProviderCredentialPair struct {
	Credential CredentialID `bson:"credential"`
	Provider   common.ID    `bson:"provider"`
}

// ProviderCredentialMappings is a list of mappings between cloud providers and cloud credentials.
type ProviderCredentialMappings []ProviderCredentialPair

// ConvertToExternal converts to map[credID]providerID
func (mapping ProviderCredentialMappings) ConvertToExternal() map[string]common.ID {
	var result = map[string]common.ID{}
	for _, pair := range mapping {
		result[pair.Credential.String()] = pair.Provider
	}
	return result
}

// Deployment is the storage format of deployment
type Deployment struct {
	ID          common.ID `bson:"_id"`
	Name        string    `bson:"name"`
	Description string    `bson:"description"`
	CreatedAt   time.Time `bson:"created_at"`
	UpdatedAt   time.Time `bson:"updated_at"`
	Workspace   common.ID `bson:"workspace"`
	// user who created the deployment
	CreatedBy            Creator                         `bson:",inline"`
	Template             common.ID                       `bson:"template"`
	TemplateType         string                          `bson:"template_type"`
	PrimaryCloudProvider common.ID                       `bson:"primary_provider"`
	CurrentStatus        service.DeploymentStatus        `bson:"current_status"`
	PendingStatus        service.DeploymentPendingStatus `bson:"pending_status"`
	StatusMsg            string                          `bson:"status_msg"`
	CloudCredentials     ProviderCredentialMappings      `bson:"cloud_credentials"`
	GitCredential        CredentialID                    `bson:"git_credential"`
	// most recent run sorted by creation timestamp
	LastRun  *common.ID         `bson:"last_run"`
	Deletion DeploymentDeletion `bson:"deletion"`
}

// Owner returns the owner of the deployment.
// Currently owner is the creator.
func (d Deployment) Owner() string {
	return d.CreatedBy.User
}

// HasLastRun returns true if the deployment has a last run
func (d Deployment) HasLastRun() bool {
	if d.LastRun != nil && *d.LastRun != "" {
		return true
	}
	return false
}

// FindCloudCredential find cloud credential for a provider if any.
func (d Deployment) FindCloudCredential(provider common.ID) (CredentialID, bool) {
	for _, pair := range d.CloudCredentials {
		if pair.Provider == provider {
			return pair.Credential, true
		}
	}
	return "", false
}

// ConvertToExternal converts to external representation.
// Note: this is the responsibility of the caller to ensure they pass in the correct lastRun.
func (d Deployment) ConvertToExternal(lastRun DeploymentRun) service.Deployment {
	var serviceLastRun = lastRun.ConvertToExternal()
	return service.Deployment{
		ID:               d.ID,
		Name:             d.Name,
		Description:      d.Description,
		CreatedAt:        d.CreatedAt,
		UpdatedAt:        d.UpdatedAt,
		Owner:            d.Owner(),
		Workspace:        d.Workspace,
		Template:         d.Template,
		TemplateType:     d.TemplateType,
		PrimaryCloud:     d.PrimaryCloudProvider,
		CurrentStatus:    d.CurrentStatus,
		PendingStatus:    d.PendingStatus,
		StatusMsg:        d.StatusMsg,
		CloudCredentials: d.CloudCredentials.ConvertToExternal(),
		GitCredential:    d.GitCredential.String(),
		LastRun:          &serviceLastRun,
	}
}

const (
	// DeploymentNameMaxLength is max length for name
	DeploymentNameMaxLength = 100
	// DeploymentDescriptionMaxLength is max length for description
	DeploymentDescriptionMaxLength = 255
	// DeploymentStatusMsgMaxLength is max length for status msg
	DeploymentStatusMsgMaxLength = 255
)

// DeploymentRunStatus is the status of a deployment run
type DeploymentRunStatus string

// String ...
func (status DeploymentRunStatus) String() string {
	return string(status)
}

const (
	// DeploymentRunPreflight ...
	DeploymentRunPreflight DeploymentRunStatus = "pre-flight"
	// DeploymentRunRunning ...
	DeploymentRunRunning DeploymentRunStatus = "running"
	// DeploymentRunActive ...
	DeploymentRunActive DeploymentRunStatus = "active"
	// DeploymentRunErrored ...
	DeploymentRunErrored DeploymentRunStatus = "errored"
)

// DeploymentParameters ...
type DeploymentParameters []DeploymentParameter

// ConvertToExternal converts to external representation
func (dp DeploymentParameters) ConvertToExternal() []service.DeploymentParameter {
	external := make([]service.DeploymentParameter, 0)
	for _, param := range dp {
		external = append(external, param.ConvertToExternal())
	}
	return external
}

// ToParameterValues convert parameters to parameter values
func (dp DeploymentParameters) ToParameterValues() service.DeploymentParameterValues {
	if len(dp) == 0 {
		return map[string]interface{}{}
	}
	var paramValues = make(map[string]interface{})
	for _, param := range []DeploymentParameter(dp) {
		paramValues[param.Name] = param.Value
	}
	return paramValues
}

// DeploymentParameter contain the type and value of a parameter to deployment
type DeploymentParameter struct {
	Name  string      `bson:"name"`
	Type  string      `bson:"type"` // int,string,bool,float,uuid,url,secret, etc.
	Value interface{} `bson:"value"`
}

// ConvertToExternal converts to external representation
func (dp DeploymentParameter) ConvertToExternal() service.DeploymentParameter {
	return service.DeploymentParameter{
		Name:  dp.Name,
		Type:  dp.Type,
		Value: dp.Value,
	}
}

// RawStateType is type of raw state
type RawStateType string

// RawDeploymentState is the raw state of a deployment (run)
type RawDeploymentState struct {
	Type    RawStateType    `bson:"type"`
	TFState *TerraformState `bson:"terraform"`
	//State   interface{}     `bson:"state,omitempty"`
}

// DeploymentRun ...
type DeploymentRun struct {
	// ID of the deployment run
	ID               common.ID            `bson:"_id"`
	Deployment       common.ID            `bson:"deployment"`
	CreatedBy        Creator              `bson:",inline"`
	CreatedAt        time.Time            `bson:"created_at"`
	EndsAt           time.Time            `bson:"ended_at"`
	TemplateSnapshot TemplateSnapshot     `bson:"template"`
	Parameters       DeploymentParameters `bson:"parameters"`
	Status           DeploymentRunStatus  `bson:"status"`
	StatusMsg        string               `bson:"status_msg"`
	// most recent state
	LastState      DeploymentStateView `bson:"last_state"`
	StateUpdatedAt time.Time           `bson:"state_updated_at"`
	RawState       *RawDeploymentState `bson:"raw_state"`
	Logs           *string             `bson:"logs"`
	// argo workflow that execute the template
	Workflows []DeploymentWorkflow `bson:"wfs"`
	// TODO implement history later
	Histories []DeploymentHistory `bson:"histories"`
}

// HasRawState returns true if the run has raw state available
func (r DeploymentRun) HasRawState() bool {
	return r.RawState != nil
}

// HasLogs returns true if the run has logs available
func (r DeploymentRun) HasLogs() bool {
	return r.Logs != nil
}

// AddWorkflow adds a workflow to the deployment run
func (r *DeploymentRun) AddWorkflow(wf DeploymentWorkflow) {
	if r.Workflows == nil {
		r.Workflows = []DeploymentWorkflow{wf}
		return
	}
	r.Workflows = append(r.Workflows, wf)
}

// FindWorkflow finds a workflow within the run with the given workflow name
func (r *DeploymentRun) FindWorkflow(workflowName string) (DeploymentWorkflow, error) {
	if r.Workflows == nil {
		return DeploymentWorkflow{}, service.NewCacaoNotFoundError("workflow not found in run")
	}
	for _, wf := range r.Workflows {
		if wf.WorkflowName == workflowName {
			return wf, nil
		}
	}
	return DeploymentWorkflow{}, service.NewCacaoNotFoundError("workflow not found in run")
}

// EndWorkflow ends a workflow. Return error when workflow is not found in the run.
func (r *DeploymentRun) EndWorkflow(wfName, wfStatus string) error {
	for index, wf := range r.Workflows {
		if wf.WorkflowName == wfName {
			r.Workflows[index].Status = wfStatus
			r.Workflows[index].EndDate()
			return nil
		}
	}
	return service.NewCacaoNotFoundError("workflow not found in run")
}

// ConvertToExternal converts to external representation
func (r DeploymentRun) ConvertToExternal() service.DeploymentRun {
	return service.DeploymentRun{
		ID:               r.ID,
		Deployment:       r.Deployment,
		Owner:            r.CreatedBy.User,
		Start:            r.CreatedAt,
		End:              r.EndsAt,
		GitURL:           r.TemplateSnapshot.GitURL,
		CommitHash:       r.TemplateSnapshot.CommitHash,
		Parameters:       r.Parameters.ConvertToExternal(),
		CloudCredentials: nil,
		GitCredential:    "",
		Status:           r.Status.String(),
		StatusMsg:        r.StatusMsg,
		LastState:        r.LastState.ConvertToExternal(),
		StateUpdatedAt:   r.StateUpdatedAt,
	}
}

// TemplateSnapshot is a snapshot in time of a template.
// It contains info that can be used to identify a specific instance template.
type TemplateSnapshot struct {
	TemplateID      common.ID `bson:"template_id"`
	UpstreamTracked struct {
		Branch string `bson:"branch"`
		Tag    string `bson:"tag"`
	} `bson:",inline"`
	GitURL     string `bson:"git_url"`
	CommitHash string `bson:"git_commit"`
	// Path within the git repo, empty means root dir of repo
	SubPath string `bson:"sub_path"`
}

// WorkflowPurpose is the purpose of which the workflow is intended to achieve
type WorkflowPurpose string

const (
	// PreflightPurpose is a workflow launched during preflight stage, e.g. to apply prerequisite template.
	PreflightPurpose WorkflowPurpose = "preflight"
	// ExecutionPurpose is a workflow launched during execution stage, e.g. to execute the actual template.
	ExecutionPurpose WorkflowPurpose = "execution"
	// DeletionPurpose is a workflow launched to delete the resources provisioned by a deployment.
	DeletionPurpose WorkflowPurpose = "deletion"
)

// DeploymentWorkflow is a record of launched workflow for deployment purpose.
type DeploymentWorkflow struct {
	Purpose      WorkflowPurpose `bson:"purpose"`
	WorkflowName string          `bson:"wf_name"`
	AWMProvider  common.ID       `bson:"provider"`
	// TODO implement this after AWM includes creation timestamp in the emit events
	//CreatedAt    time.Time `bson:"created_at"`
	EndedAt time.Time `bson:"ended_at"`
	Status  string    `bson:"status"`
}

// NewDeploymentWorkflow creates a new DeploymentWorkflow record.
// Only fields that are required upon creation is passed as param here.
func NewDeploymentWorkflow(purpose WorkflowPurpose, workflowName string, AWMProvider common.ID) DeploymentWorkflow {
	// TODO add creation timestamp after AWM includes it in the emit events
	return DeploymentWorkflow{Purpose: purpose, WorkflowName: workflowName, AWMProvider: AWMProvider}
}

// IsSet returns true if the workflow has been set (not empty).
func (wf DeploymentWorkflow) IsSet() bool {
	return len(wf.WorkflowName) > 0
}

// Ended returns true if the workflow has been end-dated (EndedAt not zero).
func (wf DeploymentWorkflow) Ended() bool {
	return !wf.EndedAt.IsZero()
}

// EndDate populate EndedAt with current time.
func (wf *DeploymentWorkflow) EndDate() {
	wf.EndedAt = time.Now().UTC()
}

// DeploymentHistory is a history record of the state/status of the deployment and its runs.
type DeploymentHistory struct {
	// ID is the id of the history object itself
	ID         common.ID `bson:"_id"`
	Deployment common.ID `bson:"deployment"`
	// The deployment run that this history belongs to
	Run        common.ID            `bson:"run"`
	Timestamp  time.Time            `bson:"timestamp"`
	Parameters DeploymentParameters `bson:"param"`
	StateView  DeploymentStateView  `bson:"state"`
	RawState   *string              `bson:"raw_state"`
	Logs       *string              `bson:"logs"`
}

// HasRawState checks if history has raw state available
func (hist DeploymentHistory) HasRawState() bool {
	return hist.RawState != nil
}

// HasLogs checks if history has logs available
func (hist DeploymentHistory) HasLogs() bool {
	return hist.Logs != nil
}

// DeploymentStateView is a higher level view of raw state
type DeploymentStateView service.DeploymentStateView

// ConvertToExternal converts to external representation
func (sv DeploymentStateView) ConvertToExternal() service.DeploymentStateView {
	return service.DeploymentStateView(sv)
}

// DeploymentStateViewFromExternal converts external representation of state view to internal one.
func DeploymentStateViewFromExternal(state service.DeploymentStateView) DeploymentStateView {
	return DeploymentStateView(state)
}
