package deploymentcommon

// Events defined here are for internal communication between deployment microservices

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
)

// EventDeploymentRunStatusUpdated ...
const EventDeploymentRunStatusUpdated common.EventType = common.EventTypePrefix + "DeploymentRunStatusUpdated"

// DeploymentRunStatusUpdated is event emitted when the status of a deployment run is updated.
// TODO move this to cacao-common
type DeploymentRunStatusUpdated struct {
	service.Session `json:",inline"`
	Deployment      common.ID           `json:"deployment"`
	Run             common.ID           `json:"run"`
	Status          DeploymentRunStatus `json:"status"`
	OldStatus       DeploymentRunStatus `json:"old_status"`
	// For outgoing purpose
	TransactionID common.TransactionID `json:"-"`
}

// EventType ...
func (d DeploymentRunStatusUpdated) EventType() common.EventType {
	return EventDeploymentRunStatusUpdated
}

// Transaction ...
func (d DeploymentRunStatusUpdated) Transaction() common.TransactionID {
	return d.TransactionID
}

// ToCloudEvent ...
func (d DeploymentRunStatusUpdated) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEventWithTransactionID(d, string(EventDeploymentRunStatusUpdated), source, d.TransactionID)
}
