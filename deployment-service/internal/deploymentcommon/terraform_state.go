package deploymentcommon

import (
	"encoding/json"
	"fmt"
	"gitlab.com/cyverse/cacao-common/common"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
)

const (
	// TerraformRawState ...
	TerraformRawState RawStateType = "terraform"
)

// TerraformState is top level struct for Terraform raw state
type TerraformState struct {
	Version          int                    `json:"version" bson:"version" mapstructure:"version"`
	TerraformVersion string                 `json:"terraform_version" bson:"terraform_version" mapstructure:"terraform_version"`
	Serial           int                    `json:"serial" bson:"serial" mapstructure:"serial"`
	Lineage          string                 `json:"lineage" bson:"lineage" mapstructure:"lineage"` // uuid
	Outputs          map[string]interface{} `json:"outputs" bson:"outputs" mapstructure:"outputs"`
	Resources        []TerraformResource    `json:"resources" bson:"resources" mapstructure:"resources"`
}

// Type ...
func (state TerraformState) Type() RawStateType {
	return TerraformRawState
}

// String marshals the state into string, this method will log.Fatal() if marshal failed
func (state TerraformState) String() string {
	bytes, err := json.Marshal(state)
	if err != nil {
		log.Fatal(err)
	}
	return string(bytes)
}

// TerraformResource is the resource in Terraform raw state
type TerraformResource struct {
	Module     string                      `json:"module" bson:"module" mapstructure:"module"`
	Mode       string                      `json:"mode" bson:"mode" mapstructure:"mode"`
	Type       string                      `json:"type" bson:"type" mapstructure:"type"`
	Name       string                      `json:"name" bson:"name" mapstructure:"name"`
	TFProvider string                      `json:"provider" bson:"provider" mapstructure:"provider"` // "provider[\"terraform.cyverse.org/cyverse/openstack\"]"
	Instances  []TerraformResourceInstance `json:"instances" bson:"instances" mapstructure:"instances"`
}

// TerraformResourceInstance is instance of resource in Terraform raw state
type TerraformResourceInstance struct {
	IndexKey            int                    `json:"index_key" bson:"index_key" mapstructure:"index_key"`
	SchemaVersion       int                    `json:"schema_version" bson:"schema_version" mapstructure:"schema_version"`
	Attributes          map[string]interface{} `json:"attributes" bson:"attributes" mapstructure:"attributes"`
	SensitiveAttributes []interface{}          `json:"sensitive_attributes" bson:"sensitive_attributes" mapstructure:"sensitive_attributes"`
	Private             string                 `json:"private" bson:"private" mapstructure:"private"` // base64 encoded
	Dependencies        []string               `json:"dependencies" bson:"dependencies" mapstructure:"dependencies"`
}

// TerraformStateParser is parser for Terraform raw state
type TerraformStateParser struct {
	// temporarily stores the resources that are auxiliary during parsing, e.g. ip association
	auxiliaryResources []TerraformResource
	// Assume all resources in the raw state is provisioned on this provider, and use this provider as the provider in state view.
	// TODO figure out a way to fetch provider from resource or from terraform, rather than making this assumption.
	primaryProvider common.ID
}

// FromJSONString parse Terraform state from JSON string
func (parser TerraformStateParser) FromJSONString(jsonStateStr string) (*TerraformState, error) {
	var tfState TerraformState
	err := json.Unmarshal([]byte(jsonStateStr), &tfState)
	if err != nil {
		return nil, err
	}
	return &tfState, nil
}

// ToStateView converts TerraformState to a state view
func (parser *TerraformStateParser) ToStateView(state TerraformState, primaryProvider common.ID) (service.DeploymentStateView, error) {
	parser.auxiliaryResources = make([]TerraformResource, 0)
	parser.primaryProvider = primaryProvider

	var view service.DeploymentStateView
	allResources := make([]service.DeploymentResource, 0)
	for _, tfResType := range state.Resources {
		if parser.filterAuxiliaryResources(tfResType) {
			continue
		}
		resources, err := parser.parseResource(tfResType)
		if err != nil {
			return view, err
		}
		allResources = append(allResources, resources...)
	}
	allResources, err := parser.attachAuxiliaryResources(allResources)
	if err != nil {
		return view, err
	}
	view.Resources = allResources
	return view, nil
}

// filter out auxiliary resources, and return true if resource is auxiliary
func (parser *TerraformStateParser) filterAuxiliaryResources(resource TerraformResource) bool {
	switch resource.Type {
	case openstackComputeFloatingIPAssociate:
		// since IP association is not a real resource. stores the it for later
		parser.auxiliaryResources = append(parser.auxiliaryResources, resource)
		return true
	default:
		return false
	}
}

func (parser TerraformStateParser) parseResource(tfRes TerraformResource) ([]service.DeploymentResource, error) {
	var resources = make([]service.DeploymentResource, 0)
	for _, tfResInst := range tfRes.Instances {
		res, err := parser.parseResourceInstance(tfRes, tfResInst)
		if err != nil {
			return nil, err
		}
		if res != nil {
			resources = append(resources, *res)
		}
	}
	return resources, nil
}

// might return resource=nil, since some Terraform Resource does not map to a deployment resource (e.g. IP association)
func (parser TerraformStateParser) parseResourceInstance(
	tfRes TerraformResource, tfResInst TerraformResourceInstance) (*service.DeploymentResource, error) {
	var resource service.DeploymentResource
	switch tfRes.Type {
	case openstackComputeInstanceV2:
		var vmInstance OpenstackComputeInstance
		err := mapstructure.Decode(tfResInst.Attributes, &vmInstance)
		if err != nil {
			return nil, err
		}
		resource = service.DeploymentResource{
			ID:                  vmInstance.ID,
			Type:                service.OpenStackInstance,
			ProviderType:        "openstack",
			Provider:            parser.primaryProvider,
			Attributes:          tfResInst.Attributes,
			SensitiveAttributes: nil,
			AvailableActions:    make([]service.DeploymentResourceAction, 0),
		}
	case openstackNetworkingFloatingipV2:
		var floatingIP OpenstackNetworkingFloatingIP
		err := mapstructure.Decode(tfResInst.Attributes, &floatingIP)
		if err != nil {
			return nil, err
		}
		resource = service.DeploymentResource{
			ID:                  floatingIP.ID,
			Type:                service.DeploymentResourceType(tfRes.Type),
			ProviderType:        "openstack",
			Provider:            parser.primaryProvider,
			Attributes:          tfResInst.Attributes,
			SensitiveAttributes: nil,
			AvailableActions:    make([]service.DeploymentResourceAction, 0),
		}
	case openstackComputeFloatingIPAssociate:
		// not a real resource, this should be filtered out
	case openstackBlockstorageVolumeV3:
		var vol OpenstackBlockStorageVolume
		err := mapstructure.Decode(tfResInst.Attributes, &vol)
		if err != nil {
			return nil, err
		}
		resource = service.DeploymentResource{
			ID:                  vol.ID,
			Type:                service.OpenStackVolume,
			ProviderType:        "openstack",
			Provider:            parser.primaryProvider,
			Attributes:          tfResInst.Attributes,
			SensitiveAttributes: nil,
			AvailableActions:    make([]service.DeploymentResourceAction, 0),
		}
	default:
		var resID string
		var idFound bool
		for _, field := range []string{"id", "Id", "ID"} {
			value, ok := tfResInst.Attributes[field]
			if ok {
				str, ok := value.(string)
				if ok {
					resID = str
					idFound = true
					break
				}
			}
		}
		if !idFound {
			log.WithFields(log.Fields{"resType": tfRes.Type,
				"resInstAttr": tfResInst.Attributes}).Warnf("unable to get ID from resource")
			return nil, nil
		}

		resource = service.DeploymentResource{
			ID:                  resID,
			Type:                service.DeploymentResourceType(tfRes.Type),
			ProviderType:        "", // FIXME need to figure out how to get this info
			Provider:            parser.primaryProvider,
			Attributes:          tfResInst.Attributes,
			SensitiveAttributes: nil,
			AvailableActions:    make([]service.DeploymentResourceAction, 0),
		}
	}
	return &resource, nil
}

// attachAuxiliaryResources attaches attributes of auxiliary resources to its related resource  (e.g. attach IP association to VM)
func (parser TerraformStateParser) attachAuxiliaryResources(resources []service.DeploymentResource) ([]service.DeploymentResource, error) {
	for _, auxRes := range parser.auxiliaryResources {
		switch auxRes.Type {
		case openstackComputeFloatingIPAssociate:
			for _, tfResInst := range auxRes.Instances {
				var ipAssociate OpenstackComputeFloatingIPAssociate
				err := mapstructure.Decode(tfResInst.Attributes, &ipAssociate)
				if err != nil {
					return nil, err
				}
				attached := false
				for _, res := range resources {
					if res.Type == service.OpenStackInstance && res.ID == ipAssociate.InstanceID {
						// "floating_ip" is from field tag in cacao-common.service.OpenStackInstanceAttributes
						res.Attributes["floating_ip"] = ipAssociate.FloatingIP
						attached = true
						break
					}
				}
				if !attached {
					// this indicates that the TF state is corrupted, since the instance resource is missing from the state.
					log.WithFields(log.Fields{
						"function":    "TerraformStateParser.attachAuxiliaryResources",
						"floating_ip": ipAssociate.FloatingIP,
						"instance_id": ipAssociate.InstanceID,
					}).Error("TF state corrupted, ip associate resource has nothing to attach to")
					return nil, fmt.Errorf(
						"TF state corrupted, ip associate resource (%s/%s) has nothing to attach to",
						ipAssociate.FloatingIP,
						ipAssociate.InstanceID,
					)
				}
			}
		default:
			panic("unknown aux resource: " + auxRes.Type)
		}
	}
	return resources, nil
}
