package deploymentcommon

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gotest.tools/v3/assert"
	"testing"
)

func TestTerraformStateParser_ToStateView(t *testing.T) {

	type args struct {
		state           TerraformState
		primaryProvider common.ID
	}
	tests := []struct {
		name    string
		args    args
		want    service.DeploymentStateView
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			"",
			args{
				state: TerraformState{
					Version:          0,
					TerraformVersion: "",
					Serial:           0,
					Lineage:          "",
					Outputs:          nil,
					Resources: []TerraformResource{
						{
							Module:     "module.koa",
							Mode:       "managed",
							Type:       openstackComputeInstanceV2,
							Name:       "os_instances",
							TFProvider: "provider[\\\"terraform.cyverse.org/cyverse/openstack\\\"]",
							Instances: []TerraformResourceInstance{
								{
									IndexKey:      0,
									SchemaVersion: 0,
									Attributes: map[string]interface{}{
										"access_ip_v4":            "172.0.0.0",
										"access_ip_v6":            "",
										"admin_pass":              nil,
										"all_metadata":            map[string]interface{}{},
										"all_tags":                []string{},
										"availability_zone":       "nova",
										"availability_zone_hints": nil,
										"block_device":            []interface{}{},
										"config_drive":            nil,
										"flavor_id":               "ffffffff-ffff-ffff-aaaa-aaaaaaaaaaaa",
										"flavor_name":             "tiny1",
										"floating_ip":             nil,
										"force_delete":            false,
										"id":                      "dddddddd-dddd-dddd-dddd-dddddddddddd",
										"image_id":                "aaaaaaaa-aaaa-aaaa-eeee-eeeeeeeeeeee",
										"image_name":              "ubuntu",
										"key_pair":                "myKeyPair",
										"metadata":                nil,
										"name":                    "test_instance",
										"network": []map[string]interface{}{
											{
												"access_network": false,
												"fixed_ip_v4":    "172.0.0.0",
												"fixed_ip_v6":    "",
												"floating_ip":    "",
												"mac":            "aa:aa:aa:aa:cc:cc",
												"name":           "user-api-net",
												"port":           "",
												"uuid":           "eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee",
											},
										},
										"network_mode":        nil,
										"personality":         []interface{}{},
										"power_state":         "active",
										"region":              "Region",
										"scheduler_hints":     []interface{}{},
										"security_groups":     []string{"user-api-sg"},
										"stop_before_destroy": false,
										"tags":                nil,
										"timeouts":            nil,
										"user_data":           nil,
										"vendor_options":      []interface{}{},
										"volume":              []interface{}{},
									},
									SensitiveAttributes: nil,
									Private:             "",
									Dependencies:        nil,
								},
							},
						},
					},
				},
				primaryProvider: common.ID("provider-aaaaaaaaaaaaaaaaaaaa"),
			},
			service.DeploymentStateView{
				Resources: []service.DeploymentResource{
					{
						ID:           "dddddddd-dddd-dddd-dddd-dddddddddddd",
						Type:         service.OpenStackInstance,
						ProviderType: "openstack",
						Provider:     "provider-aaaaaaaaaaaaaaaaaaaa",
						Attributes: map[string]interface{}{
							"access_ip_v4":            "172.0.0.0",
							"access_ip_v6":            "",
							"admin_pass":              nil,
							"all_metadata":            map[string]interface{}{},
							"all_tags":                []string{},
							"availability_zone":       "nova",
							"availability_zone_hints": nil,
							"block_device":            []interface{}{},
							"config_drive":            nil,
							"flavor_id":               "ffffffff-ffff-ffff-aaaa-aaaaaaaaaaaa",
							"flavor_name":             "tiny1",
							"floating_ip":             nil,
							"force_delete":            false,
							"id":                      "dddddddd-dddd-dddd-dddd-dddddddddddd",
							"image_id":                "aaaaaaaa-aaaa-aaaa-eeee-eeeeeeeeeeee",
							"image_name":              "ubuntu",
							"key_pair":                "myKeyPair",
							"metadata":                nil,
							"name":                    "test_instance",
							"network": []map[string]interface{}{
								{
									"access_network": false,
									"fixed_ip_v4":    "172.0.0.0",
									"fixed_ip_v6":    "",
									"floating_ip":    "",
									"mac":            "aa:aa:aa:aa:cc:cc",
									"name":           "user-api-net",
									"port":           "",
									"uuid":           "eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee",
								},
							},
							"network_mode":        nil,
							"personality":         []interface{}{},
							"power_state":         "active",
							"region":              "Region",
							"scheduler_hints":     []interface{}{},
							"security_groups":     []string{"user-api-sg"},
							"stop_before_destroy": false,
							"tags":                nil,
							"timeouts":            nil,
							"user_data":           nil,
							"vendor_options":      []interface{}{},
							"volume":              []interface{}{},
						},
						SensitiveAttributes: nil,
						AvailableActions:    []service.DeploymentResourceAction{},
					},
				},
			},
			false,
		},
		{
			"1instance+1ip",
			args{
				state: TerraformState{
					Version:          4,
					TerraformVersion: "0.14.4",
					Serial:           3,
					Lineage:          "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
					Outputs:          map[string]interface{}{},
					Resources: []TerraformResource{
						{
							Module:     "module.cacao",
							Mode:       "managed",
							Type:       "openstack_compute_floatingip_associate_v2",
							Name:       "os_floatingips_associate",
							TFProvider: "provider[\"terraform.cyverse.org/cyverse/openstack\"]",
							Instances: []TerraformResourceInstance{
								{
									IndexKey:      0,
									SchemaVersion: 0,
									Attributes: map[string]interface{}{
										"timeouts":              nil,
										"wait_until_associated": nil,
										"fixed_ip":              "",
										"floating_ip":           "8.8.8.8",
										"id":                    "8.8.8.8/dddddddd-dddd-dddd-dddd-d1234ddddddd/",
										"instance_id":           "dddddddd-dddd-dddd-dddd-d1234ddddddd",
										"region":                "RegionOne",
									},
									SensitiveAttributes: []interface{}{},
									Private:             "aaaaaaaaaaa",
									Dependencies: []string{
										"module.cacao.openstack_compute_instance_v2.os_instances",
										"module.cacao.openstack_networking_floatingip_v2.os_floatingips",
									},
								},
							},
						},
						{
							Module:     "module.cacao",
							Mode:       "managed",
							Type:       "openstack_compute_instance_v2",
							Name:       "os_instances",
							TFProvider: "provider[\"terraform.cyverse.org/cyverse/openstack\"]",
							Instances: []TerraformResourceInstance{
								{
									IndexKey:      0,
									SchemaVersion: 0,
									Attributes: map[string]interface{}{
										"personality":             []interface{}{},
										"all_tags":                []interface{}{},
										"region":                  "region123",
										"admin_pass":              nil,
										"access_ip_v4":            "10.0.0.1",
										"tags":                    nil,
										"availability_zone_hints": nil,
										"network_mode":            nil,
										"network": []map[string]interface{}{
											{
												"port":           "",
												"uuid":           "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
												"access_network": false,
												"fixed_ip_v4":    "10.0.0.1",
												"fixed_ip_v6":    "",
												"floating_ip":    "",
												"mac":            "aa:aa:aa:aa:aa:aa",
												"name":           "cacao-admin-api-net",
											},
										},
										"stop_before_destroy": false,
										"name":                "tf-test",
										"id":                  "dddddddd-dddd-dddd-dddd-d1234ddddddd",
										"flavor_id":           "faaaaaaa-aaaa-aaaa-aaaa-aaaaaaaa1234",
										"all_metadata":        map[string]interface{}{},
										"block_device":        []interface{}{},
										"volume":              []interface{}{},
										"vendor_options":      []interface{}{},
										"access_ip_v6":        "",
										"power_state":         "active",
										"user_data":           nil,
										"image_id":            "aaaaaaaa-aaaa-aaaa-aaaa-a1234aaaaaaa",
										"flavor_name":         "tiny1",
										"key_pair":            "my-keypair",
										"timeouts":            nil,
										"floating_ip":         nil,
										"image_name":          "ubuntu-20.04",
										"availability_zone":   "nova",
										"scheduler_hints":     []interface{}{},
										"security_groups": []string{
											"cacao-admin-api-sg",
										},
										"metadata":     nil,
										"config_drive": nil,
										"force_delete": false,
									},
									SensitiveAttributes: []interface{}{},
									Private:             "aaaaaaaaaaa",
									Dependencies:        []string{},
								},
							},
						},
						{
							Module:     "module.cacao",
							Mode:       "managed",
							Type:       "openstack_networking_floatingip_v2",
							Name:       "os_floatingips",
							TFProvider: "provider[\"terraform.cyverse.org/cyverse/openstack\"]",
							Instances: []TerraformResourceInstance{
								{
									IndexKey:      0,
									SchemaVersion: 0,
									Attributes: map[string]interface{}{
										"pool":        "ext-net",
										"dns_name":    "",
										"value_specs": nil,
										"region":      "region123",
										"id":          "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbb1234",
										"all_tags":    []interface{}{},
										"tags":        []interface{}{},
										"fixed_ip":    "",
										"port_id":     "",
										"description": "floating ip for tf-test, 0/1",
										"address":     "8.8.8.8",
										"timeouts":    nil,
										"tenant_id":   "aaaaaaaaaaaaaaaaaaaaccccccccc123",
										"subnet_id":   "aaaaaaaa-aaaa-aaaa-aaaa-bbbbbbbbb123",
										"dns_domain":  "",
									},
									SensitiveAttributes: []interface{}{},
									Private:             "aaaaaaaaaaa",
									Dependencies:        []string{},
								},
							},
						},
					},
				},
				primaryProvider: common.ID("provider-aaaaaaaaaaaaaaaaaaaa"),
			},
			service.DeploymentStateView{
				Resources: []service.DeploymentResource{
					{
						ID:           "dddddddd-dddd-dddd-dddd-d1234ddddddd",
						Type:         service.OpenStackInstance,
						ProviderType: "openstack",
						Provider:     "provider-aaaaaaaaaaaaaaaaaaaa",
						Attributes: map[string]interface{}{
							"personality":             []interface{}{},
							"all_tags":                []interface{}{},
							"region":                  "region123",
							"admin_pass":              nil,
							"access_ip_v4":            "10.0.0.1",
							"tags":                    nil,
							"availability_zone_hints": nil,
							"network_mode":            nil,
							"network": []map[string]interface{}{
								{
									"port":           "",
									"uuid":           "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
									"access_network": false,
									"fixed_ip_v4":    "10.0.0.1",
									"fixed_ip_v6":    "",
									"floating_ip":    "",
									"mac":            "aa:aa:aa:aa:aa:aa",
									"name":           "cacao-admin-api-net",
								},
							},
							"stop_before_destroy": false,
							"name":                "tf-test",
							"id":                  "dddddddd-dddd-dddd-dddd-d1234ddddddd",
							"flavor_id":           "faaaaaaa-aaaa-aaaa-aaaa-aaaaaaaa1234",
							"all_metadata":        map[string]interface{}{},
							"block_device":        []interface{}{},
							"volume":              []interface{}{},
							"vendor_options":      []interface{}{},
							"access_ip_v6":        "",
							"power_state":         "active",
							"user_data":           nil,
							"image_id":            "aaaaaaaa-aaaa-aaaa-aaaa-a1234aaaaaaa",
							"flavor_name":         "tiny1",
							"key_pair":            "my-keypair",
							"timeouts":            nil,
							"floating_ip":         "8.8.8.8", // floating ip should be attached here
							"image_name":          "ubuntu-20.04",
							"availability_zone":   "nova",
							"scheduler_hints":     []interface{}{},
							"security_groups": []string{
								"cacao-admin-api-sg",
							},
							"metadata":     nil,
							"config_drive": nil,
							"force_delete": false,
						},
						SensitiveAttributes: nil,
						AvailableActions:    []service.DeploymentResourceAction{},
					},
					{
						ID:           "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbb1234",
						Type:         "openstack_networking_floatingip_v2",
						ProviderType: "openstack",
						Provider:     "provider-aaaaaaaaaaaaaaaaaaaa",
						Attributes: map[string]interface{}{
							"pool":        "ext-net",
							"dns_name":    "",
							"value_specs": nil,
							"region":      "region123",
							"id":          "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbb1234",
							"all_tags":    []interface{}{},
							"tags":        []interface{}{},
							"fixed_ip":    "",
							"port_id":     "",
							"description": "floating ip for tf-test, 0/1",
							"address":     "8.8.8.8",
							"timeouts":    nil,
							"tenant_id":   "aaaaaaaaaaaaaaaaaaaaccccccccc123",
							"subnet_id":   "aaaaaaaa-aaaa-aaaa-aaaa-bbbbbbbbb123",
							"dns_domain":  "",
						},
						SensitiveAttributes: nil,
						AvailableActions:    []service.DeploymentResourceAction{},
					},
				},
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			parser := TerraformStateParser{}
			got, err := parser.ToStateView(tt.args.state, tt.args.primaryProvider)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToStateView() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			assert.DeepEqual(t, tt.want, got)
		})
	}
}
