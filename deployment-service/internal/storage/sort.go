package storage

import (
	"gitlab.com/cyverse/cacao-common/service"
)

// DeploymentSort is sort option for deployment
type DeploymentSort struct {
	SortBy  DeploymentSortField
	SortDir SortDirection
}

// FromExternalDeploymentSort converts from external representation of sorting option for deployment
func FromExternalDeploymentSort(field service.SortByField, direction service.SortDirection) (DeploymentSort, error) {
	sort := DeploymentSort{}
	switch field {
	case "":
		sort.SortBy = DefaultDeploymentSort().SortBy
	case service.SortByID:
		sort.SortBy = SortByID
	case service.SortByWorkspace:
		sort.SortBy = SortByWorkspace
	case service.SortByTemplate:
		sort.SortBy = SortByTemplate
	case service.SortByPrimaryCloud:
		sort.SortBy = SortByPrimaryCloud
	default:
		return DeploymentSort{}, service.NewCacaoGeneralError("unknown sort by field")
	}
	switch direction {
	case 0:
		sort.SortDir = DefaultDeploymentSort().SortDir
	case service.AscendingSort:
		sort.SortDir = AscendingSort
	case service.DescendingSort:
		sort.SortDir = DescendingSort
	default:
		return DeploymentSort{}, service.NewCacaoGeneralError("unknown sort direction")
	}
	return sort, nil
}

// DefaultDeploymentSort returns the default sort option
func DefaultDeploymentSort() DeploymentSort {
	return DeploymentSort{
		SortBy:  SortByID,
		SortDir: AscendingSort,
	}
}

// DeploymentSortField are fields that can be sort by for deployment
type DeploymentSortField string

// String ...
func (f DeploymentSortField) String() string {
	return string(f)
}

// Fields to sort by for deployment
const (
	SortByID           DeploymentSortField = "_id" // default
	SortByWorkspace    DeploymentSortField = "workspace"
	SortByTemplate     DeploymentSortField = "template"
	SortByPrimaryCloud DeploymentSortField = "primary_cloud"
)

// SortDirection is the direction to sort the result
type SortDirection int

// AscendingSort sorts result in ascending order
const AscendingSort SortDirection = 1 // default
// DescendingSort sorts result in descending order
const DescendingSort SortDirection = -1
