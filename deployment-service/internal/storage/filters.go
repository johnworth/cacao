package storage

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"go.mongodb.org/mongo-driver/bson"
	"time"
)

// DeploymentFilter is filter for searching deployment. All values here must have a "nil" or "zero" value that indicates
// the field is not set.
type DeploymentFilter struct {
	ID common.ID
	// Only return deployments that is created by a user
	Creator string
	// Only return deployments that has the specified source template
	Template common.ID
	// Only return deployments that is in the specified workspace
	Workspace common.ID
	// Only return deployments that has the specified primary cloud provider
	PrimaryCloudProvider common.ID
	// Only return deployments that has the specified last run status
	LastRunStatus string
	// Only return deployments that is marked as deleting
	Deleting *bool
	// Only return deployments with matching deletion workflow name
	DeletionWorkflow string
	CurrentStatus    []service.DeploymentStatus
	PendingStatus    []service.DeploymentPendingStatus
}

// ToMongoFilters converts a deployment filter object to filter in mongo-format
func (df DeploymentFilter) ToMongoFilters() bson.M {
	mongoFilters := bson.M{}
	if len(df.ID) > 0 {
		mongoFilters["_id"] = df.ID
	}
	if len(df.Creator) > 0 {
		mongoFilters["creator"] = df.Creator
	}
	if len(df.Template.String()) > 0 {
		mongoFilters["template"] = df.Template
	}
	if len(df.Workspace.String()) > 0 {
		mongoFilters["workspace"] = df.Workspace
	}
	if len(df.PrimaryCloudProvider.String()) > 0 {
		mongoFilters["provider"] = df.PrimaryCloudProvider
	}
	if len(df.LastRunStatus) > 0 {
		// FIXME fix the bson field name
		//mongoFilters["status"] = filters.LastRunStatus
	}
	if df.Deleting != nil {
		mongoFilters["deletion.deleting"] = *df.Deleting
	}
	if len(df.DeletionWorkflow) > 0 {
		mongoFilters["deletion.wf_name"] = df.DeletionWorkflow
	}
	if df.CurrentStatus != nil {
		if len(df.CurrentStatus) == 1 {
			// single status
			mongoFilters["current_status"] = df.CurrentStatus[0]
		} else if len(df.CurrentStatus) > 1 {
			// multiple status
			mongoFilters["current_status"] = bson.M{"$in": df.CurrentStatus}
		}
	}
	if df.PendingStatus != nil {
		if len(df.PendingStatus) == 1 {
			// single status
			mongoFilters["pending_status"] = df.PendingStatus[0]
		} else if len(df.PendingStatus) > 1 {
			// multiple status
			mongoFilters["pending_status"] = bson.M{"$in": df.PendingStatus}
		}
	}
	return mongoFilters
}

// DeploymentRunFilter is filter for searching deployment run
type DeploymentRunFilter struct {
	ID            common.ID
	Deployment    common.ID
	WorkflowName  string
	WorkflowEnded *bool
}

// ToMongoFilters converts a deployment run filter object to filter in mongo-format
func (drf DeploymentRunFilter) ToMongoFilters() bson.M {
	mongoFilter := bson.M{}
	if len(drf.ID.String()) > 0 {
		mongoFilter["_id"] = drf.ID.String()
	}
	if len(drf.Deployment.String()) > 0 {
		mongoFilter["deployment"] = drf.Deployment.String()
	}
	var wfElemMatch = bson.M{}
	if len(drf.WorkflowName) > 0 {
		wfElemMatch["wf_name"] = drf.WorkflowName
	}
	if drf.WorkflowEnded != nil {
		zeroTime := time.Time{}
		if *drf.WorkflowEnded {
			// ended workflow will have non-zero ended_at timestamp
			wfElemMatch["ended_at"] = bson.M{"$ne": zeroTime}
		} else {
			wfElemMatch["ended_at"] = bson.M{"$eq": zeroTime}
		}
	}
	if len(wfElemMatch) > 0 {
		mongoFilter["wfs"] = bson.M{"$elemMatch": wfElemMatch}
	}
	return mongoFilter
}

// DeploymentHistoryFilter is filter for searching deployment history
type DeploymentHistoryFilter struct {
	// Only return histories for a certain deployment
	Deployment common.ID
	// Only return histories that has a timestamp before the specified
	Before *time.Time
	// Only return histories that has a timestamp after the specified
	After *time.Time
}

// IsDeploymentSet ...
func (dhf DeploymentHistoryFilter) IsDeploymentSet() bool {
	return len(dhf.Deployment.String()) > 0
}

// IsBeforeSet ...
func (dhf DeploymentHistoryFilter) IsBeforeSet() bool {
	return dhf.Before != nil
}

// IsAfterSet ...
func (dhf DeploymentHistoryFilter) IsAfterSet() bool {
	return dhf.After != nil
}
