package storage

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"go.mongodb.org/mongo-driver/bson"
	"time"
)

// DeploymentUpdate is used for updating deployment, it contains fields that can potentially be updated.
// All fields should be nullable(ptr or slice), so that fields that has nil value is not updated in storage.
type DeploymentUpdate struct {
	Name                 *string
	Description          *string
	UpdatedAt            *time.Time
	Template             *common.ID
	TemplateType         *string
	PrimaryCloudProvider *common.ID
	CurrentStatus        *service.DeploymentStatus
	PendingStatus        *service.DeploymentPendingStatus
	StatusMsg            *string
	CloudCredentials     deploymentcommon.ProviderCredentialMappings // ProviderCredentialMappings is slice, so no *
	GitCredential        *deploymentcommon.CredentialID
	LastRun              **common.ID // LastRun is a ptr field, so **
	Deletion             *deploymentcommon.DeploymentDeletion
}

// ToBSONValues ...
func (du DeploymentUpdate) ToBSONValues() bson.M {
	result := bson.M{}
	if du.Name != nil {
		result["name"] = *du.Name
	}
	if du.Description != nil {
		result["description"] = *du.Description
	}
	if du.UpdatedAt != nil {
		result["updated_at"] = *du.UpdatedAt
	}
	if du.Template != nil {
		result["template"] = *du.Template
	}
	if du.TemplateType != nil {
		result["template_type"] = *du.TemplateType
	}
	if du.PrimaryCloudProvider != nil {
		result["primary_provider"] = *du.PrimaryCloudProvider
	}
	if du.CurrentStatus != nil {
		result["current_status"] = *du.CurrentStatus
	}
	if du.PendingStatus != nil {
		result["pending_status"] = *du.PendingStatus
	}
	if du.StatusMsg != nil {
		result["current_status"] = *du.StatusMsg
	}
	if du.CloudCredentials != nil {
		result["cloud_credentials"] = du.CloudCredentials
	}
	if du.GitCredential != nil {
		result["git_credential"] = du.GitCredential
	}
	if du.LastRun != nil {
		result["last_run"] = du.LastRun
	}
	if du.Deletion != nil {
		result["deletion"] = du.Deletion
	}
	return result
}

// DeploymentRunUpdate is used for updating deployment run, it contains fields that can potentially be updated.
// All fields should be nullable(ptr or slice), so that fields that has nil value is not updated in storage.
type DeploymentRunUpdate struct {
	Deployment       *common.ID                            `bson:"deployment"`
	EndsAt           *time.Time                            `bson:"ended_at"`
	TemplateSnapshot *deploymentcommon.TemplateSnapshot    `bson:"template"`
	Parameters       deploymentcommon.DeploymentParameters `bson:"parameters"` // DeploymentParameters is slice, so no *
	Status           *deploymentcommon.DeploymentRunStatus `bson:"status"`
	StatusMsg        *string                               `bson:"status_msg"`
	LastState        *deploymentcommon.DeploymentStateView `bson:"last_state"`
	StateUpdatedAt   *time.Time                            `bson:"state_updated_at"`
	RawState         **deploymentcommon.RawDeploymentState `bson:"raw_state"` // RawState is a ptr field, so **
	Logs             **string                              `bson:"logs"`      // Logs is a ptr field, so **
	Workflows        []deploymentcommon.DeploymentWorkflow `bson:"wfs"`
}

// ToBSONValues ...
func (dru DeploymentRunUpdate) ToBSONValues() bson.M {
	result := bson.M{}
	if dru.Deployment != nil {
		result["deployment"] = dru.Deployment
	}
	if dru.EndsAt != nil {
		result["ended_at"] = dru.EndsAt
	}
	if dru.TemplateSnapshot != nil {
		result["template"] = dru.TemplateSnapshot
	}
	if dru.Parameters != nil {
		result["parameters"] = dru.Parameters
	}
	if dru.Status != nil {
		result["status"] = dru.Status
	}
	if dru.StatusMsg != nil {
		result["status_msg"] = dru.StatusMsg
	}
	if dru.LastState != nil {
		result["last_state"] = dru.LastState
	}
	if dru.StateUpdatedAt != nil {
		result["state_updated_at"] = dru.StateUpdatedAt
	}
	if dru.RawState != nil {
		result["raw_state"] = dru.RawState
	}
	if dru.Logs != nil {
		result["logs"] = dru.Logs
	}
	if dru.Workflows != nil {
		result["wfs"] = dru.Workflows
	}
	return result
}
