package storage

import (
	"context"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// DeploymentRunCollection is the mongo collection for storing deployment runs
const DeploymentRunCollection string = "deploymentRun"

// MongoDeploymentRunStorage is a mongo storage for deployment run
type MongoDeploymentRunStorage struct {
	config   db.MongoDBConfig
	objStore db.MongoDBObjectStore
}

// NewMongoDeploymentRunStorage creates a new MongoDeploymentRunStorage
func NewMongoDeploymentRunStorage(config db.MongoDBConfig) *MongoDeploymentRunStorage {
	return &MongoDeploymentRunStorage{config: config}
}

// Init initialize the mongo storage
func (m *MongoDeploymentRunStorage) Init() error {
	objStore, err := db.CreateMongoDBObjectStore(&m.config)
	if err != nil {
		return err
	}
	m.objStore = *objStore
	log.Info("connected to Mongodb")
	return nil
}

// Create creates a deployment run
func (m MongoDeploymentRunStorage) Create(run deploymentcommon.DeploymentRun) (runID common.ID, err error) {
	m.setCreationTimestamp(&run)
	if err := m.validateBeforeCreate(run); err != nil {
		return "", err
	}

	err = m.objStore.Insert(m.collectionName(), run)
	if err != nil {
		return "", service.NewCacaoGeneralError(err.Error())
	}
	return run.ID, nil
}

func (m MongoDeploymentRunStorage) setCreationTimestamp(run *deploymentcommon.DeploymentRun) {
	emptyTime := time.Time{}
	if run.CreatedAt == emptyTime {
		run.CreatedAt = time.Now().UTC()
	}
}

func (m MongoDeploymentRunStorage) validateBeforeCreate(run deploymentcommon.DeploymentRun) error {
	if run.ID == "" {
		return service.NewCacaoInvalidParameterError("run missing id")
	}
	if run.Deployment == "" {
		return service.NewCacaoInvalidParameterError("run missing deployment")
	}
	if run.CreatedBy.User == "" {
		return service.NewCacaoInvalidParameterError("run missing creator")
	}
	if run.TemplateSnapshot.TemplateID == "" {
		return service.NewCacaoInvalidParameterError("run missing template ID")
	}
	if run.TemplateSnapshot.GitURL == "" {
		return service.NewCacaoInvalidParameterError("run missing git URL")
	}
	// if branch, tag and commit are all empty
	if run.TemplateSnapshot.UpstreamTracked.Branch == "" &&
		run.TemplateSnapshot.UpstreamTracked.Tag == "" &&
		run.TemplateSnapshot.CommitHash == "" {
		return service.NewCacaoInvalidParameterError("run missing commit hash")
	}
	// parameters must be present, even if it is empty ([]DeploymentParameter{})
	if run.Parameters == nil {
		return service.NewCacaoInvalidParameterError("run missing parameters")
	}
	if run.Status == "" {
		return service.NewCacaoInvalidParameterError("run missing status")
	}
	return nil
}

// Get returns the deployment run with the specific ID
func (m MongoDeploymentRunStorage) Get(runID common.ID) (deploymentcommon.DeploymentRun, error) {
	var run deploymentcommon.DeploymentRun
	err := m.objStore.Get(m.collectionName(), runID.String(), &run)
	if err != nil {
		log.Error(err)
		if err == mongo.ErrNoDocuments {
			errorMessage := fmt.Sprintf("deployment run %s not found", runID.String())
			return run, service.NewCacaoNotFoundError(errorMessage)
		}

		return run, service.NewCacaoGeneralError(err.Error())
	}
	return run, nil
}

// GetLatestRun returns the latest deployment run of a deployment if any
func (m MongoDeploymentRunStorage) GetLatestRun(deploymentID common.ID) (*deploymentcommon.DeploymentRun, error) {
	filter := bson.M{
		"deployment": deploymentID.String(),
	}
	sort := bson.M{
		"created_at": -1,
	}
	var run deploymentcommon.DeploymentRun
	err := m.objStore.Connection.Get(m.collectionName(), filter, &run, options.FindOne().SetSort(sort))
	if err != nil {
		log.Error(err)
		if err == mongo.ErrNoDocuments {
			errorMessage := fmt.Sprintf("deployment run for deployment %s not found", deploymentID.String())
			return nil, service.NewCacaoNotFoundError(errorMessage)
		}

		return nil, service.NewCacaoGeneralError(err.Error())
	}
	return &run, nil
}

// List return a list of deployment run with filter
func (m MongoDeploymentRunStorage) List(filter DeploymentRunFilter, pagination service.RequestPagination) ([]deploymentcommon.DeploymentRun, error) {
	var runs []deploymentcommon.DeploymentRun

	err := m.objStore.Connection.List(m.collectionName(), filter.ToMongoFilters(), &runs, m.paginationToMongoOption(pagination))
	if err != nil {
		if err == mongo.ErrNoDocuments {
			errorMessage := "deployment run not found"
			return nil, service.NewCacaoNotFoundError(errorMessage)
		}
		return nil, service.NewCacaoGeneralError(err.Error())
	}

	return runs, nil
}

// BatchGet get a list of deployment runs in batch
func (m MongoDeploymentRunStorage) BatchGet(runIDs []common.ID) ([]deploymentcommon.DeploymentRun, error) {
	logger := log.WithFields(log.Fields{
		"package":  "storage",
		"function": "MongoDeploymentRunStorage.BatchGet",
	})
	if runIDs == nil {
		logger.Error("runIDs is nil")
		return nil, service.NewCacaoGeneralError("runIDs is nil, cannot BatchGet run")
	}
	if len(runIDs) == 0 {
		return []deploymentcommon.DeploymentRun{}, nil
	} else if len(runIDs) > 100 {
		logger.WithField("count", len(runIDs)).Warn("BatchGet over 100 runs")
	}
	var filter = bson.M{
		"_id": bson.M{
			"$in": runIDs,
		},
	}
	cursor, err := m.objStore.Connection.Database.Collection(m.collectionName()).Find(context.TODO(), filter)
	if err != nil {
		logger.WithError(err).Error("error from mongo Find()")
		if err == mongo.ErrNoDocuments {
			errorMessage := "deployment runs not found"
			return nil, service.NewCacaoNotFoundError(errorMessage)
		}
		return nil, err
	}
	defer cursor.Close(context.TODO())

	var runs []deploymentcommon.DeploymentRun
	err = cursor.All(context.TODO(), &runs)
	if err != nil {
		logger.WithError(err).Error("failed to read documents from a cursor")
		return nil, err
	}
	return runs, nil
}

// Update a deployment run.
func (m MongoDeploymentRunStorage) Update(id common.ID, run DeploymentRunUpdate, filter DeploymentRunFilter) (bool, error) {
	filter.ID = id
	ok, err := m.objStore.Connection.Update(m.collectionName(), filter.ToMongoFilters(), bson.M{"$set": run.ToBSONValues()})
	if err != nil {
		log.Error(err)
		return false, service.NewCacaoGeneralError(err.Error())
	}
	return ok, nil
}

// Delete deletes a deployment run by ID
func (m MongoDeploymentRunStorage) Delete(runID common.ID) error {
	ok, err := m.objStore.Delete(m.collectionName(), runID.String())
	if err != nil {
		log.Error(err)
		return service.NewCacaoGeneralError(err.Error())
	}
	if !ok {
		return service.NewCacaoNotFoundError("deployment run not found")
	}
	return nil
}

// DeleteAllRunByDeployment deletes all deployment run within a deployment
func (m MongoDeploymentRunStorage) DeleteAllRunByDeployment(deployment common.ID) error {
	deleteResult, err := m.collection().DeleteMany(context.TODO(), bson.M{"deployment": deployment.String()}, options.Delete())
	if err != nil {
		errorMessage := "fail to delete deployment runs under deployment"
		log.WithField("error", err).Error(errorMessage)
		return service.NewCacaoGeneralError(errorMessage)
	}

	log.WithField("deletedCount", deleteResult.DeletedCount).Trace("deployment run(s) deleted")
	return nil
}

func (m MongoDeploymentRunStorage) collection() *mongo.Collection {
	return m.objStore.Connection.Database.Collection(m.collectionName())
}

func (m MongoDeploymentRunStorage) collectionName() string {
	return DeploymentRunCollection
}

func (m MongoDeploymentRunStorage) paginationToMongoOption(pagination service.RequestPagination) *options.FindOptions {
	opt := options.Find()
	if pagination.PageSizeLimit > 0 {
		opt.SetLimit(int64(pagination.PageSizeLimit))
	}
	if pagination.Offset > 0 {
		opt.SetSkip(int64(pagination.Offset))
	}
	return opt
}
