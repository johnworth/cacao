package storage

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"reflect"
	"testing"
)

// This test enforces that all fields in update struct has ptr type of the same field in Deployment struct unless it is slice.
// Field1 Type1 => Field1 *Type1.
func Test_DeploymentUpdateStruct(t *testing.T) {
	deploymentStruct := reflect.TypeOf(deploymentcommon.Deployment{})
	deploymentUpdateStruct := reflect.TypeOf(DeploymentUpdate{})
	assert.Equal(t, reflect.Struct, deploymentStruct.Kind())
	assert.Equal(t, reflect.Struct, deploymentUpdateStruct.Kind())
	// update struct should not contain more fields than the deployment struct
	assert.LessOrEqual(t, deploymentUpdateStruct.NumField(), deploymentStruct.NumField())
	deploymentStructFields := map[string]reflect.StructField{}
	for i := 0; i < deploymentStruct.NumField(); i++ {
		field := deploymentStruct.Field(i)
		deploymentStructFields[field.Name] = field
	}
	for i := 0; i < deploymentUpdateStruct.NumField(); i++ {
		updateField := deploymentUpdateStruct.Field(i)
		// check if field of update struct is null-able, either ptr type or slice
		if !assert.True(t, reflect.Ptr == updateField.Type.Kind() || reflect.Slice == updateField.Type.Kind()) {
			t.Errorf("field %s(%s) is not nullable", updateField.Name, updateField.Type.Kind().String())
		}
		field, ok := deploymentStructFields[updateField.Name]
		if assert.True(t, ok) {
			if field.Type.Kind() == reflect.Slice {
				assert.Equalf(t, reflect.Slice, updateField.Type.Kind(), updateField.Name)
			} else {
				// for non-slice field, the type in update struct should be the pointer type of the field in deployment struct
				assert.Equal(t, "*"+field.Type.String(), updateField.Type.String())
			}
		}
	}
}

// This test enforces that all fields in update struct has ptr type of the same field in DeploymentRun struct unless it is slice.
// Field1 Type1 => Field1 *Type1.
func Test_DeploymentRunUpdateStruct(t *testing.T) {
	runStruct := reflect.TypeOf(deploymentcommon.DeploymentRun{})
	runUpdateStruct := reflect.TypeOf(DeploymentRunUpdate{})
	assert.Equal(t, reflect.Struct, runStruct.Kind())
	assert.Equal(t, reflect.Struct, runUpdateStruct.Kind())
	// update struct should not contain more fields than the deployment struct
	assert.LessOrEqual(t, runUpdateStruct.NumField(), runStruct.NumField())
	deploymentStructFields := map[string]reflect.StructField{}
	for i := 0; i < runStruct.NumField(); i++ {
		field := runStruct.Field(i)
		deploymentStructFields[field.Name] = field
	}
	for i := 0; i < runUpdateStruct.NumField(); i++ {
		updateField := runUpdateStruct.Field(i)
		// check if field of update struct is null-able, either ptr type or slice
		if !assert.True(t, reflect.Ptr == updateField.Type.Kind() || reflect.Slice == updateField.Type.Kind()) {
			t.Errorf("field %s(%s) is not nullable", updateField.Name, updateField.Type.Kind().String())
		}
		field, ok := deploymentStructFields[updateField.Name]
		if assert.True(t, ok) {
			if field.Type.Kind() == reflect.Slice {
				assert.Equal(t, reflect.Slice, updateField.Type.Kind())
			} else {
				// for non-slice field, the type in update struct should be the pointer type of the field in deployment struct
				assert.Equal(t, "*"+field.Type.String(), updateField.Type.String())
			}
		}
	}
}
