package storage

import (
	"context"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const deploymentMongoCollection string = "deployment"

// MongoDeploymentStorage is mongo storage for deployment object
type MongoDeploymentStorage struct {
	conf     db.MongoDBConfig
	objStore *db.MongoDBObjectStore
}

// NewMongoDeploymentStorage creates a new MongoDeploymentStorage
func NewMongoDeploymentStorage(conf db.MongoDBConfig) *MongoDeploymentStorage {
	return &MongoDeploymentStorage{
		conf: conf,
	}
}

// Init establish connection to mongo
func (m *MongoDeploymentStorage) Init() error {
	objStore, err := db.CreateMongoDBObjectStore(&m.conf)
	if err != nil {
		return err
	}
	m.objStore = objStore
	log.Info("connected to Mongodb")
	m.createIndexes()
	return nil
}

// return the underlying mongo collection object
func (m *MongoDeploymentStorage) mongoCollection() *mongo.Collection {
	return m.objStore.Connection.Database.Collection(deploymentMongoCollection)
}

func (m *MongoDeploymentStorage) createIndexes() {
	models := []mongo.IndexModel{
		{
			Keys: bson.M{
				"creator": AscendingSort,
			},
			Options: options.Index().SetName("creator"),
		},
		{
			Keys: bson.M{
				"workspace": AscendingSort,
			},
			Options: options.Index().SetName("workspace"),
		},
		{
			Keys: bson.M{
				"template": AscendingSort,
			},
			Options: options.Index().SetName("template"),
		},
		{
			Keys: bson.M{
				"primary_provider": AscendingSort,
			},
			Options: options.Index().SetName("primary_provider"),
		},
	}
	indexNames, err := m.mongoCollection().Indexes().CreateMany(context.TODO(), models)
	if err != nil {
		return
	}
	for _, indexName := range indexNames {
		log.WithField("name", indexName).Info("created an index for deployment collection")
	}
}

// Create creates a Deployment object in mongo
func (m MongoDeploymentStorage) Create(deployment deploymentcommon.Deployment) error {
	m.setCreationTimestamp(&deployment)
	if err := m.validateBeforeCreate(deployment); err != nil {
		return err
	}

	err := m.objStore.Insert(deploymentMongoCollection, deployment)
	if err != nil {
		return service.NewCacaoGeneralError(err.Error())
	}
	return nil
}

func (m MongoDeploymentStorage) setCreationTimestamp(deployment *deploymentcommon.Deployment) {
	emptyTime := time.Time{}
	if deployment.CreatedAt == emptyTime {
		deployment.CreatedAt = time.Now().UTC()
	}
}

func (m MongoDeploymentStorage) validateBeforeCreate(deployment deploymentcommon.Deployment) error {
	if deployment.ID == "" {
		return service.NewCacaoInvalidParameterError("deployment missing ID")
	}
	if len(deployment.Name) > deploymentcommon.DeploymentNameMaxLength {
		return service.NewCacaoInvalidParameterError("name of deployment too long")
	}
	if len(deployment.Description) > deploymentcommon.DeploymentDescriptionMaxLength {
		return service.NewCacaoInvalidParameterError("deployment description too long")
	}
	if deployment.CreatedBy.User == "" {
		return service.NewCacaoInvalidParameterError("deployment missing creator")
	}
	if deployment.Template == "" {
		return service.NewCacaoInvalidParameterError("deployment missing template")
	}
	if deployment.TemplateType == "" {
		return service.NewCacaoInvalidParameterError("deployment missing template type")
	}
	if deployment.PrimaryCloudProvider == "" {
		return service.NewCacaoInvalidParameterError("deployment missing primary provider")
	}
	if deployment.CurrentStatus == "" {
		return service.NewCacaoGeneralError("deployment missing current status")
	}
	err := validateDeploymentStatus(deployment.CurrentStatus, deployment.PendingStatus)
	if err != nil {
		return err
	}
	if len(deployment.StatusMsg) > deploymentcommon.DeploymentStatusMsgMaxLength {
		return service.NewCacaoInvalidParameterError("deployment status msg too long")
	}
	return nil
}

// Get fetches a Deployment by its ID
func (m MongoDeploymentStorage) Get(id common.ID) (deploymentcommon.Deployment, error) {
	var deployment deploymentcommon.Deployment
	err := m.objStore.Get(deploymentMongoCollection, string(id), &deployment)
	if err != nil {
		log.Error(err)
		if err == mongo.ErrNoDocuments {
			errorMessage := fmt.Sprintf("deployment %s not found", id.String())
			return deployment, service.NewCacaoNotFoundError(errorMessage)
		}

		return deployment, service.NewCacaoGeneralError(err.Error())
	}
	return deployment, nil
}

// List returns a list of deployments with pagination (offet&limit), sort and filters
func (m MongoDeploymentStorage) List(filters DeploymentFilter, offset int64, limit int64, sort DeploymentSort) ([]deploymentcommon.Deployment, error) {
	var deployments []deploymentcommon.Deployment

	findOpts := options.Find().SetSkip(offset)
	if limit > 0 {
		findOpts.SetLimit(limit)
	}
	findOpts.SetSort(m.sortOption(sort))

	// TODO use range queries for better performance if sort by ID (or other unique identifier)
	cursor, err := m.mongoCollection().Find(
		context.TODO(), filters.ToMongoFilters(), findOpts)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			errorMessage := "deployment not found"
			return nil, service.NewCacaoNotFoundError(errorMessage)
		}
		return nil, service.NewCacaoGeneralError(err.Error())
	}

	err = cursor.All(context.TODO(), &deployments)
	if err != nil {
		return nil, service.NewCacaoGeneralError(err.Error())
	}
	return deployments, nil
}

// Update updates a deployment with specified ID
func (m MongoDeploymentStorage) Update(id common.ID, deployment DeploymentUpdate, filter DeploymentFilter) (bool, error) {
	filter.ID = id // set ID just in case
	ok, err := m.objStore.Connection.Update(
		deploymentMongoCollection,
		filter.ToMongoFilters(),
		bson.M{"$set": deployment.ToBSONValues()},
	)
	if err != nil {
		log.Error(err)
		return false, service.NewCacaoGeneralError(err.Error())
	}
	return ok, nil
}

// Delete deletes a deployment by its ID
func (m MongoDeploymentStorage) Delete(id common.ID) error {
	ok, err := m.objStore.Delete(deploymentMongoCollection, id.String())
	if err != nil {
		log.Error(err)
		return service.NewCacaoGeneralError(err.Error())
	}
	if !ok {
		return service.NewCacaoNotFoundError("deployment not found")
	}
	return nil
}

// UpdateStatus update status for a deployment. Return true if a deployment is updated, false if no deployment is updated.
func (m MongoDeploymentStorage) UpdateStatus(
	id common.ID,
	status service.DeploymentStatus,
	pendingStatus service.DeploymentPendingStatus,
	statusMsg string,
	filter DeploymentFilter) (bool, error) {

	filter.ID = id // set ID just in case
	err := validateDeploymentStatus(status, pendingStatus)
	if err != nil {
		return false, err
	}
	updateFields := bson.M{
		"current_status": status,
		"pending_status": pendingStatus,
		"status_msg":     statusMsg,
	}
	ok, err := m.objStore.Connection.Update(deploymentMongoCollection,
		filter.ToMongoFilters(),
		bson.M{"$set": updateFields})
	if err != nil {
		return false, err
	}
	return ok, nil
}

func validateDeploymentStatus(status service.DeploymentStatus, pendingStatus service.DeploymentPendingStatus) error {
	if !status.Valid() {
		return service.NewCacaoGeneralError("deployment has invalid current status")
	}
	if !pendingStatus.Valid() {
		return service.NewCacaoGeneralError("deployment has invalid pending status")
	}
	if !status.ValidPendingStatus(pendingStatus) {
		return service.NewCacaoGeneralError("deployment has invalid pending status")
	}
	return nil
}

func (m MongoDeploymentStorage) sortOption(sort DeploymentSort) bson.M {
	opt := bson.M{}
	opt[sort.SortBy.String()] = sort.SortDir
	return opt
}
