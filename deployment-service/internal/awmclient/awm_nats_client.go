package awmclient

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/mitchellh/mapstructure"
	"github.com/nats-io/stan.go"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
)

// implements ArgoWorkflowMediatorClient
type awmClient struct {
	natsConf messaging.NatsConfig
	stanConf messaging.StanConfig
}

// NewArgoWorkflowMediator creates a new ArgoWorkflowMediator
func NewArgoWorkflowMediator(natsConf messaging.NatsConfig, stanConf messaging.StanConfig) ArgoWorkflowMediatorClient {
	if stanConf.EventsTimeout <= 0 {
		stanConf.EventsTimeout = 30 // 30 sec
	}
	return &awmClient{natsConf: natsConf, stanConf: stanConf}
}

// CreateAsync ...
func (client awmClient) CreateAsync(provider AWMProvider, username string, workflowFilename string, opts ...AWMWorkflowCreateOpt) error {
	ce, err := client.newCreationRequest(provider, username, workflowFilename, opts...)
	if err != nil {
		return err
	}

	err = client.stanPublish(ce)
	if err != nil {
		return err
	}
	return nil
}

// Create creates a new workflow, workflow name is returned on success.
// TODO use messaging.EventSource when AWM support transaction ID. Currently AWM uses EventID of request as identifier instead of transaction ID.
func (client awmClient) Create(ctx context.Context, provider AWMProvider, username string, workflowFilename string, opts ...AWMWorkflowCreateOpt) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "awmClient.Create",
	})
	logger.Trace()

	ce, err := client.newCreationRequest(provider, username, workflowFilename, opts...)
	if err != nil {
		return "", err
	}
	requestID := common.EventID(ce.ID())

	var resultChan = make(chan cloudevents.Event)

	sc, sub, err := client.listenForResponse(ctx, map[common.EventType]struct{}{
		WorkflowCreatedEvent:      {},
		WorkflowCreateFailedEvent: {},
	}, requestID, resultChan)
	if err != nil {
		return "", err
	}
	defer sc.Close()
	defer sub.Unsubscribe()

	err = client.stanPublish(ce)
	if err != nil {
		return "", err
	}
	resultCe, err := client.waitForResponse(ctx, resultChan)
	if err != nil {
		return "", err
	}
	return client.handleCreationResult(resultCe)
}

func (client awmClient) newCreationRequest(provider AWMProvider, username string, workflowFilename string, opts ...AWMWorkflowCreateOpt) (*cloudevents.Event, error) {
	event := WorkflowCreate{
		Provider:         provider,
		Username:         nil,
		WorkflowFilename: workflowFilename,
		WfDat:            nil,
	}
	if username != "" {
		event.Username = &username
	}
	var wfOption awmWorkflowCreateOptions

	for _, optFunc := range opts {
		optFunc(&wfOption)
	}

	err := client.applyCreatOptions(&event, wfOption)
	if err != nil {
		return nil, err
	}
	ce, err := messaging.CreateCloudEventWithTransactionID(
		event,
		string(WorkflowCreateRequestedEvent),
		client.natsConf.ClientID,
		messaging.NewTransactionID(),
	)
	if err != nil {
		return nil, err
	}
	return &ce, nil
}

// apply option to event
func (client awmClient) applyCreatOptions(event *WorkflowCreate, wfOpt awmWorkflowCreateOptions) error {
	switch event.WorkflowFilename {
	case TerraformWorkflowFilename:
		return mapstructure.Decode(wfOpt.tfWfData, &event.WfDat)
	default:
		return fmt.Errorf("UnknownWorkflow")
	}
}

func (client awmClient) handleCreationResult(resultCe *cloudevents.Event) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "awmClient.handleCreationResult",
	})

	switch common.EventType(resultCe.Type()) {
	case WorkflowCreatedEvent:
		var resultEvent WorkflowCreated
		err := json.Unmarshal(resultCe.Data(), &resultEvent)
		if err != nil {
			logger.WithError(err).Error("fail to unmarshal received cloudevent")
			return "", err
		}
		return resultEvent.WorkflowName, nil
	case WorkflowCreateFailedEvent:
		var resultEvent WorkflowCreateFailed
		err := json.Unmarshal(resultCe.Data(), &resultEvent)
		if err != nil {
			logger.WithError(err).Error("fail to unmarshal received cloudevent")
			return "", err
		}
		return "", fmt.Errorf("%s, %s", resultEvent.Error, resultEvent.Msg)
	default:
		return "", fmt.Errorf("unknown response event type, %s", resultCe.Type())
	}
}

// TerminateAsync ...
func (client awmClient) TerminateAsync(provider AWMProvider, wfName string) error {
	ce, err := client.newTerminationRequest(provider, wfName)
	if err != nil {
		return err
	}

	err = client.stanPublish(ce)
	if err != nil {
		return err
	}
	return nil
}

// Terminate terminates a workflow
// TODO use messaging.EventSource when AWM support transaction ID. Currently AWM uses EventID of request as identifier instead of transaction ID.
func (client awmClient) Terminate(ctx context.Context, provider AWMProvider, wfName string) error {
	ce, err := client.newTerminationRequest(provider, wfName)
	if err != nil {
		return err
	}
	requestID := common.EventID(ce.ID())

	var resultChan = make(chan cloudevents.Event)

	sc, sub, err := client.listenForResponse(ctx, map[common.EventType]struct{}{
		WorkflowTerminatedEvent:      {},
		WorkflowTerminateFailedEvent: {},
	}, requestID, resultChan)
	if err != nil {
		return err
	}
	defer sc.Close()
	defer sub.Unsubscribe()

	err = client.stanPublish(ce)
	if err != nil {
		return err
	}

	resultCe, err := client.waitForResponse(ctx, resultChan)
	if err != nil {
		return err
	}
	return client.handleTerminationResultEvent(resultCe)
}

func (client awmClient) newTerminationRequest(provider AWMProvider, wfName string) (*cloudevents.Event, error) {
	event := WorkflowTerminate{
		Provider:     provider,
		WorkflowName: wfName,
	}
	ce, err := messaging.CreateCloudEventWithTransactionID(
		event,
		string(WorkflowTerminateRequestedEvent),
		client.natsConf.ClientID,
		messaging.NewTransactionID(),
	)
	if err != nil {
		return nil, err
	}
	return &ce, nil
}

func (client awmClient) handleTerminationResultEvent(resultCe *cloudevents.Event) error {
	switch common.EventType(resultCe.Type()) {
	case WorkflowTerminatedEvent:
		var resultEvent WorkflowTerminated
		err := json.Unmarshal(resultCe.Data(), &resultEvent)
		if err != nil {
			return service.NewCacaoMarshalError("unabled to unmarshal JSON bytes to WorkflowTerminated")
		}
		return nil
	case WorkflowTerminateFailedEvent:
		var resultEvent WorkflowTerminateFailed
		err := json.Unmarshal(resultCe.Data(), &resultEvent)
		if err != nil {
			return service.NewCacaoMarshalError("unabled to unmarshal JSON bytes to WorkflowTerminateFailed")
		}
		return service.NewCacaoGeneralError(resultEvent.Error)
	default:
		errorMessage := "unknown event type"
		log.WithField("eventType", resultCe.Type()).Error(errorMessage)
		return service.NewCacaoGeneralError(errorMessage)
	}
}

// ResubmitAsync ...
func (client awmClient) ResubmitAsync(provider AWMProvider, wfName string) error {
	return service.NewCacaoNotImplementedError("not implemented") // TODO implement workflow resubmission
}

// Resubmit resubmits a workflow, returns the new workflow name on success.
func (client awmClient) Resubmit(ctx context.Context, provider AWMProvider, wfName string) (string, error) {
	return "", service.NewCacaoNotImplementedError("not implemented") // TODO implement workflow resubmission
}

// WaitResp ...
func (client awmClient) listenForResponse(
	ctx context.Context,
	eventTypes map[common.EventType]struct{},
	requestID common.EventID,
	resultChan chan<- cloudevents.Event) (stan.Conn, stan.Subscription, error) {

	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "awmClient.listenForResponse",
	})
	sc, err := stan.Connect(client.stanConf.ClusterID, client.natsConf.ClientID, stan.NatsURL(client.natsConf.URL))
	if err != nil {
		logger.Error(err)
		return nil, nil, err
	}

	handler := func(m *stan.Msg) {
		ce, err := messaging.ConvertStan(m)
		if err != nil {
			logger.Error(err)
			return
		}
		_, ok := eventTypes[common.EventType(ce.Type())]
		if !ok {
			logger.WithField("eventType", ce.Type()).Trace("different event type")
			return
		}
		var staging struct {
			RequestID common.EventID `json:"req_id"`
		}
		err = json.Unmarshal(ce.Data(), &staging)
		if err != nil {
			logger.WithError(err).Error("fail to unmarshal to extract requestID")
			return
		}
		if staging.RequestID != requestID {
			logger.WithField("requestID", requestID).Trace("different requestID")
			return
		}
		resultChan <- ce
	}
	sub, err := sc.Subscribe(common.EventsSubject, handler, stan.StartWithLastReceived())
	if err != nil {
		sc.Close()
		return nil, nil, err
	}
	return sc, sub, nil
}

func (client awmClient) waitForResponse(ctx context.Context, resultChan <-chan cloudevents.Event) (*cloudevents.Event, error) {
	newCtx, cancel := context.WithTimeout(ctx, time.Second*time.Duration(client.stanConf.EventsTimeout))
	var result cloudevents.Event
	select {
	case <-newCtx.Done():
		cancel()
		return nil, newCtx.Err()
	case result = <-resultChan:
		cancel()
		return &result, nil
	}
}

func (client awmClient) stanPublish(ce *cloudevents.Event) error {
	natsConf := client.natsConf
	natsConf.ClientID = xid.New().String()
	conn, err := messaging.ConnectStanForServiceClient(&natsConf, &client.stanConf)
	if err != nil {
		return err
	}
	defer conn.Disconnect()

	return conn.PublishCloudEvent(ce)
}
