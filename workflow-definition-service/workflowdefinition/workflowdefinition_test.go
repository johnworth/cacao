package workflowdefinition

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"sync"
	"testing"
	"time"

	"gitlab.com/cyverse/cacao/common"
	"gitlab.com/cyverse/cacao/workflow-definition-service/workflowdefinition/types"

	"github.com/nats-io/nats-streaming-server/server"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson"
	"gopkg.in/src-d/go-git.v4"
)

func init() {
	log.SetOutput(ioutil.Discard)
}

var user = common.User{
	Username: "test_user",
	Secrets: map[string]common.Secret{
		"ID": {
			ID:       "ID",
			Type:     common.GitSecret,
			Username: "github_user",
			Value:    "github_token",
		},
		"Empty": {ID: "Empty", Type: common.GitSecret, Username: "", Value: ""},
	},
}

var wfd = &WorkflowDefinition{
	ID:          "xid",
	Name:        "workflow-definition-1",
	Description: "This is the first WorkflowDefinition",
	Repository: &common.Repository{
		RawURL: "./testdata/test-git",
		URL:    "./testdata/test-git",
		Branch: "master",
	},
	Owner: "test_user",
	Type:  "default",
}

var natsInfo = map[string]string{
	"cluster_id": "cacao-cluster",
	"client_id":  "workflow-definition",
	"address":    "nats://localhost:4222",
}

func runServer(t *testing.T) *server.StanServer {
	s, err := server.RunServer(natsInfo["cluster_id"])
	assert.NoError(t, err)
	return s
}

func TestGetID(t *testing.T) {
	assert.Equal(t, "xid", wfd.GetID())
}

func TestDefine(t *testing.T) {
	os.Rename("./testdata/test-git/git", "./testdata/test-git/.git")
	defer os.Rename("./testdata/test-git/.git", "./testdata/test-git/git")

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)
	adapter.GetMock().On("Replace", wfd).Return(nil)

	wfd, err := define("xid", "./testdata/test-git", "", user, "ID")
	assert.NoError(t, err)
	assert.Equal(t, "workflow-definition-1", wfd.Name)
	assert.Equal(t, "This is the first WorkflowDefinition", wfd.Description)
	assert.Equal(t, "test_user", wfd.Owner)
	assert.Equal(t, "./testdata/test-git", wfd.Repository.URL)
	assert.Equal(t, "master", wfd.Repository.Branch)
}

func TestDefineFailedURLParse(t *testing.T) {
	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)
	adapter.GetMock().On("Replace", wfd).Return(nil)

	wfd, err := define("xid", "%&", "", user, "ID")
	assert.Error(t, err)
	assert.Equal(t, "test_user", wfd.Owner)
}

func TestDefineWithNoType(t *testing.T) {
	os.Rename("./testdata/test-git-no-type/git", "./testdata/test-git-no-type/.git")
	defer os.Rename("./testdata/test-git-no-type/.git", "./testdata/test-git-no-type/git")

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)

	wfd, err := define("xid", "./testdata/test-git-no-type", "master", user, "ID")
	assert.Error(t, err)
	assert.Contains(t, wfd.Error, "'type' parameter is required")
}

func TestDefineWithInvalidName(t *testing.T) {
	os.Rename("./testdata/test-git-bad-name/git", "./testdata/test-git-bad-name/.git")
	defer os.Rename("./testdata/test-git-bad-name/.git", "./testdata/test-git-bad-name/git")

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)

	wfd, err := define("xid", "./testdata/test-git-bad-name", "master", user, "ID")
	assert.Error(t, err)
	// Make sure the error is actually complaining about the name format.
	assert.Contains(t, wfd.Error, "DNS-1123")
}

func TestDefineWithInvalidImage(t *testing.T) {
	os.Rename("./testdata/test-git-bad-image-name/git", "./testdata/test-git-bad-image-name/.git")
	defer os.Rename("./testdata/test-git-bad-image-name/.git", "./testdata/test-git-bad-image-name/git")

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)

	wfd, err := define("xid", "./testdata/test-git-bad-image-name", "master", user, "ID")
	assert.Error(t, err)
	assert.Contains(t, wfd.Error, "invalid value for Image")
}

func TestDefineWithNonexistentRepository(t *testing.T) {
	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)

	wfd, err := define("xid", "./testdata/fake-git", "master", user, "ID")
	assert.Error(t, err)
	assert.Equal(t, WorkflowDefinition{
		ID:    "xid",
		Owner: "test_user",
		Repository: &common.Repository{
			RawURL: "./testdata/fake-git",
			URL:    "./testdata/fake-git",
			Branch: "master",
		},
	}, wfd)
}

func TestDefineWithNonexistentBranch(t *testing.T) {
	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)

	wfd, err := define("xid", "./testdata/test-git", "fake-branch", user, "ID")
	assert.Error(t, err)
	assert.Equal(t, WorkflowDefinition{
		ID:    "xid",
		Owner: "test_user",
		Repository: &common.Repository{
			RawURL: "./testdata/test-git",
			URL:    "./testdata/test-git",
			Branch: "fake-branch",
		},
	}, wfd)
}

func TestDefineWithMissingFile(t *testing.T) {
	os.Rename("./testdata/test-git-bad/git", "./testdata/test-git-bad/.git")
	defer os.Rename("./testdata/test-git-bad/.git", "./testdata/test-git-bad/git")

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)

	wfd, err := define("xid", "./testdata/test-git-bad", "master", user, "ID")
	assert.Error(t, err)
	assert.Equal(t, "test_user", wfd.Owner)
	assert.Equal(t, "./testdata/test-git-bad", wfd.Repository.RawURL)
	assert.Equal(t, "./testdata/test-git-bad", wfd.Repository.URL)
	assert.Equal(t, "master", wfd.Repository.Branch)
	assert.Empty(t, wfd.Name)
	assert.Empty(t, wfd.Description)
}

func TestDefineWithMissingDockerfile(t *testing.T) {
	os.Rename("./testdata/test-git-no-dockerfile/git", "./testdata/test-git-no-dockerfile/.git")
	defer os.Rename("./testdata/test-git-no-dockerfile/.git", "./testdata/test-git-no-dockerfile/git")

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)

	wfd, err := define("xid", "./testdata/test-git-no-dockerfile", "master", user, "ID")
	assert.Error(t, err)
	assert.Equal(t, "test_user", wfd.Owner)
	assert.Equal(t, "./testdata/test-git-no-dockerfile", wfd.Repository.RawURL)
	assert.Equal(t, "./testdata/test-git-no-dockerfile", wfd.Repository.URL)
	assert.Equal(t, "master", wfd.Repository.Branch)
	assert.Equal(t, "correct error in git and update: dockerfile 'Dockerfile' not found", wfd.Error)
}

func TestDefineErrorFirstSave(t *testing.T) {
	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(fmt.Errorf(""))

	wfd, err := define("xid", "./testdata/test-git-bad", "master", user, "ID")
	assert.Error(t, err)
	assert.Equal(t, "test_user", wfd.Owner)
	assert.Equal(t, "./testdata/test-git-bad", wfd.Repository.RawURL)
	assert.Equal(t, "./testdata/test-git-bad", wfd.Repository.URL)
	assert.Equal(t, "master", wfd.Repository.Branch)
	assert.Empty(t, wfd.Name)
	assert.Empty(t, wfd.Description)
}

func TestGet(t *testing.T) {
	wfdBson, err := bson.Marshal(wfd)
	assert.NoError(t, err)
	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Read", "xid").Return(wfdBson, nil)

	wfd, err := get("xid", "test_user")
	assert.NoError(t, err)
	assert.Equal(t, "workflow-definition-1", wfd.Name)
	assert.Equal(t, "This is the first WorkflowDefinition", wfd.Description)
	assert.Equal(t, "test_user", wfd.Owner)
	assert.Equal(t, "./testdata/test-git", wfd.Repository.URL)
	assert.Equal(t, "master", wfd.Repository.Branch)
}

func TestGetForUser(t *testing.T) {
	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("ReadForUser", "test_user").Return([]map[string]interface{}{
		{
			"_id":         "xid",
			"name":        "workflow-definition-1",
			"description": "This is the first WorkflowDefinition",
			"repository": map[string]interface{}{
				"rawurl": "./testdata/test-git",
				"url":    "./testdata/test-git",
				"branch": "master",
			},
			"type":  "default",
			"owner": "test_user",
		},
		{
			"_id":         "xid",
			"name":        "workflow-definition-1",
			"description": "This is the first WorkflowDefinition",
			"repository": map[string]interface{}{
				"rawurl": "./testdata/test-git",
				"url":    "./testdata/test-git",
				"branch": "not-master",
			},
			"type":  "default",
			"owner": "test_user",
		},
	}, nil)

	wfds, err := getForUser("test_user")
	assert.NoError(t, err)
	assert.Len(t, wfds, 2)
	assert.Equal(t, "master", wfds[0].Repository.Branch)
	assert.Equal(t, "not-master", wfds[1].Repository.Branch)
}

func TestDelete(t *testing.T) {
	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Delete", "xid").Return(nil)

	err := wfd.delete()
	assert.NoError(t, err)
}

func TestUpdate(t *testing.T) {
	os.Rename("./testdata/test-git/git", "./testdata/test-git/.git")
	defer os.Rename("./testdata/test-git/.git", "./testdata/test-git/git")
	defer os.RemoveAll("./testdata/test-git-commit")

	// First clone the repo into a new one so we can push and delete
	git.PlainClone("./testdata/test-git-commit", true, &git.CloneOptions{
		URL: "./testdata/test-git",
	})

	wfd := WorkflowDefinition{
		ID: "xid",
		Repository: &common.Repository{
			RawURL: "./testdata/test-git-commit",
			URL:    "./testdata/test-git-commit",
			Branch: "master",
		},
		Type:        "default",
		Name:        "workflow-definition-1",
		Description: "This is the first WorkflowDefinition",
		Owner:       "test_user",
	}

	// Edit and write to the repository, then reverse changes on our object
	oldName := wfd.Name
	wfd.Name = "test-name"
	_, err := wfd.writeToGit("Commit", user.Secrets["ID"])
	assert.NoError(t, err)
	wfd.Name = oldName

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)
	adapter.GetMock().On("validate", mock.Anything).Return(nil)

	err = wfd.update(user, "ID")
	assert.NoError(t, err)
	assert.Equal(t, "test-name", wfd.Name)
}

func TestUpdateInvalidWFD(t *testing.T) {
	os.Rename("./testdata/test-git/git", "./testdata/test-git/.git")
	defer os.Rename("./testdata/test-git/.git", "./testdata/test-git/git")
	defer os.RemoveAll("./testdata/test-git-commit")

	// First clone the repo into a new one so we can push and delete
	git.PlainClone("./testdata/test-git-commit", true, &git.CloneOptions{
		URL: "./testdata/test-git",
	})

	wfd := WorkflowDefinition{
		ID: "xid",
		Repository: &common.Repository{
			RawURL: "./testdata/test-git-commit",
			URL:    "./testdata/test-git-commit",
			Branch: "master",
		},
		Type:        "default",
		Name:        "workflow-definition-1",
		Description: "This is the first WorkflowDefinition",
		Owner:       "test_user",
	}

	// Edit and write to the repository, then reverse changes on our object
	oldName := wfd.Name
	wfd.Name = "Invalid name"
	_, err := wfd.writeToGit("Commit", user.Secrets["ID"])
	assert.NoError(t, err)
	wfd.Name = oldName

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)
	adapter.GetMock().On("validate", mock.Anything).Return(nil)

	err = wfd.update(user, "ID")
	assert.Error(t, err)
	// Make sure the workflow wasn't updated if the proposed update was invalid.
	assert.Equal(t, "workflow-definition-1", wfd.Name)
}

func TestUpdateError(t *testing.T) {
	os.Rename("./testdata/test-git/git", "./testdata/test-git/.git")
	defer os.Rename("./testdata/test-git/.git", "./testdata/test-git/git")

	// First clone the repo into a new one so we can push and delete
	git.PlainClone("./testdata/test-git-commit", true, &git.CloneOptions{
		URL: "./testdata/test-git",
	})

	wfd := WorkflowDefinition{
		ID: "xid",
		Repository: &common.Repository{
			RawURL: "./testdata/test-git-commit",
			URL:    "./testdata/test-git-commit",
			Branch: "master",
		},
		Name:        "workflow-definition-1",
		Description: "This is the first WorkflowDefinition",
		Owner:       "test_user",
	}

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", wfd).Return(fmt.Errorf(""))

	// Delete repository to force error
	os.RemoveAll("./testdata/test-git-commit")
	err := wfd.update(user, "ID")
	assert.Error(t, err)
}

func TestEdit(t *testing.T) {
	os.Rename("./testdata/test-git/git", "./testdata/test-git/.git")
	defer os.Rename("./testdata/test-git/.git", "./testdata/test-git/git")
	defer os.RemoveAll("./testdata/test-git-commit")

	// First clone the repo into a new one so we can push and delete
	git.PlainClone("./testdata/test-git-commit", true, &git.CloneOptions{
		URL: "./testdata/test-git",
	})

	wfd := WorkflowDefinition{
		ID: "xid",
		Repository: &common.Repository{
			RawURL: "./testdata/test-git-commit",
			URL:    "./testdata/test-git-commit",
			Branch: "master",
		},
		Type:        "default",
		Name:        "workflow-definition-1",
		Description: "This is the first WorkflowDefinition",
		Owner:       "test_user",
	}

	request := Request{
		WorkflowDefinition: WorkflowDefinition{
			Description: "This WorkflowDefinition has been edited",
			Name:        "workflow-definition-1-edited",
		},
		BasicRequest: common.BasicRequest{
			User:        user,
			GitSecretID: "ID",
		},
	}
	reqBytes, err := json.Marshal(request)
	assert.NoError(t, err)

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)

	err = wfd.edit(reqBytes, "", user.Secrets["ID"])
	assert.NoError(t, err)
	assert.Equal(t, "workflow-definition-1-edited", wfd.Name)
}

func TestEditInvalidWFD(t *testing.T) {
	os.Rename("./testdata/test-git/git", "./testdata/test-git/.git")
	defer os.Rename("./testdata/test-git/.git", "./testdata/test-git/git")
	defer os.RemoveAll("./testdata/test-git-commit")

	// First clone the repo into a new one so we can push and delete
	git.PlainClone("./testdata/test-git-commit", true, &git.CloneOptions{
		URL: "./testdata/test-git",
	})

	wfd := WorkflowDefinition{
		ID: "xid",
		Repository: &common.Repository{
			RawURL: "./testdata/test-git-commit",
			URL:    "./testdata/test-git-commit",
			Branch: "master",
		},
		Type:        "default",
		Name:        "workflow-definition-1",
		Description: "This is the first WorkflowDefinition",
		Owner:       "test_user",
	}

	request := Request{
		WorkflowDefinition: WorkflowDefinition{
			Description: "This WorkflowDefinition has been edited",
			Name:        "Edited with invalid name",
		},
		BasicRequest: common.BasicRequest{
			User:        user,
			GitSecretID: "ID",
		},
	}
	reqBytes, err := json.Marshal(request)
	assert.NoError(t, err)

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)

	err = wfd.edit(reqBytes, "", user.Secrets["ID"])
	assert.Error(t, err)
	// Make sure the workflow wasn't edited if the proposed edit was invalid.
	assert.Equal(t, "workflow-definition-1", wfd.Name)
}

func TestEditEmptySecret(t *testing.T) {
	os.Rename("./testdata/test-git/git", "./testdata/test-git/.git")
	defer os.Rename("./testdata/test-git/.git", "./testdata/test-git/git")
	defer os.RemoveAll("./testdata/test-git-commit")

	// First clone the repo into a new one so we can push and delete
	git.PlainClone("./testdata/test-git-commit", true, &git.CloneOptions{
		URL: "./testdata/test-git",
	})

	wfd := WorkflowDefinition{
		ID: "xid",
		Repository: &common.Repository{
			RawURL: "./testdata/test-git-commit",
			URL:    "./testdata/test-git-commit",
			Branch: "master",
		},
		Type:        "default",
		Name:        "workflow-definition-1",
		Description: "This is the first WorkflowDefinition",
		Owner:       "test_user",
	}

	request := Request{
		WorkflowDefinition: WorkflowDefinition{
			Description: "This WorkflowDefinition has been edited",
			Name:        "workflow-definition-1-edited",
		},
		BasicRequest: common.BasicRequest{
			User:        user,
			GitSecretID: "Empty",
		},
	}
	reqBytes, err := json.Marshal(request)
	assert.NoError(t, err)

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)

	err = wfd.edit(reqBytes, "", common.Secret{})
	assert.Error(t, err)
	assert.Equal(t, "a secret is required for pushing to git, but none are present", err.Error())
}

func TestEditError(t *testing.T) {
	os.Rename("./testdata/test-git/git", "./testdata/test-git/.git")
	defer os.Rename("./testdata/test-git/.git", "./testdata/test-git/git")

	wfd := WorkflowDefinition{
		Repository: &common.Repository{
			RawURL: "./testdata/test-git-commit",
			URL:    "./testdata/test-git-commit",
			Branch: "master",
		},
		Name:        "workflow-definition-1",
		Description: "This is the first WorkflowDefinition",
		Owner:       "test_user",
	}

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(fmt.Errorf(""))

	request := Request{
		WorkflowDefinition: WorkflowDefinition{
			Description: "This WorkflowDefinition has been edited",
			Name:        "Edited WorkflowDefinition",
		},
		BasicRequest: common.BasicRequest{
			User:        user,
			GitSecretID: "ID",
		},
	}
	reqBytes, err := json.Marshal(request)
	assert.NoError(t, err)

	err = wfd.edit(reqBytes, "", user.Secrets["ID"])
	assert.Error(t, err)
}

func TestNatsCreate(t *testing.T) {
	wfdBson, err := bson.Marshal(wfd)
	assert.NoError(t, err)
	os.Rename("./testdata/test-git/git", "./testdata/test-git/.git")
	defer os.Rename("./testdata/test-git/.git", "./testdata/test-git/git")

	s := runServer(t)
	defer s.Shutdown()

	var wg sync.WaitGroup
	wg.Add(1)
	go common.StreamingTestSubscriber("WorkflowDefinition.Create", "WorkflowDefinition.CreateQueue", Create, natsInfo, &wg)
	request, err := json.Marshal(&Request{
		WorkflowDefinition: WorkflowDefinition{
			ID: "xid",
		},
		URL:    "./testdata/test-git",
		Branch: "master",
		BasicRequest: common.BasicRequest{
			User: user,
		},
	})
	assert.NoError(t, err)

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)
	adapter.GetMock().On("Replace", wfd).Return(nil)
	adapter.GetMock().On("Read", "xid").Return(wfdBson, nil)

	common.StreamingPublish(
		request,
		natsInfo,
		"WorkflowDefinition.Create",
		"WorkflowDefinitionTest",
	)
	wg.Wait()

	wfd, err := get("xid", "test_user")
	assert.NoError(t, err)
	assert.Equal(t, "workflow-definition-1", wfd.Name)
	assert.Equal(t, "This is the first WorkflowDefinition", wfd.Description)
}

func TestNatsCreateWithoutUser(t *testing.T) {
	os.Rename("./testdata/test-git/git", "./testdata/test-git/.git")
	defer os.Rename("./testdata/test-git/.git", "./testdata/test-git/git")

	s := runServer(t)
	defer s.Shutdown()

	var wg sync.WaitGroup
	wg.Add(1)
	go common.StreamingTestSubscriber("WorkflowDefinition.Create", "WorkflowDefinition.CreateQueue", Create, natsInfo, &wg)
	request, err := json.Marshal(&Request{
		WorkflowDefinition: WorkflowDefinition{
			ID: "xid",
		},
		URL:    "./testdata/test-git",
		Branch: "master",
		BasicRequest: common.BasicRequest{
			User: user,
		},
	})
	assert.NoError(t, err)

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)
	// adapter.getMock().On("replace", wfd).Return(nil)
	adapter.GetMock().On("Read", "xid").
		Return([]byte{}, fmt.Errorf(""))

	common.StreamingPublish(
		request,
		natsInfo,
		"WorkflowDefinition.Create",
		"WorkflowDefinitionTest",
	)
	wg.Wait()

	wfd, err := get("xid", "test_user")
	assert.Error(t, err)
	assert.Empty(t, wfd)
}

func TestNatsCreateError(t *testing.T) {
	s := runServer(t)
	defer s.Shutdown()

	var wg sync.WaitGroup
	wg.Add(1)
	go common.StreamingTestSubscriber("WorkflowDefinition.Create", "WorkflowDefinition.CreateQueue", Create, natsInfo, &wg)
	request, err := json.Marshal(&Request{
		WorkflowDefinition: WorkflowDefinition{
			ID: "xid",
		},
		URL:    "./testdata/test-git",
		Branch: "master",
		BasicRequest: common.BasicRequest{
			User: user,
		},
	})
	assert.NoError(t, err)

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(fmt.Errorf(""))
	adapter.GetMock().On("Read", "xid").Return([]byte{}, fmt.Errorf(""))

	common.StreamingPublish(
		request,
		natsInfo,
		"WorkflowDefinition.Create",
		"WorkflowDefinitionTest",
	)
	wg.Wait()

	wfd, err := get("xid", "test_user")
	assert.Error(t, err)
	assert.Empty(t, wfd.Name)
	assert.Empty(t, wfd.Description)
}

func TestNatsDelete(t *testing.T) {
	s := runServer(t)
	defer s.Shutdown()

	var wg sync.WaitGroup
	wg.Add(1)
	go common.StreamingTestSubscriber("WorkflowDefinition.Delete", "WorkflowDefinition.DeleteQueue", Delete, natsInfo, &wg)
	request, err := json.Marshal(&Request{
		BasicRequest: common.BasicRequest{
			WorkflowID: "xid",
			User:       user,
		},
	})
	assert.NoError(t, err)

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Delete", "xid").Return(nil)
	adapter.GetMock().On("Read", "xid").Return([]byte{}, fmt.Errorf(""))

	common.StreamingPublish(
		request,
		natsInfo,
		"WorkflowDefinition.Delete",
		"WorkflowDefinitionTest",
	)
	wg.Wait()

	wfd, err := get("xid", "test_user")
	assert.Error(t, err)
	assert.Empty(t, wfd.Name)
	assert.Empty(t, wfd.Description)
}

func TestNatsUpdateFromGit(t *testing.T) {
	s := runServer(t)
	defer s.Shutdown()

	os.Rename("./testdata/test-git/git", "./testdata/test-git/.git")
	defer os.Rename("./testdata/test-git/.git", "./testdata/test-git/git")
	defer os.RemoveAll("./testdata/test-git-commit")

	// First clone the repo into a new one so we can push and delete
	git.PlainClone("./testdata/test-git-commit", true, &git.CloneOptions{
		URL: "./testdata/test-git",
	})

	var wg sync.WaitGroup
	wg.Add(1)
	go common.StreamingTestSubscriber(
		"WorkflowDefinition.Update",
		"WorkflowDefinition.UpdateQueue",
		Update,
		natsInfo,
		&wg,
	)

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)

	wfd := WorkflowDefinition{
		ID: "xid",
		Repository: &common.Repository{
			RawURL: "./testdata/test-git-commit",
			URL:    "./testdata/test-git-commit",
			Branch: "master",
		},
		Name:        "workflow-definition-1",
		Description: "This is the first WorkflowDefinition",
		Owner:       "test_user",
		Type:        "default",
	}
	_, err := bson.Marshal(wfd)
	assert.NoError(t, err)
	wfd.save()

	// Edit and write to the repository, then reverse changes on our object
	oldName := wfd.Name
	wfd.Name = "test-name"
	_, err = wfd.writeToGit("Commit", user.Secrets["ID"])
	assert.NoError(t, err)
	wfdBson, err := bson.Marshal(wfd)
	assert.NoError(t, err)
	wfd.Name = oldName

	request, err := json.Marshal(&Request{
		BasicRequest: common.BasicRequest{
			WorkflowID: "xid",
			User:       user,
		},
	})
	assert.NoError(t, err)

	adapter.GetMock().On("Read", "xid").Return(wfdBson, nil)
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)
	common.StreamingPublish(
		request,
		natsInfo,
		"WorkflowDefinition.Update",
		"WorkflowDefinitionTest",
	)
	wg.Wait()

	updatedWfd := WorkflowDefinition{
		ID: "xid",
		Repository: &common.Repository{
			RawURL: "./testdata/test-git-commit",
			URL:    "./testdata/test-git-commit",
			Branch: "master",
		},
		Name:        "TEST FILE",
		Description: "This is the first WorkflowDefinition",
		Owner:       "test_user",
	}
	updatedWfdBson, err := bson.Marshal(updatedWfd)
	assert.NoError(t, err)
	adapter.GetMock().On("Read", "xid").Return(updatedWfdBson, nil)

	wfdNew, err := get("xid", "test_user")
	assert.NoError(t, err)
	assert.Equal(t, "test-name", wfdNew.Name)
	assert.Equal(t, wfd.Description, wfdNew.Description)
}

func TestNatsUpdateError(t *testing.T) {
	s := runServer(t)
	defer s.Shutdown()

	var wg sync.WaitGroup
	wg.Add(1)
	go common.StreamingTestSubscriber("WorkflowDefinition.Update", "WorkflowDefinition.UpdateQueue", Update, natsInfo, &wg)

	request, err := json.Marshal(&Request{
		BasicRequest: common.BasicRequest{
			WorkflowID: "xid",
			User:       user,
		},
	})
	assert.NoError(t, err)

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Read", "xid").
		Return([]byte{}, fmt.Errorf(""))
	common.StreamingPublish(
		request,
		natsInfo,
		"WorkflowDefinition.Update",
		"WorkflowDefinitionTest",
	)
	wg.Wait()
}

func TestNatsUpdateWithoutUser(t *testing.T) {
	s := runServer(t)
	defer s.Shutdown()

	os.Rename("./testdata/test-git/git", "./testdata/test-git/.git")
	defer os.Rename("./testdata/test-git/.git", "./testdata/test-git/git")
	defer os.RemoveAll("./testdata/test-git-commit")

	// First clone the repo into a new one so we can push and delete
	git.PlainClone("./testdata/test-git-commit", true, &git.CloneOptions{
		URL: "./testdata/test-git",
	})

	var wg sync.WaitGroup
	wg.Add(1)
	go common.StreamingTestSubscriber("WorkflowDefinition.Update", "WorkflowDefinition.UpdateQueue", Update, natsInfo, &wg)

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)

	wfd := WorkflowDefinition{
		ID: "xid",
		Repository: &common.Repository{
			RawURL: "./testdata/test-git-commit",
			URL:    "./testdata/test-git-commit",
			Branch: "master",
		},
		Name:        "workflow-definition-1",
		Description: "This is the first WorkflowDefinition",
		Owner:       "test_user",
		Type:        "default",
		Workflow:    types.ContainerWorkflow(nil),
	}
	wfd.save()
	_, err := bson.Marshal(wfd)
	assert.NoError(t, err)

	// Edit and write to the repository, then reverse changes on our object
	oldName := wfd.Name
	wfd.Name = "test-name"
	_, err = bson.Marshal(wfd)
	assert.NoError(t, err)
	_, err = wfd.writeToGit("Commit", user.Secrets["ID"])
	assert.NoError(t, err)
	wfd.Name = oldName
	wfdBson, err := bson.Marshal(wfd)
	assert.NoError(t, err)

	request, err := json.Marshal(&Request{
		BasicRequest: common.BasicRequest{
			WorkflowID: "xid",
		},
	})
	assert.NoError(t, err)

	adapter.GetMock().On("Read", "xid").Return(wfdBson, nil)
	adapter.GetMock().On("Replace", mock.Anything).Return(nil)
	common.StreamingPublish(
		request,
		natsInfo,
		"WorkflowDefinition.Update",
		"WorkflowDefinitionTest",
	)
	wg.Wait()

	adapter.GetMock().On("Read", "xid").Return(wfdBson, nil)

	// Since this test cannot get user, there should be no change
	wfdNew, err := get("xid", "test_user")
	assert.NoError(t, err)
	assert.Equal(t, &wfd, wfdNew)
}

func TestNatsGet(t *testing.T) {
	s := runServer(t)
	defer s.Shutdown()

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber("WorkflowDefinition.Get", "WorkflowDefinition.GetQueue", Get, natsInfo, &wg)
	time.Sleep(100 * time.Millisecond)

	wfdBson, err := bson.Marshal(wfd)
	assert.NoError(t, err)
	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Read", "xid").Return(wfdBson, nil)

	msg, err := common.PublishRequest(
		&common.BasicRequest{WorkflowID: "xid", User: user},
		"WorkflowDefinition.Get",
		"WorkflowDefinitionTest",
		natsInfo["address"],
	)
	wg.Wait()
	assert.NoError(t, err)

	var result struct {
		WorkflowDefinition *WorkflowDefinition `json:"data"`
	}
	err = json.Unmarshal(msg, &result)
	assert.NoError(t, err)
	assert.Equal(t, "workflow-definition-1", result.WorkflowDefinition.Name)
	assert.Equal(t, "This is the first WorkflowDefinition", result.WorkflowDefinition.Description)
	assert.Equal(t, "test_user", result.WorkflowDefinition.Owner)
	assert.Equal(t, "./testdata/test-git", result.WorkflowDefinition.Repository.URL)
	assert.Equal(t, "master", result.WorkflowDefinition.Repository.Branch)
}

func TestNatsGetError(t *testing.T) {
	s := runServer(t)
	defer s.Shutdown()

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber("WorkflowDefinition.Get", "WorkflowDefinition.GetQueue", Get, natsInfo, &wg)
	time.Sleep(100 * time.Millisecond)

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("Read", "xid").Return([]byte{}, fmt.Errorf("ERROR"))

	msg, err := common.PublishRequest(
		&common.BasicRequest{WorkflowID: "xid", User: user},
		"WorkflowDefinition.Get",
		"WorkflowDefinitionTest",
		natsInfo["address"],
	)
	wg.Wait()
	assert.NoError(t, err)

	var result struct {
		Error struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		} `json:"error"`
	}
	err = json.Unmarshal(msg, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result.Error)
	assert.Equal(t, 500, result.Error.Code)
	assert.Equal(t, "unable to get WorkflowDefinition 'xid': error reading from database: ERROR", result.Error.Message)
}

func TestNatsGetForUser(t *testing.T) {
	s := runServer(t)
	defer s.Shutdown()

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber("WorkflowDefinition.GetForUser", "WorkflowDefinition.GetForUserQueue", GetForUser, natsInfo, &wg)
	time.Sleep(100 * time.Millisecond)

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("ReadForUser", "test_user").Return([]map[string]interface{}{
		{
			"_id":         "xid",
			"name":        "workflow-definition-1",
			"description": "This is the first WorkflowDefinition",
			"repository": map[string]interface{}{
				"rawurl": "./testdata/test-git",
				"url":    "./testdata/test-git",
				"branch": "master",
			},
			"type":  "default",
			"owner": "test_user",
		},
		{
			"_id":         "xid",
			"name":        "workflow-definition-1",
			"description": "This is the first WorkflowDefinition",
			"repository": map[string]interface{}{
				"rawurl": "./testdata/test-git",
				"url":    "./testdata/test-git",
				"branch": "not-master",
			},
			"type":  "default",
			"owner": "test_user",
		},
	}, nil)

	msg, err := common.PublishRequest(
		&common.BasicRequest{User: user},
		"WorkflowDefinition.GetForUser",
		"WorkflowDefinitionTest",
		natsInfo["address"],
	)
	wg.Wait()
	assert.NoError(t, err)

	var result struct {
		WorkflowDefinitions []WorkflowDefinition `json:"data"`
	}
	err = json.Unmarshal(msg, &result)
	assert.NoError(t, err)
	assert.Len(t, result.WorkflowDefinitions, 2)
	assert.Equal(t, "master", result.WorkflowDefinitions[0].Repository.Branch)
	assert.Equal(t, "not-master", result.WorkflowDefinitions[1].Repository.Branch)
}

func TestNatsGetForUserError(t *testing.T) {
	s := runServer(t)
	defer s.Shutdown()

	var wg sync.WaitGroup
	wg.Add(1)
	go common.SynchronousTestSubscriber("WorkflowDefinition.GetForUser", "WorkflowDefinition.GetForUserQueue", GetForUser, natsInfo, &wg)
	time.Sleep(100 * time.Millisecond)

	adapter = common.MockedAdapter{new(mock.Mock)}
	adapter.GetMock().On("ReadForUser", "test_user").Return([]map[string]interface{}{}, fmt.Errorf("ERROR"))

	msg, err := common.PublishRequest(
		&common.BasicRequest{User: user},
		"WorkflowDefinition.GetForUser",
		"WorkflowDefinitionTest",
		natsInfo["address"],
	)
	wg.Wait()
	assert.NoError(t, err)

	var result struct {
		Error struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		} `json:"error"`
	}
	err = json.Unmarshal(msg, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result.Error)
	assert.Equal(t, 500, result.Error.Code)
	assert.Equal(t, "unable to get WorkflowDefinitions for 'test_user': ERROR", result.Error.Message)
}
