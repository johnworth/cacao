package types

import (
	"fmt"
	"os"

	"gitlab.com/cyverse/cacao/common"

	api "istio.io/api/networking/v1beta1"
	istio "istio.io/client-go/pkg/apis/networking/v1beta1"
	core "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	defaultAPIHost = "ca.cyverse.org"
	istioNamespace = "istio-system"
)

var (
	cacaoAPIHost string
)

func init() {
	cacaoAPIHost = os.Getenv("CACAO_API_HOST")
	if len(cacaoAPIHost) == 0 {
		cacaoAPIHost = defaultAPIHost
	}
}

func createIstioIngressService(name string, ports []core.ServicePort) *core.Service {
	return &core.Service{
		ObjectMeta: meta.ObjectMeta{
			Name:      name,
			Namespace: "istio-system",
			Labels: map[string]string{
				"app":                         "istio-ingressgateway",
				"istio":                       "ingressgateway",
				"operator.istio.io/component": "IngressGateways",
				"operator.istio.io/managed":   "Reconcile",
				"release":                     "istio",
			},
		},
		Spec: core.ServiceSpec{
			Ports: ports,
			Selector: map[string]string{
				"app":   "istio-ingressgateway",
				"istio": "ingressgateway",
			},
			Type: core.ServiceTypeLoadBalancer,
		},
	}
}

// createDestinationRule creates a new Istio DestinationRule to route to a K8s
// Service based on app name and version
func createDestinationRule(name, id, version string) *istio.DestinationRule {
	return &istio.DestinationRule{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"app":      name,
				"version":  version,
				"cacao_id": id,
			},
		},
		Spec: api.DestinationRule{
			Host: name,
			Subsets: []*api.Subset{
				{
					Name: version,
					Labels: map[string]string{
						"app":      name,
						"version":  version,
						"cacao_id": id,
					},
				},
			},
		},
	}
}

// createHTTPRoute is used to create a Route that will be used as part of a VirtualService
func createHTTPRoute(host, version, path string, port int32) *api.HTTPRoute {
	return &api.HTTPRoute{
		Match: []*api.HTTPMatchRequest{
			{
				Uri: &api.StringMatch{
					MatchType: &api.StringMatch_Prefix{
						Prefix: path + "/",
					},
				},
			},
			{
				Uri: &api.StringMatch{
					MatchType: &api.StringMatch_Prefix{
						Prefix: path,
					},
				},
			},
		},
		Rewrite: &api.HTTPRewrite{Uri: "/"},
		Route: []*api.HTTPRouteDestination{
			{
				Destination: &api.Destination{
					Host:   host,
					Subset: version,
					Port:   &api.PortSelector{Number: uint32(port)},
				},
			},
		},
	}
}

// createTCPRoute is used to create a Route that will be used as part of a VirtualService
func createTCPRoute(name, version string, port int32) *api.TCPRoute {
	return &api.TCPRoute{
		Match: []*api.L4MatchAttributes{
			{Port: uint32(port)},
		},
		Route: []*api.RouteDestination{
			{
				Destination: &api.Destination{
					Host:   name,
					Subset: version,
					Port:   &api.PortSelector{Number: uint32(port)},
				},
			},
		},
	}
}

// createVirtualService creates an Istio VirtualService to control routing
// from an endpoint like "/user/workflow" to the correct K8s Service
func createVirtualService(name, host, version, uid string, httpRoutes []*api.HTTPRoute, tcpRoutes []*api.TCPRoute) *istio.VirtualService {
	// Now create and return VirtualService
	return &istio.VirtualService{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"app":      name,
				"version":  version,
				"cacao_id": uid,
			},
		},
		Spec: api.VirtualService{
			Gateways: []string{"cacao-gateway", name},
			Hosts:    []string{host},
			Http:     httpRoutes,
			Tcp:      tcpRoutes,
		},
	}
}

// createTCPGateway ...
func createTCPGateway(name string, ports []core.ServicePort) *istio.Gateway {
	var servers []*api.Server
	for _, p := range ports {
		servers = append(servers, &api.Server{
			Hosts: []string{"*"},
			Port: &api.Port{
				Number:   uint32(p.Port),
				Name:     p.Name,
				Protocol: "TCP",
			},
		})
	}
	return &istio.Gateway{
		ObjectMeta: meta.ObjectMeta{
			Name: name,
		},
		Spec: api.Gateway{
			Selector: map[string]string{"istio": "ingressgateway"},
			Servers:  servers,
		},
	}
}

func standardIstioRouting(clientsets *common.K8sClientsets, adminClientsets *common.K8sClientsets, request map[string]interface{}, name, namespace string, cluster common.Cluster) (err error) {
	runID := request["id"].(string)
	version := request["version"].(string)
	httpPorts := request["httpPorts"].([]core.ServicePort)
	tcpPorts := request["tcpPorts"].([]core.ServicePort)

	// Create DestinationRule
	dr := createDestinationRule(name, runID, version)
	_, err = clientsets.CreateDestinationRule(dr, namespace)
	if err != nil && err.Error() == fmt.Sprintf(`destinationrules.networking.istio.io "%s" already exists`, dr.Name) {
		_, err = clientsets.UpdateDestinationRule(dr, namespace)
	}
	if err != nil {
		return fmt.Errorf("Error creating DestinationRule: %v", err)
	}

	// Create routes for VirtualService
	var httpRoutes []*api.HTTPRoute
	for _, p := range httpPorts {
		path := fmt.Sprintf("/%s:%d", runID, p.Port)
		httpRoutes = append(httpRoutes, createHTTPRoute(name, version, path, p.Port))
	}
	var tcpRoutes []*api.TCPRoute
	for _, p := range tcpPorts {
		tcpRoutes = append(tcpRoutes, createTCPRoute(name, version, p.Port))
	}

	// Create VirtualService
	vs := createVirtualService(name, cluster.Host, version, runID, httpRoutes, tcpRoutes)
	_, err = clientsets.CreateVirtualService(vs, namespace)
	if err != nil && err.Error() == fmt.Sprintf(`virtualservices.networking.istio.io "%s" already exists`, vs.Name) {
		_, err = clientsets.UpdateVirtualService(vs, namespace)
	}
	if err != nil {
		return fmt.Errorf("Error creating VirtualService: %v", err)
	}

	// TCP things
	if len(tcpPorts) > 0 {
		tcpGateway := createTCPGateway(name, tcpPorts)
		_, err = clientsets.CreateGateway(tcpGateway, namespace)
		if err != nil && err.Error() != fmt.Sprintf(`gateways.networking.istio.io "%s" already exists`, tcpGateway.Name) {
			return fmt.Errorf("Error creating TCP Gateway: %v", err)
		}
		ingressService := createIstioIngressService(name, tcpPorts)
		_, err = adminClientsets.CreateService(ingressService, istioNamespace)
		if err != nil {
			return fmt.Errorf("Error creating Service for istio-ingressgateway: %v", err)
		}
	}
	return err
}
