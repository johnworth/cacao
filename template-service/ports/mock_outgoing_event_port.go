package ports

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/template-service/types"
)

/////////////////////////////////////////////////////////////////////////
// TemplateType
/////////////////////////////////////////////////////////////////////////

// TypeCreatedHandler is a handler for TypeCreated event
type TypeCreatedHandler func(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// TypeCreateFailedHandler is a handler for TypeCreateFailed event
type TypeCreateFailedHandler func(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// TypeUpdatedHandler is a handler for TypeUpdated event
type TypeUpdatedHandler func(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// TypeUpdateFailedHandler is a handler for TypeUpdateFailed event
type TypeUpdateFailedHandler func(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// TypeDeletedHandler is a handler for TypeDeleted event
type TypeDeletedHandler func(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// TypeDeleteFailedHandler is a handler for TypeDeleteFailed event
type TypeDeleteFailedHandler func(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

/////////////////////////////////////////////////////////////////////////
// Template
/////////////////////////////////////////////////////////////////////////

// ImportedHandler is a handler for Imported event
type ImportedHandler func(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// ImportFailedHandler is a handler for ImportFailed event
type ImportFailedHandler func(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// UpdatedHandler is a handler for Updated event
type UpdatedHandler func(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// UpdateFailedHandler is a handler for UpdateFailed event
type UpdateFailedHandler func(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// DeletedHandler is a handler for Deleted event
type DeletedHandler func(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// DeleteFailedHandler is a handler for DeleteFailed event
type DeleteFailedHandler func(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// SyncedHandler is a handler for Synced event
type SyncedHandler func(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// SyncFailedHandler is a handler for SyncFailed event
type SyncFailedHandler func(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// MockOutgoingEventPort is a mock implementation of OutgoingEventPort
type MockOutgoingEventPort struct {
	Config              *types.Config
	ImportedHandler     ImportedHandler
	ImportFailedHandler ImportFailedHandler
	UpdatedHandler      UpdatedHandler
	UpdateFailedHandler UpdateFailedHandler
	DeletedHandler      DeletedHandler
	DeleteFailedHandler DeleteFailedHandler
	SyncedHandler       SyncedHandler
	SyncFailedHandler   SyncFailedHandler
}

// Init inits the port
func (port *MockOutgoingEventPort) Init(config *types.Config) {
	port.Config = config
}

// Finalize finalizes the port
func (port *MockOutgoingEventPort) Finalize() {}

// SetImportedHandler sets a handler for Imported event
func (port *MockOutgoingEventPort) SetImportedHandler(importedHandler ImportedHandler) {
	port.ImportedHandler = importedHandler
}

// SetImportFailedHandler sets a handler for ImportFailed event
func (port *MockOutgoingEventPort) SetImportFailedHandler(importFailedHandler ImportFailedHandler) {
	port.ImportFailedHandler = importFailedHandler
}

// SetUpdatedHandler sets a handler for Updated event
func (port *MockOutgoingEventPort) SetUpdatedHandler(updatedHandler UpdatedHandler) {
	port.UpdatedHandler = updatedHandler
}

// SetUpdateFailedHandler sets a handler for UpdateFailed event
func (port *MockOutgoingEventPort) SetUpdateFailedHandler(updateFailedHandler UpdateFailedHandler) {
	port.UpdateFailedHandler = updateFailedHandler
}

// SetDeletedHandler sets a handler for Deleted event
func (port *MockOutgoingEventPort) SetDeletedHandler(deletedHandler DeletedHandler) {
	port.DeletedHandler = deletedHandler
}

// SetDeleteFailedHandler sets a handler for DeleteFailed event
func (port *MockOutgoingEventPort) SetDeleteFailedHandler(deleteFailedHandler DeleteFailedHandler) {
	port.DeleteFailedHandler = deleteFailedHandler
}

// SetSyncedHandler sets a handler for Synced event
func (port *MockOutgoingEventPort) SetSyncedHandler(syncedHandler SyncedHandler) {
	port.SyncedHandler = syncedHandler
}

// SetSyncFailedHandler sets a handler for SyncFailed event
func (port *MockOutgoingEventPort) SetSyncFailedHandler(syncFailedHandler SyncFailedHandler) {
	port.SyncFailedHandler = syncFailedHandler
}

// Imported icreates an event for template import
func (port *MockOutgoingEventPort) Imported(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.ImportedHandler(actor, emulator, template, status, transactionID)
}

// ImportFailed creates an event for template import failure
func (port *MockOutgoingEventPort) ImportFailed(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.ImportFailedHandler(actor, emulator, template, status, transactionID)
}

// Updated creates an event for template update
func (port *MockOutgoingEventPort) Updated(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.UpdatedHandler(actor, emulator, template, status, transactionID)
}

// UpdateFailed creates an event for template update failure
func (port *MockOutgoingEventPort) UpdateFailed(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.UpdateFailedHandler(actor, emulator, template, status, transactionID)
}

// Deleted creates an event for template deletion
func (port *MockOutgoingEventPort) Deleted(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.DeletedHandler(actor, emulator, template, status, transactionID)
}

// DeleteFailed creates an event for template deletion failure
func (port *MockOutgoingEventPort) DeleteFailed(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.DeleteFailedHandler(actor, emulator, template, status, transactionID)
}

// Synced creates an event for template sync succeeded
func (port *MockOutgoingEventPort) Synced(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.SyncedHandler(actor, emulator, template, status, transactionID)
}

// SyncFailed creates an event for template sync failure
func (port *MockOutgoingEventPort) SyncFailed(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.SyncFailedHandler(actor, emulator, template, status, transactionID)
}
