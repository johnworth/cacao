package fixtures

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/template-service/ports"
	"gitlab.com/cyverse/cacao/template-service/types"

	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
)

const (
	// TemplateTypeFixturesFilename is the name of a file containing fixtures
	TemplateTypeFixturesFilename string = "template_type_fixtures.json"
)

// DataFixtures communicates to IncomingEventPort to add fixtures
type DataFixtures struct {
	Config            *types.Config
	IncomingEventPort ports.IncomingEventPort
	IncomingQueryPort ports.IncomingQueryPort
}

// Init initializes the fixtures
func (fixtures *DataFixtures) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.fixtures",
		"function": "DataFixtures.Init",
	})

	logger.Info("initializing DataFixtures")

	fixtures.Config = config
}

// Start starts adding the fixtures
func (fixtures *DataFixtures) Start() {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.fixtures",
		"function": "DataFixtures.Start",
	})

	logger.Info("starting DataFixtures")

	actor := "cacao-admin"
	emulator := actor

	fixtures.addTemplateTypes(actor, emulator)
}

func (fixtures *DataFixtures) getTemplateTypeFixtures() ([]types.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.fixtures",
		"function": "DataFixtures.getTemplateTypeFixtures",
	})

	// read template_type_fixtures.json
	absFixturesFilename, err := filepath.Abs(TemplateTypeFixturesFilename)
	if err != nil {
		errorMessage := fmt.Sprintf("Could not access the fixtures file %s", TemplateTypeFixturesFilename)
		logger.WithError(err).Errorf(errorMessage)
		return []types.TemplateType{}, fmt.Errorf(errorMessage)
	}

	_, err = os.Stat(absFixturesFilename)
	if err != nil {
		logger.WithError(err).Errorf("the fixture file (%s) error", absFixturesFilename)
		return []types.TemplateType{}, err
	}

	jsonBytes, err := ioutil.ReadFile(absFixturesFilename)
	if err != nil {
		logger.WithError(err).Errorf("Could not read the fixture file %s", absFixturesFilename)
		return []types.TemplateType{}, err
	}

	templateTypeFixtures := []types.TemplateType{}

	err = json.Unmarshal(jsonBytes, &templateTypeFixtures)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into []TemplateType"
		logger.WithError(err).Error(errorMessage)
		return []types.TemplateType{}, fmt.Errorf(errorMessage)
	}

	return templateTypeFixtures, nil
}

func (fixtures *DataFixtures) addTemplateTypes(actor string, emulator string) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.fixtures",
		"function": "DataFixtures.addTemplateTypes",
	})

	templateTypes, err := fixtures.IncomingQueryPort.ListTypes(actor, emulator)
	if err != nil {
		logger.WithError(err).Fatal("unable to list template types")
	}

	if len(templateTypes) == 0 {
		// add fixtures only when there is no template types registered
		logger.Info("adding template types")

		templateTypes, err := fixtures.getTemplateTypeFixtures()
		if err != nil {
			logger.WithError(err).Fatal("unable to get template type fixtures")
		}

		for _, templateType := range templateTypes {
			logger.Infof(`adding template type "%s"`, templateType.Name)

			transactionID := cacao_common_messaging.NewTransactionID()
			err := fixtures.IncomingEventPort.CreateType(actor, emulator, templateType, transactionID)
			if err != nil {
				logger.WithError(err).Errorf("failed to add template type %s", templateType.Name)
			}
		}

		logger.Infof("added %d template types", len(templateTypes))
	} else {
		logger.Infof("there are %d template types already - skip", len(templateTypes))
	}
}
