package main

import (
	"sync"
	"time"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"

	"gitlab.com/cyverse/cacao/template-service/adapters"
	"gitlab.com/cyverse/cacao/template-service/domain"
	"gitlab.com/cyverse/cacao/template-service/fixtures"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func main() {
	var config types.Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.Fatal(err.Error())
	}

	// This is to overwrite default cluster id
	config.StanConfig.ClusterID = types.DefaultNatsClusterID
	// This is to overwrite default nats subject
	config.NatsConfig.WildcardSubject = types.DefaultNatsWildcardSubject

	// other empty parameters will be filled with defaults
	config.ProcessDefaults()

	// create an initial Domain object
	var dmain domain.Domain
	dmain.Init(&config)

	// init port implementations
	queryInImpl := &domain.QueryPortImpl{}
	queryInImpl.Init(&config)
	dmain.QueryIn = queryInImpl

	eventInImpl := &domain.EventPortImpl{}
	eventInImpl.Init(&config)
	dmain.EventIn = eventInImpl

	// add and initialize the storage adapter
	mongoAdapter := &adapters.MongoAdapter{}
	mongoAdapter.Init(&config)
	dmain.Storage = mongoAdapter

	// add and initialize the template source adapter
	gitAdapter := &adapters.GitAdapter{}
	gitAdapter.Init(&config)
	dmain.TemplateSource = gitAdapter

	// add and initialize the credential adapter
	credentialAdapter := &adapters.CredentialAdapter{}
	credentialAdapter.Init(&config)
	dmain.Credential = credentialAdapter

	// add and initialize the query adapter
	queryAdapter := &adapters.QueryAdapter{}
	queryAdapter.Init(&config)
	queryAdapter.IncomingPort = queryInImpl

	eventAdapter := &adapters.EventAdapter{}
	eventAdapter.Init(&config)
	eventAdapter.IncomingPort = eventInImpl

	dmain.EventOut = eventAdapter

	// initialize the fixtures
	dataFixtures := &fixtures.DataFixtures{}
	dataFixtures.Init(&config)
	dataFixtures.IncomingQueryPort = queryInImpl
	dataFixtures.IncomingEventPort = eventInImpl

	// start
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		dmain.Start()
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		queryAdapter.Start()
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		eventAdapter.Start()
	}()

	wg.Add(1)
	go func() {
		// start with delay
		time.Sleep(1 * time.Second)
		defer wg.Done()
		dataFixtures.Start()
	}()

	wg.Wait()

	// finalize
	eventAdapter.Finalize()
	queryAdapter.Finalize()

	eventInImpl.Finalize()
	queryInImpl.Finalize()

	dmain.Finalize()
}
