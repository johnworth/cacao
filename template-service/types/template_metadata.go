package types

import (
	cacao_common_format "gitlab.com/cyverse/cacao-common/format"
)

// TemplateParameterValue is used to store values stores a parameter value
type TemplateParameterValue struct {
	Default     interface{}   `json:"default,omitempty"`
	Type        string        `json:"type,omitempty"`
	Enum        []interface{} `json:"enum,omitempty"`
	UILabel     string        `json:"ui_label,omitempty"`
	Description string        `json:"description,omitempty"`
}

// TemplateParameters stores parameters
type TemplateParameters map[string]TemplateParameterValue

const (
	// TemplateMetadataCacaoPostTaskSchema is a schema for CacaoPostTask data type
	TemplateMetadataCacaoPostTaskSchema string = `
	{
		"$schema": "http://json-schema.org/draft-07/schema#",
		"type": "object",
		"properties": {
			"type": {
				"type": "string"
			},
			"location_type": {
				"type": "string"
			}
		},
		"required": ["type"],
		"additionalProperties": true
	}`

	// TemplateMetadataParameterDataTypeSchema is a schema for a data type used in Parameters field of TemplateMetadata struct
	TemplateMetadataParameterDataTypeSchema string = `
	{
		"$schema": "http://json-schema.org/draft-07/schema#",
		"type": "object",
		"properties": {
			"type": {
				"type": "string"
			},
			"default": {
				"type": ["number", "string", "boolean"]
			},
			"enum": {
				"type": "array",
				"items": {
					"type": ["number", "string", "boolean"]
				}
			},
			"ui_label": {
				"type": "string"
			},
			"description": {
				"type": "string"
			}
		},
		"required": ["type"],
		"additionalProperties": true
	}`

	// TemplateMetadataSchema is a schema for TemplateMetadata data type
	TemplateMetadataSchema string = `
	{
		"$schema": "http://json-schema.org/draft-07/schema#",
	 	"definitions": {
			"cacao_post_task": ` + TemplateMetadataCacaoPostTaskSchema + `,
			"parameter_data_type": ` + TemplateMetadataParameterDataTypeSchema + `
	 	},
	 	"type": "object",
	 	"properties": {
			"name": {
				"type": "string"
			},
			"author": {
				"type": "string"
			},
			"author_email": {
				"type": "string"
			},
			"description": {
				"type": "string"
			},
			"template_type": {
				"type": "string"
			},
			"cacao_post_tasks": {
				"type": "array",
				"items": {
					"$ref": "#/definitions/cacao_post_task"
				}
			},
			"parameters": {
				"type": "object",
				"properties": {
					"username": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"instance_count": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"instance_name": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"instance_uuid": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"instance_id": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"share_uuid": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"share_name": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"share_description": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"share_protocol": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"share_type": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"share_size": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"share_network_uuid": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"flavor": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"project": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"keypair": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"keypair_name": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"public_ssh_key": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"image": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"power_state": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"ip_pool": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"external_network_uuid": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"external_subnet_uuid": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"tenant_cidr": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"volume_id": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"volume_name": {
						"$ref": "#/definitions/parameter_data_type"
					},
					"volume_size": {
						"$ref": "#/definitions/parameter_data_type"
					}
				},
				"additionalProperties": true
			}
		},
		"required": ["name", "author", "template_type"],
		"additionalProperties": false
	}`
)

// ValidateTemplateMetadataJSON validates JSON formatted template metdata, returns error if the format is not valid
func ValidateTemplateMetadataJSON(data []byte) error {
	return cacao_common_format.ValidateJSONSchema(data, []byte(TemplateMetadataSchema))
}
