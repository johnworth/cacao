package types

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// ConvertTypeFromModel converts TemplateTypeModel to TemplateType
func ConvertTypeFromModel(model cacao_common_service.TemplateTypeModel) TemplateType {
	templateType := TemplateType{
		Name:          model.Name,
		Formats:       model.Formats,
		Engine:        model.Engine,
		ProviderTypes: model.ProviderTypes,
	}

	return templateType
}

// ConvertFromModel converts TemplateModel to Template
func ConvertFromModel(model cacao_common_service.TemplateModel) Template {
	cacaoPostTasks := []CacaoPostTask{}
	for _, modelCacaoPostTask := range model.Metadata.CacaoPostTasks {
		cacaoPostTask := CacaoPostTask{
			Type:         modelCacaoPostTask.Type,
			LocationType: modelCacaoPostTask.LocationType,
		}
		cacaoPostTasks = append(cacaoPostTasks, cacaoPostTask)
	}

	template := Template{
		ID:          model.ID,
		Owner:       model.Owner,
		Name:        model.Name,
		Description: model.Description,
		Public:      model.Public,
		Source: TemplateSource{
			Type:             model.Source.Type,
			URI:              model.Source.URI,
			AccessParameters: model.Source.AccessParameters,
			Visibility:       model.Source.Visibility,
		},
		Metadata: TemplateMetadata{
			Name:             model.Metadata.Name,
			Author:           model.Metadata.Author,
			AuthorEmail:      model.Metadata.AuthorEmail,
			Description:      model.Metadata.Description,
			TemplateTypeName: model.Metadata.TemplateTypeName,
			CacaoPostTasks:   cacaoPostTasks,
			Parameters:       model.Metadata.Parameters,
		},
		CreatedAt: model.CreatedAt,
		UpdatedAt: model.UpdatedAt,
	}

	return template
}

// ConvertTypeToModel converts TemplateType to TemplateTypeModel
func ConvertTypeToModel(session cacao_common_service.Session, templateType TemplateType) cacao_common_service.TemplateTypeModel {
	return cacao_common_service.TemplateTypeModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
			ErrorType:       session.ErrorType,
			ErrorMessage:    session.ErrorMessage,
			ServiceError:    session.ServiceError,
		},
		Name:          templateType.Name,
		Formats:       templateType.Formats,
		Engine:        templateType.Engine,
		ProviderTypes: templateType.ProviderTypes,
	}
}

// ConvertTypeToListItemModel converts TemplateType to TemplateTypeListItemModel
func ConvertTypeToListItemModel(templateType TemplateType) cacao_common_service.TemplateTypeListItemModel {
	return cacao_common_service.TemplateTypeListItemModel{
		Name:          templateType.Name,
		Formats:       templateType.Formats,
		Engine:        templateType.Engine,
		ProviderTypes: templateType.ProviderTypes,
	}
}

// ConvertToModel converts Template to TemplateModel
func ConvertToModel(session cacao_common_service.Session, template Template) cacao_common_service.TemplateModel {
	cacaoPostTasks := []cacao_common_service.CacaoPostTask{}
	for _, templateCacaoPostTask := range template.Metadata.CacaoPostTasks {
		cacaoPostTask := cacao_common_service.CacaoPostTask{
			Type:         templateCacaoPostTask.Type,
			LocationType: templateCacaoPostTask.LocationType,
		}
		cacaoPostTasks = append(cacaoPostTasks, cacaoPostTask)
	}

	return cacao_common_service.TemplateModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
			ErrorType:       session.ErrorType,
			ErrorMessage:    session.ErrorMessage,
			ServiceError:    session.ServiceError,
		},
		ID:          template.ID,
		Owner:       template.Owner,
		Name:        template.Name,
		Description: template.Description,
		Public:      template.Public,
		Source: cacao_common_service.TemplateSource{
			Type:             template.Source.Type,
			URI:              template.Source.URI,
			AccessParameters: template.Source.AccessParameters,
			Visibility:       template.Source.Visibility,
		},
		Metadata: cacao_common_service.TemplateMetadata{
			Name:             template.Metadata.Name,
			Author:           template.Metadata.Author,
			AuthorEmail:      template.Metadata.AuthorEmail,
			Description:      template.Metadata.Description,
			TemplateTypeName: template.Metadata.TemplateTypeName,
			CacaoPostTasks:   cacaoPostTasks,
			Parameters:       template.Metadata.Parameters,
		},
		CreatedAt: template.CreatedAt,
		UpdatedAt: template.UpdatedAt,
	}
}

// ConvertToListItemModel converts Template to TemplateListItemModel
func ConvertToListItemModel(template Template) cacao_common_service.TemplateListItemModel {
	cacaoPostTasks := []cacao_common_service.CacaoPostTask{}
	for _, templateCacaoPostTask := range template.Metadata.CacaoPostTasks {
		cacaoPostTask := cacao_common_service.CacaoPostTask{
			Type:         templateCacaoPostTask.Type,
			LocationType: templateCacaoPostTask.LocationType,
		}
		cacaoPostTasks = append(cacaoPostTasks, cacaoPostTask)
	}

	return cacao_common_service.TemplateListItemModel{
		ID:          template.ID,
		Owner:       template.Owner,
		Name:        template.Name,
		Description: template.Description,
		Public:      template.Public,
		Source: cacao_common_service.TemplateSource{
			Type:             template.Source.Type,
			URI:              template.Source.URI,
			AccessParameters: template.Source.AccessParameters,
			Visibility:       template.Source.Visibility,
		},
		Metadata: cacao_common_service.TemplateMetadata{
			Name:             template.Metadata.Name,
			Author:           template.Metadata.Author,
			AuthorEmail:      template.Metadata.AuthorEmail,
			Description:      template.Metadata.Description,
			TemplateTypeName: template.Metadata.TemplateTypeName,
			CacaoPostTasks:   cacaoPostTasks,
			Parameters:       template.Metadata.Parameters,
		},
		CreatedAt: template.CreatedAt,
		UpdatedAt: template.UpdatedAt,
	}
}
