package types

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// TemplateType is a struct for storing template type information
type TemplateType struct {
	Name          string                                      `bson:"_id" json:"name,omitempty"`
	Formats       []cacao_common_service.TemplateFormat       `bson:"formats" json:"formats,omitempty"`
	Engine        cacao_common_service.TemplateEngine         `bson:"engine" json:"engine"`
	ProviderTypes []cacao_common_service.TemplateProviderType `bson:"provider_types" json:"provider_types,omitempty"`
}

// TemplateSource is a struct for storing template source information
type TemplateSource struct {
	Type cacao_common_service.TemplateSourceType `bson:"type" json:"type,omitempty"`
	URI  string                                  `bson:"uri" json:"uri,omitempty"`
	// AccessParamters is used to pass additional parameters for accessing source (e.g., git branch name ...)
	// e.g., for git,
	// "branch": "master"
	// "tag": "tag_test"
	// "path": "/sub_dir_to_template_in_repo"
	AccessParameters map[string]interface{}                        `bson:"access_parameters" json:"access_parameters,omitempty"`
	Visibility       cacao_common_service.TemplateSourceVisibility `bson:"source_visibility" json:"source_visibility,omitempty"`
}

// CacaoPostTask is a struct for a post task in metadata
type CacaoPostTask struct {
	Type         string `bson:"type" json:"type,omitempty"`
	LocationType string `bson:"location_type" json:"location_type,omitempty"`
}

// TemplateMetadata is a struct for template metadata
// The struct is generated from a metadata file (e.g., https://github.com/cyverse/tf-openstack-single-image/blob/master/metadata.json)
type TemplateMetadata struct {
	Name             string          `bson:"name" json:"name,omitempty"`
	Author           string          `bson:"author" json:"author,omitempty"`
	AuthorEmail      string          `bson:"author_email" json:"author_email,omitempty"`
	Description      string          `bson:"description" json:"description,omitempty"`
	TemplateTypeName string          `bson:"template_type" json:"template_type,omitempty"`
	CacaoPostTasks   []CacaoPostTask `bson:"cacao_post_tasks" json:"cacao_post_tasks,omitempty"`
	// Parameters stores template parameters
	// e.g.,
	// "instance_count": {"default": 1, "type": "integer"}
	// "instance_name": {"type": "string"},
	Parameters map[string]interface{} `bson:"parameters" json:"parameters,omitempty"`
}

// Template is a struct for storing template information
type Template struct {
	ID          common.ID        `bson:"_id" json:"id,omitempty"`
	Owner       string           `bson:"owner" json:"owner,omitempty"`
	Name        string           `bson:"name" json:"name,omitempty"`
	Description string           `bson:"description" json:"description,omitempty"`
	Public      bool             `bson:"public" json:"public,omitempty"`
	Source      TemplateSource   `bson:"source" json:"source,omitempty"`
	Metadata    TemplateMetadata `bson:"metadata" json:"metadata,omitempty"`
	CreatedAt   time.Time        `bson:"created_at" json:"created_at,omitempty"`
	UpdatedAt   time.Time        `bson:"updated_at" json:"updated_at,omitempty"`
}
