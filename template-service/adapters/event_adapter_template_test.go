package adapters

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func TestTemplateImportEvent(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testTemplateName := "test_template1"
	testTemplateID := cacao_common_service.NewTemplateID()
	testCredentialID := "test_credential1"

	eventData := cacao_common_service.TemplateModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID:          testTemplateID,
		Name:        testTemplateName,
		Description: "description",
		Public:      true,
		Source: cacao_common_service.TemplateSource{
			Type: types.TemplateSourceTypeGit,
			URI:  "https://github.com/cyverse/tf-openstack-single-image",
			AccessParameters: map[string]interface{}{
				"branch": "master",
			},
			Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
		},
		CredentialID: testCredentialID,
	}

	eventInImpl.SetImportHandler(func(actor string, emulator string, template types.Template, credentialID string, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateID, template.ID)
		assert.Equal(t, testCredentialID, credentialID)
		return nil
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TemplateImportRequestedEvent, eventData)
	assert.NoError(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTemplateImportEventFailed(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testTemplateName := "test_template1"
	testTemplateID := cacao_common_service.NewTemplateID()
	testCredentialID := "test_credential1"

	eventData := cacao_common_service.TemplateModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID:          testTemplateID,
		Name:        testTemplateName,
		Description: "description",
		Public:      true,
		Source: cacao_common_service.TemplateSource{
			Type: types.TemplateSourceTypeGit,
			URI:  "https://github.com/cyverse/tf-openstack-single-image",
			AccessParameters: map[string]interface{}{
				"branch": "master",
			},
			Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
		},
		CredentialID: testCredentialID,
	}

	eventInImpl.SetImportHandler(func(actor string, emulator string, template types.Template, credentialID string, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateID, template.ID)
		assert.Equal(t, testCredentialID, credentialID)
		return fmt.Errorf("unable to import a template")
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TemplateImportRequestedEvent, eventData)
	assert.Error(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTemplateUpdateEvent(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testTemplateName := "test_template1"
	testTemplateID := cacao_common_service.NewTemplateID()

	eventData := cacao_common_service.TemplateModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID:          testTemplateID,
		Name:        testTemplateName,
		Description: "description",
		Public:      true,
		Source: cacao_common_service.TemplateSource{
			Type: types.TemplateSourceTypeGit,
			URI:  "https://github.com/cyverse/tf-openstack-single-image",
			AccessParameters: map[string]interface{}{
				"branch": "master",
			},
			Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
		},
	}

	eventInImpl.SetUpdateHandler(func(actor string, emulator string, template types.Template, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateID, template.ID)
		return nil
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TemplateUpdateRequestedEvent, eventData)
	assert.NoError(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTemplateUpdateEventFailed(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testTemplateName := "test_template1"
	testTemplateID := cacao_common_service.NewTemplateID()

	eventData := cacao_common_service.TemplateModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID:          testTemplateID,
		Name:        testTemplateName,
		Description: "description",
		Public:      true,
		Source: cacao_common_service.TemplateSource{
			Type: types.TemplateSourceTypeGit,
			URI:  "https://github.com/cyverse/tf-openstack-single-image",
			AccessParameters: map[string]interface{}{
				"branch": "master",
			},
			Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
		},
	}

	eventInImpl.SetUpdateHandler(func(actor string, emulator string, template types.Template, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateID, template.ID)
		return fmt.Errorf("unable to update a template")
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TemplateUpdateRequestedEvent, eventData)
	assert.Error(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTemplateDeleteEvent(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testTemplateID := cacao_common_service.NewTemplateID()

	eventData := cacao_common_service.TemplateModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID: testTemplateID,
	}

	eventInImpl.SetDeleteHandler(func(actor string, emulator string, template types.Template, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateID, template.ID)
		return nil
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TemplateDeleteRequestedEvent, eventData)
	assert.NoError(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTemplateDeleteEventFailed(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testTemplateID := cacao_common_service.NewTemplateID()

	eventData := cacao_common_service.TemplateModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID: testTemplateID,
	}

	eventInImpl.SetDeleteHandler(func(actor string, emulator string, template types.Template, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateID, template.ID)
		return fmt.Errorf("unable to delete a template")
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TemplateDeleteRequestedEvent, eventData)
	assert.Error(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTemplateSyncEvent(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testTemplateID := cacao_common_service.NewTemplateID()
	testCredentialID := "test_credential1"

	eventData := cacao_common_service.TemplateModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID:           testTemplateID,
		CredentialID: testCredentialID,
	}

	eventInImpl.SetSyncHandler(func(actor string, emulator string, template types.Template, credentialID string, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateID, template.ID)
		assert.Equal(t, testCredentialID, credentialID)
		return nil
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TemplateSyncRequestedEvent, eventData)
	assert.NoError(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTemplateSyncEventFailed(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testTemplateID := cacao_common_service.NewTemplateID()
	testCredentialID := "test_credential1"

	eventData := cacao_common_service.TemplateModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID:           testTemplateID,
		CredentialID: testCredentialID,
	}

	eventInImpl.SetSyncHandler(func(actor string, emulator string, template types.Template, credentialID string, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateID, template.ID)
		assert.Equal(t, testCredentialID, credentialID)
		return fmt.Errorf("unable to sync a template")
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TemplateSyncRequestedEvent, eventData)
	assert.Error(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTemplateImportedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testTemplateID := cacao_common_service.NewTemplateID()

	eventData := types.Template{
		ID:    testTemplateID,
		Owner: testUser,
		Name:  "test_template",
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TemplateImportedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TemplateImportedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.TemplateModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testTemplateID, response.ID)
		return nil
	})

	err := eventAdapter.Imported(testUser, testUser, eventData, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTemplateImportFailedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testTemplateID := cacao_common_service.NewTemplateID()

	eventData := types.Template{
		ID:    testTemplateID,
		Owner: testUser,
		Name:  "test_template",
	}

	testErrorMessage := "template is not created"

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TemplateImportFailedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TemplateImportFailedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.TemplateModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testTemplateID, response.ID)
		assert.Equal(t, testErrorMessage, response.GetServiceError().ContextualError())
		return nil
	})

	err := eventAdapter.ImportFailed(testUser, testUser, eventData, fmt.Errorf(testErrorMessage), testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTemplateUpdatedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testTemplateID := cacao_common_service.NewTemplateID()

	eventData := types.Template{
		ID:    testTemplateID,
		Owner: testUser,
		Name:  "test_template",
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TemplateUpdatedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TemplateUpdatedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.TemplateModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testTemplateID, response.ID)
		return nil
	})

	err := eventAdapter.Updated(testUser, testUser, eventData, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTemplateUpdateFailedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testTemplateID := cacao_common_service.NewTemplateID()

	eventData := types.Template{
		ID:    testTemplateID,
		Owner: testUser,
		Name:  "test_template",
	}

	testErrorMessage := "template is not updated"

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TemplateUpdateFailedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TemplateUpdateFailedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.TemplateModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testTemplateID, response.ID)
		assert.Equal(t, testErrorMessage, response.GetServiceError().ContextualError())
		return nil
	})

	err := eventAdapter.UpdateFailed(testUser, testUser, eventData, fmt.Errorf(testErrorMessage), testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTemplateDeletedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testTemplateID := cacao_common_service.NewTemplateID()

	eventData := types.Template{
		ID:    testTemplateID,
		Owner: testUser,
		Name:  "test_template",
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TemplateDeletedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TemplateDeletedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.TemplateModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testTemplateID, response.ID)
		return nil
	})

	err := eventAdapter.Deleted(testUser, testUser, eventData, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTemplateDeleteFailedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testTemplateID := cacao_common_service.NewTemplateID()

	eventData := types.Template{
		ID:    testTemplateID,
		Owner: testUser,
		Name:  "test_template",
	}

	testErrorMessage := "template is not deleted"

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TemplateDeleteFailedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TemplateDeleteFailedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.TemplateModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testTemplateID, response.ID)
		assert.Equal(t, testErrorMessage, response.GetServiceError().ContextualError())
		return nil
	})

	err := eventAdapter.DeleteFailed(testUser, testUser, eventData, fmt.Errorf(testErrorMessage), testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTemplateSyncedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testTemplateID := cacao_common_service.NewTemplateID()

	eventData := types.Template{
		ID:    testTemplateID,
		Owner: testUser,
		Name:  "test_template",
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TemplateSyncedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TemplateSyncedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.TemplateModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testTemplateID, response.ID)
		return nil
	})

	err := eventAdapter.Synced(testUser, testUser, eventData, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTemplateSyncFailedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testTemplateID := cacao_common_service.NewTemplateID()

	eventData := types.Template{
		ID:    testTemplateID,
		Owner: testUser,
		Name:  "test_template",
	}

	testErrorMessage := "template is not synced"

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TemplateSyncFailedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TemplateSyncFailedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.TemplateModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testTemplateID, response.ID)
		assert.Equal(t, testErrorMessage, response.GetServiceError().ContextualError())
		return nil
	})

	err := eventAdapter.SyncFailed(testUser, testUser, eventData, fmt.Errorf(testErrorMessage), testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}
