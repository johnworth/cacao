package adapters

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// List returns all templates owned by a user
func (adapter *MongoAdapter) List(user string) ([]types.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.List",
	})

	results := []types.Template{}

	filter := map[string]interface{}{
		"$or": []map[string]interface{}{
			{
				"owner": user,
			},
			{
				"public": true,
			},
		},
	}

	err := adapter.Store.ListConditional(adapter.Config.TemplateMongoDBCollectionName, filter, &results)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to list templates for the user %s", user)
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return results, nil
}

// MockList sets expected results for List
func (adapter *MongoAdapter) MockList(user string, expectedTemplates []types.Template, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	filter := map[string]interface{}{
		"$or": []map[string]interface{}{
			{
				"owner": user,
			},
			{
				"public": true,
			},
		},
	}

	mock.On("ListConditional", adapter.Config.TemplateMongoDBCollectionName, filter).Return(expectedTemplates, expectedError)
	return nil
}

// Get returns the template with the ID
func (adapter *MongoAdapter) Get(user string, templateID cacao_common.ID) (types.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.Get",
	})

	result := types.Template{}

	err := adapter.Store.Get(adapter.Config.TemplateMongoDBCollectionName, templateID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the template for id %s", templateID)
		logger.WithError(err).Error(errorMessage)
		return result, cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if !result.Public && result.Owner != user {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the template for id %s", templateID)
		logger.Error(errorMessage)
		return result, cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	return result, nil
}

// MockGet sets expected results for Get
func (adapter *MongoAdapter) MockGet(user string, templateID cacao_common.ID, expectedTemplate types.Template, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Get", adapter.Config.TemplateMongoDBCollectionName, templateID.String()).Return(expectedTemplate, expectedError)
	return nil
}

// Create inserts a template
func (adapter *MongoAdapter) Create(template types.Template) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.Create",
	})

	err := adapter.Store.Insert(adapter.Config.TemplateMongoDBCollectionName, template)
	if err != nil {
		if cacao_common_db.IsDuplicateError(err) {
			errorMessage := fmt.Sprintf("unable to insert a template because id %s conflicts", template.ID)
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.NewCacaoAlreadyExistError(errorMessage)
		}

		errorMessage := fmt.Sprintf("unable to insert a template with id %s", template.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockCreate sets expected results for Create
func (adapter *MongoAdapter) MockCreate(template types.Template, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Insert", adapter.Config.TemplateMongoDBCollectionName).Return(expectedError)
	return nil
}

// Update updates/edits a template
func (adapter *MongoAdapter) Update(template types.Template, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.Update",
	})

	// get and check ownership
	result := types.Template{}

	err := adapter.Store.Get(adapter.Config.TemplateMongoDBCollectionName, template.ID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the template for id %s", template.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != template.Owner {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the template for id %s", template.ID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	// update
	updated, err := adapter.Store.Update(adapter.Config.TemplateMongoDBCollectionName, template.ID.String(), template, updateFieldNames)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to update the template for id %s", template.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !updated {
		errorMessage := fmt.Sprintf("unable to update the template for id %s", template.ID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockUpdate sets expected results for Update
func (adapter *MongoAdapter) MockUpdate(existingTemplate types.Template, newTemplate types.Template, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.Config.TemplateMongoDBCollectionName, newTemplate.ID.String()).Return(existingTemplate, expectedError)
	mock.On("Update", adapter.Config.TemplateMongoDBCollectionName, newTemplate.ID.String()).Return(expectedResult, expectedError)
	return nil
}

// Delete deletes a template
func (adapter *MongoAdapter) Delete(user string, templateID cacao_common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.Delete",
	})

	// get and check ownership
	result := types.Template{}

	err := adapter.Store.Get(adapter.Config.TemplateMongoDBCollectionName, templateID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the template for id %s", templateID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != user {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the template for id %s", templateID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	// delete
	deleted, err := adapter.Store.Delete(adapter.Config.TemplateMongoDBCollectionName, templateID.String())
	if err != nil {
		errorMessage := fmt.Sprintf("unable to delete the template for id %s", templateID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !deleted {
		errorMessage := fmt.Sprintf("unable to delete the template for id %s", templateID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockDelete sets expected results for Delete
func (adapter *MongoAdapter) MockDelete(user string, templateID cacao_common.ID, existingTemplate types.Template, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.Config.TemplateMongoDBCollectionName, templateID.String()).Return(existingTemplate, expectedError)
	mock.On("Delete", adapter.Config.TemplateMongoDBCollectionName, templateID.String()).Return(expectedResult, expectedError)
	return nil
}
