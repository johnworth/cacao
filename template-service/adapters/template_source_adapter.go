package adapters

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/git"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// GitAdapter implements TemplateSourcePort
type GitAdapter struct {
	Config           *types.Config
	GitStorageOption *git.RAMStorageOption
}

// Init initialize git adapter
func (adapter *GitAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "GitAdapter.Init",
	})

	logger.Info("Initializing Git Adapter")

	adapter.Config = config

	adapter.GitStorageOption = &git.RAMStorageOption{
		SizeLimit: config.GitRAMSizeLimit,
	}
}

// Finalize finalizes mongodb adapter
func (adapter *GitAdapter) Finalize() {
	adapter.GitStorageOption = nil
}

// GetTemplateMetadata returns all template metadata
func (adapter *GitAdapter) GetTemplateMetadata(source types.TemplateSource, credential cacao_common_service.TemplateSourceCredential) (types.TemplateMetadata, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "GitAdapter.GetTemplateMetadata",
	})

	logger.Infof("Reading template metadata from %s", source.Type)
	if source.Type == types.TemplateSourceTypeGit {
		return adapter.getTemplateMetadataFromGit(source, credential)
	}

	errorMessage := fmt.Sprintf("unhandled template source type %s", source.Type)
	logger.Error(errorMessage)
	return types.TemplateMetadata{}, cacao_common_service.NewCacaoNotImplementedError(errorMessage)
}

func (adapter *GitAdapter) getTemplateMetadataFromGit(source types.TemplateSource, credential cacao_common_service.TemplateSourceCredential) (types.TemplateMetadata, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "GitAdapter.getTemplateMetadataFromGit",
	})

	logger.Infof("Reading template metadata from %s", source.URI)

	referenceType := git.BranchReferenceType
	referenceName := "master"
	templateDir := "/"

	if val, ok := source.AccessParameters["branch"]; ok {
		referenceType = git.BranchReferenceType
		referenceName = val.(string)
	}

	if val, ok := source.AccessParameters["tag"]; ok {
		referenceType = git.TagReferenceType
		referenceName = val.(string)
	}

	if val, ok := source.AccessParameters["path"]; ok {
		templateDir = val.(string)
	}

	repoConfig := &git.RepositoryConfig{
		URL:           source.URI,
		Username:      credential.Username,
		Password:      credential.Password,
		ReferenceType: referenceType,
		ReferenceName: referenceName,
	}

	clone, err := git.NewGitCloneToRAM(repoConfig, adapter.GitStorageOption)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to clone git repository %s", source.URI)
		logger.WithError(err).Error(errorMessage)
		return types.TemplateMetadata{}, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	defer clone.Release()

	metadata, err := adapter.readTemplateMetadataFromGit(clone, templateDir)
	if err != nil {
		errorMessage := "unable to read template metadata"
		logger.WithError(err).Error(errorMessage)
		return types.TemplateMetadata{}, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return metadata, nil
}

// getMetadataFilenameCandidates returns candidate metadata filenames for a template project
func (adapter *GitAdapter) getMetadataFilenameCandidates() []string {
	return []string{
		"cacao.json",
		"metadata.json",
		"cacao-metadata.json",
		".cacao.json",
	}
}

// readTemplateMetadataFromGit reads template metadata from git repository
func (adapter *GitAdapter) readTemplateMetadataFromGit(clone *git.Clone, subdirpath string) (types.TemplateMetadata, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "GitAdapter.readTemplateMetadataFromGit",
	})

	subpath := "/"
	if len(subdirpath) > 0 {
		subpath = subdirpath
	}

	if !strings.HasPrefix(subpath, "/") {
		subpath = "/" + subpath
	}
	subpath = filepath.Clean(subpath)

	metadata := types.TemplateMetadata{}

	fs := clone.GetFileSystem()
	files, err := fs.ReadDir(subpath)
	if err != nil {
		errorMessage := fmt.Sprintf("failed to list files from %s", clone.Config.URL)
		logger.WithError(err).Error(errorMessage)
		return metadata, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	metadataFileCandidates := adapter.getMetadataFilenameCandidates()
	metadataFileCandidatesMap := map[string]bool{}

	for _, metadataFileCandidate := range metadataFileCandidates {
		metadataFileCandidatesMap[metadataFileCandidate] = true
	}

	for _, file := range files {
		if !file.IsDir() {
			// must be a file
			if _, ok := metadataFileCandidatesMap[file.Name()]; ok {
				// found
				filePath := filepath.Join(subpath, file.Name())
				fd, err := fs.Open(filePath)
				if err != nil {
					errorMessage := fmt.Sprintf("failed to open a metadata file %s", filePath)
					logger.WithError(err).Error(errorMessage)
					return metadata, cacao_common_service.NewCacaoGeneralError(errorMessage)
				}

				metadataBytes, err := ioutil.ReadAll(fd)
				if err != nil {
					errorMessage := fmt.Sprintf("failed to read metadata from a file %s", filePath)
					logger.WithError(err).Error(errorMessage)
					return metadata, cacao_common_service.NewCacaoGeneralError(errorMessage)
				}

				// validate
				err = types.ValidateTemplateMetadataJSON(metadataBytes)
				if err != nil {
					logger.Error(err)
					return metadata, cacao_common_service.NewCacaoGeneralError(err.Error())
				}

				// unmarshal
				err = json.Unmarshal(metadataBytes, &metadata)
				if err != nil {
					errorMessage := "unable to unmarshal metadata to TemplateMetadata"
					logger.WithError(err).Error(errorMessage)
					return metadata, cacao_common_service.NewCacaoMarshalError(errorMessage)
				}

				return metadata, nil
			}
		}
	}

	errorMessage := fmt.Sprintf("failed to find metadata file in the git repository %s", clone.Config.URL)
	logger.Error(errorMessage)
	return metadata, cacao_common_service.NewCacaoGeneralError(errorMessage)
}
