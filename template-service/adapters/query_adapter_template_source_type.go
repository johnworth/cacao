package adapters

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

func (adapter *QueryAdapter) handleTemplateSourceTypeListQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "QueryAdapter.handleTemplateSourceTypeListQuery",
	})

	var listRequest cacao_common_service.Session
	var listResponse cacao_common_service.TemplateSourceTypeListModel
	err := json.Unmarshal(jsonData, &listRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into Session"
		logger.WithError(err).Error(errorMessage)
		listResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(listResponse)
		if err2 != nil {
			errorMessage := "unable to marshal TemplateSourceTypeListModel into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	listResult, err := adapter.IncomingPort.ListSourceTypes(listRequest.GetSessionActor(), listRequest.SessionEmulator)
	if err == nil {
		listResponse.Session = cacao_common_service.Session{
			SessionActor:    listRequest.GetSessionActor(),
			SessionEmulator: listRequest.GetSessionEmulator(),
		}
		listResponse.TemplateSourceTypes = listResult
	} else {
		logger.Error(err)

		listResponse.Session = cacao_common_service.Session{
			SessionActor:    listRequest.GetSessionActor(),
			SessionEmulator: listRequest.GetSessionEmulator(),
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			listResponse.ServiceError = cerr.GetBase()
		} else {
			listResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(listResponse)
	if err != nil {
		errorMessage := "unable to marshal TemplateSourceTypeListModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}
