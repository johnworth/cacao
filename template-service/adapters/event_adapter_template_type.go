package adapters

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func (adapter *EventAdapter) handleTemplateTypeCreateRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.handleTemplateTypeCreateRequest",
	})

	var createRequest cacao_common_service.TemplateTypeModel
	err := json.Unmarshal(jsonData, &createRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into TemplateTypeModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	templateType := types.ConvertTypeFromModel(createRequest)

	err = adapter.IncomingPort.CreateType(createRequest.GetSessionActor(), createRequest.GetSessionEmulator(), templateType, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleTemplateTypeDeleteRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.handleTemplateTypeDeleteRequest",
	})

	var deleteRequest cacao_common_service.TemplateTypeModel
	err := json.Unmarshal(jsonData, &deleteRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into TemplateTypeModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	templateType := types.ConvertTypeFromModel(deleteRequest)

	err = adapter.IncomingPort.DeleteType(deleteRequest.GetSessionActor(), deleteRequest.GetSessionEmulator(), templateType, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleTemplateTypeUpdateRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.handleTemplateTypeUpdateRequest",
	})

	var updateRequest cacao_common_service.TemplateTypeModel
	err := json.Unmarshal(jsonData, &updateRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into TemplateTypeModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	templateType := types.ConvertTypeFromModel(updateRequest)

	if len(updateRequest.UpdateFieldNames) == 0 {
		// update all fields
		updateRequest.UpdateFieldNames = append(updateRequest.UpdateFieldNames, "name", "formats", "engine", "provider_types")
	}

	err = adapter.IncomingPort.UpdateType(updateRequest.GetSessionActor(), updateRequest.GetSessionEmulator(), templateType, updateRequest.UpdateFieldNames, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

// Implement OutgoingEventPort

// TypeCreated ...
func (adapter *EventAdapter) TypeCreated(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.TypeCreated",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertTypeToModel(session, templateType)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateTypeCreatedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeCreatedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeCreatedEvent)
	}

	return nil
}

// TypeCreateFailed ...
func (adapter *EventAdapter) TypeCreateFailed(actor string, emulator string, templateType types.TemplateType, creationError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.TypeCreateFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       creationError.Error(),
		ErrorMessage:    creationError.Error(),
	}

	if cerr, ok := creationError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(creationError.Error()).GetBase()
	}

	model := types.ConvertTypeToModel(session, templateType)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateTypeCreateFailedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeCreateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeCreateFailedEvent)
	}

	return nil
}

// TypeUpdated ...
func (adapter *EventAdapter) TypeUpdated(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.TypeUpdated",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertTypeToModel(session, templateType)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateTypeUpdatedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeUpdatedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeUpdatedEvent)
	}

	return nil
}

// TypeUpdateFailed ...
func (adapter *EventAdapter) TypeUpdateFailed(actor string, emulator string, templateType types.TemplateType, updateError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.TypeUpdateFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       updateError.Error(),
		ErrorMessage:    updateError.Error(),
	}

	if cerr, ok := updateError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(updateError.Error()).GetBase()
	}

	model := types.ConvertTypeToModel(session, templateType)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateTypeUpdateFailedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeUpdateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeUpdateFailedEvent)
	}

	return nil
}

// TypeDeleted ...
func (adapter *EventAdapter) TypeDeleted(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.TypeDeleted",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertTypeToModel(session, templateType)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateTypeDeletedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeDeletedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeDeletedEvent)
	}

	return nil
}

// TypeDeleteFailed ...
func (adapter *EventAdapter) TypeDeleteFailed(actor string, emulator string, templateType types.TemplateType, deletionError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.TypeDeleteFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       deletionError.Error(),
		ErrorMessage:    deletionError.Error(),
	}

	if cerr, ok := deletionError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(deletionError.Error()).GetBase()
	}

	model := types.ConvertTypeToModel(session, templateType)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateTypeDeleteFailedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeDeleteFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeDeleteFailedEvent)
	}

	return nil
}
