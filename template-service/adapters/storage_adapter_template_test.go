package adapters

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func TestMongoAdapterList(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"

	expectedResults := []types.Template{
		{
			ID:          "0001",
			Owner:       testUser,
			Name:        "test_template1",
			Description: "test_template_description1",
			Public:      true,
			Source: types.TemplateSource{
				Type: types.TemplateSourceTypeGit,
				URI:  "https://github.com/cyverse/tf-openstack-single-image",
				AccessParameters: map[string]interface{}{
					"branch": "master",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
			},
			Metadata: types.TemplateMetadata{
				Name:             "single image openstack instances",
				Author:           "Edwin Skidmore",
				AuthorEmail:      "edwin@cyverse.org",
				Description:      "launches a single image on a single cloud",
				TemplateTypeName: "openstack_terraform",
			},
			CreatedAt: testTime,
			UpdatedAt: testTime,
		},
		{
			ID:          "0002",
			Owner:       testUser,
			Name:        "test_template2",
			Description: "test_template_description2",
			Public:      false,
			Source: types.TemplateSource{
				Type: types.TemplateSourceTypeGit,
				URI:  "https://github.com/cyverse/tf-openstack-single-image",
				AccessParameters: map[string]interface{}{
					"branch": "master",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
			},
			Metadata: types.TemplateMetadata{
				Name:             "single image openstack instances",
				Author:           "Edwin Skidmore",
				AuthorEmail:      "edwin@cyverse.org",
				Description:      "launches a single image on a single cloud",
				TemplateTypeName: "openstack_terraform",
			},
			CreatedAt: testTime,
			UpdatedAt: testTime,
		},
	}
	err := mongoAdapter.MockList(testUser, expectedResults, nil)
	assert.NoError(t, err)

	results, err := mongoAdapter.List(testUser)
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	mongoAdapter.Finalize()
}
func TestMongoAdapterGet(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testTemplateID := cacao_common_service.NewTemplateID()

	expectedResult := types.Template{
		ID:          testTemplateID,
		Owner:       testUser,
		Name:        "test_template1",
		Description: "test_template_description1",
		Public:      true,
		Source: types.TemplateSource{
			Type: types.TemplateSourceTypeGit,
			URI:  "https://github.com/cyverse/tf-openstack-single-image",
			AccessParameters: map[string]interface{}{
				"branch": "master",
			},
			Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
		},
		Metadata: types.TemplateMetadata{
			Name:             "single image openstack instances",
			Author:           "Edwin Skidmore",
			AuthorEmail:      "edwin@cyverse.org",
			Description:      "launches a single image on a single cloud",
			TemplateTypeName: "openstack_terraform",
		},
		CreatedAt: testTime,
		UpdatedAt: testTime,
	}
	err := mongoAdapter.MockGet(testUser, testTemplateID, expectedResult, nil)
	assert.NoError(t, err)

	result, err := mongoAdapter.Get(testUser, testTemplateID)
	assert.NoError(t, err)
	assert.Equal(t, result, expectedResult)

	mongoAdapter.Finalize()
}

func TestMongoAdapterCreate(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testTemplateID := cacao_common_service.NewTemplateID()

	testTemplate := types.Template{
		ID:          testTemplateID,
		Owner:       testUser,
		Name:        "test_template1",
		Description: "test_template_description1",
		Public:      true,
		Source: types.TemplateSource{
			Type: types.TemplateSourceTypeGit,
			URI:  "https://github.com/cyverse/tf-openstack-single-image",
			AccessParameters: map[string]interface{}{
				"branch": "master",
			},
			Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
		},
		Metadata: types.TemplateMetadata{
			Name:             "single image openstack instances",
			Author:           "Edwin Skidmore",
			AuthorEmail:      "edwin@cyverse.org",
			Description:      "launches a single image on a single cloud",
			TemplateTypeName: "openstack_terraform",
		},
		CreatedAt: testTime,
		UpdatedAt: testTime,
	}

	err := mongoAdapter.MockCreate(testTemplate, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Create(testTemplate)
	assert.NoError(t, err)

	mongoAdapter.Finalize()
}

func TestMongoAdapterUpdate(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testTemplateID := cacao_common_service.NewTemplateID()

	testExistingTemplate := types.Template{
		ID:          testTemplateID,
		Owner:       testUser,
		Name:        "test_template1",
		Description: "test_template_description1",
		Public:      true,
		Source: types.TemplateSource{
			Type: types.TemplateSourceTypeGit,
			URI:  "https://github.com/cyverse/tf-openstack-single-image",
			AccessParameters: map[string]interface{}{
				"branch": "master",
			},
			Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
		},
		Metadata: types.TemplateMetadata{
			Name:             "single image openstack instances",
			Author:           "Edwin Skidmore",
			AuthorEmail:      "edwin@cyverse.org",
			Description:      "launches a single image on a single cloud",
			TemplateTypeName: "openstack_terraform",
		},
		CreatedAt: testTime,
		UpdatedAt: testTime,
	}

	testUpdateTemplate := types.Template{
		ID:          testTemplateID,
		Owner:       testUser,
		Name:        "test_template1",
		Description: "test_template_description2",
		Public:      false,
	}

	err := mongoAdapter.MockUpdate(testExistingTemplate, testUpdateTemplate, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Update(testUpdateTemplate, []string{"description", "public"})
	assert.NoError(t, err)

	mongoAdapter.Finalize()
}

func TestMongoAdapterDelete(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testTemplateID := cacao_common_service.NewTemplateID()

	testTemplate := types.Template{
		ID:          testTemplateID,
		Owner:       testUser,
		Name:        "test_template1",
		Description: "test_template_description1",
		Public:      true,
		Source: types.TemplateSource{
			Type: types.TemplateSourceTypeGit,
			URI:  "https://github.com/cyverse/tf-openstack-single-image",
			AccessParameters: map[string]interface{}{
				"branch": "master",
			},
			Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
		},
		Metadata: types.TemplateMetadata{
			Name:             "single image openstack instances",
			Author:           "Edwin Skidmore",
			AuthorEmail:      "edwin@cyverse.org",
			Description:      "launches a single image on a single cloud",
			TemplateTypeName: "openstack_terraform",
		},
		CreatedAt: testTime,
		UpdatedAt: testTime,
	}

	err := mongoAdapter.MockDelete(testUser, testTemplateID, testTemplate, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Delete(testUser, testTemplateID)
	assert.NoError(t, err)

	mongoAdapter.Finalize()
}
