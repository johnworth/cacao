package adapters

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// CredentialAdapter implements CredentialPort
type CredentialAdapter struct {
	Config *types.Config
}

// Init initialize credential adapter
func (adapter *CredentialAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "CredentialAdapter.Init",
	})

	logger.Info("initializing CredentialAdapter")

	adapter.Config = config
}

// Finalize finalizes credential adapter
func (adapter *CredentialAdapter) Finalize() {
}

// GetTemplateSourceCredential returns template source credential
func (adapter *CredentialAdapter) GetTemplateSourceCredential(actor string, emulator string, credentialID string) (cacao_common_service.TemplateSourceCredential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "CredentialAdapter.GetTemplateSourceCredential",
	})

	natsConfig := adapter.Config.NatsConfig
	natsConfig.ClientID = natsConfig.ClientID + "-outquery" + xid.New().String()

	credentialClient, err := cacao_common_service.NewNatsCredentialClient(context.TODO(), actor, emulator, natsConfig, adapter.Config.StanConfig)
	if err != nil {
		errorMessage := "unable to create a credential client"
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.TemplateSourceCredential{}, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	credential, err := credentialClient.Get(credentialID)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the credential for id %s", credentialID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.TemplateSourceCredential{}, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	jsonData := []byte(credential.GetValue())

	templateSourceCredential := cacao_common_service.TemplateSourceCredential{}
	err = json.Unmarshal(jsonData, &templateSourceCredential)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into TemplateSourceCredential"
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.TemplateSourceCredential{}, cacao_common_service.NewCacaoMarshalError(errorMessage)
	}

	return templateSourceCredential, nil
}
