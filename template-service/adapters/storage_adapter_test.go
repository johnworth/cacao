package adapters

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func createTestMongoAdapter() *MongoAdapter {
	var config types.Config
	config.ProcessDefaults()

	mongoAdapter := &MongoAdapter{}
	mongoAdapter.InitMock(&config)

	return mongoAdapter
}

func TestInitMongoAdapter(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()
	assert.NotNil(t, mongoAdapter)
	assert.NotEmpty(t, mongoAdapter.Store)

	mongoAdapter.Finalize()
}
