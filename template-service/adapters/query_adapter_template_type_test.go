package adapters

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func TestTemplateTypeListQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"

	queryData := cacao_common_service.TemplateTypeModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
	}

	expectedResults := []types.TemplateType{
		{
			Name: "test_template_type1",
			Formats: []cacao_common_service.TemplateFormat{
				cacao_common_service.TemplateFormatYAML,
				cacao_common_service.TemplateFormatJSON,
			},
			Engine: cacao_common_service.TemplateEngineTerraform,
			ProviderTypes: []cacao_common_service.TemplateProviderType{
				cacao_common_service.TemplateProviderTypeOpenStack,
				cacao_common_service.TemplateProviderTypeKubernetes,
			},
		},
		{
			Name: "test_template_type2",
			Formats: []cacao_common_service.TemplateFormat{
				cacao_common_service.TemplateFormatYAML,
			},
			Engine: cacao_common_service.TemplateEngineTerraform,
			ProviderTypes: []cacao_common_service.TemplateProviderType{
				cacao_common_service.TemplateProviderTypeOpenStack,
			},
		},
	}

	queryInImpl.SetListTypesHandler(func(actor string, emulator string) ([]types.TemplateType, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		return expectedResults, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.TemplateTypeListQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.TemplateTypeListModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result)
	assert.NoError(t, result.GetServiceError())
	assert.EqualValues(t, len(expectedResults), len(result.GetTemplateTypes()))

	firstTemplateType := result.GetTemplateTypes()[0]
	assert.EqualValues(t, expectedResults[0].Name, firstTemplateType.GetName())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestTemplateTypeListByProviderTypeQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testProviderType := cacao_common_service.TemplateProviderTypeOpenStack

	queryData := cacao_common_service.TemplateTypeModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ProviderTypes: []cacao_common_service.TemplateProviderType{
			testProviderType,
		},
	}

	expectedResults := []types.TemplateType{
		{
			Name: "test_template_type1",
			Formats: []cacao_common_service.TemplateFormat{
				cacao_common_service.TemplateFormatYAML,
				cacao_common_service.TemplateFormatJSON,
			},
			Engine: cacao_common_service.TemplateEngineTerraform,
			ProviderTypes: []cacao_common_service.TemplateProviderType{
				cacao_common_service.TemplateProviderTypeOpenStack,
				cacao_common_service.TemplateProviderTypeKubernetes,
			},
		},
		{
			Name: "test_template_type2",
			Formats: []cacao_common_service.TemplateFormat{
				cacao_common_service.TemplateFormatYAML,
			},
			Engine: cacao_common_service.TemplateEngineTerraform,
			ProviderTypes: []cacao_common_service.TemplateProviderType{
				cacao_common_service.TemplateProviderTypeOpenStack,
			},
		},
	}

	queryInImpl.SetListTypesHandler(func(actor string, emulator string) ([]types.TemplateType, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		return expectedResults, nil
	})

	queryInImpl.SetListTypesForProviderTypeHandler(func(actor string, emulator string, providerType cacao_common_service.TemplateProviderType) ([]types.TemplateType, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testProviderType, providerType)
		return expectedResults, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.TemplateTypeListQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.TemplateTypeListModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result)
	assert.NoError(t, result.GetServiceError())
	assert.EqualValues(t, len(expectedResults), len(result.GetTemplateTypes()))

	firstTemplateType := result.GetTemplateTypes()[0]
	assert.EqualValues(t, expectedResults[0].Name, firstTemplateType.GetName())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestTemplateTypeListQueryFailed(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testProviderType := cacao_common_service.TemplateProviderTypeOpenStack

	queryData := cacao_common_service.TemplateTypeModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ProviderTypes: []cacao_common_service.TemplateProviderType{
			testProviderType,
		},
	}

	queryInImpl.SetListTypesHandler(func(actor string, emulator string) ([]types.TemplateType, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		return nil, fmt.Errorf("unable to list template types")
	})

	queryInImpl.SetListTypesForProviderTypeHandler(func(actor string, emulator string, providerType cacao_common_service.TemplateProviderType) ([]types.TemplateType, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		return nil, fmt.Errorf("unable to list template types")
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.TemplateTypeListQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.TemplateTypeListModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.TemplateTypes)
	assert.Error(t, result.GetServiceError())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestTemplateTypeGetQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testTemplateTypeName := "test_template_type_name1"

	queryData := cacao_common_service.TemplateTypeModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		Name: testTemplateTypeName,
	}

	expectedResult := types.TemplateType{
		Name: testTemplateTypeName,
		Formats: []cacao_common_service.TemplateFormat{
			cacao_common_service.TemplateFormatYAML,
			cacao_common_service.TemplateFormatJSON,
		},
		Engine: cacao_common_service.TemplateEngineTerraform,
		ProviderTypes: []cacao_common_service.TemplateProviderType{
			cacao_common_service.TemplateProviderTypeOpenStack,
			cacao_common_service.TemplateProviderTypeKubernetes,
		},
	}

	queryInImpl.SetGetTypeHandler(func(actor string, emulator string, templateTypeName string) (types.TemplateType, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateTypeName, templateTypeName)
		return expectedResult, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.TemplateTypeGetQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.TemplateTypeModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result.Name)
	assert.NoError(t, result.GetServiceError())

	assert.Equal(t, expectedResult.Name, result.Name)

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestTemplateTypeGetQueryFailed(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testTemplateTypeName := "test_template_type_name1"

	queryData := cacao_common_service.TemplateTypeModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		Name: testTemplateTypeName,
	}

	queryInImpl.SetGetTypeHandler(func(actor string, emulator string, templateTypeName string) (types.TemplateType, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateTypeName, templateTypeName)
		return types.TemplateType{}, fmt.Errorf("unable to get a template type")
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.TemplateTypeGetQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.TemplateTypeModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.Name)
	assert.Error(t, result.GetServiceError())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}
