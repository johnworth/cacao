package adapters

import (
	"testing"

	"github.com/stretchr/testify/assert"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func createTestGitAdapter() *GitAdapter {
	var config types.Config
	config.ProcessDefaults()

	gitAdapter := &GitAdapter{}
	gitAdapter.Init(&config)

	return gitAdapter
}

func TestInitGitAdapter(t *testing.T) {
	gitAdapter := createTestGitAdapter()
	assert.NotNil(t, gitAdapter)
	assert.NotNil(t, gitAdapter.GitStorageOption)

	gitAdapter.Finalize()
}

func TestGetTemplateMetadata(t *testing.T) {
	gitAdapter := createTestGitAdapter()

	source := types.TemplateSource{
		Type: types.TemplateSourceTypeGit,
		URI:  "https://github.com/cyverse/tf-openstack-single-image",
		AccessParameters: map[string]interface{}{
			"branch": "master",
			"path":   "/",
		},
	}

	credential := cacao_common_service.TemplateSourceCredential{
		Username: "",
		Password: "",
	}

	metadata, err := gitAdapter.GetTemplateMetadata(source, credential)
	assert.NoError(t, err)
	assert.Equal(t, "openstack_terraform", metadata.TemplateTypeName)
	assert.Equal(t, "edwin@cyverse.org", metadata.AuthorEmail)

	gitAdapter.Finalize()
}
