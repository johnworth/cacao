package domain

import (
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// ListTemplateTypes returns all template types
func (d *Domain) ListTemplateTypes(actor string, emulator string) ([]types.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "Domain.ListTemplateTypes",
	})

	logger.Info("List template types")

	templateTypes, err := d.Storage.ListTypes()
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return templateTypes, nil
}

// ListTemplateTypesForProviderType returns all template types that supports the given provider type
func (d *Domain) ListTemplateTypesForProviderType(actor string, emulator string, templateProviderType cacao_common_service.TemplateProviderType) ([]types.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "Domain.ListTemplateTypesForProviderType",
	})

	logger.Infof("List template types for provider type %s", templateProviderType)

	templateTypes, err := d.Storage.ListTypesForProviderType(templateProviderType)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return templateTypes, nil
}

// GetTemplateType returns the template type with the name
func (d *Domain) GetTemplateType(actor string, emulator string, templateTypeName string) (types.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "Domain.GetTemplateType",
	})

	logger.Infof("Get template type %s", templateTypeName)

	templateType, err := d.Storage.GetType(templateTypeName)
	if err != nil {
		logger.Error(err)
		return types.TemplateType{}, err
	}

	return templateType, nil
}

// CreateTemplateType creates a template type
func (d *Domain) CreateTemplateType(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.CreateTemplateType",
	})

	logger.Infof("Create template type %s", templateType.Name)

	err := d.Storage.CreateType(templateType)
	if err != nil {
		createFailedEvent := types.TemplateType{
			Name: templateType.Name,
		}

		err2 := d.EventOut.TypeCreateFailed(actor, emulator, createFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a TypeCreateFailed event")
		}

		logger.Error(err)
		return err
	}

	// output event
	createdEvent := types.TemplateType{
		Name: templateType.Name,
	}

	err = d.EventOut.TypeCreated(actor, emulator, createdEvent, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("Failed to send a TypeCreated event")
	}

	return nil
}

// UpdateTemplateType updates the template type
func (d *Domain) UpdateTemplateType(actor string, emulator string, templateType types.TemplateType, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.UpdateTemplateType",
	})

	logger.Infof("Update template type %s", templateType.Name)

	err := d.Storage.UpdateType(templateType, updateFieldNames)
	if err != nil {
		updateFailedEvent := types.TemplateType{
			Name: templateType.Name,
		}

		err2 := d.EventOut.TypeUpdateFailed(actor, emulator, updateFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an TypeUpdateFailed event")
		}

		logger.Error(err)
		return err
	}

	// output event
	updatedEvent := types.TemplateType{
		Name: templateType.Name,
	}

	err = d.EventOut.TypeUpdated(actor, emulator, updatedEvent, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send an Updated event")
	}

	return nil
}

// DeleteTemplateType deletes the template type
func (d *Domain) DeleteTemplateType(actor string, emulator string, templateTypeName string, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.DeleteTemplateType",
	})

	logger.Infof("Delete template type %s", templateTypeName)

	err := d.Storage.DeleteType(templateTypeName)
	if err != nil {
		deleteFailedEvent := types.TemplateType{
			Name: templateTypeName,
		}

		err2 := d.EventOut.TypeDeleteFailed(actor, emulator, deleteFailedEvent, err, transactionID)
		if err != nil {
			logger.WithError(err2).Errorf("failed to send a TypeDeleteFailed event")
		}

		logger.Error(err)
		return err
	}

	// output event
	deletedEvent := types.TemplateType{
		Name: templateTypeName,
	}

	err = d.EventOut.TypeDeleted(actor, emulator, deletedEvent, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send a Deleted event")
	}

	return nil
}
