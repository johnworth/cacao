package domain

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// ListTypes retrieves all template types
func (impl *QueryPortImpl) ListTypes(actor string, emulator string) ([]types.TemplateType, error) {
	if len(actor) == 0 {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	responseChannel := make(chan types.TemplateChannelResponse)

	// request to model
	impl.Channel <- types.TemplateChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.TemplateType{
			ProviderTypes: []cacao_common_service.TemplateProviderType{},
		},
		Operation: string(cacao_common_service.TemplateTypeListQueryOp),
		Response:  responseChannel,
	}

	// receive response
	templateTypeResponse := <-responseChannel

	if templateTypeResponse.Error != nil {
		return nil, templateTypeResponse.Error
	}

	templateTypes, ok := templateTypeResponse.Data.([]types.TemplateType)
	if !ok {
		return nil, cacao_common_service.NewCacaoMarshalError("unable to convert response data into TemplateType array")
	}

	return templateTypes, nil
}

// ListTypesForProviderType retrieves all template types for the given provider type
func (impl *QueryPortImpl) ListTypesForProviderType(actor string, emulator string, providerType cacao_common_service.TemplateProviderType) ([]types.TemplateType, error) {
	if len(actor) == 0 {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	responseChannel := make(chan types.TemplateChannelResponse)

	// request to model
	impl.Channel <- types.TemplateChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.TemplateType{
			ProviderTypes: []cacao_common_service.TemplateProviderType{
				providerType,
			},
		},
		Operation: string(cacao_common_service.TemplateTypeListQueryOp),
		Response:  responseChannel,
	}

	// receive response
	templateTypeResponse := <-responseChannel

	if templateTypeResponse.Error != nil {
		return nil, templateTypeResponse.Error
	}

	templateTypes, ok := templateTypeResponse.Data.([]types.TemplateType)
	if !ok {
		return nil, cacao_common_service.NewCacaoMarshalError("unable to convert response data into TemplateType array")
	}

	return templateTypes, nil
}

// GetType retrieves the template type
func (impl *QueryPortImpl) GetType(actor string, emulator string, templateTypeName string) (types.TemplateType, error) {
	if len(actor) == 0 {
		return types.TemplateType{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(templateTypeName) == 0 {
		return types.TemplateType{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: template type name is empty")
	}

	responseChannel := make(chan types.TemplateChannelResponse)

	// request to model
	impl.Channel <- types.TemplateChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.TemplateType{
			Name: templateTypeName,
		},
		Operation: string(cacao_common_service.TemplateTypeGetQueryOp),
		Response:  responseChannel,
	}

	// receive response
	templateTypeResponse := <-responseChannel

	if templateTypeResponse.Error != nil {
		return types.TemplateType{}, templateTypeResponse.Error
	}

	templateType, ok := templateTypeResponse.Data.(types.TemplateType)
	if !ok {
		return types.TemplateType{}, cacao_common_service.NewCacaoMarshalError("unable to convert response data into TemplateType")
	}

	return templateType, nil
}
