package domain

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// ListTemplates returns templates owned by a user
func (d *Domain) ListTemplates(actor string, emulator string) ([]types.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "Domain.ListTemplates",
	})

	logger.Info("List templates")

	templates, err := d.Storage.List(actor)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return templates, nil
}

// GetTemplate returns the template with the ID
func (d *Domain) GetTemplate(actor string, emulator string, templateID cacao_common.ID) (types.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "Domain.GetTemplate",
	})

	logger.Infof("Get template %s", templateID.String())

	template, err := d.Storage.Get(actor, templateID)
	if err != nil {
		logger.Error(err)
		return types.Template{}, err
	}

	return template, nil
}

// ImportTemplate imports a template
func (d *Domain) ImportTemplate(actor string, emulator string, template types.Template, credentialID string, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.ImportTemplate",
	})

	logger.Infof("Import template %s", template.ID.String())

	// Import
	templateMetadata, err := d.importTemplateMetadata(actor, emulator, template.Source, credentialID)
	if err != nil {
		importFailedEvent := types.Template{
			ID:    template.ID,
			Owner: actor,
			Name:  template.Name,
		}

		err2 := d.EventOut.ImportFailed(actor, emulator, importFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an ImportFailed event")
		}

		logger.Error(err)
		return err
	}

	template.Metadata = templateMetadata

	// fill default
	if len(template.Name) == 0 {
		template.Name = templateMetadata.Name
	}

	if len(template.Description) == 0 {
		template.Description = templateMetadata.Description
	}

	err = d.Storage.Create(template)
	if err != nil {
		importFailedEvent := types.Template{
			ID:    template.ID,
			Owner: actor,
			Name:  template.Name,
		}

		err2 := d.EventOut.ImportFailed(actor, emulator, importFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an ImportFailed event")
		}

		logger.Error(err)
		return err
	}

	// output event
	importedEvent := types.Template{
		ID:        template.ID,
		Owner:     actor,
		Name:      template.Name,
		CreatedAt: template.CreatedAt,
	}

	err = d.EventOut.Imported(actor, emulator, importedEvent, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send an Imported event")
	}

	return nil
}

// UpdateTemplate updates the template
func (d *Domain) UpdateTemplate(actor string, emulator string, template types.Template, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.UpdateTemplate",
	})

	logger.Infof("Update template %s", template.ID.String())

	err := d.Storage.Update(template, updateFieldNames)
	if err != nil {
		updateFailedEvent := types.Template{
			ID:    template.ID,
			Owner: actor,
			Name:  template.Name, // may be empty
		}

		err2 := d.EventOut.UpdateFailed(actor, emulator, updateFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an UpdateFailed event")
		}

		logger.Error(err)
		return err
	}

	// get the final result
	updatedTemplate, err := d.Storage.Get(actor, template.ID)
	if err != nil {
		updateFailedEvent := types.Template{
			ID:    template.ID,
			Owner: actor,
			Name:  template.Name,
		}

		err2 := d.EventOut.UpdateFailed(actor, emulator, updateFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an UpdateFailed event")
		}

		logger.Error(err)
		return err
	}

	// output event
	updatedEvent := types.Template{
		ID:        template.ID,
		Owner:     actor,
		Name:      updatedTemplate.Name,
		UpdatedAt: updatedTemplate.UpdatedAt,
	}

	err = d.EventOut.Updated(actor, emulator, updatedEvent, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send an Updated event")
	}

	return nil
}

// DeleteTemplate deletes the template
func (d *Domain) DeleteTemplate(actor string, emulator string, templateID cacao_common.ID, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.DeleteTemplate",
	})

	logger.Infof("Delete template %s", templateID.String())

	// get the template first
	template, err := d.Storage.Get(actor, templateID)
	if err != nil {
		// not exist
		deleteFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor,
			Name:  "", // unknown
		}

		err2 := d.EventOut.DeleteFailed(actor, emulator, deleteFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a DeleteFailed event")
		}

		logger.Error(err)
		return err
	}

	err = d.Storage.Delete(actor, templateID)
	if err != nil {
		deleteFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor,
			Name:  template.Name,
		}

		err2 := d.EventOut.DeleteFailed(actor, emulator, deleteFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a DeleteFailed event")
		}

		logger.Error(err)
		return err
	}

	// output event
	deletedEvent := types.Template{
		ID:    templateID,
		Owner: actor,
		Name:  template.Name,
	}

	err = d.EventOut.Deleted(actor, emulator, deletedEvent, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send a Deleted event")
	}

	return nil
}

// SyncTemplate syncs the template
func (d *Domain) SyncTemplate(actor string, emulator string, templateID cacao_common.ID, credentialID string, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.SyncTemplate",
	})

	logger.Infof("Sync template %s", templateID.String())

	now := time.Now().UTC()

	// get the template
	template, err := d.Storage.Get(actor, templateID)
	if err != nil {
		syncFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor,
			Name:  "", // unknown
		}

		err2 := d.EventOut.SyncFailed(actor, emulator, syncFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an SyncFailed event")
		}

		logger.Error(err)
		return err
	}

	// Import
	templateMetadata, err := d.importTemplateMetadata(actor, emulator, template.Source, credentialID)
	if err != nil {
		syncFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor,
			Name:  template.Name,
		}

		err2 := d.EventOut.SyncFailed(actor, emulator, syncFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an SyncFailed event")
		}

		logger.Error(err)
		return err
	}

	template.Metadata = templateMetadata
	template.UpdatedAt = now

	updateFields := []string{
		"metadata", "updated_at",
	}

	err = d.Storage.Update(template, updateFields)
	if err != nil {
		syncFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor,
			Name:  template.Name,
		}

		err2 := d.EventOut.SyncFailed(actor, emulator, syncFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an SyncFailed event")
		}

		logger.Error(err)
		return err
	}

	// output event
	syncedEvent := types.Template{
		ID:        template.ID,
		Owner:     actor,
		Name:      template.Name,
		UpdatedAt: template.UpdatedAt,
	}

	err = d.EventOut.Synced(actor, emulator, syncedEvent, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send a Synced event")
	}

	return nil
}

func (d *Domain) importTemplateMetadata(actor string, emulator string, source types.TemplateSource, credentialID string) (types.TemplateMetadata, error) {
	if len(source.Type) == 0 {
		return types.TemplateMetadata{}, fmt.Errorf("source type is empty")
	}

	if len(source.URI) == 0 {
		return types.TemplateMetadata{}, fmt.Errorf("source URI is empty")
	}

	if len(credentialID) > 0 {
		credential, err := d.Credential.GetTemplateSourceCredential(actor, emulator, credentialID)
		if err != nil {
			return types.TemplateMetadata{}, err
		}

		return d.TemplateSource.GetTemplateMetadata(source, credential)
	}

	return d.TemplateSource.GetTemplateMetadata(source, cacao_common_service.TemplateSourceCredential{})
}
