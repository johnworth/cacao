package domain

import (
	"time"

	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// Import imports a template
func (impl *EventPortImpl) Import(actor string, emulator string, template types.Template, credentialID string, transactionID cacao_common.TransactionID) error {
	if len(actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(template.ID) == 0 {
		template.ID = cacao_common_service.NewTemplateID()
	}

	// if this is empty, copy from metadata
	//if len(template.Name) == 0 {
	//	return cacao_common_service.NewCacaoInvalidParameterError("input validation error: name is empty")
	//}

	if len(template.Source.Type) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: source type is empty")
	}

	if len(template.Source.URI) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: source URI is empty")
	}

	now := time.Now().UTC()

	responseChannel := make(chan types.TemplateChannelResponse)

	templateRequest := types.TemplateChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.Template{
			ID:          template.ID,
			Owner:       actor,
			Name:        template.Name,
			Description: template.Description,
			Public:      template.Public,
			Source:      template.Source,
			CreatedAt:   now,
			UpdatedAt:   now,
		},
		CredentialID:  credentialID,
		Operation:     string(cacao_common_service.TemplateImportRequestedEvent),
		TransactionID: transactionID,
		Response:      responseChannel,
	}

	// request to model
	impl.Channel <- templateRequest

	// receive response
	templateResponse := <-responseChannel
	return templateResponse.Error
}

// Update updates the template
func (impl *EventPortImpl) Update(actor string, emulator string, template types.Template, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
	if len(actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(template.ID) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: template id is empty")
	}

	now := time.Now().UTC()

	responseChannel := make(chan types.TemplateChannelResponse)

	templateRequest := types.TemplateChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.Template{
			ID:          template.ID,
			Owner:       actor,
			Name:        template.Name,
			Description: template.Description,
			Public:      template.Public,
			Source:      template.Source,
			UpdatedAt:   now,
		},
		Operation:        string(cacao_common_service.TemplateUpdateRequestedEvent),
		UpdateFieldNames: append(updateFieldNames, "updated_at"),
		TransactionID:    transactionID,
		Response:         responseChannel,
	}

	// request to model
	impl.Channel <- templateRequest

	// receive response
	templateResponse := <-responseChannel
	return templateResponse.Error
}

// Delete deletes the template
func (impl *EventPortImpl) Delete(actor string, emulator string, template types.Template, transactionID cacao_common.TransactionID) error {
	if len(actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(template.ID) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: template id is empty")
	}

	responseChannel := make(chan types.TemplateChannelResponse)

	templateRequest := types.TemplateChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.Template{
			ID: template.ID,
		},
		Operation:     string(cacao_common_service.TemplateDeleteRequestedEvent),
		TransactionID: transactionID,
		Response:      responseChannel,
	}

	// request to model
	impl.Channel <- templateRequest

	// receive response
	templateResponse := <-responseChannel
	return templateResponse.Error
}

// Sync syncs the template
func (impl *EventPortImpl) Sync(actor string, emulator string, template types.Template, credentialID string, transactionID cacao_common.TransactionID) error {
	if len(actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(template.ID) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: template id is empty")
	}

	now := time.Now().UTC()

	responseChannel := make(chan types.TemplateChannelResponse)

	templateRequest := types.TemplateChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.Template{
			ID:        template.ID,
			UpdatedAt: now,
		},
		CredentialID:  credentialID,
		Operation:     string(cacao_common_service.TemplateSyncRequestedEvent),
		TransactionID: transactionID,
		Response:      responseChannel,
	}

	// request to model
	impl.Channel <- templateRequest

	// receive response
	templateResponse := <-responseChannel
	return templateResponse.Error
}
