package domain

import (
	"fmt"
	"sync"

	log "github.com/sirupsen/logrus"

	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/ports"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// Domain is the base struct for the domain service
type Domain struct {
	Config         *types.Config
	Storage        ports.PersistentStoragePort
	TemplateSource ports.TemplateSourcePort
	Credential     ports.CredentialPort
	QueryIn        ports.IncomingQueryPort
	EventIn        ports.IncomingEventPort
	EventOut       ports.OutgoingEventPort
}

// Init initializes all the specified adapters
func (d *Domain) Init(config *types.Config) {
	d.Config = config
}

// Finalize ...
func (d *Domain) Finalize() {
}

// Start will start the domain object, and in turn start all the async adapters
func (d *Domain) Start() {
	// using waitgroups to block termination gracefully
	var wg sync.WaitGroup

	// It is here that we need to handle asynchronous adapters
	// create channel for incoming queries, qchan
	wg.Add(1)
	qchannel := make(chan types.TemplateChannelRequest, types.DefaultChannelBufferSize)
	d.QueryIn.InitChannel(qchannel)
	go func() {
		defer wg.Done()
		d.QueryIn.Start()
	}()

	// start the domain's query worker
	wg.Add(1)
	go d.processQueryWorker(qchannel, &wg)

	wg.Add(1)
	echannel := make(chan types.TemplateChannelRequest, types.DefaultChannelBufferSize)
	d.EventIn.InitChannel(echannel)
	go func() {
		defer wg.Done()
		d.EventIn.Start()
	}()

	// start the domain's event worker
	wg.Add(1)
	go d.processEventWorker(echannel, &wg)

	wg.Wait()
}

func (d *Domain) processQueryWorker(qchannel chan types.TemplateChannelRequest, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.processQueryWorker",
	})

	defer wg.Done()

	for request := range qchannel {
		logger.Debugf("received a query - %s", request.Operation)

		switch request.Operation {
		// TemplateType
		case string(cacao_common_service.TemplateTypeListQueryOp):
			templateTypeRequest, ok := request.Data.(types.TemplateType)
			if !ok {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoMarshalError("unable to convert request message into template type"),
				}
				return
			}

			if len(templateTypeRequest.ProviderTypes) > 0 {
				templateTypes, err := d.ListTemplateTypesForProviderType(request.Actor, request.Emulator, templateTypeRequest.ProviderTypes[0])
				if request.Response != nil {
					request.Response <- types.TemplateChannelResponse{
						Data:  templateTypes,
						Error: err,
					}
				}
			} else {
				templateTypes, err := d.ListTemplateTypes(request.Actor, request.Emulator)
				if request.Response != nil {
					request.Response <- types.TemplateChannelResponse{
						Data:  templateTypes,
						Error: err,
					}
				}
			}
		case string(cacao_common_service.TemplateTypeGetQueryOp):
			templateTypeRequest, ok := request.Data.(types.TemplateType)
			if !ok {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoMarshalError("unable to convert request message into template type"),
				}
				return
			}

			templateType, err := d.GetTemplateType(request.Actor, request.Emulator, templateTypeRequest.Name)
			if request.Response != nil {
				request.Response <- types.TemplateChannelResponse{
					Data:  templateType,
					Error: err,
				}
			}
		// TemplateSourceType
		case string(cacao_common_service.TemplateSourceTypeListQueryOp):
			templateSourceTypes, err := d.ListTemplateSourceTypes(request.Actor, request.Emulator)
			if request.Response != nil {
				request.Response <- types.TemplateChannelResponse{
					Data:  templateSourceTypes,
					Error: err,
				}
			}
		// Template
		case string(cacao_common_service.TemplateListQueryOp):
			templates, err := d.ListTemplates(request.Actor, request.Emulator)
			if request.Response != nil {
				request.Response <- types.TemplateChannelResponse{
					Data:  templates,
					Error: err,
				}
			}
		case string(cacao_common_service.TemplateGetQueryOp):
			templateRequest, ok := request.Data.(types.Template)
			if !ok {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoMarshalError("unable to convert request message into template"),
				}
				return
			}

			template, err := d.GetTemplate(request.Actor, request.Emulator, templateRequest.ID)
			if request.Response != nil {
				request.Response <- types.TemplateChannelResponse{
					Data:  template,
					Error: err,
				}
			}
		default:
			if request.Response != nil {
				errorMessage := fmt.Sprintf("unhandled query : %s", request.Operation)
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoNotImplementedError(errorMessage),
				}
			}
		}
	}
}

func (d *Domain) processEventWorker(echannel chan types.TemplateChannelRequest, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.processEventWorker",
	})

	defer wg.Done()

	for request := range echannel {
		logger.Debugf("received an event - %s", request.Operation)

		switch request.Operation {
		// TemplateType
		case string(cacao_common_service.TemplateTypeCreateRequestedEvent):
			templateTypeRequest, ok := request.Data.(types.TemplateType)
			if !ok {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoMarshalError("unable to convert data into template type"),
				}
				return
			}

			err := d.CreateTemplateType(request.Actor, request.Emulator, templateTypeRequest, request.TransactionID)
			if request.Response != nil {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: err,
				}
			}
		case string(cacao_common_service.TemplateTypeUpdateRequestedEvent):
			templateTypeRequest, ok := request.Data.(types.TemplateType)
			if !ok {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoMarshalError("unable to convert data into template type"),
				}
				return
			}

			err := d.UpdateTemplateType(request.Actor, request.Emulator, templateTypeRequest, request.UpdateFieldNames, request.TransactionID)
			if request.Response != nil {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: err,
				}
			}
		case string(cacao_common_service.TemplateTypeDeleteRequestedEvent):
			templateTypeRequest, ok := request.Data.(types.TemplateType)
			if !ok {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoMarshalError("unable to convert data into template type"),
				}
				return
			}

			err := d.DeleteTemplateType(request.Actor, request.Emulator, templateTypeRequest.Name, request.TransactionID)
			if request.Response != nil {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: err,
				}
			}
		// Template
		case string(cacao_common_service.TemplateImportRequestedEvent):
			templateRequest, ok := request.Data.(types.Template)
			if !ok {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoMarshalError("unable to convert data into template"),
				}
				return
			}

			err := d.ImportTemplate(request.Actor, request.Emulator, templateRequest, request.CredentialID, request.TransactionID)
			if request.Response != nil {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: err,
				}
			}
		case string(cacao_common_service.TemplateUpdateRequestedEvent):
			templateRequest, ok := request.Data.(types.Template)
			if !ok {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoMarshalError("unable to convert data into template"),
				}
				return
			}

			err := d.UpdateTemplate(request.Actor, request.Emulator, templateRequest, request.UpdateFieldNames, request.TransactionID)
			if request.Response != nil {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: err,
				}
			}
		case string(cacao_common_service.TemplateDeleteRequestedEvent):
			templateRequest, ok := request.Data.(types.Template)
			if !ok {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoMarshalError("unable to convert data into template"),
				}
				return
			}

			err := d.DeleteTemplate(request.Actor, request.Emulator, templateRequest.ID, request.TransactionID)
			if request.Response != nil {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: err,
				}
			}
		case string(cacao_common_service.TemplateSyncRequestedEvent):
			templateRequest, ok := request.Data.(types.Template)
			if !ok {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoMarshalError("unable to convert data into template"),
				}
				return
			}

			err := d.SyncTemplate(request.Actor, request.Emulator, templateRequest.ID, request.CredentialID, request.TransactionID)
			if request.Response != nil {
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: err,
				}
			}
		default:
			if request.Response != nil {
				errorMessage := fmt.Sprintf("unhandled query : %s", request.Operation)
				request.Response <- types.TemplateChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoNotImplementedError(errorMessage),
				}
			}
		}
	}
}
