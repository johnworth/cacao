package domain

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// List retrieves all templates of the user
func (impl *QueryPortImpl) List(actor string, emulator string) ([]types.Template, error) {
	if len(actor) == 0 {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	responseChannel := make(chan types.TemplateChannelResponse)

	// request to model
	impl.Channel <- types.TemplateChannelRequest{
		Actor:     actor,
		Emulator:  emulator,
		Data:      types.Template{},
		Operation: string(cacao_common_service.TemplateListQueryOp),
		Response:  responseChannel,
	}

	// receive response
	templateResponse := <-responseChannel

	if templateResponse.Error != nil {
		return nil, templateResponse.Error
	}

	templates, ok := templateResponse.Data.([]types.Template)
	if !ok {
		return nil, cacao_common_service.NewCacaoMarshalError("unable to convert response data into Template array")
	}

	return templates, nil
}

// Get retrieves the template
func (impl *QueryPortImpl) Get(actor string, emulator string, templateID cacao_common.ID) (types.Template, error) {
	if len(actor) == 0 {
		return types.Template{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(templateID) == 0 {
		return types.Template{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: template ID is empty")
	}

	responseChannel := make(chan types.TemplateChannelResponse)

	// request to model
	impl.Channel <- types.TemplateChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.Template{
			ID: templateID,
		},
		Operation: string(cacao_common_service.TemplateGetQueryOp),
		Response:  responseChannel,
	}

	// receive response
	templateResponse := <-responseChannel

	if templateResponse.Error != nil {
		return types.Template{}, templateResponse.Error
	}

	template, ok := templateResponse.Data.(types.Template)
	if !ok {
		return types.Template{}, cacao_common_service.NewCacaoMarshalError("unable to convert response data into Template")
	}

	return template, nil
}
