package domain

import (
	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// ListTemplateSourceTypes returns all TemplateSourceTypes
func (d *Domain) ListTemplateSourceTypes(actor string, emulator string) ([]cacao_common_service.TemplateSourceType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.ListTemplateSourceTypes",
	})

	logger.Info("List template source types")

	return []cacao_common_service.TemplateSourceType{
		types.TemplateSourceTypeGit,
	}, nil
}
