# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Added `deployment-metadata-service`
- Added `workflow-event-aggregator`
- Added `workspace-microservice`
- Added `template-microservice`
### Changed
- Update integration tests to work with new users microservice
- Add standalone scripts for linting, formatting, and dependency checks
- Switch from Istio ingress to NGINX ingress
- Ansible is being forced to use Python 3, which fixes deployment on Ubuntu 18.04.
- User cluster deployment has been temporarily disabled.
- Microservices related to container based workflows (`build-service`, `cs-service`, `phylax-service`, `pleo`, and `workflow-definition-service`) have been temporarily removed from deployment.
- Tests related to container based workflows have been temporarily disabled
- A deployment target for CI environments has been added.
- An integration test Ansible playbook as been added and configured to run in Gitlab CI.
- References to the "Nafigos" project name have been replaced with "CACAO".
- Refactored `workspace-microservice` to use `cacao-common` utility functions
- Added unit tests to `workspace-microservice` 
- Implemented CLI subcommands for `workspace-microservice`
- Added support for Globus authentication
- Move prerequisite template from JSON file to provider metatdata.
- Inject certain parameter for prerequisite template
- Override value of certain special parameter types at runtime
### Removed
