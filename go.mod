module gitlab.com/cyverse/cacao

go 1.16

require (
	github.com/amonsat/fullname_parser v0.0.0-20180221140204-0879740fa92c
	github.com/armon/go-metrics v0.3.4 // indirect
	github.com/armon/go-radix v1.0.0 // indirect
	github.com/cloudevents/sdk-go v1.2.0
	github.com/cloudevents/sdk-go/v2 v2.3.1
	github.com/coreos/go-oidc v2.2.1+incompatible
	github.com/dgraph-io/ristretto v0.1.0
	github.com/docker/distribution v2.7.1+incompatible
	github.com/eko/gocache v1.2.0
	github.com/fatih/color v1.9.0
	github.com/go-redis/redis/v8 v8.11.0
	github.com/googleapis/gnostic v0.4.0 // indirect
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.7.4
	github.com/hashicorp/go-immutable-radix v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1
	github.com/hashicorp/go-uuid v1.0.2 // indirect
	github.com/hokaccha/go-prettyjson v0.0.0-20190818114111-108c894c2c0e
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mitchellh/cli v1.1.2
	github.com/mitchellh/mapstructure v1.4.1
	github.com/mitchellh/reflectwalk v1.0.1 // indirect
	github.com/nats-io/nats-streaming-server v0.23.0
	github.com/nats-io/nats.go v1.13.0
	github.com/nats-io/stan.go v0.10.0
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/openfaas/faas-cli v0.0.0-20201030090417-6914d950c9ba
	github.com/openfaas/faas-netes v0.0.0-20201205133446-c402b912ce21
	github.com/posener/complete v1.2.1 // indirect
	github.com/pquerna/cachecontrol v0.0.0-20200921180117-858c6e7e6b7e // indirect
	github.com/rs/xid v1.3.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/tidwall/pretty v1.0.1 // indirect
	github.com/xeipuuv/gojsonschema v1.2.0
	gitlab.com/cyverse/cacao-common v0.0.0-20211110165753-9939a5b80951
	go.mongodb.org/mongo-driver v1.5.2
	go.uber.org/zap v1.14.1 // indirect
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/mod v0.5.0 // indirect
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43
	golang.org/x/tools v0.1.7 // indirect
	gopkg.in/square/go-jose.v2 v2.5.1 // indirect
	gopkg.in/src-d/go-billy.v4 v4.3.2
	gopkg.in/src-d/go-git.v4 v4.13.1
	gopkg.in/yaml.v2 v2.3.0
	gotest.tools/v3 v3.0.2
	honnef.co/go/tools v0.0.1-2020.1.6 // indirect
	istio.io/api v0.0.0-20201112235759-fa4ee46c5dc2
	istio.io/client-go v1.8.0
	k8s.io/api v0.18.2
	k8s.io/apimachinery v0.18.2
	k8s.io/client-go v0.18.2
	sigs.k8s.io/yaml v1.2.0
)
