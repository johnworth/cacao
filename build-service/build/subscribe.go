package build

import (
	"encoding/json"

	"gitlab.com/cyverse/cacao/common"

	log "github.com/sirupsen/logrus"
)

// Request is a struct containing the necessary fields for all Pleo
// operations coming from the API
type Request struct {
	common.BasicRequest
	Namespace     string `json:"namespace,omitempty"`
	RunAfterBuild bool   `json:"run,omitempty"`
	// Fields from WorkflowDefinition
	WorkflowDefinition struct {
		Name       string              `json:"name,omitempty"`
		Build      []*common.BuildStep `json:"build,omitempty"`
		Repository common.Repository   `json:"repository,omitempty"`
		Error      string              `json:"error_msg,omitempty"`
	} `json:"data,omitempty"`
}

// PreBuild is a wrapper around the build function to perform some additional
// checks before building. First, it makes sure the WorkflowDefinition actually
// has build instructions. Then, it will run the build function asynchronously for
// each image in the build instructions. Runs synchronously in testing environment
func PreBuild(reqBytes []byte, natsInfo map[string]string) {
	logger := log.WithFields(log.Fields{
		"package":  "build",
		"function": "PreBuild",
	})

	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal JSON bytes into Request")
		return
	}

	logRequest := request
	logRequest.BasicRequest = logRequest.GetRedacted()
	logger = logger.WithFields(log.Fields{"request": logRequest})

	msg, err := json.Marshal(request)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal JSON request")
		return
	}

	// Send straight to Pleo if there is not a Dockerfile to build
	if len(request.WorkflowDefinition.Build) == 0 {
		logger.Warn("unable to build a WorkflowDefinition without Build instructions")
		// If user requested running after build, skip to Run
		if request.RunAfterBuild {
			logger.Info("user requested RunAfterBuild so skipping to Run")
			err = common.StreamingPublish(msg, natsInfo, "Pleo.Run", "Build")
			if err != nil {
				logger.WithFields(log.Fields{"error": err}).Error("unable to send message to Pleo")
				return
			}
		}
		return
	}

	// Start the build concurrently but still record and log the error
	go func() {
		logger.Info("starting build")
		err := build(request, natsInfo)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable build and push container images from WorkflowDefinition")
		}
	}()
	common.StreamingPublish(msg, natsInfo, "Build.Start", "Build")
}
