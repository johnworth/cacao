package build

import (
	"testing"

	"gitlab.com/cyverse/cacao/common"

	"github.com/stretchr/testify/assert"
	core "k8s.io/api/core/v1"
)

func TestCreateRegistrySecretDockerhub(t *testing.T) {
	secret, err := createRegistrySecret("ID", common.Secret{
		ID:       "SecretID",
		Username: "TestUser",
		Value:    "DockerhubToken",
		Type:     common.DockerhubSecret,
	})
	assert.NoError(t, err)
	assert.Equal(t, "ID", secret.ObjectMeta.Name)
	assert.Equal(t,
		`{"auths": {"https://index.docker.io/v1/": {"auth": "VGVzdFVzZXI6RG9ja2VyaHViVG9rZW4="}}}`,
		secret.StringData["config.json"],
	)
	assert.Empty(t, secret.StringData["kaniko-secret.json"])
}

func TestCreateRegistrySecretGCR(t *testing.T) {
	secret, err := createRegistrySecret("ID", common.Secret{
		ID:       "SecretID",
		Username: "TestUser",
		Value:    "R0NSIFNFQ1JFVA==",
		Type:     common.GCRSecret,
	})
	assert.NoError(t, err)
	assert.Equal(t, "ID", secret.ObjectMeta.Name)
	assert.Equal(t, "GCR SECRET", secret.StringData["kaniko-secret.json"])
	assert.Empty(t, secret.StringData["config.json"])
}

func TestCreateRegistrySecretGCRBadEncoding(t *testing.T) {
	secret, err := createRegistrySecret("ID", common.Secret{
		ID:       "SecretID",
		Username: "TestUser",
		Value:    "this is not base64 :^)",
		Type:     common.GCRSecret,
	})
	assert.Error(t, err)
	assert.Empty(t, secret)
	assert.Equal(t, "illegal base64 data at input byte 4", err.Error())
}

func TestCreateJobDockerhub(t *testing.T) {
	job := createJob(
		"resource-name",
		"ID",
		[]*common.BuildStep{
			{
				Image:      "user/image:latest",
				Dockerfile: "Dockerfile",
				Args: []core.EnvVar{
					{Name: "TEST", Value: "TEST"},
				},
			},
		},
		common.Secret{
			ID:       "SecretID",
			Value:    "GithubToken",
			Username: "TestUser",
			Type:     common.GitSecret,
		},
		"github.com/user/project",
		"master",
	)
	assert.NotEmpty(t, job)
	assert.Equal(t, "resource-name", job.ObjectMeta.Name)
	assert.Equal(t, "ID", job.ObjectMeta.Labels["cacao_id"])
	assert.Len(t, job.Spec.Template.Spec.Containers, 1)
	assert.Len(t, job.Spec.Template.Spec.Containers[0].Args, 5)
	assert.Len(t, job.Spec.Template.Spec.Containers[0].Env, 1)
	assert.Equal(t, "DOCKER_CONFIG", job.Spec.Template.Spec.Containers[0].Env[0].Name)
	assert.Equal(t, "/secret", job.Spec.Template.Spec.Containers[0].Env[0].Value)
}

func TestCreateJobGCR(t *testing.T) {
	job := createJob(
		"resource-name",
		"ID",
		[]*common.BuildStep{
			{
				Image:      "gcr.io/user/image:latest",
				Dockerfile: "Dockerfile",
				Args: []core.EnvVar{
					{Name: "TEST", Value: "TEST"},
				},
			},
		},
		common.Secret{
			ID:       "SecretID",
			Value:    "GithubToken",
			Username: "TestUser",
			Type:     common.GitSecret,
		},
		"github.com/user/project",
		"master",
	)
	assert.NotEmpty(t, job)
	assert.Equal(t, "resource-name", job.ObjectMeta.Name)
	assert.Equal(t, "ID", job.ObjectMeta.Labels["cacao_id"])
	assert.Len(t, job.Spec.Template.Spec.Containers, 1)
	assert.Len(t, job.Spec.Template.Spec.Containers[0].Args, 5)
	assert.Len(t, job.Spec.Template.Spec.Containers[0].Env, 1)
	assert.Equal(t, "GOOGLE_APPLICATION_CREDENTIALS", job.Spec.Template.Spec.Containers[0].Env[0].Name)
	assert.Equal(t, "/secret/kaniko-secret.json", job.Spec.Template.Spec.Containers[0].Env[0].Value)
}

func TestBuildArgsArray(t *testing.T) {
	args := []core.EnvVar{
		{Name: "TEST", Value: "WOW"},
		{Name: "TEST2", Value: "WOW2"},
	}
	result := createBuildArgs(args)
	assert.Contains(t, [][]string{
		{"--build-arg", "TEST=WOW", "--build-arg", "TEST2=WOW2"},
		{"--build-arg", "TEST2=WOW2", "--build-arg", "TEST=WOW"},
	}, result)
}
