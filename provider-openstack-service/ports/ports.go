package ports

import (
	"context"

	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// Port is the base interface for Ports, in which all should support an Init() function (but it may be noop)
type Port interface {
	Init(config types.Configuration)
}

// AsyncPort is a special type of port that will has an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly waitgroups)
type AsyncPort interface {
	Port
	Start()
}

// IncomingQueryPort is an example interface for a query port.
// Internal to the adapter there may be additional methods, but at the very least, it is an async port
// Also, incoming and outgoing ports are declared separately for illustrative purposes
// method set signatures will allow one adapter fulfill the purposes of both income and outoing query ports
type IncomingQueryPort interface {
	AsyncPort
	InitChannel(data chan types.CloudEventRequest)
}

// CredentialMS ...
type CredentialMS interface {
	GetCredential(actor, emulator, ID string) (*types.Credential, error)
	ListCredentialsByTag(actor, emulator, tagName string) ([]types.Credential, error)
}

// OpenStack ...
type OpenStack interface {
	GetApplicationCredential(ctx context.Context, id string) (*providers.ApplicationCredential, error)
	GetImage(ctx context.Context, id string) (*providers.Image, error)
	GetFlavor(ctx context.Context, id string) (*providers.Flavor, error)
	GetProject(ctx context.Context, id string) (*providers.Project, error)
	ListApplicationCredentialsForceable(ctx context.Context, force bool) ([]providers.ApplicationCredential, error)
	ListApplicationCredentials(ctx context.Context) ([]providers.ApplicationCredential, error)
	ListImagesForceable(ctx context.Context, force bool) ([]providers.Image, error)
	ListImages(ctx context.Context) ([]providers.Image, error)
	ListFlavorsForceable(ctx context.Context, force bool) ([]providers.Flavor, error)
	ListFlavors(ctx context.Context) ([]providers.Flavor, error)
	ListProjectsForceable(ctx context.Context, force bool) ([]providers.Project, error)
	ListProjects(ctx context.Context) ([]providers.Project, error)
	GetCacheKey(prefix string, username string, credID string) string
}
