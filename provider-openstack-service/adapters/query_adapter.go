package adapters

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/messaging"

	"github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// QueryAdapter implements ports.IncomingQueryPort & ports.OutgoingQueryPort
type QueryAdapter struct {
	queryChan chan types.CloudEventRequest
	clientID  string
	subject   string
	queue     string
	nc        *nats.Conn
	sub       *nats.Subscription
}

// NewQueryAdapter creates a new QueryAdapter
func NewQueryAdapter(conf types.Configuration) (*QueryAdapter, error) {
	retval := &QueryAdapter{subject: conf.NATS.WildcardSubject}
	err := retval.natsConnect(conf)
	if err != nil {
		return nil, err
	}
	retval.clientID = conf.NATS.ClientID
	retval.queue = conf.NATS.QueueGroup
	return retval, nil
}

// Init perform initialization on the adapter
func (adapter *QueryAdapter) Init(conf types.Configuration) {
	err := adapter.natsConnect(conf)
	if err != nil {
		log.Fatal(err)
	}

	adapter.clientID = conf.NATS.ClientID
	adapter.queue = conf.NATS.QueueGroup
}

// natsConnect establish the NATS connection
func (adapter *QueryAdapter) natsConnect(conf types.Configuration) error {
	nc, err := nats.Connect(conf.NATS.URL)
	if err != nil {
		return err
	}
	log.Info("NATS connection established")

	adapter.nc = nc

	return nil
}

// InitChannel takes in the data channel for depositing incoming message
func (adapter *QueryAdapter) InitChannel(c chan types.CloudEventRequest) {
	adapter.queryChan = c
}

// Start starts the queue subscriber
func (adapter *QueryAdapter) Start() {
	err := adapter.natsQueueSubscribe()
	if err != nil {
		log.Fatal(err)
	}
}

// natsQueueSubscribe starts the subscriber
func (adapter *QueryAdapter) natsQueueSubscribe() error {
	// subscribe with adapter.natsCallbck()
	sub, err := adapter.nc.QueueSubscribe(adapter.subject, adapter.queue, adapter.natsCallback)
	if err != nil {
		return err
	}
	adapter.sub = sub
	return nil
}

func (adapter QueryAdapter) natsCallback(m *nats.Msg) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "QueryAdapter.natsCallback",
	})
	ce, err := messaging.ConvertNats(m)
	if err != nil {
		logger.WithError(err).Error("unable to get request from CloudEvent")
		return
	}

	log.WithFields(log.Fields{"request": string(ce.Data())}).Trace("received message")
	var request = types.CloudEventRequest{
		CloudEvent: ce,
		Replyer: QueryReplyer{
			replySubject: m.Reply,
			adapter:      &adapter,
		},
	}

	// deposit query into channel
	adapter.queryChan <- request
}

// QueryReplyer implements types.QueryReply and allows the domain to send
// a response.
type QueryReplyer struct {
	replySubject string
	adapter      *QueryAdapter
}

// Reply send a response
func (r QueryReplyer) Reply(ce cloudevents.Event) error {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "QueryReplyer.Reply",
		"eventType": ce.Type(),
		"tid":       messaging.GetTransactionID(&ce),
	})
	marshal, err := ce.MarshalJSON()
	if err != nil {
		logger.WithError(err).Error("fail to marshal reply cloudevent into JSON")
		return err
	}
	if err = r.adapter.nc.Publish(r.replySubject, marshal); err != nil {
		logger.WithError(err).Error("fail to publish reply")
		return err
	}
	logger.WithField("dataLength", len(ce.Data())).Info("query replied")
	logger.WithField("data", string(ce.Data())).Trace("reply")
	return nil
}
