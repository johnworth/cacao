package adapters

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"

	"github.com/eko/gocache/cache"
	"github.com/eko/gocache/store"
)

// OpenStackAdapter is used to access metadata from OpenStack clusters
type OpenStackAdapter struct {
	cli   OpenStackCLI
	cache cache.CacheInterface
	ttl   time.Duration
	env   types.Environment
}

// NewOpenStackAdapter creates a new *OpenStackAdapter.
func NewOpenStackAdapter(ttl time.Duration, cacheMgr cache.CacheInterface) *OpenStackAdapter {
	cli := &OpenStackCLIAdapter{}
	retval := &OpenStackAdapter{
		cli:   cli,
		cache: cacheMgr,
		ttl:   ttl,
		env:   splitEnvironment(os.Environ()),
	}
	return retval
}

func splitEnvironment(env []string) types.Environment {
	retval := map[string]string{}

	for _, setting := range env {
		parts := strings.Split(setting, "=")
		if len(parts) != 2 {
			continue
		}
		retval[parts[0]] = parts[1]
	}

	return retval
}

// GetApplicationCredential attempts to retrieve a specific OpenStack application credential by ID. If a cached
// value exists, it will be returned rather than contacting the OpenStackCluster.
func (o *OpenStackAdapter) GetApplicationCredential(ctx context.Context, id string) (*providers.ApplicationCredential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.GetApplicationCredential",
	})
	var (
		applicationCredential *providers.ApplicationCredential
		err                   error
	)

	applicationCredentials, err := o.ListApplicationCredentials(ctx)
	if err != nil {
		return nil, err
	}

	for _, i := range applicationCredentials {
		if i.ID == id {
			applicationCredential = &i
			logger.WithField("applicationCredential", applicationCredential.ID).Info("ApplicationCredential fetched")
			return applicationCredential, nil
		}
	}

	err = fmt.Errorf("ApplicationCredential not found: %s", id)
	logger.WithError(err).Error()
	return nil, err
}

// GetImage attempts to retrieve a specific OpenStack image by ID. If a cached
// value exists, it will be returned rather than contacting the OpenStackCluster.
func (o *OpenStackAdapter) GetImage(ctx context.Context, id string) (*providers.Image, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.GetImage",
	})
	var (
		image *providers.Image
		err   error
	)

	images, err := o.ListImages(ctx)
	if err != nil {
		return nil, err
	}

	for _, i := range images {
		if i.ID == id {
			image = &i
			logger.WithField("image", image.ID).Info("image fetched")
			return image, nil
		}
	}

	err = fmt.Errorf("image not found: %s", id)
	logger.WithError(err).Error()
	return nil, err
}

// GetFlavor attempts to retrieve a specific OpenStack flavor by ID. If a cached
// value exists, it will be returned rather than contacting the OpenStackCluster.
func (o *OpenStackAdapter) GetFlavor(ctx context.Context, id string) (*providers.Flavor, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.GetFlavor",
	})
	var (
		flavor *providers.Flavor
		err    error
	)

	flavors, err := o.ListFlavors(ctx)
	if err != nil {
		return nil, err
	}

	for _, f := range flavors {
		if f.ID == id {
			flavor = &f
			logger.WithField("flavor", flavor.ID).Info("flavor fetched")
			return flavor, nil
		}
	}

	err = fmt.Errorf("flavor not found: %s", id)
	logger.WithError(err).Error()
	return nil, err
}

// GetProject attempts to retrieve a specific OpenStack Project by ID. If a cached
// value exists, it will be returned rather than contacting the OpenStackCluster.
func (o *OpenStackAdapter) GetProject(ctx context.Context, id string) (*providers.Project, error) {
	var (
		project *providers.Project
		err     error
	)

	projects, err := o.ListProjects(ctx)
	if err != nil {
		return nil, err
	}

	for _, f := range projects {
		if f.ID == id {
			project = &f
			break
		}
	}

	if project == nil {
		err = fmt.Errorf("project not found: %s", id)
	}

	return project, err
}

// unused
// func (o *OpenStackAdapter) getCLI(ctx context.Context, prefix, id string, cmd types.OpenStackGetter) ([]byte, error) {
// 	var (
// 		bytes            []byte
// 		outputString     string
// 		err              error
// 		username, credID string
// 	)
// 	username = ctx.Value(types.UsernameKey).(string)
// 	credID = ctx.Value(types.CredentialsIDKey).(string)
// 	key := o.GetCacheKey(prefix, username, credID)

// 	value, err := o.cache.Get(key)
// 	if err != nil {
// 		bytes, err = cmd(ctx, o.env, id)
// 		outputString = string(bytes)
// 		o.cache.Set(key, outputString, &store.Options{
// 			Cost:       int64(len(bytes)),
// 			Expiration: o.ttl,
// 		})
// 	} else {
// 		outputString = value.(string)
// 	}

// 	return []byte(outputString), err
// }

// ListApplicationCredentialsForceable attempts to list all the available flavors in an OpenStack
// cluster. If a cached value exists, it will be returned rather than
// contacting the OpenStackCluster. If force is set to true, the cache is reset
// with the results of a new execution of the listing command.
func (o *OpenStackAdapter) ListApplicationCredentialsForceable(ctx context.Context, force bool) ([]providers.ApplicationCredential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListApplicationCredentialsForceable",
	})
	var (
		applicationCredentials       []providers.ApplicationCredential
		applicationCredentialsString string
		username                     string
		credID                       string
		err                          error
	)
	username = ctx.Value(types.UsernameKey).(string)
	credID = ctx.Value(types.CredentialsIDKey).(string)

	cacheKey := o.GetCacheKey(types.CachePrefixListApplicationCredentials, username, credID)
	value, err := o.cache.Get(cacheKey)

	if err != nil || force {
		logger.WithError(err).Debug("cache miss")
		applicationCredentialBytes, err2 := o.cli.ListApplicationCredentials(ctx, o.env)
		if err2 != nil {
			return nil, err2
		}
		applicationCredentialsString = string(applicationCredentialBytes)
		err = o.cache.Set(cacheKey, applicationCredentialsString, &store.Options{
			Cost:       int64(len(applicationCredentialBytes)),
			Expiration: o.ttl,
		})
		if err != nil {
			logger.WithError(err).Error("fail to set cache")
		}
	} else {
		applicationCredentialsString = value.(string)
	}

	err = json.Unmarshal([]byte(applicationCredentialsString), &applicationCredentials)
	logger.WithField("length", len(applicationCredentials)).Info("ApplicationCredentials listed")
	return applicationCredentials, err
}

// ListApplicationCredentials calls ListApplicationCredentialsForceable with the force option set to false.
// You should use this one, it's rare to want to bypass the cache.
func (o *OpenStackAdapter) ListApplicationCredentials(ctx context.Context) ([]providers.ApplicationCredential, error) {
	return o.ListApplicationCredentialsForceable(ctx, false)
}

// ListImagesForceable attempts to list all the available flavors in an OpenStack
// cluster. If a cached value exists, it will be returned rather than
// contacting the OpenStackCluster. If force is set to true, the cache is reset
// with the results of a new execution of the listing command.
func (o *OpenStackAdapter) ListImagesForceable(ctx context.Context, force bool) ([]providers.Image, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListImagesForceable",
	})
	var (
		images       []providers.Image
		imagesString string
		username     string
		credID       string
		err          error
	)
	username = ctx.Value(types.UsernameKey).(string)
	credID = ctx.Value(types.CredentialsIDKey).(string)

	cacheKey := o.GetCacheKey(types.CachePrefixListImages, username, credID)
	value, err := o.cache.Get(cacheKey)

	if err != nil || force {
		logger.WithError(err).Debug("cache miss")
		imageBytes, err2 := o.cli.ListImages(ctx, o.env)
		if err2 != nil {
			return nil, err2
		}
		imagesString = string(imageBytes)
		err = o.cache.Set(cacheKey, imagesString, &store.Options{
			Cost:       int64(len(imageBytes)),
			Expiration: o.ttl,
		})
		if err != nil {
			logger.WithError(err).Error("fail to set cache")
		}
	} else {
		imagesString = value.(string)
	}

	err = json.Unmarshal([]byte(imagesString), &images)
	logger.WithField("length", len(images)).Info("images listed")
	return images, err
}

// ListImages calls ListImagesForceable with the force option set to false.
// You should use this one, it's rare to want to bypass the cache.
func (o *OpenStackAdapter) ListImages(ctx context.Context) ([]providers.Image, error) {
	return o.ListImagesForceable(ctx, false)
}

// ListFlavorsForceable attempts to list all the available flavors in an OpenStack
// cluster. If a cached value exists, it will be returned rather than
// contacting the OpenStackCluster.
func (o *OpenStackAdapter) ListFlavorsForceable(ctx context.Context, force bool) ([]providers.Flavor, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListFlavorsForceable",
	})
	var (
		flavors       []providers.Flavor
		flavorsString string
		err           error
		username      string
		credID        string
	)

	username = ctx.Value(types.UsernameKey).(string)
	credID = ctx.Value(types.CredentialsIDKey).(string)

	cacheKey := o.GetCacheKey(types.CachePrefixListFlavors, username, credID)
	value, err := o.cache.Get(cacheKey)
	if err != nil || force {
		logger.WithError(err).Debug("cache miss")
		flavorBytes, err2 := o.cli.ListFlavors(ctx, o.env)
		if err2 != nil {
			return nil, err2
		}
		flavorsString = string(flavorBytes)
		err = o.cache.Set(cacheKey, flavorsString, &store.Options{
			Cost:       int64(len(flavorBytes)),
			Expiration: o.ttl,
		})
		if err != nil {
			logger.WithError(err).Error("fail to set cache")
		}
	} else {
		flavorsString = value.(string)
	}

	err = json.Unmarshal([]byte(flavorsString), &flavors)
	logger.WithField("length", len(flavors)).Info("flavors listed")
	return flavors, err
}

// ListFlavors calls ListFlavorsForceable with the force option set to false.
// If in doubt, call this one. It's rare to want to bypass the cache.
func (o *OpenStackAdapter) ListFlavors(ctx context.Context) ([]providers.Flavor, error) {
	return o.ListFlavorsForceable(ctx, false)
}

// ListProjectsForceable attempts to list all the available projects in an OpenStack
// cluster. If a cached value exists, it will be returned rather than
// contacting the OpenStackCluster.
func (o *OpenStackAdapter) ListProjectsForceable(ctx context.Context, force bool) ([]providers.Project, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListProjectsForceable",
	})
	var (
		projects       []providers.Project
		projectsString string
		err            error
		username       string
		credID         string
	)

	username = ctx.Value(types.UsernameKey).(string)
	credID = ctx.Value(types.CredentialsIDKey).(string)

	cacheKey := o.GetCacheKey(types.CachePrefixListProjects, username, credID)
	value, err := o.cache.Get(cacheKey)
	if err != nil || force {
		logger.WithError(err).Debug("cache miss")
		projectBytes, err2 := o.cli.ListProjects(ctx, o.env)
		if err2 != nil {
			return nil, err2
		}
		projectsString = string(projectBytes)
		err = o.cache.Set(cacheKey, projectsString, &store.Options{
			Cost:       int64(len(projectBytes)),
			Expiration: o.ttl,
		})
		if err != nil {
			logger.WithError(err).Error("fail to set cache")
		}
	} else {
		projectsString = value.(string)
	}

	err = json.Unmarshal([]byte(projectsString), &projects)
	logger.WithField("length", len(projects)).Info("Projects listed")
	return projects, err
}

// ListProjects calls ListProjectsForceable with the force option set to false.
// If in doubt, call this one. It's rare to want to bypass the cache.
func (o *OpenStackAdapter) ListProjects(ctx context.Context) ([]providers.Project, error) {
	return o.ListProjectsForceable(ctx, false)
}

// GetCacheKey accepts a prefix and generates a key that can be used to retrieve
// a cached value.  By convention, this prefix should match the function which
// is calling GetCacheKey.
func (o *OpenStackAdapter) GetCacheKey(prefix string, username string, credID string) string {
	return fmt.Sprintf("%s.%s.%s", prefix, username, credID)
}
