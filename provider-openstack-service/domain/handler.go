package domain

import (
	"context"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/hashicorp/go-multierror"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
)

// Handler is query handler
type Handler interface {
	Handle(ctx context.Context, request providers.ProviderRequest) Reply
}

// Reply is an interface implemented by all reply structs in cacao-common (e.g. providers.ImageListReply)
type Reply interface {
	ToCloudEvent(source string) (cloudevents.Event, error)
}

// ApplicationCredentialListHandler handles getting a list of ApplicationCredentials
type ApplicationCredentialListHandler struct {
	OpenStack    ports.OpenStack
	CredSelector CredentialSelector
}

// Handle ...
func (h ApplicationCredentialListHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	cred, err := h.CredSelector.SelectCredential(request)
	if err != nil {
		return h.errorReply(request, err)
	}

	applicationCredentialList, err := h.OpenStack.ListApplicationCredentials(withCredential(ctx, cred))
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.ApplicationCredentialListReply{
		BaseProviderReply:      requestToBaseReply(request),
		ApplicationCredentials: applicationCredentialList,
	}
}

func (h ApplicationCredentialListHandler) errorReply(request providers.ProviderRequest, err error) providers.ApplicationCredentialListReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ApplicationCredentialListHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.ApplicationCredentialListReply{
		BaseProviderReply:      errorReply(request, err),
		ApplicationCredentials: nil,
	}
}

// ApplicationCredentialGetHandler handles getting a single ApplicationCredential
type ApplicationCredentialGetHandler struct {
	OpenStack    ports.OpenStack
	CredSelector CredentialSelector
}

// Handle ...
func (h ApplicationCredentialGetHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	applicationCredentialID, ok := request.Args.(string)
	if !ok {
		return h.errorReply(request, service.NewCacaoMarshalError("flavor ID is not string"))
	}
	cred, err := h.CredSelector.SelectCredential(request)
	if err != nil {
		return h.errorReply(request, err)
	}

	ApplicationCredential, err := h.OpenStack.GetApplicationCredential(withCredential(ctx, cred), applicationCredentialID)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.GetApplicationCredentialReply{
		BaseProviderReply:     requestToBaseReply(request),
		ApplicationCredential: ApplicationCredential,
	}
}

func (h ApplicationCredentialGetHandler) errorReply(request providers.ProviderRequest, err error) providers.GetApplicationCredentialReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ApplicationCredentialGetHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.GetApplicationCredentialReply{
		BaseProviderReply:     errorReply(request, err),
		ApplicationCredential: nil,
	}
}

// ImageListHandler handles getting a list of images
type ImageListHandler struct {
	OpenStack    ports.OpenStack
	CredSelector CredentialSelector
}

// Handle ...
func (h ImageListHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	var args providers.ImageListingArgs
	err := mapstructure.Decode(request.Args, &args)
	if err != nil {
		return h.errorReply(request, service.NewCacaoMarshalError(err.Error()))
	}
	cred, err := h.CredSelector.SelectCredential(request)
	if err != nil {
		return h.errorReply(request, err)
	}

	imageList, err := h.OpenStack.ListImages(withCredential(ctx, cred))
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.ImageListReply{
		BaseProviderReply: requestToBaseReply(request),
		Images:            imageList,
	}
}

func (h ImageListHandler) errorReply(request providers.ProviderRequest, err error) providers.ImageListReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ImageListHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.ImageListReply{
		BaseProviderReply: errorReply(request, err),
		Images:            nil,
	}
}

// ImageGetHandler handles getting a single image
type ImageGetHandler struct {
	OpenStack    ports.OpenStack
	CredSelector CredentialSelector
}

// Handle ...
func (h ImageGetHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	imageID, ok := request.Args.(string)
	if !ok {
		return h.errorReply(request, service.NewCacaoMarshalError("image ID is not string"))
	}
	cred, err := h.CredSelector.SelectCredential(request)
	if err != nil {
		return h.errorReply(request, err)
	}

	image, err := h.OpenStack.GetImage(withCredential(ctx, cred), imageID)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.GetImageReply{
		BaseProviderReply: requestToBaseReply(request),
		Image:             image,
	}
}

func (h ImageGetHandler) errorReply(request providers.ProviderRequest, err error) providers.GetImageReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ImageGetHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.GetImageReply{
		BaseProviderReply: errorReply(request, err),
		Image:             nil,
	}
}

// FlavorListHandler handles getting a list of flavors
type FlavorListHandler struct {
	OpenStack    ports.OpenStack
	CredSelector CredentialSelector
}

// Handle ...
func (h FlavorListHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	var args providers.FlavorListingArgs
	err := mapstructure.Decode(request.Args, &args)
	if err != nil {
		return h.errorReply(request, service.NewCacaoMarshalError(err.Error()))
	}
	cred, err := h.CredSelector.SelectCredential(request)
	if err != nil {
		return h.errorReply(request, err)
	}

	flavorList, err := h.OpenStack.ListFlavors(withCredential(ctx, cred))
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.FlavorListReply{
		BaseProviderReply: requestToBaseReply(request),
		Flavors:           flavorList,
	}
}

func (h FlavorListHandler) errorReply(request providers.ProviderRequest, err error) providers.FlavorListReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "FlavorListHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.FlavorListReply{
		BaseProviderReply: errorReply(request, err),
		Flavors:           nil,
	}
}

// FlavorGetHandler handles getting a single flavor
type FlavorGetHandler struct {
	OpenStack    ports.OpenStack
	CredSelector CredentialSelector
}

// Handle ...
func (h FlavorGetHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	flavorID, ok := request.Args.(string)
	if !ok {
		return h.errorReply(request, service.NewCacaoMarshalError("flavor ID is not string"))
	}
	cred, err := h.CredSelector.SelectCredential(request)
	if err != nil {
		return h.errorReply(request, err)
	}

	flavor, err := h.OpenStack.GetFlavor(withCredential(ctx, cred), flavorID)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.GetFlavorReply{
		BaseProviderReply: requestToBaseReply(request),
		Flavor:            flavor,
	}
}

func (h FlavorGetHandler) errorReply(request providers.ProviderRequest, err error) providers.GetFlavorReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "FlavorGetHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.GetFlavorReply{
		BaseProviderReply: errorReply(request, err),
		Flavor:            nil,
	}
}

// ProjectListHandler handles getting a list of projects
type ProjectListHandler struct {
	OpenStack    ports.OpenStack
	CredSelector CredentialSelector
}

// Handle ...
func (h ProjectListHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	cred, err := h.CredSelector.SelectCredential(request)
	if err != nil {
		return h.errorReply(request, err)
	}

	projectList, err := h.OpenStack.ListProjects(withCredential(ctx, cred))
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.ProjectListReply{
		BaseProviderReply: requestToBaseReply(request),
		Projects:          projectList,
	}
}

func (h ProjectListHandler) errorReply(request providers.ProviderRequest, err error) providers.ProjectListReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ProjectListHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.ProjectListReply{
		BaseProviderReply: errorReply(request, err),
		Projects:          nil,
	}
}

// ProjectGetHandler handles getting a single Project
type ProjectGetHandler struct {
	OpenStack    ports.OpenStack
	CredSelector CredentialSelector
}

// Handle ...
func (h ProjectGetHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	projectID, ok := request.Args.(string)
	if !ok {
		return h.errorReply(request, service.NewCacaoMarshalError("Project ID is not string"))
	}
	cred, err := h.CredSelector.SelectCredential(request)
	if err != nil {
		return h.errorReply(request, err)
	}

	Project, err := h.OpenStack.GetProject(withCredential(ctx, cred), projectID)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.GetProjectReply{
		BaseProviderReply: requestToBaseReply(request),
		Project:           Project,
	}
}

func (h ProjectGetHandler) errorReply(request providers.ProviderRequest, err error) providers.GetProjectReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ProjectGetHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.GetProjectReply{
		BaseProviderReply: errorReply(request, err),
		Project:           nil,
	}
}

// CachePopulateHandler ...
type CachePopulateHandler struct {
	OpenStack    ports.OpenStack
	CredSelector CredentialSelector
}

// Handle ...
func (h CachePopulateHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "CachePopulateHandler.Handle",
	})
	cred, err := h.CredSelector.SelectCredential(request)
	if err != nil {
		logger.WithError(err).Error("fail to fetch credential")
		return h.errorReply(request, err)
	}

	var errors error
	_, err = h.OpenStack.ListApplicationCredentialsForceable(withCredential(ctx, cred), true)
	if err != nil {
		logger.WithError(err).Error("fail to force fetch application credential list")
		errors = multierror.Append(errors, err)
	}
	_, err = h.OpenStack.ListFlavorsForceable(withCredential(ctx, cred), true)
	if err != nil {
		logger.WithError(err).Error("fail to force fetch flavor list")
		errors = multierror.Append(errors, err)
	}
	_, err = h.OpenStack.ListImagesForceable(withCredential(ctx, cred), true)
	if err != nil {
		logger.WithError(err).Error("fail to force fetch image list")
		errors = multierror.Append(errors, err)
	}
	_, err = h.OpenStack.ListProjectsForceable(withCredential(ctx, cred), true)
	if err != nil {
		logger.WithError(err).Error("fail to force fetch project list")
		errors = multierror.Append(errors, err)
	}
	if errors != nil {
		return h.errorReply(request, errors)
	}

	logger.Info("success")
	return providers.CachePopulateReply{
		Session:   request.Session,
		Operation: request.Operation,
		Provider:  request.Provider,
	}
}

func (h CachePopulateHandler) errorReply(request providers.ProviderRequest, err error) providers.CachePopulateReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "CachePopulateHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.CachePopulateReply(errorReply(request, err))
}
