package domain

import (
	"context"
	"encoding/json"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/adapters"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// Domain define the functions that a Domain type must support.
type Domain interface {
	Init(c types.Configuration)
	Start()
}

// OpenStackDomain is a domain that lists images/flavors from OpenStack and writes the responses out
type OpenStackDomain struct {
	config    *types.Configuration
	QueryIn   ports.IncomingQueryPort
	OpenStack ports.OpenStack
	CredMS    ports.CredentialMS
}

// NewOpenStackDomain returns a new *ImageLister. Init is called
// on it as part of this function.
func NewOpenStackDomain(c *types.Configuration, queryIn ports.IncomingQueryPort, openstack *adapters.OpenStackAdapter, credMS ports.CredentialMS) *OpenStackDomain {
	retval := &OpenStackDomain{
		config:    c,
		QueryIn:   queryIn,
		OpenStack: openstack,
		CredMS:    credMS,
	}
	return retval
}

// Start fires up the domain object, allowing it to listen for incoming queries.
func (domain *OpenStackDomain) Start() {
	var wg sync.WaitGroup

	incomingQueryChan := make(chan types.CloudEventRequest, domain.config.DefaultChannelBufferSize)
	domain.QueryIn.InitChannel(incomingQueryChan)
	go domain.QueryIn.Start()

	for i := 0; i < domain.config.QueryWorkerCount; i++ {
		wg.Add(1)
		go domain.processQueryWorker(incomingQueryChan, &wg)
	}

	wg.Wait()
}

// processQueryWorker performs the listing and writes out the response on the ResponsePort.
func (domain *OpenStackDomain) processQueryWorker(cloudEventReqChan chan types.CloudEventRequest, wg *sync.WaitGroup) {
	defer wg.Done()

	origin := context.Background()

	for cloudEventReq := range cloudEventReqChan {
		ctx, cancel := context.WithTimeout(origin, time.Second*60)
		domain.processQuery(ctx, cloudEventReq)
		cancel()
	}
}

func (domain *OpenStackDomain) processQuery(ctx context.Context, cloudEventReq types.CloudEventRequest) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "OpenStackDomain.processQuery",
	})
	request, err := domain.parseRequest(cloudEventReq.CloudEvent)
	if err != nil {
		logger.WithError(err).Error("fail to parse request")
		return
	}
	ctx = context.WithValue(ctx, types.UsernameKey, request.SessionActor)
	ctx = context.WithValue(ctx, types.TransactionIDKey, messaging.GetTransactionID(&cloudEventReq.CloudEvent))
	handler := domain.dispatchRequest(request)
	if handler == nil {
		logger.WithField("op", request.Operation).Error("no handler selected for query")
		return
	}
	response := handler.Handle(ctx, request)
	replyCe, err := response.ToCloudEvent("openstack-provider-service")
	if err != nil {
		logger.WithError(err).Error("fail to convert reply to cloudevent")
		return
	}
	if err = cloudEventReq.Replyer.Reply(replyCe); err != nil {
		logger.WithError(err).Error("fail to send reply")
	}
}

func (domain *OpenStackDomain) parseRequest(ce cloudevents.Event) (providers.ProviderRequest, error) {
	var req providers.ProviderRequest
	err := json.Unmarshal(ce.Data(), &req)
	if err != nil {
		return providers.ProviderRequest{}, err
	}
	return req, nil
}

// selects a handler based on the operation
func (domain *OpenStackDomain) dispatchRequest(request providers.ProviderRequest) Handler {
	credSelector := CredentialSelector{domain.CredMS}
	switch request.Operation {
	case providers.ApplicationCredentialsGetOp:
		return ApplicationCredentialGetHandler{domain.OpenStack, credSelector}
	case providers.ApplicationCredentialsListOp:
		return ApplicationCredentialListHandler{domain.OpenStack, credSelector}
	case providers.ImagesGetOp:
		return ImageGetHandler{domain.OpenStack, credSelector}
	case providers.ImagesListOp:
		return ImageListHandler{domain.OpenStack, credSelector}
	case providers.FlavorsGetOp:
		return FlavorGetHandler{domain.OpenStack, credSelector}
	case providers.FlavorsListOp:
		return FlavorListHandler{domain.OpenStack, credSelector}
	case providers.ProjectsGetOp:
		return ProjectGetHandler{domain.OpenStack, credSelector}
	case providers.ProjectsListOp:
		return ProjectListHandler{domain.OpenStack, credSelector}
	// Force the cache to get filled in response to the cache.populate operation.
	// Remember, the user is set in the environment variables passed along in the
	// credentials, so the username will not appear in the args here.
	case providers.CachePopulateOp:
		return CachePopulateHandler{domain.OpenStack, credSelector}
	default:
		return nil
	}
}
