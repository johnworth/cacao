package domain

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	portmocks "gitlab.com/cyverse/cacao/provider-openstack-service/ports/mocks"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"testing"
)

func TestImageListHandler_Handle(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID: credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation:  providers.ImagesListOp,
			Provider:   common.NewID("provider"),
			Credential: credID,
			Args:       nil,
		}
		imageList = []providers.Image{
			{
				ID:              "image-1",
				Name:            "image-1-name",
				DiskFormat:      "",
				ContainerFormat: "",
				Size:            123,
				Checksum:        "",
				Status:          "",
				Visibility:      "",
				Protected:       false,
				Project:         "",
				Tags:            nil,
			},
		}
	)
	expectedCtx := context.WithValue(context.Background(), types.CredentialsIDKey, credID)
	expectedCtx = context.WithValue(expectedCtx, types.EnvironmentKey, cred.OpenStackEnv)
	osMock := &portmocks.OpenStack{}
	osMock.On("ListImages", expectedCtx).Return(imageList, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", actor, emulator, credID).Return(&cred, nil)
	h := ImageListHandler{
		OpenStack:    osMock,
		CredSelector: CredentialSelector{credMock},
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.ImageListReply{}, reply) {
		return
	}

	imageListReply := reply.(providers.ImageListReply)
	assert.NoError(t, imageListReply.Session.GetServiceError())
	assert.Equal(t, imageList, imageListReply.Images)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageListHandler_Handle_CredentialFetchFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation:  providers.ImagesListOp,
			Provider:   common.NewID("provider"),
			Credential: credID,
			Args:       nil,
		}
	)
	osMock := &portmocks.OpenStack{}
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", actor, emulator, credID).Return(nil, errors.New("failed"))
	h := ImageListHandler{
		OpenStack:    osMock,
		CredSelector: CredentialSelector{credMock},
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.ImageListReply{}, reply) {
		return
	}

	imageListReply := reply.(providers.ImageListReply)
	assert.Error(t, imageListReply.Session.GetServiceError())
	assert.Nil(t, imageListReply.Images)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageListHandler_Handle_OperationFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID: credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation:  providers.ImagesListOp,
			Provider:   common.NewID("provider"),
			Credential: credID,
			Args:       nil,
		}
	)
	expectedCtx := context.WithValue(context.Background(), types.CredentialsIDKey, credID)
	expectedCtx = context.WithValue(expectedCtx, types.EnvironmentKey, cred.OpenStackEnv)
	osMock := &portmocks.OpenStack{}
	osMock.On("ListImages", expectedCtx).Return(nil, errors.New("failed"))
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", actor, emulator, credID).Return(&cred, nil)
	h := ImageListHandler{
		OpenStack:    osMock,
		CredSelector: CredentialSelector{credMock},
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.ImageListReply{}, reply) {
		return
	}

	imageListReply := reply.(providers.ImageListReply)
	assert.Error(t, imageListReply.Session.GetServiceError())
	assert.Nil(t, imageListReply.Images)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageGetHandler_Handle(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID: credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		imageID = "image-id-123"
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation:  providers.ImagesGetOp,
			Provider:   common.NewID("provider"),
			Credential: credID,
			Args:       imageID,
		}
		image = providers.Image{
			ID:              imageID,
			Name:            "image-1-name",
			DiskFormat:      "",
			ContainerFormat: "",
			Size:            123,
			Checksum:        "",
			Status:          "",
			Visibility:      "",
			Protected:       false,
			Project:         "",
			Tags:            nil,
		}
	)
	expectedCtx := context.WithValue(context.Background(), types.CredentialsIDKey, credID)
	expectedCtx = context.WithValue(expectedCtx, types.EnvironmentKey, cred.OpenStackEnv)
	osMock := &portmocks.OpenStack{}
	osMock.On("GetImage", expectedCtx, imageID).Return(&image, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", actor, emulator, credID).Return(&cred, nil)
	h := ImageGetHandler{
		OpenStack:    osMock,
		CredSelector: CredentialSelector{credMock},
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetImageReply{}, reply) {
		return
	}

	getImageReply := reply.(providers.GetImageReply)
	assert.NoError(t, getImageReply.Session.GetServiceError())
	assert.NotNil(t, getImageReply.Image)
	assert.Equal(t, image, *getImageReply.Image)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageGetHandler_Handle_BadArgs(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation:  providers.ImagesGetOp,
			Provider:   common.NewID("provider"),
			Credential: credID,
			Args:       nil,
		}
	)
	osMock := &portmocks.OpenStack{}
	credMock := &portmocks.CredentialMS{}
	h := ImageGetHandler{
		OpenStack:    osMock,
		CredSelector: CredentialSelector{credMock},
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetImageReply{}, reply) {
		return
	}

	getImageReply := reply.(providers.GetImageReply)
	assert.Error(t, getImageReply.Session.GetServiceError())
	assert.Nil(t, getImageReply.Image)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageGetHandler_Handle_CredentialFetchFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		imageID  = "image-id-123"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation:  providers.ImagesGetOp,
			Provider:   common.NewID("provider"),
			Credential: credID,
			Args:       imageID,
		}
	)
	osMock := &portmocks.OpenStack{}
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", actor, emulator, credID).Return(nil, errors.New("failed"))
	h := ImageGetHandler{
		OpenStack:    osMock,
		CredSelector: CredentialSelector{credMock},
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetImageReply{}, reply) {
		return
	}

	getImageReply := reply.(providers.GetImageReply)
	assert.Error(t, getImageReply.Session.GetServiceError())
	assert.Nil(t, getImageReply.Image)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestImageGetHandler_Handle_OperationFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID: credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		imageID = "image-id-123"
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation:  providers.ImagesGetOp,
			Provider:   common.NewID("provider"),
			Credential: credID,
			Args:       imageID,
		}
	)
	expectedCtx := context.WithValue(context.Background(), types.CredentialsIDKey, credID)
	expectedCtx = context.WithValue(expectedCtx, types.EnvironmentKey, cred.OpenStackEnv)
	osMock := &portmocks.OpenStack{}
	osMock.On("GetImage", expectedCtx, imageID).Return(nil, errors.New("failed"))
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", actor, emulator, credID).Return(&cred, nil)
	h := ImageGetHandler{
		OpenStack:    osMock,
		CredSelector: CredentialSelector{credMock},
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetImageReply{}, reply) {
		return
	}

	getImageReply := reply.(providers.GetImageReply)
	assert.Error(t, getImageReply.Session.GetServiceError())
	assert.Nil(t, getImageReply.Image)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorListHandler_Handle(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID: credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation:  providers.FlavorsListOp,
			Provider:   common.NewID("provider"),
			Credential: credID,
			Args:       nil,
		}
		flavorList = []providers.Flavor{
			{
				ID:        "flavor-1",
				Name:      "flavor-1-name",
				RAM:       123,
				Ephemeral: 234,
				VCPUs:     1,
				IsPublic:  false,
				Disk:      345,
			},
		}
	)
	expectedCtx := context.WithValue(context.Background(), types.CredentialsIDKey, credID)
	expectedCtx = context.WithValue(expectedCtx, types.EnvironmentKey, cred.OpenStackEnv)
	osMock := &portmocks.OpenStack{}
	osMock.On("ListFlavors", expectedCtx).Return(flavorList, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", actor, emulator, credID).Return(&cred, nil)
	h := FlavorListHandler{
		OpenStack:    osMock,
		CredSelector: CredentialSelector{credMock},
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.FlavorListReply{}, reply) {
		return
	}

	flavorListReply := reply.(providers.FlavorListReply)
	assert.NoError(t, flavorListReply.Session.GetServiceError())
	assert.Equal(t, flavorList, flavorListReply.Flavors)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorListHandler_Handle_CredentialFetchFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation:  providers.FlavorsListOp,
			Provider:   common.NewID("provider"),
			Credential: credID,
			Args:       nil,
		}
	)
	osMock := &portmocks.OpenStack{}
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", actor, emulator, credID).Return(nil, errors.New("failed"))
	h := FlavorListHandler{
		OpenStack:    osMock,
		CredSelector: CredentialSelector{credMock},
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.FlavorListReply{}, reply) {
		return
	}

	flavorListReply := reply.(providers.FlavorListReply)
	assert.Error(t, flavorListReply.Session.GetServiceError())
	assert.Nil(t, flavorListReply.Flavors)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorListHandler_Handle_OperationFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID: credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation:  providers.FlavorsListOp,
			Provider:   common.NewID("provider"),
			Credential: credID,
			Args:       nil,
		}
	)
	expectedCtx := context.WithValue(context.Background(), types.CredentialsIDKey, credID)
	expectedCtx = context.WithValue(expectedCtx, types.EnvironmentKey, cred.OpenStackEnv)
	osMock := &portmocks.OpenStack{}
	osMock.On("ListFlavors", expectedCtx).Return(nil, errors.New("failed"))
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", actor, emulator, credID).Return(&cred, nil)
	h := FlavorListHandler{
		OpenStack:    osMock,
		CredSelector: CredentialSelector{credMock},
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.FlavorListReply{}, reply) {
		return
	}

	flavorListReply := reply.(providers.FlavorListReply)
	assert.Error(t, flavorListReply.Session.GetServiceError())
	assert.Nil(t, flavorListReply.Flavors)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorGetHandler_Handle(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID: credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		flavorID = "flavor-id-123"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation:  providers.FlavorsListOp,
			Provider:   common.NewID("provider"),
			Credential: credID,
			Args:       flavorID,
		}
		flavor = providers.Flavor{
			ID:        flavorID,
			Name:      "flavor-1-name",
			RAM:       123,
			Ephemeral: 234,
			VCPUs:     1,
			IsPublic:  false,
			Disk:      345,
		}
	)
	expectedCtx := context.WithValue(context.Background(), types.CredentialsIDKey, credID)
	expectedCtx = context.WithValue(expectedCtx, types.EnvironmentKey, cred.OpenStackEnv)
	osMock := &portmocks.OpenStack{}
	osMock.On("GetFlavor", expectedCtx, flavorID).Return(&flavor, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", actor, emulator, credID).Return(&cred, nil)
	h := FlavorGetHandler{
		OpenStack:    osMock,
		CredSelector: CredentialSelector{credMock},
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetFlavorReply{}, reply) {
		return
	}

	getFlavorReply := reply.(providers.GetFlavorReply)
	assert.NoError(t, getFlavorReply.Session.GetServiceError())
	assert.NotNil(t, getFlavorReply.Flavor)
	assert.Equal(t, flavor, *getFlavorReply.Flavor)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorGetHandler_Handle_BadArgs(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation:  providers.FlavorsListOp,
			Provider:   common.NewID("provider"),
			Credential: credID,
			Args:       nil,
		}
	)
	osMock := &portmocks.OpenStack{}
	credMock := &portmocks.CredentialMS{}
	h := FlavorGetHandler{
		OpenStack:    osMock,
		CredSelector: CredentialSelector{credMock},
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetFlavorReply{}, reply) {
		return
	}

	getFlavorReply := reply.(providers.GetFlavorReply)
	assert.Error(t, getFlavorReply.Session.GetServiceError())
	assert.Nil(t, getFlavorReply.Flavor)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorGetHandler_Handle_CredentialFetchFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		flavorID = "flavor-id-123"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation:  providers.FlavorsListOp,
			Provider:   common.NewID("provider"),
			Credential: credID,
			Args:       flavorID,
		}
	)
	osMock := &portmocks.OpenStack{}
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", actor, emulator, credID).Return(nil, errors.New("failed"))
	h := FlavorGetHandler{
		OpenStack:    osMock,
		CredSelector: CredentialSelector{credMock},
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetFlavorReply{}, reply) {
		return
	}

	getFlavorReply := reply.(providers.GetFlavorReply)
	assert.Error(t, getFlavorReply.Session.GetServiceError())
	assert.Nil(t, getFlavorReply.Flavor)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}

func TestFlavorGetHandler_Handle_OperationFailed(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID: credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		flavorID = "flavor-id-123"
		request  = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation:  providers.FlavorsListOp,
			Provider:   common.NewID("provider"),
			Credential: credID,
			Args:       flavorID,
		}
	)
	expectedCtx := context.WithValue(context.Background(), types.CredentialsIDKey, credID)
	expectedCtx = context.WithValue(expectedCtx, types.EnvironmentKey, cred.OpenStackEnv)
	osMock := &portmocks.OpenStack{}
	osMock.On("GetFlavor", expectedCtx, flavorID).Return(nil, errors.New("failed"))
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", actor, emulator, credID).Return(&cred, nil)
	h := FlavorGetHandler{
		OpenStack:    osMock,
		CredSelector: CredentialSelector{credMock},
	}

	reply := h.Handle(context.Background(), request)
	if !assert.IsType(t, providers.GetFlavorReply{}, reply) {
		return
	}

	getFlavorReply := reply.(providers.GetFlavorReply)
	assert.Error(t, getFlavorReply.Session.GetServiceError())
	assert.Nil(t, getFlavorReply.Flavor)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
}
