package domain

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	portmocks "gitlab.com/cyverse/cacao/provider-openstack-service/ports/mocks"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"testing"
	"time"
)

func TestCredentialSelector(t *testing.T) {
	t.Run("cred in request", testCredentialSelectorCredInRequest)
	t.Run("cred in request failed", testCredentialSelectorCredInRequestFailed)
	t.Run("cred matched tag", testCredentialSelectorMatchedTag)
	t.Run("cred matched tag multiple", testCredentialSelectorMultipleMatchedTag)
	t.Run("cred matched tag multiple with updated timestamp", testCredentialSelectorMultipleMatchedTagWithUpdatedTimestamp)
	t.Run("cred matched name", testCredentialSelectorMatchedName)

}

func testCredentialSelectorCredInRequest(t *testing.T) {
	t.Parallel()
	var credMock = &portmocks.CredentialMS{}
	var selector = CredentialSelector{credMock}
	var request = providers.ProviderRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "emulator123",
		},
		Operation:  "",
		Provider:   common.NewID("provider"),
		Credential: "my-cred-123",
		Args:       nil,
	}
	var cred = &types.Credential{
		ID:           request.Credential,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Time{},
		OpenStackEnv: map[string]string{},
	}
	credMock.On(
		"GetCredential",
		request.GetSessionActor(),
		request.GetSessionEmulator(),
		request.Credential,
	).Return(cred, nil)

	selectedCred, err := selector.SelectCredential(request)
	assert.NoError(t, err)
	assert.Equal(t, cred, selectedCred)
	credMock.AssertExpectations(t)
}

func testCredentialSelectorCredInRequestFailed(t *testing.T) {
	t.Parallel()
	var credMock = &portmocks.CredentialMS{}
	var selector = CredentialSelector{credMock}
	var request = providers.ProviderRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "emulator123",
		},
		Operation:  "",
		Provider:   common.NewID("provider"),
		Credential: "my-cred-123",
		Args:       nil,
	}
	expectedErr := errors.New("something failed")
	credMock.On(
		"GetCredential",
		request.GetSessionActor(),
		request.GetSessionEmulator(),
		request.Credential,
	).Return(nil, expectedErr)

	selectedCred, err := selector.SelectCredential(request)
	assert.Error(t, err)
	assert.Equal(t, expectedErr, err)
	assert.Nil(t, selectedCred)
	credMock.AssertExpectations(t)
}

func testCredentialSelectorMatchedTag(t *testing.T) {
	t.Parallel()
	var credMock = &portmocks.CredentialMS{}
	var selector = CredentialSelector{credMock}
	var request = providers.ProviderRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "emulator123",
		},
		Operation:  "",
		Provider:   common.NewID("provider"),
		Credential: "",
		Args:       nil,
	}
	var cred = types.Credential{
		ID:           request.Credential,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Time{},
		OpenStackEnv: map[string]string{},
	}
	var credList = []types.Credential{
		cred,
	}
	credMock.On(
		"ListCredentialsByTag",
		request.GetSessionActor(),
		request.GetSessionEmulator(),
		request.Provider.String(),
	).Return(credList, nil)

	selectedCred, err := selector.SelectCredential(request)
	assert.NoError(t, err)
	assert.Equal(t, &cred, selectedCred)
	credMock.AssertExpectations(t)
}

func testCredentialSelectorMultipleMatchedTag(t *testing.T) {
	t.Parallel()
	var credMock = &portmocks.CredentialMS{}
	var selector = CredentialSelector{credMock}
	var request = providers.ProviderRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "emulator123",
		},
		Operation:  "",
		Provider:   common.NewID("provider"),
		Credential: "",
		Args:       nil,
	}
	timeNow := time.Now()
	var credList = []types.Credential{
		{
			ID:           request.Credential,
			CreatedAt:    timeNow.Add(time.Second * 10),
			UpdatedAt:    time.Time{},
			OpenStackEnv: map[string]string{},
		},
		{
			ID:           request.Credential,
			CreatedAt:    timeNow.Add(time.Minute), // most recent
			UpdatedAt:    time.Time{},
			OpenStackEnv: map[string]string{},
		},
		{
			ID:           request.Credential,
			CreatedAt:    timeNow,
			UpdatedAt:    time.Time{},
			OpenStackEnv: map[string]string{},
		},
	}
	credMock.On(
		"ListCredentialsByTag",
		request.GetSessionActor(),
		request.GetSessionEmulator(),
		request.Provider.String(),
	).Return(credList, nil)

	selectedCred, err := selector.SelectCredential(request)
	assert.NoError(t, err)
	assert.Equal(t, &credList[1], selectedCred)
	credMock.AssertExpectations(t)
}

func testCredentialSelectorMultipleMatchedTagWithUpdatedTimestamp(t *testing.T) {
	t.Parallel()
	var credMock = &portmocks.CredentialMS{}
	var selector = CredentialSelector{credMock}
	var request = providers.ProviderRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "emulator123",
		},
		Operation:  "",
		Provider:   common.NewID("provider"),
		Credential: "",
		Args:       nil,
	}
	timeNow := time.Now()
	var credList = []types.Credential{
		{
			ID:           request.Credential,
			CreatedAt:    timeNow.Add(time.Second * 10),
			UpdatedAt:    timeNow.Add(time.Second * 30),
			OpenStackEnv: map[string]string{},
		},
		{
			ID:           request.Credential,
			CreatedAt:    timeNow.Add(time.Second * 10),
			UpdatedAt:    timeNow.Add(time.Minute + time.Second),
			OpenStackEnv: map[string]string{},
		},
		{
			ID:           request.Credential,
			CreatedAt:    timeNow.Add(time.Minute),
			UpdatedAt:    time.Time{},
			OpenStackEnv: map[string]string{},
		},
		{
			ID:           request.Credential,
			CreatedAt:    timeNow,
			UpdatedAt:    timeNow.Add(time.Minute * 5), // most recent
			OpenStackEnv: map[string]string{},
		},
		{
			ID:           request.Credential,
			CreatedAt:    timeNow,
			UpdatedAt:    time.Time{},
			OpenStackEnv: map[string]string{},
		},
		{
			ID:           request.Credential,
			CreatedAt:    timeNow,
			UpdatedAt:    timeNow.Add(time.Second * 20),
			OpenStackEnv: map[string]string{},
		},
	}
	credMock.On(
		"ListCredentialsByTag",
		request.GetSessionActor(),
		request.GetSessionEmulator(),
		request.Provider.String(),
	).Return(credList, nil)

	selectedCred, err := selector.SelectCredential(request)
	assert.NoError(t, err)
	assert.Equal(t, &credList[3], selectedCred)
	credMock.AssertExpectations(t)
}

func testCredentialSelectorMatchedName(t *testing.T) {
	t.Parallel()
	var credMock = &portmocks.CredentialMS{}
	var selector = CredentialSelector{credMock}
	var request = providers.ProviderRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "emulator123",
		},
		Operation:  "",
		Provider:   common.NewID("provider"),
		Credential: "",
		Args:       nil,
	}
	var cred = &types.Credential{
		ID:           request.Credential,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Time{},
		OpenStackEnv: map[string]string{},
	}
	credMock.On(
		"ListCredentialsByTag",
		request.GetSessionActor(),
		request.GetSessionEmulator(),
		request.Provider.String(),
	).Return(nil, errors.New("something wrong")) // match by tag failed
	credMock.On(
		"GetCredential",
		request.GetSessionActor(),
		request.GetSessionEmulator(),
		request.Provider.String(),
	).Return(cred, nil) // match by name

	selectedCred, err := selector.SelectCredential(request)
	assert.NoError(t, err)
	assert.Equal(t, cred, selectedCred)
	credMock.AssertExpectations(t)
}
