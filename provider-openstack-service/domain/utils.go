package domain

import (
	"context"

	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

func errorReply(request providers.ProviderRequest, err error) providers.BaseProviderReply {
	var svcErr service.CacaoErrorBase
	if svcErr1, ok := err.(service.CacaoError); ok {
		svcErr = svcErr1.GetBase()
	} else {
		svcErr = service.NewCacaoGeneralError(err.Error()).GetBase()
	}
	return providers.BaseProviderReply{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    svcErr,
		},
		Operation: request.Operation,
		Provider:  request.Provider,
	}
}

func requestToBaseReply(request providers.ProviderRequest) providers.BaseProviderReply {
	return providers.BaseProviderReply{
		Session:   request.Session,
		Operation: request.Operation,
		Provider:  request.Provider,
	}
}

func withCredential(parent context.Context, credentials *types.Credential) context.Context {
	var ctx = parent
	ctx = context.WithValue(ctx, types.CredentialsIDKey, credentials.ID)
	ctx = context.WithValue(ctx, types.EnvironmentKey, credentials.OpenStackEnv)
	return ctx
}
