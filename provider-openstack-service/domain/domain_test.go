package domain

import (
	"context"
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	portmocks "gitlab.com/cyverse/cacao/provider-openstack-service/ports/mocks"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"testing"
)

type MockReplyer struct {
	ce *cloudevents.Event
}

func (m *MockReplyer) Reply(ce cloudevents.Event) error {
	m.ce = &ce
	return nil
}

func TestOpenStackDomain_processQuery_imageList(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID: credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation:  providers.ImagesListOp,
			Provider:   common.NewID("provider"),
			Credential: "cred-123",
			Args:       nil,
		}
		subject     = providers.OpenStackQueryPrefix + providers.ImagesListOp
		originalCtx = context.Background()
		ce, _       = messaging.CreateCloudEventWithTransactionID(request, string(subject), "mock", messaging.NewTransactionID())
		imageList   = []providers.Image{
			{
				ID:              "image-1",
				Name:            "image-1-name",
				DiskFormat:      "",
				ContainerFormat: "",
				Size:            123,
				Checksum:        "",
				Status:          "",
				Visibility:      "",
				Protected:       false,
				Project:         "",
				Tags:            nil,
			},
		}
	)
	mockReplyer := &MockReplyer{}
	ceRequest := types.CloudEventRequest{
		CloudEvent: ce,
		Replyer:    mockReplyer,
	}
	expectedCtx := context.WithValue(originalCtx, types.UsernameKey, actor)
	expectedCtx = context.WithValue(expectedCtx, types.TransactionIDKey, messaging.GetTransactionID(&ce))
	expectedCtx = context.WithValue(expectedCtx, types.CredentialsIDKey, credID)
	expectedCtx = context.WithValue(expectedCtx, types.EnvironmentKey, cred.OpenStackEnv)
	osMock := &portmocks.OpenStack{}
	osMock.On("ListImages", expectedCtx).Return(imageList, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", actor, emulator, credID).Return(&cred, nil)
	domain := &OpenStackDomain{
		OpenStack: osMock,
		CredMS:    credMock,
	}

	domain.processQuery(originalCtx, ceRequest)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
	if !assert.NotNil(t, mockReplyer.ce) {
		return
	}
	var reply providers.ImageListReply
	assert.NoError(t, json.Unmarshal(mockReplyer.ce.Data(), &reply))
	assert.NotNil(t, reply.Images)
	assert.Equal(t, imageList, reply.Images)
	assert.NoError(t, reply.Session.GetServiceError())
}

func TestOpenStackDomain_processQuery_flavorList(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID: credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation:  providers.FlavorsListOp,
			Provider:   common.NewID("provider"),
			Credential: "cred-123",
			Args:       nil,
		}
		subject     = providers.OpenStackQueryPrefix + providers.FlavorsListOp
		originalCtx = context.Background()
		ce, _       = messaging.CreateCloudEventWithTransactionID(request, string(subject), "mock", messaging.NewTransactionID())
		flavorList  = []providers.Flavor{
			{
				ID:        "flavor-1",
				Name:      "flavor-1-name",
				RAM:       123,
				Ephemeral: 234,
				VCPUs:     1,
				IsPublic:  false,
				Disk:      345,
			},
		}
	)
	mockReplyer := &MockReplyer{}
	ceRequest := types.CloudEventRequest{
		CloudEvent: ce,
		Replyer:    mockReplyer,
	}
	expectedCtx := context.WithValue(originalCtx, types.UsernameKey, actor)
	expectedCtx = context.WithValue(expectedCtx, types.TransactionIDKey, messaging.GetTransactionID(&ce))
	expectedCtx = context.WithValue(expectedCtx, types.CredentialsIDKey, credID)
	expectedCtx = context.WithValue(expectedCtx, types.EnvironmentKey, cred.OpenStackEnv)
	osMock := &portmocks.OpenStack{}
	osMock.On("ListFlavors", expectedCtx).Return(flavorList, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", actor, emulator, credID).Return(&cred, nil)
	domain := &OpenStackDomain{
		OpenStack: osMock,
		CredMS:    credMock,
	}

	domain.processQuery(originalCtx, ceRequest)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
	if !assert.NotNil(t, mockReplyer.ce) {
		return
	}
	var reply providers.FlavorListReply
	assert.NoError(t, json.Unmarshal(mockReplyer.ce.Data(), &reply))
	assert.NotNil(t, reply.Flavors)
	assert.Equal(t, flavorList, reply.Flavors)
	assert.NoError(t, reply.Session.GetServiceError())
}
