package types

import (
	"context"
	"errors"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"

	"github.com/cloudevents/sdk-go/v2/event"
	"gitlab.com/cyverse/cacao-common/messaging"
)

// CachePrefixListApplicationCredentials is the prefix on the cache key for the results of a
// user requesting a list of ApplicationCredentials.
const CachePrefixListApplicationCredentials = "ListApplicationCredentials"

// CachePrefixGetApplicationCredential is the prefix on the cache key for the results of a
// user requesting a single ApplicationCredential.
const CachePrefixGetApplicationCredential = "GetApplicationCredential"

// CachePrefixListFlavors is the prefix on the cache key for the results of a
// user requesting a list of flavors.
const CachePrefixListFlavors = "ListFlavors"

// CachePrefixGetFlavor is the prefix on the cache key for the results of a
// user requesting a single flavor.
const CachePrefixGetFlavor = "GetFlavor"

// CachePrefixListImages is the prefix on the cache key for the results of a
// user requesting a list of images.
const CachePrefixListImages = "ListImages"

// CachePrefixGetImage is the rpefix on the cache key for the results of a
// user requesting a single image.
const CachePrefixGetImage = "GetImage"

// CachePrefixListProjects is the prefix on the cache key for the results of a
// user requesting a list of Projects.
const CachePrefixListProjects = "ListProjects"

// CachePrefixGetProject is the rpefix on the cache key for the results of a
// user requesting a single Project.
const CachePrefixGetProject = "GetProject"

// Configuration contains the settings needed to configure this microservice.
type Configuration struct {
	NATS                     messaging.NatsConfig
	STAN                     messaging.StanConfig
	DefaultChannelBufferSize int    `envconfig:"DEFAULT_CHANNEL_BUFFER_SIZE" default:"1"`
	QueryWorkerCount         int    `envconfig:"QUERY_WORKER_COUNT" default:"10"`
	LogLevel                 string `envconfig:"LOG_LEVEL" default:"trace"`
	RedisAddress             string `envconfig:"REDIS_ADDRESS" default:"redis:6379"`
	RedisPassword            string `envconfig:"REDIS_PASSWORD" default:""`
	RedisDB                  string `envConfig:"REDIS_DB" default:"0"`
	// expiration period for cache entry in seconds, default to 1 hour
	CacheTTL int `envConfig:"CACHE_TTL" default:"3600"`
}

// Validate returns an error if there's something wrong with the configuration.
func (c *Configuration) Validate() error {
	if c.NATS.ClientID == "" {
		return errors.New("NATS_CLIENT_ID environment variable must be set")
	}

	if c.NATS.QueueGroup == "" {
		return errors.New("NATS_QGROUP environment variable must be set")
	}

	if c.QueryWorkerCount <= 0 {
		return errors.New("QUERY_WORKER_COUNT cannot be zero or negative")
	}

	// if c.NATS.NatsDurableName == "" {
	// 	return errors.New("NATS_DURABLE_NAME environment variable must be set")
	// }

	return nil
}

const (
	// UsernameKey is what you should use as a key for the username in context.Context.
	UsernameKey = iota

	// CredentialsIDKey is what you should use as a key for the credentials ID in context.Context.
	CredentialsIDKey

	// TransactionIDKey is the key for the transaction ID in context.Context.
	TransactionIDKey

	// EnvironmentKey is the key for the environment variables stored in context.Context.
	EnvironmentKey
)

// QueryReply defines a type that can send a response of some sort.
type QueryReply interface {
	Reply(cloudevents.Event) error
}

// CloudEventRequest is what gets comes in as a request
type CloudEventRequest struct {
	CloudEvent event.Event
	Replyer    QueryReply
}

// OpenStackGetter represents a function that calls `openstack show` via
// os/exec.CommandContext and returns the output.
type OpenStackGetter func(context.Context, Environment, string) ([]byte, error)

// Credential ...
type Credential struct {
	ID           string
	CreatedAt    time.Time
	UpdatedAt    time.Time
	OpenStackEnv Environment
}

// Environment is environment variables represented in a key-value pairs fashion.
// This is used to carry OpenStack credential info and pass to OpenStack CLI.
type Environment map[string]string
