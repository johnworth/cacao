# provider-openstack-service

A microservice that contacts registered providers and provides access to
resources contained within them.

Currently contains the following operations on OpenStack-related resources:

- images.get - Get the details for a single image, denoted by its UUID within
  the OpenStack cluster.
- images.list - List available images in the OpenStack cluster.
- flavors.get - Get the details for a single flavor, denoted by its UUID within
  the OpenStack cluster.
- flavors.list - List the available flavors in the OpenStack cluster.

## Building

To build this microservice, you'll need to have a reasonably up-to-date version
of Go installed. Then run the following in the top-level directory containing
the service code:

```
go build
```

The service should build with that single step. If that is not the case, please
file an issue.

This service uses Go modules.

## Configuration

The service is configured through a set of environment variables. Here is the
important subset of the variables:

- `NATS_URL` - The connection string to the NATS cluster. Defaults to
  `nats://nats:4222`.
- `NATS_CLIENT_ID` - Required. Has no default.
- `NATS_QUEUE_GROUP` - Required. Has no default.
- `NATS_WILDCARD_SUBJECT` - Defaults to `cyverse.>`. Should be set to
  `cacao.openstack.>`.
- `REDIS_ADDRESS` - Defaults to `redis:6379`.
- `REDIS_PASSWORD` - Defaults to an empty string.
- `REDIS_DB` - Defaults to `0` and should be parseable as an integer.

## Credentials Access

This service looks up a user's OpenStack access info from the Credentials
service. The access info should be in a JSON format that looks like the
following:

```
{
	"OS_AUTH_URL":             "https://example.com:5000/v3",
	"OS_PROJECT_ID":           "replaceme",
	"OS_PROJECT_NAME":         "replaceme",
	"OS_PROJECT_DOMAIN_ID":    "replaceme",
	"OS_USER_DOMAIN_NAME":     "replaceme",
	"OS_USERNAME":             "replaceme",
	"OS_PASSWORD":             "replaceme",
	"OS_REGION_NAME":          "replaceme",
	"OS_INTERFACE":            "public",
	"OS_IDENTITY_API_VERSION": "3"
}
```

The name of the credential maps to the providerID passed in with the request.
It's not hardcoded in anywhere. For example, if the incoming request says that
the providerID is `openstack` then `openstack` is used as the name of the
credential that gets retrieved. However, if the providerID is set to a UUID,
then the UUID will be used to look up the credential.

The providerID corresponds to the value passed in to the
`/providers/{providerid}/*` endpoints in the api-service.

## Examples

It's recommended to use the nats-cli provided at
https://github.com/nats-io/natscli. The commands below section assume that the
nats-cli commands are installed.

During development, I normally have a nats-server running locally in debug mode.
I fire it up like this:

```
nats-server --debug
```

In another terminal, I fire up a subscription to the reply subject that I will
set on the messages I send to the service:

```
nats sub cacao.replies
```

In yet another terminal, I start up a build of the service:

```
NATS_URL=nats://127.0.0.1:4222 NATS_CLIENT_ID=test NATS_QGROUP=test NATS_DURABLE_NAME=test ./provider-openstack-service
```

Finally, I have a short Go script that I use to send messages to the service
over NATS:

```
NATS_URL=nats://localhost:4222 NATS_SUBJECT=cacao.providers.openstack.images.list NATS_REPLY=cacao.replies go run scripts/sendmsg.go
```
