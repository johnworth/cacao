package main

import (
	"time"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/provider-openstack-service/adapters"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

var config types.Configuration

func init() {
	var err error

	if err = envconfig.Process("", &config); err != nil {
		log.Fatal(err.Error())
	}
	logLevel, err := log.ParseLevel(config.LogLevel)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(logLevel)

	if err = config.Validate(); err != nil {
		log.Fatal(err)
	}
}

func main() {
	osAdapter := adapters.NewOpenStackAdapter(time.Second*time.Duration(config.CacheTTL), adapters.NewRedisCacheManager(config))

	osQueryAdapter, err := adapters.NewQueryAdapter(config)
	if err != nil {
		log.Fatal(err)
	}

	credMS := adapters.NewCredentialMicroservice(config.NATS, config.STAN)

	imageLister := domain.NewOpenStackDomain(&config, osQueryAdapter, osAdapter, credMS)
	imageLister.Start()
}
