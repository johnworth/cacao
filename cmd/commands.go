package main

import (
	"github.com/mitchellh/cli"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/cmd/command"
)

// Commands holds a map of all possible commands
func Commands(meta *command.Meta) map[string]cli.CommandFactory {
	log.Trace("commands.go/Commands: start")
	baseCommand := command.NewBaseCommand(*meta)
	log.Trace("commands.go/Commands: received baseCommand")
	return map[string]cli.CommandFactory{
		"create": func() (cli.Command, error) {
			return &command.CreateCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"create provider": func() (cli.Command, error) {
			return &command.CreateProviderCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"create user": func() (cli.Command, error) {
			return &command.CreateUserCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		// "create wfd": func() (cli.Command, error) {
		// 	return &command.CreateWfdCommand{
		// 		BaseCommand: &baseCommand,
		// 	}, nil
		// },
		"create credential": func() (cli.Command, error) {
			return &command.CreateCredentialCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		// "create cluster": func() (cli.Command, error) {
		// 	return &command.CreateClusterCommand{
		// 		BaseCommand: &baseCommand,
		// 	}, nil
		// },
		// "build": func() (cli.Command, error) {
		// 	return &command.BuildCommand{
		// 		BaseCommand: &baseCommand,
		// 	}, nil
		// },
		// "run": func() (cli.Command, error) {
		// 	return &command.RunCommand{
		// 		BaseCommand: &baseCommand,
		// 	}, nil
		// },
		"create workspace": func() (cli.Command, error) {
			return &command.CreateWorkspaceCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"create template": func() (cli.Command, error) {
			return &command.CreateTemplateCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"create template_type": func() (cli.Command, error) {
			return &command.CreateTemplateTypeCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"create deployment": func() (cli.Command, error) {
			return &command.CreateDeploymentCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"create run": func() (cli.Command, error) {
			return &command.CreateRunCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"delete": func() (cli.Command, error) {
			return &command.DeleteCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"delete provider": func() (cli.Command, error) {
			return &command.DeleteProviderCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"delete user": func() (cli.Command, error) {
			return &command.DeleteUserCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		// "delete wfd": func() (cli.Command, error) {
		// 	return &command.DeleteWfdCommand{
		// 		BaseCommand: &baseCommand,
		// 	}, nil
		// },
		"delete credential": func() (cli.Command, error) {
			return &command.DeleteCredentialCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		// "delete cluster": func() (cli.Command, error) {
		// 	return &command.DeleteClusterCommand{
		// 		BaseCommand: &baseCommand,
		// 	}, nil
		// },
		"delete deployment": func() (cli.Command, error) {
			return &command.DeleteDeploymentCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"delete workspace": func() (cli.Command, error) {
			return &command.DeleteWorkspaceCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"delete template": func() (cli.Command, error) {
			return &command.DeleteTemplateCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"delete template_type": func() (cli.Command, error) {
			return &command.DeleteTemplateTypeCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"get": func() (cli.Command, error) {
			return &command.GetCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		// "get cluster": func() (cli.Command, error) {
		// 	return &command.GetClusterCommand{
		// 		BaseCommand: &baseCommand,
		// 	}, nil
		// },
		"get credential": func() (cli.Command, error) {
			return &command.GetCredentialCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"get provider": func() (cli.Command, error) {
			return &command.GetProviderCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"get user": func() (cli.Command, error) {
			return &command.GetUserCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		// "get wfd": func() (cli.Command, error) {
		// 	return &command.GetWfdCommand{
		// 		BaseCommand: &baseCommand,
		// 	}, nil
		// },
		"get run": func() (cli.Command, error) {
			return &command.GetRunCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		// "get reports": func() (cli.Command, error) {
		// 	return &command.GetReportsCommand{
		// 		BaseCommand: &baseCommand,
		// 	}, nil
		// },
		"get workspace": func() (cli.Command, error) {
			return &command.GetWorkspaceCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"get template": func() (cli.Command, error) {
			return &command.GetTemplateCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"get template_type": func() (cli.Command, error) {
			return &command.GetTemplateTypeCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"get template_source_type": func() (cli.Command, error) {
			return &command.GetTemplateSourceTypeCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"get deployment": func() (cli.Command, error) {
			return &command.GetDeploymentCommand{
				BaseCommand: &baseCommand,
				Meta:        *meta,
			}, nil
		},
		"get build": func() (cli.Command, error) {
			return &command.GetBuildCommand{
				BaseCommand: &baseCommand,
				Meta:        *meta,
			}, nil
		},
		"get version": func() (cli.Command, error) {
			return &command.GetVersionCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"login": func() (cli.Command, error) {
			return &command.LoginCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"logout": func() (cli.Command, error) {
			return &command.LogoutCommand{
				BaseCommand: &baseCommand,
			}, nil
		},

		"version": func() (cli.Command, error) {
			return &command.VersionCommand{
				Version:  Version,
				Revision: GitCommit,
				Name:     Name,
			}, nil
		},
		"update": func() (cli.Command, error) {
			return &command.UpdateCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		// "update cluster": func() (cli.Command, error) {
		// 	return &command.UpdateClusterCommand{
		// 		BaseCommand: &baseCommand,
		// 	}, nil
		// },
		"update credential": func() (cli.Command, error) {
			return &command.UpdateCredentialCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"update provider": func() (cli.Command, error) {
			return &command.UpdateProviderCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"update user": func() (cli.Command, error) {
			return &command.UpdateUserCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		// "update wfd": func() (cli.Command, error) {
		// 	return &command.UpdateWfdCommand{
		// 		BaseCommand: &baseCommand,
		// 	}, nil
		// },
		"update workspace": func() (cli.Command, error) {
			return &command.UpdateWorkspaceCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"update deployment": func() (cli.Command, error) {
			return &command.UpdateDeploymentCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"update template": func() (cli.Command, error) {
			return &command.UpdateTemplateCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"update template_type": func() (cli.Command, error) {
			return &command.UpdateTemplateTypeCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"sync": func() (cli.Command, error) {
			return &command.SyncCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"sync template": func() (cli.Command, error) {
			return &command.SyncTemplateCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
	}
}
