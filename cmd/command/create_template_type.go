package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"io/ioutil"
	"strings"
)

// CreateTemplateTypeCommand ...
type CreateTemplateTypeCommand struct {
	*BaseCommand
}

// Run ...
func (c *CreateTemplateTypeCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	name := flagSet.String("name", "", "name of template type, must be unique")
	formats := flagSet.String("formats", "", "a comma-separated list of template formats")
	engine := flagSet.String("engine", "", "name of template engine")
	providers := flagSet.String("providers", "", "a comma-separated list of provider types")
	flagSet.Parse(args)

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  name: '%s'\n  formats: '%s'\n  engine: '%s'\n  providers: '%s'\n", *filename, *name, *formats, *engine, *providers)

	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))

		if len(*name) > 0 || len(*formats) > 0 || len(*engine) > 0 || len(*providers) > 0 {
			data = c.override(data, name, formats, engine, providers)
			c.PrintDebug("Successfully overrode parameters in file:\n%s\n", string(data))
		}
	} else {
		if len(*name) == 0 {
			c.UI.Error(c.Help())
			return 1
		}
		if len(*formats) == 0 {
			c.UI.Error(c.Help())
			return 1
		}
		if len(*engine) == 0 {
			c.UI.Error(c.Help())
			return 1
		}
		if len(*providers) == 0 {
			c.UI.Error(c.Help())
			return 1
		}

		data = c.override(data, name, formats, engine, providers)
	}

	c.PrintDebug("Constructed request body:\n%s\n", string(data))

	req := c.NewRequest("POST", "/templates/types", "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *CreateTemplateTypeCommand) Synopsis() string {
	return "create a template type"
}

// Help ...
func (c *CreateTemplateTypeCommand) Help() string {
	helpText := `
	Usage: cacao create template_type [options]

	Of the following options, it is required that you use
	either '-f <filename>' or utilize other options

	Options:
		-f <filename>
			name of json file to read user info from. Cannot be used with
			other options
		-name <string>
			name of the template type. Must be unique
			(required)
		-formats <string>
			a comma-separated list of template formats the template type can support
		-engine <string>
			template engine the type supports
		-providers <string>
			a comm-separated list of providers the template type supports
`
	return strings.TrimSpace(helpText)
}

func (c *CreateTemplateTypeCommand) override(data []byte, name *string, formats *string, engine *string, providers *string) []byte {
	var templateTypeJSON struct {
		Name          string   `json:"name,omitempty"`
		Formats       []string `json:"formats,omitempty"`
		Engine        string   `json:"engine,omitempty"`
		ProviderTypes []string `json:"provider_types,omitempty"`
	}

	json.Unmarshal(data, &templateTypeJSON)

	if len(*name) > 0 {
		templateTypeJSON.Name = *name
	}

	if len(*formats) > 0 {
		formatStrings := strings.Split(*formats, ",")
		formatStringsTrimmed := make([]string, 0, len(formatStrings))
		for _, s := range formatStrings {
			formatStringsTrimmed = append(formatStringsTrimmed, strings.TrimSpace(s))
		}

		templateTypeJSON.Formats = formatStringsTrimmed
	}

	if len(*engine) > 0 {
		templateTypeJSON.Engine = *engine
	}

	if len(*providers) > 0 {
		providersStrings := strings.Split(*providers, ",")
		providersStringsTrimmed := make([]string, 0, len(providersStrings))
		for _, s := range providersStrings {
			providersStringsTrimmed = append(providersStringsTrimmed, strings.TrimSpace(s))
		}

		templateTypeJSON.ProviderTypes = providersStringsTrimmed
	}

	data, err := json.Marshal(templateTypeJSON)
	if err != nil {
		panic(err)
	}
	return data
}
