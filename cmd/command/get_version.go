package command

import (
	"strings"
)

// GetVersionCommand ...
type GetVersionCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetVersionCommand) Run(args []string) int {

	if len(args) == 0 {
		req := c.NewRequest("GET", "/version/", "")
		c.DoRequest(req)
		return 0
	}

	if args[0] == "help" {
		c.PrintDebug(c.Help())
		c.UI.Error(c.Help())
		return 1
	}

	return 0
}

// Synopsis ...
func (c *GetVersionCommand) Synopsis() string {
	return "Gets the version information for the current running of Cacao"
}

// Help ...
func (c *GetVersionCommand) Help() string {
	helpText := `
Usage: cacao get version

Gets the version information for the current running of Cacao
`
	return strings.TrimSpace(helpText)
}
