package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"io/ioutil"
	"strings"
)

// UpdateCredentialCommand ...
type UpdateCredentialCommand struct {
	*BaseCommand
}

// Run ...
func (c *UpdateCredentialCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	//variable should be oid, will be used in future
	_ = flagSet.String("oname", "", "old id to update")
	nid := flagSet.String("nname", "", "new id to use")
	credentialType := flagSet.String("type", "", "type of credential (gcr, dockerhub, or git)")
	//username := flagSet.String("username", "", "username to use with credential")
	value := flagSet.String("value", "", "value of the credential")
	valueFile := flagSet.String("value-file", "", "file that contains the value of the credential")
	if err := flagSet.Parse(args); err != nil {
		c.UI.Error(err.Error())
		c.UI.Info(c.Help())
		return 1
	}
	args = flagSet.Args()

	c.PrintDebug("Parsed command line flags:\n  nname: '%s'\n  credentialType: '%s'\n  value: '%s'\n valueFile: '%s'\\n", *nid, *credentialType, *value, *valueFile)

	if len(args) > 0 && args[0] == "help" {
		c.PrintDebug(c.Help())
		return 1
	}

	if len(*valueFile) > 0 {
		fileContent, err := ioutil.ReadFile(*valueFile)
		if err != nil {
			c.UI.Error(err.Error())
			return 1
		}
		*value = string(fileContent)
	}

	var credential = struct {
		Type  string
		ID    string
		Value string
	}{
		Type:  *credentialType,
		ID:    *nid,
		Value: *value,
	}
	data, err := json.Marshal(credential)
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	c.PrintDebug("Constructed request body:\n%s\n", string(data))

	req := c.NewRequest("POST", "/credentials", "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *UpdateCredentialCommand) Synopsis() string {
	return "update a credential for current user"
}

// Help ...
func (c *UpdateCredentialCommand) Help() string {
	helpText := `
Usage: cacao update credential -oname a -nname e -type b -value c

Options:
	-oname <string>
		old name of the credential to be updated
	-nname <string>
  		new name of credential
	-type <string>
		type of the credential (git, dockerhub, or gcr)
	-value <string>
		value of the credential (often a personal access token)
	-value-file <string>
		file that contains the value of the credential
`
	return strings.TrimSpace(helpText)
}
