package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"gitlab.com/cyverse/cacao-common/common"
	"io/ioutil"
	"strings"

	"gitlab.com/cyverse/cacao-common/service"
)

// UpdateProviderCommand ...
type UpdateProviderCommand struct {
	*BaseCommand
}

// Run ...
func (c *UpdateProviderCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	name := flagSet.String("name", "", "name")
	url := flagSet.String("url", "", "url")
	providerType := flagSet.String("type", "", "type")
	metadata := flagSet.String("metadata", "", "metadata")
	metadataFilename := flagSet.String("metadata-file", "", "metadata JSON file")
	if err := flagSet.Parse(args); err != nil {
		c.UI.Error(err.Error())
		c.UI.Error(c.Help())
		return 1
	}
	args = flagSet.Args()

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  name: '%s'\n  url: '%s'\n  type: '%s'\n  metadata: '%s'\n metadata-file: '%s'\n", *filename, *name, *url, *providerType, *metadata, *metadataFilename)

	if len(args) == 1 && args[0] == "help" {
		c.UI.Error(c.Help())
		return 0
	}

	var data []byte
	var err error
	provider := service.ProviderModel{}

	if len(*filename) > 0 {
		provider, err = readProviderFile(*filename)
		if err != nil {
			c.UI.Error(err.Error())
			return 1
		}
		c.PrintDebug("Successfully read file '%s':\n%s", filename, string(data))
	}

	err = c.override(&provider, &args[0], name, url, providerType, metadata, metadataFilename)
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}
	if provider.ID == "" {
		c.UI.Error("provider ID cannot be empty")
		c.UI.Error(c.Help())
		return 1
	}

	data, err = json.Marshal(&provider)
	if err != nil {
		panic(err)
	}

	c.PrintDebug("Constructed request body:\n%s\n", string(data))

	req := c.NewRequest("PATCH", "/providers/"+string(provider.ID), "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *UpdateProviderCommand) Synopsis() string {
	return "update a provider"
}

// Help ...
func (c *UpdateProviderCommand) Help() string {
	helpText := `
	Usage: cacao update provider [options] ID
	ID is optional if -f is used

	Options:
		-f <filename>
			name of JSON file to read secret info from
		-name
			update provider with new name
		-url
			update provider with new url
		-type
			update provider with new type
		-metadata <string>
			should be formated as a JSON object string, i.e. {key1:<value1>, key2:<value2>, ...}
		-metadata-file <string>
			optional, filename of a JSON file contains metadata for the provider.
`
	return strings.TrimSpace(helpText)
}

func (c *UpdateProviderCommand) override(
	provider *service.ProviderModel,
	id *string,
	name *string,
	url *string,
	providerType *string,
	metadataStr *string,
	metadataFilename *string,
) error {
	if len(*id) > 0 {
		provider.ID = common.ID(*id)
	}
	if len(*name) > 0 {
		provider.Name = *name
	}
	if len(*url) > 0 {
		provider.URL = *url
	}
	if len(*providerType) > 0 {
		provider.Type = *providerType
	}
	if len(*metadataStr) > 0 {
		var metadata map[string]interface{}
		if err := json.Unmarshal([]byte(*metadataStr), &metadata); err != nil {
			return err
		}
		provider.Metadata = metadata
	}
	if metadataFilename != nil && len(*metadataFilename) > 0 {
		metadata, err := readProviderMetadataFromFile(*metadataFilename)
		if err != nil {
			return err
		}
		c.PrintDebug("read metadata from file:\n%v\n", metadata)
		provider.Metadata = metadata
	}
	return nil
}
