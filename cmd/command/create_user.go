package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"io/ioutil"
	"strings"

	"gitlab.com/cyverse/cacao-common/common"
)

// CreateUserCommand ...
type CreateUserCommand struct {
	*BaseCommand
}

// Run ...
func (c *CreateUserCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	username := flagSet.String("username", "", "username")
	firstname := flagSet.String("firstname", "", "firstname")
	lastname := flagSet.String("lastname", "", "lastname")
	email := flagSet.String("email", "", "email")
	admin := flagSet.Bool("admin", false, "admin")
	flagSet.Parse(args)

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  username: '%s'\n  first_name: '%s'\n  last_name: '%s'\n  email: '%s'\n  admin: %t\n",
		*filename, *username, *firstname, *lastname, *email, *admin)

	var data []byte
	var err error
	huser := common.HTTPUser{}

	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		json.Unmarshal(data, &huser)
		if err != nil {
			panic(err)
		}
		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))
	}

	c.override(&huser, username, firstname, lastname, email, admin)

	if len(huser.Username) == 0 {
		c.UI.Error(c.Help())
		return 1
	}

	data, err = json.Marshal(&huser)
	if err != nil {
		panic(err)
	}

	c.PrintDebug("Constructed request body:\n%s\n", string(data))

	req := c.NewRequest("POST", "/users", "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *CreateUserCommand) Synopsis() string {
	return "create a new user"
}

// Help ...
func (c *CreateUserCommand) Help() string {
	helpText := `
	Usage: cacao create user [options]

	Of the following options, it is required that you use either '-f <filename>' 
	or utilize at least '-username' and '-password'

	Options:
		-f <filename>
	  		name of json file to read user info from
		-username <string>
			(required if -f is not used)
		-password <string>
			(required if -f is not used)
		-admin
			give new user admin permissions
		-create-kubeconfig
			automatically generate a new kubeconfig to access the user cluster (default true)
	`
	return strings.TrimSpace(helpText)
}

func (c *CreateUserCommand) override(huser *common.HTTPUser, username *string, firstname *string, lastname *string, email *string, admin *bool) {

	if len(*username) > 0 {
		huser.Username = *username
	}
	if len(*firstname) > 0 {
		huser.FirstName = *firstname
	}
	if len(*username) > 0 {
		huser.Username = *username
	}
	if len(*lastname) > 0 {
		huser.LastName = *lastname
	}
	if len(*email) > 0 {
		huser.PrimaryEmail = *email
	}

	if *admin {
		huser.IsAdmin = true
	}
}
