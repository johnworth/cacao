package command

import (
	"strings"
)

// DeleteProviderCommand ...
type DeleteProviderCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteProviderCommand) Run(args []string) int {
	if len(args) != 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("DELETE", "/providers/"+args[0], "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *DeleteProviderCommand) Synopsis() string {
	return "delete a provider"
}

// Help ...
func (c *DeleteProviderCommand) Help() string {
	helpText := `
Usage: cacao delete provider [ID]

Run 'cacao get provider' to view list of providers.
`
	return strings.TrimSpace(helpText)
}
