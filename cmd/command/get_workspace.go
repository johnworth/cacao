package command

import (
	"strings"
)

// GetWorkspaceCommand ...
type GetWorkspaceCommand struct {
	Meta
	*BaseCommand
}

// Run ...
func (c *GetWorkspaceCommand) Run(args []string) int {
	var workspaceID string
	if len(args) > 0 {
		workspaceID = "/" + args[0]
	}

	req := c.NewRequest("GET", "/workspaces"+workspaceID, "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetWorkspaceCommand) Synopsis() string {
	return "get workspaces"
}

// Help ...
func (c *GetWorkspaceCommand) Help() string {
	helpText := `
Usage: cacao get workspace [ID]

	Gets all workspaces unless specific ID is provided
`
	return strings.TrimSpace(helpText)
}
