package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"io/ioutil"
	"strings"

	"gitlab.com/cyverse/cacao-common/service"
)

// CreateProviderCommand ...
type CreateProviderCommand struct {
	*BaseCommand
}

// Run ...
func (c *CreateProviderCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	name := flagSet.String("name", "", "name")
	url := flagSet.String("url", "", "url")
	providerType := flagSet.String("type", "", "type")
	metadata := flagSet.String("metadata", "", "metadata")
	metadataFilename := flagSet.String("metadata-file", "", "metadata JSON file")
	if err := flagSet.Parse(args); err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  name: '%s'\n  url: '%s'\n  type: '%s'\n  metadata: '%s'\n metadata-file: '%s'\n", *filename, *name, *url, *providerType, *metadata, *metadataFilename)

	var data []byte
	var err error
	provider := service.ProviderModel{}

	if len(*filename) > 0 {
		provider, err = readProviderFile(*filename)
		if err != nil {
			c.UI.Error(err.Error())
			return 1
		}
		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))
	}

	err = c.override(&provider, name, url, providerType, metadata, metadataFilename)
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	if len(provider.Name) == 0 || len(provider.URL) == 0 || len(provider.Type) == 0 {
		c.UI.Error(c.Help())
		return 1
	}

	data, err = json.Marshal(&provider)
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	c.PrintDebug("Constructed request body:\n%s\n", string(data))

	req := c.NewRequest("POST", "/providers", "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *CreateProviderCommand) Synopsis() string {
	return "create a new provider"
}

// Help ...
func (c *CreateProviderCommand) Help() string {
	helpText := `
	Usage: cacao create provider [options]

	Of the following options, it is required that you use either '-f <filename>' 
	or all of '-name', '-url', and '-type'

	Options:
		-f <filename>
	  		name of json file to read provider info from
		-name <string>
			(required if -f is not used)
		-url <string>
			(required if -f is not used)
		-type <string>
			(required if -f is not used)
		-metadata <string>
			optional, should be formated as a JSON object string, i.e. {key1:<value1>, key2:<value2>, ...}
		-metadata-file <string>
			optional, filename of a JSON file contains metadata for the provider.
	`
	return strings.TrimSpace(helpText)
}

func (c *CreateProviderCommand) override(
	provider *service.ProviderModel,
	name *string,
	url *string,
	providerType *string,
	metadataStr *string,
	metadataFilename *string,
) error {
	if len(*name) > 0 {
		provider.Name = *name
	}
	if len(*url) != 0 {
		provider.URL = *url
	}
	if len(*providerType) > 0 {
		provider.Type = *providerType
	}
	if len(*metadataStr) > 0 {
		var metadata map[string]interface{}
		if err := json.Unmarshal([]byte(*metadataStr), &metadata); err != nil {
			return err
		}
		provider.Metadata = metadata
	}
	if len(*metadataFilename) > 0 {
		metadata, err := readProviderMetadataFromFile(*metadataFilename)
		if err != nil {
			return err
		}
		c.PrintDebug("read metadata from file:\n%v\n", metadata)
		provider.Metadata = metadata
	}
	return nil
}

func readProviderFile(filename string) (service.ProviderModel, error) {
	var provider service.ProviderModel
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return service.ProviderModel{}, err
	}
	if err = json.Unmarshal(data, &provider); err != nil {
		return service.ProviderModel{}, err
	}
	return service.ProviderModel{}, err
}

func readProviderMetadataFromFile(filename string) (map[string]interface{}, error) {
	fileContent, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	var metadata map[string]interface{}
	if err = json.Unmarshal(fileContent, &metadata); err != nil {
		return nil, err
	}
	return metadata, nil
}
