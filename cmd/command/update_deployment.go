package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	hm "gitlab.com/cyverse/cacao-common/http"
	"strings"
)

// UpdateDeploymentCommand ...
type UpdateDeploymentCommand struct {
	*BaseCommand
}

var deploymentUpdateFlagVar struct {
	Name        string
	Description string
	Param       deploymentParamList
	CloudCred   stringList
	GitCred     string
}

// Run ...
func (c *UpdateDeploymentCommand) Run(args []string) int {
	flagSet := c.getFlagSet()
	err := flagSet.Parse(args)
	if err != nil {
		c.UI.Error(err.Error())
		c.UI.Error(c.Help())
		return 1
	}
	args = flagSet.Args()

	if len(args) < 1 {
		c.UI.Error("Incorrect number of positional arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	deploymentID := args[0]

	var updateRequest = hm.DeploymentUpdate{}
	var needsUpdate bool
	if deploymentUpdateFlagVar.Name != "" {
		updateRequest.Name = &deploymentUpdateFlagVar.Name
		needsUpdate = true
	}
	if deploymentUpdateFlagVar.Description != "" {
		updateRequest.Description = &deploymentUpdateFlagVar.Description
		needsUpdate = true
	}
	if deploymentUpdateFlagVar.CloudCred != nil && len(deploymentUpdateFlagVar.CloudCred) > 0 {
		updateRequest.CloudCredentials = deploymentUpdateFlagVar.CloudCred
		needsUpdate = true
	}
	if deploymentUpdateFlagVar.GitCred != "" {
		updateRequest.GitCredentialID = &deploymentUpdateFlagVar.GitCred
		needsUpdate = true
	}
	if !needsUpdate {
		fmt.Println("nothing to update")
		return 0
	}

	marshal, err := json.Marshal(updateRequest)
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	req := c.NewRequest("PATCH", "/deployments/"+deploymentID, string(marshal))
	c.DoRequest(req)
	return 0
}

func (c *UpdateDeploymentCommand) getFlagSet() *flag.FlagSet {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	flagSet.StringVar(&deploymentUpdateFlagVar.Name, "name", "", "deployment name.")
	flagSet.StringVar(&deploymentUpdateFlagVar.Description, "description", "", "deployment description.")
	flagSet.Var(&deploymentUpdateFlagVar.Param, "param", "parameter values for deployment run.")
	flagSet.Var(&deploymentUpdateFlagVar.CloudCred, "cloud-cred", "cloud credential deployment.")
	flagSet.StringVar(&deploymentUpdateFlagVar.GitCred, "git-cred", "", "git credential for fetching the template used by deployment.")
	return flagSet
}

// Synopsis ...
func (c *UpdateDeploymentCommand) Synopsis() string {
	return "update deployment [DEPLOYMENT ID]"
}

// Help ...
func (c *UpdateDeploymentCommand) Help() string {
	helpText := `
	Usage: cacao update deployment [DEPLOYMENT ID]

	Options:
		-name					update name
		-description			update description
		-param <key>=<value>	update parameter key-value pairs
		-cloud-cred				update cloud credential
		-git-cred				update git credential
`
	return strings.TrimSpace(helpText)
}

// implements flag.Value
type deploymentParamList []hm.KeyValue

// String ...
func (i *deploymentParamList) String() string {
	marshal, err := json.Marshal([]hm.KeyValue(*i))
	if err != nil {
		panic(err)
	}
	return string(marshal)
}

// Set ...
func (i *deploymentParamList) Set(value string) error {
	split := strings.SplitN(value, "=", 2)
	if len(split) < 2 {
		return fmt.Errorf("parameter pair '%s' is not valid", value)
	}
	*i = append(*i, hm.KeyValue{Key: split[0], Value: split[1]})
	return nil
}

// stringList implements flag.Value
type stringList []string

func (l stringList) String() string {
	if l == nil {
		return "nil"
	}
	if len(l) == 0 {
		return "[]"
	}
	var result1 bytes.Buffer
	result1.WriteRune('[')
	for i, str := range l {
		result1.WriteString(str)
		if i < len(l)-1 {
			result1.WriteString(", ")
		}
	}
	result1.WriteRune(']')
	return result1.String()
}

func (l stringList) Set(s string) error {
	if l == nil {
		l = make([]string, 0, 1)
	}
	l = append(l, s)
	return nil
}
