package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// GetCommand ...
type GetCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *GetCommand) Synopsis() string {
	return "get information on a cluster, credential, user, version, workspace or Work Flow Definition "
}

// Help ...
func (c *GetCommand) Help() string {
	helpText := `
	Usage: cacao get <subcommand>

`
	return strings.TrimSpace(helpText)
}
