package command

import (
	"strings"
)

// DeleteDeploymentCommand ...
type DeleteDeploymentCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteDeploymentCommand) Run(args []string) int {
	if len(args) != 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("DELETE", "/deployments/"+args[0], "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *DeleteDeploymentCommand) Synopsis() string {
	return "delete a run by [Runner ID]"
}

// Help ...
func (c *DeleteDeploymentCommand) Help() string {
	helpText := `
Usage: cacao delete deployment [DEPLOYMENT ID]

Delete a deployment.
`
	return strings.TrimSpace(helpText)
}
