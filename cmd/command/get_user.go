package command

import (
	"strings"

	log "github.com/sirupsen/logrus"
)

// GetUserCommand ...
type GetUserCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetUserCommand) Run(args []string) int {
	log.Trace("command/get_user.Run: start")
	var username string
	if len(args) > 0 {
		username = "/" + args[0]
	}

	req := c.NewRequest("GET", "/users"+username, "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetUserCommand) Synopsis() string {
	return "get users"
}

// Help ...
func (c *GetUserCommand) Help() string {
	helpText := `
Usage: cacao get user [USERNAME]

	Gets all users unless specific USERNAME is provided
`
	return strings.TrimSpace(helpText)
}
