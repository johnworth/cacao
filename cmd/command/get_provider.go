package command

import (
	"strings"

	log "github.com/sirupsen/logrus"
)

// GetProviderCommand ...
type GetProviderCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetProviderCommand) Run(args []string) int {
	log.Trace("command/get_provider.Run: start")
	var id string
	if len(args) > 0 {
		id = "/" + args[0]
	}

	req := c.NewRequest("GET", "/providers"+id, "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetProviderCommand) Synopsis() string {
	return "get providers"
}

// Help ...
func (c *GetProviderCommand) Help() string {
	helpText := `
Usage: cacao get provider [ID]

	Gets all providers unless specific ID is provided
`
	return strings.TrimSpace(helpText)
}
