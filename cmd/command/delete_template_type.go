package command

import (
	"strings"
)

// DeleteTemplateTypeCommand ...
type DeleteTemplateTypeCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteTemplateTypeCommand) Run(args []string) int {
	if len(args) != 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("DELETE", "/templates/types/"+args[0], "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *DeleteTemplateTypeCommand) Synopsis() string {
	return "delete a template type"
}

// Help ...
func (c *DeleteTemplateTypeCommand) Help() string {
	helpText := `
Usage: cacao delete template_type [template type name]

Run 'cacao get template_type' to view list of template types.
`
	return strings.TrimSpace(helpText)
}
