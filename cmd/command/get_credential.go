package command

import (
	"flag"
	"strings"
)

// GetCredentialCommand ...
type GetCredentialCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetCredentialCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	//variable should be oid, will be used in future

	if len(args) == 0 {
		req := c.NewRequest("GET", "/credentials", "")
		c.DoRequest(req)
		return 0
	}

	if args[0] == "help" {
		c.PrintDebug(c.Help())
		c.UI.Error(c.Help())
		return 1
	}

	id := flagSet.String("name", "", "credential name to retrieve")
	flagSet.Parse(args)
	args = flagSet.Args()

	c.PrintDebug("Parsed command line flags:\n  name: '%s'", *id)

	req := c.NewRequest("GET", "/credentials/"+(*id), "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetCredentialCommand) Synopsis() string {
	return "get a credential for user"
}

// Help ...
func (c *GetCredentialCommand) Help() string {
	helpText := `
Usage: cacao get credential -name a OR cacao get credential

Options:
	-name <string>
  		identifier of the credential`
	return strings.TrimSpace(helpText)
}
