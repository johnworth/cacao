package command

import (
	"strings"
)

// GetTemplateTypeCommand ...
type GetTemplateTypeCommand struct {
	Meta
	*BaseCommand
}

// Run ...
func (c *GetTemplateTypeCommand) Run(args []string) int {
	var templateTypeName string
	if len(args) > 0 {
		templateTypeName = "/" + args[0]
	}

	req := c.NewRequest("GET", "/templates/types"+templateTypeName, "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetTemplateTypeCommand) Synopsis() string {
	return "get template types"
}

// Help ...
func (c *GetTemplateTypeCommand) Help() string {
	helpText := `
Usage: cacao get template_type [name]

	Gets all template types unless specific name is provided
`
	return strings.TrimSpace(helpText)
}
