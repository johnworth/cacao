package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// DeleteCommand ...
type DeleteCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *DeleteCommand) Synopsis() string {
	return "delete a cluster, credential, user, workspace or Work Flow Definition "
}

// Help ...
func (c *DeleteCommand) Help() string {
	helpText := `
	Usage: cacao delete <subcommand>

`
	return strings.TrimSpace(helpText)
}
