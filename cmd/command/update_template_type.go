package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"io/ioutil"
	"strings"
)

// UpdateTemplateTypeCommand ...
type UpdateTemplateTypeCommand struct {
	*BaseCommand
}

// Run ...
func (c *UpdateTemplateTypeCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	formats := flagSet.String("formats", "", "a comma-separated list of template formats")
	engine := flagSet.String("engine", "", "name of template engine")
	providers := flagSet.String("providers", "", "a comma-separated list of provider types")
	flagSet.Parse(args)
	args = flagSet.Args()

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  formats: '%s'\n  engine: '%s'\n  providers: '%s'\n", *filename, *formats, *engine, *providers)

	if len(args) == 1 && args[0] == "help" {
		c.UI.Error(c.Help())
		return 0
	}

	if len(args) != 1 {
		c.UI.Error("Incorrect number of positional arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	templateTypeName := args[0]

	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))
	}

	data = c.override(data, formats, engine, providers)

	req := c.NewRequest("PATCH", "/templates/types/"+templateTypeName, "")

	// Only fill request body if it is not empty
	if len(data) > 0 {
		req.Body = ioutil.NopCloser(bytes.NewReader(data))
	}
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *UpdateTemplateTypeCommand) Synopsis() string {
	return "update a template type"
}

// Help ...
func (c *UpdateTemplateTypeCommand) Help() string {
	helpText := `
	Usage: cacao update template_type [options] [NAME]

	Options:
		-f <filename>
			name of JSON file to read info from
		-formats <string>
			a comma-separated list of template formats the template type can support
		-engine <string>
			template engine the type supports
		-providers <string>
			a comm-separated list of providers the template type supports
`
	return strings.TrimSpace(helpText)
}

func (c *UpdateTemplateTypeCommand) override(data []byte, formats *string, engine *string, providers *string) []byte {
	var templateTypeJSON struct {
		Formats       []string `json:"formats,omitempty"`
		Engine        string   `json:"engine,omitempty"`
		ProviderTypes []string `json:"provider_types,omitempty"`
	}

	json.Unmarshal(data, &templateTypeJSON)

	if len(*formats) > 0 {
		formatStrings := strings.Split(*formats, ",")
		formatStringsTrimmed := make([]string, 0, len(formatStrings))
		for _, s := range formatStrings {
			formatStringsTrimmed = append(formatStringsTrimmed, strings.TrimSpace(s))
		}

		templateTypeJSON.Formats = formatStringsTrimmed
	}

	if len(*engine) > 0 {
		templateTypeJSON.Engine = *engine
	}

	if len(*providers) > 0 {
		providersStrings := strings.Split(*providers, ",")
		providersStringsTrimmed := make([]string, 0, len(providersStrings))
		for _, s := range providersStrings {
			providersStringsTrimmed = append(providersStringsTrimmed, strings.TrimSpace(s))
		}

		templateTypeJSON.ProviderTypes = providersStringsTrimmed
	}

	data, err := json.Marshal(templateTypeJSON)
	if err != nil {
		panic(err)
	}
	return data
}
