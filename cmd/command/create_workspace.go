package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
)

// CreateWorkspaceCommand ...
type CreateWorkspaceCommand struct {
	*BaseCommand
}

// Run ...
func (c *CreateWorkspaceCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	name := flagSet.String("name", "", "name of workspace")
	description := flagSet.String("description", "", "description")
	defaultProviderID := flagSet.String("default_provider_id", "", "default provider id")
	flagSet.Parse(args)

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  name: '%s'\n  description: '%s'\n  defaultProviderID: '%s'\n", *filename, *name, *description, *defaultProviderID)

	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))

		if len(*name) > 0 || len(*description) > 0 || len(*defaultProviderID) > 0 {
			data = c.override(data, name, description, defaultProviderID)
			c.PrintDebug("Successfully overrode parameters in file:\n%s\n", string(data))
		}
	} else {
		if len(*name) == 0 {
			c.UI.Error(c.Help())
			return 1
		}
		data = []byte(fmt.Sprintf(`{
	"name": "%s",
	"description": "%s",
	"default_provider_id": "%s"
}`, *name, *description, *defaultProviderID))
	}

	c.PrintDebug("Constructed request body:\n%s\n", string(data))

	req := c.NewRequest("POST", "/workspaces", "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *CreateWorkspaceCommand) Synopsis() string {
	return "create a workspace"
}

// Help ...
func (c *CreateWorkspaceCommand) Help() string {
	helpText := `
	Usage: cacao create workspace [options]

	Of the following options, it is required that you use
	either '-f <filename>' or utilize other options

	Options:
		-f <filename>
			name of json file to read user info from. Cannot be used with
			other options
		-name <string>
			name of the workspace
			(required)
		-description <string>
			(optional) description
		-default_provider_id <string>
			(optional) default provider id to use if none is specified
`
	return strings.TrimSpace(helpText)
}

func (c *CreateWorkspaceCommand) override(data []byte, name *string, description *string, defaultProviderID *string) []byte {
	var workspaceJSON struct {
		Name              string `json:"name,omitempty"`
		Description       string `json:"description,omitempty"`
		DefaultProviderID string `json:"default_provider_id,omitempty"`
	}

	json.Unmarshal(data, &workspaceJSON)

	if len(*name) > 0 {
		workspaceJSON.Name = *name
	}
	if len(*description) > 0 {
		workspaceJSON.Description = *description
	}
	if len(*defaultProviderID) > 0 {
		workspaceJSON.DefaultProviderID = *defaultProviderID
	}

	data, err := json.Marshal(workspaceJSON)
	if err != nil {
		panic(err)
	}
	return data
}
