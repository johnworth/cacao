package command

import (
	"strings"
)

// DeleteTemplateCommand ...
type DeleteTemplateCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteTemplateCommand) Run(args []string) int {
	if len(args) != 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("DELETE", "/templates/"+args[0], "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *DeleteTemplateCommand) Synopsis() string {
	return "delete a template"
}

// Help ...
func (c *DeleteTemplateCommand) Help() string {
	helpText := `
Usage: cacao delete template [template ID]

Run 'cacao get template' to view list of templates.
`
	return strings.TrimSpace(helpText)
}
