package command

import (
	"strings"
)

// DeleteWorkspaceCommand ...
type DeleteWorkspaceCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteWorkspaceCommand) Run(args []string) int {
	if len(args) != 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("DELETE", "/workspaces/"+args[0], "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *DeleteWorkspaceCommand) Synopsis() string {
	return "delete a workspace"
}

// Help ...
func (c *DeleteWorkspaceCommand) Help() string {
	helpText := `
Usage: cacao delete workspace [workspace ID]

Run 'cacao get workspace' to view list of workspaces.
`
	return strings.TrimSpace(helpText)
}
