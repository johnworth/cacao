package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"io/ioutil"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// CreateTemplateCommand ...
type CreateTemplateCommand struct {
	*BaseCommand
}

// Run ...
func (c *CreateTemplateCommand) Run(args []string) int {
	log.Trace("create_template.go/Run start")
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	name := flagSet.String("name", "", "name of template")
	description := flagSet.String("desc", "", "description")
	public := flagSet.String("public", "", "public template")
	sourceType := flagSet.String("source_type", "", "template source type")
	sourceURI := flagSet.String("source_uri", "", "template source URI")
	sourceBranch := flagSet.String("source_branch", "", "template source branch")
	sourceTag := flagSet.String("source_tag", "", "template source tag")
	sourcePath := flagSet.String("source_path", "", "template source path in the repo")
	sourcePrivate := flagSet.String("source_private", "", "template source requires auth")
	credentialID := flagSet.String("credential_id", "", "credential ID for source access")

	flagSet.Parse(args)

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  name: '%s'\n  description: '%s'\n  public: '%s'\n	source type: '%s'\n	source URI: '%s'\n	source branch: '%s'\n	source tag: '%s'\n	source path: '%s'\n	source private: '%s'\n	credential ID: '%s'\n",
		*filename, *name, *description, *public, *sourceType, *sourceURI, *sourceBranch, *sourceTag, *sourcePath, *sourcePrivate, *credentialID)

	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))

		if len(*name) > 0 || len(*description) > 0 || len(*public) > 0 || len(*sourceType) > 0 || len(*sourceURI) > 0 || len(*sourceBranch) > 0 || len(*sourceTag) > 0 || len(*sourcePath) > 0 || len(*sourcePrivate) > 0 || len(*credentialID) > 0 {
			data = c.override(data, name, description, public, sourceType, sourceURI, sourceBranch, sourceTag, sourcePath, sourcePrivate, credentialID)
			c.PrintDebug("Successfully overrode parameters in file:\n%s\n", string(data))
		}
	} else {
		if len(*sourceType) == 0 {
			c.UI.Error(c.Help())
			return 1
		}

		if len(*sourceURI) == 0 {
			c.UI.Error(c.Help())
			return 1
		}

		if len(*sourcePrivate) != 0 {
			sourcePrivateBool, err := strconv.ParseBool(*sourcePrivate)
			if err != nil {
				sourcePrivateBool = false
			}

			if sourcePrivateBool {
				// private source
				// requires credential ID
				if len(*credentialID) == 0 {
					c.UI.Error(c.Help())
					return 1
				}
			}

		}

		data = c.override(data, name, description, public, sourceType, sourceURI, sourceBranch, sourceTag, sourcePath, sourcePrivate, credentialID)
	}

	c.PrintDebug("Constructed request body:\n%s\n", string(data))

	req := c.NewRequest("POST", "/templates", "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *CreateTemplateCommand) Synopsis() string {
	return "import a template"
}

// Help ...
func (c *CreateTemplateCommand) Help() string {
	helpText := `
	Usage: cacao create template [options]

	Of the following options, it is required that you use
	either '-f <filename>' or utilize other options

	Options:
		-f <filename>
			name of json file to read user info from. Cannot be used with
			other options
		-name <string>
			(optional) name of the template
		-desc <string>
			(optional) description of the template
		-public <string>
			(optional) set this template public
		-source_type <string>
			type of the template source
			(required)
		-source_uri <string>
			URI of the template source
			(required)
		-source_branch <string>
			(optional) branch of the template source (GIT)
		-source_tag <string>
			(optional) tag of the template source (GIT)
		-source_path <string>
			(optional) path in the template source repo (GIT)
		-source_private <string>
			(optional) set this template source to require authentication, requires credential_id
		-credential_id <string>
			(optional) credential ID to access the private template source
`
	return strings.TrimSpace(helpText)
}

func (c *CreateTemplateCommand) override(data []byte, name *string, description *string, public *string, sourceType *string, sourceURI *string, sourceBranch *string, sourceTag *string, sourcePath *string, sourcePrivate *string, credentialID *string) []byte {
	log.Trace("create_template.go/override start")

	var templateJSON struct {
		Name        string                              `json:"name,omitempty"`
		Description string                              `json:"description,omitempty"`
		Public      bool                                `json:"public,omitempty"`
		Source      cacao_common_service.TemplateSource `json:"source,omitempty"`
	}

	json.Unmarshal(data, &templateJSON)

	if len(*name) > 0 {
		templateJSON.Name = *name
	}

	if len(*description) > 0 {
		templateJSON.Description = *description
	}

	if len(*public) > 0 {
		isPublic, err := strconv.ParseBool(*public)
		if err != nil {
			isPublic = false
		}

		templateJSON.Public = isPublic
	}

	if len(*sourceType) > 0 {
		templateJSON.Source.Type = cacao_common_service.TemplateSourceType(strings.ToLower(*sourceType))
	}

	if len(*sourceURI) > 0 {
		templateJSON.Source.URI = *sourceURI
	}
	log.Trace("create_template.go/override: templateJSON.Source.URI = " + templateJSON.Source.URI)

	// before setting AccessParameters, must initialize if it doesn't exist
	if templateJSON.Source.AccessParameters == nil {
		templateJSON.Source.AccessParameters = make(map[string]interface{})
	}

	if len(*sourceBranch) > 0 {
		templateJSON.Source.AccessParameters["branch"] = *sourceBranch
	}

	if len(*sourceTag) > 0 {
		templateJSON.Source.AccessParameters["tag"] = *sourceBranch
	}

	if len(*sourcePath) > 0 {
		templateJSON.Source.AccessParameters["path"] = *sourcePath
	}

	if len(*sourcePrivate) > 0 {
		isSourcePrivate, err := strconv.ParseBool(*sourcePrivate)
		if err != nil {
			isSourcePrivate = false
		}

		if isSourcePrivate {
			templateJSON.Source.Visibility = cacao_common_service.TemplateSourceVisibilityPrivate
		} else {
			templateJSON.Source.Visibility = cacao_common_service.TemplateSourceVisibilityPublic
		}
	}

	if len(templateJSON.Source.Visibility) == 0 {
		templateJSON.Source.Visibility = cacao_common_service.TemplateSourceVisibilityPublic
	}

	log.Trace("create_template.go/override: marshaling data")
	data, err := json.Marshal(templateJSON)
	if err != nil {
		panic(err)
	}
	return data
}
