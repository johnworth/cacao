package command

import (
	"strings"
)

// GetTemplateCommand ...
type GetTemplateCommand struct {
	Meta
	*BaseCommand
}

// Run ...
func (c *GetTemplateCommand) Run(args []string) int {
	var templateID string
	if len(args) > 0 {
		templateID = "/" + args[0]
	}

	req := c.NewRequest("GET", "/templates"+templateID, "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetTemplateCommand) Synopsis() string {
	return "get templates"
}

// Help ...
func (c *GetTemplateCommand) Help() string {
	helpText := `
Usage: cacao get template [ID]

	Gets all templates unless specific ID is provided
`
	return strings.TrimSpace(helpText)
}
