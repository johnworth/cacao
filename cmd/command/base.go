package command

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	rawurl "net/url"
	"os"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

var home string = os.Getenv("HOME")

// APIAddress is used to save address of API
type APIAddress struct {
	APIUrl string `json:"api"`
}

// Username is used to save the username
type Username struct {
	Username string `json:"username"`
}

// BaseCommand ...
type BaseCommand struct {
	Meta
	cacaoAPI  string
	authToken string
	username  string
}

// NewBaseCommand ...
func NewBaseCommand(meta Meta) (c BaseCommand) {
	log.Trace("NewBaseCommand: start")
	c.Meta = meta
	c.GetCacaoAPI()
	c.GetUsername()
	c.CheckIDToken()
	return
}

// Client returns an HTTP Client to be used by the CLI
func (c *BaseCommand) Client() *http.Client {
	log.Trace("Client: start")

	return &http.Client{}
}

// DoRequest will either execute a Request or print the curl command
func (c *BaseCommand) DoRequest(req *http.Request) {
	log.Trace("DoRequest: start")
	c.CheckConfiguration(false)
	c.PrintDebug("Request struct:\n%v\n", req)
	if len(req.Header.Get("Authorization")) == 0 {
		// set the auth header again, in case user just logged in
		req.Header.Set("Authorization", c.authToken)
	}

	if c.OutputCurlString {
		c.PrintCurlString(req)
	} else {
		resp, err := c.Client().Do(req)
		if err != nil {
			panic(err)
		}
		c.PrintDebug("Response struct:\n%v\n", resp)
		c.PrintHTTPResponse(resp)
	}
}

// SaveRequest will either execute a Request or print the curl command
func (c *BaseCommand) SaveRequest(req *http.Request) bool {
	log.Trace("SaveRequest: start")

	c.PrintDebug("Request struct:\n%v\n", req)
	if c.OutputCurlString {
		c.PrintCurlString(req)
	}
	resp, err := c.Client().Do(req)
	if err != nil {
		panic(err)
	}
	c.PrintDebug("Response struct:\n%v\n", resp)
	return c.SaveResponse(resp)
}

// NewRequest creates a new HTTP request with the correct header
func (c *BaseCommand) NewRequest(verb, path string, data string) *http.Request {
	log.Trace("NewRequest: start")
	req, err := http.NewRequest(verb, c.cacaoAPI+path, nil)
	if err != nil {
		panic(err)
	}
	if len(data) > 0 {
		req.Body = ioutil.NopCloser(strings.NewReader(data))
	}
	req.Header.Add("Authorization", c.authToken)
	return req
}

func isError(err error) bool {
	log.Trace("isError: start")
	if err != nil {
		fmt.Println(err.Error())
	}
	return (err != nil)
}

// CheckConfiguration checks to see if the configuration file exists.
func (c *BaseCommand) CheckConfiguration(isLoginCommand bool) {
	log.Trace("command/base.go/CheckConfiguration: start")
	var loggedIn bool

	// if ~/.cacao/config.json doesn't exist ask for username and password
	// else check the id_token found in the file
	_, err := os.Stat(home + "/.cacao/config.json")

	if os.IsNotExist(err) {

		if !isLoginCommand {
			fmt.Println("You need to login first before using this command.")
		}

		if _, loggedIn = c.PostUser(); !loggedIn {
			panic(!loggedIn)
		}
	} else if isLoginCommand {
		err := c.UpdateSession()
		if err != nil {
			panic(err)
		}
	}
	c.CheckIDToken()
	c.PrintDebug("Logged in Successfully to Keycloak!")
	return
}

// GetCredentials passes back username and password
func (c *BaseCommand) GetCredentials() (string, string) {
	log.Trace("GetCredentials: start")
	var username string
	if c.username != "" {
		username = c.GetInput("Username " + "(" + c.username + ")" + ": ")
		if username == "" {
			username = c.username
		}
	} else {
		username = c.GetInput("Username: ")
	}
	password, err := c.UI.AskSecret("Password:")
	if err != nil {
		panic(err)
	}
	return username, password
}

// PostUser posts the user information to Rest API
func (c *BaseCommand) PostUser() (string, bool) {
	log.Trace("PostUser: start")
	tries := 0
	var username, passwd string
	for tries < 3 {
		username, passwd = c.GetCredentials()
		req := c.NewRequest("POST", "/user/login", fmt.Sprintf("username=%s&password=%s", username, passwd))

		loggedIn := c.SaveRequest(req)
		c.PrintDebug("loggedIn is : " + strconv.FormatBool(loggedIn))

		if loggedIn {
			tries = 5
			fmt.Println("Successfully logged in!!")
			// Saving the username on a JSON file
			naf := Username{username}
			result, err := json.Marshal(naf)
			if err != nil {
				panic(err)
			}
			err = ioutil.WriteFile(home+"/.cacao/username.json", result, 0644)
			if err != nil {
				panic(err)
			}
			c.username = username

		} else {
			fmt.Println("Incorrect username or password, please try again.")
			tries++
			if tries == 3 {
				fmt.Println("Unable to log in with provided credentials")
				return "", false
			}
		}
	}
	return username, true
}

// CheckIDToken checks if the IDToken exists in the config.json file
func (c *BaseCommand) CheckIDToken() {
	log.Trace("CheckIDToken: start")
	var exists bool
	var authToken string
	authToken, exists = c.GetJSONValue("id_token", "config")
	if exists {
		c.authToken = authToken
	}
}

// UpdateSession logs out the user, clears the token and deleted all the files on ~/.cacao/
func (c *BaseCommand) UpdateSession() error {
	log.Trace("UpdateSession: start")
	var loggedIn bool
	home := os.Getenv("HOME")
	// Removing all the files on ~/.cacao/
	err := os.Remove(home + "/.cacao/config.json")
	if err != nil {
		fmt.Printf("No user is currently signed in\n")
		panic(err)
	}
	errs := os.Remove(home + "/.cacao/api.json")
	if errs != nil {
		fmt.Printf("api.json not found or could not be removed\n")
	}
	errs = os.Remove(home + "/.cacao/username.json")
	if errs != nil {
		fmt.Printf("username.json not found or could not be removed\n")
	}
	c.AskForCacaoAPI()
	if _, loggedIn = c.PostUser(); !loggedIn {
		panic(!loggedIn)
	}
	c.authToken = ""
	return err
}

// GetInput gets the input from the user
func (c *BaseCommand) GetInput(prompt string) string {
	log.Trace("GetInput: start")

	fmt.Printf(prompt)
	reader := bufio.NewReader(os.Stdin)
	input, _ := reader.ReadString('\n')
	input = strings.Replace(input, "\n", "", -1)
	return input
}

// AskForCacaoAPI asks for the user to enter the address of the API
func (c *BaseCommand) AskForCacaoAPI() {
	log.Trace("AskForCacaoAPI: start")

	var tries int
	var cacaoAPI string
	fmt.Println("Please provide address of Cacao API.")
	fmt.Println("Format Should be: http://<hostname>:<port>	  or    <hostname>:<port> ")
	fmt.Println("(Developers: this should match value of API_DOMAIN in install/config.yml)")
	for tries < 3 {
		if c.cacaoAPI == "" {
			cacaoAPI = c.GetInput("Cacao API address: ")
		} else {
			var newCacaoAPI string
			cacaoAPI = c.GetInput("Cacao API address (" + c.cacaoAPI + "):")
			if newCacaoAPI == "" {
				cacaoAPI = c.cacaoAPI
			}
		}
		if len(cacaoAPI) > 4 && cacaoAPI[0:4] != "http" {
			cacaoAPI = "http://" + cacaoAPI
		}
		if _, err := rawurl.ParseRequestURI(cacaoAPI + "/users"); err != nil {
			if tries == 2 {
				fmt.Println("Invalid API address, please run cacao command again to try")
				panic(err)
			}
			fmt.Println("Invalid API address, please try again.")
		} else {
			_, err := os.Stat(home + "/.cacao")
			if os.IsNotExist(err) {
				errDir := os.MkdirAll(home+"/.cacao", 0755)
				if errDir != nil {
					panic(err)
				}
			}
			naf := APIAddress{cacaoAPI}
			result, err := json.Marshal(naf)
			if err != nil {
				panic(err)
			}
			err = ioutil.WriteFile(home+"/.cacao/api.json", result, 0644)
			if err != nil {
				panic(err)
			}
			c.cacaoAPI = cacaoAPI
			tries = 2
		}
		tries++
	}
}

// GetJSONValue extracts the value from the json object
func (c *BaseCommand) GetJSONValue(key string, filename string) (string, bool) {
	log.Trace("GetJSONValue: start")

	var exists bool
	var value string
	jsonFile, _ := os.Open(home + "/.cacao/" + filename + ".json")
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)

	// Check to see if the length of the bytes is greater than '0'
	if len(byteValue) > 0 {
		var result map[string]interface{}
		json.Unmarshal([]byte(byteValue), &result)
		if v, ok := result[key]; ok {
			exists = true
			value = v.(string)
			c.PrintDebug("value: " + value + "\n")
		}
	}
	return value, exists
}

// GetHTTPResponse returns the response of the http request
func (c *BaseCommand) GetHTTPResponse() *http.Response {
	log.Trace("GetHTTPResponse: start")

	req := c.NewRequest("GET", "/users", "")
	response, err := c.Client().Do(req)
	if err != nil {
		panic(err)
	}
	return response
}

// GetCacaoAPI checks for ~/.cacao/api.json or asks users for cacao API address
func (c *BaseCommand) GetCacaoAPI() {
	log.Trace("GetCacaoAPI: start")

	var cacaoAPI string
	var exists bool
	_, err := os.Stat(home + "/.cacao/api.json")
	if os.IsNotExist(err) {
		c.AskForCacaoAPI()
	} else {
		cacaoAPI, exists = c.GetJSONValue("api", "api")
		if !exists {
			c.AskForCacaoAPI()
		}
		if _, err := rawurl.ParseRequestURI(cacaoAPI + "/users"); err != nil {
			c.AskForCacaoAPI()
		}
		c.cacaoAPI = cacaoAPI
	}

}

// GetUsername checks for ~/.cacao/api.json or asks users for cacao API address
func (c *BaseCommand) GetUsername() {
	log.Trace("GetUsername: start")

	var username string
	var exists bool
	_, err := os.Stat(home + "/.cacao/username.json")
	if os.IsNotExist(err) {
		c.username = ""
	} else {
		username, exists = c.GetJSONValue("username", "username")
		if !exists {
			c.username = ""
		}
		c.username = username
	}
}
