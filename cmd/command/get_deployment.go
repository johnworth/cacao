package command

import (
	"flag"
	"gitlab.com/cyverse/cacao-common/common"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

// GetDeploymentCommand ...
type GetDeploymentCommand struct {
	Meta
	*BaseCommand
	deploymentID string
	workspaceID  string
	fullRun      bool
	offset       int
	pageSize     int
}

// Help ...
func (c GetDeploymentCommand) Help() string {
	helpText := `
Usage: cacao get deployment [DEPLOYMENT ID]
       cacao get deployment

	Gets a single deployment if deployment ID is provided.
	Otherwise, list deployments that a user has.

	-workspace filter by workspace
	-full-run get full run object for last run
	-offset offset for pagination
	-page-size page size for pagination
`
	return strings.TrimSpace(helpText)
}

func (c GetDeploymentCommand) printHelp() {
	c.UI.Output(c.Help())
}

// Run ...
func (c *GetDeploymentCommand) Run(args []string) int {
	if len(args) < 1 {
		// no args provided, list all deployment user has
		return c.list()
	}
	id := common.ID(args[0])
	if id.Validate() && id.Prefix() == "deployment" {
		return c.getOne(id)
	}

	flagSet := flag.NewFlagSet("", flag.ContinueOnError)
	flagSet.Usage = c.printHelp
	flagSet.StringVar(&c.workspaceID, "workspace", "", "filter deployments by workspace")
	flagSet.BoolVar(&c.fullRun, "full-run", false, "return full run object for last run")
	flagSet.IntVar(&c.offset, "offset", 0, "offset for pagination")
	flagSet.IntVar(&c.pageSize, "page-size", -1, "page size for pagination, -1 for no limit")
	err := flagSet.Parse(args)
	if err != nil {
		return 1
	}
	c.list()
	return 0
}

func (c GetDeploymentCommand) getOne(deploymentID common.ID) int {
	req := c.NewRequest("GET", "/deployments/"+deploymentID.String(), "")
	c.DoRequest(req)
	return 0
}

func (c GetDeploymentCommand) list() int {
	var req *http.Request
	var query = url.Values{}
	if c.workspaceID != "" {
		query.Set("workspace-id", c.workspaceID)
	}
	if c.fullRun {
		query.Set("full-run", "t")
	}
	if c.offset > 0 {
		query.Set("offset", strconv.Itoa(c.offset))
	}
	if c.pageSize > 0 {
		query.Set("page-size", strconv.Itoa(c.pageSize))
	}
	path := "/deployments"
	if query.Encode() != "" {
		path += "?" + query.Encode()
	}
	req = c.NewRequest("GET", path, "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c GetDeploymentCommand) Synopsis() string {
	return "get deployment"
}
