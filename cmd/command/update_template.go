package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"io/ioutil"
	"strconv"
	"strings"

	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// UpdateTemplateCommand ...
type UpdateTemplateCommand struct {
	*BaseCommand
}

const (
	emptyTemplateArgumentString = "$$EMPTY_ARGUMENT_STRING$$"
)

// processEmptyArgument checks if arg is given or not. If given, return true.
// if arg is not given via CLI argument, make the variable empty and return false.
func (c *UpdateTemplateCommand) processEmptyArgument(arg *string) bool {
	setValue := true
	if *arg == emptyTemplateArgumentString {
		setValue = false
		*arg = ""
	}
	return setValue
}

// Run ...
func (c *UpdateTemplateCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	name := flagSet.String("name", emptyTemplateArgumentString, "name of template")
	description := flagSet.String("desc", emptyTemplateArgumentString, "description")
	public := flagSet.String("public", emptyTemplateArgumentString, "public template")
	sourceType := flagSet.String("source_type", emptyTemplateArgumentString, "template source type")
	sourceURI := flagSet.String("source_uri", emptyTemplateArgumentString, "template source URI")
	sourceBranch := flagSet.String("source_branch", emptyTemplateArgumentString, "template source branch")
	sourceTag := flagSet.String("source_tag", emptyTemplateArgumentString, "template source tag")
	sourcePath := flagSet.String("source_path", emptyTemplateArgumentString, "template source path in the repo")
	sourcePrivate := flagSet.String("source_private", emptyTemplateArgumentString, "template source requires auth")
	flagSet.Parse(args)
	args = flagSet.Args()

	setName := c.processEmptyArgument(name)
	setDescription := c.processEmptyArgument(description)
	setPublic := c.processEmptyArgument(public)
	setSourceType := c.processEmptyArgument(sourceType)
	setSourceURI := c.processEmptyArgument(sourceURI)
	setSourceBranch := c.processEmptyArgument(sourceBranch)
	setSourceTag := c.processEmptyArgument(sourceTag)
	setSourcePath := c.processEmptyArgument(sourcePath)
	setSourcePrivate := c.processEmptyArgument(sourcePrivate)

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  name: '%s'\n  description: '%s'\n  public: '%s'\n	source type: '%s'\n	source URI: '%s'\n	source branch: '%s'\n	source tag: '%s'\n	source path: '%s'\n	source private: '%s'\n",
		*filename, *name, *description, *public, *sourceType, *sourceURI, *sourceBranch, *sourceTag, *sourcePath, *sourcePrivate)

	if len(args) == 1 && args[0] == "help" {
		c.UI.Error(c.Help())
		return 0
	}

	if len(args) != 1 {
		c.UI.Error("Incorrect number of positional arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	if len(*name) == 0 && setName {
		c.UI.Error("Template Name must not be empty")
		return 1
	}

	if setSourceType || setSourceURI || setSourceBranch || setSourceTag || setSourcePath || setSourcePrivate {
		if len(*sourceType) == 0 {
			c.UI.Error("Template Source Type must not be empty")
			return 1
		}

		if len(*sourceURI) == 0 {
			c.UI.Error("Template Source URI must not be empty")
			return 1
		}

		if len(*sourceBranch) == 0 && setSourceBranch {
			c.UI.Error("Template Source Branch must not be empty")
			return 1
		}

		if len(*sourceTag) == 0 && setSourceTag {
			c.UI.Error("Template Source Tag must not be empty")
			return 1
		}

		if len(*sourcePath) == 0 && setSourcePath {
			c.UI.Error("Template Source Path must not be empty")
			return 1
		}

		if len(*sourcePrivate) == 0 && setSourcePrivate {
			c.UI.Error("Template Source Private must not be empty")
			return 1
		}
	}

	templateID := args[0]

	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))
	}

	data = c.override(data, name, setName, description, setDescription, public, setPublic, sourceType, setSourceType, sourceURI, setSourceURI, sourceBranch, setSourceBranch, sourceTag, setSourceTag, sourcePath, setSourcePath, sourcePrivate, setSourcePrivate)

	req := c.NewRequest("PATCH", "/templates/"+templateID, "")

	// Only fill request body if it is not empty
	if len(data) > 0 {
		req.Body = ioutil.NopCloser(bytes.NewReader(data))
	}
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *UpdateTemplateCommand) Synopsis() string {
	return "update a template"
}

// Help ...
func (c *UpdateTemplateCommand) Help() string {
	helpText := `
	Usage: cacao update template [options] [ID]

	Options:
		-f <filename>
			name of JSON file to read info from
			-name <string>
			(optional) name of the template
		-desc <string>
			(optional) description of the template
		-public <string>
			(optional) set this template public
		-source_type <string>
			type of the template source
			(required)
		-source_uri <string>
			URI of the template source
			(required)
		-source_branch <string>
			(optional) branch of the template source (GIT)
		-source_tag <string>
			(optional) tag of the template source (GIT)
		-source_path <string>
			(optional) path in the template source repo (GIT)
		-source_private <bool>
			(optional) set this template source to require authentication, requires credential_id
`
	return strings.TrimSpace(helpText)
}

func (c *UpdateTemplateCommand) override(data []byte, name *string, setName bool, description *string, setDescription bool, public *string, setPublic bool, sourceType *string, setSourceType bool, sourceURI *string, setSourceURI bool, sourceBranch *string, setSourceBranch bool, sourceTag *string, setSourceTag bool, sourcePath *string, setSourcePath bool, sourcePrivate *string, setSourcePrivate bool) []byte {
	var templateJSON struct {
		Name        string      `json:"name,omitempty"`
		Description string      `json:"description,omitempty"`
		Public      bool        `json:"public,omitempty"`
		Source      interface{} `json:"source,omitempty"`
		//Source      cacao_common_service.TemplateSource `json:"source,omitempty"`
	}

	json.Unmarshal(data, &templateJSON)

	if setName {
		templateJSON.Name = *name
	}

	if setDescription {
		templateJSON.Description = *description
	}

	if setPublic {
		isPublic, err := strconv.ParseBool(*public)
		if err != nil {
			isPublic = false
		}

		templateJSON.Public = isPublic
	}

	if setSourceType || setSourceURI || setSourceBranch || setSourceTag || setSourcePath || setSourcePrivate {
		source := cacao_common_service.TemplateSource{}
		source.AccessParameters = make(map[string]interface{})

		if setSourceType {
			source.Type = cacao_common_service.TemplateSourceType(strings.ToLower(*sourceType))
		}

		if setSourceURI {
			source.URI = *sourceURI
		}

		if setSourceBranch {
			source.AccessParameters["branch"] = *sourceBranch
		}

		if setSourceTag {
			source.AccessParameters["tag"] = *sourceBranch
		}

		if setSourcePath {
			source.AccessParameters["path"] = *sourcePath
		} else {
			source.AccessParameters["path"] = "/"
		}

		source.Visibility = cacao_common_service.TemplateSourceVisibilityPrivate

		if setSourcePrivate {
			isSourcePrivate, err := strconv.ParseBool(*sourcePrivate)
			if err != nil {
				isSourcePrivate = false
			}

			if isSourcePrivate {
				source.Visibility = cacao_common_service.TemplateSourceVisibilityPrivate
			} else {
				source.Visibility = cacao_common_service.TemplateSourceVisibilityPublic
			}
		}

		templateJSON.Source = source
	}

	data, err := json.Marshal(templateJSON)
	if err != nil {
		panic(err)
	}
	return data
}
