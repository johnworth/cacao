package command

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strings"

	"github.com/fatih/color"
	prettyjson "github.com/hokaccha/go-prettyjson"
	"github.com/mitchellh/cli"
)

// Meta contain the meta-option that nearly all subcommand inherited.
type Meta struct {
	UI               cli.Ui
	OutputFormat     string
	OutputCurlString bool
	Debug            bool
}

// PrintCurlString will output a string that is the equivalent curl command for
// the specified request. This will be helpful for debugging. A majority of the
// code in this function comes from Hashicorp Vault's equivalent function:
// https://github.com/hashicorp/vault/blob/528604359c20a32fef628d86abdb7663e81235c3/api/output_string.go#L35-L64
func (m *Meta) PrintCurlString(req *http.Request) {
	result := "curl"
	result += fmt.Sprintf(" -X %s", req.Method)
	for k, v := range req.Header {
		for _, h := range v {
			result += fmt.Sprintf(` -H "%s: %s"`, k, h)
		}
	}

	var body []byte
	var err error
	if req.Body != nil {
		body, err = ioutil.ReadAll(req.Body)
		if err != nil {
			m.UI.Error("Unable to parse request body when constructing curl string")
			return
		}
		req.Body = ioutil.NopCloser(bytes.NewBuffer(body))
	}

	if len(body) > 0 {
		removeWhitespace := regexp.MustCompile(`[\t\n]`)
		escapedBody := string(removeWhitespace.ReplaceAll(body, []byte("")))
		escapedBody = strings.Replace(escapedBody, "'", "'\"'\"'", -1)
		result += fmt.Sprintf(" -d '%s'", escapedBody)
	}

	result += fmt.Sprintf(" %s", req.URL.String())
	m.UI.Output(result)
}

// PrintHTTPResponse is used to print the body and error (if it exists) from an HTTP Response
func (m *Meta) PrintHTTPResponse(response *http.Response) {
	respBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}

	// Determine if response contains Object or Array
	x := bytes.TrimLeft(respBody, " \t\r\n")
	isObject := len(x) > 0 && x[0] == '{'

	if len(respBody) > 0 {
		if response.StatusCode < 300 {
			if m.OutputFormat == "default_pretty" && isObject {
				var respJSON map[string]interface{}
				json.Unmarshal(respBody, &respJSON)

				formatter := NewFormatter()
				prettyJSON, err := formatter.Marshal(respJSON)
				if err != nil {
					panic(err)
				}
				m.UI.Output(string(prettyJSON))
			} else {
				m.UI.Output(string(respBody))
			}
		} else {
			m.UI.Error(string(respBody))
		}
	}
}

// PrintDebug is used to print some output only when the debug flag is used
func (m *Meta) PrintDebug(msg string, extras ...interface{}) {
	if m.Debug {
		m.UI.Output(fmt.Sprintf(msg, extras...))
	}
}

// NewFormatter returns a new formatter with following default values.
func NewFormatter() *prettyjson.Formatter {
	return &prettyjson.Formatter{
		KeyColor:        color.New(color.FgBlue, color.Bold),
		StringColor:     color.New(color.FgGreen, color.Bold),
		BoolColor:       color.New(color.FgYellow, color.Bold),
		NumberColor:     color.New(color.FgCyan, color.Bold),
		NullColor:       color.New(color.FgBlack, color.Bold),
		StringMaxLength: 0,
		DisabledColor:   false,
		Indent:          4,
		Newline:         "\n",
	}
}

// SaveResponse is used to save keycloak login information from an HTTP Response
func (m *Meta) SaveResponse(response *http.Response) bool {
	respBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	errResp := string(respBody)[2:7]
	if len(respBody) > 0 {
		if response.StatusCode < 300 && errResp != "error" {
			home := os.Getenv("HOME")
			_, err := os.Stat(home + "/.cacao")
			if os.IsNotExist(err) {
				errDir := os.MkdirAll(home+"/.cacao", 0755)
				if errDir != nil {
					panic(err)
				}
			}
			err = ioutil.WriteFile(home+"/.cacao/config.json", respBody, 0644)
			if err != nil {
				panic(err)
			}
			fmt.Println("Successfully saved id_token at " + home + "/.cacao/config.json")
			return true
		}
	}
	return false
}
