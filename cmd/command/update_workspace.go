package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"io/ioutil"
	"strings"
)

// UpdateWorkspaceCommand ...
type UpdateWorkspaceCommand struct {
	*BaseCommand
}

const (
	emptyWorkspaceArgumentString = "$$EMPTY_ARGUMENT_STRING$$"
)

// processEmptyArgument checks if arg is given or not. If given, return true.
// if arg is not given via CLI argument, make the variable empty and return false.
func (c *UpdateWorkspaceCommand) processEmptyArgument(arg *string) bool {
	setValue := true
	if *arg == emptyWorkspaceArgumentString {
		setValue = false
		*arg = ""
	}
	return setValue
}

// Run ...
func (c *UpdateWorkspaceCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	name := flagSet.String("name", emptyWorkspaceArgumentString, "name of workspace")
	description := flagSet.String("description", emptyWorkspaceArgumentString, "description")
	defaultProviderID := flagSet.String("default_provider_id", emptyWorkspaceArgumentString, "default provider id")
	flagSet.Parse(args)
	args = flagSet.Args()

	setName := c.processEmptyArgument(name)
	setDescription := c.processEmptyArgument(description)
	setDefaultProviderID := c.processEmptyArgument(defaultProviderID)

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  name: '%s'\n  description: '%s'\n  defaultProviderID: '%s'\n", *filename, *name, *description, *defaultProviderID)

	if len(args) == 1 && args[0] == "help" {
		c.UI.Error(c.Help())
		return 0
	}

	if len(args) != 1 {
		c.UI.Error("Incorrect number of positional arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	if len(*name) == 0 && setName {
		c.UI.Error("Workspace Name must not be empty")
		return 1
	}

	workspaceID := args[0]

	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))
	}

	data = c.override(data, name, setName, description, setDescription, defaultProviderID, setDefaultProviderID)

	req := c.NewRequest("PATCH", "/workspaces/"+workspaceID, "")

	// Only fill request body if it is not empty
	if len(data) > 0 {
		req.Body = ioutil.NopCloser(bytes.NewReader(data))
	}
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *UpdateWorkspaceCommand) Synopsis() string {
	return "update a workspace"
}

// Help ...
func (c *UpdateWorkspaceCommand) Help() string {
	helpText := `
	Usage: cacao update workspace [options] [ID]

	Options:
		-f <filename>
			name of JSON file to read info from
		-name <string>
			Name of the workspace, should not be an empty string
		-description <string>
			description
		-default_provider_id <string>
			default provider id to use
`
	return strings.TrimSpace(helpText)
}

func (c *UpdateWorkspaceCommand) override(data []byte, name *string, setName bool, description *string, setDescription bool, defaultProviderID *string, setDefaultProviderID bool) []byte {
	var workspaceJSON struct {
		Name              string `json:"name,omitempty"`
		Description       string `json:"description,omitempty"`
		DefaultProviderID string `json:"default_provider_id,omitempty"`
	}

	json.Unmarshal(data, &workspaceJSON)

	if setName {
		workspaceJSON.Name = *name
	}

	if setDescription {
		workspaceJSON.Description = *description
	}

	if setDefaultProviderID {
		workspaceJSON.DefaultProviderID = *defaultProviderID
	}

	data, err := json.Marshal(workspaceJSON)
	if err != nil {
		panic(err)
	}
	return data
}
