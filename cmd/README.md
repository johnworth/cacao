# CACAO CLI

### Usage

Build using:

```bash
go build -o cacao
```

Then, use according to usage instructions from `./cacao --help` and `./cacao --help <subcommand>`

Before using commands other than 'help', you need to define the following environment variables:
  - `CACAO_API` - base URL for API
    - **NOTE**: if this does not lead with the scheme, `http://` will be prepended so if `https` is required, make sure it is included in the variable
  - `CACAO_TOKEN` - Keycloak ID Token or password if using userpass auth locally


### Development

To add a new command, you first need to add a new file in `command/` and follow the example of other files. Then, add the struct to the map in `commands.go`.
